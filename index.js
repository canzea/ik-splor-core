import * as d3 from 'd3';

export {LayoutFactory as layouts} from "./src/LayoutFactory";
export {ShapeFactory as shapes} from "./src/ShapeFactory";

// Will be deprecated
export {LayoutFactory as LayoutFactory} from "./src/LayoutFactory";
// Will be deprecated
export {ShapeFactory as ShapeFactory} from "./src/ShapeFactory";

export {ViewerCanvas as ViewerCanvas} from "./src/ViewerCanvas";
export {default as Registry} from "./src/Registry";

export {
  version
} from "./build/package";
