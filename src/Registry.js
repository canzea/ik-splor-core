//export * from './ViewerCanvas';
//export * from './ShapeFactory';
//export * from './LayoutFactory';
import {ShapeFactory} from './ShapeFactory';
import {ViewerCanvas} from './ViewerCanvas';
import {LayoutFactory} from './LayoutFactory';

export function yell(name) { return `HEY ${name.toUpperCase()}!!`; }

export default class DUDE {
    constructor() {
    }
}

import CommentShape from './shapes/comment/shape.js';
ShapeFactory.register(new CommentShape("comment"));

import ApplicationShape from './shapes/application/shape.js';
ShapeFactory.register(new ApplicationShape("application"));

import Icon from './shapes/icon/shape.js';
ShapeFactory.register(new Icon("icon"));
import Image from './shapes/image/shape.js';
ShapeFactory.register(new Image("image"));

import Circle from './shapes/circle/shape.js';
ShapeFactory.register(new Circle("circle"));
ShapeFactory.register(new Circle("circle2"));
ShapeFactory.register(new Circle("circle3"));

import Milestone from './shapes/milestone/shape.js';
ShapeFactory.register(new Milestone("milestone"));


import Button from './shapes/button/shape.js';
ShapeFactory.register(new Button("button"));

import Timeline from './shapes/timeline/shape.js';
ShapeFactory.register(new Timeline("timeline"));

import Timeline2 from './shapes/timeline2/shape.js';
ShapeFactory.register(new Timeline2("timeline2"));

import Panel from './shapes/panel/shape.js';
ShapeFactory.register(new Panel("panel"));

import List from './shapes/list/shape.js';
ShapeFactory.register(new List("list"));

import Dimension from './shapes/dimension/shape.js';
ShapeFactory.register(new Dimension("dimension"));


import DefaultShape from './shapes/box/shape';
ShapeFactory.register(new DefaultShape("box"));

import TabShape from './shapes/tab/shape';
ShapeFactory.register(new TabShape("tab"));


import Swimlane from './layouts/swimlane/layout.js';
LayoutFactory.register(new Swimlane("swimlane"));

import DefaultLayout from './layouts/default.js';
LayoutFactory.register(new DefaultLayout("default"));

import TabLayout from './layouts/tabs/tabs.js';
LayoutFactory.register(new TabLayout("tabs"));

import TimelineLayout from './layouts/timeline/timeline.js';
LayoutFactory.register(new TimelineLayout("timelines"));

import CarouselLayout from './layouts/carousel/carousel.js';
LayoutFactory.register(new CarouselLayout("carousel"));


// Specifics
import Lifecycle from './shapes/lifecycle/shape.js';
ShapeFactory.register(new Lifecycle("lifecycle"));

ShapeFactory.clone('box', 'box2');