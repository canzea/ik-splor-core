//import BuildingBlockShape from './shape-catalog/BuildingBlock';

class _ShapeFactory {

    constructor() {
        this.shapes = {};
    }

    getShapeNames() {
        return Object.keys(this.shapes);
    }

    getShape (shape) {
        if (!this.shapes.hasOwnProperty(shape)) {
            console.log("WARN: Shape not found " + shape);
            return null;
        }
        return this.shapes[shape];
    }

    clone (shapeFrom, shapeTo) {
        let object = Object.create(this.shapes[shapeFrom]);
        object.domainType = shapeTo;
        this.shapes[object.domainType] = object;
        return this;
    }

    register (object) {
        this.shapes[object.domainType] = object;
        return this;
    }
}

export let ShapeFactory = new _ShapeFactory();
