
import './style.scss';
import fa from 'fontawesome';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      let x = 100;
      let y = 50;

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y);

      chgSet.append("text")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", 0)
          .text(function(d) { return d.label; });

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", (x/2) - 12)
          .attr("dy", -(y/2) + 15)
          .text(function(d) { return fa(d.icon); });

  }
}

export default Shape;
