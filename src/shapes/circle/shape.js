import './style.scss';

import * as d3 from 'd3';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {

      chgSet.append("circle");

      chgSet.filter(function (d) { return !d.hasOwnProperty("position") || d.position == "right"; })
          .append("text")
          .attr("text-anchor", "left")
          .attr("dx", function (d) { const circle = d3.select(this.parentNode).select('circle').node(); return 2 + (circle.getBBox().width/2); })
          .attr("dy", 5)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

      chgSet.filter(function (d) { return d.position == "center"; })
          .append("text")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

  }
}

export default Shape;
