
import fa from "fontawesome";
import * as d3 from 'd3';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {

      chgSet.append("text")
          .attr("class", function(d) { return (d.class ? "icon " + d.class:"icon"); })
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 5)
          .text(function(d) { return fa(d.icon); });

      chgSet.append("text").filter(function (d) { return d.label; })
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "2.0em")
          .text(function(d) { return d.label; });

  }
}

export default Shape;
