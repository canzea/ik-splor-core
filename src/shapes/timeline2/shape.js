
import fa from 'fontawesome';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      const shape = this.domainType;

      chgSet.append("rect")
          .attr("class", "line")
          .attr("rx", function (d) { return shape == "timeline2" ? 8:0; })
          .attr("ry", function (d) { return shape == "timeline2" ? 8:0; })
          .style("fill", function(d) { return (d.hasOwnProperty("color") ? d.color:"blue"); })
          .attr("width", function (d) { return (d.hasOwnProperty("width") ? d.width : 200); })
          .attr("height", 20);


      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", 10)
          .attr("dy", 14)
          .text(function(d) { return fa(d.icon); });

      const txtNode = chgSet.append("text")
          .attr("text-anchor", "left")
          .attr("dx", 18)
          .attr("dy", 14)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

  }
}

export default Shape;
