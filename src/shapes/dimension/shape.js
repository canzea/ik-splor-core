
import * as d3 from 'd3';
import './style.scss';
import fa from "fontawesome";

import TextUtils from '../../utils/TextUtils';

class Shape {

  constructor(domainType) {
    this.domainType = domainType;
  }

  update(fullSet) {
  }

  build(chgSet) {
      let x = 100;
      let y = 50;

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y);
  }
}

export default Shape;

