
import * as d3 from "d3";
import './style.scss';
import {ViewerCanvas} from "../../ViewerCanvas";
import TextUtils from '../../utils/TextUtils';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      let self = this;

      chgSet.classed("clickable", true);

      // Width calculated by the layout
      chgSet.append("rect")
          .attr("x", -150/2)
          .attr("y", -40/2)
          .attr("height", 40)
          .attr("width", 150);

      const textNode = chgSet.append("text")
          .attr("class", "label")
          .attr("text-anchor", "middle");

      textNode.append("tspan")
            .attr("x", 0)
            .attr("y", function (d) { const lines = TextUtils.splitByNL(d.label); return (lines.length == 1 ? 5:-3); })
            .text(function (d) { const lines = TextUtils.splitByNL(d.label); return lines[0]; });

      textNode.append("tspan")
            .attr("x", 0)
            .attr("y", function (d) { const bbox = this.parentNode.getBBox(); return bbox.height - 3; })
            .text(function (d) { const lines = TextUtils.splitByNL(d.label); return (lines.length == 1 ? "":lines[1]); });

  }

}

export default Shape;
