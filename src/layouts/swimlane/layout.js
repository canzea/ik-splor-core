import './style.css';
import Default from '../default.js';

class Layout extends Default {
  constructor(domainType) {
    super(domainType);
    this.domainType = domainType;
  }

  build(d3visual, zone, root) {
      var self = this;
      let obj = root.append("g");

      obj.classed("_zone", true);
      obj.classed("_" + this.domainType, true);

//      let vb = d3visual.attr("viewBox").split(' ');
//
//      let viewWidth = vb[2];
//      let viewHeight = vb[3];
//
//      console.log("_swimlane: Resizing zone : W="+viewWidth+",H="+viewHeight);
//
//      let pageWidth = d3visual.attr("width");
//      let pageHeight = d3visual.attr("height");
//      // THis needs to be a calculation based on a ratio
//
//      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
//
//      let rectDimension = [];
//
//      for ( let v = 0; v < 4; v++) {
//          if (typeof zone.rectangle[v] == "string") {
//            //console.log("Evaluating: " + zone.rectangle[v]);
//            //console.log(" ... to : " + eval(zone.rectangle[v]));
//            rectDimension.push(eval(zone.rectangle[v]));
//          } else {
//            rectDimension.push(zone.rectangle[v]);
//          }
//      }
//      zone.calculatedRectangle = rectDimension;
//
//      if (pageHeightSet) {
//          rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
//      }

      this.calculateRectangle (d3visual, zone);

      let rect = obj.append("rect")
          //.style("filter", "url(#drop-shadow)")
          .attr("x", zone.calculatedRectangle[0])
          .attr("y", zone.calculatedRectangle[1])
          .attr("width", zone.calculatedRectangle[2])
          .attr("height", zone.calculatedRectangle[3])
          .attr("class", "lane"); //fff899

      obj.append("rect")
          .attr("x", zone.calculatedRectangle[0])
          .attr("y", zone.calculatedRectangle[1])
          .attr("width", function (d) { return zone.orientation == "horizontal" ? 80: 40; })
          .attr("height", zone.calculatedRectangle[3])
          .attr("class", "laneLabel"); //fff899

      let transform = "translate(" + zone.calculatedRectangle[0] + "," + zone.calculatedRectangle[1] + ") rotate(-90) translate(" + (- (zone.calculatedRectangle[3] / 2)) + "," + "15" + ")";
      if (zone.orientation == "horizontal") {
        transform = "translate(" + (zone.calculatedRectangle[0] + (80/2)) + "," + (zone.calculatedRectangle[1] + (zone.calculatedRectangle[3]/2)) + ")";
      }

      const textNodes = obj
          .append("text")
          .attr("transform", transform)
          .attr("text-anchor", "middle")
          //.attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", 0);

      textNodes.append("tspan")
            .attr("x", 0)
            .attr("y", 0)
            .text(function (d) { const lines = self.splitCaption(zone.title, 20); return lines[0]; });

      textNodes.append("tspan")
            .attr("x", 0)
            .attr("y", 15)
            .text(function (d) { const lines = self.splitCaption(zone.title, 20); return (lines.length == 1 ? "":lines[1]); });

      if (zone.hasOwnProperty("image")) {
          transform = "translate(" + (zone.calculatedRectangle[0] + (10)) + "," + (zone.calculatedRectangle[1] + (zone.calculatedRectangle[3]/2) - 60) + ")";

          obj.append("image")
              .attr("width", function(d) { return 60; })
              .attr("height", function(d) { return 40; })
              .attr("xlink:href", function(d) { return zone.image; })
              .attr("transform", transform);
      }
/*
      let box = null;

      rect.each (function (d) {
        box = this.getBBox();
      });

          */
  }

  splitCaption(caption, MAXIMUM_CHARS_PER_LINE=6) {

    var words = caption.split(' ');
    var line = "";

    var lines = [];
    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + " ";
        if (testLine.length > MAXIMUM_CHARS_PER_LINE && line.length > 0)
        {
            lines.push(line);

            line = words[n] + " ";
        }
        else {
            line = testLine;
        }
    }
    lines.push(line);

    return lines;
  }
}

export default Layout;