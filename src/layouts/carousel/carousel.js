
import * as d3 from "d3";
import './style.scss';
import {ViewerCanvas} from '../../ViewerCanvas';
import Default from '../default.js';

class Layout extends Default {
  constructor(domainType) {
    super(domainType);
    this.domainType = domainType;
  }

  build(d3visual, zone, root) {
      const self = this;
      let obj = root.append("g");

      obj.classed("_zone", true);
      obj.classed("_" + this.domainType, true);

      let vb = d3visual.attr("viewBox").split(' ');

      let viewWidth = vb[2];
      let viewHeight = vb[3];

      for ( let v = 0; v < 4; v++) {
          if (typeof zone.rectangle[v] == "string") {
            //console.log("Evaluating: " + zone.rectangle[v]);
            //console.log(" ... to : " + eval(zone.rectangle[v]));
            zone.rectangle[v] = eval(zone.rectangle[v]);
          }
      }

      let rect = obj.append("rect")
          .style("filter", "url(#drop-shadow)")
          .attr("x", zone.rectangle[0])
          .attr("y", zone.rectangle[1])
          .attr("width", zone.rectangle[2])
          .attr("height", zone.rectangle[3]);//fff899

  }

  enrichData (nodes) {
//
//      let segs = [];
//      for ( let x = 0 ; x < graph.nodes.length; x++) {
//          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
//          if (seg != "" && segs.indexOf(seg) < 0) {
//              segs.push(seg);
//          }
//      }

      this.recurse(nodes, 0);

//      // calculate the segment layout
//      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
//      for ( let p = 0; p < segs.length; p++) {
//          let tot = 0;
//          let x,num;
//          for ( x = 0 ; x < graph.nodes.length; x++) {
//              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
//              if (seg == segs[p]) {
//                  tot++;
//              }
//              if (graph.nodes[x].children) {
//                  let ls = graph.nodes[x].children;
//                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
//                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
//                      if (seg == segs[p]) {
//                        tot++;
//                      }
//
//                      if (ls[xL1].children) {
//                          let lsL2 = ls[xL1].children;
//                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
//                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
//                              if (seg == segs[p]) {
//                                tot++;
//                              }
//                          }
//                      }
//                  }
//              }
//          }
//          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
////              this.recurse(graph.nodes, 1);
//              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
////              if (seg == segs[p]) {
////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
////                  num++;
////              }
////              this.recurse(graph.nodes[x], 1);
////              if (graph.nodes[x].children) {
////                  let ls = graph.nodes[x].children;
////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
////                      if (seg == segs[p]) {
////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
////                          numL1++;
////                      }
////                      this.recurse(ls[xL1], 2);
//
////                      if (ls[xL1].children) {
////                          let lsL2 = ls[xL1].children;
////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
////                              if (seg == segs[p]) {
////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
////                                  numL2++;
////                              }
////                          }
////                      }
//
//           //       }
//            //  }
//          }
//      }
  }

  recurse (nodes, level) {
      let tot = nodes.length;
      for ( let num = 0,x = 0 ; x < nodes.length; x++) {
          const nd = nodes[x];
          if (nd.type == "panel" || level > 0) {
              nd.layout = { "seq":num,"total":tot,"seg":"SEG"+level, "segnum":level, "level":level };
              num++;
              if (nd.children) {
                  this.recurse(nd.children, level+1);
              }
          }
      }
  }

  configEvents(d3visual, newNodesTotal, zones, viewer) {
      let self = this;

      let newNodes = newNodesTotal.filter(function(d) { return d.type == 'icon'; });

      let force = viewer.force;

      // Use x and y if it was provided, otherwise stick it in the center of the zone
      newNodesTotal
          .attr("cx", function(d) { const dim = self.getDim(d, zones); return d.x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); })
          .attr("cy", function(d) { const dim = self.getDim(d, zones); return d.y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); });

//nodeSet, index
      newNodes
        .on('mouseenter', function (e) {
//            $(this).addClass("node-hover");
//            console.log("NODE: " +JSON.stringify(nodeSet));
        })
        .on('mouseleave', function (e) {
            //$(this).removeClass("node-hover");
        })
        .on('click', function (e) {
            d3.event.stopPropagation();
            self.togglePanel(viewer, this.parentNode, (e.name == "down")); /*self.toggleNavigation(viewer, e, this); */
        });

        function dragstarted(d) {
            if (!d3.event.active) force.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
            viewer.downX = Math.round(d.x + d.y);
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d) {
            if (!d3.event.active) force.alphaTarget(0);
            d.fx = null;
            d.fy = null;
            setTimeout (function () {
                viewer.trigger('change');
            }, 200);
        }

        newNodesTotal.call(d3.drag()
           .on("start", dragstarted)
           .on("drag", dragged)
           .on("end", dragended));

        newNodes.each(function (d, i) {
            if (i == 0) {
                self.togglePanel (viewer, this.parentNode, true);
            }
        });
  }

  getDim(d, zones) {
      const index = d.boundary.charCodeAt(0) - 65;
      if (zones.length <= index) {
          //console.log("INVALID BOUNDARY: " + d.boundary);
      }
      const rect = zones[index].rectangle;
      const dim = {x1:rect[0],y1:rect[1],x2:rect[0] + rect[2],y2:rect[1] + rect[3]};
      //console.log("K: " + JSON.stringify(dim, null, 2));
      return dim;
  }

  togglePanel (viewer, root, down) {
     let panels = [];
     d3.select(root).selectAll("g._panel").each ( function (d) {
        panels.push(d);
        d3.select(root).selectAll("g.node_" + d.name).style("display", "none");
     });

     if (viewer.chosenPanel >= 0) {
        let data = panels[viewer.chosenPanel];
        d3.select(root).selectAll("g.node_" + data.name).style("display", "none");
     } else {
        viewer.chosenPanel = -1;
     }

     if (down) {
         viewer.chosenPanel--;
         if (viewer.chosenPanel < 0) {
            viewer.chosenPanel = 0; //(panels.length - 1);
         }
     } else {
         viewer.chosenPanel++;
         if (viewer.chosenPanel >= panels.length) {
            viewer.chosenPanel = (panels.length - 1);
         }
     }
     let data = panels[viewer.chosenPanel];
     d3.select(root).selectAll("g.node_" + data.name).style("display", "block");

     this.updateTableOfContents(viewer, root, panels);
  }

  updateTableOfContents (viewer, root, panels) {
     d3.select(root).select("g._carousel").remove();

     d3.select(root).append("g").classed("_carousel", true).selectAll(".toc")
        .data(panels)
        .enter()
        .append("text")
        .classed("toc", true)
        .style("fill", "black")
        .classed("selected", function (x, i) { return (i == viewer.chosenPanel ? true:false); })
        .attr("transform", function (x, i) { return "translate(" + 15 + "," + (25+(i*14)) + ")"; })
        .text(function (x) { return x.label; });

     d3.select(root).selectAll(".toc")
        .data(panels)
        .exit().remove();

  }

  toggleNavigation (viewer, e, object) {
     d3.event.stopPropagation();

     if (viewer.downX == Math.round(e.x + e.y)) {
        viewer.navigation.toggle(e, d3.select(object));
     } else {
        viewer.navigation.close(e);
     }
  }

  organize(d3visual, zoneIndex, zones) {
  }
}

export default Layout;