
import * as d3 from "d3";
import './style.scss';

class Layout {
  constructor(domainType) {
    this.domainType = domainType;
  }

  enrichData () {
  }

  organize(d3visual, zone, root) {
  }

  configEvents(d3visual, newNodes, zones, viewer) {
      let self = this;

      let force = viewer.force;

      // Use x and y if it was provided, otherwise stick it in the center of the zone
      newNodes
          .attr("cx", function(d) { const dim = self.getDim(d, zones); return d.x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); })
          .attr("cy", function(d) { const dim = self.getDim(d, zones); return d.y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); });

//nodeSet, index
      newNodes
        .on('mouseenter', function (e) {
//            $(this).addClass("node-hover");
//            console.log("NODE: " +JSON.stringify(nodeSet));
        })
        .on('mouseleave', function (e) {
            //$(this).removeClass("node-hover");
        })
        .on('click', function (e) {
            //const set = vis.select("g.nodeSet").selectAll("g.node");
            if (viewer.downX == Math.round(e.x + e.y)) {
              //let matchedItem = set.filter(function (d) { return (e.index == d.index); });
              viewer.navigation.toggle(e, d3.select(this));
            } else {
              viewer.navigation.close(e);
            }
        });


          function dragstarted(d) {
                d3.select(this).classed("fixed", d.fixed == true);
                if (!d3.event.active) force.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
                viewer.downX = Math.round(d.x + d.y);

          }

          function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
          }

          function dragended(d) {
                if (!d3.event.active) force.alphaTarget(0);

                if (!d.fixed) {
                    d.fx = null;
                    d.fy = null;
                }
                setTimeout (function () {
                    viewer.trigger('change');
                }, 200);
          }

          newNodes.call(d3.drag()
              .on("start", dragstarted)
              .on("drag", dragged)
              .on("end", dragended));


      //let clickable = newNodes.filter(function(d) { return d3.select(this).classed('clickable'); });
      let clickable = newNodes.selectAll('.clickable');
      clickable
        .on('click', function (e, i) { d3.event.stopPropagation(); viewer.trigger("shapeClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */});
  }

  getDim(d, zones) {
      const index = d.boundary.charCodeAt(0) - 65;
      if (zones.length <= index) {
          //console.log("INVALID BOUNDARY: " + d.boundary);
      }
      const rect = (zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle);
      const dim = {x1:rect[0],y1:rect[1],x2:rect[0] + rect[2],y2:rect[1] + rect[3]};
      //console.log("K: " + JSON.stringify(dim, null, 2));
      return dim;
  }

  build(d3visual, zone, root, viewer) {
      const self = this;
      let obj = root.append("g");

      obj.classed("_zone", true);
      obj.classed("_" + this.domainType, true);

      obj.append('rect');

      this.refreshSize(viewer, zone, obj);

  }

  refreshSize (viewer, zone, zoneNode) {
      let d3visual = viewer.vis;

      this.calculateRectangle (d3visual, zone);

      let rect = zoneNode.select("rect")
          .style("filter", "url(#drop-shadow)")
          .attr("x", zone.calculatedRectangle[0])
          .attr("y", zone.calculatedRectangle[1])
          .attr("width", zone.calculatedRectangle[2])
          .attr("height", zone.calculatedRectangle[3]);
  }

  calculateRectangle (d3visual, zone) {
      let vb = d3visual.attr("viewBox").split(' ');

      let viewWidth = vb[2];
      let viewHeight = vb[3];

      console.log("_default: Resizing zone : W="+viewWidth+",H="+viewHeight);

      let pageWidth = d3visual.attr("width");
      let pageHeight = d3visual.attr("height");
      // THis needs to be a calculation based on a ratio

      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);

      let rectDimension = [];


      for ( let v = 0; v < 4; v++) {
          if (typeof zone.rectangle[v] == "string") {
            //console.log("Evaluating: " + zone.rectangle[v]);
            //console.log(" ... to : " + eval(zone.rectangle[v]));
            rectDimension.push(eval(zone.rectangle[v]));
          } else {
            rectDimension.push(zone.rectangle[v]);
          }
      }
      if (pageHeightSet) {
          rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
      }

      zone.calculatedRectangle = rectDimension;
      console.log("_default: Resized zone : W="+rectDimension[2]+",H="+rectDimension[3]  );

  }
}

export default Layout;