
class TextUtils {

    static splitByNL (name) {
        // look for a \n
        let ind = name.indexOf("\n");
        if (ind != -1) {
            return [name.substr(0, ind), name.substr(ind)];
        } else {
            return [name];
        }
    }

    static splitByWidth(caption, maxWidth, width) {
        if (maxWidth < width) {
            return [caption];
        }
        const ratio = Math.round(caption.length * (width/maxWidth));
        console.log("Max = "+maxWidth+", "+width + " RATIO: " + ratio + " : " + caption.length);
        return TextUtils.splitByWords (caption, ratio);
    }

    static splitByWords(caption, maxCharsPerLine) {

        var words = caption.split(' ');
        var line = "";

        var lines = [];
        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + " ";
            if (testLine.length > maxCharsPerLine && line.length > 0)
            {
                lines.push(line);

                line = words[n] + " ";
            }
            else {
                line = testLine;
            }
        }
        lines.push(line);

        return lines;
    }
}

export default TextUtils;