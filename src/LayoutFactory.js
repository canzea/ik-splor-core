//import BuildingBlockShape from './shape-catalog/BuildingBlock';

class _LayoutFactory {

    constructor() {
        this.layouts = {};
    }

    getLayoutNames() {
        return Object.keys(this.layouts);
    }

    getLayout (layout) {
        if (!this.layouts.hasOwnProperty(layout)) {
            return null;
        }
        return this.layouts[layout];
    }

    clone (layoutFrom, layoutTo) {
        let object = Object.create(this.layouts[layoutFrom]);
        object.domainType = layoutTo;
        this.layouts[object.domainType] = object;
        return this;
    }

    register (object) {
        this.layouts[object.domainType] = object;
        return this;
    }
}

export let LayoutFactory = new _LayoutFactory();
