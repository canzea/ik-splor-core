import * as d3 from "d3";
import Navigation from './NavigationHorizontal.js';

import fa from 'fontawesome';

import {ShapeFactory} from './ShapeFactory';
import {LayoutFactory} from './LayoutFactory';

export class ViewerCanvas {
  constructor() {
    this.defaultConfig = {
       "menu_items": [],
       "pan_and_zoom": false,
       "enable_zone_boundaries": false,
       "enable_collision_avoidance": false,
       "preferred_ratio": 0.6666666,
       "background_color": "black",
       "fixed_width": false,
       "paper_width": 1000,
       "force_collide": 0
    };
  }

  mountNode(root) {
    this.clientWidth = root.clientWidth;
    this.domNode = d3.select(root).append("svg").node();
    this.contentNode = d3.select(root).append("div").node();
    this.initCanvas();
  }

  setState(state) {
    this.state = state;
  }

  trigger (event, data, doc = this.state) {
    if (this.events && this.events[event]) {
        let func = this.events[event][0];
        for (let funcIndex = 0; funcIndex < this.events[event].length; funcIndex++ ) {
            let func = this.events[event][funcIndex];
            func(event, data, doc);
        }
    }
  }

  on (event, func) {
    if (typeof this.events == "undefined") {
        this.events = [];
    }

    if (typeof this.events[event] == "undefined") {
        this.events[event] = [];
    }
    this.events[event].push(func);
  }

  zoom (vbox) {
      console.log("vbox = "+vbox);
      let vb = this.vis.attr("viewBox"); // "0 0 1000 800"
      this.vis.transition().attr("viewBox", vbox);
      return vb;
  }

  printRect(elemRect, label="E") {
     console.log(label + ": Top=" + elemRect.top+",Left="+elemRect.left+",Width="+elemRect.width+",Height="+elemRect.height);
  }

  zoomToNode (node, zoomAmount) {
      const self = this;
      let nd = d3.selectAll('g.node').filter(function (d, i) { return node == i; }).each(function (d) {

          //self.vis.attr("viewBox", "0 0 1000 800");

          let bb = this.getBoundingClientRect();
          self.printRect(bb, "View box");
          // 0 0 1000 150
          //let zoom = d3.zoom().scaleBy(5);
          //zoom.scaleBy(this, 5);
          let cy = Number(d3.select(this).attr("cy"));
          let cx = Number(d3.select(this).attr("cx"));
          var newViewBox = [
            (cx-(0)), cy, 300, (300*150/1000)
          ].join(' ');
          //var transform = d3.zoomTransform(d3.select(this).node());
          //transform.scale(10);
          //self.zoom (newViewBox);
      });
  }

  initCanvas() {
      const self = this;
      const el = this.domNode;

      //this.zones = this.state.zones;

      const paperWidth = this.getConfig("paper_width");
      let ratio = this.getConfig("preferred_ratio");
      const fixedWidth = this.getConfig("fixed_width");


      let full = el.getBoundingClientRect();
      ratio = full.height/full.width;

      const ow = (fixedWidth ? paperWidth : el.parentNode.clientWidth);
      const oh = ow * ratio;


      console.log("W = "+ow +", H = " + oh + " : " + el.parentNode.clientWidth + " (original = " + self.clientWidth+")");

      let vis = d3.select(el)
          .attr("width", ow);

      let rect = [0, 0, paperWidth, paperWidth * ratio];

      const zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];

      vis.attr("preserveAspectRatio", "xMinYMin");
      vis.attr("viewBox", zone); // "0 0 1000 800"

      const defs = vis.append("defs");
      defs.selectAll("marker")
          .data(["suit", "licensing", "resolved", "lifecycle"])
        .enter().append("marker")
          .attr("id", function(d) { return d; })
          .attr("class", function(d) { return "_end_" + d; })
          .attr("viewBox", "0 -5 10 10")
          .attr("refX", 0)
          .attr("refY", 0)
          .attr("markerWidth", 5)
          .attr("markerHeight", 5)
          .attr("orient", "auto")
        .append("path")
          .attr("d", "M0,-5L10,0L0,5");

      const filter = defs.append("filter").attr("id", "drop-shadow").attr("x", 0).attr("y", 0).attr("height", "150%").attr("width", "150%");
      filter.append("feOffset").attr("in", "SourceAlpha").attr("dx", 3).attr("dy", 3).attr("result", "offOut");
      filter.append("feGaussianBlur").attr("in", "offOut").attr("stdDeviation", 1).attr("result", "blurOut");
      filter.append("feBlend").attr("in", "SourceGraphic").attr("in2", "blurOut").attr("mode", "normal");
//      const feMerge = filter.append("feMerge");
//      feMerge.append("feMergeNode").attr("in", "offsetBlur");
//      feMerge.append("feMergeNode").attr("in", "SourceGraphic");


//      let all = vis.append("rect")
//          .style("fill", this.getConfig("background_color"))
//          .attr("width", "100%")
//          .attr("height", "100%")
//          .attr("x", 0)
//          .attr("y", 0);

      let zoneRoot = vis.append("g")
          .attr("class", "zones");

      let linesRoot = vis.append("g").attr("class", "all_lines");

      let nodeSetRoot = vis.append("g").attr("class", "nodeSet");


      const calcLinkDistance = function (a, b) {
//          return Math.abs(Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)));

         if (a.source.boundary != a.target.boundary) {
            return ow/4;
         } else if (a.type == "step-after") {
            return (a.hint ? a.hint : 150);
         } else if (a.type == "straight" || a.type == "step-before") {
//            console.log("POSITIONS: " + JSON.stringify(a.source));
//            console.log(" AND " + JSON.stringify(a.target));
//            console.log(" ..X " + a.source.x +", "+a.target.x);
//            console.log(" ..Y " + a.source.y +", "+a.target.y);
//            console.log(" ..Y " + a.source.y +", "+a.target.y);
//            console.log(" ..CX " + ((a.source.x - a.target.x) * 2));
//            console.log(" ..CY " + ((a.source.y - a.target.y) * 2));
//            console.log(" ..Answer " + (Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
//            console.log("SQRT : " + Math.sqrt(Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
            return (a.hint ? a.hint : 150);
         } else {
            return (a.hint ? a.hint : 80);
         }
      };

      let simulation = d3.forceSimulation()
          .force("link", d3.forceLink([]).distance(calcLinkDistance).strength(0.5))
          .force("collide",d3.forceCollide( function(d){ return self.getConfig("force_collide"); }).iterations(1) )
          .force("charge", d3.forceManyBody().strength(0));

      let force = simulation;
//          .links([])
//          .nodes([])
//          .gravity(0)
//          .charge(0)
//          .linkDistance(calcLinkDistance)
//          .size([ow, oh]);


      this.vis = vis;
      this.force = force;

      // fa-book, fa-th, fa-share-square-o, fa-camera-retro
      const navData = [
        {"id":"1", "title":"Documentation", "icon":fa.book},
        {"id":"2", "title":"Configuration", "icon":fa('tasks')},
        {"id":"3", "title":"Topology", "icon":fa.th },
        {"id":"4", "title":"Audit", "icon":fa('camera-retro') }
      ];

      this.navigation = new Navigation();

      this.navigation.attach(vis, this, this.contentNode);
//      this.navigation.addMenuItem(navData[0]);
//      this.navigation.addMenuItem(navData[1]);
//      this.navigation.addMenuItem(navData[2]);
//      this.navigation.addMenuItem(navData[3]);

      this.navigation.update();
//console.log("NAV = "+faIconChars[0].unicode);

//      all.on("click", function (d) {
//         self.closePopups();
//      });
  }

  closePopups () {
     this.navigation.close();
  }

  refreshZones (zones, config) {

      let self = this;
      let vis = this.vis;

      let pageHeight = vis.node().parentNode.clientHeight;
      let pageWidth = vis.node().parentNode.clientWidth;

      let zoneRoot = vis.select("g.zones");

      zoneRoot.selectAll("g").remove();

      vis.attr("pageWidth", pageWidth);
      vis.attr("pageHeight", pageHeight);

      this.zones = zones;

      zones.forEach(function (zone, zoneIndex) {

        let layout = LayoutFactory.getLayout(zone.type);
        if (layout == null) {
            console.log("WARNING: Layout not found in catalog: " +zone.type);
        } else {
            layout.build(vis, zone, zoneRoot, self);

            layout.organize(vis, zoneIndex, zones);
        }
      });

      zoneRoot.selectAll("g")
        .on('mouseenter', function (nodeSet, index) {
            d3.select(this).classed("zone-hover", true);
        })
        .on('mouseleave', function (e) {
            d3.select(this).classed("zone-hover", false);
        });

      zoneRoot.selectAll("g").selectAll("rect").on("click", function (d) {
         self.closePopups();
      });

  }


  refreshSize() {
      const self = this;
      const el = this.domNode;
      let vis = this.vis;

      const paperWidth = this.getConfig("paper_width");
      const ratio = this.getConfig("preferred_ratio");
      const fixedWidth = this.getConfig("fixed_width");

      const ow = (fixedWidth == false ? el.parentNode.clientWidth : paperWidth);
      const oh = (ratio == 0 ? el.parentNode.clientHeight:ow*ratio); //el.parentNode.clientHeight; //ow * ratio;


      console.log("refreshSize() : SVG [W = "+ow +", H = " + oh + "] :: Parent clientHeight="+  el.parentNode.clientHeight);
      console.log("refreshSize() : Body: W = "+ document.body.clientWidth +", H = " + document.body.clientHeight);

      vis
          .attr("width", ow)
          .attr("height", oh);

      d3.select(this.domNode.parentNode).style("background-color", this.getConfig("background_color"));


      // scale the viewbox to maximize the zoom
      // Adjust the height to nothing larger than what the zones are using
      vis.select("g.zones").each(function (d) {

          // if ratio is 0, then set the height to the height of all zones
          //
          let bbox = this.getBBox();
          let parentBbox = this.parentNode.getBBox();

          if (ratio != 0) {
              let rect = [0, 0, paperWidth, paperWidth * ratio];
              let zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];
              console.log("Setting ViewBox (has ratio) to " + zone);
              vis.attr("viewBox", zone);
          }
      });

      self.zones.forEach(function (zone, zoneIndex) {

        let layout = LayoutFactory.getLayout(zone.type);
        if (zone.type == "tabs" || zone.type == "default" || zone.type == "carousel") {
            layout.refreshSize(self, zone, vis.select("g.zones").filter(function (d, i) { return i == zoneIndex; }));
        }
      });

      vis.select("g.zones").each(function () {

          // if ratio is 0, then set the height to the height of all zones
          //
          let bbox = this.getBBox();
          let parentBbox = this.parentNode.getBBox();

          if (ratio == 0) {
              let rect = [0, 0, paperWidth, (bbox.height)];
              console.log("refreshSize() : BBOX: W = "+ bbox.width +", H = " + bbox.height);

              let zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];

              console.log("Setting ViewBox (ratio=0) to " + zone);
              vis.attr("viewBox", zone);
          }
      });

      this.updatePanZoom();

      this.updateMenuItems();

//
//      let rect = [0, 0, ow, ow * ratio];
//
//      const zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];
//
//      vis.attr("preserveAspectRatio", "xMinYMin")
      //vis.attr("viewBox", zone); // "0 0 1000 800"
  }

  getDim(d) {
      const index = d.boundary.charCodeAt(0) - 65;
      if (this.zones.length <= index) {
          //console.log("INVALID BOUNDARY: " + d.boundary);
      }
      const rect = (this.zones[index].hasOwnProperty('calculatedRectangle') ? this.zones[index].calculatedRectangle : this.zones[index].rectangle);
      const dim = {x1:rect[0],y1:rect[1],x2:rect[0] + rect[2],y2:rect[1] + rect[3]};
      //console.log("K: " + JSON.stringify(dim, null, 2));
      return dim;
  }

  clear() {
      //console.log("CLEARING");
      this.vis.select("g.nodeSet").selectAll("g.node").remove();
  }

  getConfig(c) {
      let config = (this.state ? this.state.config : null);
      if (config && config.hasOwnProperty(c)) {
        return config[c];
      } else {
        return this.defaultConfig[c];
      }
  }

  traverseChildren (newNodes) {
      const self = this;
      const vis = this.svg;

      newNodes.each(function (d) {

          if (d.children) {

              let nodeSet = d3.select(this.parentNode).selectAll("g.node_" + d.name)
                  .data(d.children);

              let newNodesC = nodeSet.enter().append("g")
                .attr("class", "node_" + d.name + " nodec");

              let names = ShapeFactory.getShapeNames();
              for ( let nm in names) {
                  let shap = ShapeFactory.getShape(names[nm]);
                  shap.build(newNodesC.filter(function(d) { return d.type == names[nm]; }).classed("_" + names[nm], true));

                  if (typeof shap.update === 'function') {
                      shap.update(nodeSet.filter(function(d) { return d.type == names[nm]; }).classed("_" + names[nm], true));
                  }

              }

              newNodesC
                  .attr("cx", function(d) { const dim = self.getDim(d); const x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); d.x = x; d.fx =(d.fixed ? x:null); })
                  .attr("cy", function(d) { const dim = self.getDim(d); const y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); d.y = y; d.fy =(d.fixed ? y:null); });

              self.zones.forEach(function (zone, index) {
                let layout = LayoutFactory.getLayout(zone.type);
                let filtered = newNodesC.filter(function (d) { const i = d.boundary.charCodeAt(0) - 65; return index == i; });
                layout.configEvents(vis, filtered, self.zones, self);
              });

//              // Attach Events
//              self.zones.forEach(function (zone, index) {
//                let layout = LayoutFactory.getLayout(zone.type);
//                //layout.configEvents(vis, newNodesC, self.zones, self);
//              });

              self.traverseChildren (newNodesC);
          }
      });
  }

  zoomed (transform) {
        const vis = this.vis;
        vis.select('g.nodeSet').attr("transform", transform);
        vis.select('g.zones').attr("transform", transform);
        vis.select('g.all_lines').attr("transform", transform);
        vis.select('g.nav').attr("transform", transform);
  }

  zoomReset () {
    this.vis.transition()
      .duration(750)
      .call(this.zoom.transform, d3.zoomIdentity);
  }

  updateMenuItems() {
     const self = this;
     const menus = this.getConfig('menu_items');
     menus.forEach ( function (m, i) {
        const nav = {"id":m.id, "title": m.label, "icon":fa(m.icon)};
        self.navigation.addMenuItem(nav);
     });
     self.navigation.update();
  }

  updatePanZoom() {
      const self = this;
      const vis = this.vis;
      const oh = vis.attr("height");
      const ow = vis.attr("width");

      function zoomed() {
         self.zoomed (d3.event.transform);
      }

      this.zoom = d3.zoom()
          .scaleExtent([1, 40])
          .translateExtent([[-100, -100], [ow + 90, oh + 100]])
          .on("zoom", zoomed);

      if (this.getConfig("pan_and_zoom")) {
          vis.call(this.zoom);
      }
  }

  update() {
      const self = this;
      const vis = this.vis;

      const force = this.force;

      self.registerLinks();
      self.refreshZones (this.state.zones, {});

      this.zones.forEach(function (zone, index) {
        let layout = LayoutFactory.getLayout(zone.type);
        layout.enrichData(self.state.nodes);
      });

      const linkInfo = vis.select("g.all_lines")
                .selectAll("g")
                .data(this.state.links);

      const linkGroup = linkInfo.enter()
        .insert("g")
        .attr("class", function (d) { return (d.hasOwnProperty("class") ? "_"+d.type + " _" + d.class : "_" + d.type); });

      linkGroup.append("path")
          .attr("class", "link");

      linkGroup.filter(function (d) { return d.ends; }).append("circle")
                .attr("r", 5)
                .attr("fill", "#298EFE")
                .attr("class", "circleSource");

      linkGroup.filter(function (d) { return d.ends; }).append("circle")
                .attr("r", 5)
                .attr("fill", "#298EFE")
                .attr("class", "circleTarget");

      linkGroup.append("text")
                .attr("text-anchor", "left")
                .text(function (d) { return "Link Text"; })
                .attr("class", "linkText");


      linkInfo.exit().remove();

      const links = linkGroup.selectAll("path");

      let nodeSet = vis.select("g.nodeSet").selectAll("g.node")
          .data(this.state.nodes, function (k, i) { return (k.index); });

      nodeSet.exit().each(function (d) {
        console.log("DETECTED AN EXIT : " + JSON.stringify(d));
      });
      const newNodes = nodeSet.enter().append("g")
          .attr("class", "node");

      let names = ShapeFactory.getShapeNames();
      for ( let nm in names) {
          let shap = ShapeFactory.getShape(names[nm]);
          shap.build(newNodes.filter(function(d) { return d.type == names[nm]; }).classed("_" + names[nm], true));

          if (typeof shap.update === 'function') {
              shap.update(nodeSet.filter(function(d) { return d.type == names[nm]; }).classed("_" + names[nm], true));
          }
      }

      newNodes
          .attr("cx", function(d) { const dim = self.getDim(d); const x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); d.x = x; d.fx =(d.fixed ? x:null); })
          .attr("cy", function(d) { const dim = self.getDim(d); const y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); d.y = y; d.fy =(d.fixed ? y:null); });

      // Add children (attach tab navigation)
      // Calculate the grouping metadata for the layout
      // Organize the tab hierarchy by adjusting the x,y,width coordinates

      // Get the navigation to work with the children


      this.traverseChildren(newNodes);



      this.zones.forEach(function (zone, index) {
        let layout = LayoutFactory.getLayout(zone.type);
        let filtered = newNodes.filter(function (d) { const i = d.boundary.charCodeAt(0) - 65; return index == i; });
        layout.configEvents(vis, filtered, self.zones, self);
      });

      // Organize:
      this.zones.forEach(function (zone, index) {
        let layout = LayoutFactory.getLayout(zone.type);
        layout.organize(vis, index, self.zones);
      });



      nodeSet.exit().remove();

      nodeSet = vis.select("g.nodeSet").selectAll("g.nodec, g.node")

      let ticked = function() {

          const radius = 20;
          const w = vis.attr("width");
          const h = vis.attr("height");

          // enforce zone boundaries
          if (self.getConfig('enable_zone_boundaries')) {
            nodeSet
                .attr("cx", function(d) { const dim = self.getDim(d); d.x = Math.max(dim.x1 + radius, Math.min(dim.x2 - radius, d.x)); return d.x;})
                .attr("cy", function(d) { const dim = self.getDim(d); d.y = Math.max(dim.y1 + radius, Math.min(dim.y2 - radius, d.y)); return d.y;});
          }

          if (self.getConfig('enable_collision_avoidance')) {
            nodeSet.each(self.collide(0.5));
          }

          nodeSet
              .attr("transform", function(d) {
                  return "translate(" + d.x + "," + d.y + ")";
              });

          let ln = d3.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .curve(d3.curveLinear); // step, linear

          let stepBefore = d3.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .curve(d3.curveStepBefore); // step, step-after, step

          let stepAfter = d3.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .curve(d3.curveStepAfter); // step, step-after, step

          let cardinal = d3.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .curve(d3.curveCardinal); // step, step-after, step


          function layout (d) {
              let lay = "south";
              let p1 = d.getPointAtLength(0);
              let p2 = d.getPointAtLength(d.getTotalLength() / 2);
              let angle = Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
              //console.log("Answer = " + angle);
          }

          links
            .attr("d", function (d) {
              let sourcePoints = {"x":d.source.x, "y":d.source.y};
              let targetPoints = {"x":d.target.x, "y":d.target.y};
              let a = 0;

              // Source
              if (d.hasOwnProperty('ends')) {
                  let deltaX = 0;
                  let deltaY = 0;

                  // Find the source node and based on its bounding box, set the position

                  let s1 = nodeSet.filter(function (inf, ind) { return d.source.index == ind; });
                  let t1 = nodeSet.filter(function (inf, ind) { return d.target.index == ind; });

                  let sourceEnd = d.ends.substring(0,1);
                  let targetEnd = d.ends.substring(2,3);

                  if (d.ends == "auto" && (d.type == "straight" || d.type == "step-before")) {
                    if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                        sourceEnd = "S";
                        targetEnd = "W";
                    } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                        sourceEnd = "S";
                        targetEnd = "E";
                    } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                        sourceEnd = "N";
                        targetEnd = "E";
                    } else {
                        sourceEnd = "N";
                        targetEnd = "W";
                    }
                  }

                  if (d.ends == "auto" && d.type == "step-after") {
                    if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                        sourceEnd = "E";
                        targetEnd = "N";
                    } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                        sourceEnd = "W";
                        targetEnd = "N";
                    } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                        sourceEnd = "W";
                        targetEnd = "S";
                    } else {
                        sourceEnd = "E";
                        targetEnd = "S";
                    }
                  }

                  s1.each(function () {
                      if (sourceEnd == "W") { deltaX = -this.getBBox().width/2; }
                      if (sourceEnd == "E") { deltaX = this.getBBox().width/2; }
                      if (sourceEnd == "N") { deltaY = -this.getBBox().height/2; }
                      if (sourceEnd == "S") { deltaY = this.getBBox().height/2; }
                  });

                  sourcePoints = { "x":d.source.x + deltaX, "y":d.source.y + deltaY };

                  deltaX = 0;
                  deltaY = 0;
                  t1.each(function () {
                      if (targetEnd == "W") { deltaX = -this.getBBox().width/2; }
                      if (targetEnd == "E") { deltaX = this.getBBox().width/2; }
                      if (targetEnd == "N") { deltaY = -this.getBBox().height/2; }
                      if (targetEnd == "S") { deltaY = this.getBBox().height/2; }
                  });

                  targetPoints = { "x":d.target.x + deltaX, "y":d.target.y + deltaY };

              }


              const lineData = [sourcePoints, targetPoints];
              if (d.type == "straight" || d.type == "step-before") {
                return stepBefore(lineData);
              } else if (d.type == "step-after") {
                return stepAfter(lineData);
              } else if (d.type == "cardinal") {
                return cardinal(lineData);
              } else {
                return ln(lineData);
              }
          });

          // display text
          links.each (function (d) {
                //console.log("Total length = " + this.getTotalLength());
                let labelPosition = 50;
                if (d.hasOwnProperty("labelPosition")) {
                  labelPosition = d.labelPosition;
                }
                let point = this.getPointAtLength(this.getTotalLength() * (labelPosition/100));
                let label = d3.select(this.parentNode).select("text");
                label.text(function (ld) { return d.label; });
                label.attr("transform", "translate(" + (point.x - 5)+","+(point.y-5)+")");
                let circleSource = d3.select(this.parentNode).select("circle.circleSource");
                let sourcePoint = this.getPointAtLength(0);
                circleSource.attr("transform", "translate(" + sourcePoint.x+","+sourcePoint.y+")");
                let circleTarget = d3.select(this.parentNode).select("circle.circleTarget");
                let targetPoint = this.getPointAtLength(this.getTotalLength()-0);
                circleTarget.attr("transform", "translate(" + targetPoint.x+","+targetPoint.y+")");
                //layout (this);
          });

      };

      // Restart the force layout
      let nodeList = [];
      for (let a in this.state.nodes) { nodeList.push(this.state.nodes[a]); };

      for ( let i = 0 ; i < this.state.nodes.length; i++) {
          if (this.state.nodes[i].children) {
              for ( let j = 0 ; j < this.state.nodes[i].children.length; j++) {
                 nodeList.push(this.state.nodes[i].children[j]);
              }

          }
      }


      force
        .nodes(nodeList)
        .on("tick", ticked);

      force
        .force("link").links(this.state.links);
  }


  destroy() {
      this.force.on("tick", null);
      d3.select(this.domNode).remove();
      d3.select(this.contentNode).remove();
  }

  // Resolves collisions between d and all other nodes.
  collide(alpha) {


    const padding = 1.5,
          maxRadius = 12;
    const quadtree = d3.quadtree(this.state.nodes);

    return function(d) {
      d.radius = 25;
      let r = d.radius + maxRadius + padding,
          nx1 = d.x - r,
          nx2 = d.x + r,
          ny1 = d.y - r,
          ny2 = d.y + r;

      if (d.x == null) {
          //console.log("Ignoring.. " + d.name);
          return;
      }
      quadtree.visit(function(quad, x1, y1, x2, y2) {
        //console.log("Match: " + x1+","+y1+","+x2+","+y2 + " : " + quad.point);
        //if (quad.leaf == false) {
        //    return true;
        //}
        // quad.point has the object details
        if (quad.point && (quad.point !== d)) {

          if (quad.point.boundary != d.boundary) {
              //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
          }
          //if (d.x == null) {
          //  return true;
          //}
          //console.log("D = "+JSON.stringify(d, null, 2));
          //console.log("Quad = "+JSON.stringify(quad, null, 2));
          quad.point.radius = 25;
          let x = d.x - quad.point.x,
              y = d.y - quad.point.y,
              l = Math.abs(Math.sqrt(x * x + y * y)),
              r = d.radius + quad.point.radius + padding;
          //console.log("X="+x+",Y="+y);
          //console.log("L="+l+",R="+r);
          if (l < r) {
            l = (l - r) / l * alpha;

            // Need to make sure we are not moving it outside of boundary
            x = x * l;
            y = y * l;
            if (isNaN(x) || isNaN(y)) {
              //console.log("ILLEGAL VALUE!");
              x = 0.00;
              y = 0.01;
              //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
            }
            //console.log("Changing..." + x+", "+y+", dx ="+d.x+", "+d.y);
            d.x = d.x - x;
            d.y = d.y - y;
            //console.log("ANSWER: d="+d.x+", "+d.y);
            quad.point.x += x;
            quad.point.y += y;
            //console.log("Change to: " +d.x+", "+d.y+" :: " + quad.point.x+", "+quad.point.y);
          }
        }
        //console.log("Answer: " +x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1);
        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
      });
    };
  }

  addNode(name, fixed, type, boundary) {
      const nd = {"name":name, "fixed":(fixed ? true:false), "type":type, "boundary":boundary };
      this.state.nodes.push(nd);
      this.update();
  }

  removeNode(name) {

      this.state.nodes = this.state.nodes.filter(function(node) {return (node["name"] != name); });
      this.state.links = this.state.links.filter(function(link) {return ((link["source"]["name"] != name)&&(link["target"]["name"] != name)); });
      this.update();
  }

  findNode(name) {
      for (let i in this.state.nodes) if (this.state.nodes[i]["name"] === name) return this.state.nodes[i];
  }

  registerLinks () {
      const links = this.state.links;
      for (let i = 0 ; i < links.length; i++) {
         const link = links[i];
         if ((typeof link.source) == "string") {
             link.source = this.findNode(link.source);
         }
         if ((typeof link.target) == "string") {
             link.target = this.findNode(link.target);
         }
      }
  }

  render() {
    if (this.state) {
      this.update();
    }
    //var node = document.createElement("svg");
    //this.domNode.appendChild(node);
  }
}
