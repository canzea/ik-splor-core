

# Getting started


    npm run watch

Then go to:

    http://localhost:20000/

Useful link:

    https://code.lengstorf.com/learn-rollup-js/

# Publishing

    npm run publish

# Examples

http://codepen.io/ikethecoder/pen/ggwxOO
