import React from 'react';
import ReactDOM from 'react-dom';

import App from './app.js';
import './styles/custom.scss';


ReactDOM.render(
    <App />, document.getElementById('viewer')
);