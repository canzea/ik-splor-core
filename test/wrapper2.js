import React from 'react';

import {ViewerCanvas} from '../src/index';

class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

      const self = this;
      d3.json('start.json', function(json) {
            // adjust the position of the "page" to below the navigation line

            let nodes = json.nodes;
            let zones = json.zones;
            let links = json.links;
            let config = json.config;

            let vc = new ViewerCanvas();
            vc.mountNode (self.refs.root);

            vc.setState({"config":config, "zones":zones, "graph":{"nodes":nodes,"links":[]}, "flow":true, "links":links});
            vc.componentDidMount();
            vc.refreshZones (zones, {});
            vc.render();

            vc.refreshSize();

      });


      // navigation menus
      // fix the eval() part - perhaps use moustache
      // update the shape to do the filter all the time automatically

//
//      window.onresize = function(event) {
//        vc.refreshSize();
//      };
  }

  render() {
    return (
        <div id="base" ref="root">
        </div>);
  }
}

export default TheWrapper;