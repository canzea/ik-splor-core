import React from 'react';
import ReactDOM from 'react-dom';

import * as d3 from 'd3';
import Registry from "../src/Registry";
import {ViewerCanvas} from "../src/ViewerCanvas";

class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
      function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      this.renderModals();

      let source = getParameterByName('p');
      let self = this;

      let vc = new ViewerCanvas();
      vc.mountNode (self.refs.root);

      vc.on("tabClick", function (e, data, doc) {
          console.log("TAB CLICKED: " + JSON.stringify(data));
      });

      vc.on("shapeClick", function (e, data, doc) {
          console.log("SHAPE CLICK: " + JSON.stringify(doc));
          //data.selected = !data.selected;
          //doc.nodes[0].label = doc.nodes[0].label + 'x';
          //doc.nodes[0].values[0].selected = !data.selected;
          //doc.nodes[0].index = null;
          //doc.nodes.splice(i, 1);
          //let n = doc.nodes.pop();
          //vc.update();

          data.selected = !data.selected;
          //doc.nodes.push(n);

          vc.update();

          //console.log("SHAPE CLICKED: " + data.content.url);
          //window.location.href = data.content.url;
      });

      vc.on("shapeNavClick", function (e, data, doc) {
          console.log("SHAPE NAV CLICKED: " + JSON.stringify(data));
          $("#myModal").modal();
          vc.trigger("closeNavigation");
      });

      vc.on("change", function (e, data, doc) {
          if (typeof(Storage) !== "undefined") {
             localStorage.setItem(source, JSON.stringify(doc));
          }
      })

      window.onresize = function(event) {
          vc.refreshSize();
      };

      if (localStorage.getItem(source) != null && getParameterByName('f') != "true") {
          vc.setState(JSON.parse(localStorage.getItem(source)));
          vc.render();
          vc.refreshSize();
      } else if (source == null) {
          vc.setState({"nodes":[], "links":[], "zones":[]});
          vc.render();
          vc.refreshSize();
          alert("No source passed in as query 'p'");
      } else {
          d3.json(source, function(json) {
              vc.setState(json);
              vc.render();

              //vc.trigger("tabClick", json.nodes[0].children[0]);
              vc.refreshSize();
          });
      }
  }

  render() {
    return (
        <div id="base" ref="root">
        </div>);
  }

  renderModals() {
    ReactDOM.render(

      <div>
         <div id="myModal" className="modal fade" role="dialog">
           <div className="modal-dialog">
             <div className="modal-content">
               <div className="modal-header">
                 <button type="button" className="close" data-dismiss="modal">&times;</button>
                 <h4 className="modal-title">Modal Header</h4>
               </div>
               <div className="modal-body">
                 <p>Some text in the modal.</p>
               </div>
               <div className="modal-footer">
                 <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
           </div>
         </div>
      </div>

      ,document.getElementById('modals')
    );
  }
}

export default TheWrapper;