import React from 'react';
import ReactDOM from 'react-dom';

import {Hello} from './hello';

import Wrapper from './wrapper';

const App = (props) => {
      let hel = new Hello();
  return (
    <div>
        <Wrapper a="b"/>
        {props.children}
    </div>
  );
};

App.propTypes = {
  children: React.PropTypes.element
};

export default App;