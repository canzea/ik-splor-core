(function (exports,d3) {
'use strict';

function __$styleInject(css, returnValue) {
  if (typeof document === 'undefined') {
    return returnValue;
  }
  css = css || '';
  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet){
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
  head.appendChild(style);
  return returnValue;
}

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

//import BuildingBlockShape from './shape-catalog/BuildingBlock';

var _LayoutFactory = function () {
    function _LayoutFactory() {
        classCallCheck(this, _LayoutFactory);

        this.layouts = {};
    }

    createClass(_LayoutFactory, [{
        key: "getLayoutNames",
        value: function getLayoutNames() {
            return Object.keys(this.layouts);
        }
    }, {
        key: "getLayout",
        value: function getLayout(layout) {
            if (!this.layouts.hasOwnProperty(layout)) {
                return null;
            }
            return this.layouts[layout];
        }
    }, {
        key: "clone",
        value: function clone(layoutFrom, layoutTo) {
            var object = Object.create(this.layouts[layoutFrom]);
            object.domainType = layoutTo;
            this.layouts[object.domainType] = object;
            return this;
        }
    }, {
        key: "register",
        value: function register(object) {
            this.layouts[object.domainType] = object;
            return this;
        }
    }]);
    return _LayoutFactory;
}();

var LayoutFactory = new _LayoutFactory();

//import BuildingBlockShape from './shape-catalog/BuildingBlock';

var _ShapeFactory = function () {
    function _ShapeFactory() {
        classCallCheck(this, _ShapeFactory);

        this.shapes = {};
    }

    createClass(_ShapeFactory, [{
        key: "getShapeNames",
        value: function getShapeNames() {
            return Object.keys(this.shapes);
        }
    }, {
        key: "getShape",
        value: function getShape(shape) {
            if (!this.shapes.hasOwnProperty(shape)) {
                console.log("WARN: Shape not found " + shape);
                return null;
            }
            return this.shapes[shape];
        }
    }, {
        key: "clone",
        value: function clone(shapeFrom, shapeTo) {
            var object = Object.create(this.shapes[shapeFrom]);
            object.domainType = shapeTo;
            this.shapes[object.domainType] = object;
            return this;
        }
    }, {
        key: "register",
        value: function register(object) {
            this.shapes[object.domainType] = object;
            return this;
        }
    }]);
    return _ShapeFactory;
}();

var ShapeFactory = new _ShapeFactory();

var Navigation = function () {
  function Navigation() {
    classCallCheck(this, Navigation);
  }

  createClass(Navigation, [{
    key: "attach",
    value: function attach(svg, viewer, contentNode) {
      var self = this;
      this.svg = svg;
      this.contentNode = contentNode;
      this.viewer = viewer;

      svg.append("g").attr("class", "nav");
      this.state = { "mode": "closed", "selected": -1 };

      this.menuItems = [];

      viewer.on("closeNavigation", function (e) {
        self.close();
      });
    }
  }, {
    key: "addMenuItem",
    value: function addMenuItem(item) {
      this.menuItems.push(item);
    }
  }, {
    key: "update",
    value: function update() {
      var self = this;
      var vis = this.svg;

      this.root = vis.select("g.nav").append("g");

      this.backdrop = vis.select("g.nav").select("g").append("rect").attr("fill", "#999999").attr("x", 0).attr("y", 0).attr("rx", 6).attr("ry", 6).attr("width", "280").attr("height", "80").style("visibility", "hidden");

      var menuNodes = vis.select("g.nav").select("g").selectAll("g.navDetail").data(this.menuItems);

      var navNodes = menuNodes.enter().append("g").attr("class", "navDetail").style("visibility", "hidden");

      menuNodes.exit().remove();

      navNodes.append("rect").attr("fill", "white").attr("x", 2).attr("y", 4).attr("rx", 6).attr("ry", 6).attr("transform", "rotate(35)").attr("width", "55").attr("height", "12");

      navNodes.append("circle").style("cursor", "default").attr("fill", "black").attr("stroke", "white").attr("stroke-width", "2").attr("r", "10");

      navNodes.append("text").style("cursor", "default").attr("dx", 0).attr("dy", "0").attr("text-anchor", "middle").attr('dominant-baseline', 'central').style('font-family', 'FontAwesome').style('font-size', '10px').style("fill", "white").text(function (d) {
        return d.icon;
      });

      navNodes.append("text").style("cursor", "default").attr("dx", 8).attr("dy", 12).attr("fill", "black").attr("text-anchor", "left").style('font-size', '0.4em').attr("transform", "rotate(35)").text(function (d) {
        return d.title;
      });

      this.backdrop.on('click', function (e) {
        self.close();
      });

      navNodes.on('click', function (e) {
        self.viewer.trigger("shapeNavClick", { "data": self.state.data, "menuItem": e });
        //self.close();
      });

      navNodes.selectAll('circle,text,rect').on('mouseenter', function (nodeSet, index) {
        d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", true);
      }).on('mouseleave', function (e) {
        d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", false);
        //            vis.select(this).selectAll('circle').removeClass("nav-hover");
      });
    }
  }, {
    key: "toggle",
    value: function toggle(e, matchedItem) {
      var self = this;
      var pos = null; //{x:e.x,y:e.y};
      var state = self.state;

      if (typeof state.newNode != "undefined" && state.newNode != null) {
        //console.log("REMOVING: " + state.newNode);
        state.newNode.parentNode.removeChild(state.newNode);
        state.newNode = null;
      }

      var notes = [];
      if (matchedItem) {
        matchedItem.each(function (d) {
          var bounded = this.getBBox();
          self.state.newNode = this.cloneNode(true);

          self.svg.select("g.nav").node().appendChild(self.state.newNode);
          d3.select(self.state.newNode).on('click', function (e) {
            self.close();
          });

          pos = { left: e.x + bounded.x, top: e.y + bounded.y, width: bounded.width, height: bounded.height, bounding: this.getBoundingClientRect() };

          notes = d.hasOwnProperty('notes') ? d.notes : [];
          self.state.data = d;
        });
      }
      if (this.state.mode == "closed") {
        this.doOpen(e, pos, notes);
      } else {
        if (this.state.selected != e.index) {
          this.doOpen(e, pos, notes);
        } else {
          this.close(e);
        }
      }
    }
  }, {
    key: "open",
    value: function open(e) {
      this.doOpen(e, { x: e.x, y: e.y });
    }
  }, {
    key: "doOpen",
    value: function doOpen(e, position, notes) {
      var self = this;
      this.state.x = position.left;
      this.state.y = position.top;
      this.state.selected = e.index;
      this.state.mode = "open";

      var scale = 1.6;
      var delta = 0;

      var start = 198;
      // calculate positions of menu items
      var positions = [0];
      for (var i = 0; i < 10; i++) {
        if (start + 36 * i > 360) {
          positions.push(start + 36 * i - 360);
        } else {
          positions.push(start + 36 * i);
        }
      }

      var vis = this.svg;

      // Position the navigation in the right place
      vis.select("g.nav").selectAll('g.node')
      //.attr("transform", "translate(0,0)")
      .style("pointer-events", "none");

      vis.select("g.nav").select("g").attr("transform", function (d) {
        return "translate(" + (position.left + position.width / 2) + "," + (position.top + position.height / 2) + ")";
      });
      vis.select("g.nav").select('rect').attr("opacity", 0).style("visibility", "visible").attr("width", function (d) {
        return Math.max(position.width, 150) + 10;
      }).attr("height", function (d) {
        return position.height + 10;
      }).attr("transform", function (d) {
        return "translate(" + (-position.width / 2 - 5) + "," + (-position.height / 2 - 5) + ")";
      });

      // Determine how big the height should be based on the "notes" that are provided
      // Move a navigation DIV to the same position for displaying
      var element = this.contentNode.parentNode;
      var bodyRect = document.body.getBoundingClientRect(),
          elemRect = element.getBoundingClientRect(),
          offset = elemRect.top - bodyRect.top;

      var svgBB = self.svg.node().getBoundingClientRect();
      this.printRect(svgBB, "svg BCR");

      var svgParentBB = self.svg.node().parentNode.getBoundingClientRect();
      this.printRect(svgParentBB, "SVG Parent BCR");

      var navBB = vis.select("g.nav").select('rect').node().getBoundingClientRect();
      this.printRect(navBB, "NAV BCR");

      this.printRect(position, "obj");
      this.printRect(position.bounding, "obj BCR");
      this.printRect(elemRect, "nav BCR");
      this.printRect(bodyRect, "body BCR");

      console.log("scrollTop : " + document.body.scrollTop);
      var minHeight = Math.max(position.bounding.height + 25, self.menuItems == 0 ? 50 : 145);
      var minWidth = navBB.width;

      d3.select(this.contentNode).classed("content", true).style("display", "block").style("position", "absolute").style("width", "" + (minWidth - 10) + "px").style("left", "" + (navBB.left - svgParentBB.left + 5) + "px").style("top", "" + (navBB.top - svgParentBB.top + navBB.height) + "px");
      //.style("left", ""+ (position.bounding.left) + "px")
      //.style("top", "" + (-bodyRect.top + position.bounding.top + minHeight)  + "px");

      // + position.bounding.top
      d3.select(this.contentNode).selectAll("table").remove();

      var noteNode = d3.select(this.contentNode).append("table").selectAll("tr").data(notes);
      noteNode.enter().append("tr").html(function (d) {
        if (typeof d == "string") {
          return d;
        } else {
          return "<td class='key'>" + d.key + "</td><td class='value'>" + d.value + "</td>";
        }
      });

      var dims = this.contentNode.getBoundingClientRect();
      this.printRect(dims);

      var offset2 = dims.top - elemRect.top;

      //console.log("OFFSET = "+offset2+", "+dims.top);
      var contentHeight = dims.height;

      var ratio = position.height / position.bounding.height;

      // Adjust the height using the same ratio between boundingrect and BBBox
      vis.select("g.nav").select('rect').attr("height", function (d) {
        return 0 + (navBB.height + contentHeight * ratio);
      }).attr("width", function (d) {
        return Number(d3.select(this).attr('width')) + (self.menuItems.length == 0 ? 0 : 52) + self.menuItems.length * 38;
      }).transition().attr("opacity", 0.9).on("end", function () {
        return;
      });

      var menuPosition = 0;
      vis.select("g.nav").selectAll('g.navDetail').style("opacity", 0).style("visibility", "visible").attr("transform", function (d) {
        return "translate(" + 0 + "," + (20 + delta) + ") scale(" + scale + ")";
      }).transition().style("opacity", 100).attr("transform", function (d) {
        var y = -position.height / 2 + 20 + delta;
        var x = position.width / 2 + (22 + 1) * menuPosition++ * scale + 25;
        return "translate(" + x + "," + y + ") scale(" + scale + ")";
      });
    }
  }, {
    key: "close",
    value: function close() {
      var state = this.state;
      var vis = this.svg;

      if (state.mode == "closed") {
        return;
      }
      state.mode = "closed";

      d3.select(this.contentNode).style("display", "none");

      vis.select("g.nav").selectAll('g.navDetail').transition().style("opacity", 0).attr("transform", function () {
        return "translate(" + state.x + "," + (state.y + 40) + ")";
      }).on("end", function () {
        vis.select("g.nav").selectAll('g.navDetail').style("visibility", "hidden");
      });

      if (state.newNode) {
        //console.log("REMOVING: " + state.newNode);
        state.newNode.parentNode.removeChild(state.newNode);
        state.newNode = null;
        state.data = null;
      }

      vis.select("g.nav").select('rect').transition().attr("opacity", 0).on("end", function () {
        vis.select("g.nav").select('rect').style("visibility", "hidden");
      });
    }
  }, {
    key: "printRect",
    value: function printRect(elemRect) {
      var label = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "E";

      console.log(label + ": Top=" + elemRect.top + ",Left=" + elemRect.left + ",Width=" + elemRect.width + ",Height=" + elemRect.height);
    }
  }]);
  return Navigation;
}();

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var index = createCommonjsModule(function (module) {
var fa=function(i){return fa[i.replace(/-./g,function(x){return x.substr(1).toUpperCase()})]};fa.glass="\uf000";fa.music="\uf001";fa.search="\uf002";fa.envelopeO="\uf003";fa.heart="\uf004";fa.star="\uf005";fa.starO="\uf006";fa.user="\uf007";fa.film="\uf008";fa.thLarge="\uf009";fa.th="\uf00a";fa.thList="\uf00b";fa.check="\uf00c";fa.remove="\uf00d";fa.close="\uf00d";fa.times="\uf00d";fa.searchPlus="\uf00e";fa.searchMinus="\uf010";fa.powerOff="\uf011";fa.signal="\uf012";fa.gear="\uf013";fa.cog="\uf013";fa.trashO="\uf014";fa.home="\uf015";fa.fileO="\uf016";fa.clockO="\uf017";fa.road="\uf018";fa.download="\uf019";fa.arrowCircleODown="\uf01a";fa.arrowCircleOUp="\uf01b";fa.inbox="\uf01c";fa.playCircleO="\uf01d";fa.rotateRight="\uf01e";fa.repeat="\uf01e";fa.refresh="\uf021";fa.listAlt="\uf022";fa.lock="\uf023";fa.flag="\uf024";fa.headphones="\uf025";fa.volumeOff="\uf026";fa.volumeDown="\uf027";fa.volumeUp="\uf028";fa.qrcode="\uf029";fa.barcode="\uf02a";fa.tag="\uf02b";fa.tags="\uf02c";fa.book="\uf02d";fa.bookmark="\uf02e";fa.print="\uf02f";fa.camera="\uf030";fa.font="\uf031";fa.bold="\uf032";fa.italic="\uf033";fa.textHeight="\uf034";fa.textWidth="\uf035";fa.alignLeft="\uf036";fa.alignCenter="\uf037";fa.alignRight="\uf038";fa.alignJustify="\uf039";fa.list="\uf03a";fa.dedent="\uf03b";fa.outdent="\uf03b";fa.indent="\uf03c";fa.videoCamera="\uf03d";fa.photo="\uf03e";fa.image="\uf03e";fa.pictureO="\uf03e";fa.pencil="\uf040";fa.mapMarker="\uf041";fa.adjust="\uf042";fa.tint="\uf043";fa.edit="\uf044";fa.pencilSquareO="\uf044";fa.shareSquareO="\uf045";fa.checkSquareO="\uf046";fa.arrows="\uf047";fa.stepBackward="\uf048";fa.fastBackward="\uf049";fa.backward="\uf04a";fa.play="\uf04b";fa.pause="\uf04c";fa.stop="\uf04d";fa.forward="\uf04e";fa.fastForward="\uf050";fa.stepForward="\uf051";fa.eject="\uf052";fa.chevronLeft="\uf053";fa.chevronRight="\uf054";fa.plusCircle="\uf055";fa.minusCircle="\uf056";fa.timesCircle="\uf057";fa.checkCircle="\uf058";fa.questionCircle="\uf059";fa.infoCircle="\uf05a";fa.crosshairs="\uf05b";fa.timesCircleO="\uf05c";fa.checkCircleO="\uf05d";fa.ban="\uf05e";fa.arrowLeft="\uf060";fa.arrowRight="\uf061";fa.arrowUp="\uf062";fa.arrowDown="\uf063";fa.mailForward="\uf064";fa.share="\uf064";fa.expand="\uf065";fa.compress="\uf066";fa.plus="\uf067";fa.minus="\uf068";fa.asterisk="\uf069";fa.exclamationCircle="\uf06a";fa.gift="\uf06b";fa.leaf="\uf06c";fa.fire="\uf06d";fa.eye="\uf06e";fa.eyeSlash="\uf070";fa.warning="\uf071";fa.exclamationTriangle="\uf071";fa.plane="\uf072";fa.calendar="\uf073";fa.random="\uf074";fa.comment="\uf075";fa.magnet="\uf076";fa.chevronUp="\uf077";fa.chevronDown="\uf078";fa.retweet="\uf079";fa.shoppingCart="\uf07a";fa.folder="\uf07b";fa.folderOpen="\uf07c";fa.arrowsV="\uf07d";fa.arrowsH="\uf07e";fa.barChartO="\uf080";fa.barChart="\uf080";fa.twitterSquare="\uf081";fa.facebookSquare="\uf082";fa.cameraRetro="\uf083";fa.key="\uf084";fa.gears="\uf085";fa.cogs="\uf085";fa.comments="\uf086";fa.thumbsOUp="\uf087";fa.thumbsODown="\uf088";fa.starHalf="\uf089";fa.heartO="\uf08a";fa.signOut="\uf08b";fa.linkedinSquare="\uf08c";fa.thumbTack="\uf08d";fa.externalLink="\uf08e";fa.signIn="\uf090";fa.trophy="\uf091";fa.githubSquare="\uf092";fa.upload="\uf093";fa.lemonO="\uf094";fa.phone="\uf095";fa.squareO="\uf096";fa.bookmarkO="\uf097";fa.phoneSquare="\uf098";fa.twitter="\uf099";fa.facebookF="\uf09a";fa.facebook="\uf09a";fa.github="\uf09b";fa.unlock="\uf09c";fa.creditCard="\uf09d";fa.feed="\uf09e";fa.rss="\uf09e";fa.hddO="\uf0a0";fa.bullhorn="\uf0a1";fa.bell="\uf0f3";fa.certificate="\uf0a3";fa.handORight="\uf0a4";fa.handOLeft="\uf0a5";fa.handOUp="\uf0a6";fa.handODown="\uf0a7";fa.arrowCircleLeft="\uf0a8";fa.arrowCircleRight="\uf0a9";fa.arrowCircleUp="\uf0aa";fa.arrowCircleDown="\uf0ab";fa.globe="\uf0ac";fa.wrench="\uf0ad";fa.tasks="\uf0ae";fa.filter="\uf0b0";fa.briefcase="\uf0b1";fa.arrowsAlt="\uf0b2";fa.group="\uf0c0";fa.users="\uf0c0";fa.chain="\uf0c1";fa.link="\uf0c1";fa.cloud="\uf0c2";fa.flask="\uf0c3";fa.cut="\uf0c4";fa.scissors="\uf0c4";fa.copy="\uf0c5";fa.filesO="\uf0c5";fa.paperclip="\uf0c6";fa.save="\uf0c7";fa.floppyO="\uf0c7";fa.square="\uf0c8";fa.navicon="\uf0c9";fa.reorder="\uf0c9";fa.bars="\uf0c9";fa.listUl="\uf0ca";fa.listOl="\uf0cb";fa.strikethrough="\uf0cc";fa.underline="\uf0cd";fa.table="\uf0ce";fa.magic="\uf0d0";fa.truck="\uf0d1";fa.pinterest="\uf0d2";fa.pinterestSquare="\uf0d3";fa.googlePlusSquare="\uf0d4";fa.googlePlus="\uf0d5";fa.money="\uf0d6";fa.caretDown="\uf0d7";fa.caretUp="\uf0d8";fa.caretLeft="\uf0d9";fa.caretRight="\uf0da";fa.columns="\uf0db";fa.unsorted="\uf0dc";fa.sort="\uf0dc";fa.sortDown="\uf0dd";fa.sortDesc="\uf0dd";fa.sortUp="\uf0de";fa.sortAsc="\uf0de";fa.envelope="\uf0e0";fa.linkedin="\uf0e1";fa.rotateLeft="\uf0e2";fa.undo="\uf0e2";fa.legal="\uf0e3";fa.gavel="\uf0e3";fa.dashboard="\uf0e4";fa.tachometer="\uf0e4";fa.commentO="\uf0e5";fa.commentsO="\uf0e6";fa.flash="\uf0e7";fa.bolt="\uf0e7";fa.sitemap="\uf0e8";fa.umbrella="\uf0e9";fa.paste="\uf0ea";fa.clipboard="\uf0ea";fa.lightbulbO="\uf0eb";fa.exchange="\uf0ec";fa.cloudDownload="\uf0ed";fa.cloudUpload="\uf0ee";fa.userMd="\uf0f0";fa.stethoscope="\uf0f1";fa.suitcase="\uf0f2";fa.bellO="\uf0a2";fa.coffee="\uf0f4";fa.cutlery="\uf0f5";fa.fileTextO="\uf0f6";fa.buildingO="\uf0f7";fa.hospitalO="\uf0f8";fa.ambulance="\uf0f9";fa.medkit="\uf0fa";fa.fighterJet="\uf0fb";fa.beer="\uf0fc";fa.hSquare="\uf0fd";fa.plusSquare="\uf0fe";fa.angleDoubleLeft="\uf100";fa.angleDoubleRight="\uf101";fa.angleDoubleUp="\uf102";fa.angleDoubleDown="\uf103";fa.angleLeft="\uf104";fa.angleRight="\uf105";fa.angleUp="\uf106";fa.angleDown="\uf107";fa.desktop="\uf108";fa.laptop="\uf109";fa.tablet="\uf10a";fa.mobilePhone="\uf10b";fa.mobile="\uf10b";fa.circleO="\uf10c";fa.quoteLeft="\uf10d";fa.quoteRight="\uf10e";fa.spinner="\uf110";fa.circle="\uf111";fa.mailReply="\uf112";fa.reply="\uf112";fa.githubAlt="\uf113";fa.folderO="\uf114";fa.folderOpenO="\uf115";fa.smileO="\uf118";fa.frownO="\uf119";fa.mehO="\uf11a";fa.gamepad="\uf11b";fa.keyboardO="\uf11c";fa.flagO="\uf11d";fa.flagCheckered="\uf11e";fa.terminal="\uf120";fa.code="\uf121";fa.mailReplyAll="\uf122";fa.replyAll="\uf122";fa.starHalfEmpty="\uf123";fa.starHalfFull="\uf123";fa.starHalfO="\uf123";fa.locationArrow="\uf124";fa.crop="\uf125";fa.codeFork="\uf126";fa.unlink="\uf127";fa.chainBroken="\uf127";fa.question="\uf128";fa.info="\uf129";fa.exclamation="\uf12a";fa.superscript="\uf12b";fa.subscript="\uf12c";fa.eraser="\uf12d";fa.puzzlePiece="\uf12e";fa.microphone="\uf130";fa.microphoneSlash="\uf131";fa.shield="\uf132";fa.calendarO="\uf133";fa.fireExtinguisher="\uf134";fa.rocket="\uf135";fa.maxcdn="\uf136";fa.chevronCircleLeft="\uf137";fa.chevronCircleRight="\uf138";fa.chevronCircleUp="\uf139";fa.chevronCircleDown="\uf13a";fa.anchor="\uf13d";fa.unlockAlt="\uf13e";fa.bullseye="\uf140";fa.ellipsisH="\uf141";fa.ellipsisV="\uf142";fa.rssSquare="\uf143";fa.playCircle="\uf144";fa.ticket="\uf145";fa.minusSquare="\uf146";fa.minusSquareO="\uf147";fa.levelUp="\uf148";fa.levelDown="\uf149";fa.checkSquare="\uf14a";fa.pencilSquare="\uf14b";fa.externalLinkSquare="\uf14c";fa.shareSquare="\uf14d";fa.compass="\uf14e";fa.toggleDown="\uf150";fa.caretSquareODown="\uf150";fa.toggleUp="\uf151";fa.caretSquareOUp="\uf151";fa.toggleRight="\uf152";fa.caretSquareORight="\uf152";fa.euro="\uf153";fa.eur="\uf153";fa.gbp="\uf154";fa.dollar="\uf155";fa.usd="\uf155";fa.rupee="\uf156";fa.inr="\uf156";fa.cny="\uf157";fa.rmb="\uf157";fa.yen="\uf157";fa.jpy="\uf157";fa.ruble="\uf158";fa.rouble="\uf158";fa.rub="\uf158";fa.won="\uf159";fa.krw="\uf159";fa.bitcoin="\uf15a";fa.btc="\uf15a";fa.file="\uf15b";fa.fileText="\uf15c";fa.sortAlphaAsc="\uf15d";fa.sortAlphaDesc="\uf15e";fa.sortAmountAsc="\uf160";fa.sortAmountDesc="\uf161";fa.sortNumericAsc="\uf162";fa.sortNumericDesc="\uf163";fa.thumbsUp="\uf164";fa.thumbsDown="\uf165";fa.youtubeSquare="\uf166";fa.youtube="\uf167";fa.xing="\uf168";fa.xingSquare="\uf169";fa.youtubePlay="\uf16a";fa.dropbox="\uf16b";fa.stackOverflow="\uf16c";fa.instagram="\uf16d";fa.flickr="\uf16e";fa.adn="\uf170";fa.bitbucket="\uf171";fa.bitbucketSquare="\uf172";fa.tumblr="\uf173";fa.tumblrSquare="\uf174";fa.longArrowDown="\uf175";fa.longArrowUp="\uf176";fa.longArrowLeft="\uf177";fa.longArrowRight="\uf178";fa.apple="\uf179";fa.windows="\uf17a";fa.android="\uf17b";fa.linux="\uf17c";fa.dribbble="\uf17d";fa.skype="\uf17e";fa.foursquare="\uf180";fa.trello="\uf181";fa.female="\uf182";fa.male="\uf183";fa.gittip="\uf184";fa.gratipay="\uf184";fa.sunO="\uf185";fa.moonO="\uf186";fa.archive="\uf187";fa.bug="\uf188";fa.vk="\uf189";fa.weibo="\uf18a";fa.renren="\uf18b";fa.pagelines="\uf18c";fa.stackExchange="\uf18d";fa.arrowCircleORight="\uf18e";fa.arrowCircleOLeft="\uf190";fa.toggleLeft="\uf191";fa.caretSquareOLeft="\uf191";fa.dotCircleO="\uf192";fa.wheelchair="\uf193";fa.vimeoSquare="\uf194";fa.turkishLira="\uf195";fa.try="\uf195";fa.plusSquareO="\uf196";fa.spaceShuttle="\uf197";fa.slack="\uf198";fa.envelopeSquare="\uf199";fa.wordpress="\uf19a";fa.openid="\uf19b";fa.institution="\uf19c";fa.bank="\uf19c";fa.university="\uf19c";fa.mortarBoard="\uf19d";fa.graduationCap="\uf19d";fa.yahoo="\uf19e";fa.google="\uf1a0";fa.reddit="\uf1a1";fa.redditSquare="\uf1a2";fa.stumbleuponCircle="\uf1a3";fa.stumbleupon="\uf1a4";fa.delicious="\uf1a5";fa.digg="\uf1a6";fa.piedPiperPp="\uf1a7";fa.piedPiperAlt="\uf1a8";fa.drupal="\uf1a9";fa.joomla="\uf1aa";fa.language="\uf1ab";fa.fax="\uf1ac";fa.building="\uf1ad";fa.child="\uf1ae";fa.paw="\uf1b0";fa.spoon="\uf1b1";fa.cube="\uf1b2";fa.cubes="\uf1b3";fa.behance="\uf1b4";fa.behanceSquare="\uf1b5";fa.steam="\uf1b6";fa.steamSquare="\uf1b7";fa.recycle="\uf1b8";fa.automobile="\uf1b9";fa.car="\uf1b9";fa.cab="\uf1ba";fa.taxi="\uf1ba";fa.tree="\uf1bb";fa.spotify="\uf1bc";fa.deviantart="\uf1bd";fa.soundcloud="\uf1be";fa.database="\uf1c0";fa.filePdfO="\uf1c1";fa.fileWordO="\uf1c2";fa.fileExcelO="\uf1c3";fa.filePowerpointO="\uf1c4";fa.filePhotoO="\uf1c5";fa.filePictureO="\uf1c5";fa.fileImageO="\uf1c5";fa.fileZipO="\uf1c6";fa.fileArchiveO="\uf1c6";fa.fileSoundO="\uf1c7";fa.fileAudioO="\uf1c7";fa.fileMovieO="\uf1c8";fa.fileVideoO="\uf1c8";fa.fileCodeO="\uf1c9";fa.vine="\uf1ca";fa.codepen="\uf1cb";fa.jsfiddle="\uf1cc";fa.lifeBouy="\uf1cd";fa.lifeBuoy="\uf1cd";fa.lifeSaver="\uf1cd";fa.support="\uf1cd";fa.lifeRing="\uf1cd";fa.circleONotch="\uf1ce";fa.ra="\uf1d0";fa.resistance="\uf1d0";fa.rebel="\uf1d0";fa.ge="\uf1d1";fa.empire="\uf1d1";fa.gitSquare="\uf1d2";fa.git="\uf1d3";fa.yCombinatorSquare="\uf1d4";fa.ycSquare="\uf1d4";fa.hackerNews="\uf1d4";fa.tencentWeibo="\uf1d5";fa.qq="\uf1d6";fa.wechat="\uf1d7";fa.weixin="\uf1d7";fa.send="\uf1d8";fa.paperPlane="\uf1d8";fa.sendO="\uf1d9";fa.paperPlaneO="\uf1d9";fa.history="\uf1da";fa.circleThin="\uf1db";fa.header="\uf1dc";fa.paragraph="\uf1dd";fa.sliders="\uf1de";fa.shareAlt="\uf1e0";fa.shareAltSquare="\uf1e1";fa.bomb="\uf1e2";fa.soccerBallO="\uf1e3";fa.futbolO="\uf1e3";fa.tty="\uf1e4";fa.binoculars="\uf1e5";fa.plug="\uf1e6";fa.slideshare="\uf1e7";fa.twitch="\uf1e8";fa.yelp="\uf1e9";fa.newspaperO="\uf1ea";fa.wifi="\uf1eb";fa.calculator="\uf1ec";fa.paypal="\uf1ed";fa.googleWallet="\uf1ee";fa.ccVisa="\uf1f0";fa.ccMastercard="\uf1f1";fa.ccDiscover="\uf1f2";fa.ccAmex="\uf1f3";fa.ccPaypal="\uf1f4";fa.ccStripe="\uf1f5";fa.bellSlash="\uf1f6";fa.bellSlashO="\uf1f7";fa.trash="\uf1f8";fa.copyright="\uf1f9";fa.at="\uf1fa";fa.eyedropper="\uf1fb";fa.paintBrush="\uf1fc";fa.birthdayCake="\uf1fd";fa.areaChart="\uf1fe";fa.pieChart="\uf200";fa.lineChart="\uf201";fa.lastfm="\uf202";fa.lastfmSquare="\uf203";fa.toggleOff="\uf204";fa.toggleOn="\uf205";fa.bicycle="\uf206";fa.bus="\uf207";fa.ioxhost="\uf208";fa.angellist="\uf209";fa.cc="\uf20a";fa.shekel="\uf20b";fa.sheqel="\uf20b";fa.ils="\uf20b";fa.meanpath="\uf20c";fa.buysellads="\uf20d";fa.connectdevelop="\uf20e";fa.dashcube="\uf210";fa.forumbee="\uf211";fa.leanpub="\uf212";fa.sellsy="\uf213";fa.shirtsinbulk="\uf214";fa.simplybuilt="\uf215";fa.skyatlas="\uf216";fa.cartPlus="\uf217";fa.cartArrowDown="\uf218";fa.diamond="\uf219";fa.ship="\uf21a";fa.userSecret="\uf21b";fa.motorcycle="\uf21c";fa.streetView="\uf21d";fa.heartbeat="\uf21e";fa.venus="\uf221";fa.mars="\uf222";fa.mercury="\uf223";fa.intersex="\uf224";fa.transgender="\uf224";fa.transgenderAlt="\uf225";fa.venusDouble="\uf226";fa.marsDouble="\uf227";fa.venusMars="\uf228";fa.marsStroke="\uf229";fa.marsStrokeV="\uf22a";fa.marsStrokeH="\uf22b";fa.neuter="\uf22c";fa.genderless="\uf22d";fa.facebookOfficial="\uf230";fa.pinterestP="\uf231";fa.whatsapp="\uf232";fa.server="\uf233";fa.userPlus="\uf234";fa.userTimes="\uf235";fa.hotel="\uf236";fa.bed="\uf236";fa.viacoin="\uf237";fa.train="\uf238";fa.subway="\uf239";fa.medium="\uf23a";fa.yc="\uf23b";fa.yCombinator="\uf23b";fa.optinMonster="\uf23c";fa.opencart="\uf23d";fa.expeditedssl="\uf23e";fa.mousePointer="\uf245";fa.iCursor="\uf246";fa.objectGroup="\uf247";fa.objectUngroup="\uf248";fa.stickyNote="\uf249";fa.stickyNoteO="\uf24a";fa.ccJcb="\uf24b";fa.ccDinersClub="\uf24c";fa.clone="\uf24d";fa.balanceScale="\uf24e";fa.hourglassO="\uf250";fa.hourglass="\uf254";fa.handGrabO="\uf255";fa.handRockO="\uf255";fa.handStopO="\uf256";fa.handPaperO="\uf256";fa.handScissorsO="\uf257";fa.handLizardO="\uf258";fa.handSpockO="\uf259";fa.handPointerO="\uf25a";fa.handPeaceO="\uf25b";fa.trademark="\uf25c";fa.registered="\uf25d";fa.creativeCommons="\uf25e";fa.gg="\uf260";fa.ggCircle="\uf261";fa.tripadvisor="\uf262";fa.odnoklassniki="\uf263";fa.odnoklassnikiSquare="\uf264";fa.getPocket="\uf265";fa.wikipediaW="\uf266";fa.safari="\uf267";fa.chrome="\uf268";fa.firefox="\uf269";fa.opera="\uf26a";fa.internetExplorer="\uf26b";fa.tv="\uf26c";fa.television="\uf26c";fa.contao="\uf26d";fa.amazon="\uf270";fa.calendarPlusO="\uf271";fa.calendarMinusO="\uf272";fa.calendarTimesO="\uf273";fa.calendarCheckO="\uf274";fa.industry="\uf275";fa.mapPin="\uf276";fa.mapSigns="\uf277";fa.mapO="\uf278";fa.map="\uf279";fa.commenting="\uf27a";fa.commentingO="\uf27b";fa.houzz="\uf27c";fa.vimeo="\uf27d";fa.blackTie="\uf27e";fa.fonticons="\uf280";fa.redditAlien="\uf281";fa.edge="\uf282";fa.creditCardAlt="\uf283";fa.codiepie="\uf284";fa.modx="\uf285";fa.fortAwesome="\uf286";fa.usb="\uf287";fa.productHunt="\uf288";fa.mixcloud="\uf289";fa.scribd="\uf28a";fa.pauseCircle="\uf28b";fa.pauseCircleO="\uf28c";fa.stopCircle="\uf28d";fa.stopCircleO="\uf28e";fa.shoppingBag="\uf290";fa.shoppingBasket="\uf291";fa.hashtag="\uf292";fa.bluetooth="\uf293";fa.bluetoothB="\uf294";fa.percent="\uf295";fa.gitlab="\uf296";fa.wpbeginner="\uf297";fa.wpforms="\uf298";fa.envira="\uf299";fa.universalAccess="\uf29a";fa.wheelchairAlt="\uf29b";fa.questionCircleO="\uf29c";fa.blind="\uf29d";fa.audioDescription="\uf29e";fa.volumeControlPhone="\uf2a0";fa.braille="\uf2a1";fa.assistiveListeningSystems="\uf2a2";fa.aslInterpreting="\uf2a3";fa.americanSignLanguageInterpreting="\uf2a3";fa.deafness="\uf2a4";fa.hardOfHearing="\uf2a4";fa.deaf="\uf2a4";fa.glide="\uf2a5";fa.glideG="\uf2a6";fa.signing="\uf2a7";fa.signLanguage="\uf2a7";fa.lowVision="\uf2a8";fa.viadeo="\uf2a9";fa.viadeoSquare="\uf2aa";fa.snapchat="\uf2ab";fa.snapchatGhost="\uf2ac";fa.snapchatSquare="\uf2ad";fa.piedPiper="\uf2ae";fa.firstOrder="\uf2b0";fa.yoast="\uf2b1";fa.themeisle="\uf2b2";fa.googlePlusCircle="\uf2b3";fa.googlePlusOfficial="\uf2b3";fa.fa="\uf2b4";fa.fontAwesome="\uf2b4";fa.handshakeO="\uf2b5";fa.envelopeOpen="\uf2b6";fa.envelopeOpenO="\uf2b7";fa.linode="\uf2b8";fa.addressBook="\uf2b9";fa.addressBookO="\uf2ba";fa.vcard="\uf2bb";fa.addressCard="\uf2bb";fa.vcardO="\uf2bc";fa.addressCardO="\uf2bc";fa.userCircle="\uf2bd";fa.userCircleO="\uf2be";fa.userO="\uf2c0";fa.idBadge="\uf2c1";fa.driversLicense="\uf2c2";fa.idCard="\uf2c2";fa.driversLicenseO="\uf2c3";fa.idCardO="\uf2c3";fa.quora="\uf2c4";fa.freeCodeCamp="\uf2c5";fa.telegram="\uf2c6";fa.shower="\uf2cc";fa.podcast="\uf2ce";fa.windowMaximize="\uf2d0";fa.windowMinimize="\uf2d1";fa.windowRestore="\uf2d2";fa.timesRectangle="\uf2d3";fa.windowClose="\uf2d3";fa.timesRectangleO="\uf2d4";fa.windowCloseO="\uf2d4";fa.bandcamp="\uf2d5";fa.grav="\uf2d6";fa.etsy="\uf2d7";fa.imdb="\uf2d8";fa.ravelry="\uf2d9";fa.eercast="\uf2da";fa.microchip="\uf2db";fa.snowflakeO="\uf2dc";fa.superpowers="\uf2dd";fa.wpexplorer="\uf2de";fa.meetup="\uf2e0";module.exports=fa;
});

var fa = (index && typeof index === 'object' && 'default' in index ? index['default'] : index);

var ViewerCanvas = function () {
    function ViewerCanvas() {
        classCallCheck(this, ViewerCanvas);

        this.defaultConfig = {
            "menu_items": [],
            "pan_and_zoom": false,
            "enable_zone_boundaries": false,
            "enable_collision_avoidance": false,
            "preferred_ratio": 0.6666666,
            "background_color": "black",
            "fixed_width": false,
            "paper_width": 1000,
            "force_collide": 0
        };
    }

    createClass(ViewerCanvas, [{
        key: 'mountNode',
        value: function mountNode(root) {
            this.clientWidth = root.clientWidth;
            this.domNode = d3.select(root).append("svg").node();
            this.contentNode = d3.select(root).append("div").node();
            this.initCanvas();
        }
    }, {
        key: 'setState',
        value: function setState(state) {
            this.state = state;
        }
    }, {
        key: 'trigger',
        value: function trigger(event$$1, data) {
            var doc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.state;

            if (this.events && this.events[event$$1]) {
                var func = this.events[event$$1][0];
                for (var funcIndex = 0; funcIndex < this.events[event$$1].length; funcIndex++) {
                    var _func = this.events[event$$1][funcIndex];
                    _func(event$$1, data, doc);
                }
            }
        }
    }, {
        key: 'on',
        value: function on(event$$1, func) {
            if (typeof this.events == "undefined") {
                this.events = [];
            }

            if (typeof this.events[event$$1] == "undefined") {
                this.events[event$$1] = [];
            }
            this.events[event$$1].push(func);
        }
    }, {
        key: 'zoom',
        value: function zoom$$1(vbox) {
            console.log("vbox = " + vbox);
            var vb = this.vis.attr("viewBox"); // "0 0 1000 800"
            this.vis.transition().attr("viewBox", vbox);
            return vb;
        }
    }, {
        key: 'printRect',
        value: function printRect(elemRect) {
            var label = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "E";

            console.log(label + ": Top=" + elemRect.top + ",Left=" + elemRect.left + ",Width=" + elemRect.width + ",Height=" + elemRect.height);
        }
    }, {
        key: 'zoomToNode',
        value: function zoomToNode(node, zoomAmount) {
            var self = this;
            var nd = d3.selectAll('g.node').filter(function (d, i) {
                return node == i;
            }).each(function (d) {

                //self.vis.attr("viewBox", "0 0 1000 800");

                var bb = this.getBoundingClientRect();
                self.printRect(bb, "View box");
                // 0 0 1000 150
                //let zoom = d3.zoom().scaleBy(5);
                //zoom.scaleBy(this, 5);
                var cy = Number(d3.select(this).attr("cy"));
                var cx = Number(d3.select(this).attr("cx"));
                var newViewBox = [cx - 0, cy, 300, 300 * 150 / 1000].join(' ');
                //var transform = d3.zoomTransform(d3.select(this).node());
                //transform.scale(10);
                //self.zoom (newViewBox);
            });
        }
    }, {
        key: 'initCanvas',
        value: function initCanvas() {
            var self = this;
            var el = this.domNode;

            //this.zones = this.state.zones;

            var paperWidth = this.getConfig("paper_width");
            var ratio = this.getConfig("preferred_ratio");
            var fixedWidth = this.getConfig("fixed_width");

            var full = el.getBoundingClientRect();
            ratio = full.height / full.width;

            var ow = fixedWidth ? paperWidth : el.parentNode.clientWidth;
            var oh = ow * ratio;

            console.log("W = " + ow + ", H = " + oh + " : " + el.parentNode.clientWidth + " (original = " + self.clientWidth + ")");

            var vis = d3.select(el).attr("width", ow);

            var rect = [0, 0, paperWidth, paperWidth * ratio];

            var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];

            vis.attr("preserveAspectRatio", "xMinYMin");
            vis.attr("viewBox", zone); // "0 0 1000 800"

            var defs = vis.append("defs");
            defs.selectAll("marker").data(["suit", "licensing", "resolved", "lifecycle"]).enter().append("marker").attr("id", function (d) {
                return d;
            }).attr("class", function (d) {
                return "_end_" + d;
            }).attr("viewBox", "0 -5 10 10").attr("refX", 0).attr("refY", 0).attr("markerWidth", 5).attr("markerHeight", 5).attr("orient", "auto").append("path").attr("d", "M0,-5L10,0L0,5");

            var filter = defs.append("filter").attr("id", "drop-shadow").attr("x", 0).attr("y", 0).attr("height", "150%").attr("width", "150%");
            filter.append("feOffset").attr("in", "SourceAlpha").attr("dx", 3).attr("dy", 3).attr("result", "offOut");
            filter.append("feGaussianBlur").attr("in", "offOut").attr("stdDeviation", 1).attr("result", "blurOut");
            filter.append("feBlend").attr("in", "SourceGraphic").attr("in2", "blurOut").attr("mode", "normal");
            //      const feMerge = filter.append("feMerge");
            //      feMerge.append("feMergeNode").attr("in", "offsetBlur");
            //      feMerge.append("feMergeNode").attr("in", "SourceGraphic");


            //      let all = vis.append("rect")
            //          .style("fill", this.getConfig("background_color"))
            //          .attr("width", "100%")
            //          .attr("height", "100%")
            //          .attr("x", 0)
            //          .attr("y", 0);

            var zoneRoot = vis.append("g").attr("class", "zones");

            var linesRoot = vis.append("g").attr("class", "all_lines");

            var nodeSetRoot = vis.append("g").attr("class", "nodeSet");

            var calcLinkDistance = function calcLinkDistance(a, b) {
                //          return Math.abs(Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)));

                if (a.source.boundary != a.target.boundary) {
                    return ow / 4;
                } else if (a.type == "step-after") {
                    return a.hint ? a.hint : 150;
                } else if (a.type == "straight" || a.type == "step-before") {
                    //            console.log("POSITIONS: " + JSON.stringify(a.source));
                    //            console.log(" AND " + JSON.stringify(a.target));
                    //            console.log(" ..X " + a.source.x +", "+a.target.x);
                    //            console.log(" ..Y " + a.source.y +", "+a.target.y);
                    //            console.log(" ..Y " + a.source.y +", "+a.target.y);
                    //            console.log(" ..CX " + ((a.source.x - a.target.x) * 2));
                    //            console.log(" ..CY " + ((a.source.y - a.target.y) * 2));
                    //            console.log(" ..Answer " + (Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
                    //            console.log("SQRT : " + Math.sqrt(Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
                    return a.hint ? a.hint : 150;
                } else {
                    return a.hint ? a.hint : 80;
                }
            };

            var simulation = d3.forceSimulation().force("link", d3.forceLink([]).distance(calcLinkDistance).strength(0.5)).force("collide", d3.forceCollide(function (d) {
                return self.getConfig("force_collide");
            }).iterations(1)).force("charge", d3.forceManyBody().strength(0));

            var force = simulation;
            //          .links([])
            //          .nodes([])
            //          .gravity(0)
            //          .charge(0)
            //          .linkDistance(calcLinkDistance)
            //          .size([ow, oh]);


            this.vis = vis;
            this.force = force;

            // fa-book, fa-th, fa-share-square-o, fa-camera-retro
            var navData = [{ "id": "1", "title": "Documentation", "icon": fa.book }, { "id": "2", "title": "Configuration", "icon": fa('tasks') }, { "id": "3", "title": "Topology", "icon": fa.th }, { "id": "4", "title": "Audit", "icon": fa('camera-retro') }];

            this.navigation = new Navigation();

            this.navigation.attach(vis, this, this.contentNode);
            //      this.navigation.addMenuItem(navData[0]);
            //      this.navigation.addMenuItem(navData[1]);
            //      this.navigation.addMenuItem(navData[2]);
            //      this.navigation.addMenuItem(navData[3]);

            this.navigation.update();
            //console.log("NAV = "+faIconChars[0].unicode);

            //      all.on("click", function (d) {
            //         self.closePopups();
            //      });
        }
    }, {
        key: 'closePopups',
        value: function closePopups() {
            this.navigation.close();
        }
    }, {
        key: 'refreshZones',
        value: function refreshZones(zones, config) {

            var self = this;
            var vis = this.vis;

            var pageHeight = vis.node().parentNode.clientHeight;
            var pageWidth = vis.node().parentNode.clientWidth;

            var zoneRoot = vis.select("g.zones");

            zoneRoot.selectAll("g").remove();

            vis.attr("pageWidth", pageWidth);
            vis.attr("pageHeight", pageHeight);

            this.zones = zones;

            zones.forEach(function (zone, zoneIndex) {

                var layout = LayoutFactory.getLayout(zone.type);
                if (layout == null) {
                    console.log("WARNING: Layout not found in catalog: " + zone.type);
                } else {
                    layout.build(vis, zone, zoneRoot, self);

                    layout.organize(vis, zoneIndex, zones);
                }
            });

            zoneRoot.selectAll("g").on('mouseenter', function (nodeSet, index) {
                d3.select(this).classed("zone-hover", true);
            }).on('mouseleave', function (e) {
                d3.select(this).classed("zone-hover", false);
            });

            zoneRoot.selectAll("g").selectAll("rect").on("click", function (d) {
                self.closePopups();
            });
        }
    }, {
        key: 'refreshSize',
        value: function refreshSize() {
            var self = this;
            var el = this.domNode;
            var vis = this.vis;

            var paperWidth = this.getConfig("paper_width");
            var ratio = this.getConfig("preferred_ratio");
            var fixedWidth = this.getConfig("fixed_width");

            var ow = fixedWidth == false ? el.parentNode.clientWidth : paperWidth;
            var oh = ratio == 0 ? el.parentNode.clientHeight : ow * ratio; //el.parentNode.clientHeight; //ow * ratio;


            console.log("refreshSize() : SVG [W = " + ow + ", H = " + oh + "] :: Parent clientHeight=" + el.parentNode.clientHeight);
            console.log("refreshSize() : Body: W = " + document.body.clientWidth + ", H = " + document.body.clientHeight);

            vis.attr("width", ow).attr("height", oh);

            d3.select(this.domNode.parentNode).style("background-color", this.getConfig("background_color"));

            // scale the viewbox to maximize the zoom
            // Adjust the height to nothing larger than what the zones are using
            vis.select("g.zones").each(function (d) {

                // if ratio is 0, then set the height to the height of all zones
                //
                var bbox = this.getBBox();
                var parentBbox = this.parentNode.getBBox();

                if (ratio != 0) {
                    var rect = [0, 0, paperWidth, paperWidth * ratio];
                    var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];
                    console.log("Setting ViewBox (has ratio) to " + zone);
                    vis.attr("viewBox", zone);
                }
            });

            self.zones.forEach(function (zone, zoneIndex) {

                var layout = LayoutFactory.getLayout(zone.type);
                if (zone.type == "tabs" || zone.type == "default" || zone.type == "carousel") {
                    layout.refreshSize(self, zone, vis.select("g.zones").filter(function (d, i) {
                        return i == zoneIndex;
                    }));
                }
            });

            vis.select("g.zones").each(function () {

                // if ratio is 0, then set the height to the height of all zones
                //
                var bbox = this.getBBox();
                var parentBbox = this.parentNode.getBBox();

                if (ratio == 0) {
                    var rect = [0, 0, paperWidth, bbox.height];
                    console.log("refreshSize() : BBOX: W = " + bbox.width + ", H = " + bbox.height);

                    var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];

                    console.log("Setting ViewBox (ratio=0) to " + zone);
                    vis.attr("viewBox", zone);
                }
            });

            this.updatePanZoom();

            this.updateMenuItems();

            //
            //      let rect = [0, 0, ow, ow * ratio];
            //
            //      const zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];
            //
            //      vis.attr("preserveAspectRatio", "xMinYMin")
            //vis.attr("viewBox", zone); // "0 0 1000 800"
        }
    }, {
        key: 'getDim',
        value: function getDim(d) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (this.zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = this.zones[index].hasOwnProperty('calculatedRectangle') ? this.zones[index].calculatedRectangle : this.zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'clear',
        value: function clear() {
            //console.log("CLEARING");
            this.vis.select("g.nodeSet").selectAll("g.node").remove();
        }
    }, {
        key: 'getConfig',
        value: function getConfig(c) {
            var config = this.state ? this.state.config : null;
            if (config && config.hasOwnProperty(c)) {
                return config[c];
            } else {
                return this.defaultConfig[c];
            }
        }
    }, {
        key: 'traverseChildren',
        value: function traverseChildren(newNodes) {
            var self = this;
            var vis = this.svg;

            newNodes.each(function (d) {
                var _this = this;

                if (d.children) {
                    (function () {

                        var nodeSet = d3.select(_this.parentNode).selectAll("g.node_" + d.name).data(d.children);

                        var newNodesC = nodeSet.enter().append("g").attr("class", "node_" + d.name + " nodec");

                        var names = ShapeFactory.getShapeNames();

                        var _loop = function _loop(nm) {
                            var shap = ShapeFactory.getShape(names[nm]);
                            shap.build(newNodesC.filter(function (d) {
                                return d.type == names[nm];
                            }).classed("_" + names[nm], true));

                            if (typeof shap.update === 'function') {
                                shap.update(nodeSet.filter(function (d) {
                                    return d.type == names[nm];
                                }).classed("_" + names[nm], true));
                            }
                        };

                        for (var nm in names) {
                            _loop(nm);
                        }

                        newNodesC.attr("cx", function (d) {
                            var dim = self.getDim(d);var x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;d.x = x;d.fx = d.fixed ? x : null;
                        }).attr("cy", function (d) {
                            var dim = self.getDim(d);var y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;d.y = y;d.fy = d.fixed ? y : null;
                        });

                        self.zones.forEach(function (zone, index) {
                            var layout = LayoutFactory.getLayout(zone.type);
                            var filtered = newNodesC.filter(function (d) {
                                var i = d.boundary.charCodeAt(0) - 65;return index == i;
                            });
                            layout.configEvents(vis, filtered, self.zones, self);
                        });

                        //              // Attach Events
                        //              self.zones.forEach(function (zone, index) {
                        //                let layout = LayoutFactory.getLayout(zone.type);
                        //                //layout.configEvents(vis, newNodesC, self.zones, self);
                        //              });

                        self.traverseChildren(newNodesC);
                    })();
                }
            });
        }
    }, {
        key: 'zoomed',
        value: function zoomed(transform) {
            var vis = this.vis;
            vis.select('g.nodeSet').attr("transform", transform);
            vis.select('g.zones').attr("transform", transform);
            vis.select('g.all_lines').attr("transform", transform);
            vis.select('g.nav').attr("transform", transform);
        }
    }, {
        key: 'zoomReset',
        value: function zoomReset() {
            this.vis.transition().duration(750).call(this.zoom.transform, d3.zoomIdentity);
        }
    }, {
        key: 'updateMenuItems',
        value: function updateMenuItems() {
            var self = this;
            var menus = this.getConfig('menu_items');
            menus.forEach(function (m, i) {
                var nav = { "id": m.id, "title": m.label, "icon": fa(m.icon) };
                self.navigation.addMenuItem(nav);
            });
            self.navigation.update();
        }
    }, {
        key: 'updatePanZoom',
        value: function updatePanZoom() {
            var self = this;
            var vis = this.vis;
            var oh = vis.attr("height");
            var ow = vis.attr("width");

            function zoomed() {
                self.zoomed(d3.event.transform);
            }

            this.zoom = d3.zoom().scaleExtent([1, 40]).translateExtent([[-100, -100], [ow + 90, oh + 100]]).on("zoom", zoomed);

            if (this.getConfig("pan_and_zoom")) {
                vis.call(this.zoom);
            }
        }
    }, {
        key: 'update',
        value: function update() {
            var self = this;
            var vis = this.vis;

            var force = this.force;

            self.registerLinks();
            self.refreshZones(this.state.zones, {});

            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                layout.enrichData(self.state.nodes);
            });

            var linkInfo = vis.select("g.all_lines").selectAll("g").data(this.state.links);

            var linkGroup = linkInfo.enter().insert("g").attr("class", function (d) {
                return d.hasOwnProperty("class") ? "_" + d.type + " _" + d.class : "_" + d.type;
            });

            linkGroup.append("path").attr("class", "link");

            linkGroup.filter(function (d) {
                return d.ends;
            }).append("circle").attr("r", 5).attr("fill", "#298EFE").attr("class", "circleSource");

            linkGroup.filter(function (d) {
                return d.ends;
            }).append("circle").attr("r", 5).attr("fill", "#298EFE").attr("class", "circleTarget");

            linkGroup.append("text").attr("text-anchor", "left").text(function (d) {
                return "Link Text";
            }).attr("class", "linkText");

            linkInfo.exit().remove();

            var links = linkGroup.selectAll("path");

            var nodeSet = vis.select("g.nodeSet").selectAll("g.node").data(this.state.nodes, function (k, i) {
                return k.index;
            });

            nodeSet.exit().each(function (d) {
                console.log("DETECTED AN EXIT : " + JSON.stringify(d));
            });
            var newNodes = nodeSet.enter().append("g").attr("class", "node");

            var names = ShapeFactory.getShapeNames();

            var _loop2 = function _loop2(nm) {
                var shap = ShapeFactory.getShape(names[nm]);
                shap.build(newNodes.filter(function (d) {
                    return d.type == names[nm];
                }).classed("_" + names[nm], true));

                if (typeof shap.update === 'function') {
                    shap.update(nodeSet.filter(function (d) {
                        return d.type == names[nm];
                    }).classed("_" + names[nm], true));
                }
            };

            for (var nm in names) {
                _loop2(nm);
            }

            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d);var x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;d.x = x;d.fx = d.fixed ? x : null;
            }).attr("cy", function (d) {
                var dim = self.getDim(d);var y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;d.y = y;d.fy = d.fixed ? y : null;
            });

            // Add children (attach tab navigation)
            // Calculate the grouping metadata for the layout
            // Organize the tab hierarchy by adjusting the x,y,width coordinates

            // Get the navigation to work with the children


            this.traverseChildren(newNodes);

            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                var filtered = newNodes.filter(function (d) {
                    var i = d.boundary.charCodeAt(0) - 65;return index == i;
                });
                layout.configEvents(vis, filtered, self.zones, self);
            });

            // Organize:
            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                layout.organize(vis, index, self.zones);
            });

            nodeSet.exit().remove();

            nodeSet = vis.select("g.nodeSet").selectAll("g.nodec, g.node");

            var ticked = function ticked() {

                var radius = 20;
                var w = vis.attr("width");
                var h = vis.attr("height");

                // enforce zone boundaries
                if (self.getConfig('enable_zone_boundaries')) {
                    nodeSet.attr("cx", function (d) {
                        var dim = self.getDim(d);d.x = Math.max(dim.x1 + radius, Math.min(dim.x2 - radius, d.x));return d.x;
                    }).attr("cy", function (d) {
                        var dim = self.getDim(d);d.y = Math.max(dim.y1 + radius, Math.min(dim.y2 - radius, d.y));return d.y;
                    });
                }

                if (self.getConfig('enable_collision_avoidance')) {
                    nodeSet.each(self.collide(0.5));
                }

                nodeSet.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                var ln = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveLinear); // step, linear

                var stepBefore = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveStepBefore); // step, step-after, step

                var stepAfter = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveStepAfter); // step, step-after, step

                var cardinal = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveCardinal); // step, step-after, step


                links.attr("d", function (d) {
                    var sourcePoints = { "x": d.source.x, "y": d.source.y };
                    var targetPoints = { "x": d.target.x, "y": d.target.y };
                    var a = 0;

                    // Source
                    if (d.hasOwnProperty('ends')) {
                        (function () {
                            var deltaX = 0;
                            var deltaY = 0;

                            // Find the source node and based on its bounding box, set the position

                            var s1 = nodeSet.filter(function (inf, ind) {
                                return d.source.index == ind;
                            });
                            var t1 = nodeSet.filter(function (inf, ind) {
                                return d.target.index == ind;
                            });

                            var sourceEnd = d.ends.substring(0, 1);
                            var targetEnd = d.ends.substring(2, 3);

                            if (d.ends == "auto" && (d.type == "straight" || d.type == "step-before")) {
                                if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "S";
                                    targetEnd = "W";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "S";
                                    targetEnd = "E";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                                    sourceEnd = "N";
                                    targetEnd = "E";
                                } else {
                                    sourceEnd = "N";
                                    targetEnd = "W";
                                }
                            }

                            if (d.ends == "auto" && d.type == "step-after") {
                                if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "E";
                                    targetEnd = "N";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "W";
                                    targetEnd = "N";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                                    sourceEnd = "W";
                                    targetEnd = "S";
                                } else {
                                    sourceEnd = "E";
                                    targetEnd = "S";
                                }
                            }

                            s1.each(function () {
                                if (sourceEnd == "W") {
                                    deltaX = -this.getBBox().width / 2;
                                }
                                if (sourceEnd == "E") {
                                    deltaX = this.getBBox().width / 2;
                                }
                                if (sourceEnd == "N") {
                                    deltaY = -this.getBBox().height / 2;
                                }
                                if (sourceEnd == "S") {
                                    deltaY = this.getBBox().height / 2;
                                }
                            });

                            sourcePoints = { "x": d.source.x + deltaX, "y": d.source.y + deltaY };

                            deltaX = 0;
                            deltaY = 0;
                            t1.each(function () {
                                if (targetEnd == "W") {
                                    deltaX = -this.getBBox().width / 2;
                                }
                                if (targetEnd == "E") {
                                    deltaX = this.getBBox().width / 2;
                                }
                                if (targetEnd == "N") {
                                    deltaY = -this.getBBox().height / 2;
                                }
                                if (targetEnd == "S") {
                                    deltaY = this.getBBox().height / 2;
                                }
                            });

                            targetPoints = { "x": d.target.x + deltaX, "y": d.target.y + deltaY };
                        })();
                    }

                    var lineData = [sourcePoints, targetPoints];
                    if (d.type == "straight" || d.type == "step-before") {
                        return stepBefore(lineData);
                    } else if (d.type == "step-after") {
                        return stepAfter(lineData);
                    } else if (d.type == "cardinal") {
                        return cardinal(lineData);
                    } else {
                        return ln(lineData);
                    }
                });

                // display text
                links.each(function (d) {
                    //console.log("Total length = " + this.getTotalLength());
                    var labelPosition = 50;
                    if (d.hasOwnProperty("labelPosition")) {
                        labelPosition = d.labelPosition;
                    }
                    var point = this.getPointAtLength(this.getTotalLength() * (labelPosition / 100));
                    var label = d3.select(this.parentNode).select("text");
                    label.text(function (ld) {
                        return d.label;
                    });
                    label.attr("transform", "translate(" + (point.x - 5) + "," + (point.y - 5) + ")");
                    var circleSource = d3.select(this.parentNode).select("circle.circleSource");
                    var sourcePoint = this.getPointAtLength(0);
                    circleSource.attr("transform", "translate(" + sourcePoint.x + "," + sourcePoint.y + ")");
                    var circleTarget = d3.select(this.parentNode).select("circle.circleTarget");
                    var targetPoint = this.getPointAtLength(this.getTotalLength() - 0);
                    circleTarget.attr("transform", "translate(" + targetPoint.x + "," + targetPoint.y + ")");
                    //layout (this);
                });
            };

            // Restart the force layout
            var nodeList = [];
            for (var a in this.state.nodes) {
                nodeList.push(this.state.nodes[a]);
            }

            for (var i = 0; i < this.state.nodes.length; i++) {
                if (this.state.nodes[i].children) {
                    for (var j = 0; j < this.state.nodes[i].children.length; j++) {
                        nodeList.push(this.state.nodes[i].children[j]);
                    }
                }
            }

            force.nodes(nodeList).on("tick", ticked);

            force.force("link").links(this.state.links);
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            this.force.on("tick", null);
            d3.select(this.domNode).remove();
            d3.select(this.contentNode).remove();
        }

        // Resolves collisions between d and all other nodes.

    }, {
        key: 'collide',
        value: function collide(alpha) {

            var padding = 1.5,
                maxRadius = 12;
            var quadtree$$1 = d3.quadtree(this.state.nodes);

            return function (d) {
                d.radius = 25;
                var r = d.radius + maxRadius + padding,
                    nx1 = d.x - r,
                    nx2 = d.x + r,
                    ny1 = d.y - r,
                    ny2 = d.y + r;

                if (d.x == null) {
                    //console.log("Ignoring.. " + d.name);
                    return;
                }
                quadtree$$1.visit(function (quad, x1, y1, x2, y2) {
                    //console.log("Match: " + x1+","+y1+","+x2+","+y2 + " : " + quad.point);
                    //if (quad.leaf == false) {
                    //    return true;
                    //}
                    // quad.point has the object details
                    if (quad.point && quad.point !== d) {

                        if (quad.point.boundary != d.boundary) {}
                        //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;

                        //if (d.x == null) {
                        //  return true;
                        //}
                        //console.log("D = "+JSON.stringify(d, null, 2));
                        //console.log("Quad = "+JSON.stringify(quad, null, 2));
                        quad.point.radius = 25;
                        var x = d.x - quad.point.x,
                            y = d.y - quad.point.y,
                            l = Math.abs(Math.sqrt(x * x + y * y)),
                            _r = d.radius + quad.point.radius + padding;
                        //console.log("X="+x+",Y="+y);
                        //console.log("L="+l+",R="+r);
                        if (l < _r) {
                            l = (l - _r) / l * alpha;

                            // Need to make sure we are not moving it outside of boundary
                            x = x * l;
                            y = y * l;
                            if (isNaN(x) || isNaN(y)) {
                                //console.log("ILLEGAL VALUE!");
                                x = 0.00;
                                y = 0.01;
                                //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                            }
                            //console.log("Changing..." + x+", "+y+", dx ="+d.x+", "+d.y);
                            d.x = d.x - x;
                            d.y = d.y - y;
                            //console.log("ANSWER: d="+d.x+", "+d.y);
                            quad.point.x += x;
                            quad.point.y += y;
                            //console.log("Change to: " +d.x+", "+d.y+" :: " + quad.point.x+", "+quad.point.y);
                        }
                    }
                    //console.log("Answer: " +x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1);
                    return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                });
            };
        }
    }, {
        key: 'addNode',
        value: function addNode(name, fixed, type, boundary) {
            var nd = { "name": name, "fixed": fixed ? true : false, "type": type, "boundary": boundary };
            this.state.nodes.push(nd);
            this.update();
        }
    }, {
        key: 'removeNode',
        value: function removeNode(name) {

            this.state.nodes = this.state.nodes.filter(function (node) {
                return node["name"] != name;
            });
            this.state.links = this.state.links.filter(function (link) {
                return link["source"]["name"] != name && link["target"]["name"] != name;
            });
            this.update();
        }
    }, {
        key: 'findNode',
        value: function findNode(name) {
            for (var i in this.state.nodes) {
                if (this.state.nodes[i]["name"] === name) return this.state.nodes[i];
            }
        }
    }, {
        key: 'registerLinks',
        value: function registerLinks() {
            var links = this.state.links;
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                if (typeof link.source == "string") {
                    link.source = this.findNode(link.source);
                }
                if (typeof link.target == "string") {
                    link.target = this.findNode(link.target);
                }
            }
        }
    }, {
        key: 'render',
        value: function render() {
            if (this.state) {
                this.update();
            }
            //var node = document.createElement("svg");
            //this.domNode.appendChild(node);
        }
    }]);
    return ViewerCanvas;
}();

__$styleInject("._comment{stroke-width:0px}._comment text{font-size:1em;font-family:Open Sans;fill:#fff}", undefined);

var TextUtils = function () {
    function TextUtils() {
        classCallCheck(this, TextUtils);
    }

    createClass(TextUtils, null, [{
        key: "splitByNL",
        value: function splitByNL(name) {
            // look for a \n
            var ind = name.indexOf("\n");
            if (ind != -1) {
                return [name.substr(0, ind), name.substr(ind)];
            } else {
                return [name];
            }
        }
    }, {
        key: "splitByWidth",
        value: function splitByWidth(caption, maxWidth, width) {
            if (maxWidth < width) {
                return [caption];
            }
            var ratio = Math.round(caption.length * (width / maxWidth));
            console.log("Max = " + maxWidth + ", " + width + " RATIO: " + ratio + " : " + caption.length);
            return TextUtils.splitByWords(caption, ratio);
        }
    }, {
        key: "splitByWords",
        value: function splitByWords(caption, maxCharsPerLine) {

            var words = caption.split(' ');
            var line$$1 = "";

            var lines = [];
            for (var n = 0; n < words.length; n++) {
                var testLine = line$$1 + words[n] + " ";
                if (testLine.length > maxCharsPerLine && line$$1.length > 0) {
                    lines.push(line$$1);

                    line$$1 = words[n] + " ";
                } else {
                    line$$1 = testLine;
                }
            }
            lines.push(line$$1);

            return lines;
        }
    }]);
    return TextUtils;
}();

var Shape = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var tmpTxtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 0).attr("dy", 0).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            var bbox = [];
            tmpTxtNode.each(function (d, i) {
                bbox[i] = this.getBBox();
            });

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 0).attr("dy", 0).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).each(function (d, ti) {
                d3.select(this).selectAll('tspan').data(TextUtils.splitByWidth(d.label, bbox[ti].width, d.width ? d.width : 999999)).enter().append("tspan").attr("x", 0).attr("y", function (d, i) {
                    return i * bbox[ti].height;
                }).text(function (line$$1) {
                    return line$$1;
                });
            });

            tmpTxtNode.each(function (d) {
                this.parentNode.removeChild(this);
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", -10).attr("dy", 0).text(function (d) {
                return fa(d.icon);
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._application{fill:#f6d5b9}._application rect.active{fill:#43bb3f}._application rect.inactive{fill:#f19996}._application rect.global{fill:#ccc}._application rect{stroke-width:0px;stroke:#999}._application text{fill:#000;font-family:Open Sans}._application text.label{font-size:.6em}._application text.bottomLeft,._application text.bottomRight,._application text.topLeft,._application text.topRight{font-size:.4em}._application text.appId{font-size:.45em;font-weight:700}", undefined);

var RectangleShape = function () {
    function RectangleShape(domainType) {
        classCallCheck(this, RectangleShape);

        this.domainType = domainType;
    }

    createClass(RectangleShape, [{
        key: 'build',
        value: function build(chgSet) {
            var self = this;

            var x = 120;
            var y = 60;

            chgSet.append("rect").attr("x", -x / 2).attr("y", -y / 2).attr("width", x).attr("height", y).attr("class", function (d) {
                return d.state;
            });
            //.style("transform", "translate(-50%, -50%)")
            //.style("filter", "url(#drop-shadow)")

            chgSet.append("text").attr("class", "appId").attr("text-anchor", "middle").attr("dx", 0).attr("dy", 16).text(function (d) {
                return d.appId;
            });

            var appNameNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            appNameNode.append("tspan").attr("x", 0).attr("y", -4).text(function (d) {
                var lines = TextUtils.splitByWords(d.label, 20);return lines[0];
            });

            appNameNode.append("tspan").attr("x", 0).attr("y", 6).text(function (d) {
                var lines = TextUtils.splitByWords(d.label, 20);return lines.length == 1 ? "" : lines[1];
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("topLeft");
            }).append("text").attr("x", -x / 2 + 2).attr("y", -y / 2 + 8).attr("class", "topLeft").attr("text-anchor", "start").text(function (d) {
                return d.labels.topLeft;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("topRight");
            }).append("text").attr("x", x / 2 - 2).attr("y", -y / 2 + 8).attr("class", "topRight").attr("text-anchor", "end").text(function (d) {
                return d.labels.topRight;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("bottomLeft");
            }).append("text").attr("x", -x / 2 + 2).attr("y", y / 2 - 4).attr("class", "bottomLeft").attr("text-anchor", "start").text(function (d) {
                return d.labels.bottomLeft;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("bottomRight");
            }).append("text").attr("x", x / 2 - 2).attr("y", y / 2 - 4).attr("class", "bottomRight").attr("text-anchor", "end").text(function (d) {
                return d.labels.bottomRight;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty("image");
            }).append("image").attr("x", x / 2 - 20 - 2).attr("y", y / 2 - 10 - 2).attr("width", function (d) {
                return 20;
            }).attr("height", function (d) {
                return 10;
            }).attr("xlink:href", function (d) {
                return d.image;
            });

            chgSet.filter(function (d) {
                return typeof d.alerts != "undefined";
            }).each(function (d) {

                var alertNodes = d3.select(this).selectAll("text.alerts").data(d.alerts);

                alertNodes.enter().append("text").attr("class", function (t) {
                    return t.class ? "icon " + t.class : "icon";
                }).attr("text-anchor", "end").attr("dx", function (a, i) {
                    return -x / 2 + 15 + i * 20;
                }).attr("dy", -y / 2 - 2).text(function (t) {
                    return fa(t.icon);
                }).on("click", function (e, i) {
                    d3.event.stopPropagation();

                    alert("clicked " + i + " : " + JSON.stringify(e) + " OBJ: " + d.name);
                });
            });
        }
    }]);
    return RectangleShape;
}();

__$styleInject("._icon{fill:#ccc;stroke:#333;stroke-width:1.5px}._icon text,._icon text.label{fill:#fff;stroke-width:0;font-size:.5em}", undefined);

var Shape$1 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            chgSet.append("text").attr("class", function (d) {
                return d.class ? "icon " + d.class : "icon";
            }).attr("text-anchor", "middle").attr("dx", 0).attr("dy", 5).text(function (d) {
                return fa(d.icon);
            });

            chgSet.append("text").filter(function (d) {
                return d.label;
            }).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).attr("text-anchor", "middle").attr("dx", 0).attr("dy", "2.0em").text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("", undefined);

var Shape$2 = function () {
  function Shape(domainType) {
    classCallCheck(this, Shape);

    this.domainType = domainType;
  }

  createClass(Shape, [{
    key: 'build',
    value: function build(chgSet) {
      chgSet.append("image").attr("x", function (d) {
        return -d.width / 2;
      }).attr("y", function (d) {
        return -d.height / 2;
      }).attr("width", function (d) {
        return d.width;
      }).attr("height", function (d) {
        return d.height;
      }).attr("class", function (d) {
        return d.class ? d.class : "image";
      }).attr("xlink:href", function (d) {
        return d.image;
      });
    }
  }]);
  return Shape;
}();

__$styleInject("._circle{fill:#ccc;stroke:#999;stroke-width:.2em}._circle circle{r:20}._circle text{fill:#fff;stroke-width:0;font-size:.6em;font-family:Open Sans}._circle2{fill:#6cf;stroke:#09c;stroke-width:.1em}._circle2 circle{r:10}._circle2 text{fill:#6cf;stroke-width:0;font-size:.4em;font-family:Open Sans}._circle3 circle{fill:#fff;stroke:#000;stroke-width:.1em;r:10}._circle3 text{fill:#000;font-size:.7em;font-weight:700;font-family:Arial}", undefined);

var Shape$3 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            chgSet.append("circle");

            chgSet.filter(function (d) {
                return !d.hasOwnProperty("position") || d.position == "right";
            }).append("text").attr("text-anchor", "left").attr("dx", function (d) {
                var circle = d3.select(this.parentNode).select('circle').node();return 2 + circle.getBBox().width / 2;
            }).attr("dy", 5).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.position == "center";
            }).append("text").attr("text-anchor", "middle").attr("dominant-baseline", "central").attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._milestone circle{fill:#fff}._milestone text{fill:#fff;font-size:.7em;font-family:Open Sans}", undefined);

var Shape$4 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 12).attr("dy", 4).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.append("circle").attr("r", 6).style("stroke", function (d) {
                return d.hasOwnProperty("color") ? d.color : "grey";
            }).style("stroke-width", ".3em");
        }
    }]);
    return Shape;
}();

__$styleInject("._button rect{fill:#000;stroke-width:.08em;stroke:red}._button text.label{fill:#fff;font-family:Open Sans,sans-serif;font-size:.9em}", undefined);

var Shape$5 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: "build",
        value: function build(chgSet) {
            var self = this;

            chgSet.classed("clickable", true);

            // Width calculated by the layout
            chgSet.append("rect").attr("x", -150 / 2).attr("y", -40 / 2).attr("height", 40).attr("width", 150);

            var textNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? 5 : -3;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines[0];
            });

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var bbox = this.parentNode.getBBox();return bbox.height - 3;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? "" : lines[1];
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._timeline{fill:#000;stroke:#333;stroke-width:0px}._timeline rect{stroke-width:0}._timeline circle{fill:#fff;stroke:#000}._timeline text{font-size:.7em;font-family:Open Sans}", undefined);

var Shape$6 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 12).attr("dy", -5).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").style("fill", function (d) {
                return d.color;
            }).attr("dx", 5).attr("dy", -5).text(function (d) {
                return fa(d.icon);
            });

            chgSet.append("circle").attr("r", 6).style("stroke", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).style("stroke-width", ".3em").attr("transform", function (d) {
                var width = d.hasOwnProperty("width") ? d.width : 200;return "translate(" + (width + 6) + ", 3)";
            });

            chgSet.append("rect").attr("class", "line").style("fill", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).attr("width", function (d) {
                return d.hasOwnProperty("width") ? d.width : 200;
            }).attr("height", 6);
        }
    }]);
    return Shape;
}();

__$styleInject("._timeline2 rect{stroke:#333;stroke-width:0}._timeline2 text{font-size:.7em;font-family:Open Sans;fill:#fff}._timeline2 text.icon{fill:#fff}", undefined);

var Shape$7 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var shape = this.domainType;

            chgSet.append("rect").attr("class", "line").attr("rx", function (d) {
                return shape == "timeline2" ? 8 : 0;
            }).attr("ry", function (d) {
                return shape == "timeline2" ? 8 : 0;
            }).style("fill", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).attr("width", function (d) {
                return d.hasOwnProperty("width") ? d.width : 200;
            }).attr("height", 20);

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", 10).attr("dy", 14).text(function (d) {
                return fa(d.icon);
            });

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 18).attr("dy", 14).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._panel{fill:#fff}", undefined);

var Shape$8 = function () {
  function Shape(domainType) {
    classCallCheck(this, Shape);

    this.domainType = domainType;
  }

  createClass(Shape, [{
    key: "build",
    value: function build(newNodes) {
      newNodes.each(function (d) {
        if (!d.hasOwnProperty("name")) {
          alert("Panel requires a name! " + JSON.stringify(d));
        }
      });
    }
  }]);
  return Shape;
}();

__$styleInject("._list{fill:#fff}._list rect{stroke-width:0px;stroke:#999;fill:#ccc}._list rect.tools{fill:#999}._list text.tools{font-size:.5em}._list text.xs{font-size:.4em}._list text{fill:#00008b;font-size:.6em}._list .value text,._list text{font-family:Open Sans,Arial;cursor:default}._list .value text{font-size:.8em}._list .value rect.selected{fill:#add8e6}._list .value rect.unselected{fill:#efefef}._list .value rect.noselection{fill:#90ee90}._list .value text.count{font-size:.6em}", undefined);

var Shape$9 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'update',
        value: function update(fullSet) {
            fullSet.each(function (d) {

                var valueNodes = d3.select(this).selectAll("g.value");

                valueNodes.select("rect").attr("class", function (i) {
                    return i.selected ? "selected" : i.noSelections ? "noselection" : "unselected";
                });

                valueNodes.select("text.item").text(function (i) {
                    return i.label;
                });

                valueNodes.select("text.count").text(function (i) {
                    return i.count;
                });

                valueNodes.select("text.icon").text(function (i) {
                    return i.selected ? fa("check-square") : fa("square-o");
                });
            });
        }
    }, {
        key: 'build',
        value: function build(chgSet) {
            var self = this;

            var x = 180;
            var y = 20;
            var rowHeight = 18;

            chgSet = chgSet.append("g").attr("transform", "scale(1.0)");

            chgSet.append("rect").attr("x", -x / 2).attr("y", function (d) {
                return -(y + d.values.length * rowHeight) / 2;
            }).attr("width", x).attr("height", function (d) {
                return y + d.values.length * rowHeight;
            });

            chgSet.append("text").attr("class", "label").attr("text-anchor", "left").attr("dx", 2 + -x / 2).attr("dy", function (d) {
                return 8 - (y + d.values.length * rowHeight) / 2;
            }).text(function (d) {
                return d.label.toUpperCase();
            });

            this.buildTools(chgSet, x, y);

            chgSet.each(function (d) {

                var valueNodes = d3.select(this).selectAll("g.value").data(d.values);
                var newNodes = valueNodes.enter().append("g").classed("value", true);
                var pos = -5;

                newNodes.append("rect").attr("x", -(x / 2) + 0).attr("y", function (i) {
                    pos += rowHeight;return 7 - (y + d.values.length * rowHeight) / 2 + pos;
                }).attr("width", x - 0).attr("height", rowHeight).attr("class", function (i) {
                    return i.selected ? "selected" : i.noSelections ? "noselection" : "unselected";
                });

                pos = -5;
                newNodes.append("text").attr("class", "item").attr("text-anchor", "left").attr("dx", -(x / 2) + 10 + 15).attr("dy", function (i) {
                    pos += rowHeight;return 20 - (y + d.values.length * rowHeight) / 2 + pos;
                }).text(function (i) {
                    return i.label;
                });
                pos = -5;
                newNodes.append("text").attr("class", "count").attr("text-anchor", "right").attr("dx", x / 2 - 20).attr("dy", function (i) {
                    pos += rowHeight;return 18 - (y + d.values.length * rowHeight) / 2 + pos;
                }).text(function (i) {
                    return i.count;
                });
                pos = -5;
                var xx = newNodes.append("text").attr("class", "icon").attr("text-anchor", "left").attr("dx", -(x / 2) + 10).attr("dy", function (i) {
                    pos += rowHeight;return 20 - (y + d.values.length * rowHeight) / 2 + pos;
                }).text(function (i) {
                    pos += rowHeight;return i.selected ? fa("check-square") : fa("square-o");
                });

                newNodes.classed("clickable", true);
            });
        }
    }, {
        key: 'buildTools',
        value: function buildTools(chgSet, x, y) {

            var rowHeight = 18;

            chgSet.append("rect").classed('tools', true).attr("x", -x / 2).attr("y", function (d) {
                return 11 - (y + d.values.length * rowHeight) / 2;
            }).attr("width", x).attr("height", function (d) {
                return -11 + y + d.values.length * rowHeight;
            });

            // apply paging... dots for 5 pages, then >> and << for paging, and a number box
            // apply search... by putting in text, will search for the options related to that text

            chgSet.append("text").classed('tools', true).classed('icon', true).attr("text-anchor", "left").attr("dx", -(x / 2) + 4).attr("dy", function (d) {
                return 18 - (y + d.values.length * rowHeight) / 2;
            }).text(function (i) {
                return fa('search');
            });

            chgSet.append("text").classed('tools', true).classed('icon', true).attr("text-anchor", "right").attr("dx", x / 2 - 20 - 30).attr("dy", function (d) {
                return 18 - (y + d.values.length * rowHeight) / 2;
            }).text(function (i) {
                return fa('backward');
            });

            chgSet.append("text").classed('tools', true).classed('icon', true).attr("text-anchor", "right").attr("dx", x / 2 - 20 - 15).attr("dy", function (d) {
                return 18 - (y + d.values.length * rowHeight) / 2;
            }).text(function (i) {
                return fa('forward');
            });

            [1, 2, 3, 4, 5].forEach(function (a, i) {
                chgSet.append("text").classed('tools', true).classed('icon', true).classed('xs', true).attr("text-anchor", "left").attr("dx", -(x / 2) + 6 + 15 + i * 10).attr("dy", function (d) {
                    return 17.5 - (y + d.values.length * rowHeight) / 2;
                }).text(function (i) {
                    return fa('circle');
                });
            });

            // page x / y
            chgSet.append("text").classed('tools', true).attr("text-anchor", "right").attr("dx", x / 2 - 20).attr("dy", function (d) {
                return 18 - (y + d.values.length * rowHeight) / 2;
            }).text(function (i) {
                return 1 + " / " + 20;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._dimension{fill:#fff}._dimension rect{stroke-width:1em;stroke:#999}", undefined);

var Shape$10 = function () {
  function Shape(domainType) {
    classCallCheck(this, Shape);

    this.domainType = domainType;
  }

  createClass(Shape, [{
    key: 'update',
    value: function update(fullSet) {}
  }, {
    key: 'build',
    value: function build(chgSet) {
      var x = 100;
      var y = 50;

      chgSet.append("rect").attr("x", -x / 2).attr("y", -y / 2).attr("width", x).attr("height", y);
    }
  }]);
  return Shape;
}();

__$styleInject("._box{fill:blue}._box rect{stroke-width:0px;stroke:#999}._box text{fill:#fff;stroke:#fff;stroke-width:0;font-family:Open Sans Condensed,sans-serif}._box text.label{font-size:.85em}._box circle.option_x{fill:#fff}._box text.option_x{fill:#000;font-family:Open Sans Condensed,sans-serif}._box .icon{font-family:FontAwesome;fill:#fff;stroke-width:0;font-size:1em}._box2{fill:green}", undefined);

var Shape$11 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var x = 100;
            var y = 50;

            chgSet.append("rect").attr("x", -x / 2).attr("y", -y / 2).attr("width", x).attr("height", y);

            chgSet.append("text").attr("class", function (d) {
                return d.class ? d.class : "label";
            }).attr("text-anchor", "middle").attr("dominant-baseline", "central").attr("dx", 0).attr("dy", 0).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", x / 2 - 12).attr("dy", -(y / 2) + 15).text(function (d) {
                return fa(d.icon);
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._tab rect{fill:#000;stroke-width:.08em;stroke:red}._tab text.label{fill:#fff;font-family:Open Sans,sans-serif;font-size:1.4em}", undefined);

var Shape$12 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: "build",
        value: function build(chgSet) {
            var self = this;

            chgSet.classed("tab", true);

            // Width calculated by the layout
            chgSet.append("rect").attr("x", -200 / 2).attr("y", -50 / 2).attr("height", 50).attr("width", 200);

            var textNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            textNode.append("tspan").attr("x", 0).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines[0];
            });

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var bbox = this.parentNode.getBBox();return bbox.height;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? "" : lines[1];
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._swimlane{margin:5px;fill:#fff;stroke:#333;stroke-width:.2;x-stroke-dasharray:5.5}._swimlane rect{opacity:.6}._swimlane .laneLabel{fill:#eca50d}._swimlane text{font-family:Open Sans;font-size:.8em;fill:#000;font-weight:700}", undefined);

__$styleInject("._default{fill:#000;stroke:#333;stroke-width:.2;opacity:.7}text.label-xl{font-size:140%}text.label-l{font-size:120%}text.label-s{font-size:80%}text.label-xs{font-size:60%}div.content td.key{font-weight:700;text-align:right;width:50%;padding:5px}div.content td.key,div.content td.value{font-size:.8em;text-transform:uppercase;border-bottom:1px solid #ccc}div.content{background-color:#fff;border:1px solid #999;font-family:Arial;font-size:.85em;border-radius:5px;padding-bottom:5px}div.content div{padding:5px}.icon{font-family:FontAwesome!important}g._straight .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._straight text.linkText{fill:#fff;font-size:.7em}g._linear .link{fill:none;stroke:#7bb9fe;stroke-width:1px}g._step-before .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._step-before text.linkText{fill:#fff;font-size:.7em}g._step-after .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._step-after text.linkText{fill:#fff;font-size:.7em}g._link2 path.link{fill:none;stroke:#90ee90;stroke-width:2px;stroke-dasharray:(6,3)}g._link2 circle.circleSource,g._link2 circle.circleTarget{fill:#90ee90}", undefined);

var Layout$1 = function () {
    function Layout(domainType) {
        classCallCheck(this, Layout);

        this.domainType = domainType;
    }

    createClass(Layout, [{
        key: "enrichData",
        value: function enrichData() {}
    }, {
        key: "organize",
        value: function organize(d3visual, zone, root) {}
    }, {
        key: "configEvents",
        value: function configEvents(d3visual, newNodes, zones, viewer) {
            var self = this;

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                //const set = vis.select("g.nodeSet").selectAll("g.node");
                if (viewer.downX == Math.round(e.x + e.y)) {
                    //let matchedItem = set.filter(function (d) { return (e.index == d.index); });
                    viewer.navigation.toggle(e, d3.select(this));
                } else {
                    viewer.navigation.close(e);
                }
            });

            function dragstarted(d) {
                d3.select(this).classed("fixed", d.fixed == true);
                if (!d3.event.active) force.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
                viewer.downX = Math.round(d.x + d.y);
            }

            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function dragended(d) {
                if (!d3.event.active) force.alphaTarget(0);

                if (!d.fixed) {
                    d.fx = null;
                    d.fy = null;
                }
                setTimeout(function () {
                    viewer.trigger('change');
                }, 200);
            }

            newNodes.call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

            //let clickable = newNodes.filter(function(d) { return d3.select(this).classed('clickable'); });
            var clickable = newNodes.selectAll('.clickable');
            clickable.on('click', function (e, i) {
                d3.event.stopPropagation();viewer.trigger("shapeClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */
            });
        }
    }, {
        key: "getDim",
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: "build",
        value: function build(d3visual, zone, root, viewer) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            obj.append('rect');

            this.refreshSize(viewer, zone, obj);
        }
    }, {
        key: "refreshSize",
        value: function refreshSize(viewer, zone, zoneNode) {
            var d3visual = viewer.vis;

            this.calculateRectangle(d3visual, zone);

            var rect = zoneNode.select("rect").style("filter", "url(#drop-shadow)").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]);
        }
    }, {
        key: "calculateRectangle",
        value: function calculateRectangle(d3visual, zone) {
            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            console.log("_default: Resizing zone : W=" + viewWidth + ",H=" + viewHeight);

            var pageWidth = d3visual.attr("width");
            var pageHeight = d3visual.attr("height");
            // THis needs to be a calculation based on a ratio

            var pageHeightSet = typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1;

            var rectDimension = [];

            for (var v = 0; v < 4; v++) {
                if (typeof zone.rectangle[v] == "string") {
                    //console.log("Evaluating: " + zone.rectangle[v]);
                    //console.log(" ... to : " + eval(zone.rectangle[v]));
                    rectDimension.push(eval(zone.rectangle[v]));
                } else {
                    rectDimension.push(zone.rectangle[v]);
                }
            }
            if (pageHeightSet) {
                rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
            }

            zone.calculatedRectangle = rectDimension;
            console.log("_default: Resized zone : W=" + rectDimension[2] + ",H=" + rectDimension[3]);
        }
    }]);
    return Layout;
}();

var Layout = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            //      let vb = d3visual.attr("viewBox").split(' ');
            //
            //      let viewWidth = vb[2];
            //      let viewHeight = vb[3];
            //
            //      console.log("_swimlane: Resizing zone : W="+viewWidth+",H="+viewHeight);
            //
            //      let pageWidth = d3visual.attr("width");
            //      let pageHeight = d3visual.attr("height");
            //      // THis needs to be a calculation based on a ratio
            //
            //      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
            //
            //      let rectDimension = [];
            //
            //      for ( let v = 0; v < 4; v++) {
            //          if (typeof zone.rectangle[v] == "string") {
            //            //console.log("Evaluating: " + zone.rectangle[v]);
            //            //console.log(" ... to : " + eval(zone.rectangle[v]));
            //            rectDimension.push(eval(zone.rectangle[v]));
            //          } else {
            //            rectDimension.push(zone.rectangle[v]);
            //          }
            //      }
            //      zone.calculatedRectangle = rectDimension;
            //
            //      if (pageHeightSet) {
            //          rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
            //      }

            this.calculateRectangle(d3visual, zone);

            var rect = obj.append("rect")
            //.style("filter", "url(#drop-shadow)")
            .attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]).attr("class", "lane"); //fff899

            obj.append("rect").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", function (d) {
                return zone.orientation == "horizontal" ? 80 : 40;
            }).attr("height", zone.calculatedRectangle[3]).attr("class", "laneLabel"); //fff899

            var transform = "translate(" + zone.calculatedRectangle[0] + "," + zone.calculatedRectangle[1] + ") rotate(-90) translate(" + -(zone.calculatedRectangle[3] / 2) + "," + "15" + ")";
            if (zone.orientation == "horizontal") {
                transform = "translate(" + (zone.calculatedRectangle[0] + 80 / 2) + "," + (zone.calculatedRectangle[1] + zone.calculatedRectangle[3] / 2) + ")";
            }

            var textNodes = obj.append("text").attr("transform", transform).attr("text-anchor", "middle")
            //.attr("dominant-baseline", "central")
            .attr("dx", 0).attr("dy", 0);

            textNodes.append("tspan").attr("x", 0).attr("y", 0).text(function (d) {
                var lines = self.splitCaption(zone.title, 20);return lines[0];
            });

            textNodes.append("tspan").attr("x", 0).attr("y", 15).text(function (d) {
                var lines = self.splitCaption(zone.title, 20);return lines.length == 1 ? "" : lines[1];
            });

            if (zone.hasOwnProperty("image")) {
                transform = "translate(" + (zone.calculatedRectangle[0] + 10) + "," + (zone.calculatedRectangle[1] + zone.calculatedRectangle[3] / 2 - 60) + ")";

                obj.append("image").attr("width", function (d) {
                    return 60;
                }).attr("height", function (d) {
                    return 40;
                }).attr("xlink:href", function (d) {
                    return zone.image;
                }).attr("transform", transform);
            }
            /*
                  let box = null;
            
                  rect.each (function (d) {
                    box = this.getBBox();
                  });
            
                      */
        }
    }, {
        key: 'splitCaption',
        value: function splitCaption(caption) {
            var MAXIMUM_CHARS_PER_LINE = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 6;


            var words = caption.split(' ');
            var line$$1 = "";

            var lines = [];
            for (var n = 0; n < words.length; n++) {
                var testLine = line$$1 + words[n] + " ";
                if (testLine.length > MAXIMUM_CHARS_PER_LINE && line$$1.length > 0) {
                    lines.push(line$$1);

                    line$$1 = words[n] + " ";
                } else {
                    line$$1 = testLine;
                }
            }
            lines.push(line$$1);

            return lines;
        }
    }]);
    return Layout;
}(Layout$1);

__$styleInject("._tab text.option_x{fill:#fff;font-size:1em}.selected_tab rect{fill:red}.page{position:absolute;background-color:pink;padding:0;margin:0}.page svg{fill:#fff}", undefined);

var Layout$2 = function () {
    function Layout(domainType) {
        classCallCheck(this, Layout);

        this.domainType = domainType;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root, viewer) {
            var self = this;
            viewer.on("tabClick", function (event$$1, e) {
                var tabsToOpen = [];
                var tabToOpen = e.name;
                while (tabToOpen != null) {
                    var tabNode = d3visual.selectAll("g.tab").filter(function (d) {
                        return d.name == tabToOpen;
                    });
                    if (tabNode.classed("selected_tab") == false || e.name == tabNode.datum().name) {
                        tabsToOpen.push(tabNode);
                    }
                    tabToOpen = tabNode.datum().layout.parent;
                }
                var to = tabsToOpen.reverse();
                for (var ind in to) {
                    var clickedObject = to[ind];
                    self.toggleTab(viewer, clickedObject.datum(), clickedObject.node());
                }
            });
        }
    }, {
        key: 'enrichData',
        value: function enrichData(nodes) {
            //
            //      let segs = [];
            //      for ( let x = 0 ; x < graph.nodes.length; x++) {
            //          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //          if (seg != "" && segs.indexOf(seg) < 0) {
            //              segs.push(seg);
            //          }
            //      }
            var badNodes = [];
            var fullNodeList = [];

            this.recurse(null, nodes, "", 0, badNodes, fullNodeList);

            //      // calculate the segment layout
            //      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
            //      for ( let p = 0; p < segs.length; p++) {
            //          let tot = 0;
            //          let x,num;
            //          for ( x = 0 ; x < graph.nodes.length; x++) {
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //              if (seg == segs[p]) {
            //                  tot++;
            //              }
            //              if (graph.nodes[x].children) {
            //                  let ls = graph.nodes[x].children;
            //                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            //                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            //                      if (seg == segs[p]) {
            //                        tot++;
            //                      }
            //
            //                      if (ls[xL1].children) {
            //                          let lsL2 = ls[xL1].children;
            //                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            //                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            //                              if (seg == segs[p]) {
            //                                tot++;
            //                              }
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
            ////              this.recurse(graph.nodes, 1);
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            ////              if (seg == segs[p]) {
            ////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
            ////                  num++;
            ////              }
            ////              this.recurse(graph.nodes[x], 1);
            ////              if (graph.nodes[x].children) {
            ////                  let ls = graph.nodes[x].children;
            ////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            ////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            ////                      if (seg == segs[p]) {
            ////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
            ////                          numL1++;
            ////                      }
            ////                      this.recurse(ls[xL1], 2);
            //
            ////                      if (ls[xL1].children) {
            ////                          let lsL2 = ls[xL1].children;
            ////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            ////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            ////                              if (seg == segs[p]) {
            ////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
            ////                                  numL2++;
            ////                              }
            ////                          }
            ////                      }
            //
            //           //       }
            //            //  }
            //          }
            //      }
        }
    }, {
        key: 'recurse',
        value: function recurse(parent, nodes, rootTag, level, badNodes, fullNodeList) {
            var tot = 0;
            for (var num = 0, x = 0; x < nodes.length; x++) {
                var nd = nodes[x];
                if (nd.type == "tab") {
                    tot++;
                }
            }
            for (var _num = 0, _x = 0; _x < nodes.length; _x++) {
                var _nd = nodes[_x];
                if (_nd.type == "tab") {
                    _nd.layout = { "seq": _num, "total": tot, "seg": rootTag, "segnum": level, "level": level, "parent": parent == null ? null : parent.name };
                    _num++;

                    if (_nd.hasOwnProperty("name") == false) {
                        alert("Warning: Tab nodes require name " + JSON.stringify(_nd));
                        badNodes.push(_nd);
                    } else if (_nd.name in fullNodeList) {
                        alert("Warning: Tab node names must be unique " + JSON.stringify(_nd));
                        badNodes.push(_nd);
                    } else {
                        fullNodeList[_nd.name] = _nd;
                    }

                    if (_nd.children) {
                        this.recurse(_nd, _nd.children, rootTag + "_L" + level + "T" + (_num - 1), level + 1, badNodes, fullNodeList);
                    }
                }
            }
        }
    }, {
        key: 'configEvents',
        value: function configEvents(d3visual, newNodesTotal, zones, viewer) {
            var self = this;

            var newNodes = newNodesTotal.filter(function (d) {
                return d.type == 'tab';
            });

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                viewer.trigger("tabClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */
            });

            // NO DRAG/DROP support for tabs
            function dragstarted(d) {
                viewer.downX = Math.round(d.x + d.y);
            }
            newNodes.call(d3.drag().on("start", dragstarted));

            self.attachInfoController(viewer, newNodes);
        }
    }, {
        key: 'attachInfoController',
        value: function attachInfoController(viewer, chgSet) {
            var self = this;

            chgSet.filter(function (d) {
                return d.hasOwnProperty("notes");
            }).append("text").attr("text-anchor", "middle").text(function (d) {
                return fa('info-circle');
            }).classed("decoration", true).classed("option_x", true).classed("icon", true).on("click", function (e) {
                self.toggleNavigation(viewer, e, this.parentNode);
            });
        }
    }, {
        key: 'getDim',
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {

            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];

            var maxWidth = viewWidth - 0;

            d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
                var index = d.boundary.charCodeAt(0) - 65;
                return index == zoneIndex;
            }).each(function (d) {
                d.x = (maxWidth / d.layout.total - 5) / 2 + d.layout.seq * (maxWidth / d.layout.total);
                d.y = 50 / 2 + d.layout.segnum * 55;
                d3.select(this).select("rect").attr("x", function (d) {
                    return -(maxWidth / d.layout.total - 5) / 2;
                }).attr("y", function (d) {
                    return -d3.select(this).attr("height") / 2;
                }).attr("width", function (d) {
                    return maxWidth / d.layout.total;
                });

                d3.select(this).select("text.option_x").attr("transform", function (a) {
                    return "translate(" + (maxWidth / d.layout.total / 2 - 15) + ", -10)";
                });

                d3.select(this).select("text").attr("transform", function (a) {
                    var bbox = this.getBBox();
                    var pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
                    var scale = 1;
                    if (pbbox.height / bbox.height < pbbox.width / (bbox.width + 20) && pbbox.height / bbox.height < 1) {
                        scale = pbbox.height / bbox.height == 0 ? 1 : pbbox.height / bbox.height;
                    } else {
                        scale = Math.min(pbbox.width / (bbox.width + 20), 1) == 0 ? 1 : Math.min(pbbox.width / (bbox.width + 20), 1);
                    }
                    return "scale(" + scale + ")";
                });
            });

            d3visual.select("g.nodeSet").selectAll("g.tab").filter(function (d) {
                return d.layout.level != 0;
            }).style("opacity", 0).style("display", "none");

            /*
                   //maxWidth = 800;
                   d3visual.select("g.nodeSet").selectAll("g.nodec").filter(function (d) {
                       const index = d.boundary.charCodeAt(0) - 65;
                       return (index == zoneIndex);
                   })
                   .attr("transform", function(d) {
                        let offsetX = d.layout.seq;
                        return "translate(" +(((maxWidth/d.layout.total)/2)+(offsetX * (maxWidth/d.layout.total))) + ", " + (50+(d.layout.level * 52)) + ")"; } )
                   .each(function (d) {
                       d.x = ((maxWidth/d.layout.total)/2) + (d.layout.seq * (maxWidth/d.layout.total));
                       d.y = 50 + (d.layout.segnum * 55);
            
            //           if (!d.layout) {
            //                d.layout = {"segnum":2, "total":3};
            //           }
                       //d.x = 105 + (self.aa[d.layout.segnum] * (maxWidth/d.layout.total));
                       //d.y = 70 + (d.layout.segnum * 75);
            //           self.aa[d.layout.segnum] = self.aa[d.layout.segnum] + 1;
            //           d3.select(this).attr("transform", "translate(" + d.x + "," + d.y + ")");
                      d3.select(this).select("rect")
                        .attr("x", function (d) { return -((maxWidth/d.layout.total) - 5)/2; } )
                        .attr("y", function (d) { return -d3.select(this).attr("height") / 2; } )
                        .attr("width", function (d) { return (maxWidth/d.layout.total) - 5; } );
            
                      d3.select(this).select("text.option_x").attr("transform", function(a) {
                        return "translate(" + (((maxWidth/d.layout.total)/2)-15) + ", -10)";
                      });
            
                      d3.select(this).select("text").attr("transform", function(a) {
                        const bbox = this.getBBox(); const pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
                        return "scale(" + Math.min(pbbox.width / (bbox.width + 20), 1) + ")";
                      });
                      //d3.select(this).select("rect").attr("width", (self.width - 5));
                   });
                   */
        }
    }, {
        key: 'toggleNavigation',
        value: function toggleNavigation(viewer, e, object) {
            d3.event.stopPropagation();
            if (viewer.downX == Math.round(e.x + e.y)) {
                viewer.navigation.toggle(e, d3.select(object));
            } else {
                viewer.navigation.close(e);
            }
        }
    }, {
        key: 'toggleTab',
        value: function toggleTab(viewer, d, currentTab) {
            var self = this;

            //d3.event.stopPropagation();

            // get the child tabs
            var children = d3.select(currentTab.parentNode).selectAll("g.node_" + d.name);

            if (children.size() > 0) {

                var toggleOn = children.style("display") == "none";

                this.turnOffPreviousTab(d, currentTab);

                //         if (toggleOn) {
                children.style("opacity", 0).transition().duration(1000).style("opacity", 1).style("display", "block");
                d3.select(currentTab).classed("selected_tab", true);
                //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "-"; });
                //         } else {
                //children.transition().duration(1000).style("opacity", 0).style("display","none"); //.each("end", function (e) { d3.select(this).style("display","none")
                //d3.select(currentTab).classed("selected_tab", false);
                //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "+"; });
                //         }
            } else {

                // if there are no child tabs, look for other content, such as another svg/screenshot/etc
                this.turnOffPreviousTab(d, currentTab);

                if (d3.select(".page").size() == 0) {
                    this.turnOffPreviousTab(d, currentTab);
                    d3.select(currentTab).classed("selected_tab", true);

                    if (!d.content) {
                        var content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);
                        content.style("top", "0px");
                        content.html("no content");
                        return;
                    }

                    var url = d.content.url.indexOf('?') == -1 ? d.content.url + "?r=" + Math.random() : d.content.url + "&r=" + Math.random();
                    console.log("Opening: " + url);
                    d3.json(url, function (json$$1) {

                        var content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);

                        //content.classed("level_" + d.layout.level, "true");

                        //content.style("overflow", "hidden");

                        self.refreshSize(viewer);

                        var vc = new ViewerCanvas();
                        vc.on("change", function (event$$1, _irrelevant_data, doc) {
                            viewer.trigger(event$$1, d, doc);
                        });
                        vc.mountNode(content.node());
                        vc.setState(json$$1);
                        vc.render();
                        vc.refreshSize();

                        self.vc = vc;
                    });
                }
            }
        }
    }, {
        key: 'refreshSize',
        value: function refreshSize(viewer) {
            var vb = d3.select(viewer.domNode).attr("viewBox").split(' ');

            var tab = d3.select(viewer.domNode).select('g.nodeSet').node().getBoundingClientRect();
            var bodyRect = document.body.getBoundingClientRect();

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            var pageWidth = d3.select(viewer.domNode).attr("width");
            var pageHeight = d3.select(viewer.domNode).attr("height");

            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;

            var top = Math.ceil(tab.top - bodyRect.top + tab.height + 2);

            var full = { "height": pageHeight, "width": pageWidth };

            var content = d3.select(viewer.domNode.parentNode).select('.page');

            content.style("top", top + "px");
            content.style("left", "0px");
            content.style("width", full.width + "px");
            content.style("height", full.height - top + "px");

            if (this.vc) {
                this.vc.refreshSize();
            }
        }
    }, {
        key: 'turnOffPreviousTab',
        value: function turnOffPreviousTab(tabData, currentTab) {
            // turn off any previously selected tab
            d3.select(currentTab.parentNode).selectAll("g.selected_tab").each(function (p) {
                // For all tabs that are selected at new tab level or higher (lower in position)
                if (tabData.layout.level <= p.layout.level) {
                    d3.select(this).classed("selected_tab", false);
                    var selectedChildren = d3.select(currentTab.parentNode).selectAll("g.node_" + p.name);
                    selectedChildren.transition().style("opacity", 0).style("display", "none");
                }
            });
            //d3.select(currentTab).classed("selected_tab", false);

            //     let self = this;
            //     if (typeof self.vc != "undefined" && self.vc != null) {
            //        console.log("destroy");
            //        self.vc.destroy();
            //        self.vc = null;
            //     }
            //
            //d3.select("#base").selectAll(".page").selectAll().remove();
            d3.select(currentTab.parentNode.parentNode.parentNode).selectAll(".page").remove();
        }
    }]);
    return Layout;
}();

__$styleInject("._timelines .background{fill:#fff}._timelines text{fill:#000;font-size:.7em;font-weight:700;font-family:Open Sans}._timelines .sat,.m2,.sun{fill:#add8e6}._timelines .mon,.fri,.thu,.tue,.wed{opacity:.7;fill:#fff}._timelines .jan,.jul,.mar,.may,.nov,.q1,.q3,.sep{fill:#add8e6;stroke-width:0}._timelines .feb,.apr,.aug,.dec,.jun,.oct,.q2,.q4{fill:#fff}._timelines .level1{fill:#000}._timelines .banner{fill:#fff;font-size:.6em}", undefined);

var hookCallback;

function hooks () {
    return hookCallback.apply(null, arguments);
}

// This is done to register the method called with moment()
// without creating circular dependencies.
function setHookCallback (callback) {
    hookCallback = callback;
}

function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
}

function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
}

function isObjectEmpty(obj) {
    var k;
    for (k in obj) {
        // even if its not own property I'd still call it non-empty
        return false;
    }
    return true;
}

function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
}

function map(arr, fn) {
    var res = [], i;
    for (i = 0; i < arr.length; ++i) {
        res.push(fn(arr[i], i));
    }
    return res;
}

function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}

function extend(a, b) {
    for (var i in b) {
        if (hasOwnProp(b, i)) {
            a[i] = b[i];
        }
    }

    if (hasOwnProp(b, 'toString')) {
        a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
        a.valueOf = b.valueOf;
    }

    return a;
}

function createUTC (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}

function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
        empty           : false,
        unusedTokens    : [],
        unusedInput     : [],
        overflow        : -2,
        charsLeftOver   : 0,
        nullInput       : false,
        invalidMonth    : null,
        invalidFormat   : false,
        userInvalidated : false,
        iso             : false,
        parsedDateParts : [],
        meridiem        : null
    };
}

function getParsingFlags(m) {
    if (m._pf == null) {
        m._pf = defaultParsingFlags();
    }
    return m._pf;
}

var some;
if (Array.prototype.some) {
    some = Array.prototype.some;
} else {
    some = function (fun) {
        var t = Object(this);
        var len = t.length >>> 0;

        for (var i = 0; i < len; i++) {
            if (i in t && fun.call(this, t[i], i, t)) {
                return true;
            }
        }

        return false;
    };
}

function isValid(m) {
    if (m._isValid == null) {
        var flags = getParsingFlags(m);
        var parsedParts = some.call(flags.parsedDateParts, function (i) {
            return i != null;
        });
        var isNowValid = !isNaN(m._d.getTime()) &&
            flags.overflow < 0 &&
            !flags.empty &&
            !flags.invalidMonth &&
            !flags.invalidWeekday &&
            !flags.nullInput &&
            !flags.invalidFormat &&
            !flags.userInvalidated &&
            (!flags.meridiem || (flags.meridiem && parsedParts));

        if (m._strict) {
            isNowValid = isNowValid &&
                flags.charsLeftOver === 0 &&
                flags.unusedTokens.length === 0 &&
                flags.bigHour === undefined;
        }

        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        }
        else {
            return isNowValid;
        }
    }
    return m._isValid;
}

function createInvalid (flags) {
    var m = createUTC(NaN);
    if (flags != null) {
        extend(getParsingFlags(m), flags);
    }
    else {
        getParsingFlags(m).userInvalidated = true;
    }

    return m;
}

function isUndefined(input) {
    return input === void 0;
}

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
var momentProperties = hooks.momentProperties = [];

function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
        to._isAMomentObject = from._isAMomentObject;
    }
    if (!isUndefined(from._i)) {
        to._i = from._i;
    }
    if (!isUndefined(from._f)) {
        to._f = from._f;
    }
    if (!isUndefined(from._l)) {
        to._l = from._l;
    }
    if (!isUndefined(from._strict)) {
        to._strict = from._strict;
    }
    if (!isUndefined(from._tzm)) {
        to._tzm = from._tzm;
    }
    if (!isUndefined(from._isUTC)) {
        to._isUTC = from._isUTC;
    }
    if (!isUndefined(from._offset)) {
        to._offset = from._offset;
    }
    if (!isUndefined(from._pf)) {
        to._pf = getParsingFlags(from);
    }
    if (!isUndefined(from._locale)) {
        to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
        for (i in momentProperties) {
            prop = momentProperties[i];
            val = from[prop];
            if (!isUndefined(val)) {
                to[prop] = val;
            }
        }
    }

    return to;
}

var updateInProgress = false;

// Moment prototype object
function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);
    if (!this.isValid()) {
        this._d = new Date(NaN);
    }
    // Prevent infinite loop in case updateOffset creates new moment
    // objects.
    if (updateInProgress === false) {
        updateInProgress = true;
        hooks.updateOffset(this);
        updateInProgress = false;
    }
}

function isMoment (obj) {
    return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
}

function absFloor (number) {
    if (number < 0) {
        // -0 -> 0
        return Math.ceil(number) || 0;
    } else {
        return Math.floor(number);
    }
}

function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
        value = absFloor(coercedNumber);
    }

    return value;
}

// compare two arrays, return the number of differences
function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;
    for (i = 0; i < len; i++) {
        if ((dontConvert && array1[i] !== array2[i]) ||
            (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
            diffs++;
        }
    }
    return diffs + lengthDiff;
}

function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false &&
            (typeof console !==  'undefined') && console.warn) {
        console.warn('Deprecation warning: ' + msg);
    }
}

function deprecate(msg, fn) {
    var firstTime = true;

    return extend(function () {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(null, msg);
        }
        if (firstTime) {
            var args = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = '';
                if (typeof arguments[i] === 'object') {
                    arg += '\n[' + i + '] ';
                    for (var key in arguments[0]) {
                        arg += key + ': ' + arguments[0][key] + ', ';
                    }
                    arg = arg.slice(0, -2); // Remove trailing comma and space
                } else {
                    arg = arguments[i];
                }
                args.push(arg);
            }
            warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
            firstTime = false;
        }
        return fn.apply(this, arguments);
    }, fn);
}

var deprecations = {};

function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(name, msg);
    }
    if (!deprecations[name]) {
        warn(msg);
        deprecations[name] = true;
    }
}

hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;

function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
}

function set$1 (config) {
    var prop, i;
    for (i in config) {
        prop = config[i];
        if (isFunction(prop)) {
            this[i] = prop;
        } else {
            this['_' + i] = prop;
        }
    }
    this._config = config;
    // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _ordinalParseLenient.
    this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + (/\d{1,2}/).source);
}

function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig), prop;
    for (prop in childConfig) {
        if (hasOwnProp(childConfig, prop)) {
            if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                res[prop] = {};
                extend(res[prop], parentConfig[prop]);
                extend(res[prop], childConfig[prop]);
            } else if (childConfig[prop] != null) {
                res[prop] = childConfig[prop];
            } else {
                delete res[prop];
            }
        }
    }
    for (prop in parentConfig) {
        if (hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])) {
            // make sure changes to properties don't modify parent config
            res[prop] = extend({}, res[prop]);
        }
    }
    return res;
}

function Locale(config) {
    if (config != null) {
        this.set(config);
    }
}

var keys;

if (Object.keys) {
    keys = Object.keys;
} else {
    keys = function (obj) {
        var i, res = [];
        for (i in obj) {
            if (hasOwnProp(obj, i)) {
                res.push(i);
            }
        }
        return res;
    };
}

var defaultCalendar = {
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    nextWeek : 'dddd [at] LT',
    lastDay : '[Yesterday at] LT',
    lastWeek : '[Last] dddd [at] LT',
    sameElse : 'L'
};

function calendar (key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
}

var defaultLongDateFormat = {
    LTS  : 'h:mm:ss A',
    LT   : 'h:mm A',
    L    : 'MM/DD/YYYY',
    LL   : 'MMMM D, YYYY',
    LLL  : 'MMMM D, YYYY h:mm A',
    LLLL : 'dddd, MMMM D, YYYY h:mm A'
};

function longDateFormat (key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
        return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
        return val.slice(1);
    });

    return this._longDateFormat[key];
}

var defaultInvalidDate = 'Invalid date';

function invalidDate () {
    return this._invalidDate;
}

var defaultOrdinal = '%d';
var defaultOrdinalParse = /\d{1,2}/;

function ordinal (number) {
    return this._ordinal.replace('%d', number);
}

var defaultRelativeTime = {
    future : 'in %s',
    past   : '%s ago',
    s  : 'a few seconds',
    m  : 'a minute',
    mm : '%d minutes',
    h  : 'an hour',
    hh : '%d hours',
    d  : 'a day',
    dd : '%d days',
    M  : 'a month',
    MM : '%d months',
    y  : 'a year',
    yy : '%d years'
};

function relativeTime (number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return (isFunction(output)) ?
        output(number, withoutSuffix, string, isFuture) :
        output.replace(/%d/i, number);
}

function pastFuture (diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
}

var aliases = {};

function addUnitAlias (unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
}

function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
}

function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
        if (hasOwnProp(inputObject, prop)) {
            normalizedProp = normalizeUnits(prop);
            if (normalizedProp) {
                normalizedInput[normalizedProp] = inputObject[prop];
            }
        }
    }

    return normalizedInput;
}

var priorities = {};

function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
}

function getPrioritizedUnits(unitsObj) {
    var units = [];
    for (var u in unitsObj) {
        units.push({unit: u, priority: priorities[u]});
    }
    units.sort(function (a, b) {
        return a.priority - b.priority;
    });
    return units;
}

function makeGetSet (unit, keepTime) {
    return function (value) {
        if (value != null) {
            set$2(this, unit, value);
            hooks.updateOffset(this, keepTime);
            return this;
        } else {
            return get$1(this, unit);
        }
    };
}

function get$1 (mom, unit) {
    return mom.isValid() ?
        mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
}

function set$2 (mom, unit, value) {
    if (mom.isValid()) {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
    }
}

// MOMENTS

function stringGet (units) {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
        return this[units]();
    }
    return this;
}


function stringSet (units, value) {
    if (typeof units === 'object') {
        units = normalizeObjectUnits(units);
        var prioritized = getPrioritizedUnits(units);
        for (var i = 0; i < prioritized.length; i++) {
            this[prioritized[i].unit](units[prioritized[i].unit]);
        }
    } else {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units](value);
        }
    }
    return this;
}

function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? (forceSign ? '+' : '') : '-') +
        Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}

var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

var formatFunctions = {};

var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
function addFormatToken (token, padded, ordinal, callback) {
    var func = callback;
    if (typeof callback === 'string') {
        func = function () {
            return this[callback]();
        };
    }
    if (token) {
        formatTokenFunctions[token] = func;
    }
    if (padded) {
        formatTokenFunctions[padded[0]] = function () {
            return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
        };
    }
    if (ordinal) {
        formatTokenFunctions[ordinal] = function () {
            return this.localeData().ordinal(func.apply(this, arguments), token);
        };
    }
}

function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
        return input.replace(/^\[|\]$/g, '');
    }
    return input.replace(/\\/g, '');
}

function makeFormatFunction(format) {
    var array = format.match(formattingTokens), i, length;

    for (i = 0, length = array.length; i < length; i++) {
        if (formatTokenFunctions[array[i]]) {
            array[i] = formatTokenFunctions[array[i]];
        } else {
            array[i] = removeFormattingTokens(array[i]);
        }
    }

    return function (mom) {
        var output = '', i;
        for (i = 0; i < length; i++) {
            output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
        }
        return output;
    };
}

// format date using native date object
function formatMoment(m, format) {
    if (!m.isValid()) {
        return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    return formatFunctions[format](m);
}

function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
        return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;
    while (i >= 0 && localFormattingTokens.test(format)) {
        format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
        localFormattingTokens.lastIndex = 0;
        i -= 1;
    }

    return format;
}

var match1         = /\d/;            //       0 - 9
var match2         = /\d\d/;          //      00 - 99
var match3         = /\d{3}/;         //     000 - 999
var match4         = /\d{4}/;         //    0000 - 9999
var match6         = /[+-]?\d{6}/;    // -999999 - 999999
var match1to2      = /\d\d?/;         //       0 - 99
var match3to4      = /\d\d\d\d?/;     //     999 - 9999
var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
var match1to3      = /\d{1,3}/;       //       0 - 999
var match1to4      = /\d{1,4}/;       //       0 - 9999
var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

var matchUnsigned  = /\d+/;           //       0 - inf
var matchSigned    = /[+-]?\d+/;      //    -inf - inf

var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;


var regexes = {};

function addRegexToken (token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
        return (isStrict && strictRegex) ? strictRegex : regex;
    };
}

function getParseRegexForToken (token, config) {
    if (!hasOwnProp(regexes, token)) {
        return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
}

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
    }));
}

function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

var tokens = {};

function addParseToken (token, callback) {
    var i, func = callback;
    if (typeof token === 'string') {
        token = [token];
    }
    if (isNumber(callback)) {
        func = function (input, array) {
            array[callback] = toInt(input);
        };
    }
    for (i = 0; i < token.length; i++) {
        tokens[token[i]] = func;
    }
}

function addWeekParseToken (token, callback) {
    addParseToken(token, function (input, array, config, token) {
        config._w = config._w || {};
        callback(input, config._w, config, token);
    });
}

function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
        tokens[token](input, config._a, config, token);
    }
}

var YEAR = 0;
var MONTH = 1;
var DATE = 2;
var HOUR = 3;
var MINUTE = 4;
var SECOND = 5;
var MILLISECOND = 6;
var WEEK = 7;
var WEEKDAY = 8;

var indexOf;

if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
} else {
    indexOf = function (o) {
        // I know
        var i;
        for (i = 0; i < this.length; ++i) {
            if (this[i] === o) {
                return i;
            }
        }
        return -1;
    };
}

function daysInMonth(year, month) {
    return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
}

// FORMATTING

addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
});

addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
});

addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
});

// ALIASES

addUnitAlias('month', 'M');

// PRIORITY

addUnitPriority('month', 8);

// PARSING

addRegexToken('M',    match1to2);
addRegexToken('MM',   match1to2, match2);
addRegexToken('MMM',  function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
});
addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
});

addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
});

addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict);
    // if we didn't find a month name, mark the date as invalid.
    if (month != null) {
        array[MONTH] = month;
    } else {
        getParsingFlags(config).invalidMonth = input;
    }
});

// LOCALES

var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
function localeMonths (m, format) {
    if (!m) {
        return this._months;
    }
    return isArray(this._months) ? this._months[m.month()] :
        this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
}

var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
function localeMonthsShort (m, format) {
    if (!m) {
        return this._monthsShort;
    }
    return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
        this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
}

function handleStrictParse(monthName, format, strict) {
    var i, ii, mom, llc = monthName.toLocaleLowerCase();
    if (!this._monthsParse) {
        // this is not used
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
        for (i = 0; i < 12; ++i) {
            mom = createUTC([2000, i]);
            this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
            this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeMonthsParse (monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
        return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
    }

    // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        if (strict && !this._longMonthsParse[i]) {
            this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
        }
        if (!strict && !this._monthsParse[i]) {
            regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            return i;
        } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            return i;
        } else if (!strict && this._monthsParse[i].test(monthName)) {
            return i;
        }
    }
}

// MOMENTS

function setMonth (mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
        // No op
        return mom;
    }

    if (typeof value === 'string') {
        if (/^\d+$/.test(value)) {
            value = toInt(value);
        } else {
            value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (!isNumber(value)) {
                return mom;
            }
        }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
    return mom;
}

function getSetMonth (value) {
    if (value != null) {
        setMonth(this, value);
        hooks.updateOffset(this, true);
        return this;
    } else {
        return get$1(this, 'Month');
    }
}

function getDaysInMonth () {
    return daysInMonth(this.year(), this.month());
}

var defaultMonthsShortRegex = matchWord;
function monthsShortRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsShortStrictRegex;
        } else {
            return this._monthsShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsShortRegex')) {
            this._monthsShortRegex = defaultMonthsShortRegex;
        }
        return this._monthsShortStrictRegex && isStrict ?
            this._monthsShortStrictRegex : this._monthsShortRegex;
    }
}

var defaultMonthsRegex = matchWord;
function monthsRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsStrictRegex;
        } else {
            return this._monthsRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsRegex')) {
            this._monthsRegex = defaultMonthsRegex;
        }
        return this._monthsStrictRegex && isStrict ?
            this._monthsStrictRegex : this._monthsRegex;
    }
}

function computeMonthsParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom;
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        shortPieces.push(this.monthsShort(mom, ''));
        longPieces.push(this.months(mom, ''));
        mixedPieces.push(this.months(mom, ''));
        mixedPieces.push(this.monthsShort(mom, ''));
    }
    // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 12; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
    }
    for (i = 0; i < 24; i++) {
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
}

// FORMATTING

addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
});

addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
});

addFormatToken(0, ['YYYY',   4],       0, 'year');
addFormatToken(0, ['YYYYY',  5],       0, 'year');
addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

addUnitAlias('year', 'y');

// PRIORITIES

addUnitPriority('year', 1);

// PARSING

addRegexToken('Y',      matchSigned);
addRegexToken('YY',     match1to2, match2);
addRegexToken('YYYY',   match1to4, match4);
addRegexToken('YYYYY',  match1to6, match6);
addRegexToken('YYYYYY', match1to6, match6);

addParseToken(['YYYYY', 'YYYYYY'], YEAR);
addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
});

// HELPERS

function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

// HOOKS

hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
};

// MOMENTS

var getSetYear = makeGetSet('FullYear', true);

function getIsLeapYear () {
    return isLeapYear(this.year());
}

function createDate (y, m, d, h, M, s, ms) {
    //can't just apply() to create a date:
    //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
    var date = new Date(y, m, d, h, M, s, ms);

    //the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
        date.setFullYear(y);
    }
    return date;
}

function createUTCDate (y) {
    var date = new Date(Date.UTC.apply(null, arguments));

    //the Date.UTC function remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
    }
    return date;
}

// start-of-first-week - start-of-year
function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
        fwd = 7 + dow - doy,
        // first-week day local weekday -- which local weekday is fwd
        fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

    return -fwdlw + fwd - 1;
}

//http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear, resDayOfYear;

    if (dayOfYear <= 0) {
        resYear = year - 1;
        resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
        resYear = year + 1;
        resDayOfYear = dayOfYear - daysInYear(year);
    } else {
        resYear = year;
        resDayOfYear = dayOfYear;
    }

    return {
        year: resYear,
        dayOfYear: resDayOfYear
    };
}

function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek, resYear;

    if (week < 1) {
        resYear = mom.year() - 1;
        resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
        resWeek = week - weeksInYear(mom.year(), dow, doy);
        resYear = mom.year() + 1;
    } else {
        resYear = mom.year();
        resWeek = week;
    }

    return {
        week: resWeek,
        year: resYear
    };
}

function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}

// FORMATTING

addFormatToken('w', ['ww', 2], 'wo', 'week');
addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

addUnitAlias('week', 'w');
addUnitAlias('isoWeek', 'W');

// PRIORITIES

addUnitPriority('week', 5);
addUnitPriority('isoWeek', 5);

// PARSING

addRegexToken('w',  match1to2);
addRegexToken('ww', match1to2, match2);
addRegexToken('W',  match1to2);
addRegexToken('WW', match1to2, match2);

addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
});

// HELPERS

// LOCALES

function localeWeek (mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
}

var defaultLocaleWeek = {
    dow : 0, // Sunday is the first day of the week.
    doy : 6  // The week that contains Jan 1st is the first week of the year.
};

function localeFirstDayOfWeek () {
    return this._week.dow;
}

function localeFirstDayOfYear () {
    return this._week.doy;
}

// MOMENTS

function getSetWeek (input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
}

function getSetISOWeek (input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
}

// FORMATTING

addFormatToken('d', 0, 'do', 'day');

addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
});

addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
});

addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
});

addFormatToken('e', 0, 0, 'weekday');
addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

addUnitAlias('day', 'd');
addUnitAlias('weekday', 'e');
addUnitAlias('isoWeekday', 'E');

// PRIORITY
addUnitPriority('day', 11);
addUnitPriority('weekday', 11);
addUnitPriority('isoWeekday', 11);

// PARSING

addRegexToken('d',    match1to2);
addRegexToken('e',    match1to2);
addRegexToken('E',    match1to2);
addRegexToken('dd',   function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
});
addRegexToken('ddd',   function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
});
addRegexToken('dddd',   function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
});

addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict);
    // if we didn't get a weekday name, mark the date as invalid
    if (weekday != null) {
        week.d = weekday;
    } else {
        getParsingFlags(config).invalidWeekday = input;
    }
});

addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
});

// HELPERS

function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
        return input;
    }

    if (!isNaN(input)) {
        return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);
    if (typeof input === 'number') {
        return input;
    }

    return null;
}

function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
        return locale.weekdaysParse(input) % 7 || 7;
    }
    return isNaN(input) ? null : input;
}

// LOCALES

var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
function localeWeekdays (m, format) {
    if (!m) {
        return this._weekdays;
    }
    return isArray(this._weekdays) ? this._weekdays[m.day()] :
        this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
}

var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
function localeWeekdaysShort (m) {
    return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}

var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
function localeWeekdaysMin (m) {
    return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}

function handleStrictParse$1(weekdayName, format, strict) {
    var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._minWeekdaysParse = [];

        for (i = 0; i < 7; ++i) {
            mom = createUTC([2000, 1]).day(i);
            this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
            this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
            this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeWeekdaysParse (weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
        return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._minWeekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already

        mom = createUTC([2000, 1]).day(i);
        if (strict && !this._fullWeekdaysParse[i]) {
            this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
            this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
            this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
        }
        if (!this._weekdaysParse[i]) {
            regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
            return i;
        }
    }
}

// MOMENTS

function getSetDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    if (input != null) {
        input = parseWeekday(input, this.localeData());
        return this.add(input - day, 'd');
    } else {
        return day;
    }
}

function getSetLocaleDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
}

function getSetISODayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }

    // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.

    if (input != null) {
        var weekday = parseIsoWeekday(input, this.localeData());
        return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
        return this.day() || 7;
    }
}

var defaultWeekdaysRegex = matchWord;
function weekdaysRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysStrictRegex;
        } else {
            return this._weekdaysRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            this._weekdaysRegex = defaultWeekdaysRegex;
        }
        return this._weekdaysStrictRegex && isStrict ?
            this._weekdaysStrictRegex : this._weekdaysRegex;
    }
}

var defaultWeekdaysShortRegex = matchWord;
function weekdaysShortRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysShortStrictRegex;
        } else {
            return this._weekdaysShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysShortRegex')) {
            this._weekdaysShortRegex = defaultWeekdaysShortRegex;
        }
        return this._weekdaysShortStrictRegex && isStrict ?
            this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
}

var defaultWeekdaysMinRegex = matchWord;
function weekdaysMinRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysMinStrictRegex;
        } else {
            return this._weekdaysMinRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysMinRegex')) {
            this._weekdaysMinRegex = defaultWeekdaysMinRegex;
        }
        return this._weekdaysMinStrictRegex && isStrict ?
            this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
}


function computeWeekdaysParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom, minp, shortp, longp;
    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, 1]).day(i);
        minp = this.weekdaysMin(mom, '');
        shortp = this.weekdaysShort(mom, '');
        longp = this.weekdays(mom, '');
        minPieces.push(minp);
        shortPieces.push(shortp);
        longPieces.push(longp);
        mixedPieces.push(minp);
        mixedPieces.push(shortp);
        mixedPieces.push(longp);
    }
    // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.
    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 7; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;

    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
}

// FORMATTING

function hFormat() {
    return this.hours() % 12 || 12;
}

function kFormat() {
    return this.hours() || 24;
}

addFormatToken('H', ['HH', 2], 0, 'hour');
addFormatToken('h', ['hh', 2], 0, hFormat);
addFormatToken('k', ['kk', 2], 0, kFormat);

addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});

addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
});

addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

function meridiem (token, lowercase) {
    addFormatToken(token, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
}

meridiem('a', true);
meridiem('A', false);

// ALIASES

addUnitAlias('hour', 'h');

// PRIORITY
addUnitPriority('hour', 13);

// PARSING

function matchMeridiem (isStrict, locale) {
    return locale._meridiemParse;
}

addRegexToken('a',  matchMeridiem);
addRegexToken('A',  matchMeridiem);
addRegexToken('H',  match1to2);
addRegexToken('h',  match1to2);
addRegexToken('HH', match1to2, match2);
addRegexToken('hh', match1to2, match2);

addRegexToken('hmm', match3to4);
addRegexToken('hmmss', match5to6);
addRegexToken('Hmm', match3to4);
addRegexToken('Hmmss', match5to6);

addParseToken(['H', 'HH'], HOUR);
addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
});
addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
});
addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
});
addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
});

// LOCALES

function localeIsPM (input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return ((input + '').toLowerCase().charAt(0) === 'p');
}

var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
function localeMeridiem (hours, minutes, isLower) {
    if (hours > 11) {
        return isLower ? 'pm' : 'PM';
    } else {
        return isLower ? 'am' : 'AM';
    }
}


// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    ordinalParse: defaultOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};

// internal storage for locale config files
var locales = {};
var localeFamilies = {};
var globalLocale;

function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
}

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
function chooseLocale(names) {
    var i = 0, j, next, locale, split;

    while (i < names.length) {
        split = normalizeLocale(names[i]).split('-');
        j = split.length;
        next = normalizeLocale(names[i + 1]);
        next = next ? next.split('-') : null;
        while (j > 0) {
            locale = loadLocale(split.slice(0, j).join('-'));
            if (locale) {
                return locale;
            }
            if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                //the next array item is better than a shallower substring of this one
                break;
            }
            j--;
        }
        i++;
    }
    return null;
}

function loadLocale(name) {
    var oldLocale = null;
    // TODO: Find a better way to register and load all the locales in Node
    if (!locales[name] && (typeof module !== 'undefined') &&
            module && module.exports) {
        try {
            oldLocale = globalLocale._abbr;
            require('./locale/' + name);
            // because defineLocale currently also sets the global locale, we
            // want to undo that for lazy loaded locales
            getSetGlobalLocale(oldLocale);
        } catch (e) { }
    }
    return locales[name];
}

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
function getSetGlobalLocale (key, values) {
    var data;
    if (key) {
        if (isUndefined(values)) {
            data = getLocale(key);
        }
        else {
            data = defineLocale(key, values);
        }

        if (data) {
            // moment.duration._locale = moment._locale = data;
            globalLocale = data;
        }
    }

    return globalLocale._abbr;
}

function defineLocale (name, config) {
    if (config !== null) {
        var parentConfig = baseConfig;
        config.abbr = name;
        if (locales[name] != null) {
            deprecateSimple('defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                    'an existing locale. moment.defineLocale(localeName, ' +
                    'config) should only be used for creating a new locale ' +
                    'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
            parentConfig = locales[name]._config;
        } else if (config.parentLocale != null) {
            if (locales[config.parentLocale] != null) {
                parentConfig = locales[config.parentLocale]._config;
            } else {
                if (!localeFamilies[config.parentLocale]) {
                    localeFamilies[config.parentLocale] = [];
                }
                localeFamilies[config.parentLocale].push({
                    name: name,
                    config: config
                });
                return null;
            }
        }
        locales[name] = new Locale(mergeConfigs(parentConfig, config));

        if (localeFamilies[name]) {
            localeFamilies[name].forEach(function (x) {
                defineLocale(x.name, x.config);
            });
        }

        // backwards compat for now: also set the locale
        // make sure we set the locale AFTER all child locales have been
        // created, so we won't end up with the child locale set.
        getSetGlobalLocale(name);


        return locales[name];
    } else {
        // useful for testing
        delete locales[name];
        return null;
    }
}

function updateLocale(name, config) {
    if (config != null) {
        var locale, parentConfig = baseConfig;
        // MERGE
        if (locales[name] != null) {
            parentConfig = locales[name]._config;
        }
        config = mergeConfigs(parentConfig, config);
        locale = new Locale(config);
        locale.parentLocale = locales[name];
        locales[name] = locale;

        // backwards compat for now: also set the locale
        getSetGlobalLocale(name);
    } else {
        // pass null for config to unupdate, useful for tests
        if (locales[name] != null) {
            if (locales[name].parentLocale != null) {
                locales[name] = locales[name].parentLocale;
            } else if (locales[name] != null) {
                delete locales[name];
            }
        }
    }
    return locales[name];
}

// returns locale data
function getLocale (key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
        key = key._locale._abbr;
    }

    if (!key) {
        return globalLocale;
    }

    if (!isArray(key)) {
        //short-circuit everything else
        locale = loadLocale(key);
        if (locale) {
            return locale;
        }
        key = [key];
    }

    return chooseLocale(key);
}

function listLocales() {
    return keys(locales);
}

function checkOverflow (m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
        overflow =
            a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
            a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
            a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
            a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
            a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
            a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
            -1;

        if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
            overflow = DATE;
        }
        if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
            overflow = WEEK;
        }
        if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
            overflow = WEEKDAY;
        }

        getParsingFlags(m).overflow = overflow;
    }

    return m;
}

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

var isoDates = [
    ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
    ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
    ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
    ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
    ['YYYY-DDD', /\d{4}-\d{3}/],
    ['YYYY-MM', /\d{4}-\d\d/, false],
    ['YYYYYYMMDD', /[+-]\d{10}/],
    ['YYYYMMDD', /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ['GGGG[W]WWE', /\d{4}W\d{3}/],
    ['GGGG[W]WW', /\d{4}W\d{2}/, false],
    ['YYYYDDD', /\d{7}/]
];

// iso time formats and regexes
var isoTimes = [
    ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
    ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
    ['HH:mm:ss', /\d\d:\d\d:\d\d/],
    ['HH:mm', /\d\d:\d\d/],
    ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
    ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
    ['HHmmss', /\d\d\d\d\d\d/],
    ['HHmm', /\d\d\d\d/],
    ['HH', /\d\d/]
];

var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
function configFromISO(config) {
    var i, l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime, dateFormat, timeFormat, tzFormat;

    if (match) {
        getParsingFlags(config).iso = true;

        for (i = 0, l = isoDates.length; i < l; i++) {
            if (isoDates[i][1].exec(match[1])) {
                dateFormat = isoDates[i][0];
                allowTime = isoDates[i][2] !== false;
                break;
            }
        }
        if (dateFormat == null) {
            config._isValid = false;
            return;
        }
        if (match[3]) {
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(match[3])) {
                    // match[2] should be 'T' or space
                    timeFormat = (match[2] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (timeFormat == null) {
                config._isValid = false;
                return;
            }
        }
        if (!allowTime && timeFormat != null) {
            config._isValid = false;
            return;
        }
        if (match[4]) {
            if (tzRegex.exec(match[4])) {
                tzFormat = 'Z';
            } else {
                config._isValid = false;
                return;
            }
        }
        config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
        configFromStringAndFormat(config);
    } else {
        config._isValid = false;
    }
}

// date from iso format or fallback
function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
        config._d = new Date(+matched[1]);
        return;
    }

    configFromISO(config);
    if (config._isValid === false) {
        delete config._isValid;
        hooks.createFromInputFallback(config);
    }
}

hooks.createFromInputFallback = deprecate(
    'value provided is not in a recognized ISO format. moment construction falls back to js Date(), ' +
    'which is not reliable across all browsers and versions. Non ISO date formats are ' +
    'discouraged and will be removed in an upcoming major release. Please refer to ' +
    'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
    function (config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    }
);

// Pick the first defined of two or three arguments.
function defaults$1(a, b, c) {
    if (a != null) {
        return a;
    }
    if (b != null) {
        return b;
    }
    return c;
}

function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());
    if (config._useUTC) {
        return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }
    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
function configFromArray (config) {
    var i, date, input = [], currentDate, yearToUse;

    if (config._d) {
        return;
    }

    currentDate = currentDateArray(config);

    //compute day of the year from weeks and weekdays
    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        dayOfYearFromWeekInfo(config);
    }

    //if the day of the year is set, figure out what it is
    if (config._dayOfYear) {
        yearToUse = defaults$1(config._a[YEAR], currentDate[YEAR]);

        if (config._dayOfYear > daysInYear(yearToUse)) {
            getParsingFlags(config)._overflowDayOfYear = true;
        }

        date = createUTCDate(yearToUse, 0, config._dayOfYear);
        config._a[MONTH] = date.getUTCMonth();
        config._a[DATE] = date.getUTCDate();
    }

    // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything
    for (i = 0; i < 3 && config._a[i] == null; ++i) {
        config._a[i] = input[i] = currentDate[i];
    }

    // Zero out whatever was not defaulted, including time
    for (; i < 7; i++) {
        config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
    }

    // Check for 24:00:00.000
    if (config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0) {
        config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.
    if (config._tzm != null) {
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
        config._a[HOUR] = 24;
    }
}

function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

    w = config._w;
    if (w.GG != null || w.W != null || w.E != null) {
        dow = 1;
        doy = 4;

        // TODO: We need to take the current isoWeekYear, but that depends on
        // how we interpret now (local, utc, fixed offset). So create
        // a now version of current config (take local/utc/offset flags, and
        // create now).
        weekYear = defaults$1(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
        week = defaults$1(w.W, 1);
        weekday = defaults$1(w.E, 1);
        if (weekday < 1 || weekday > 7) {
            weekdayOverflow = true;
        }
    } else {
        dow = config._locale._week.dow;
        doy = config._locale._week.doy;

        var curWeek = weekOfYear(createLocal(), dow, doy);

        weekYear = defaults$1(w.gg, config._a[YEAR], curWeek.year);

        // Default to current week.
        week = defaults$1(w.w, curWeek.week);

        if (w.d != null) {
            // weekday -- low day numbers are considered next week
            weekday = w.d;
            if (weekday < 0 || weekday > 6) {
                weekdayOverflow = true;
            }
        } else if (w.e != null) {
            // local weekday -- counting starts from begining of week
            weekday = w.e + dow;
            if (w.e < 0 || w.e > 6) {
                weekdayOverflow = true;
            }
        } else {
            // default to begining of week
            weekday = dow;
        }
    }
    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
        getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
        getParsingFlags(config)._overflowWeekday = true;
    } else {
        temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }
}

// constant that refers to the ISO standard
hooks.ISO_8601 = function () {};

// date from string and format string
function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
        configFromISO(config);
        return;
    }

    config._a = [];
    getParsingFlags(config).empty = true;

    // This array is used to make a Date, either with `new Date` or `Date.UTC`
    var string = '' + config._i,
        i, parsedInput, tokens, token, skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
        token = tokens[i];
        parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        // console.log('token', token, 'parsedInput', parsedInput,
        //         'regex', getParseRegexForToken(token, config));
        if (parsedInput) {
            skipped = string.substr(0, string.indexOf(parsedInput));
            if (skipped.length > 0) {
                getParsingFlags(config).unusedInput.push(skipped);
            }
            string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
            totalParsedInputLength += parsedInput.length;
        }
        // don't parse if it's not a known token
        if (formatTokenFunctions[token]) {
            if (parsedInput) {
                getParsingFlags(config).empty = false;
            }
            else {
                getParsingFlags(config).unusedTokens.push(token);
            }
            addTimeToArrayFromToken(token, parsedInput, config);
        }
        else if (config._strict && !parsedInput) {
            getParsingFlags(config).unusedTokens.push(token);
        }
    }

    // add remaining unparsed input length to the string
    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
    if (string.length > 0) {
        getParsingFlags(config).unusedInput.push(string);
    }

    // clear _12h flag if hour is <= 12
    if (config._a[HOUR] <= 12 &&
        getParsingFlags(config).bigHour === true &&
        config._a[HOUR] > 0) {
        getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem;
    // handle meridiem
    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

    configFromArray(config);
    checkOverflow(config);
}


function meridiemFixWrap (locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
        // nothing to do
        return hour;
    }
    if (locale.meridiemHour != null) {
        return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
        // Fallback
        isPm = locale.isPM(meridiem);
        if (isPm && hour < 12) {
            hour += 12;
        }
        if (!isPm && hour === 12) {
            hour = 0;
        }
        return hour;
    } else {
        // this is not supposed to happen
        return hour;
    }
}

// date from string and array of format strings
function configFromStringAndArray(config) {
    var tempConfig,
        bestMoment,

        scoreToBeat,
        i,
        currentScore;

    if (config._f.length === 0) {
        getParsingFlags(config).invalidFormat = true;
        config._d = new Date(NaN);
        return;
    }

    for (i = 0; i < config._f.length; i++) {
        currentScore = 0;
        tempConfig = copyConfig({}, config);
        if (config._useUTC != null) {
            tempConfig._useUTC = config._useUTC;
        }
        tempConfig._f = config._f[i];
        configFromStringAndFormat(tempConfig);

        if (!isValid(tempConfig)) {
            continue;
        }

        // if there is any input that was not parsed add a penalty for that format
        currentScore += getParsingFlags(tempConfig).charsLeftOver;

        //or tokens
        currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

        getParsingFlags(tempConfig).score = currentScore;

        if (scoreToBeat == null || currentScore < scoreToBeat) {
            scoreToBeat = currentScore;
            bestMoment = tempConfig;
        }
    }

    extend(config, bestMoment || tempConfig);
}

function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}

function createFromConfig (config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

function prepareConfig (config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || (format === undefined && input === '')) {
        return createInvalid({nullInput: true});
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
        config._d = input;
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    }  else {
        configFromInput(config);
    }

    if (!isValid(config)) {
        config._d = null;
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (input === undefined) {
        config._d = new Date(hooks.now());
    } else if (isDate(input)) {
        config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if (typeof(input) === 'object') {
        configFromObject(config);
    } else if (isNumber(input)) {
        // from milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

function createLocalOrUTC (input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
        strict = locale;
        locale = undefined;
    }

    if ((isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)) {
        input = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}

function createLocal (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}

var prototypeMin = deprecate(
    'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other < this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

var prototypeMax = deprecate(
    'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other > this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
function pickBy(fn, moments) {
    var res, i;
    if (moments.length === 1 && isArray(moments[0])) {
        moments = moments[0];
    }
    if (!moments.length) {
        return createLocal();
    }
    res = moments[0];
    for (i = 1; i < moments.length; ++i) {
        if (!moments[i].isValid() || moments[i][fn](res)) {
            res = moments[i];
        }
    }
    return res;
}

// TODO: Use [].sort instead?
function min () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isBefore', args);
}

function max () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isAfter', args);
}

var now = function () {
    return Date.now ? Date.now() : +(new Date());
};

function Duration (duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;

    // representation for dateAddRemove
    this._milliseconds = +milliseconds +
        seconds * 1e3 + // 1000
        minutes * 6e4 + // 1000 * 60
        hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately
    this._days = +days +
        weeks * 7;
    // It is impossible translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.
    this._months = +months +
        quarters * 3 +
        years * 12;

    this._data = {};

    this._locale = getLocale();

    this._bubble();
}

function isDuration (obj) {
    return obj instanceof Duration;
}

function absRound (number) {
    if (number < 0) {
        return Math.round(-1 * number) * -1;
    } else {
        return Math.round(number);
    }
}

// FORMATTING

function offset (token, separator) {
    addFormatToken(token, 0, 0, function () {
        var offset = this.utcOffset();
        var sign = '+';
        if (offset < 0) {
            offset = -offset;
            sign = '-';
        }
        return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
    });
}

offset('Z', ':');
offset('ZZ', '');

// PARSING

addRegexToken('Z',  matchShortOffset);
addRegexToken('ZZ', matchShortOffset);
addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
});

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
var chunkOffset = /([\+\-]|\d\d)/gi;

function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
        return null;
    }

    var chunk   = matches[matches.length - 1] || [];
    var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);

    return minutes === 0 ?
      0 :
      parts[0] === '+' ? minutes : -minutes;
}

// Return a moment from input, that is local/utc/zone equivalent to model.
function cloneWithOffset(input, model) {
    var res, diff;
    if (model._isUTC) {
        res = model.clone();
        diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
        // Use low-level api, because this fn is low-level api.
        res._d.setTime(res._d.valueOf() + diff);
        hooks.updateOffset(res, false);
        return res;
    } else {
        return createLocal(input).local();
    }
}

function getDateOffset (m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
}

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
function getSetOffset (input, keepLocalTime) {
    var offset = this._offset || 0,
        localAdjust;
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    if (input != null) {
        if (typeof input === 'string') {
            input = offsetFromString(matchShortOffset, input);
            if (input === null) {
                return this;
            }
        } else if (Math.abs(input) < 16) {
            input = input * 60;
        }
        if (!this._isUTC && keepLocalTime) {
            localAdjust = getDateOffset(this);
        }
        this._offset = input;
        this._isUTC = true;
        if (localAdjust != null) {
            this.add(localAdjust, 'm');
        }
        if (offset !== input) {
            if (!keepLocalTime || this._changeInProgress) {
                addSubtract(this, createDuration(input - offset, 'm'), 1, false);
            } else if (!this._changeInProgress) {
                this._changeInProgress = true;
                hooks.updateOffset(this, true);
                this._changeInProgress = null;
            }
        }
        return this;
    } else {
        return this._isUTC ? offset : getDateOffset(this);
    }
}

function getSetZone (input, keepLocalTime) {
    if (input != null) {
        if (typeof input !== 'string') {
            input = -input;
        }

        this.utcOffset(input, keepLocalTime);

        return this;
    } else {
        return -this.utcOffset();
    }
}

function setOffsetToUTC (keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
}

function setOffsetToLocal (keepLocalTime) {
    if (this._isUTC) {
        this.utcOffset(0, keepLocalTime);
        this._isUTC = false;

        if (keepLocalTime) {
            this.subtract(getDateOffset(this), 'm');
        }
    }
    return this;
}

function setOffsetToParsedOffset () {
    if (this._tzm != null) {
        this.utcOffset(this._tzm);
    } else if (typeof this._i === 'string') {
        var tZone = offsetFromString(matchOffset, this._i);
        if (tZone != null) {
            this.utcOffset(tZone);
        }
        else {
            this.utcOffset(0, true);
        }
    }
    return this;
}

function hasAlignedHourOffset (input) {
    if (!this.isValid()) {
        return false;
    }
    input = input ? createLocal(input).utcOffset() : 0;

    return (this.utcOffset() - input) % 60 === 0;
}

function isDaylightSavingTime () {
    return (
        this.utcOffset() > this.clone().month(0).utcOffset() ||
        this.utcOffset() > this.clone().month(5).utcOffset()
    );
}

function isDaylightSavingTimeShifted () {
    if (!isUndefined(this._isDSTShifted)) {
        return this._isDSTShifted;
    }

    var c = {};

    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
        var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
        this._isDSTShifted = this.isValid() &&
            compareArrays(c._a, other.toArray()) > 0;
    } else {
        this._isDSTShifted = false;
    }

    return this._isDSTShifted;
}

function isLocal () {
    return this.isValid() ? !this._isUTC : false;
}

function isUtcOffset () {
    return this.isValid() ? this._isUTC : false;
}

function isUtc () {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
}

// ASP.NET json date format regex
var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

function createDuration (input, key) {
    var duration = input,
        // matching against regexp is expensive, do it on demand
        match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
        duration = {
            ms : input._milliseconds,
            d  : input._days,
            M  : input._months
        };
    } else if (isNumber(input)) {
        duration = {};
        if (key) {
            duration[key] = input;
        } else {
            duration.milliseconds = input;
        }
    } else if (!!(match = aspNetRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y  : 0,
            d  : toInt(match[DATE])                         * sign,
            h  : toInt(match[HOUR])                         * sign,
            m  : toInt(match[MINUTE])                       * sign,
            s  : toInt(match[SECOND])                       * sign,
            ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
        };
    } else if (!!(match = isoRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y : parseIso(match[2], sign),
            M : parseIso(match[3], sign),
            w : parseIso(match[4], sign),
            d : parseIso(match[5], sign),
            h : parseIso(match[6], sign),
            m : parseIso(match[7], sign),
            s : parseIso(match[8], sign)
        };
    } else if (duration == null) {// checks for null or undefined
        duration = {};
    } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
        diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

        duration = {};
        duration.ms = diffRes.milliseconds;
        duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
        ret._locale = input._locale;
    }

    return ret;
}

createDuration.fn = Duration.prototype;

function parseIso (inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.'));
    // apply sign while we're at it
    return (isNaN(res) ? 0 : res) * sign;
}

function positiveMomentsDifference(base, other) {
    var res = {milliseconds: 0, months: 0};

    res.months = other.month() - base.month() +
        (other.year() - base.year()) * 12;
    if (base.clone().add(res.months, 'M').isAfter(other)) {
        --res.months;
    }

    res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

    return res;
}

function momentsDifference(base, other) {
    var res;
    if (!(base.isValid() && other.isValid())) {
        return {milliseconds: 0, months: 0};
    }

    other = cloneWithOffset(other, base);
    if (base.isBefore(other)) {
        res = positiveMomentsDifference(base, other);
    } else {
        res = positiveMomentsDifference(other, base);
        res.milliseconds = -res.milliseconds;
        res.months = -res.months;
    }

    return res;
}

// TODO: remove 'name' arg after deprecation is removed
function createAdder(direction, name) {
    return function (val, period) {
        var dur, tmp;
        //invert the arguments, but complain about it
        if (period !== null && !isNaN(+period)) {
            deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
            'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
            tmp = val; val = period; period = tmp;
        }

        val = typeof val === 'string' ? +val : val;
        dur = createDuration(val, period);
        addSubtract(this, dur, direction);
        return this;
    };
}

function addSubtract (mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
        // No op
        return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (milliseconds) {
        mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }
    if (days) {
        set$2(mom, 'Date', get$1(mom, 'Date') + days * isAdding);
    }
    if (months) {
        setMonth(mom, get$1(mom, 'Month') + months * isAdding);
    }
    if (updateOffset) {
        hooks.updateOffset(mom, days || months);
    }
}

var add      = createAdder(1, 'add');
var subtract = createAdder(-1, 'subtract');

function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' :
            diff < -1 ? 'lastWeek' :
            diff < 0 ? 'lastDay' :
            diff < 1 ? 'sameDay' :
            diff < 2 ? 'nextDay' :
            diff < 7 ? 'nextWeek' : 'sameElse';
}

function calendar$1 (time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';

    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
}

function clone () {
    return new Moment(this);
}

function isAfter (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() > localInput.valueOf();
    } else {
        return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
}

function isBefore (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() < localInput.valueOf();
    } else {
        return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
}

function isBetween (from, to, units, inclusivity) {
    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
        (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
}

function isSame (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(units || 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() === localInput.valueOf();
    } else {
        inputMs = localInput.valueOf();
        return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
}

function isSameOrAfter (input, units) {
    return this.isSame(input, units) || this.isAfter(input,units);
}

function isSameOrBefore (input, units) {
    return this.isSame(input, units) || this.isBefore(input,units);
}

function diff (input, units, asFloat) {
    var that,
        zoneDelta,
        delta, output;

    if (!this.isValid()) {
        return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
        return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

    units = normalizeUnits(units);

    if (units === 'year' || units === 'month' || units === 'quarter') {
        output = monthDiff(this, that);
        if (units === 'quarter') {
            output = output / 3;
        } else if (units === 'year') {
            output = output / 12;
        }
    } else {
        delta = this - that;
        output = units === 'second' ? delta / 1e3 : // 1000
            units === 'minute' ? delta / 6e4 : // 1000 * 60
            units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
            units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
            units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
            delta;
    }
    return asFloat ? output : absFloor(output);
}

function monthDiff (a, b) {
    // difference in months
    var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
        anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2, adjust;

    if (b - anchor < 0) {
        anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor - anchor2);
    } else {
        anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor2 - anchor);
    }

    //check for negative zero, return zero if negative zero
    return -(wholeMonthDiff + adjust) || 0;
}

hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

function toString () {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
}

function toISOString () {
    var m = this.clone().utc();
    if (0 < m.year() && m.year() <= 9999) {
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            return this.toDate().toISOString();
        } else {
            return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
    } else {
        return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }
}

/**
 * Return a human readable representation of a moment that can
 * also be evaluated to get a new moment which is the same
 *
 * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
 */
function inspect () {
    if (!this.isValid()) {
        return 'moment.invalid(/* ' + this._i + ' */)';
    }
    var func = 'moment';
    var zone = '';
    if (!this.isLocal()) {
        func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
        zone = 'Z';
    }
    var prefix = '[' + func + '("]';
    var year = (0 < this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';

    return this.format(prefix + year + datetime + suffix);
}

function format (inputString) {
    if (!inputString) {
        inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }
    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
}

function from (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function fromNow (withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}

function to (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function toNow (withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
function locale (key) {
    var newLocaleData;

    if (key === undefined) {
        return this._locale._abbr;
    } else {
        newLocaleData = getLocale(key);
        if (newLocaleData != null) {
            this._locale = newLocaleData;
        }
        return this;
    }
}

var lang = deprecate(
    'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
    function (key) {
        if (key === undefined) {
            return this.localeData();
        } else {
            return this.locale(key);
        }
    }
);

function localeData () {
    return this._locale;
}

function startOf (units) {
    units = normalizeUnits(units);
    // the following switch intentionally omits break keywords
    // to utilize falling through the cases.
    switch (units) {
        case 'year':
            this.month(0);
            /* falls through */
        case 'quarter':
        case 'month':
            this.date(1);
            /* falls through */
        case 'week':
        case 'isoWeek':
        case 'day':
        case 'date':
            this.hours(0);
            /* falls through */
        case 'hour':
            this.minutes(0);
            /* falls through */
        case 'minute':
            this.seconds(0);
            /* falls through */
        case 'second':
            this.milliseconds(0);
    }

    // weeks are a special case
    if (units === 'week') {
        this.weekday(0);
    }
    if (units === 'isoWeek') {
        this.isoWeekday(1);
    }

    // quarters are also special
    if (units === 'quarter') {
        this.month(Math.floor(this.month() / 3) * 3);
    }

    return this;
}

function endOf (units) {
    units = normalizeUnits(units);
    if (units === undefined || units === 'millisecond') {
        return this;
    }

    // 'date' is an alias for 'day', so it should be considered as such.
    if (units === 'date') {
        units = 'day';
    }

    return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
}

function valueOf () {
    return this._d.valueOf() - ((this._offset || 0) * 60000);
}

function unix () {
    return Math.floor(this.valueOf() / 1000);
}

function toDate () {
    return new Date(this.valueOf());
}

function toArray$1 () {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
}

function toObject () {
    var m = this;
    return {
        years: m.year(),
        months: m.month(),
        date: m.date(),
        hours: m.hours(),
        minutes: m.minutes(),
        seconds: m.seconds(),
        milliseconds: m.milliseconds()
    };
}

function toJSON () {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
}

function isValid$1 () {
    return isValid(this);
}

function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

function invalidAt () {
    return getParsingFlags(this).overflow;
}

function creationData() {
    return {
        input: this._i,
        format: this._f,
        locale: this._locale,
        isUTC: this._isUTC,
        strict: this._strict
    };
}

// FORMATTING

addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
});

addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
});

function addWeekYearFormatToken (token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
}

addWeekYearFormatToken('gggg',     'weekYear');
addWeekYearFormatToken('ggggg',    'weekYear');
addWeekYearFormatToken('GGGG',  'isoWeekYear');
addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

addUnitAlias('weekYear', 'gg');
addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

addUnitPriority('weekYear', 1);
addUnitPriority('isoWeekYear', 1);


// PARSING

addRegexToken('G',      matchSigned);
addRegexToken('g',      matchSigned);
addRegexToken('GG',     match1to2, match2);
addRegexToken('gg',     match1to2, match2);
addRegexToken('GGGG',   match1to4, match4);
addRegexToken('gggg',   match1to4, match4);
addRegexToken('GGGGG',  match1to6, match6);
addRegexToken('ggggg',  match1to6, match6);

addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
});

addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
});

// MOMENTS

function getSetWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input,
            this.week(),
            this.weekday(),
            this.localeData()._week.dow,
            this.localeData()._week.doy);
}

function getSetISOWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input, this.isoWeek(), this.isoWeekday(), 1, 4);
}

function getISOWeeksInYear () {
    return weeksInYear(this.year(), 1, 4);
}

function getWeeksInYear () {
    var weekInfo = this.localeData()._week;
    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}

function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;
    if (input == null) {
        return weekOfYear(this, dow, doy).year;
    } else {
        weeksTarget = weeksInYear(input, dow, doy);
        if (week > weeksTarget) {
            week = weeksTarget;
        }
        return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
}

function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
}

// FORMATTING

addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

addUnitAlias('quarter', 'Q');

// PRIORITY

addUnitPriority('quarter', 7);

// PARSING

addRegexToken('Q', match1);
addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
});

// MOMENTS

function getSetQuarter (input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PRIOROITY
addUnitPriority('date', 9);

// PARSING

addRegexToken('D',  match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    return isStrict ? locale._ordinalParse : locale._ordinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
addUnitPriority('dayOfYear', 4);

// PARSING

addRegexToken('DDD',  match1to3);
addRegexToken('DDDD', match3);
addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
});

// HELPERS

// MOMENTS

function getSetDayOfYear (input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
}

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PRIORITY

addUnitPriority('minute', 14);

// PARSING

addRegexToken('m',  match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PRIORITY

addUnitPriority('second', 15);

// PARSING

addRegexToken('s',  match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
});

addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
});

addFormatToken(0, ['SSS', 3], 0, 'millisecond');
addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
});
addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
});
addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
});
addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
});
addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
});
addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
});


// ALIASES

addUnitAlias('millisecond', 'ms');

// PRIORITY

addUnitPriority('millisecond', 16);

// PARSING

addRegexToken('S',    match1to3, match1);
addRegexToken('SS',   match1to3, match2);
addRegexToken('SSS',  match1to3, match3);

var token;
for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
}

function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
}

for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
}
// MOMENTS

var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

addFormatToken('z',  0, 0, 'zoneAbbr');
addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

function getZoneAbbr () {
    return this._isUTC ? 'UTC' : '';
}

function getZoneName () {
    return this._isUTC ? 'Coordinated Universal Time' : '';
}

var proto = Moment.prototype;

proto.add               = add;
proto.calendar          = calendar$1;
proto.clone             = clone;
proto.diff              = diff;
proto.endOf             = endOf;
proto.format            = format;
proto.from              = from;
proto.fromNow           = fromNow;
proto.to                = to;
proto.toNow             = toNow;
proto.get               = stringGet;
proto.invalidAt         = invalidAt;
proto.isAfter           = isAfter;
proto.isBefore          = isBefore;
proto.isBetween         = isBetween;
proto.isSame            = isSame;
proto.isSameOrAfter     = isSameOrAfter;
proto.isSameOrBefore    = isSameOrBefore;
proto.isValid           = isValid$1;
proto.lang              = lang;
proto.locale            = locale;
proto.localeData        = localeData;
proto.max               = prototypeMax;
proto.min               = prototypeMin;
proto.parsingFlags      = parsingFlags;
proto.set               = stringSet;
proto.startOf           = startOf;
proto.subtract          = subtract;
proto.toArray           = toArray$1;
proto.toObject          = toObject;
proto.toDate            = toDate;
proto.toISOString       = toISOString;
proto.inspect           = inspect;
proto.toJSON            = toJSON;
proto.toString          = toString;
proto.unix              = unix;
proto.valueOf           = valueOf;
proto.creationData      = creationData;

// Year
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
proto.quarter = proto.quarters = getSetQuarter;

// Month
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
proto.hour = proto.hours = getSetHour;

// Minute
proto.minute = proto.minutes = getSetMinute;

// Second
proto.second = proto.seconds = getSetSecond;

// Millisecond
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

function preParsePostFormat (string) {
    return string;
}

var proto$1 = Locale.prototype;

proto$1.calendar        = calendar;
proto$1.longDateFormat  = longDateFormat;
proto$1.invalidDate     = invalidDate;
proto$1.ordinal         = ordinal;
proto$1.preparse        = preParsePostFormat;
proto$1.postformat      = preParsePostFormat;
proto$1.relativeTime    = relativeTime;
proto$1.pastFuture      = pastFuture;
proto$1.set             = set$1;

// Month
proto$1.months            =        localeMonths;
proto$1.monthsShort       =        localeMonthsShort;
proto$1.monthsParse       =        localeMonthsParse;
proto$1.monthsRegex       = monthsRegex;
proto$1.monthsShortRegex  = monthsShortRegex;

// Week
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
proto$1.weekdays       =        localeWeekdays;
proto$1.weekdaysMin    =        localeWeekdaysMin;
proto$1.weekdaysShort  =        localeWeekdaysShort;
proto$1.weekdaysParse  =        localeWeekdaysParse;

proto$1.weekdaysRegex       =        weekdaysRegex;
proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;

function get$2 (format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
}

function listMonthsImpl (format, index, field) {
    if (isNumber(format)) {
        index = format;
        format = undefined;
    }

    format = format || '';

    if (index != null) {
        return get$2(format, index, field, 'month');
    }

    var i;
    var out = [];
    for (i = 0; i < 12; i++) {
        out[i] = get$2(format, i, field, 'month');
    }
    return out;
}

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
function listWeekdaysImpl (localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    } else {
        format = localeSorted;
        index = format;
        localeSorted = false;

        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
        return get$2(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];
    for (i = 0; i < 7; i++) {
        out[i] = get$2(format, (i + shift) % 7, field, 'day');
    }
    return out;
}

function listMonths (format, index) {
    return listMonthsImpl(format, index, 'months');
}

function listMonthsShort (format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
}

function listWeekdays (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
}

function listWeekdaysShort (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
}

function listWeekdaysMin (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
}

getSetGlobalLocale('en', {
    ordinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal : function (number) {
        var b = number % 10,
            output = (toInt(number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
        return number + output;
    }
});

// Side effect imports
hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

var mathAbs = Math.abs;

function abs () {
    var data           = this._data;

    this._milliseconds = mathAbs(this._milliseconds);
    this._days         = mathAbs(this._days);
    this._months       = mathAbs(this._months);

    data.milliseconds  = mathAbs(data.milliseconds);
    data.seconds       = mathAbs(data.seconds);
    data.minutes       = mathAbs(data.minutes);
    data.hours         = mathAbs(data.hours);
    data.months        = mathAbs(data.months);
    data.years         = mathAbs(data.years);

    return this;
}

function addSubtract$1 (duration, input, value, direction) {
    var other = createDuration(input, value);

    duration._milliseconds += direction * other._milliseconds;
    duration._days         += direction * other._days;
    duration._months       += direction * other._months;

    return duration._bubble();
}

// supports only 2.0-style add(1, 's') or add(duration)
function add$1 (input, value) {
    return addSubtract$1(this, input, value, 1);
}

// supports only 2.0-style subtract(1, 's') or subtract(duration)
function subtract$1 (input, value) {
    return addSubtract$1(this, input, value, -1);
}

function absCeil (number) {
    if (number < 0) {
        return Math.floor(number);
    } else {
        return Math.ceil(number);
    }
}

function bubble () {
    var milliseconds = this._milliseconds;
    var days         = this._days;
    var months       = this._months;
    var data         = this._data;
    var seconds, minutes, hours, years, monthsFromDays;

    // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166
    if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
            (milliseconds <= 0 && days <= 0 && months <= 0))) {
        milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

    // The following code bubbles up values, see the tests for
    // examples of what that means.
    data.milliseconds = milliseconds % 1000;

    seconds           = absFloor(milliseconds / 1000);
    data.seconds      = seconds % 60;

    minutes           = absFloor(seconds / 60);
    data.minutes      = minutes % 60;

    hours             = absFloor(minutes / 60);
    data.hours        = hours % 24;

    days += absFloor(hours / 24);

    // convert days to months
    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays));

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    data.days   = days;
    data.months = months;
    data.years  = years;

    return this;
}

function daysToMonths (days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
}

function monthsToDays (months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
}

function as (units) {
    var days;
    var months;
    var milliseconds = this._milliseconds;

    units = normalizeUnits(units);

    if (units === 'month' || units === 'year') {
        days   = this._days   + milliseconds / 864e5;
        months = this._months + daysToMonths(days);
        return units === 'month' ? months : months / 12;
    } else {
        // handle milliseconds separately because of floating point math errors (issue #1867)
        days = this._days + Math.round(monthsToDays(this._months));
        switch (units) {
            case 'week'   : return days / 7     + milliseconds / 6048e5;
            case 'day'    : return days         + milliseconds / 864e5;
            case 'hour'   : return days * 24    + milliseconds / 36e5;
            case 'minute' : return days * 1440  + milliseconds / 6e4;
            case 'second' : return days * 86400 + milliseconds / 1000;
            // Math.floor prevents floating point math errors here
            case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
            default: throw new Error('Unknown unit ' + units);
        }
    }
}

// TODO: Use this.as('ms')?
function valueOf$1 () {
    return (
        this._milliseconds +
        this._days * 864e5 +
        (this._months % 12) * 2592e6 +
        toInt(this._months / 12) * 31536e6
    );
}

function makeAs (alias) {
    return function () {
        return this.as(alias);
    };
}

var asMilliseconds = makeAs('ms');
var asSeconds      = makeAs('s');
var asMinutes      = makeAs('m');
var asHours        = makeAs('h');
var asDays         = makeAs('d');
var asWeeks        = makeAs('w');
var asMonths       = makeAs('M');
var asYears        = makeAs('y');

function get$3 (units) {
    units = normalizeUnits(units);
    return this[units + 's']();
}

function makeGetter(name) {
    return function () {
        return this._data[name];
    };
}

var milliseconds = makeGetter('milliseconds');
var seconds      = makeGetter('seconds');
var minutes      = makeGetter('minutes');
var hours        = makeGetter('hours');
var days         = makeGetter('days');
var months       = makeGetter('months');
var years        = makeGetter('years');

function weeks () {
    return absFloor(this.days() / 7);
}

var round = Math.round;
var thresholds = {
    s: 45,  // seconds to minute
    m: 45,  // minutes to hour
    h: 22,  // hours to day
    d: 26,  // days to month
    M: 11   // months to year
};

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}

function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds  = round(duration.as('s'));
    var minutes  = round(duration.as('m'));
    var hours    = round(duration.as('h'));
    var days     = round(duration.as('d'));
    var months   = round(duration.as('M'));
    var years    = round(duration.as('y'));

    var a = seconds < thresholds.s && ['s', seconds]  ||
            minutes <= 1           && ['m']           ||
            minutes < thresholds.m && ['mm', minutes] ||
            hours   <= 1           && ['h']           ||
            hours   < thresholds.h && ['hh', hours]   ||
            days    <= 1           && ['d']           ||
            days    < thresholds.d && ['dd', days]    ||
            months  <= 1           && ['M']           ||
            months  < thresholds.M && ['MM', months]  ||
            years   <= 1           && ['y']           || ['yy', years];

    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
}

// This function allows you to set the rounding function for relative time strings
function getSetRelativeTimeRounding (roundingFunction) {
    if (roundingFunction === undefined) {
        return round;
    }
    if (typeof(roundingFunction) === 'function') {
        round = roundingFunction;
        return true;
    }
    return false;
}

// This function allows you to set a threshold for relative time strings
function getSetRelativeTimeThreshold (threshold, limit) {
    if (thresholds[threshold] === undefined) {
        return false;
    }
    if (limit === undefined) {
        return thresholds[threshold];
    }
    thresholds[threshold] = limit;
    return true;
}

function humanize (withSuffix) {
    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
        output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
}

var abs$1 = Math.abs;

function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    var seconds = abs$1(this._milliseconds) / 1000;
    var days         = abs$1(this._days);
    var months       = abs$1(this._months);
    var minutes, hours, years;

    // 3600 seconds -> 60 minutes -> 1 hour
    minutes           = absFloor(seconds / 60);
    hours             = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60;

    // 12 months -> 1 year
    years  = absFloor(months / 12);
    months %= 12;


    // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds;
    var total = this.asSeconds();

    if (!total) {
        // this is the same as C#'s (Noda) and python (isodate)...
        // but not other JS (goog.date)
        return 'P0D';
    }

    return (total < 0 ? '-' : '') +
        'P' +
        (Y ? Y + 'Y' : '') +
        (M ? M + 'M' : '') +
        (D ? D + 'D' : '') +
        ((h || m || s) ? 'T' : '') +
        (h ? h + 'H' : '') +
        (m ? m + 'M' : '') +
        (s ? s + 'S' : '');
}

var proto$2 = Duration.prototype;

proto$2.abs            = abs;
proto$2.add            = add$1;
proto$2.subtract       = subtract$1;
proto$2.as             = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds      = asSeconds;
proto$2.asMinutes      = asMinutes;
proto$2.asHours        = asHours;
proto$2.asDays         = asDays;
proto$2.asWeeks        = asWeeks;
proto$2.asMonths       = asMonths;
proto$2.asYears        = asYears;
proto$2.valueOf        = valueOf$1;
proto$2._bubble        = bubble;
proto$2.get            = get$3;
proto$2.milliseconds   = milliseconds;
proto$2.seconds        = seconds;
proto$2.minutes        = minutes;
proto$2.hours          = hours;
proto$2.days           = days;
proto$2.weeks          = weeks;
proto$2.months         = months;
proto$2.years          = years;
proto$2.humanize       = humanize;
proto$2.toISOString    = toISOString$1;
proto$2.toString       = toISOString$1;
proto$2.toJSON         = toISOString$1;
proto$2.locale         = locale;
proto$2.localeData     = localeData;

// Deprecations
proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
proto$2.lang = lang;

// Side effect imports

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});

// Side effect imports

//! moment.js
//! version : 2.17.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

hooks.version = '2.17.1';

setHookCallback(createLocal);

hooks.fn                    = proto;
hooks.min                   = min;
hooks.max                   = max;
hooks.now                   = now;
hooks.utc                   = createUTC;
hooks.unix                  = createUnix;
hooks.months                = listMonths;
hooks.isDate                = isDate;
hooks.locale                = getSetGlobalLocale;
hooks.invalid               = createInvalid;
hooks.duration              = createDuration;
hooks.isMoment              = isMoment;
hooks.weekdays              = listWeekdays;
hooks.parseZone             = createInZone;
hooks.localeData            = getLocale;
hooks.isDuration            = isDuration;
hooks.monthsShort           = listMonthsShort;
hooks.weekdaysMin           = listWeekdaysMin;
hooks.defineLocale          = defineLocale;
hooks.updateLocale          = updateLocale;
hooks.locales               = listLocales;
hooks.weekdaysShort         = listWeekdaysShort;
hooks.normalizeUnits        = normalizeUnits;
hooks.relativeTimeRounding = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat        = getCalendarFormat;
hooks.prototype             = proto;

var Layout$3 = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {
            var zone = zones[zoneIndex];

            var base = new Date(zone.start + '-01');
            var offset = base.getDay();

            //  moment().diff(date_time, 'minutes')
            d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
                var index = d.boundary.charCodeAt(0) - 65;
                return index == zoneIndex;
            }).each(function (d, i) {
                if (zone.unit == "monthly" && d.hasOwnProperty("startDate")) {
                    var days = hooks(base).add(Number(offset) + Number(d.startDate), 'days').diff(base, 'days');

                    d.x = 100 + days * 22;
                    d3.select(this).select("rect").attr("width", function (d) {
                        return d.hasOwnProperty("duration") ? d.duration * 22 : 100;
                    });
                }
                if (zone.unit == "quarterly" && d.hasOwnProperty("startDate")) {
                    var months = hooks(base).add(Number(d.startDate), 'months').diff(base, 'months');

                    d.x = 100 + months * 30;

                    d3.select(this).select("rect").attr("width", function (d) {
                        return d.hasOwnProperty("duration") ? d.duration * 30 : 100;
                    });
                    d3.select(this).select("circle").attr("transform", function (d) {
                        return "translate(" + (d.hasOwnProperty("duration") ? d.duration * 30 + 6 : 100) + "," + 3 + ")";
                    });
                }
            });
        }
    }, {
        key: 'build',
        value: function build(d3visual, zone, root) {

            function addMonths(dateObj, num) {

                var currentMonth = dateObj.getMonth();
                dateObj.setMonth(dateObj.getMonth() + num);

                if (dateObj.getMonth() != (currentMonth + num) % 12) {
                    dateObj.setDate(0);
                }
                return dateObj;
            }

            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            //      let vb = d3visual.attr("viewBox").split(' ');
            //
            //      let viewWidth = vb[2];
            //      let viewHeight = vb[3];
            //
            //      let pageHeight = d3visual.node().parentNode.clientHeight;
            //      let pageWidth = d3visual.node().parentNode.clientWidth;
            //      // THis needs to be a calculation based on a ratio
            //
            //      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
            //
            //      for ( let v = 0; v < 4; v++) {
            //          if (typeof zone.rectangle[v] == "string") {
            //            //console.log("Evaluating: " + zone.rectangle[v]);
            //            //console.log(" ... to : " + eval(zone.rectangle[v]));
            //            zone.rectangle[v] = eval(zone.rectangle[v]);
            //          }
            //      }
            //
            //      if (pageHeightSet) {
            //          zone.rectangle[3] = ((zone.rectangle[2]+zone.rectangle[0]) * pageHeight/pageWidth) - 20;
            //      }

            this.calculateRectangle(d3visual, zone);

            var rect = obj.append("rect").attr("class", "background").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("rx", 8).attr("ry", 8).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]); //fff899


            // 26 weeks
            // 90*4 = 360

            // zone.unit == quarterly : show the month lines
            // zone.unit == monthly : show the weekly lines (with weekend)

            var milestoneGap = 0;
            var milestones = [];
            var bars = [];
            var level1 = [];
            if (zone.unit == "monthly") {
                var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
                var date = new Date(zone.start + '-01');

                var segmentWidth = 22;
                milestoneGap = 210;

                var offset = date.getDay();
                if (offset >= 0 && offset <= 6) {
                    date.setDate(date.getDate() - offset);
                    for (var i = 0; i <= offset; i++) {
                        milestones.push({ "label": "" + date.getDate() + "", "position": 100 + i * segmentWidth });
                        bars.push({ "width": segmentWidth, "class": days[date.getDay()], "position": 100 + bars.length * segmentWidth - segmentWidth / 2 });
                        date.setDate(date.getDate() + 1);
                    }
                }
                for (var _i = offset + 1; _i < 40; _i++) {
                    milestones.push({ "label": "" + date.getDate() + "", "position": 100 + _i * segmentWidth });
                    bars.push({ "width": segmentWidth, "class": days[date.getDay()], "position": 100 + bars.length * segmentWidth - segmentWidth / 2 });
                    date.setDate(date.getDate() + 1);
                    if (date.getDate() == 1) {
                        break;
                    }
                }
                if (offset > 0) {
                    var _title = hooks(date).subtract(1, 'month').format('MMMM YYYY');
                    level1.push({ "width": segmentWidth * (offset + 1), "label": _title, "class": "level1", "position": 100 - segmentWidth / 2 });
                }
                var title = hooks(date).format('MMMM YYYY');

                level1.push({ "width": segmentWidth * (bars.length - offset - 1), "label": title, "class": "level1", "position": 100 + (offset + 1) * segmentWidth - segmentWidth / 2 });

                zone.bars = bars;
            } else if (zone.unit == "quarterly") {

                var quarter = ["q1", "q2", "q3", "q4"];
                var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
                date = new Date(zone.start + '-20');

                var segments = 3 * 9; // 10 quarters

                milestoneGap = segments * 30;

                for (var _i2 = 0; _i2 < segments; _i2++) {
                    milestones.push({ "label": "" + months[date.getMonth()] + "", "position": 120 + (_i2 * 30 + 15) });
                    if (date.getMonth() % 3 == 0) {
                        bars.push({ "width": 90, "class": quarter[Math.floor(date.getMonth() / 3)], "position": 120 + bars.length * 90 });
                        if (date.getMonth() % 12 == 0) {
                            if (segments - _i2 < 12) {
                                level1.push({ "width": 30 * (segments - _i2) - 5, "label": date.getFullYear(), "class": "level1", "position": 120 + (bars.length - 1) * 90 });
                            } else {
                                level1.push({ "width": 30 * 3 * 4 - 5, "label": date.getFullYear(), "class": "level1", "position": 120 + (bars.length - 1) * 90 });
                            }
                        }
                    }
                    addMonths(date, 1);
                }

                zone.bars = bars;
            }

            obj.selectAll("text").data(milestones).enter().append("text").attr("text-anchor", "middle").attr("transform", function (d, i) {
                return "translate(" + d.position + ",40)";
            }).text(function (d) {
                return d.label;
            });

            var count = 0;
            obj.selectAll("line").data(zone.milestones).enter().append("line").attr("x1", function (d, i) {
                return 120 + milestoneGap * i;
            }).attr("x2", function (d, i) {
                return 120 + milestoneGap * i;
            }).attr("y1", zone.calculatedRectangle[1]).attr("y2", zone.calculatedRectangle[3]).attr("stroke", "gray").attr("stroke-width", "1");

            var banner = obj.append("g").selectAll("rect").data(level1).enter();

            banner.append("rect").attr("rx", 8).attr("width", function (d) {
                return d.width;
            }).attr("height", 18).attr("class", function (d) {
                return d.class;
            }).attr("y", 12).attr("x", function (d, i) {
                return d.position;
            });

            banner.append("text").attr("text-anchor", "middle").classed("banner", true).attr("transform", function (d, i) {
                return "translate(" + (d.width / 2 + d.position) + ",25)";
            }).text(function (d) {
                return d.label;
            });

            obj.append("g").selectAll("rect").data(bars).enter().append("rect").attr("width", function (d) {
                return d.width;
            }).attr("height", zone.calculatedRectangle[3] - 40).attr("class", function (d) {
                return d.class;
            }).attr("y", zone.calculatedRectangle[1] + 40).attr("x", function (d, i) {
                return d.position;
            });
        }
    }]);
    return Layout;
}(Layout$1);

__$styleInject("._carousel{fill:#ccc}._carousel .toc{fill:#000;font-size:.7em;font-family:Open Sans}._carousel .selected{fill:blue!important;font-weight:700!important}text.carousel_down,text.carousel_up{font-size:4em;fill:#999}", undefined);

var Layout$4 = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            for (var v = 0; v < 4; v++) {
                if (typeof zone.rectangle[v] == "string") {
                    //console.log("Evaluating: " + zone.rectangle[v]);
                    //console.log(" ... to : " + eval(zone.rectangle[v]));
                    zone.rectangle[v] = eval(zone.rectangle[v]);
                }
            }

            var rect = obj.append("rect").style("filter", "url(#drop-shadow)").attr("x", zone.rectangle[0]).attr("y", zone.rectangle[1]).attr("width", zone.rectangle[2]).attr("height", zone.rectangle[3]); //fff899
        }
    }, {
        key: 'enrichData',
        value: function enrichData(nodes) {
            //
            //      let segs = [];
            //      for ( let x = 0 ; x < graph.nodes.length; x++) {
            //          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //          if (seg != "" && segs.indexOf(seg) < 0) {
            //              segs.push(seg);
            //          }
            //      }

            this.recurse(nodes, 0);

            //      // calculate the segment layout
            //      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
            //      for ( let p = 0; p < segs.length; p++) {
            //          let tot = 0;
            //          let x,num;
            //          for ( x = 0 ; x < graph.nodes.length; x++) {
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //              if (seg == segs[p]) {
            //                  tot++;
            //              }
            //              if (graph.nodes[x].children) {
            //                  let ls = graph.nodes[x].children;
            //                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            //                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            //                      if (seg == segs[p]) {
            //                        tot++;
            //                      }
            //
            //                      if (ls[xL1].children) {
            //                          let lsL2 = ls[xL1].children;
            //                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            //                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            //                              if (seg == segs[p]) {
            //                                tot++;
            //                              }
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
            ////              this.recurse(graph.nodes, 1);
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            ////              if (seg == segs[p]) {
            ////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
            ////                  num++;
            ////              }
            ////              this.recurse(graph.nodes[x], 1);
            ////              if (graph.nodes[x].children) {
            ////                  let ls = graph.nodes[x].children;
            ////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            ////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            ////                      if (seg == segs[p]) {
            ////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
            ////                          numL1++;
            ////                      }
            ////                      this.recurse(ls[xL1], 2);
            //
            ////                      if (ls[xL1].children) {
            ////                          let lsL2 = ls[xL1].children;
            ////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            ////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            ////                              if (seg == segs[p]) {
            ////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
            ////                                  numL2++;
            ////                              }
            ////                          }
            ////                      }
            //
            //           //       }
            //            //  }
            //          }
            //      }
        }
    }, {
        key: 'recurse',
        value: function recurse(nodes, level) {
            var tot = nodes.length;
            for (var num = 0, x = 0; x < nodes.length; x++) {
                var nd = nodes[x];
                if (nd.type == "panel" || level > 0) {
                    nd.layout = { "seq": num, "total": tot, "seg": "SEG" + level, "segnum": level, "level": level };
                    num++;
                    if (nd.children) {
                        this.recurse(nd.children, level + 1);
                    }
                }
            }
        }
    }, {
        key: 'configEvents',
        value: function configEvents(d3visual, newNodesTotal, zones, viewer) {
            var self = this;

            var newNodes = newNodesTotal.filter(function (d) {
                return d.type == 'icon';
            });

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodesTotal.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                d3.event.stopPropagation();
                self.togglePanel(viewer, this.parentNode, e.name == "down"); /*self.toggleNavigation(viewer, e, this); */
            });

            function dragstarted(d) {
                if (!d3.event.active) force.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
                viewer.downX = Math.round(d.x + d.y);
            }

            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function dragended(d) {
                if (!d3.event.active) force.alphaTarget(0);
                d.fx = null;
                d.fy = null;
                setTimeout(function () {
                    viewer.trigger('change');
                }, 200);
            }

            newNodesTotal.call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

            newNodes.each(function (d, i) {
                if (i == 0) {
                    self.togglePanel(viewer, this.parentNode, true);
                }
            });
        }
    }, {
        key: 'getDim',
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'togglePanel',
        value: function togglePanel(viewer, root, down) {
            var panels = [];
            d3.select(root).selectAll("g._panel").each(function (d) {
                panels.push(d);
                d3.select(root).selectAll("g.node_" + d.name).style("display", "none");
            });

            if (viewer.chosenPanel >= 0) {
                var _data = panels[viewer.chosenPanel];
                d3.select(root).selectAll("g.node_" + _data.name).style("display", "none");
            } else {
                viewer.chosenPanel = -1;
            }

            if (down) {
                viewer.chosenPanel--;
                if (viewer.chosenPanel < 0) {
                    viewer.chosenPanel = 0; //(panels.length - 1);
                }
            } else {
                viewer.chosenPanel++;
                if (viewer.chosenPanel >= panels.length) {
                    viewer.chosenPanel = panels.length - 1;
                }
            }
            var data = panels[viewer.chosenPanel];
            d3.select(root).selectAll("g.node_" + data.name).style("display", "block");

            this.updateTableOfContents(viewer, root, panels);
        }
    }, {
        key: 'updateTableOfContents',
        value: function updateTableOfContents(viewer, root, panels) {
            d3.select(root).select("g._carousel").remove();

            d3.select(root).append("g").classed("_carousel", true).selectAll(".toc").data(panels).enter().append("text").classed("toc", true).style("fill", "black").classed("selected", function (x, i) {
                return i == viewer.chosenPanel ? true : false;
            }).attr("transform", function (x, i) {
                return "translate(" + 15 + "," + (25 + i * 14) + ")";
            }).text(function (x) {
                return x.label;
            });

            d3.select(root).selectAll(".toc").data(panels).exit().remove();
        }
    }, {
        key: 'toggleNavigation',
        value: function toggleNavigation(viewer, e, object) {
            d3.event.stopPropagation();

            if (viewer.downX == Math.round(e.x + e.y)) {
                viewer.navigation.toggle(e, d3.select(object));
            } else {
                viewer.navigation.close(e);
            }
        }
    }, {
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {}
    }]);
    return Layout;
}(Layout$1);

__$styleInject("._lifecycle{fill:orange;stroke:#ff8c00;stroke-width:1.5px}._lifecycle .iconText{font-family:FontAwesome;fill:#000;stroke-width:0;font-size:15px}._lifecycle .labelText{fill:#000;stroke-width:0;font-size:.35em;font-family:Arial}._lifecycle .arrow{stroke:#ff8c00;stroke-width:1.5px}._end_lifecycle{fill:#ff8c00}", undefined);

var Shape$13 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            chgSet.append("circle").attr("r", 15);

            chgSet.append("text").attr("class", "iconText").attr("text-anchor", "middle").attr("dx", 0).attr("dy", "0.1em").text(function (d) {
                var hex = d.title.icon.substr(3);return unescape('%u' + hex);
            });

            chgSet.append("text").attr("class", "labelText").attr("text-anchor", "middle").attr("dx", 0).attr("dy", "1.6em").text(function (d) {
                return d.title.label;
            });

            var build = chgSet.filter(function (d) {
                return d.title.label == "Build";
            });
            var learn = chgSet.filter(function (d) {
                return d.title.label == "Learn";
            });
            var measure = chgSet.filter(function (d) {
                return d.title.label == "Measure";
            });

            build.append("line").attr("class", "arrow").attr("x1", 0).attr("y1", 15).attr("x2", 0).attr("y2", 20);
            build.append("line").attr("class", "arrow").attr("marker-end", "url(#lifecycle)").attr("x1", 15).attr("y1", 0).attr("x2", 30).attr("y2", 0);

            measure.append("line").attr("class", "arrow").attr("marker-end", "url(#lifecycle)").attr("x1", -15).attr("y1", 0).attr("x2", -30).attr("y2", 0);
            measure.append("line").attr("class", "arrow").attr("x1", 0).attr("y1", -15).attr("x2", 0).attr("y2", -20);

            learn.append("line").attr("class", "arrow").attr("x1", 15).attr("y1", 0).attr("x2", 20).attr("y2", 0);
            learn.append("line").attr("class", "arrow").attr("marker-end", "url(#lifecycle)").attr("x1", 0).attr("y1", -15).attr("x2", 0).attr("y2", -30);
        }
    }]);
    return Shape;
}();

//export * from './ViewerCanvas';
//export * from './ShapeFactory';
//export * from './LayoutFactory';


var DUDE = function DUDE() {
    classCallCheck(this, DUDE);
};

ShapeFactory.register(new Shape("comment"));

ShapeFactory.register(new RectangleShape("application"));

ShapeFactory.register(new Shape$1("icon"));
ShapeFactory.register(new Shape$2("image"));

ShapeFactory.register(new Shape$3("circle"));
ShapeFactory.register(new Shape$3("circle2"));
ShapeFactory.register(new Shape$3("circle3"));

ShapeFactory.register(new Shape$4("milestone"));

ShapeFactory.register(new Shape$5("button"));

ShapeFactory.register(new Shape$6("timeline"));

ShapeFactory.register(new Shape$7("timeline2"));

ShapeFactory.register(new Shape$8("panel"));

ShapeFactory.register(new Shape$9("list"));

ShapeFactory.register(new Shape$10("dimension"));

ShapeFactory.register(new Shape$11("box"));

ShapeFactory.register(new Shape$12("tab"));

LayoutFactory.register(new Layout("swimlane"));

LayoutFactory.register(new Layout$1("default"));

LayoutFactory.register(new Layout$2("tabs"));

LayoutFactory.register(new Layout$3("timelines"));

LayoutFactory.register(new Layout$4("carousel"));

// Specifics
ShapeFactory.register(new Shape$13("lifecycle"));

ShapeFactory.clone('box', 'box2');

var version = "0.0.38";

exports.layouts = LayoutFactory;
exports.shapes = ShapeFactory;
exports.LayoutFactory = LayoutFactory;
exports.ShapeFactory = ShapeFactory;
exports.ViewerCanvas = ViewerCanvas;
exports.Registry = DUDE;
exports.version = version;

}((this.iksplor = this.iksplor || {}),d3));
