(function (exports,d3) {
'use strict';

function __$styleInject(css, returnValue) {
  if (typeof document === 'undefined') {
    return returnValue;
  }
  css = css || '';
  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet){
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
  head.appendChild(style);
  return returnValue;
}

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};











var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

//import BuildingBlockShape from './shape-catalog/BuildingBlock';

var _ShapeFactory = function () {
    function _ShapeFactory() {
        classCallCheck(this, _ShapeFactory);

        this.shapes = {};
    }

    createClass(_ShapeFactory, [{
        key: "getShapeNames",
        value: function getShapeNames() {
            return Object.keys(this.shapes);
        }
    }, {
        key: "getShape",
        value: function getShape(shape) {
            if (!this.shapes.hasOwnProperty(shape)) {
                return null;
            }
            return this.shapes[shape];
        }
    }, {
        key: "register",
        value: function register(object) {
            this.shapes[object.domainType] = object;
            return this;
        }
    }]);
    return _ShapeFactory;
}();

var ShapeFactory = new _ShapeFactory();

var Navigation = function () {
  function Navigation() {
    classCallCheck(this, Navigation);
  }

  createClass(Navigation, [{
    key: "attach",
    value: function attach(svg, viewer, contentNode) {
      var self = this;
      this.svg = svg;
      this.contentNode = contentNode;
      this.viewer = viewer;

      svg.append("g").attr("class", "nav");
      this.state = { "mode": "closed", "selected": -1 };

      this.menuItems = [];

      viewer.on("closeNavigation", function (e) {
        self.close();
      });
    }
  }, {
    key: "addMenuItem",
    value: function addMenuItem(item) {
      this.menuItems.push(item);
    }
  }, {
    key: "update",
    value: function update() {
      var self = this;
      var vis = this.svg;

      this.root = vis.select("g.nav").append("g");

      this.backdrop = vis.select("g.nav").select("g").append("rect").attr("fill", "#999999").attr("x", 0).attr("y", 0).attr("rx", 6).attr("ry", 6).attr("width", "280").attr("height", "80").style("visibility", "hidden");

      var menuNodes = vis.select("g.nav").select("g").selectAll("g.navDetail").data(this.menuItems);

      var navNodes = menuNodes.enter().append("g").attr("class", "navDetail").style("visibility", "hidden");

      menuNodes.exit().remove();

      navNodes.append("rect").attr("fill", "white").attr("x", 2).attr("y", 4).attr("rx", 6).attr("ry", 6).attr("transform", "rotate(35)").attr("width", "55").attr("height", "12");

      navNodes.append("circle").style("cursor", "default").attr("fill", "black").attr("stroke", "white").attr("stroke-width", "2").attr("r", "10");

      navNodes.append("text").style("cursor", "default").attr("dx", 0).attr("dy", "0").attr("text-anchor", "middle").attr('dominant-baseline', 'central').style('font-family', 'FontAwesome').style('font-size', '10px').style("fill", "white").text(function (d) {
        return d.icon;
      });

      navNodes.append("text").style("cursor", "default").attr("dx", 8).attr("dy", 12).attr("fill", "black").attr("text-anchor", "left").style('font-size', '0.4em').attr("transform", "rotate(35)").text(function (d) {
        return d.title;
      });

      this.backdrop.on('click', function (e) {
        self.close();
      });

      navNodes.on('click', function (e) {
        self.viewer.trigger("shapeNavClick", { "data": self.state.data, "menuItem": e });
        //self.close();
      });

      navNodes.selectAll('circle,text,rect').on('mouseenter', function (nodeSet, index) {
        d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", true);
      }).on('mouseleave', function (e) {
        d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", false);
        //            vis.select(this).selectAll('circle').removeClass("nav-hover");
      });
    }
  }, {
    key: "toggle",
    value: function toggle(e, matchedItem) {
      var self = this;
      var pos = null; //{x:e.x,y:e.y};
      var state = self.state;

      if (typeof state.newNode != "undefined" && state.newNode != null) {
        //console.log("REMOVING: " + state.newNode);
        state.newNode.parentNode.removeChild(state.newNode);
        state.newNode = null;
      }

      var notes = [];
      if (matchedItem) {
        matchedItem.each(function (d) {
          var bounded = this.getBBox();
          self.state.newNode = this.cloneNode(true);

          self.svg.select("g.nav").node().appendChild(self.state.newNode);
          d3.select(self.state.newNode).on('click', function (e) {
            self.close();
          });

          pos = { left: e.x + bounded.x, top: e.y + bounded.y, width: bounded.width, height: bounded.height, bounding: this.getBoundingClientRect() };

          notes = d.hasOwnProperty('notes') ? d.notes : [];
          self.state.data = d;
        });
      }
      if (this.state.mode == "closed") {
        this.doOpen(e, pos, notes);
      } else {
        if (this.state.selected != e.index) {
          this.doOpen(e, pos, notes);
        } else {
          this.close(e);
        }
      }
    }
  }, {
    key: "open",
    value: function open(e) {
      this.doOpen(e, { x: e.x, y: e.y });
    }
  }, {
    key: "doOpen",
    value: function doOpen(e, position, notes) {
      var self = this;
      this.state.x = position.left;
      this.state.y = position.top;
      this.state.selected = e.index;
      this.state.mode = "open";

      var scale = 1.6;
      var delta = 0;

      var start = 198;
      // calculate positions of menu items
      var positions = [0];
      for (var i = 0; i < 10; i++) {
        if (start + 36 * i > 360) {
          positions.push(start + 36 * i - 360);
        } else {
          positions.push(start + 36 * i);
        }
      }

      var vis = this.svg;

      // Determine how big the height should be based on the "notes" that are provided
      // Move a navigation DIV to the same position for displaying
      var element = this.contentNode.parentNode;
      var bodyRect = document.body.getBoundingClientRect(),
          elemRect = element.getBoundingClientRect(),
          offset = elemRect.top - bodyRect.top;

      this.printRect(position);
      this.printRect(elemRect);
      this.printRect(bodyRect);

      var minHeight = Math.max(position.bounding.height + 5, self.menuItems == 0 ? 50 : 145);

      d3.select(this.contentNode).classed("content", true).style("display", "block").style("position", "absolute").style("width", "" + (self.menuItems.length * 38 + position.bounding.width) + "px").style("left", "" + position.bounding.left + "px").style("top", "" + (-elemRect.top + position.bounding.top + minHeight) + "px");

      d3.select(this.contentNode).selectAll("table").remove();

      var noteNode = d3.select(this.contentNode).append("table").selectAll("tr").data(notes);
      noteNode.enter().append("tr").html(function (d) {
        if (typeof d == "string") {
          return d;
        } else {
          return "<td class='key'>" + d.key + "</td><td class='value'>" + d.value + "</td>";
        }
      });

      var dims = this.contentNode.getBoundingClientRect();
      this.printRect(dims);

      var offset2 = dims.top - elemRect.top;

      //console.log("OFFSET = "+offset2+", "+dims.top);
      var contentHeight = dims.height;

      vis.select("g.nav").selectAll('g.node')
      //.attr("transform", "translate(0,0)")
      .style("pointer-events", "none");

      vis.select("g.nav").select("g").attr("transform", function (d) {
        return "translate(" + (position.left + position.width / 2) + "," + (position.top + position.height / 2) + ")";
      });
      vis.select("g.nav").select('rect').attr("opacity", 0).style("visibility", "visible").attr("transform", function (d) {
        return "translate(" + (-position.width / 2 - 5) + "," + (-position.height / 2 - 5) + ")";
      }).attr("width", function (d) {
        return (self.menuItems.length ? 0 : 0) + self.menuItems.length * 38 + position.width + 10;
      }).attr("height", function (d) {
        return 0 + Math.max(position.height + contentHeight, 60) + 0;
      }).transition().attr("opacity", 0.9).on("end", function () {
        return;
      });

      var menuPosition = 0;
      vis.select("g.nav").selectAll('g.navDetail').style("opacity", 0).style("visibility", "visible").attr("transform", function (d) {
        return "translate(" + 0 + "," + (20 + delta) + ") scale(" + scale + ")";
      }).transition().style("opacity", 100).attr("transform", function (d) {
        var y = -position.height / 2 + 20 + delta;
        var x = position.width / 2 + (22 + 1) * menuPosition++ * scale + 25;
        return "translate(" + x + "," + y + ") scale(" + scale + ")";
      });
    }
  }, {
    key: "close",
    value: function close() {
      var state = this.state;
      var vis = this.svg;

      if (state.mode == "closed") {
        return;
      }
      state.mode = "closed";

      d3.select(this.contentNode).style("display", "none");

      vis.select("g.nav").selectAll('g.navDetail').transition().style("opacity", 0).attr("transform", function () {
        return "translate(" + state.x + "," + (state.y + 40) + ")";
      }).on("end", function () {
        vis.select("g.nav").selectAll('g.navDetail').style("visibility", "hidden");
      });

      if (state.newNode) {
        //console.log("REMOVING: " + state.newNode);
        state.newNode.parentNode.removeChild(state.newNode);
        state.newNode = null;
        state.data = null;
      }

      vis.select("g.nav").select('rect').transition().attr("opacity", 0).on("end", function () {
        vis.select("g.nav").select('rect').style("visibility", "hidden");
      });
    }
  }, {
    key: "printRect",
    value: function printRect(elemRect) {
      console.log("E: Top=" + elemRect.top + ",Left=" + elemRect.left + ",Width=" + elemRect.width + ",Height=" + elemRect.height);
    }
  }]);
  return Navigation;
}();

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var index = createCommonjsModule(function (module) {
var fa=function(i){return fa[i.replace(/-./g,function(x){return x.substr(1).toUpperCase()})]};fa.glass="\uf000";fa.music="\uf001";fa.search="\uf002";fa.envelopeO="\uf003";fa.heart="\uf004";fa.star="\uf005";fa.starO="\uf006";fa.user="\uf007";fa.film="\uf008";fa.thLarge="\uf009";fa.th="\uf00a";fa.thList="\uf00b";fa.check="\uf00c";fa.remove="\uf00d";fa.close="\uf00d";fa.times="\uf00d";fa.searchPlus="\uf00e";fa.searchMinus="\uf010";fa.powerOff="\uf011";fa.signal="\uf012";fa.gear="\uf013";fa.cog="\uf013";fa.trashO="\uf014";fa.home="\uf015";fa.fileO="\uf016";fa.clockO="\uf017";fa.road="\uf018";fa.download="\uf019";fa.arrowCircleODown="\uf01a";fa.arrowCircleOUp="\uf01b";fa.inbox="\uf01c";fa.playCircleO="\uf01d";fa.rotateRight="\uf01e";fa.repeat="\uf01e";fa.refresh="\uf021";fa.listAlt="\uf022";fa.lock="\uf023";fa.flag="\uf024";fa.headphones="\uf025";fa.volumeOff="\uf026";fa.volumeDown="\uf027";fa.volumeUp="\uf028";fa.qrcode="\uf029";fa.barcode="\uf02a";fa.tag="\uf02b";fa.tags="\uf02c";fa.book="\uf02d";fa.bookmark="\uf02e";fa.print="\uf02f";fa.camera="\uf030";fa.font="\uf031";fa.bold="\uf032";fa.italic="\uf033";fa.textHeight="\uf034";fa.textWidth="\uf035";fa.alignLeft="\uf036";fa.alignCenter="\uf037";fa.alignRight="\uf038";fa.alignJustify="\uf039";fa.list="\uf03a";fa.dedent="\uf03b";fa.outdent="\uf03b";fa.indent="\uf03c";fa.videoCamera="\uf03d";fa.photo="\uf03e";fa.image="\uf03e";fa.pictureO="\uf03e";fa.pencil="\uf040";fa.mapMarker="\uf041";fa.adjust="\uf042";fa.tint="\uf043";fa.edit="\uf044";fa.pencilSquareO="\uf044";fa.shareSquareO="\uf045";fa.checkSquareO="\uf046";fa.arrows="\uf047";fa.stepBackward="\uf048";fa.fastBackward="\uf049";fa.backward="\uf04a";fa.play="\uf04b";fa.pause="\uf04c";fa.stop="\uf04d";fa.forward="\uf04e";fa.fastForward="\uf050";fa.stepForward="\uf051";fa.eject="\uf052";fa.chevronLeft="\uf053";fa.chevronRight="\uf054";fa.plusCircle="\uf055";fa.minusCircle="\uf056";fa.timesCircle="\uf057";fa.checkCircle="\uf058";fa.questionCircle="\uf059";fa.infoCircle="\uf05a";fa.crosshairs="\uf05b";fa.timesCircleO="\uf05c";fa.checkCircleO="\uf05d";fa.ban="\uf05e";fa.arrowLeft="\uf060";fa.arrowRight="\uf061";fa.arrowUp="\uf062";fa.arrowDown="\uf063";fa.mailForward="\uf064";fa.share="\uf064";fa.expand="\uf065";fa.compress="\uf066";fa.plus="\uf067";fa.minus="\uf068";fa.asterisk="\uf069";fa.exclamationCircle="\uf06a";fa.gift="\uf06b";fa.leaf="\uf06c";fa.fire="\uf06d";fa.eye="\uf06e";fa.eyeSlash="\uf070";fa.warning="\uf071";fa.exclamationTriangle="\uf071";fa.plane="\uf072";fa.calendar="\uf073";fa.random="\uf074";fa.comment="\uf075";fa.magnet="\uf076";fa.chevronUp="\uf077";fa.chevronDown="\uf078";fa.retweet="\uf079";fa.shoppingCart="\uf07a";fa.folder="\uf07b";fa.folderOpen="\uf07c";fa.arrowsV="\uf07d";fa.arrowsH="\uf07e";fa.barChartO="\uf080";fa.barChart="\uf080";fa.twitterSquare="\uf081";fa.facebookSquare="\uf082";fa.cameraRetro="\uf083";fa.key="\uf084";fa.gears="\uf085";fa.cogs="\uf085";fa.comments="\uf086";fa.thumbsOUp="\uf087";fa.thumbsODown="\uf088";fa.starHalf="\uf089";fa.heartO="\uf08a";fa.signOut="\uf08b";fa.linkedinSquare="\uf08c";fa.thumbTack="\uf08d";fa.externalLink="\uf08e";fa.signIn="\uf090";fa.trophy="\uf091";fa.githubSquare="\uf092";fa.upload="\uf093";fa.lemonO="\uf094";fa.phone="\uf095";fa.squareO="\uf096";fa.bookmarkO="\uf097";fa.phoneSquare="\uf098";fa.twitter="\uf099";fa.facebookF="\uf09a";fa.facebook="\uf09a";fa.github="\uf09b";fa.unlock="\uf09c";fa.creditCard="\uf09d";fa.feed="\uf09e";fa.rss="\uf09e";fa.hddO="\uf0a0";fa.bullhorn="\uf0a1";fa.bell="\uf0f3";fa.certificate="\uf0a3";fa.handORight="\uf0a4";fa.handOLeft="\uf0a5";fa.handOUp="\uf0a6";fa.handODown="\uf0a7";fa.arrowCircleLeft="\uf0a8";fa.arrowCircleRight="\uf0a9";fa.arrowCircleUp="\uf0aa";fa.arrowCircleDown="\uf0ab";fa.globe="\uf0ac";fa.wrench="\uf0ad";fa.tasks="\uf0ae";fa.filter="\uf0b0";fa.briefcase="\uf0b1";fa.arrowsAlt="\uf0b2";fa.group="\uf0c0";fa.users="\uf0c0";fa.chain="\uf0c1";fa.link="\uf0c1";fa.cloud="\uf0c2";fa.flask="\uf0c3";fa.cut="\uf0c4";fa.scissors="\uf0c4";fa.copy="\uf0c5";fa.filesO="\uf0c5";fa.paperclip="\uf0c6";fa.save="\uf0c7";fa.floppyO="\uf0c7";fa.square="\uf0c8";fa.navicon="\uf0c9";fa.reorder="\uf0c9";fa.bars="\uf0c9";fa.listUl="\uf0ca";fa.listOl="\uf0cb";fa.strikethrough="\uf0cc";fa.underline="\uf0cd";fa.table="\uf0ce";fa.magic="\uf0d0";fa.truck="\uf0d1";fa.pinterest="\uf0d2";fa.pinterestSquare="\uf0d3";fa.googlePlusSquare="\uf0d4";fa.googlePlus="\uf0d5";fa.money="\uf0d6";fa.caretDown="\uf0d7";fa.caretUp="\uf0d8";fa.caretLeft="\uf0d9";fa.caretRight="\uf0da";fa.columns="\uf0db";fa.unsorted="\uf0dc";fa.sort="\uf0dc";fa.sortDown="\uf0dd";fa.sortDesc="\uf0dd";fa.sortUp="\uf0de";fa.sortAsc="\uf0de";fa.envelope="\uf0e0";fa.linkedin="\uf0e1";fa.rotateLeft="\uf0e2";fa.undo="\uf0e2";fa.legal="\uf0e3";fa.gavel="\uf0e3";fa.dashboard="\uf0e4";fa.tachometer="\uf0e4";fa.commentO="\uf0e5";fa.commentsO="\uf0e6";fa.flash="\uf0e7";fa.bolt="\uf0e7";fa.sitemap="\uf0e8";fa.umbrella="\uf0e9";fa.paste="\uf0ea";fa.clipboard="\uf0ea";fa.lightbulbO="\uf0eb";fa.exchange="\uf0ec";fa.cloudDownload="\uf0ed";fa.cloudUpload="\uf0ee";fa.userMd="\uf0f0";fa.stethoscope="\uf0f1";fa.suitcase="\uf0f2";fa.bellO="\uf0a2";fa.coffee="\uf0f4";fa.cutlery="\uf0f5";fa.fileTextO="\uf0f6";fa.buildingO="\uf0f7";fa.hospitalO="\uf0f8";fa.ambulance="\uf0f9";fa.medkit="\uf0fa";fa.fighterJet="\uf0fb";fa.beer="\uf0fc";fa.hSquare="\uf0fd";fa.plusSquare="\uf0fe";fa.angleDoubleLeft="\uf100";fa.angleDoubleRight="\uf101";fa.angleDoubleUp="\uf102";fa.angleDoubleDown="\uf103";fa.angleLeft="\uf104";fa.angleRight="\uf105";fa.angleUp="\uf106";fa.angleDown="\uf107";fa.desktop="\uf108";fa.laptop="\uf109";fa.tablet="\uf10a";fa.mobilePhone="\uf10b";fa.mobile="\uf10b";fa.circleO="\uf10c";fa.quoteLeft="\uf10d";fa.quoteRight="\uf10e";fa.spinner="\uf110";fa.circle="\uf111";fa.mailReply="\uf112";fa.reply="\uf112";fa.githubAlt="\uf113";fa.folderO="\uf114";fa.folderOpenO="\uf115";fa.smileO="\uf118";fa.frownO="\uf119";fa.mehO="\uf11a";fa.gamepad="\uf11b";fa.keyboardO="\uf11c";fa.flagO="\uf11d";fa.flagCheckered="\uf11e";fa.terminal="\uf120";fa.code="\uf121";fa.mailReplyAll="\uf122";fa.replyAll="\uf122";fa.starHalfEmpty="\uf123";fa.starHalfFull="\uf123";fa.starHalfO="\uf123";fa.locationArrow="\uf124";fa.crop="\uf125";fa.codeFork="\uf126";fa.unlink="\uf127";fa.chainBroken="\uf127";fa.question="\uf128";fa.info="\uf129";fa.exclamation="\uf12a";fa.superscript="\uf12b";fa.subscript="\uf12c";fa.eraser="\uf12d";fa.puzzlePiece="\uf12e";fa.microphone="\uf130";fa.microphoneSlash="\uf131";fa.shield="\uf132";fa.calendarO="\uf133";fa.fireExtinguisher="\uf134";fa.rocket="\uf135";fa.maxcdn="\uf136";fa.chevronCircleLeft="\uf137";fa.chevronCircleRight="\uf138";fa.chevronCircleUp="\uf139";fa.chevronCircleDown="\uf13a";fa.anchor="\uf13d";fa.unlockAlt="\uf13e";fa.bullseye="\uf140";fa.ellipsisH="\uf141";fa.ellipsisV="\uf142";fa.rssSquare="\uf143";fa.playCircle="\uf144";fa.ticket="\uf145";fa.minusSquare="\uf146";fa.minusSquareO="\uf147";fa.levelUp="\uf148";fa.levelDown="\uf149";fa.checkSquare="\uf14a";fa.pencilSquare="\uf14b";fa.externalLinkSquare="\uf14c";fa.shareSquare="\uf14d";fa.compass="\uf14e";fa.toggleDown="\uf150";fa.caretSquareODown="\uf150";fa.toggleUp="\uf151";fa.caretSquareOUp="\uf151";fa.toggleRight="\uf152";fa.caretSquareORight="\uf152";fa.euro="\uf153";fa.eur="\uf153";fa.gbp="\uf154";fa.dollar="\uf155";fa.usd="\uf155";fa.rupee="\uf156";fa.inr="\uf156";fa.cny="\uf157";fa.rmb="\uf157";fa.yen="\uf157";fa.jpy="\uf157";fa.ruble="\uf158";fa.rouble="\uf158";fa.rub="\uf158";fa.won="\uf159";fa.krw="\uf159";fa.bitcoin="\uf15a";fa.btc="\uf15a";fa.file="\uf15b";fa.fileText="\uf15c";fa.sortAlphaAsc="\uf15d";fa.sortAlphaDesc="\uf15e";fa.sortAmountAsc="\uf160";fa.sortAmountDesc="\uf161";fa.sortNumericAsc="\uf162";fa.sortNumericDesc="\uf163";fa.thumbsUp="\uf164";fa.thumbsDown="\uf165";fa.youtubeSquare="\uf166";fa.youtube="\uf167";fa.xing="\uf168";fa.xingSquare="\uf169";fa.youtubePlay="\uf16a";fa.dropbox="\uf16b";fa.stackOverflow="\uf16c";fa.instagram="\uf16d";fa.flickr="\uf16e";fa.adn="\uf170";fa.bitbucket="\uf171";fa.bitbucketSquare="\uf172";fa.tumblr="\uf173";fa.tumblrSquare="\uf174";fa.longArrowDown="\uf175";fa.longArrowUp="\uf176";fa.longArrowLeft="\uf177";fa.longArrowRight="\uf178";fa.apple="\uf179";fa.windows="\uf17a";fa.android="\uf17b";fa.linux="\uf17c";fa.dribbble="\uf17d";fa.skype="\uf17e";fa.foursquare="\uf180";fa.trello="\uf181";fa.female="\uf182";fa.male="\uf183";fa.gittip="\uf184";fa.gratipay="\uf184";fa.sunO="\uf185";fa.moonO="\uf186";fa.archive="\uf187";fa.bug="\uf188";fa.vk="\uf189";fa.weibo="\uf18a";fa.renren="\uf18b";fa.pagelines="\uf18c";fa.stackExchange="\uf18d";fa.arrowCircleORight="\uf18e";fa.arrowCircleOLeft="\uf190";fa.toggleLeft="\uf191";fa.caretSquareOLeft="\uf191";fa.dotCircleO="\uf192";fa.wheelchair="\uf193";fa.vimeoSquare="\uf194";fa.turkishLira="\uf195";fa.try="\uf195";fa.plusSquareO="\uf196";fa.spaceShuttle="\uf197";fa.slack="\uf198";fa.envelopeSquare="\uf199";fa.wordpress="\uf19a";fa.openid="\uf19b";fa.institution="\uf19c";fa.bank="\uf19c";fa.university="\uf19c";fa.mortarBoard="\uf19d";fa.graduationCap="\uf19d";fa.yahoo="\uf19e";fa.google="\uf1a0";fa.reddit="\uf1a1";fa.redditSquare="\uf1a2";fa.stumbleuponCircle="\uf1a3";fa.stumbleupon="\uf1a4";fa.delicious="\uf1a5";fa.digg="\uf1a6";fa.piedPiperPp="\uf1a7";fa.piedPiperAlt="\uf1a8";fa.drupal="\uf1a9";fa.joomla="\uf1aa";fa.language="\uf1ab";fa.fax="\uf1ac";fa.building="\uf1ad";fa.child="\uf1ae";fa.paw="\uf1b0";fa.spoon="\uf1b1";fa.cube="\uf1b2";fa.cubes="\uf1b3";fa.behance="\uf1b4";fa.behanceSquare="\uf1b5";fa.steam="\uf1b6";fa.steamSquare="\uf1b7";fa.recycle="\uf1b8";fa.automobile="\uf1b9";fa.car="\uf1b9";fa.cab="\uf1ba";fa.taxi="\uf1ba";fa.tree="\uf1bb";fa.spotify="\uf1bc";fa.deviantart="\uf1bd";fa.soundcloud="\uf1be";fa.database="\uf1c0";fa.filePdfO="\uf1c1";fa.fileWordO="\uf1c2";fa.fileExcelO="\uf1c3";fa.filePowerpointO="\uf1c4";fa.filePhotoO="\uf1c5";fa.filePictureO="\uf1c5";fa.fileImageO="\uf1c5";fa.fileZipO="\uf1c6";fa.fileArchiveO="\uf1c6";fa.fileSoundO="\uf1c7";fa.fileAudioO="\uf1c7";fa.fileMovieO="\uf1c8";fa.fileVideoO="\uf1c8";fa.fileCodeO="\uf1c9";fa.vine="\uf1ca";fa.codepen="\uf1cb";fa.jsfiddle="\uf1cc";fa.lifeBouy="\uf1cd";fa.lifeBuoy="\uf1cd";fa.lifeSaver="\uf1cd";fa.support="\uf1cd";fa.lifeRing="\uf1cd";fa.circleONotch="\uf1ce";fa.ra="\uf1d0";fa.resistance="\uf1d0";fa.rebel="\uf1d0";fa.ge="\uf1d1";fa.empire="\uf1d1";fa.gitSquare="\uf1d2";fa.git="\uf1d3";fa.yCombinatorSquare="\uf1d4";fa.ycSquare="\uf1d4";fa.hackerNews="\uf1d4";fa.tencentWeibo="\uf1d5";fa.qq="\uf1d6";fa.wechat="\uf1d7";fa.weixin="\uf1d7";fa.send="\uf1d8";fa.paperPlane="\uf1d8";fa.sendO="\uf1d9";fa.paperPlaneO="\uf1d9";fa.history="\uf1da";fa.circleThin="\uf1db";fa.header="\uf1dc";fa.paragraph="\uf1dd";fa.sliders="\uf1de";fa.shareAlt="\uf1e0";fa.shareAltSquare="\uf1e1";fa.bomb="\uf1e2";fa.soccerBallO="\uf1e3";fa.futbolO="\uf1e3";fa.tty="\uf1e4";fa.binoculars="\uf1e5";fa.plug="\uf1e6";fa.slideshare="\uf1e7";fa.twitch="\uf1e8";fa.yelp="\uf1e9";fa.newspaperO="\uf1ea";fa.wifi="\uf1eb";fa.calculator="\uf1ec";fa.paypal="\uf1ed";fa.googleWallet="\uf1ee";fa.ccVisa="\uf1f0";fa.ccMastercard="\uf1f1";fa.ccDiscover="\uf1f2";fa.ccAmex="\uf1f3";fa.ccPaypal="\uf1f4";fa.ccStripe="\uf1f5";fa.bellSlash="\uf1f6";fa.bellSlashO="\uf1f7";fa.trash="\uf1f8";fa.copyright="\uf1f9";fa.at="\uf1fa";fa.eyedropper="\uf1fb";fa.paintBrush="\uf1fc";fa.birthdayCake="\uf1fd";fa.areaChart="\uf1fe";fa.pieChart="\uf200";fa.lineChart="\uf201";fa.lastfm="\uf202";fa.lastfmSquare="\uf203";fa.toggleOff="\uf204";fa.toggleOn="\uf205";fa.bicycle="\uf206";fa.bus="\uf207";fa.ioxhost="\uf208";fa.angellist="\uf209";fa.cc="\uf20a";fa.shekel="\uf20b";fa.sheqel="\uf20b";fa.ils="\uf20b";fa.meanpath="\uf20c";fa.buysellads="\uf20d";fa.connectdevelop="\uf20e";fa.dashcube="\uf210";fa.forumbee="\uf211";fa.leanpub="\uf212";fa.sellsy="\uf213";fa.shirtsinbulk="\uf214";fa.simplybuilt="\uf215";fa.skyatlas="\uf216";fa.cartPlus="\uf217";fa.cartArrowDown="\uf218";fa.diamond="\uf219";fa.ship="\uf21a";fa.userSecret="\uf21b";fa.motorcycle="\uf21c";fa.streetView="\uf21d";fa.heartbeat="\uf21e";fa.venus="\uf221";fa.mars="\uf222";fa.mercury="\uf223";fa.intersex="\uf224";fa.transgender="\uf224";fa.transgenderAlt="\uf225";fa.venusDouble="\uf226";fa.marsDouble="\uf227";fa.venusMars="\uf228";fa.marsStroke="\uf229";fa.marsStrokeV="\uf22a";fa.marsStrokeH="\uf22b";fa.neuter="\uf22c";fa.genderless="\uf22d";fa.facebookOfficial="\uf230";fa.pinterestP="\uf231";fa.whatsapp="\uf232";fa.server="\uf233";fa.userPlus="\uf234";fa.userTimes="\uf235";fa.hotel="\uf236";fa.bed="\uf236";fa.viacoin="\uf237";fa.train="\uf238";fa.subway="\uf239";fa.medium="\uf23a";fa.yc="\uf23b";fa.yCombinator="\uf23b";fa.optinMonster="\uf23c";fa.opencart="\uf23d";fa.expeditedssl="\uf23e";fa.mousePointer="\uf245";fa.iCursor="\uf246";fa.objectGroup="\uf247";fa.objectUngroup="\uf248";fa.stickyNote="\uf249";fa.stickyNoteO="\uf24a";fa.ccJcb="\uf24b";fa.ccDinersClub="\uf24c";fa.clone="\uf24d";fa.balanceScale="\uf24e";fa.hourglassO="\uf250";fa.hourglass="\uf254";fa.handGrabO="\uf255";fa.handRockO="\uf255";fa.handStopO="\uf256";fa.handPaperO="\uf256";fa.handScissorsO="\uf257";fa.handLizardO="\uf258";fa.handSpockO="\uf259";fa.handPointerO="\uf25a";fa.handPeaceO="\uf25b";fa.trademark="\uf25c";fa.registered="\uf25d";fa.creativeCommons="\uf25e";fa.gg="\uf260";fa.ggCircle="\uf261";fa.tripadvisor="\uf262";fa.odnoklassniki="\uf263";fa.odnoklassnikiSquare="\uf264";fa.getPocket="\uf265";fa.wikipediaW="\uf266";fa.safari="\uf267";fa.chrome="\uf268";fa.firefox="\uf269";fa.opera="\uf26a";fa.internetExplorer="\uf26b";fa.tv="\uf26c";fa.television="\uf26c";fa.contao="\uf26d";fa.amazon="\uf270";fa.calendarPlusO="\uf271";fa.calendarMinusO="\uf272";fa.calendarTimesO="\uf273";fa.calendarCheckO="\uf274";fa.industry="\uf275";fa.mapPin="\uf276";fa.mapSigns="\uf277";fa.mapO="\uf278";fa.map="\uf279";fa.commenting="\uf27a";fa.commentingO="\uf27b";fa.houzz="\uf27c";fa.vimeo="\uf27d";fa.blackTie="\uf27e";fa.fonticons="\uf280";fa.redditAlien="\uf281";fa.edge="\uf282";fa.creditCardAlt="\uf283";fa.codiepie="\uf284";fa.modx="\uf285";fa.fortAwesome="\uf286";fa.usb="\uf287";fa.productHunt="\uf288";fa.mixcloud="\uf289";fa.scribd="\uf28a";fa.pauseCircle="\uf28b";fa.pauseCircleO="\uf28c";fa.stopCircle="\uf28d";fa.stopCircleO="\uf28e";fa.shoppingBag="\uf290";fa.shoppingBasket="\uf291";fa.hashtag="\uf292";fa.bluetooth="\uf293";fa.bluetoothB="\uf294";fa.percent="\uf295";fa.gitlab="\uf296";fa.wpbeginner="\uf297";fa.wpforms="\uf298";fa.envira="\uf299";fa.universalAccess="\uf29a";fa.wheelchairAlt="\uf29b";fa.questionCircleO="\uf29c";fa.blind="\uf29d";fa.audioDescription="\uf29e";fa.volumeControlPhone="\uf2a0";fa.braille="\uf2a1";fa.assistiveListeningSystems="\uf2a2";fa.aslInterpreting="\uf2a3";fa.americanSignLanguageInterpreting="\uf2a3";fa.deafness="\uf2a4";fa.hardOfHearing="\uf2a4";fa.deaf="\uf2a4";fa.glide="\uf2a5";fa.glideG="\uf2a6";fa.signing="\uf2a7";fa.signLanguage="\uf2a7";fa.lowVision="\uf2a8";fa.viadeo="\uf2a9";fa.viadeoSquare="\uf2aa";fa.snapchat="\uf2ab";fa.snapchatGhost="\uf2ac";fa.snapchatSquare="\uf2ad";fa.piedPiper="\uf2ae";fa.firstOrder="\uf2b0";fa.yoast="\uf2b1";fa.themeisle="\uf2b2";fa.googlePlusCircle="\uf2b3";fa.googlePlusOfficial="\uf2b3";fa.fa="\uf2b4";fa.fontAwesome="\uf2b4";fa.handshakeO="\uf2b5";fa.envelopeOpen="\uf2b6";fa.envelopeOpenO="\uf2b7";fa.linode="\uf2b8";fa.addressBook="\uf2b9";fa.addressBookO="\uf2ba";fa.vcard="\uf2bb";fa.addressCard="\uf2bb";fa.vcardO="\uf2bc";fa.addressCardO="\uf2bc";fa.userCircle="\uf2bd";fa.userCircleO="\uf2be";fa.userO="\uf2c0";fa.idBadge="\uf2c1";fa.driversLicense="\uf2c2";fa.idCard="\uf2c2";fa.driversLicenseO="\uf2c3";fa.idCardO="\uf2c3";fa.quora="\uf2c4";fa.freeCodeCamp="\uf2c5";fa.telegram="\uf2c6";fa.shower="\uf2cc";fa.podcast="\uf2ce";fa.windowMaximize="\uf2d0";fa.windowMinimize="\uf2d1";fa.windowRestore="\uf2d2";fa.timesRectangle="\uf2d3";fa.windowClose="\uf2d3";fa.timesRectangleO="\uf2d4";fa.windowCloseO="\uf2d4";fa.bandcamp="\uf2d5";fa.grav="\uf2d6";fa.etsy="\uf2d7";fa.imdb="\uf2d8";fa.ravelry="\uf2d9";fa.eercast="\uf2da";fa.microchip="\uf2db";fa.snowflakeO="\uf2dc";fa.superpowers="\uf2dd";fa.wpexplorer="\uf2de";fa.meetup="\uf2e0";module.exports=fa;
});

var fa = (index && typeof index === 'object' && 'default' in index ? index['default'] : index);

//import BuildingBlockShape from './shape-catalog/BuildingBlock';

var _LayoutFactory = function () {
    function _LayoutFactory() {
        classCallCheck(this, _LayoutFactory);

        this.layouts = {};
    }

    createClass(_LayoutFactory, [{
        key: "getLayoutNames",
        value: function getLayoutNames() {
            return Object.keys(this.layouts);
        }
    }, {
        key: "getLayout",
        value: function getLayout(layout) {
            if (!this.layouts.hasOwnProperty(layout)) {
                return null;
            }
            return this.layouts[layout];
        }
    }, {
        key: "register",
        value: function register(object) {
            this.layouts[object.domainType] = object;
            return this;
        }
    }]);
    return _LayoutFactory;
}();

var LayoutFactory = new _LayoutFactory();

var ViewerCanvas = function () {
    function ViewerCanvas() {
        classCallCheck(this, ViewerCanvas);

        this.defaultConfig = {
            "enable_zone_boundaries": false,
            "enable_collision_avoidance": false,
            "preferred_ratio": 0.6666666,
            "background_color": "black",
            "fixed_width": false,
            "paper_width": 1000,
            "force_collide": 0
        };
    }

    createClass(ViewerCanvas, [{
        key: 'mountNode',
        value: function mountNode(root) {
            this.clientWidth = root.clientWidth;
            this.domNode = d3.select(root).append("svg").node();
            this.contentNode = d3.select(root).append("div").node();
            this.initCanvas();
        }
    }, {
        key: 'setState',
        value: function setState(state) {
            this.state = state;
        }
    }, {
        key: 'trigger',
        value: function trigger(event$$1, data) {
            var doc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.state;

            if (this.events && this.events[event$$1]) {
                var func = this.events[event$$1][0];
                for (var funcIndex = 0; funcIndex < this.events[event$$1].length; funcIndex++) {
                    var _func = this.events[event$$1][funcIndex];
                    _func(event$$1, data, doc);
                }
            }
        }
    }, {
        key: 'on',
        value: function on(event$$1, func) {
            if (typeof this.events == "undefined") {
                this.events = [];
            }

            if (typeof this.events[event$$1] == "undefined") {
                this.events[event$$1] = [];
            }
            this.events[event$$1].push(func);
        }
    }, {
        key: 'initCanvas',
        value: function initCanvas() {
            var self = this;
            var el = this.domNode;

            //this.zones = this.state.zones;

            var paperWidth = this.getConfig("paper_width");
            var ratio = this.getConfig("preferred_ratio");
            var fixedWidth = this.getConfig("fixed_width");

            var full = el.getBoundingClientRect();
            ratio = full.height / full.width;

            var ow = fixedWidth ? paperWidth : el.parentNode.clientWidth;
            var oh = ow * ratio;

            console.log("W = " + ow + ", H = " + oh + " : " + el.parentNode.clientWidth + " (original = " + self.clientWidth + ")");

            var vis = d3.select(el).attr("width", ow);

            var rect = [0, 0, paperWidth, paperWidth * ratio];

            var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];

            vis.attr("preserveAspectRatio", "xMinYMin");
            vis.attr("viewBox", zone); // "0 0 1000 800"

            var defs = vis.append("defs");
            defs.selectAll("marker").data(["suit", "licensing", "resolved", "lifecycle"]).enter().append("marker").attr("id", function (d) {
                return d;
            }).attr("class", function (d) {
                return "_end_" + d;
            }).attr("viewBox", "0 -5 10 10").attr("refX", 0).attr("refY", 0).attr("markerWidth", 5).attr("markerHeight", 5).attr("orient", "auto").append("path").attr("d", "M0,-5L10,0L0,5");

            var filter = defs.append("filter").attr("id", "drop-shadow").attr("x", 0).attr("y", 0).attr("height", "150%").attr("width", "150%");
            filter.append("feOffset").attr("in", "SourceAlpha").attr("dx", 3).attr("dy", 3).attr("result", "offOut");
            filter.append("feGaussianBlur").attr("in", "offOut").attr("stdDeviation", 1).attr("result", "blurOut");
            filter.append("feBlend").attr("in", "SourceGraphic").attr("in2", "blurOut").attr("mode", "normal");
            //      const feMerge = filter.append("feMerge");
            //      feMerge.append("feMergeNode").attr("in", "offsetBlur");
            //      feMerge.append("feMergeNode").attr("in", "SourceGraphic");


            //      let all = vis.append("rect")
            //          .style("fill", this.getConfig("background_color"))
            //          .attr("width", "100%")
            //          .attr("height", "100%")
            //          .attr("x", 0)
            //          .attr("y", 0);

            var zoneRoot = vis.append("g").attr("class", "zones");

            vis.append("g").attr("class", "all_lines");

            vis.append("g").attr("class", "nodeSet");

            var calcLinkDistance = function calcLinkDistance(a, b) {
                //          return Math.abs(Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)));

                if (a.source.boundary != a.target.boundary) {
                    return ow / 4;
                } else if (a.type == "step-after") {
                    return a.hint ? a.hint : 150;
                } else if (a.type == "straight" || a.type == "step-before") {
                    //            console.log("POSITIONS: " + JSON.stringify(a.source));
                    //            console.log(" AND " + JSON.stringify(a.target));
                    //            console.log(" ..X " + a.source.x +", "+a.target.x);
                    //            console.log(" ..Y " + a.source.y +", "+a.target.y);
                    //            console.log(" ..Y " + a.source.y +", "+a.target.y);
                    //            console.log(" ..CX " + ((a.source.x - a.target.x) * 2));
                    //            console.log(" ..CY " + ((a.source.y - a.target.y) * 2));
                    //            console.log(" ..Answer " + (Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
                    //            console.log("SQRT : " + Math.sqrt(Math.abs((a.source.x - a.target.x) * 2) + Math.abs((a.source.y - a.target.y) * 2)));
                    return a.hint ? a.hint : 150;
                } else {
                    return a.hint ? a.hint : 80;
                }
            };

            var simulation = d3.forceSimulation().force("link", d3.forceLink([]).distance(calcLinkDistance).strength(0.5)).force("collide", d3.forceCollide(function (d) {
                return self.getConfig("force_collide");
            }).iterations(1)).force("charge", d3.forceManyBody().strength(0));

            var force = simulation;
            //          .links([])
            //          .nodes([])
            //          .gravity(0)
            //          .charge(0)
            //          .linkDistance(calcLinkDistance)
            //          .size([ow, oh]);

            this.vis = vis;
            this.force = force;

            // fa-book, fa-th, fa-share-square-o, fa-camera-retro
            var navData = [{ "id": "1", "title": "Documentation", "icon": fa.book }, { "id": "2", "title": "Configuration", "icon": fa('tasks') }, { "id": "3", "title": "Topology", "icon": fa.th }, { "id": "4", "title": "Audit", "icon": fa('camera-retro') }];

            this.navigation = new Navigation();

            this.navigation.attach(vis, this, this.contentNode);
            //     this.navigation.addMenuItem(navData[0]);
            //      this.navigation.addMenuItem(navData[1]);
            //      this.navigation.addMenuItem(navData[2]);
            //      this.navigation.addMenuItem(navData[3]);

            this.navigation.update();
            //console.log("NAV = "+faIconChars[0].unicode);

            //      all.on("click", function (d) {
            //         self.closePopups();
            //      });
        }
    }, {
        key: 'closePopups',
        value: function closePopups() {
            this.navigation.close();
        }
    }, {
        key: 'refreshZones',
        value: function refreshZones(zones, config) {

            var self = this;
            var vis = this.vis;

            var pageHeight = vis.node().parentNode.clientHeight;
            var pageWidth = vis.node().parentNode.clientWidth;

            var zoneRoot = vis.select("g.zones");

            zoneRoot.selectAll("g").remove();

            vis.attr("pageWidth", pageWidth);
            vis.attr("pageHeight", pageHeight);

            this.zones = zones;

            zones.forEach(function (zone, zoneIndex) {

                var layout = LayoutFactory.getLayout(zone.type);
                layout.build(vis, zone, zoneRoot, self);

                layout.organize(vis, zoneIndex, zones);
            });

            zoneRoot.selectAll("g").on('mouseenter', function (nodeSet, index) {
                d3.select(this).classed("zone-hover", true);
            }).on('mouseleave', function (e) {
                d3.select(this).classed("zone-hover", false);
            });

            zoneRoot.selectAll("g").selectAll("rect").on("click", function (d) {
                self.closePopups();
            });
        }
    }, {
        key: 'refreshSize',
        value: function refreshSize() {
            var self = this;
            var el = this.domNode;
            var vis = this.vis;

            var paperWidth = this.getConfig("paper_width");
            var ratio = this.getConfig("preferred_ratio");
            var fixedWidth = this.getConfig("fixed_width");

            var ow = fixedWidth == false ? el.parentNode.clientWidth : paperWidth;
            var oh = ratio == 0 ? el.parentNode.clientHeight : ow * ratio; //el.parentNode.clientHeight; //ow * ratio;


            console.log("refreshSize() : SVG [W = " + ow + ", H = " + oh + "] :: Parent clientHeight=" + el.parentNode.clientHeight);
            console.log("refreshSize() : Body: W = " + document.body.clientWidth + ", H = " + document.body.clientHeight);

            vis.attr("width", ow).attr("height", oh);

            d3.select(this.domNode.parentNode).style("background-color", this.getConfig("background_color"));

            // scale the viewbox to maximize the zoom
            // Adjust the height to nothing larger than what the zones are using
            vis.select("g.zones").each(function (d) {

                // if ratio is 0, then set the height to the height of all zones
                //
                var bbox = this.getBBox();
                var parentBbox = this.parentNode.getBBox();

                if (ratio != 0) {
                    var rect = [0, 0, paperWidth, paperWidth * ratio];
                    var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];
                    console.log("Setting ViewBox (has ratio) to " + zone);
                    vis.attr("viewBox", zone);
                }
            });

            self.zones.forEach(function (zone, zoneIndex) {

                var layout = LayoutFactory.getLayout(zone.type);
                if (zone.type == "tabs" || zone.type == "default" || zone.type == "carousel") {
                    layout.refreshSize(self, zone, vis.select("g.zones").filter(function (d, i) {
                        return i == zoneIndex;
                    }));
                }
            });

            vis.select("g.zones").each(function () {

                // if ratio is 0, then set the height to the height of all zones
                //
                var bbox = this.getBBox();
                var parentBbox = this.parentNode.getBBox();

                if (ratio == 0) {
                    var rect = [0, 0, paperWidth, bbox.height];
                    console.log("refreshSize() : BBOX: W = " + bbox.width + ", H = " + bbox.height);

                    var zone = "" + rect[0] + " " + rect[1] + " " + rect[2] + " " + rect[3];

                    console.log("Setting ViewBox (ratio=0) to " + zone);
                    vis.attr("viewBox", zone);
                }
            });

            //
            //      let rect = [0, 0, ow, ow * ratio];
            //
            //      const zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];
            //
            //      vis.attr("preserveAspectRatio", "xMinYMin")
            //vis.attr("viewBox", zone); // "0 0 1000 800"
        }
    }, {
        key: 'getDim',
        value: function getDim(d) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (this.zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = this.zones[index].hasOwnProperty('calculatedRectangle') ? this.zones[index].calculatedRectangle : this.zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'clear',
        value: function clear() {
            //console.log("CLEARING");
            this.vis.select("g.nodeSet").selectAll("g.node").remove();
        }
    }, {
        key: 'getConfig',
        value: function getConfig(c) {
            var config = this.state ? this.state.config : null;
            if (config && config.hasOwnProperty(c)) {
                return config[c];
            } else {
                return this.defaultConfig[c];
            }
        }
    }, {
        key: 'traverseChildren',
        value: function traverseChildren(newNodes) {
            var self = this;
            var vis = this.svg;

            newNodes.each(function (d) {
                var _this = this;

                if (d.children) {
                    (function () {

                        var nodeSet = d3.select(_this.parentNode).selectAll("g.node_" + d.name).data(d.children);

                        var newNodesC = nodeSet.enter().append("g").attr("class", "node_" + d.name + " nodec");

                        var names = ShapeFactory.getShapeNames();

                        var _loop = function _loop(nm) {
                            var shap = ShapeFactory.getShape(names[nm]);
                            shap.build(newNodesC.filter(function (d) {
                                return d.type == names[nm];
                            }).classed("_" + names[nm], true));
                        };

                        for (var nm in names) {
                            _loop(nm);
                        }

                        newNodesC.attr("cx", function (d) {
                            var dim = self.getDim(d);var x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;d.x = x;d.fx = d.fixed ? x : null;
                        }).attr("cy", function (d) {
                            var dim = self.getDim(d);var y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;d.y = y;d.fy = d.fixed ? y : null;
                        });

                        self.zones.forEach(function (zone, index) {
                            var layout = LayoutFactory.getLayout(zone.type);
                            var filtered = newNodesC.filter(function (d) {
                                var i = d.boundary.charCodeAt(0) - 65;return index == i;
                            });
                            layout.configEvents(vis, filtered, self.zones, self);
                        });

                        //              // Attach Events
                        //              self.zones.forEach(function (zone, index) {
                        //                let layout = LayoutFactory.getLayout(zone.type);
                        //                //layout.configEvents(vis, newNodesC, self.zones, self);
                        //              });

                        self.traverseChildren(newNodesC);
                    })();
                }
            });
        }
    }, {
        key: 'update',
        value: function update() {
            var self = this;
            var vis = this.vis;

            var force = this.force;

            self.registerLinks();
            self.refreshZones(this.state.zones, {});

            var linkInfo = vis.select("g.all_lines").selectAll("g").data(this.state.links);

            var linkGroup = linkInfo.enter().insert("g").attr("class", function (d) {
                return d.hasOwnProperty("class") ? "_" + d.type + " _" + d.class : "_" + d.type;
            });

            linkGroup.append("path").attr("class", "link");

            linkGroup.filter(function (d) {
                return d.ends;
            }).append("circle").attr("r", 5).attr("fill", "#298EFE").attr("class", "circleSource");

            linkGroup.filter(function (d) {
                return d.ends;
            }).append("circle").attr("r", 5).attr("fill", "#298EFE").attr("class", "circleTarget");

            linkGroup.append("text").attr("text-anchor", "left").text(function (d) {
                return "Link Text";
            }).attr("class", "linkText");

            linkInfo.exit().remove();

            var links = linkGroup.selectAll("path");

            var nodeSet = vis.select("g.nodeSet").selectAll("g.node").data(this.state.nodes);

            var newNodes = nodeSet.enter().append("g").attr("class", "node");

            var names = ShapeFactory.getShapeNames();

            var _loop2 = function _loop2(nm) {
                var shap = ShapeFactory.getShape(names[nm]);
                shap.build(newNodes.filter(function (d) {
                    return d.type == names[nm];
                }).classed("_" + names[nm], true));
            };

            for (var nm in names) {
                _loop2(nm);
            }

            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d);var x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;d.x = x;d.fx = d.fixed ? x : null;
            }).attr("cy", function (d) {
                var dim = self.getDim(d);var y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;d.y = y;d.fy = d.fixed ? y : null;
            });

            // Add children (attach tab navigation)
            // Calculate the grouping metadata for the layout
            // Organize the tab hierarchy by adjusting the x,y,width coordinates

            // Get the navigation to work with the children


            this.traverseChildren(newNodes);

            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                layout.enrichData(self.state.nodes);
            });

            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                var filtered = newNodes.filter(function (d) {
                    var i = d.boundary.charCodeAt(0) - 65;return index == i;
                });
                layout.configEvents(vis, filtered, self.zones, self);
            });

            // Organize:
            this.zones.forEach(function (zone, index) {
                var layout = LayoutFactory.getLayout(zone.type);
                layout.organize(vis, index, self.zones);
            });

            nodeSet.exit().remove();

            nodeSet = vis.select("g.nodeSet").selectAll("g.nodec, g.node");

            var ticked = function ticked() {

                var radius = 20;
                var w = vis.attr("width");
                var h = vis.attr("height");

                // enforce zone boundaries
                if (self.getConfig('enable_zone_boundaries')) {
                    nodeSet.attr("cx", function (d) {
                        var dim = self.getDim(d);d.x = Math.max(dim.x1 + radius, Math.min(dim.x2 - radius, d.x));return d.x;
                    }).attr("cy", function (d) {
                        var dim = self.getDim(d);d.y = Math.max(dim.y1 + radius, Math.min(dim.y2 - radius, d.y));return d.y;
                    });
                }

                if (self.getConfig('enable_collision_avoidance')) {
                    nodeSet.each(self.collide(0.5));
                }

                nodeSet.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                var ln = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveLinear); // step, linear

                var stepBefore = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveStepBefore); // step, step-after, step

                var stepAfter = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveStepAfter); // step, step-after, step

                var cardinal = d3.line().x(function (d) {
                    return d.x;
                }).y(function (d) {
                    return d.y;
                }).curve(d3.curveCardinal); // step, step-after, step


                links.attr("d", function (d) {
                    var sourcePoints = { "x": d.source.x, "y": d.source.y };
                    var targetPoints = { "x": d.target.x, "y": d.target.y };
                    var a = 0;

                    // Source
                    if (d.hasOwnProperty('ends')) {
                        (function () {
                            var deltaX = 0;
                            var deltaY = 0;

                            // Find the source node and based on its bounding box, set the position

                            var s1 = nodeSet.filter(function (inf, ind) {
                                return d.source.index == ind;
                            });
                            var t1 = nodeSet.filter(function (inf, ind) {
                                return d.target.index == ind;
                            });

                            var sourceEnd = d.ends.substring(0, 1);
                            var targetEnd = d.ends.substring(2, 3);

                            if (d.ends == "auto" && (d.type == "straight" || d.type == "step-before")) {
                                if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "S";
                                    targetEnd = "W";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "S";
                                    targetEnd = "E";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                                    sourceEnd = "N";
                                    targetEnd = "E";
                                } else {
                                    sourceEnd = "N";
                                    targetEnd = "W";
                                }
                            }

                            if (d.ends == "auto" && d.type == "step-after") {
                                if (sourcePoints.x < targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "E";
                                    targetEnd = "N";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y < targetPoints.y) {
                                    sourceEnd = "W";
                                    targetEnd = "N";
                                } else if (sourcePoints.x >= targetPoints.x && sourcePoints.y >= targetPoints.y) {
                                    sourceEnd = "W";
                                    targetEnd = "S";
                                } else {
                                    sourceEnd = "E";
                                    targetEnd = "S";
                                }
                            }

                            s1.each(function () {
                                if (sourceEnd == "W") {
                                    deltaX = -this.getBBox().width / 2;
                                }
                                if (sourceEnd == "E") {
                                    deltaX = this.getBBox().width / 2;
                                }
                                if (sourceEnd == "N") {
                                    deltaY = -this.getBBox().height / 2;
                                }
                                if (sourceEnd == "S") {
                                    deltaY = this.getBBox().height / 2;
                                }
                            });

                            sourcePoints = { "x": d.source.x + deltaX, "y": d.source.y + deltaY };

                            deltaX = 0;
                            deltaY = 0;
                            t1.each(function () {
                                if (targetEnd == "W") {
                                    deltaX = -this.getBBox().width / 2;
                                }
                                if (targetEnd == "E") {
                                    deltaX = this.getBBox().width / 2;
                                }
                                if (targetEnd == "N") {
                                    deltaY = -this.getBBox().height / 2;
                                }
                                if (targetEnd == "S") {
                                    deltaY = this.getBBox().height / 2;
                                }
                            });

                            targetPoints = { "x": d.target.x + deltaX, "y": d.target.y + deltaY };
                        })();
                    }

                    var lineData = [sourcePoints, targetPoints];
                    if (d.type == "straight" || d.type == "step-before") {
                        return stepBefore(lineData);
                    } else if (d.type == "step-after") {
                        return stepAfter(lineData);
                    } else if (d.type == "cardinal") {
                        return cardinal(lineData);
                    } else {
                        return ln(lineData);
                    }
                });

                // display text
                links.each(function (d) {
                    //console.log("Total length = " + this.getTotalLength());
                    var labelPosition = 50;
                    if (d.hasOwnProperty("labelPosition")) {
                        labelPosition = d.labelPosition;
                    }
                    var point = this.getPointAtLength(this.getTotalLength() * (labelPosition / 100));
                    var label = d3.select(this.parentNode).select("text");
                    label.text(function (ld) {
                        return d.label;
                    });
                    label.attr("transform", "translate(" + (point.x - 5) + "," + (point.y - 5) + ")");
                    var circleSource = d3.select(this.parentNode).select("circle.circleSource");
                    var sourcePoint = this.getPointAtLength(0);
                    circleSource.attr("transform", "translate(" + sourcePoint.x + "," + sourcePoint.y + ")");
                    var circleTarget = d3.select(this.parentNode).select("circle.circleTarget");
                    var targetPoint = this.getPointAtLength(this.getTotalLength() - 0);
                    circleTarget.attr("transform", "translate(" + targetPoint.x + "," + targetPoint.y + ")");
                    //layout (this);
                });
            };

            // Restart the force layout
            var nodeList = [];
            for (var a in this.state.nodes) {
                nodeList.push(this.state.nodes[a]);
            }

            for (var i = 0; i < this.state.nodes.length; i++) {
                if (this.state.nodes[i].children) {
                    for (var j = 0; j < this.state.nodes[i].children.length; j++) {
                        nodeList.push(this.state.nodes[i].children[j]);
                    }
                }
            }

            force.nodes(nodeList).on("tick", ticked);

            force.force("link").links(this.state.links);
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            this.force.on("tick", null);
            d3.select(this.domNode).remove();
            d3.select(this.contentNode).remove();
        }

        // Resolves collisions between d and all other nodes.

    }, {
        key: 'collide',
        value: function collide(alpha) {

            var padding = 1.5,
                maxRadius = 12;
            var quadtree$$1 = d3.quadtree(this.state.nodes);

            return function (d) {
                d.radius = 25;
                var r = d.radius + maxRadius + padding,
                    nx1 = d.x - r,
                    nx2 = d.x + r,
                    ny1 = d.y - r,
                    ny2 = d.y + r;

                if (d.x == null) {
                    //console.log("Ignoring.. " + d.name);
                    return;
                }
                quadtree$$1.visit(function (quad, x1, y1, x2, y2) {
                    //console.log("Match: " + x1+","+y1+","+x2+","+y2 + " : " + quad.point);
                    //if (quad.leaf == false) {
                    //    return true;
                    //}
                    // quad.point has the object details
                    if (quad.point && quad.point !== d) {

                        if (quad.point.boundary != d.boundary) {}
                        //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;

                        //if (d.x == null) {
                        //  return true;
                        //}
                        //console.log("D = "+JSON.stringify(d, null, 2));
                        //console.log("Quad = "+JSON.stringify(quad, null, 2));
                        quad.point.radius = 25;
                        var x = d.x - quad.point.x,
                            y = d.y - quad.point.y,
                            l = Math.abs(Math.sqrt(x * x + y * y)),
                            _r = d.radius + quad.point.radius + padding;
                        //console.log("X="+x+",Y="+y);
                        //console.log("L="+l+",R="+r);
                        if (l < _r) {
                            l = (l - _r) / l * alpha;

                            // Need to make sure we are not moving it outside of boundary
                            x = x * l;
                            y = y * l;
                            if (isNaN(x) || isNaN(y)) {
                                //console.log("ILLEGAL VALUE!");
                                x = 0.00;
                                y = 0.01;
                                //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                            }
                            //console.log("Changing..." + x+", "+y+", dx ="+d.x+", "+d.y);
                            d.x = d.x - x;
                            d.y = d.y - y;
                            //console.log("ANSWER: d="+d.x+", "+d.y);
                            quad.point.x += x;
                            quad.point.y += y;
                            //console.log("Change to: " +d.x+", "+d.y+" :: " + quad.point.x+", "+quad.point.y);
                        }
                    }
                    //console.log("Answer: " +x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1);
                    return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                });
            };
        }
    }, {
        key: 'addNode',
        value: function addNode(name, fixed, type, boundary) {
            var nd = { "name": name, "fixed": fixed ? true : false, "type": type, "boundary": boundary };
            this.state.nodes.push(nd);
            this.update();
        }
    }, {
        key: 'removeNode',
        value: function removeNode(name) {

            this.state.nodes = this.state.nodes.filter(function (node) {
                return node["name"] != name;
            });
            this.state.links = this.state.links.filter(function (link) {
                return link["source"]["name"] != name && link["target"]["name"] != name;
            });
            this.update();
        }
    }, {
        key: 'findNode',
        value: function findNode(name) {
            for (var i in this.state.nodes) {
                if (this.state.nodes[i]["name"] === name) return this.state.nodes[i];
            }
        }
    }, {
        key: 'registerLinks',
        value: function registerLinks() {
            var links = this.state.links;
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                if (typeof link.source == "string") {
                    link.source = this.findNode(link.source);
                }
                if (typeof link.target == "string") {
                    link.target = this.findNode(link.target);
                }
            }
        }
    }, {
        key: 'render',
        value: function render() {
            if (this.state) {
                this.update();
            }
            //var node = document.createElement("svg");
            //this.domNode.appendChild(node);
        }
    }]);
    return ViewerCanvas;
}();

var version = "0.0.12";

__$styleInject("._comment{stroke-width:0px}._comment text{font-size:1em;font-family:Open Sans;fill:#fff}", undefined);

var TextUtils = function () {
    function TextUtils() {
        classCallCheck(this, TextUtils);
    }

    createClass(TextUtils, null, [{
        key: "splitByNL",
        value: function splitByNL(name) {
            // look for a \n
            var ind = name.indexOf("\n");
            if (ind != -1) {
                return [name.substr(0, ind), name.substr(ind)];
            } else {
                return [name];
            }
        }
    }, {
        key: "splitByWidth",
        value: function splitByWidth(caption, maxWidth, width) {
            if (maxWidth < width) {
                return [caption];
            }
            var ratio = Math.round(caption.length * (width / maxWidth));
            console.log("Max = " + maxWidth + ", " + width + " RATIO: " + ratio + " : " + caption.length);
            return TextUtils.splitByWords(caption, ratio);
        }
    }, {
        key: "splitByWords",
        value: function splitByWords(caption, maxCharsPerLine) {

            var words = caption.split(' ');
            var line$$1 = "";

            var lines = [];
            for (var n = 0; n < words.length; n++) {
                var testLine = line$$1 + words[n] + " ";
                if (testLine.length > maxCharsPerLine && line$$1.length > 0) {
                    lines.push(line$$1);

                    line$$1 = words[n] + " ";
                } else {
                    line$$1 = testLine;
                }
            }
            lines.push(line$$1);

            return lines;
        }
    }]);
    return TextUtils;
}();

var Shape = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var tmpTxtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 0).attr("dy", 0).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            var bbox = [];
            tmpTxtNode.each(function (d, i) {
                bbox[i] = this.getBBox();
            });

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 0).attr("dy", 0).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).each(function (d, ti) {
                d3.select(this).selectAll('tspan').data(TextUtils.splitByWidth(d.label, bbox[ti].width, d.width ? d.width : 999999)).enter().append("tspan").attr("x", 0).attr("y", function (d, i) {
                    return i * bbox[ti].height;
                }).text(function (line$$1) {
                    return line$$1;
                });
            });

            tmpTxtNode.each(function (d) {
                this.parentNode.removeChild(this);
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", -10).attr("dy", 0).text(function (d) {
                return fa(d.icon);
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._application{fill:#f6d5b9}._application rect.active{fill:#43bb3f}._application rect.inactive{fill:#f19996}._application rect.global{fill:#ccc}._application rect{stroke-width:0px;stroke:#999}._application text{fill:#000;font-family:Open Sans}._application text.label{font-size:.6em}._application text.bottomLeft,._application text.bottomRight,._application text.topLeft,._application text.topRight{font-size:.4em}._application text.appId{font-size:.45em;font-weight:700}", undefined);

var RectangleShape = function () {
    function RectangleShape(domainType) {
        classCallCheck(this, RectangleShape);

        this.domainType = domainType;
    }

    createClass(RectangleShape, [{
        key: 'build',
        value: function build(chgSet) {
            var self = this;

            var x = 120;
            var y = 60;

            chgSet.append("rect").attr("x", -x / 2).attr("y", -y / 2).attr("width", x).attr("height", y).attr("class", function (d) {
                return d.state;
            });
            //.style("transform", "translate(-50%, -50%)")
            //.style("filter", "url(#drop-shadow)")

            chgSet.append("text").attr("class", "appId").attr("text-anchor", "middle").attr("dx", 0).attr("dy", 16).text(function (d) {
                return d.appId;
            });

            var appNameNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            appNameNode.append("tspan").attr("x", 0).attr("y", -4).text(function (d) {
                var lines = TextUtils.splitByWords(d.label, 20);return lines[0];
            });

            appNameNode.append("tspan").attr("x", 0).attr("y", 6).text(function (d) {
                var lines = TextUtils.splitByWords(d.label, 20);return lines.length == 1 ? "" : lines[1];
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("topLeft");
            }).append("text").attr("x", -x / 2 + 2).attr("y", -y / 2 + 8).attr("class", "topLeft").attr("text-anchor", "start").text(function (d) {
                return d.labels.topLeft;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("topRight");
            }).append("text").attr("x", x / 2 - 2).attr("y", -y / 2 + 8).attr("class", "topRight").attr("text-anchor", "end").text(function (d) {
                return d.labels.topRight;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("bottomLeft");
            }).append("text").attr("x", -x / 2 + 2).attr("y", y / 2 - 4).attr("class", "bottomLeft").attr("text-anchor", "start").text(function (d) {
                return d.labels.bottomLeft;
            });

            chgSet.filter(function (d) {
                return d.labels.hasOwnProperty("bottomRight");
            }).append("text").attr("x", x / 2 - 2).attr("y", y / 2 - 4).attr("class", "bottomRight").attr("text-anchor", "end").text(function (d) {
                return d.labels.bottomRight;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty("image");
            }).append("image").attr("x", x / 2 - 20 - 2).attr("y", y / 2 - 10 - 2).attr("width", function (d) {
                return 20;
            }).attr("height", function (d) {
                return 10;
            }).attr("xlink:href", function (d) {
                return d.image;
            });

            chgSet.filter(function (d) {
                return typeof d.alerts != "undefined";
            }).each(function (d) {

                var alertNodes = d3.select(this).selectAll("text.alerts").data(d.alerts);

                alertNodes.enter().append("text").attr("class", function (t) {
                    return t.class ? "icon " + t.class : "icon";
                }).attr("text-anchor", "end").attr("dx", function (a, i) {
                    return -x / 2 + 15 + i * 20;
                }).attr("dy", -y / 2 - 2).text(function (t) {
                    return fa(t.icon);
                }).on("click", function (e, i) {
                    d3.event.stopPropagation();

                    alert("clicked " + i + " : " + JSON.stringify(e) + " OBJ: " + d.name);
                });
            });
        }
    }]);
    return RectangleShape;
}();

__$styleInject("._icon{fill:#ccc;stroke:#333;stroke-width:1.5px}._icon text,._icon text.label{fill:#fff;stroke-width:0;font-size:.5em}", undefined);

var Shape$1 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            chgSet.append("text").attr("class", function (d) {
                return d.class ? "icon " + d.class : "icon";
            }).attr("text-anchor", "middle").attr("dx", 0).attr("dy", 5).text(function (d) {
                return fa(d.icon);
            });

            chgSet.append("text").filter(function (d) {
                return d.label;
            }).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).attr("text-anchor", "middle").attr("dx", 0).attr("dy", "2.0em").text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("", undefined);

var Shape$2 = function () {
  function Shape(domainType) {
    classCallCheck(this, Shape);

    this.domainType = domainType;
  }

  createClass(Shape, [{
    key: 'build',
    value: function build(chgSet) {
      chgSet.append("image").attr("x", function (d) {
        return -d.width / 2;
      }).attr("y", function (d) {
        return -d.height / 2;
      }).attr("width", function (d) {
        return d.width;
      }).attr("height", function (d) {
        return d.height;
      }).attr("class", function (d) {
        return d.class ? d.class : "image";
      }).attr("xlink:href", function (d) {
        return d.image;
      });
    }
  }]);
  return Shape;
}();

__$styleInject("._circle{fill:#ccc;stroke:#999;stroke-width:.2em}._circle circle{r:20}._circle text{fill:#fff;stroke-width:0;font-size:.6em;font-family:Open Sans}._circle2{fill:#6cf;stroke:#09c;stroke-width:.1em}._circle2 circle{r:10}._circle2 text{fill:#6cf;stroke-width:0;font-size:.4em;font-family:Open Sans}._circle3 circle{fill:#fff;stroke:#000;stroke-width:.1em;r:10}._circle3 text{fill:#000;font-size:.7em;font-weight:700;font-family:Arial}", undefined);

var Shape$3 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            chgSet.append("circle");

            chgSet.filter(function (d) {
                return !d.hasOwnProperty("position") || d.position == "right";
            }).append("text").attr("text-anchor", "left").attr("dx", function (d) {
                var circle = d3.select(this.parentNode).select('circle').node();return 2 + circle.getBBox().width / 2;
            }).attr("dy", 5).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.position == "center";
            }).append("text").attr("text-anchor", "middle").attr("dominant-baseline", "central").attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._milestone circle{fill:#fff}._milestone text{fill:#fff;font-size:.7em;font-family:Open Sans}", undefined);

var Shape$4 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 12).attr("dy", 4).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.append("circle").attr("r", 6).style("stroke", function (d) {
                return d.hasOwnProperty("color") ? d.color : "grey";
            }).style("stroke-width", ".3em");
        }
    }]);
    return Shape;
}();

__$styleInject("._button rect{fill:#000;stroke-width:.08em;stroke:red}._button text.label{fill:#fff;font-family:Open Sans,sans-serif;font-size:.9em}", undefined);

var Shape$5 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: "build",
        value: function build(chgSet) {
            var self = this;

            chgSet.classed("clickable", true);

            // Width calculated by the layout
            chgSet.append("rect").attr("x", -150 / 2).attr("y", -40 / 2).attr("height", 40).attr("width", 150);

            var textNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? 5 : -3;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines[0];
            });

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var bbox = this.parentNode.getBBox();return bbox.height - 3;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? "" : lines[1];
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._timeline{fill:#000;stroke:#333;stroke-width:0px}._timeline rect{stroke-width:0}._timeline circle{fill:#fff;stroke:#000}._timeline text{font-size:.7em;font-family:Open Sans}", undefined);

var Shape$6 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 12).attr("dy", -5).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").style("fill", function (d) {
                return d.color;
            }).attr("dx", 5).attr("dy", -5).text(function (d) {
                return fa(d.icon);
            });

            chgSet.append("circle").attr("r", 6).style("stroke", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).style("stroke-width", ".3em").attr("transform", function (d) {
                var width = d.hasOwnProperty("width") ? d.width : 200;return "translate(" + (width + 6) + ", 3)";
            });

            chgSet.append("rect").attr("class", "line").style("fill", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).attr("width", function (d) {
                return d.hasOwnProperty("width") ? d.width : 200;
            }).attr("height", 6);
        }
    }]);
    return Shape;
}();

__$styleInject("._timeline2 rect{stroke:#333;stroke-width:0}._timeline2 text{font-size:.7em;font-family:Open Sans;fill:#fff}._timeline2 text.icon{fill:#fff}", undefined);

var Shape$7 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var shape = this.domainType;

            chgSet.append("rect").attr("class", "line").attr("rx", function (d) {
                return shape == "timeline2" ? 8 : 0;
            }).attr("ry", function (d) {
                return shape == "timeline2" ? 8 : 0;
            }).style("fill", function (d) {
                return d.hasOwnProperty("color") ? d.color : "blue";
            }).attr("width", function (d) {
                return d.hasOwnProperty("width") ? d.width : 200;
            }).attr("height", 20);

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", 10).attr("dy", 14).text(function (d) {
                return fa(d.icon);
            });

            var txtNode = chgSet.append("text").attr("text-anchor", "left").attr("dx", 18).attr("dy", 14).attr("class", function (d) {
                return d.class ? d.class : "label";
            }).text(function (d) {
                return d.label;
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._panel{fill:#fff}", undefined);

var Shape$8 = function () {
  function Shape(domainType) {
    classCallCheck(this, Shape);

    this.domainType = domainType;
  }

  createClass(Shape, [{
    key: "build",
    value: function build(newNodes) {
      newNodes.each(function (d) {
        if (!d.hasOwnProperty("name")) {
          alert("Panel requires a name! " + JSON.stringify(d));
        }
      });
    }
  }]);
  return Shape;
}();

__$styleInject("._box{fill:blue}._box rect{stroke-width:0px;stroke:#999}._box text{fill:#fff;stroke:#fff;stroke-width:0;font-family:Open Sans Condensed,sans-serif}._box text.label{font-size:.85em}._box circle.option_x{fill:#fff}._box text.option_x{fill:#000;font-family:Open Sans Condensed,sans-serif}._box .icon{font-family:FontAwesome;fill:#fff;stroke-width:0;font-size:1em}", undefined);

var Shape$9 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: 'build',
        value: function build(chgSet) {
            var x = 100;
            var y = 50;

            chgSet.append("rect").attr("x", -x / 2).attr("y", -y / 2).attr("width", x).attr("height", y);

            chgSet.append("text").attr("class", function (d) {
                return d.class ? d.class : "label";
            }).attr("text-anchor", "middle").attr("dominant-baseline", "central").attr("dx", 0).attr("dy", 0).text(function (d) {
                return d.label;
            });

            chgSet.filter(function (d) {
                return d.hasOwnProperty('icon');
            }).append("text").attr("class", "icon").attr("text-anchor", "middle").attr("dx", x / 2 - 12).attr("dy", -(y / 2) + 15).text(function (d) {
                return fa(d.icon);
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._tab rect{fill:#000;stroke-width:.08em;stroke:red}._tab text.label{fill:#fff;font-family:Open Sans,sans-serif;font-size:1.4em}", undefined);

var Shape$10 = function () {
    function Shape(domainType) {
        classCallCheck(this, Shape);

        this.domainType = domainType;
    }

    createClass(Shape, [{
        key: "build",
        value: function build(chgSet) {
            var self = this;

            chgSet.classed("tab", true);

            // Width calculated by the layout
            chgSet.append("rect").attr("x", -200 / 2).attr("y", -50 / 2).attr("height", 50).attr("width", 200);

            var textNode = chgSet.append("text").attr("class", "label").attr("text-anchor", "middle");

            textNode.append("tspan").attr("x", 0).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines[0];
            });

            textNode.append("tspan").attr("x", 0).attr("y", function (d) {
                var bbox = this.parentNode.getBBox();return bbox.height;
            }).text(function (d) {
                var lines = TextUtils.splitByNL(d.label);return lines.length == 1 ? "" : lines[1];
            });
        }
    }]);
    return Shape;
}();

__$styleInject("._swimlane{margin:5px;fill:#fff;stroke:#333;stroke-width:.2;x-stroke-dasharray:5.5}._swimlane rect{opacity:.6}._swimlane .laneLabel{fill:#eca50d}._swimlane text{font-family:Open Sans;font-size:.8em;fill:#000;font-weight:700}", undefined);

__$styleInject("._default{fill:#000;stroke:#333;stroke-width:.2;opacity:.7}text.label-xl{font-size:140%}text.label-l{font-size:120%}text.label-s{font-size:80%}text.label-xs{font-size:60%}div.content td.key{font-weight:700;text-align:right;width:50%;padding:5px}div.content td.key,div.content td.value{font-size:.8em;text-transform:uppercase;border-bottom:1px solid #ccc}div.content{background-color:#fff;border:1px solid #999;font-family:Arial;font-size:.85em;border-radius:5px;padding-bottom:5px}div.content div{padding:5px}.icon{font-family:FontAwesome!important}g._straight .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._straight text.linkText{fill:#fff;font-size:.7em}g._step-before .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._step-before text.linkText{fill:#fff;font-size:.7em}g._step-after .link{fill:none;stroke:#7bb9fe;stroke-width:3.5px}g._step-after text.linkText{fill:#fff;font-size:.7em}g._link2 path.link{fill:none;stroke:#90ee90;stroke-width:2px;stroke-dasharray:(6,3)}g._link2 circle.circleSource,g._link2 circle.circleTarget{fill:#90ee90}", undefined);

var Layout$1 = function () {
    function Layout(domainType) {
        classCallCheck(this, Layout);

        this.domainType = domainType;
    }

    createClass(Layout, [{
        key: "enrichData",
        value: function enrichData() {}
    }, {
        key: "organize",
        value: function organize(d3visual, zone, root) {}
    }, {
        key: "configEvents",
        value: function configEvents(d3visual, newNodes, zones, viewer) {
            var self = this;

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                //const set = vis.select("g.nodeSet").selectAll("g.node");
                if (viewer.downX == Math.round(e.x + e.y)) {
                    //let matchedItem = set.filter(function (d) { return (e.index == d.index); });
                    viewer.navigation.toggle(e, d3.select(this));
                } else {
                    viewer.navigation.close(e);
                }
            });

            function dragstarted(d) {
                d3.select(this).classed("fixed", d.fixed == true);
                if (!d3.event.active) force.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
                viewer.downX = Math.round(d.x + d.y);
            }

            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function dragended(d) {
                if (!d3.event.active) force.alphaTarget(0);

                if (!d.fixed) {
                    d.fx = null;
                    d.fy = null;
                }
                setTimeout(function () {
                    viewer.trigger('change');
                }, 200);
            }

            newNodes.call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

            var clickable = newNodes.filter(function (d) {
                return d3.select(this).classed('clickable');
            });
            clickable.on('click', function (e) {
                d3.event.stopPropagation();viewer.trigger("shapeClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */
            });
        }
    }, {
        key: "getDim",
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: "build",
        value: function build(d3visual, zone, root, viewer) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            obj.append('rect');

            this.refreshSize(viewer, zone, obj);
        }
    }, {
        key: "refreshSize",
        value: function refreshSize(viewer, zone, zoneNode) {
            var d3visual = viewer.vis;

            this.calculateRectangle(d3visual, zone);

            var rect = zoneNode.select("rect").style("filter", "url(#drop-shadow)").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]);
        }
    }, {
        key: "calculateRectangle",
        value: function calculateRectangle(d3visual, zone) {
            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            console.log("_default: Resizing zone : W=" + viewWidth + ",H=" + viewHeight);

            var pageWidth = d3visual.attr("width");
            var pageHeight = d3visual.attr("height");
            // THis needs to be a calculation based on a ratio

            var pageHeightSet = typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1;

            var rectDimension = [];

            for (var v = 0; v < 4; v++) {
                if (typeof zone.rectangle[v] == "string") {
                    //console.log("Evaluating: " + zone.rectangle[v]);
                    //console.log(" ... to : " + eval(zone.rectangle[v]));
                    rectDimension.push(eval(zone.rectangle[v]));
                } else {
                    rectDimension.push(zone.rectangle[v]);
                }
            }
            if (pageHeightSet) {
                rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
            }

            zone.calculatedRectangle = rectDimension;
            console.log("_default: Resized zone : W=" + rectDimension[2] + ",H=" + rectDimension[3]);
        }
    }]);
    return Layout;
}();

var Layout = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            //      let vb = d3visual.attr("viewBox").split(' ');
            //
            //      let viewWidth = vb[2];
            //      let viewHeight = vb[3];
            //
            //      console.log("_swimlane: Resizing zone : W="+viewWidth+",H="+viewHeight);
            //
            //      let pageWidth = d3visual.attr("width");
            //      let pageHeight = d3visual.attr("height");
            //      // THis needs to be a calculation based on a ratio
            //
            //      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
            //
            //      let rectDimension = [];
            //
            //      for ( let v = 0; v < 4; v++) {
            //          if (typeof zone.rectangle[v] == "string") {
            //            //console.log("Evaluating: " + zone.rectangle[v]);
            //            //console.log(" ... to : " + eval(zone.rectangle[v]));
            //            rectDimension.push(eval(zone.rectangle[v]));
            //          } else {
            //            rectDimension.push(zone.rectangle[v]);
            //          }
            //      }
            //      zone.calculatedRectangle = rectDimension;
            //
            //      if (pageHeightSet) {
            //          rectDimension[3] = pageHeight - 20; //((zone.rectangle[0]+zone.rectangle[2]) * pageHeight/pageWidth) - 20;
            //      }

            this.calculateRectangle(d3visual, zone);

            var rect = obj.append("rect")
            //.style("filter", "url(#drop-shadow)")
            .attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]).attr("class", "lane"); //fff899

            obj.append("rect").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("width", function (d) {
                return zone.orientation == "horizontal" ? 80 : 40;
            }).attr("height", zone.calculatedRectangle[3]).attr("class", "laneLabel"); //fff899

            var transform = "translate(" + zone.calculatedRectangle[0] + "," + zone.calculatedRectangle[1] + ") rotate(-90) translate(" + -(zone.calculatedRectangle[3] / 2) + "," + "15" + ")";
            if (zone.orientation == "horizontal") {
                transform = "translate(" + (zone.calculatedRectangle[0] + 80 / 2) + "," + (zone.calculatedRectangle[1] + zone.calculatedRectangle[3] / 2) + ")";
            }

            var textNodes = obj.append("text").attr("transform", transform).attr("text-anchor", "middle")
            //.attr("dominant-baseline", "central")
            .attr("dx", 0).attr("dy", 0);

            textNodes.append("tspan").attr("x", 0).attr("y", 0).text(function (d) {
                var lines = self.splitCaption(zone.title, 20);return lines[0];
            });

            textNodes.append("tspan").attr("x", 0).attr("y", 15).text(function (d) {
                var lines = self.splitCaption(zone.title, 20);return lines.length == 1 ? "" : lines[1];
            });

            if (zone.hasOwnProperty("image")) {
                transform = "translate(" + (zone.calculatedRectangle[0] + 10) + "," + (zone.calculatedRectangle[1] + zone.calculatedRectangle[3] / 2 - 60) + ")";

                obj.append("image").attr("width", function (d) {
                    return 60;
                }).attr("height", function (d) {
                    return 40;
                }).attr("xlink:href", function (d) {
                    return zone.image;
                }).attr("transform", transform);
            }
            /*
                  let box = null;
            
                  rect.each (function (d) {
                    box = this.getBBox();
                  });
            
                      */
        }
    }, {
        key: 'splitCaption',
        value: function splitCaption(caption) {
            var MAXIMUM_CHARS_PER_LINE = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 6;


            var words = caption.split(' ');
            var line$$1 = "";

            var lines = [];
            for (var n = 0; n < words.length; n++) {
                var testLine = line$$1 + words[n] + " ";
                if (testLine.length > MAXIMUM_CHARS_PER_LINE && line$$1.length > 0) {
                    lines.push(line$$1);

                    line$$1 = words[n] + " ";
                } else {
                    line$$1 = testLine;
                }
            }
            lines.push(line$$1);

            return lines;
        }
    }]);
    return Layout;
}(Layout$1);

__$styleInject("._tab text.option_x{fill:#fff;font-size:1em}.selected_tab rect{fill:red}.page{position:absolute;background-color:pink;padding:0;margin:0}.page svg{fill:#fff}", undefined);

var Layout$2 = function () {
    function Layout(domainType) {
        classCallCheck(this, Layout);

        this.domainType = domainType;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root, viewer) {
            var self = this;
            viewer.on("tabClick", function (event$$1, e) {
                var tabsToOpen = [];
                var tabToOpen = e.name;
                while (tabToOpen != null) {
                    var tabNode = d3visual.selectAll("g.tab").filter(function (d) {
                        return d.name == tabToOpen;
                    });
                    if (tabNode.classed("selected_tab") == false || e.name == tabNode.datum().name) {
                        tabsToOpen.push(tabNode);
                    }
                    tabToOpen = tabNode.datum().layout.parent;
                }
                var to = tabsToOpen.reverse();
                for (var ind in to) {
                    var clickedObject = to[ind];
                    self.toggleTab(viewer, clickedObject.datum(), clickedObject.node());
                }
            });
        }
    }, {
        key: 'enrichData',
        value: function enrichData(nodes) {
            //
            //      let segs = [];
            //      for ( let x = 0 ; x < graph.nodes.length; x++) {
            //          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //          if (seg != "" && segs.indexOf(seg) < 0) {
            //              segs.push(seg);
            //          }
            //      }
            var badNodes = [];
            var fullNodeList = [];

            this.recurse(null, nodes, "", 0, badNodes, fullNodeList);

            //      // calculate the segment layout
            //      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
            //      for ( let p = 0; p < segs.length; p++) {
            //          let tot = 0;
            //          let x,num;
            //          for ( x = 0 ; x < graph.nodes.length; x++) {
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //              if (seg == segs[p]) {
            //                  tot++;
            //              }
            //              if (graph.nodes[x].children) {
            //                  let ls = graph.nodes[x].children;
            //                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            //                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            //                      if (seg == segs[p]) {
            //                        tot++;
            //                      }
            //
            //                      if (ls[xL1].children) {
            //                          let lsL2 = ls[xL1].children;
            //                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            //                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            //                              if (seg == segs[p]) {
            //                                tot++;
            //                              }
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
            ////              this.recurse(graph.nodes, 1);
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            ////              if (seg == segs[p]) {
            ////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
            ////                  num++;
            ////              }
            ////              this.recurse(graph.nodes[x], 1);
            ////              if (graph.nodes[x].children) {
            ////                  let ls = graph.nodes[x].children;
            ////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            ////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            ////                      if (seg == segs[p]) {
            ////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
            ////                          numL1++;
            ////                      }
            ////                      this.recurse(ls[xL1], 2);
            //
            ////                      if (ls[xL1].children) {
            ////                          let lsL2 = ls[xL1].children;
            ////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            ////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            ////                              if (seg == segs[p]) {
            ////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
            ////                                  numL2++;
            ////                              }
            ////                          }
            ////                      }
            //
            //           //       }
            //            //  }
            //          }
            //      }
        }
    }, {
        key: 'recurse',
        value: function recurse(parent, nodes, rootTag, level, badNodes, fullNodeList) {
            var tot = 0;
            for (var num = 0, x = 0; x < nodes.length; x++) {
                var nd = nodes[x];
                if (nd.type == "tab") {
                    tot++;
                }
            }
            for (var _num = 0, _x = 0; _x < nodes.length; _x++) {
                var _nd = nodes[_x];
                if (_nd.type == "tab") {
                    _nd.layout = { "seq": _num, "total": tot, "seg": rootTag, "segnum": level, "level": level, "parent": parent == null ? null : parent.name };
                    _num++;

                    if (_nd.hasOwnProperty("name") == false) {
                        alert("Warning: Tab nodes require name " + JSON.stringify(_nd));
                        badNodes.push(_nd);
                    } else if (_nd.name in fullNodeList) {
                        alert("Warning: Tab node names must be unique " + JSON.stringify(_nd));
                        badNodes.push(_nd);
                    } else {
                        fullNodeList[_nd.name] = _nd;
                    }

                    if (_nd.children) {
                        this.recurse(_nd, _nd.children, rootTag + "_L" + level + "T" + (_num - 1), level + 1, badNodes, fullNodeList);
                    }
                }
            }
        }
    }, {
        key: 'configEvents',
        value: function configEvents(d3visual, newNodesTotal, zones, viewer) {
            var self = this;

            var newNodes = newNodesTotal.filter(function (d) {
                return d.type == 'tab';
            });

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodes.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                viewer.trigger("tabClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */
            });

            // NO DRAG/DROP support for tabs
            function dragstarted(d) {
                viewer.downX = Math.round(d.x + d.y);
            }
            newNodes.call(d3.drag().on("start", dragstarted));

            self.attachInfoController(viewer, newNodes);
        }
    }, {
        key: 'attachInfoController',
        value: function attachInfoController(viewer, chgSet) {
            var self = this;

            chgSet.filter(function (d) {
                return d.hasOwnProperty("notes");
            }).append("text").attr("text-anchor", "middle").text(function (d) {
                return fa('info-circle');
            }).classed("decoration", true).classed("option_x", true).classed("icon", true).on("click", function (e) {
                self.toggleNavigation(viewer, e, this.parentNode);
            });
        }
    }, {
        key: 'getDim',
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {

            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];

            var maxWidth = viewWidth - 0;

            d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
                var index = d.boundary.charCodeAt(0) - 65;
                return index == zoneIndex;
            }).each(function (d) {
                d.x = (maxWidth / d.layout.total - 5) / 2 + d.layout.seq * (maxWidth / d.layout.total);
                d.y = 50 / 2 + d.layout.segnum * 55;
                d3.select(this).select("rect").attr("x", function (d) {
                    return -(maxWidth / d.layout.total - 5) / 2;
                }).attr("y", function (d) {
                    return -d3.select(this).attr("height") / 2;
                }).attr("width", function (d) {
                    return maxWidth / d.layout.total;
                });

                d3.select(this).select("text.option_x").attr("transform", function (a) {
                    return "translate(" + (maxWidth / d.layout.total / 2 - 15) + ", -10)";
                });

                d3.select(this).select("text").attr("transform", function (a) {
                    var bbox = this.getBBox();
                    var pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
                    var scale = 1;
                    if (pbbox.height / bbox.height < pbbox.width / (bbox.width + 20) && pbbox.height / bbox.height < 1) {
                        scale = pbbox.height / bbox.height == 0 ? 1 : pbbox.height / bbox.height;
                    } else {
                        scale = Math.min(pbbox.width / (bbox.width + 20), 1) == 0 ? 1 : Math.min(pbbox.width / (bbox.width + 20), 1);
                    }
                    return "scale(" + scale + ")";
                });
            });

            d3visual.select("g.nodeSet").selectAll("g.tab").filter(function (d) {
                return d.layout.level != 0;
            }).style("opacity", 0).style("display", "none");

            /*
                   //maxWidth = 800;
                   d3visual.select("g.nodeSet").selectAll("g.nodec").filter(function (d) {
                       const index = d.boundary.charCodeAt(0) - 65;
                       return (index == zoneIndex);
                   })
                   .attr("transform", function(d) {
                        let offsetX = d.layout.seq;
                        return "translate(" +(((maxWidth/d.layout.total)/2)+(offsetX * (maxWidth/d.layout.total))) + ", " + (50+(d.layout.level * 52)) + ")"; } )
                   .each(function (d) {
                       d.x = ((maxWidth/d.layout.total)/2) + (d.layout.seq * (maxWidth/d.layout.total));
                       d.y = 50 + (d.layout.segnum * 55);
            
            //           if (!d.layout) {
            //                d.layout = {"segnum":2, "total":3};
            //           }
                       //d.x = 105 + (self.aa[d.layout.segnum] * (maxWidth/d.layout.total));
                       //d.y = 70 + (d.layout.segnum * 75);
            //           self.aa[d.layout.segnum] = self.aa[d.layout.segnum] + 1;
            //           d3.select(this).attr("transform", "translate(" + d.x + "," + d.y + ")");
                      d3.select(this).select("rect")
                        .attr("x", function (d) { return -((maxWidth/d.layout.total) - 5)/2; } )
                        .attr("y", function (d) { return -d3.select(this).attr("height") / 2; } )
                        .attr("width", function (d) { return (maxWidth/d.layout.total) - 5; } );
            
                      d3.select(this).select("text.option_x").attr("transform", function(a) {
                        return "translate(" + (((maxWidth/d.layout.total)/2)-15) + ", -10)";
                      });
            
                      d3.select(this).select("text").attr("transform", function(a) {
                        const bbox = this.getBBox(); const pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
                        return "scale(" + Math.min(pbbox.width / (bbox.width + 20), 1) + ")";
                      });
                      //d3.select(this).select("rect").attr("width", (self.width - 5));
                   });
                   */
        }
    }, {
        key: 'toggleNavigation',
        value: function toggleNavigation(viewer, e, object) {
            d3.event.stopPropagation();
            if (viewer.downX == Math.round(e.x + e.y)) {
                viewer.navigation.toggle(e, d3.select(object));
            } else {
                viewer.navigation.close(e);
            }
        }
    }, {
        key: 'toggleTab',
        value: function toggleTab(viewer, d, currentTab) {
            var self = this;

            //d3.event.stopPropagation();

            // get the child tabs
            var children = d3.select(currentTab.parentNode).selectAll("g.node_" + d.name);

            if (children.size() > 0) {

                var toggleOn = children.style("display") == "none";

                this.turnOffPreviousTab(d, currentTab);

                //         if (toggleOn) {
                children.style("opacity", 0).transition().duration(1000).style("opacity", 1).style("display", "block");
                d3.select(currentTab).classed("selected_tab", true);
                //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "-"; });
                //         } else {
                //children.transition().duration(1000).style("opacity", 0).style("display","none"); //.each("end", function (e) { d3.select(this).style("display","none")
                //d3.select(currentTab).classed("selected_tab", false);
                //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "+"; });
                //         }
            } else {

                // if there are no child tabs, look for other content, such as another svg/screenshot/etc
                this.turnOffPreviousTab(d, currentTab);

                if (d3.select(".page").size() == 0) {
                    this.turnOffPreviousTab(d, currentTab);
                    d3.select(currentTab).classed("selected_tab", true);

                    if (!d.content) {
                        var content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);
                        content.style("top", "0px");
                        content.html("no content");
                        return;
                    }

                    var url = d.content.url.indexOf('?') == -1 ? d.content.url + "?r=" + Math.random() : d.content.url + "&r=" + Math.random();
                    console.log("Opening: " + url);
                    d3.json(url, function (json$$1) {

                        var content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);

                        //content.classed("level_" + d.layout.level, "true");

                        //content.style("overflow", "hidden");

                        self.refreshSize(viewer);

                        var vc = new ViewerCanvas();
                        vc.on("change", function (event$$1, _irrelevant_data, doc) {
                            viewer.trigger(event$$1, d, doc);
                        });
                        vc.mountNode(content.node());
                        vc.setState(json$$1);
                        vc.render();
                        vc.refreshSize();

                        self.vc = vc;
                    });
                }
            }
        }
    }, {
        key: 'refreshSize',
        value: function refreshSize(viewer) {
            var vb = d3.select(viewer.domNode).attr("viewBox").split(' ');

            var tab = d3.select(viewer.domNode).select('g.nodeSet').node().getBoundingClientRect();
            var bodyRect = document.body.getBoundingClientRect();

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            var pageWidth = d3.select(viewer.domNode).attr("width");
            var pageHeight = d3.select(viewer.domNode).attr("height");

            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;

            var top = Math.ceil(tab.top - bodyRect.top + tab.height + 2);

            var full = { "height": pageHeight, "width": pageWidth };

            var content = d3.select(viewer.domNode.parentNode).select('.page');

            content.style("top", top + "px");
            content.style("left", "0px");
            content.style("width", full.width + "px");
            content.style("height", full.height - top + "px");

            if (this.vc) {
                this.vc.refreshSize();
            }
        }
    }, {
        key: 'turnOffPreviousTab',
        value: function turnOffPreviousTab(tabData, currentTab) {
            // turn off any previously selected tab
            d3.select(currentTab.parentNode).selectAll("g.selected_tab").each(function (p) {
                // For all tabs that are selected at new tab level or higher (lower in position)
                if (tabData.layout.level <= p.layout.level) {
                    d3.select(this).classed("selected_tab", false);
                    var selectedChildren = d3.select(currentTab.parentNode).selectAll("g.node_" + p.name);
                    selectedChildren.transition().style("opacity", 0).style("display", "none");
                }
            });
            //d3.select(currentTab).classed("selected_tab", false);

            //     let self = this;
            //     if (typeof self.vc != "undefined" && self.vc != null) {
            //        console.log("destroy");
            //        self.vc.destroy();
            //        self.vc = null;
            //     }
            //
            //d3.select("#base").selectAll(".page").selectAll().remove();
            d3.select(currentTab.parentNode.parentNode.parentNode).selectAll(".page").remove();
        }
    }]);
    return Layout;
}();

__$styleInject("._timelines .background{fill:#fff}._timelines text{fill:#000;font-size:.7em;font-weight:700;font-family:Open Sans}._timelines .sat,.m2,.sun{fill:#add8e6}._timelines .mon,.fri,.thu,.tue,.wed{opacity:.7;fill:#fff}._timelines .jan,.jul,.mar,.may,.nov,.q1,.q3,.sep{fill:#add8e6;stroke-width:0}._timelines .feb,.apr,.aug,.dec,.jun,.oct,.q2,.q4{fill:#fff}._timelines .level1{fill:#000}._timelines .banner{fill:#fff;font-size:.6em}", undefined);

var hookCallback;

function hooks() {
    return hookCallback.apply(null, arguments);
}

// This is done to register the method called with moment()
// without creating circular dependencies.
function setHookCallback(callback) {
    hookCallback = callback;
}

function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
}

function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
}

function isObjectEmpty(obj) {
    var k;
    for (k in obj) {
        // even if its not own property I'd still call it non-empty
        return false;
    }
    return true;
}

function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
}

function map(arr, fn) {
    var res = [],
        i;
    for (i = 0; i < arr.length; ++i) {
        res.push(fn(arr[i], i));
    }
    return res;
}

function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}

function extend(a, b) {
    for (var i in b) {
        if (hasOwnProp(b, i)) {
            a[i] = b[i];
        }
    }

    if (hasOwnProp(b, 'toString')) {
        a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
        a.valueOf = b.valueOf;
    }

    return a;
}

function createUTC(input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}

function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
        empty: false,
        unusedTokens: [],
        unusedInput: [],
        overflow: -2,
        charsLeftOver: 0,
        nullInput: false,
        invalidMonth: null,
        invalidFormat: false,
        userInvalidated: false,
        iso: false,
        parsedDateParts: [],
        meridiem: null
    };
}

function getParsingFlags(m) {
    if (m._pf == null) {
        m._pf = defaultParsingFlags();
    }
    return m._pf;
}

var some;
if (Array.prototype.some) {
    some = Array.prototype.some;
} else {
    some = function some(fun) {
        var t = Object(this);
        var len = t.length >>> 0;

        for (var i = 0; i < len; i++) {
            if (i in t && fun.call(this, t[i], i, t)) {
                return true;
            }
        }

        return false;
    };
}

function isValid(m) {
    if (m._isValid == null) {
        var flags = getParsingFlags(m);
        var parsedParts = some.call(flags.parsedDateParts, function (i) {
            return i != null;
        });
        var isNowValid = !isNaN(m._d.getTime()) && flags.overflow < 0 && !flags.empty && !flags.invalidMonth && !flags.invalidWeekday && !flags.nullInput && !flags.invalidFormat && !flags.userInvalidated && (!flags.meridiem || flags.meridiem && parsedParts);

        if (m._strict) {
            isNowValid = isNowValid && flags.charsLeftOver === 0 && flags.unusedTokens.length === 0 && flags.bigHour === undefined;
        }

        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        } else {
            return isNowValid;
        }
    }
    return m._isValid;
}

function createInvalid(flags) {
    var m = createUTC(NaN);
    if (flags != null) {
        extend(getParsingFlags(m), flags);
    } else {
        getParsingFlags(m).userInvalidated = true;
    }

    return m;
}

function isUndefined(input) {
    return input === void 0;
}

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
var momentProperties = hooks.momentProperties = [];

function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
        to._isAMomentObject = from._isAMomentObject;
    }
    if (!isUndefined(from._i)) {
        to._i = from._i;
    }
    if (!isUndefined(from._f)) {
        to._f = from._f;
    }
    if (!isUndefined(from._l)) {
        to._l = from._l;
    }
    if (!isUndefined(from._strict)) {
        to._strict = from._strict;
    }
    if (!isUndefined(from._tzm)) {
        to._tzm = from._tzm;
    }
    if (!isUndefined(from._isUTC)) {
        to._isUTC = from._isUTC;
    }
    if (!isUndefined(from._offset)) {
        to._offset = from._offset;
    }
    if (!isUndefined(from._pf)) {
        to._pf = getParsingFlags(from);
    }
    if (!isUndefined(from._locale)) {
        to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
        for (i in momentProperties) {
            prop = momentProperties[i];
            val = from[prop];
            if (!isUndefined(val)) {
                to[prop] = val;
            }
        }
    }

    return to;
}

var updateInProgress = false;

// Moment prototype object
function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);
    if (!this.isValid()) {
        this._d = new Date(NaN);
    }
    // Prevent infinite loop in case updateOffset creates new moment
    // objects.
    if (updateInProgress === false) {
        updateInProgress = true;
        hooks.updateOffset(this);
        updateInProgress = false;
    }
}

function isMoment(obj) {
    return obj instanceof Moment || obj != null && obj._isAMomentObject != null;
}

function absFloor(number) {
    if (number < 0) {
        // -0 -> 0
        return Math.ceil(number) || 0;
    } else {
        return Math.floor(number);
    }
}

function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
        value = absFloor(coercedNumber);
    }

    return value;
}

// compare two arrays, return the number of differences
function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;
    for (i = 0; i < len; i++) {
        if (dontConvert && array1[i] !== array2[i] || !dontConvert && toInt(array1[i]) !== toInt(array2[i])) {
            diffs++;
        }
    }
    return diffs + lengthDiff;
}

function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false && typeof console !== 'undefined' && console.warn) {
        console.warn('Deprecation warning: ' + msg);
    }
}

function deprecate(msg, fn) {
    var firstTime = true;

    return extend(function () {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(null, msg);
        }
        if (firstTime) {
            var args = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = '';
                if (_typeof(arguments[i]) === 'object') {
                    arg += '\n[' + i + '] ';
                    for (var key in arguments[0]) {
                        arg += key + ': ' + arguments[0][key] + ', ';
                    }
                    arg = arg.slice(0, -2); // Remove trailing comma and space
                } else {
                    arg = arguments[i];
                }
                args.push(arg);
            }
            warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + new Error().stack);
            firstTime = false;
        }
        return fn.apply(this, arguments);
    }, fn);
}

var deprecations = {};

function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(name, msg);
    }
    if (!deprecations[name]) {
        warn(msg);
        deprecations[name] = true;
    }
}

hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;

function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
}

function set$1(config) {
    var prop, i;
    for (i in config) {
        prop = config[i];
        if (isFunction(prop)) {
            this[i] = prop;
        } else {
            this['_' + i] = prop;
        }
    }
    this._config = config;
    // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _ordinalParseLenient.
    this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + /\d{1,2}/.source);
}

function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig),
        prop;
    for (prop in childConfig) {
        if (hasOwnProp(childConfig, prop)) {
            if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                res[prop] = {};
                extend(res[prop], parentConfig[prop]);
                extend(res[prop], childConfig[prop]);
            } else if (childConfig[prop] != null) {
                res[prop] = childConfig[prop];
            } else {
                delete res[prop];
            }
        }
    }
    for (prop in parentConfig) {
        if (hasOwnProp(parentConfig, prop) && !hasOwnProp(childConfig, prop) && isObject(parentConfig[prop])) {
            // make sure changes to properties don't modify parent config
            res[prop] = extend({}, res[prop]);
        }
    }
    return res;
}

function Locale(config) {
    if (config != null) {
        this.set(config);
    }
}

var keys;

if (Object.keys) {
    keys = Object.keys;
} else {
    keys = function keys(obj) {
        var i,
            res = [];
        for (i in obj) {
            if (hasOwnProp(obj, i)) {
                res.push(i);
            }
        }
        return res;
    };
}

var defaultCalendar = {
    sameDay: '[Today at] LT',
    nextDay: '[Tomorrow at] LT',
    nextWeek: 'dddd [at] LT',
    lastDay: '[Yesterday at] LT',
    lastWeek: '[Last] dddd [at] LT',
    sameElse: 'L'
};

function calendar(key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
}

var defaultLongDateFormat = {
    LTS: 'h:mm:ss A',
    LT: 'h:mm A',
    L: 'MM/DD/YYYY',
    LL: 'MMMM D, YYYY',
    LLL: 'MMMM D, YYYY h:mm A',
    LLLL: 'dddd, MMMM D, YYYY h:mm A'
};

function longDateFormat(key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
        return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
        return val.slice(1);
    });

    return this._longDateFormat[key];
}

var defaultInvalidDate = 'Invalid date';

function invalidDate() {
    return this._invalidDate;
}

var defaultOrdinal = '%d';
var defaultOrdinalParse = /\d{1,2}/;

function ordinal(number) {
    return this._ordinal.replace('%d', number);
}

var defaultRelativeTime = {
    future: 'in %s',
    past: '%s ago',
    s: 'a few seconds',
    m: 'a minute',
    mm: '%d minutes',
    h: 'an hour',
    hh: '%d hours',
    d: 'a day',
    dd: '%d days',
    M: 'a month',
    MM: '%d months',
    y: 'a year',
    yy: '%d years'
};

function relativeTime(number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return isFunction(output) ? output(number, withoutSuffix, string, isFuture) : output.replace(/%d/i, number);
}

function pastFuture(diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
}

var aliases = {};

function addUnitAlias(unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
}

function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
}

function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
        if (hasOwnProp(inputObject, prop)) {
            normalizedProp = normalizeUnits(prop);
            if (normalizedProp) {
                normalizedInput[normalizedProp] = inputObject[prop];
            }
        }
    }

    return normalizedInput;
}

var priorities = {};

function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
}

function getPrioritizedUnits(unitsObj) {
    var units = [];
    for (var u in unitsObj) {
        units.push({ unit: u, priority: priorities[u] });
    }
    units.sort(function (a, b) {
        return a.priority - b.priority;
    });
    return units;
}

function makeGetSet(unit, keepTime) {
    return function (value) {
        if (value != null) {
            set$2(this, unit, value);
            hooks.updateOffset(this, keepTime);
            return this;
        } else {
            return get$1(this, unit);
        }
    };
}

function get$1(mom, unit) {
    return mom.isValid() ? mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
}

function set$2(mom, unit, value) {
    if (mom.isValid()) {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
    }
}

// MOMENTS

function stringGet(units) {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
        return this[units]();
    }
    return this;
}

function stringSet(units, value) {
    if ((typeof units === 'undefined' ? 'undefined' : _typeof(units)) === 'object') {
        units = normalizeObjectUnits(units);
        var prioritized = getPrioritizedUnits(units);
        for (var i = 0; i < prioritized.length; i++) {
            this[prioritized[i].unit](units[prioritized[i].unit]);
        }
    } else {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units](value);
        }
    }
    return this;
}

function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? forceSign ? '+' : '' : '-') + Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}

var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

var formatFunctions = {};

var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
function addFormatToken(token, padded, ordinal, callback) {
    var func = callback;
    if (typeof callback === 'string') {
        func = function func() {
            return this[callback]();
        };
    }
    if (token) {
        formatTokenFunctions[token] = func;
    }
    if (padded) {
        formatTokenFunctions[padded[0]] = function () {
            return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
        };
    }
    if (ordinal) {
        formatTokenFunctions[ordinal] = function () {
            return this.localeData().ordinal(func.apply(this, arguments), token);
        };
    }
}

function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
        return input.replace(/^\[|\]$/g, '');
    }
    return input.replace(/\\/g, '');
}

function makeFormatFunction(format) {
    var array = format.match(formattingTokens),
        i,
        length;

    for (i = 0, length = array.length; i < length; i++) {
        if (formatTokenFunctions[array[i]]) {
            array[i] = formatTokenFunctions[array[i]];
        } else {
            array[i] = removeFormattingTokens(array[i]);
        }
    }

    return function (mom) {
        var output = '',
            i;
        for (i = 0; i < length; i++) {
            output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
        }
        return output;
    };
}

// format date using native date object
function formatMoment(m, format) {
    if (!m.isValid()) {
        return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    return formatFunctions[format](m);
}

function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
        return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;
    while (i >= 0 && localFormattingTokens.test(format)) {
        format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
        localFormattingTokens.lastIndex = 0;
        i -= 1;
    }

    return format;
}

var match1 = /\d/; //       0 - 9
var match2 = /\d\d/; //      00 - 99
var match3 = /\d{3}/; //     000 - 999
var match4 = /\d{4}/; //    0000 - 9999
var match6 = /[+-]?\d{6}/; // -999999 - 999999
var match1to2 = /\d\d?/; //       0 - 99
var match3to4 = /\d\d\d\d?/; //     999 - 9999
var match5to6 = /\d\d\d\d\d\d?/; //   99999 - 999999
var match1to3 = /\d{1,3}/; //       0 - 999
var match1to4 = /\d{1,4}/; //       0 - 9999
var match1to6 = /[+-]?\d{1,6}/; // -999999 - 999999

var matchUnsigned = /\d+/; //       0 - inf
var matchSigned = /[+-]?\d+/; //    -inf - inf

var matchOffset = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;

var regexes = {};

function addRegexToken(token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
        return isStrict && strictRegex ? strictRegex : regex;
    };
}

function getParseRegexForToken(token, config) {
    if (!hasOwnProp(regexes, token)) {
        return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
}

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
    }));
}

function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

var tokens = {};

function addParseToken(token, callback) {
    var i,
        func = callback;
    if (typeof token === 'string') {
        token = [token];
    }
    if (isNumber(callback)) {
        func = function func(input, array) {
            array[callback] = toInt(input);
        };
    }
    for (i = 0; i < token.length; i++) {
        tokens[token[i]] = func;
    }
}

function addWeekParseToken(token, callback) {
    addParseToken(token, function (input, array, config, token) {
        config._w = config._w || {};
        callback(input, config._w, config, token);
    });
}

function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
        tokens[token](input, config._a, config, token);
    }
}

var YEAR = 0;
var MONTH = 1;
var DATE = 2;
var HOUR = 3;
var MINUTE = 4;
var SECOND = 5;
var MILLISECOND = 6;
var WEEK = 7;
var WEEKDAY = 8;

var indexOf;

if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
} else {
    indexOf = function indexOf(o) {
        // I know
        var i;
        for (i = 0; i < this.length; ++i) {
            if (this[i] === o) {
                return i;
            }
        }
        return -1;
    };
}

function daysInMonth(year, month) {
    return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
}

// FORMATTING

addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
});

addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
});

addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
});

// ALIASES

addUnitAlias('month', 'M');

// PRIORITY

addUnitPriority('month', 8);

// PARSING

addRegexToken('M', match1to2);
addRegexToken('MM', match1to2, match2);
addRegexToken('MMM', function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
});
addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
});

addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
});

addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict);
    // if we didn't find a month name, mark the date as invalid.
    if (month != null) {
        array[MONTH] = month;
    } else {
        getParsingFlags(config).invalidMonth = input;
    }
});

// LOCALES

var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
function localeMonths(m, format) {
    if (!m) {
        return this._months;
    }
    return isArray(this._months) ? this._months[m.month()] : this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
}

var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
function localeMonthsShort(m, format) {
    if (!m) {
        return this._monthsShort;
    }
    return isArray(this._monthsShort) ? this._monthsShort[m.month()] : this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
}

function handleStrictParse(monthName, format, strict) {
    var i,
        ii,
        mom,
        llc = monthName.toLocaleLowerCase();
    if (!this._monthsParse) {
        // this is not used
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
        for (i = 0; i < 12; ++i) {
            mom = createUTC([2000, i]);
            this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
            this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'MMM') {
            ii = indexOf.call(this._shortMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._longMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeMonthsParse(monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
        return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
    }

    // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        if (strict && !this._longMonthsParse[i]) {
            this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
        }
        if (!strict && !this._monthsParse[i]) {
            regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            return i;
        } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            return i;
        } else if (!strict && this._monthsParse[i].test(monthName)) {
            return i;
        }
    }
}

// MOMENTS

function setMonth(mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
        // No op
        return mom;
    }

    if (typeof value === 'string') {
        if (/^\d+$/.test(value)) {
            value = toInt(value);
        } else {
            value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (!isNumber(value)) {
                return mom;
            }
        }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
    return mom;
}

function getSetMonth(value) {
    if (value != null) {
        setMonth(this, value);
        hooks.updateOffset(this, true);
        return this;
    } else {
        return get$1(this, 'Month');
    }
}

function getDaysInMonth() {
    return daysInMonth(this.year(), this.month());
}

var defaultMonthsShortRegex = matchWord;
function monthsShortRegex(isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsShortStrictRegex;
        } else {
            return this._monthsShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsShortRegex')) {
            this._monthsShortRegex = defaultMonthsShortRegex;
        }
        return this._monthsShortStrictRegex && isStrict ? this._monthsShortStrictRegex : this._monthsShortRegex;
    }
}

var defaultMonthsRegex = matchWord;
function monthsRegex(isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsStrictRegex;
        } else {
            return this._monthsRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsRegex')) {
            this._monthsRegex = defaultMonthsRegex;
        }
        return this._monthsStrictRegex && isStrict ? this._monthsStrictRegex : this._monthsRegex;
    }
}

function computeMonthsParse() {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var shortPieces = [],
        longPieces = [],
        mixedPieces = [],
        i,
        mom;
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        shortPieces.push(this.monthsShort(mom, ''));
        longPieces.push(this.months(mom, ''));
        mixedPieces.push(this.months(mom, ''));
        mixedPieces.push(this.monthsShort(mom, ''));
    }
    // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 12; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
    }
    for (i = 0; i < 24; i++) {
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
}

// FORMATTING

addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
});

addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
});

addFormatToken(0, ['YYYY', 4], 0, 'year');
addFormatToken(0, ['YYYYY', 5], 0, 'year');
addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

addUnitAlias('year', 'y');

// PRIORITIES

addUnitPriority('year', 1);

// PARSING

addRegexToken('Y', matchSigned);
addRegexToken('YY', match1to2, match2);
addRegexToken('YYYY', match1to4, match4);
addRegexToken('YYYYY', match1to6, match6);
addRegexToken('YYYYYY', match1to6, match6);

addParseToken(['YYYYY', 'YYYYYY'], YEAR);
addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
});

// HELPERS

function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
    return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
}

// HOOKS

hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
};

// MOMENTS

var getSetYear = makeGetSet('FullYear', true);

function getIsLeapYear() {
    return isLeapYear(this.year());
}

function createDate(y, m, d, h, M, s, ms) {
    //can't just apply() to create a date:
    //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
    var date = new Date(y, m, d, h, M, s, ms);

    //the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
        date.setFullYear(y);
    }
    return date;
}

function createUTCDate(y) {
    var date = new Date(Date.UTC.apply(null, arguments));

    //the Date.UTC function remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
    }
    return date;
}

// start-of-first-week - start-of-year
function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
    fwd = 7 + dow - doy,

    // first-week day local weekday -- which local weekday is fwd
    fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

    return -fwdlw + fwd - 1;
}

//http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear,
        resDayOfYear;

    if (dayOfYear <= 0) {
        resYear = year - 1;
        resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
        resYear = year + 1;
        resDayOfYear = dayOfYear - daysInYear(year);
    } else {
        resYear = year;
        resDayOfYear = dayOfYear;
    }

    return {
        year: resYear,
        dayOfYear: resDayOfYear
    };
}

function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek,
        resYear;

    if (week < 1) {
        resYear = mom.year() - 1;
        resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
        resWeek = week - weeksInYear(mom.year(), dow, doy);
        resYear = mom.year() + 1;
    } else {
        resYear = mom.year();
        resWeek = week;
    }

    return {
        week: resWeek,
        year: resYear
    };
}

function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}

// FORMATTING

addFormatToken('w', ['ww', 2], 'wo', 'week');
addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

addUnitAlias('week', 'w');
addUnitAlias('isoWeek', 'W');

// PRIORITIES

addUnitPriority('week', 5);
addUnitPriority('isoWeek', 5);

// PARSING

addRegexToken('w', match1to2);
addRegexToken('ww', match1to2, match2);
addRegexToken('W', match1to2);
addRegexToken('WW', match1to2, match2);

addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
});

// HELPERS

// LOCALES

function localeWeek(mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
}

var defaultLocaleWeek = {
    dow: 0, // Sunday is the first day of the week.
    doy: 6 // The week that contains Jan 1st is the first week of the year.
};

function localeFirstDayOfWeek() {
    return this._week.dow;
}

function localeFirstDayOfYear() {
    return this._week.doy;
}

// MOMENTS

function getSetWeek(input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
}

function getSetISOWeek(input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
}

// FORMATTING

addFormatToken('d', 0, 'do', 'day');

addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
});

addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
});

addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
});

addFormatToken('e', 0, 0, 'weekday');
addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

addUnitAlias('day', 'd');
addUnitAlias('weekday', 'e');
addUnitAlias('isoWeekday', 'E');

// PRIORITY
addUnitPriority('day', 11);
addUnitPriority('weekday', 11);
addUnitPriority('isoWeekday', 11);

// PARSING

addRegexToken('d', match1to2);
addRegexToken('e', match1to2);
addRegexToken('E', match1to2);
addRegexToken('dd', function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
});
addRegexToken('ddd', function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
});
addRegexToken('dddd', function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
});

addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict);
    // if we didn't get a weekday name, mark the date as invalid
    if (weekday != null) {
        week.d = weekday;
    } else {
        getParsingFlags(config).invalidWeekday = input;
    }
});

addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
});

// HELPERS

function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
        return input;
    }

    if (!isNaN(input)) {
        return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);
    if (typeof input === 'number') {
        return input;
    }

    return null;
}

function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
        return locale.weekdaysParse(input) % 7 || 7;
    }
    return isNaN(input) ? null : input;
}

// LOCALES

var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
function localeWeekdays(m, format) {
    if (!m) {
        return this._weekdays;
    }
    return isArray(this._weekdays) ? this._weekdays[m.day()] : this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
}

var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
function localeWeekdaysShort(m) {
    return m ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}

var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
function localeWeekdaysMin(m) {
    return m ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}

function handleStrictParse$1(weekdayName, format, strict) {
    var i,
        ii,
        mom,
        llc = weekdayName.toLocaleLowerCase();
    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._minWeekdaysParse = [];

        for (i = 0; i < 7; ++i) {
            mom = createUTC([2000, 1]).day(i);
            this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
            this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
            this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'dddd') {
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf.call(this._minWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeWeekdaysParse(weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
        return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._minWeekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already

        mom = createUTC([2000, 1]).day(i);
        if (strict && !this._fullWeekdaysParse[i]) {
            this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
            this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
            this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
        }
        if (!this._weekdaysParse[i]) {
            regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
            return i;
        }
    }
}

// MOMENTS

function getSetDayOfWeek(input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    if (input != null) {
        input = parseWeekday(input, this.localeData());
        return this.add(input - day, 'd');
    } else {
        return day;
    }
}

function getSetLocaleDayOfWeek(input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
}

function getSetISODayOfWeek(input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }

    // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.

    if (input != null) {
        var weekday = parseIsoWeekday(input, this.localeData());
        return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
        return this.day() || 7;
    }
}

var defaultWeekdaysRegex = matchWord;
function weekdaysRegex(isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysStrictRegex;
        } else {
            return this._weekdaysRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            this._weekdaysRegex = defaultWeekdaysRegex;
        }
        return this._weekdaysStrictRegex && isStrict ? this._weekdaysStrictRegex : this._weekdaysRegex;
    }
}

var defaultWeekdaysShortRegex = matchWord;
function weekdaysShortRegex(isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysShortStrictRegex;
        } else {
            return this._weekdaysShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysShortRegex')) {
            this._weekdaysShortRegex = defaultWeekdaysShortRegex;
        }
        return this._weekdaysShortStrictRegex && isStrict ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
}

var defaultWeekdaysMinRegex = matchWord;
function weekdaysMinRegex(isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysMinStrictRegex;
        } else {
            return this._weekdaysMinRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysMinRegex')) {
            this._weekdaysMinRegex = defaultWeekdaysMinRegex;
        }
        return this._weekdaysMinStrictRegex && isStrict ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
}

function computeWeekdaysParse() {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var minPieces = [],
        shortPieces = [],
        longPieces = [],
        mixedPieces = [],
        i,
        mom,
        minp,
        shortp,
        longp;
    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, 1]).day(i);
        minp = this.weekdaysMin(mom, '');
        shortp = this.weekdaysShort(mom, '');
        longp = this.weekdays(mom, '');
        minPieces.push(minp);
        shortPieces.push(shortp);
        longPieces.push(longp);
        mixedPieces.push(minp);
        mixedPieces.push(shortp);
        mixedPieces.push(longp);
    }
    // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.
    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 7; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;

    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
}

// FORMATTING

function hFormat() {
    return this.hours() % 12 || 12;
}

function kFormat() {
    return this.hours() || 24;
}

addFormatToken('H', ['HH', 2], 0, 'hour');
addFormatToken('h', ['hh', 2], 0, hFormat);
addFormatToken('k', ['kk', 2], 0, kFormat);

addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});

addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
});

addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
});

addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
});

function meridiem(token, lowercase) {
    addFormatToken(token, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
}

meridiem('a', true);
meridiem('A', false);

// ALIASES

addUnitAlias('hour', 'h');

// PRIORITY
addUnitPriority('hour', 13);

// PARSING

function matchMeridiem(isStrict, locale) {
    return locale._meridiemParse;
}

addRegexToken('a', matchMeridiem);
addRegexToken('A', matchMeridiem);
addRegexToken('H', match1to2);
addRegexToken('h', match1to2);
addRegexToken('HH', match1to2, match2);
addRegexToken('hh', match1to2, match2);

addRegexToken('hmm', match3to4);
addRegexToken('hmmss', match5to6);
addRegexToken('Hmm', match3to4);
addRegexToken('Hmmss', match5to6);

addParseToken(['H', 'HH'], HOUR);
addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
});
addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
});
addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
});
addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
});

// LOCALES

function localeIsPM(input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return (input + '').toLowerCase().charAt(0) === 'p';
}

var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
function localeMeridiem(hours, minutes, isLower) {
    if (hours > 11) {
        return isLower ? 'pm' : 'PM';
    } else {
        return isLower ? 'am' : 'AM';
    }
}

// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    ordinalParse: defaultOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};

// internal storage for locale config files
var locales = {};
var localeFamilies = {};
var globalLocale;

function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
}

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
function chooseLocale(names) {
    var i = 0,
        j,
        next,
        locale,
        split;

    while (i < names.length) {
        split = normalizeLocale(names[i]).split('-');
        j = split.length;
        next = normalizeLocale(names[i + 1]);
        next = next ? next.split('-') : null;
        while (j > 0) {
            locale = loadLocale(split.slice(0, j).join('-'));
            if (locale) {
                return locale;
            }
            if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                //the next array item is better than a shallower substring of this one
                break;
            }
            j--;
        }
        i++;
    }
    return null;
}

function loadLocale(name) {
    var oldLocale = null;
    // TODO: Find a better way to register and load all the locales in Node
    if (!locales[name] && typeof module !== 'undefined' && module && module.exports) {
        try {
            oldLocale = globalLocale._abbr;
            require('./locale/' + name);
            // because defineLocale currently also sets the global locale, we
            // want to undo that for lazy loaded locales
            getSetGlobalLocale(oldLocale);
        } catch (e) {}
    }
    return locales[name];
}

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
function getSetGlobalLocale(key, values) {
    var data;
    if (key) {
        if (isUndefined(values)) {
            data = getLocale(key);
        } else {
            data = defineLocale(key, values);
        }

        if (data) {
            // moment.duration._locale = moment._locale = data;
            globalLocale = data;
        }
    }

    return globalLocale._abbr;
}

function defineLocale(name, config) {
    if (config !== null) {
        var parentConfig = baseConfig;
        config.abbr = name;
        if (locales[name] != null) {
            deprecateSimple('defineLocaleOverride', 'use moment.updateLocale(localeName, config) to change ' + 'an existing locale. moment.defineLocale(localeName, ' + 'config) should only be used for creating a new locale ' + 'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
            parentConfig = locales[name]._config;
        } else if (config.parentLocale != null) {
            if (locales[config.parentLocale] != null) {
                parentConfig = locales[config.parentLocale]._config;
            } else {
                if (!localeFamilies[config.parentLocale]) {
                    localeFamilies[config.parentLocale] = [];
                }
                localeFamilies[config.parentLocale].push({
                    name: name,
                    config: config
                });
                return null;
            }
        }
        locales[name] = new Locale(mergeConfigs(parentConfig, config));

        if (localeFamilies[name]) {
            localeFamilies[name].forEach(function (x) {
                defineLocale(x.name, x.config);
            });
        }

        // backwards compat for now: also set the locale
        // make sure we set the locale AFTER all child locales have been
        // created, so we won't end up with the child locale set.
        getSetGlobalLocale(name);

        return locales[name];
    } else {
        // useful for testing
        delete locales[name];
        return null;
    }
}

function updateLocale(name, config) {
    if (config != null) {
        var locale,
            parentConfig = baseConfig;
        // MERGE
        if (locales[name] != null) {
            parentConfig = locales[name]._config;
        }
        config = mergeConfigs(parentConfig, config);
        locale = new Locale(config);
        locale.parentLocale = locales[name];
        locales[name] = locale;

        // backwards compat for now: also set the locale
        getSetGlobalLocale(name);
    } else {
        // pass null for config to unupdate, useful for tests
        if (locales[name] != null) {
            if (locales[name].parentLocale != null) {
                locales[name] = locales[name].parentLocale;
            } else if (locales[name] != null) {
                delete locales[name];
            }
        }
    }
    return locales[name];
}

// returns locale data
function getLocale(key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
        key = key._locale._abbr;
    }

    if (!key) {
        return globalLocale;
    }

    if (!isArray(key)) {
        //short-circuit everything else
        locale = loadLocale(key);
        if (locale) {
            return locale;
        }
        key = [key];
    }

    return chooseLocale(key);
}

function listLocales() {
    return keys(locales);
}

function checkOverflow(m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
        overflow = a[MONTH] < 0 || a[MONTH] > 11 ? MONTH : a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH]) ? DATE : a[HOUR] < 0 || a[HOUR] > 24 || a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0) ? HOUR : a[MINUTE] < 0 || a[MINUTE] > 59 ? MINUTE : a[SECOND] < 0 || a[SECOND] > 59 ? SECOND : a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND : -1;

        if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
            overflow = DATE;
        }
        if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
            overflow = WEEK;
        }
        if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
            overflow = WEEKDAY;
        }

        getParsingFlags(m).overflow = overflow;
    }

    return m;
}

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

var isoDates = [['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/], ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/], ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/], ['GGGG-[W]WW', /\d{4}-W\d\d/, false], ['YYYY-DDD', /\d{4}-\d{3}/], ['YYYY-MM', /\d{4}-\d\d/, false], ['YYYYYYMMDD', /[+-]\d{10}/], ['YYYYMMDD', /\d{8}/],
// YYYYMM is NOT allowed by the standard
['GGGG[W]WWE', /\d{4}W\d{3}/], ['GGGG[W]WW', /\d{4}W\d{2}/, false], ['YYYYDDD', /\d{7}/]];

// iso time formats and regexes
var isoTimes = [['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/], ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/], ['HH:mm:ss', /\d\d:\d\d:\d\d/], ['HH:mm', /\d\d:\d\d/], ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/], ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/], ['HHmmss', /\d\d\d\d\d\d/], ['HHmm', /\d\d\d\d/], ['HH', /\d\d/]];

var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
function configFromISO(config) {
    var i,
        l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime,
        dateFormat,
        timeFormat,
        tzFormat;

    if (match) {
        getParsingFlags(config).iso = true;

        for (i = 0, l = isoDates.length; i < l; i++) {
            if (isoDates[i][1].exec(match[1])) {
                dateFormat = isoDates[i][0];
                allowTime = isoDates[i][2] !== false;
                break;
            }
        }
        if (dateFormat == null) {
            config._isValid = false;
            return;
        }
        if (match[3]) {
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(match[3])) {
                    // match[2] should be 'T' or space
                    timeFormat = (match[2] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (timeFormat == null) {
                config._isValid = false;
                return;
            }
        }
        if (!allowTime && timeFormat != null) {
            config._isValid = false;
            return;
        }
        if (match[4]) {
            if (tzRegex.exec(match[4])) {
                tzFormat = 'Z';
            } else {
                config._isValid = false;
                return;
            }
        }
        config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
        configFromStringAndFormat(config);
    } else {
        config._isValid = false;
    }
}

// date from iso format or fallback
function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
        config._d = new Date(+matched[1]);
        return;
    }

    configFromISO(config);
    if (config._isValid === false) {
        delete config._isValid;
        hooks.createFromInputFallback(config);
    }
}

hooks.createFromInputFallback = deprecate('value provided is not in a recognized ISO format. moment construction falls back to js Date(), ' + 'which is not reliable across all browsers and versions. Non ISO date formats are ' + 'discouraged and will be removed in an upcoming major release. Please refer to ' + 'http://momentjs.com/guides/#/warnings/js-date/ for more info.', function (config) {
    config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
});

// Pick the first defined of two or three arguments.
function defaults$1(a, b, c) {
    if (a != null) {
        return a;
    }
    if (b != null) {
        return b;
    }
    return c;
}

function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());
    if (config._useUTC) {
        return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }
    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
function configFromArray(config) {
    var i,
        date,
        input = [],
        currentDate,
        yearToUse;

    if (config._d) {
        return;
    }

    currentDate = currentDateArray(config);

    //compute day of the year from weeks and weekdays
    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        dayOfYearFromWeekInfo(config);
    }

    //if the day of the year is set, figure out what it is
    if (config._dayOfYear) {
        yearToUse = defaults$1(config._a[YEAR], currentDate[YEAR]);

        if (config._dayOfYear > daysInYear(yearToUse)) {
            getParsingFlags(config)._overflowDayOfYear = true;
        }

        date = createUTCDate(yearToUse, 0, config._dayOfYear);
        config._a[MONTH] = date.getUTCMonth();
        config._a[DATE] = date.getUTCDate();
    }

    // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything
    for (i = 0; i < 3 && config._a[i] == null; ++i) {
        config._a[i] = input[i] = currentDate[i];
    }

    // Zero out whatever was not defaulted, including time
    for (; i < 7; i++) {
        config._a[i] = input[i] = config._a[i] == null ? i === 2 ? 1 : 0 : config._a[i];
    }

    // Check for 24:00:00.000
    if (config._a[HOUR] === 24 && config._a[MINUTE] === 0 && config._a[SECOND] === 0 && config._a[MILLISECOND] === 0) {
        config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.
    if (config._tzm != null) {
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
        config._a[HOUR] = 24;
    }
}

function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

    w = config._w;
    if (w.GG != null || w.W != null || w.E != null) {
        dow = 1;
        doy = 4;

        // TODO: We need to take the current isoWeekYear, but that depends on
        // how we interpret now (local, utc, fixed offset). So create
        // a now version of current config (take local/utc/offset flags, and
        // create now).
        weekYear = defaults$1(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
        week = defaults$1(w.W, 1);
        weekday = defaults$1(w.E, 1);
        if (weekday < 1 || weekday > 7) {
            weekdayOverflow = true;
        }
    } else {
        dow = config._locale._week.dow;
        doy = config._locale._week.doy;

        var curWeek = weekOfYear(createLocal(), dow, doy);

        weekYear = defaults$1(w.gg, config._a[YEAR], curWeek.year);

        // Default to current week.
        week = defaults$1(w.w, curWeek.week);

        if (w.d != null) {
            // weekday -- low day numbers are considered next week
            weekday = w.d;
            if (weekday < 0 || weekday > 6) {
                weekdayOverflow = true;
            }
        } else if (w.e != null) {
            // local weekday -- counting starts from begining of week
            weekday = w.e + dow;
            if (w.e < 0 || w.e > 6) {
                weekdayOverflow = true;
            }
        } else {
            // default to begining of week
            weekday = dow;
        }
    }
    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
        getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
        getParsingFlags(config)._overflowWeekday = true;
    } else {
        temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }
}

// constant that refers to the ISO standard
hooks.ISO_8601 = function () {};

// date from string and format string
function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
        configFromISO(config);
        return;
    }

    config._a = [];
    getParsingFlags(config).empty = true;

    // This array is used to make a Date, either with `new Date` or `Date.UTC`
    var string = '' + config._i,
        i,
        parsedInput,
        tokens,
        token,
        skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
        token = tokens[i];
        parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        // console.log('token', token, 'parsedInput', parsedInput,
        //         'regex', getParseRegexForToken(token, config));
        if (parsedInput) {
            skipped = string.substr(0, string.indexOf(parsedInput));
            if (skipped.length > 0) {
                getParsingFlags(config).unusedInput.push(skipped);
            }
            string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
            totalParsedInputLength += parsedInput.length;
        }
        // don't parse if it's not a known token
        if (formatTokenFunctions[token]) {
            if (parsedInput) {
                getParsingFlags(config).empty = false;
            } else {
                getParsingFlags(config).unusedTokens.push(token);
            }
            addTimeToArrayFromToken(token, parsedInput, config);
        } else if (config._strict && !parsedInput) {
            getParsingFlags(config).unusedTokens.push(token);
        }
    }

    // add remaining unparsed input length to the string
    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
    if (string.length > 0) {
        getParsingFlags(config).unusedInput.push(string);
    }

    // clear _12h flag if hour is <= 12
    if (config._a[HOUR] <= 12 && getParsingFlags(config).bigHour === true && config._a[HOUR] > 0) {
        getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem;
    // handle meridiem
    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

    configFromArray(config);
    checkOverflow(config);
}

function meridiemFixWrap(locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
        // nothing to do
        return hour;
    }
    if (locale.meridiemHour != null) {
        return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
        // Fallback
        isPm = locale.isPM(meridiem);
        if (isPm && hour < 12) {
            hour += 12;
        }
        if (!isPm && hour === 12) {
            hour = 0;
        }
        return hour;
    } else {
        // this is not supposed to happen
        return hour;
    }
}

// date from string and array of format strings
function configFromStringAndArray(config) {
    var tempConfig, bestMoment, scoreToBeat, i, currentScore;

    if (config._f.length === 0) {
        getParsingFlags(config).invalidFormat = true;
        config._d = new Date(NaN);
        return;
    }

    for (i = 0; i < config._f.length; i++) {
        currentScore = 0;
        tempConfig = copyConfig({}, config);
        if (config._useUTC != null) {
            tempConfig._useUTC = config._useUTC;
        }
        tempConfig._f = config._f[i];
        configFromStringAndFormat(tempConfig);

        if (!isValid(tempConfig)) {
            continue;
        }

        // if there is any input that was not parsed add a penalty for that format
        currentScore += getParsingFlags(tempConfig).charsLeftOver;

        //or tokens
        currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

        getParsingFlags(tempConfig).score = currentScore;

        if (scoreToBeat == null || currentScore < scoreToBeat) {
            scoreToBeat = currentScore;
            bestMoment = tempConfig;
        }
    }

    extend(config, bestMoment || tempConfig);
}

function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}

function createFromConfig(config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

function prepareConfig(config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || format === undefined && input === '') {
        return createInvalid({ nullInput: true });
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
        config._d = input;
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    } else {
        configFromInput(config);
    }

    if (!isValid(config)) {
        config._d = null;
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (input === undefined) {
        config._d = new Date(hooks.now());
    } else if (isDate(input)) {
        config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if ((typeof input === 'undefined' ? 'undefined' : _typeof(input)) === 'object') {
        configFromObject(config);
    } else if (isNumber(input)) {
        // from milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

function createLocalOrUTC(input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
        strict = locale;
        locale = undefined;
    }

    if (isObject(input) && isObjectEmpty(input) || isArray(input) && input.length === 0) {
        input = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}

function createLocal(input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}

var prototypeMin = deprecate('moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
    var other = createLocal.apply(null, arguments);
    if (this.isValid() && other.isValid()) {
        return other < this ? this : other;
    } else {
        return createInvalid();
    }
});

var prototypeMax = deprecate('moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
    var other = createLocal.apply(null, arguments);
    if (this.isValid() && other.isValid()) {
        return other > this ? this : other;
    } else {
        return createInvalid();
    }
});

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
function pickBy(fn, moments) {
    var res, i;
    if (moments.length === 1 && isArray(moments[0])) {
        moments = moments[0];
    }
    if (!moments.length) {
        return createLocal();
    }
    res = moments[0];
    for (i = 1; i < moments.length; ++i) {
        if (!moments[i].isValid() || moments[i][fn](res)) {
            res = moments[i];
        }
    }
    return res;
}

// TODO: Use [].sort instead?
function min() {
    var args = [].slice.call(arguments, 0);

    return pickBy('isBefore', args);
}

function max() {
    var args = [].slice.call(arguments, 0);

    return pickBy('isAfter', args);
}

var now = function now() {
    return Date.now ? Date.now() : +new Date();
};

function Duration(duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;

    // representation for dateAddRemove
    this._milliseconds = +milliseconds + seconds * 1e3 + // 1000
    minutes * 6e4 + // 1000 * 60
    hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately
    this._days = +days + weeks * 7;
    // It is impossible translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.
    this._months = +months + quarters * 3 + years * 12;

    this._data = {};

    this._locale = getLocale();

    this._bubble();
}

function isDuration(obj) {
    return obj instanceof Duration;
}

function absRound(number) {
    if (number < 0) {
        return Math.round(-1 * number) * -1;
    } else {
        return Math.round(number);
    }
}

// FORMATTING

function offset(token, separator) {
    addFormatToken(token, 0, 0, function () {
        var offset = this.utcOffset();
        var sign = '+';
        if (offset < 0) {
            offset = -offset;
            sign = '-';
        }
        return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~offset % 60, 2);
    });
}

offset('Z', ':');
offset('ZZ', '');

// PARSING

addRegexToken('Z', matchShortOffset);
addRegexToken('ZZ', matchShortOffset);
addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
});

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
var chunkOffset = /([\+\-]|\d\d)/gi;

function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
        return null;
    }

    var chunk = matches[matches.length - 1] || [];
    var parts = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);

    return minutes === 0 ? 0 : parts[0] === '+' ? minutes : -minutes;
}

// Return a moment from input, that is local/utc/zone equivalent to model.
function cloneWithOffset(input, model) {
    var res, diff;
    if (model._isUTC) {
        res = model.clone();
        diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
        // Use low-level api, because this fn is low-level api.
        res._d.setTime(res._d.valueOf() + diff);
        hooks.updateOffset(res, false);
        return res;
    } else {
        return createLocal(input).local();
    }
}

function getDateOffset(m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
}

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
function getSetOffset(input, keepLocalTime) {
    var offset = this._offset || 0,
        localAdjust;
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    if (input != null) {
        if (typeof input === 'string') {
            input = offsetFromString(matchShortOffset, input);
            if (input === null) {
                return this;
            }
        } else if (Math.abs(input) < 16) {
            input = input * 60;
        }
        if (!this._isUTC && keepLocalTime) {
            localAdjust = getDateOffset(this);
        }
        this._offset = input;
        this._isUTC = true;
        if (localAdjust != null) {
            this.add(localAdjust, 'm');
        }
        if (offset !== input) {
            if (!keepLocalTime || this._changeInProgress) {
                addSubtract(this, createDuration(input - offset, 'm'), 1, false);
            } else if (!this._changeInProgress) {
                this._changeInProgress = true;
                hooks.updateOffset(this, true);
                this._changeInProgress = null;
            }
        }
        return this;
    } else {
        return this._isUTC ? offset : getDateOffset(this);
    }
}

function getSetZone(input, keepLocalTime) {
    if (input != null) {
        if (typeof input !== 'string') {
            input = -input;
        }

        this.utcOffset(input, keepLocalTime);

        return this;
    } else {
        return -this.utcOffset();
    }
}

function setOffsetToUTC(keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
}

function setOffsetToLocal(keepLocalTime) {
    if (this._isUTC) {
        this.utcOffset(0, keepLocalTime);
        this._isUTC = false;

        if (keepLocalTime) {
            this.subtract(getDateOffset(this), 'm');
        }
    }
    return this;
}

function setOffsetToParsedOffset() {
    if (this._tzm != null) {
        this.utcOffset(this._tzm);
    } else if (typeof this._i === 'string') {
        var tZone = offsetFromString(matchOffset, this._i);
        if (tZone != null) {
            this.utcOffset(tZone);
        } else {
            this.utcOffset(0, true);
        }
    }
    return this;
}

function hasAlignedHourOffset(input) {
    if (!this.isValid()) {
        return false;
    }
    input = input ? createLocal(input).utcOffset() : 0;

    return (this.utcOffset() - input) % 60 === 0;
}

function isDaylightSavingTime() {
    return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset();
}

function isDaylightSavingTimeShifted() {
    if (!isUndefined(this._isDSTShifted)) {
        return this._isDSTShifted;
    }

    var c = {};

    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
        var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
        this._isDSTShifted = this.isValid() && compareArrays(c._a, other.toArray()) > 0;
    } else {
        this._isDSTShifted = false;
    }

    return this._isDSTShifted;
}

function isLocal() {
    return this.isValid() ? !this._isUTC : false;
}

function isUtcOffset() {
    return this.isValid() ? this._isUTC : false;
}

function isUtc() {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
}

// ASP.NET json date format regex
var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

function createDuration(input, key) {
    var duration = input,

    // matching against regexp is expensive, do it on demand
    match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
        duration = {
            ms: input._milliseconds,
            d: input._days,
            M: input._months
        };
    } else if (isNumber(input)) {
        duration = {};
        if (key) {
            duration[key] = input;
        } else {
            duration.milliseconds = input;
        }
    } else if (!!(match = aspNetRegex.exec(input))) {
        sign = match[1] === '-' ? -1 : 1;
        duration = {
            y: 0,
            d: toInt(match[DATE]) * sign,
            h: toInt(match[HOUR]) * sign,
            m: toInt(match[MINUTE]) * sign,
            s: toInt(match[SECOND]) * sign,
            ms: toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
        };
    } else if (!!(match = isoRegex.exec(input))) {
        sign = match[1] === '-' ? -1 : 1;
        duration = {
            y: parseIso(match[2], sign),
            M: parseIso(match[3], sign),
            w: parseIso(match[4], sign),
            d: parseIso(match[5], sign),
            h: parseIso(match[6], sign),
            m: parseIso(match[7], sign),
            s: parseIso(match[8], sign)
        };
    } else if (duration == null) {
        // checks for null or undefined
        duration = {};
    } else if ((typeof duration === 'undefined' ? 'undefined' : _typeof(duration)) === 'object' && ('from' in duration || 'to' in duration)) {
        diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

        duration = {};
        duration.ms = diffRes.milliseconds;
        duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
        ret._locale = input._locale;
    }

    return ret;
}

createDuration.fn = Duration.prototype;

function parseIso(inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.'));
    // apply sign while we're at it
    return (isNaN(res) ? 0 : res) * sign;
}

function positiveMomentsDifference(base, other) {
    var res = { milliseconds: 0, months: 0 };

    res.months = other.month() - base.month() + (other.year() - base.year()) * 12;
    if (base.clone().add(res.months, 'M').isAfter(other)) {
        --res.months;
    }

    res.milliseconds = +other - +base.clone().add(res.months, 'M');

    return res;
}

function momentsDifference(base, other) {
    var res;
    if (!(base.isValid() && other.isValid())) {
        return { milliseconds: 0, months: 0 };
    }

    other = cloneWithOffset(other, base);
    if (base.isBefore(other)) {
        res = positiveMomentsDifference(base, other);
    } else {
        res = positiveMomentsDifference(other, base);
        res.milliseconds = -res.milliseconds;
        res.months = -res.months;
    }

    return res;
}

// TODO: remove 'name' arg after deprecation is removed
function createAdder(direction, name) {
    return function (val, period) {
        var dur, tmp;
        //invert the arguments, but complain about it
        if (period !== null && !isNaN(+period)) {
            deprecateSimple(name, 'moment().' + name + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' + 'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
            tmp = val;val = period;period = tmp;
        }

        val = typeof val === 'string' ? +val : val;
        dur = createDuration(val, period);
        addSubtract(this, dur, direction);
        return this;
    };
}

function addSubtract(mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
        // No op
        return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (milliseconds) {
        mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }
    if (days) {
        set$2(mom, 'Date', get$1(mom, 'Date') + days * isAdding);
    }
    if (months) {
        setMonth(mom, get$1(mom, 'Month') + months * isAdding);
    }
    if (updateOffset) {
        hooks.updateOffset(mom, days || months);
    }
}

var add = createAdder(1, 'add');
var subtract = createAdder(-1, 'subtract');

function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' : diff < -1 ? 'lastWeek' : diff < 0 ? 'lastDay' : diff < 1 ? 'sameDay' : diff < 2 ? 'nextDay' : diff < 7 ? 'nextWeek' : 'sameElse';
}

function calendar$1(time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';

    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
}

function clone() {
    return new Moment(this);
}

function isAfter(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() > localInput.valueOf();
    } else {
        return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
}

function isBefore(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() < localInput.valueOf();
    } else {
        return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
}

function isBetween(from, to, units, inclusivity) {
    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) && (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
}

function isSame(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(units || 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() === localInput.valueOf();
    } else {
        inputMs = localInput.valueOf();
        return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
}

function isSameOrAfter(input, units) {
    return this.isSame(input, units) || this.isAfter(input, units);
}

function isSameOrBefore(input, units) {
    return this.isSame(input, units) || this.isBefore(input, units);
}

function diff(input, units, asFloat) {
    var that, zoneDelta, delta, output;

    if (!this.isValid()) {
        return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
        return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

    units = normalizeUnits(units);

    if (units === 'year' || units === 'month' || units === 'quarter') {
        output = monthDiff(this, that);
        if (units === 'quarter') {
            output = output / 3;
        } else if (units === 'year') {
            output = output / 12;
        }
    } else {
        delta = this - that;
        output = units === 'second' ? delta / 1e3 : // 1000
        units === 'minute' ? delta / 6e4 : // 1000 * 60
        units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
        units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
        units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
        delta;
    }
    return asFloat ? output : absFloor(output);
}

function monthDiff(a, b) {
    // difference in months
    var wholeMonthDiff = (b.year() - a.year()) * 12 + (b.month() - a.month()),

    // b is in (anchor - 1 month, anchor + 1 month)
    anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2,
        adjust;

    if (b - anchor < 0) {
        anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor - anchor2);
    } else {
        anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor2 - anchor);
    }

    //check for negative zero, return zero if negative zero
    return -(wholeMonthDiff + adjust) || 0;
}

hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

function toString() {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
}

function toISOString() {
    var m = this.clone().utc();
    if (0 < m.year() && m.year() <= 9999) {
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            return this.toDate().toISOString();
        } else {
            return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
    } else {
        return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }
}

/**
 * Return a human readable representation of a moment that can
 * also be evaluated to get a new moment which is the same
 *
 * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
 */
function inspect() {
    if (!this.isValid()) {
        return 'moment.invalid(/* ' + this._i + ' */)';
    }
    var func = 'moment';
    var zone = '';
    if (!this.isLocal()) {
        func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
        zone = 'Z';
    }
    var prefix = '[' + func + '("]';
    var year = 0 < this.year() && this.year() <= 9999 ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';

    return this.format(prefix + year + datetime + suffix);
}

function format(inputString) {
    if (!inputString) {
        inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }
    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
}

function from(time, withoutSuffix) {
    if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
        return createDuration({ to: this, from: time }).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function fromNow(withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}

function to(time, withoutSuffix) {
    if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
        return createDuration({ from: this, to: time }).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function toNow(withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
function locale(key) {
    var newLocaleData;

    if (key === undefined) {
        return this._locale._abbr;
    } else {
        newLocaleData = getLocale(key);
        if (newLocaleData != null) {
            this._locale = newLocaleData;
        }
        return this;
    }
}

var lang = deprecate('moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.', function (key) {
    if (key === undefined) {
        return this.localeData();
    } else {
        return this.locale(key);
    }
});

function localeData() {
    return this._locale;
}

function startOf(units) {
    units = normalizeUnits(units);
    // the following switch intentionally omits break keywords
    // to utilize falling through the cases.
    switch (units) {
        case 'year':
            this.month(0);
        /* falls through */
        case 'quarter':
        case 'month':
            this.date(1);
        /* falls through */
        case 'week':
        case 'isoWeek':
        case 'day':
        case 'date':
            this.hours(0);
        /* falls through */
        case 'hour':
            this.minutes(0);
        /* falls through */
        case 'minute':
            this.seconds(0);
        /* falls through */
        case 'second':
            this.milliseconds(0);
    }

    // weeks are a special case
    if (units === 'week') {
        this.weekday(0);
    }
    if (units === 'isoWeek') {
        this.isoWeekday(1);
    }

    // quarters are also special
    if (units === 'quarter') {
        this.month(Math.floor(this.month() / 3) * 3);
    }

    return this;
}

function endOf(units) {
    units = normalizeUnits(units);
    if (units === undefined || units === 'millisecond') {
        return this;
    }

    // 'date' is an alias for 'day', so it should be considered as such.
    if (units === 'date') {
        units = 'day';
    }

    return this.startOf(units).add(1, units === 'isoWeek' ? 'week' : units).subtract(1, 'ms');
}

function valueOf() {
    return this._d.valueOf() - (this._offset || 0) * 60000;
}

function unix() {
    return Math.floor(this.valueOf() / 1000);
}

function toDate() {
    return new Date(this.valueOf());
}

function toArray$1() {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
}

function toObject() {
    var m = this;
    return {
        years: m.year(),
        months: m.month(),
        date: m.date(),
        hours: m.hours(),
        minutes: m.minutes(),
        seconds: m.seconds(),
        milliseconds: m.milliseconds()
    };
}

function toJSON() {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
}

function isValid$1() {
    return isValid(this);
}

function parsingFlags() {
    return extend({}, getParsingFlags(this));
}

function invalidAt() {
    return getParsingFlags(this).overflow;
}

function creationData() {
    return {
        input: this._i,
        format: this._f,
        locale: this._locale,
        isUTC: this._isUTC,
        strict: this._strict
    };
}

// FORMATTING

addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
});

addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
});

function addWeekYearFormatToken(token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
}

addWeekYearFormatToken('gggg', 'weekYear');
addWeekYearFormatToken('ggggg', 'weekYear');
addWeekYearFormatToken('GGGG', 'isoWeekYear');
addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

addUnitAlias('weekYear', 'gg');
addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

addUnitPriority('weekYear', 1);
addUnitPriority('isoWeekYear', 1);

// PARSING

addRegexToken('G', matchSigned);
addRegexToken('g', matchSigned);
addRegexToken('GG', match1to2, match2);
addRegexToken('gg', match1to2, match2);
addRegexToken('GGGG', match1to4, match4);
addRegexToken('gggg', match1to4, match4);
addRegexToken('GGGGG', match1to6, match6);
addRegexToken('ggggg', match1to6, match6);

addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
});

addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
});

// MOMENTS

function getSetWeekYear(input) {
    return getSetWeekYearHelper.call(this, input, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy);
}

function getSetISOWeekYear(input) {
    return getSetWeekYearHelper.call(this, input, this.isoWeek(), this.isoWeekday(), 1, 4);
}

function getISOWeeksInYear() {
    return weeksInYear(this.year(), 1, 4);
}

function getWeeksInYear() {
    var weekInfo = this.localeData()._week;
    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}

function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;
    if (input == null) {
        return weekOfYear(this, dow, doy).year;
    } else {
        weeksTarget = weeksInYear(input, dow, doy);
        if (week > weeksTarget) {
            week = weeksTarget;
        }
        return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
}

function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
}

// FORMATTING

addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

addUnitAlias('quarter', 'Q');

// PRIORITY

addUnitPriority('quarter', 7);

// PARSING

addRegexToken('Q', match1);
addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
});

// MOMENTS

function getSetQuarter(input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PRIOROITY
addUnitPriority('date', 9);

// PARSING

addRegexToken('D', match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    return isStrict ? locale._ordinalParse : locale._ordinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
addUnitPriority('dayOfYear', 4);

// PARSING

addRegexToken('DDD', match1to3);
addRegexToken('DDDD', match3);
addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
});

// HELPERS

// MOMENTS

function getSetDayOfYear(input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add(input - dayOfYear, 'd');
}

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PRIORITY

addUnitPriority('minute', 14);

// PARSING

addRegexToken('m', match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PRIORITY

addUnitPriority('second', 15);

// PARSING

addRegexToken('s', match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
});

addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
});

addFormatToken(0, ['SSS', 3], 0, 'millisecond');
addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
});
addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
});
addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
});
addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
});
addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
});
addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
});

// ALIASES

addUnitAlias('millisecond', 'ms');

// PRIORITY

addUnitPriority('millisecond', 16);

// PARSING

addRegexToken('S', match1to3, match1);
addRegexToken('SS', match1to3, match2);
addRegexToken('SSS', match1to3, match3);

var token;
for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
}

function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
}

for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
}
// MOMENTS

var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

addFormatToken('z', 0, 0, 'zoneAbbr');
addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

function getZoneAbbr() {
    return this._isUTC ? 'UTC' : '';
}

function getZoneName() {
    return this._isUTC ? 'Coordinated Universal Time' : '';
}

var proto = Moment.prototype;

proto.add = add;
proto.calendar = calendar$1;
proto.clone = clone;
proto.diff = diff;
proto.endOf = endOf;
proto.format = format;
proto.from = from;
proto.fromNow = fromNow;
proto.to = to;
proto.toNow = toNow;
proto.get = stringGet;
proto.invalidAt = invalidAt;
proto.isAfter = isAfter;
proto.isBefore = isBefore;
proto.isBetween = isBetween;
proto.isSame = isSame;
proto.isSameOrAfter = isSameOrAfter;
proto.isSameOrBefore = isSameOrBefore;
proto.isValid = isValid$1;
proto.lang = lang;
proto.locale = locale;
proto.localeData = localeData;
proto.max = prototypeMax;
proto.min = prototypeMin;
proto.parsingFlags = parsingFlags;
proto.set = stringSet;
proto.startOf = startOf;
proto.subtract = subtract;
proto.toArray = toArray$1;
proto.toObject = toObject;
proto.toDate = toDate;
proto.toISOString = toISOString;
proto.inspect = inspect;
proto.toJSON = toJSON;
proto.toString = toString;
proto.unix = unix;
proto.valueOf = valueOf;
proto.creationData = creationData;

// Year
proto.year = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
proto.weekYear = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
proto.quarter = proto.quarters = getSetQuarter;

// Month
proto.month = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
proto.week = proto.weeks = getSetWeek;
proto.isoWeek = proto.isoWeeks = getSetISOWeek;
proto.weeksInYear = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
proto.date = getSetDayOfMonth;
proto.day = proto.days = getSetDayOfWeek;
proto.weekday = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear = getSetDayOfYear;

// Hour
proto.hour = proto.hours = getSetHour;

// Minute
proto.minute = proto.minutes = getSetMinute;

// Second
proto.second = proto.seconds = getSetSecond;

// Millisecond
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
proto.utcOffset = getSetOffset;
proto.utc = setOffsetToUTC;
proto.local = setOffsetToLocal;
proto.parseZone = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST = isDaylightSavingTime;
proto.isLocal = isLocal;
proto.isUtcOffset = isUtcOffset;
proto.isUtc = isUtc;
proto.isUTC = isUtc;

// Timezone
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
proto.dates = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

function createUnix(input) {
    return createLocal(input * 1000);
}

function createInZone() {
    return createLocal.apply(null, arguments).parseZone();
}

function preParsePostFormat(string) {
    return string;
}

var proto$1 = Locale.prototype;

proto$1.calendar = calendar;
proto$1.longDateFormat = longDateFormat;
proto$1.invalidDate = invalidDate;
proto$1.ordinal = ordinal;
proto$1.preparse = preParsePostFormat;
proto$1.postformat = preParsePostFormat;
proto$1.relativeTime = relativeTime;
proto$1.pastFuture = pastFuture;
proto$1.set = set$1;

// Month
proto$1.months = localeMonths;
proto$1.monthsShort = localeMonthsShort;
proto$1.monthsParse = localeMonthsParse;
proto$1.monthsRegex = monthsRegex;
proto$1.monthsShortRegex = monthsShortRegex;

// Week
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
proto$1.weekdays = localeWeekdays;
proto$1.weekdaysMin = localeWeekdaysMin;
proto$1.weekdaysShort = localeWeekdaysShort;
proto$1.weekdaysParse = localeWeekdaysParse;

proto$1.weekdaysRegex = weekdaysRegex;
proto$1.weekdaysShortRegex = weekdaysShortRegex;
proto$1.weekdaysMinRegex = weekdaysMinRegex;

// Hours
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;

function get$2(format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
}

function listMonthsImpl(format, index, field) {
    if (isNumber(format)) {
        index = format;
        format = undefined;
    }

    format = format || '';

    if (index != null) {
        return get$2(format, index, field, 'month');
    }

    var i;
    var out = [];
    for (i = 0; i < 12; i++) {
        out[i] = get$2(format, i, field, 'month');
    }
    return out;
}

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
function listWeekdaysImpl(localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    } else {
        format = localeSorted;
        index = format;
        localeSorted = false;

        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
        return get$2(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];
    for (i = 0; i < 7; i++) {
        out[i] = get$2(format, (i + shift) % 7, field, 'day');
    }
    return out;
}

function listMonths(format, index) {
    return listMonthsImpl(format, index, 'months');
}

function listMonthsShort(format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
}

function listWeekdays(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
}

function listWeekdaysShort(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
}

function listWeekdaysMin(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
}

getSetGlobalLocale('en', {
    ordinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal: function ordinal(number) {
        var b = number % 10,
            output = toInt(number % 100 / 10) === 1 ? 'th' : b === 1 ? 'st' : b === 2 ? 'nd' : b === 3 ? 'rd' : 'th';
        return number + output;
    }
});

// Side effect imports
hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

var mathAbs = Math.abs;

function abs() {
    var data = this._data;

    this._milliseconds = mathAbs(this._milliseconds);
    this._days = mathAbs(this._days);
    this._months = mathAbs(this._months);

    data.milliseconds = mathAbs(data.milliseconds);
    data.seconds = mathAbs(data.seconds);
    data.minutes = mathAbs(data.minutes);
    data.hours = mathAbs(data.hours);
    data.months = mathAbs(data.months);
    data.years = mathAbs(data.years);

    return this;
}

function addSubtract$1(duration, input, value, direction) {
    var other = createDuration(input, value);

    duration._milliseconds += direction * other._milliseconds;
    duration._days += direction * other._days;
    duration._months += direction * other._months;

    return duration._bubble();
}

// supports only 2.0-style add(1, 's') or add(duration)
function add$1(input, value) {
    return addSubtract$1(this, input, value, 1);
}

// supports only 2.0-style subtract(1, 's') or subtract(duration)
function subtract$1(input, value) {
    return addSubtract$1(this, input, value, -1);
}

function absCeil(number) {
    if (number < 0) {
        return Math.floor(number);
    } else {
        return Math.ceil(number);
    }
}

function bubble() {
    var milliseconds = this._milliseconds;
    var days = this._days;
    var months = this._months;
    var data = this._data;
    var seconds, minutes, hours, years, monthsFromDays;

    // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166
    if (!(milliseconds >= 0 && days >= 0 && months >= 0 || milliseconds <= 0 && days <= 0 && months <= 0)) {
        milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

    // The following code bubbles up values, see the tests for
    // examples of what that means.
    data.milliseconds = milliseconds % 1000;

    seconds = absFloor(milliseconds / 1000);
    data.seconds = seconds % 60;

    minutes = absFloor(seconds / 60);
    data.minutes = minutes % 60;

    hours = absFloor(minutes / 60);
    data.hours = hours % 24;

    days += absFloor(hours / 24);

    // convert days to months
    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays));

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    data.days = days;
    data.months = months;
    data.years = years;

    return this;
}

function daysToMonths(days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
}

function monthsToDays(months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
}

function as(units) {
    var days;
    var months;
    var milliseconds = this._milliseconds;

    units = normalizeUnits(units);

    if (units === 'month' || units === 'year') {
        days = this._days + milliseconds / 864e5;
        months = this._months + daysToMonths(days);
        return units === 'month' ? months : months / 12;
    } else {
        // handle milliseconds separately because of floating point math errors (issue #1867)
        days = this._days + Math.round(monthsToDays(this._months));
        switch (units) {
            case 'week':
                return days / 7 + milliseconds / 6048e5;
            case 'day':
                return days + milliseconds / 864e5;
            case 'hour':
                return days * 24 + milliseconds / 36e5;
            case 'minute':
                return days * 1440 + milliseconds / 6e4;
            case 'second':
                return days * 86400 + milliseconds / 1000;
            // Math.floor prevents floating point math errors here
            case 'millisecond':
                return Math.floor(days * 864e5) + milliseconds;
            default:
                throw new Error('Unknown unit ' + units);
        }
    }
}

// TODO: Use this.as('ms')?
function valueOf$1() {
    return this._milliseconds + this._days * 864e5 + this._months % 12 * 2592e6 + toInt(this._months / 12) * 31536e6;
}

function makeAs(alias) {
    return function () {
        return this.as(alias);
    };
}

var asMilliseconds = makeAs('ms');
var asSeconds = makeAs('s');
var asMinutes = makeAs('m');
var asHours = makeAs('h');
var asDays = makeAs('d');
var asWeeks = makeAs('w');
var asMonths = makeAs('M');
var asYears = makeAs('y');

function get$3(units) {
    units = normalizeUnits(units);
    return this[units + 's']();
}

function makeGetter(name) {
    return function () {
        return this._data[name];
    };
}

var milliseconds = makeGetter('milliseconds');
var seconds = makeGetter('seconds');
var minutes = makeGetter('minutes');
var hours = makeGetter('hours');
var days = makeGetter('days');
var months = makeGetter('months');
var years = makeGetter('years');

function weeks() {
    return absFloor(this.days() / 7);
}

var round = Math.round;
var thresholds = {
    s: 45, // seconds to minute
    m: 45, // minutes to hour
    h: 22, // hours to day
    d: 26, // days to month
    M: 11 // months to year
};

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}

function relativeTime$1(posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds = round(duration.as('s'));
    var minutes = round(duration.as('m'));
    var hours = round(duration.as('h'));
    var days = round(duration.as('d'));
    var months = round(duration.as('M'));
    var years = round(duration.as('y'));

    var a = seconds < thresholds.s && ['s', seconds] || minutes <= 1 && ['m'] || minutes < thresholds.m && ['mm', minutes] || hours <= 1 && ['h'] || hours < thresholds.h && ['hh', hours] || days <= 1 && ['d'] || days < thresholds.d && ['dd', days] || months <= 1 && ['M'] || months < thresholds.M && ['MM', months] || years <= 1 && ['y'] || ['yy', years];

    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
}

// This function allows you to set the rounding function for relative time strings
function getSetRelativeTimeRounding(roundingFunction) {
    if (roundingFunction === undefined) {
        return round;
    }
    if (typeof roundingFunction === 'function') {
        round = roundingFunction;
        return true;
    }
    return false;
}

// This function allows you to set a threshold for relative time strings
function getSetRelativeTimeThreshold(threshold, limit) {
    if (thresholds[threshold] === undefined) {
        return false;
    }
    if (limit === undefined) {
        return thresholds[threshold];
    }
    thresholds[threshold] = limit;
    return true;
}

function humanize(withSuffix) {
    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
        output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
}

var abs$1 = Math.abs;

function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    var seconds = abs$1(this._milliseconds) / 1000;
    var days = abs$1(this._days);
    var months = abs$1(this._months);
    var minutes, hours, years;

    // 3600 seconds -> 60 minutes -> 1 hour
    minutes = absFloor(seconds / 60);
    hours = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60;

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds;
    var total = this.asSeconds();

    if (!total) {
        // this is the same as C#'s (Noda) and python (isodate)...
        // but not other JS (goog.date)
        return 'P0D';
    }

    return (total < 0 ? '-' : '') + 'P' + (Y ? Y + 'Y' : '') + (M ? M + 'M' : '') + (D ? D + 'D' : '') + (h || m || s ? 'T' : '') + (h ? h + 'H' : '') + (m ? m + 'M' : '') + (s ? s + 'S' : '');
}

var proto$2 = Duration.prototype;

proto$2.abs = abs;
proto$2.add = add$1;
proto$2.subtract = subtract$1;
proto$2.as = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds = asSeconds;
proto$2.asMinutes = asMinutes;
proto$2.asHours = asHours;
proto$2.asDays = asDays;
proto$2.asWeeks = asWeeks;
proto$2.asMonths = asMonths;
proto$2.asYears = asYears;
proto$2.valueOf = valueOf$1;
proto$2._bubble = bubble;
proto$2.get = get$3;
proto$2.milliseconds = milliseconds;
proto$2.seconds = seconds;
proto$2.minutes = minutes;
proto$2.hours = hours;
proto$2.days = days;
proto$2.weeks = weeks;
proto$2.months = months;
proto$2.years = years;
proto$2.humanize = humanize;
proto$2.toISOString = toISOString$1;
proto$2.toString = toISOString$1;
proto$2.toJSON = toISOString$1;
proto$2.locale = locale;
proto$2.localeData = localeData;

// Deprecations
proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
proto$2.lang = lang;

// Side effect imports

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});

// Side effect imports

//! moment.js
//! version : 2.17.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

hooks.version = '2.17.1';

setHookCallback(createLocal);

hooks.fn = proto;
hooks.min = min;
hooks.max = max;
hooks.now = now;
hooks.utc = createUTC;
hooks.unix = createUnix;
hooks.months = listMonths;
hooks.isDate = isDate;
hooks.locale = getSetGlobalLocale;
hooks.invalid = createInvalid;
hooks.duration = createDuration;
hooks.isMoment = isMoment;
hooks.weekdays = listWeekdays;
hooks.parseZone = createInZone;
hooks.localeData = getLocale;
hooks.isDuration = isDuration;
hooks.monthsShort = listMonthsShort;
hooks.weekdaysMin = listWeekdaysMin;
hooks.defineLocale = defineLocale;
hooks.updateLocale = updateLocale;
hooks.locales = listLocales;
hooks.weekdaysShort = listWeekdaysShort;
hooks.normalizeUnits = normalizeUnits;
hooks.relativeTimeRounding = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat = getCalendarFormat;
hooks.prototype = proto;

var Layout$3 = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {
            var zone = zones[zoneIndex];

            var base = new Date(zone.start + '-01');
            var offset = base.getDay();

            //  moment().diff(date_time, 'minutes')
            d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
                var index = d.boundary.charCodeAt(0) - 65;
                return index == zoneIndex;
            }).each(function (d, i) {
                if (zone.unit == "monthly" && d.hasOwnProperty("startDate")) {
                    var days = hooks(base).add(Number(offset) + Number(d.startDate), 'days').diff(base, 'days');

                    d.x = 100 + days * 22;
                    d3.select(this).select("rect").attr("width", function (d) {
                        return d.hasOwnProperty("duration") ? d.duration * 22 : 100;
                    });
                }
                if (zone.unit == "quarterly" && d.hasOwnProperty("startDate")) {
                    var months = hooks(base).add(Number(d.startDate), 'months').diff(base, 'months');

                    d.x = 100 + months * 30;

                    d3.select(this).select("rect").attr("width", function (d) {
                        return d.hasOwnProperty("duration") ? d.duration * 30 : 100;
                    });
                    d3.select(this).select("circle").attr("transform", function (d) {
                        return "translate(" + (d.hasOwnProperty("duration") ? d.duration * 30 + 6 : 100) + "," + 3 + ")";
                    });
                }
            });
        }
    }, {
        key: 'build',
        value: function build(d3visual, zone, root) {

            function addMonths(dateObj, num) {

                var currentMonth = dateObj.getMonth();
                dateObj.setMonth(dateObj.getMonth() + num);

                if (dateObj.getMonth() != (currentMonth + num) % 12) {
                    dateObj.setDate(0);
                }
                return dateObj;
            }

            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            //      let vb = d3visual.attr("viewBox").split(' ');
            //
            //      let viewWidth = vb[2];
            //      let viewHeight = vb[3];
            //
            //      let pageHeight = d3visual.node().parentNode.clientHeight;
            //      let pageWidth = d3visual.node().parentNode.clientWidth;
            //      // THis needs to be a calculation based on a ratio
            //
            //      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
            //
            //      for ( let v = 0; v < 4; v++) {
            //          if (typeof zone.rectangle[v] == "string") {
            //            //console.log("Evaluating: " + zone.rectangle[v]);
            //            //console.log(" ... to : " + eval(zone.rectangle[v]));
            //            zone.rectangle[v] = eval(zone.rectangle[v]);
            //          }
            //      }
            //
            //      if (pageHeightSet) {
            //          zone.rectangle[3] = ((zone.rectangle[2]+zone.rectangle[0]) * pageHeight/pageWidth) - 20;
            //      }

            this.calculateRectangle(d3visual, zone);

            var rect = obj.append("rect").attr("class", "background").attr("x", zone.calculatedRectangle[0]).attr("y", zone.calculatedRectangle[1]).attr("rx", 8).attr("ry", 8).attr("width", zone.calculatedRectangle[2]).attr("height", zone.calculatedRectangle[3]); //fff899


            // 26 weeks
            // 90*4 = 360

            // zone.unit == quarterly : show the month lines
            // zone.unit == monthly : show the weekly lines (with weekend)

            var milestoneGap = 0;
            var milestones = [];
            var bars = [];
            var level1 = [];
            if (zone.unit == "monthly") {
                var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
                var date = new Date(zone.start + '-01');

                var segmentWidth = 22;
                milestoneGap = 210;

                var offset = date.getDay();
                if (offset >= 0 && offset <= 6) {
                    date.setDate(date.getDate() - offset);
                    for (var i = 0; i <= offset; i++) {
                        milestones.push({ "label": "" + date.getDate() + "", "position": 100 + i * segmentWidth });
                        bars.push({ "width": segmentWidth, "class": days[date.getDay()], "position": 100 + bars.length * segmentWidth - segmentWidth / 2 });
                        date.setDate(date.getDate() + 1);
                    }
                }
                for (var _i = offset + 1; _i < 40; _i++) {
                    milestones.push({ "label": "" + date.getDate() + "", "position": 100 + _i * segmentWidth });
                    bars.push({ "width": segmentWidth, "class": days[date.getDay()], "position": 100 + bars.length * segmentWidth - segmentWidth / 2 });
                    date.setDate(date.getDate() + 1);
                    if (date.getDate() == 1) {
                        break;
                    }
                }
                if (offset > 0) {
                    var _title = hooks(date).subtract(1, 'month').format('MMMM YYYY');
                    level1.push({ "width": segmentWidth * (offset + 1), "label": _title, "class": "level1", "position": 100 - segmentWidth / 2 });
                }
                var title = hooks(date).format('MMMM YYYY');

                level1.push({ "width": segmentWidth * (bars.length - offset - 1), "label": title, "class": "level1", "position": 100 + (offset + 1) * segmentWidth - segmentWidth / 2 });

                zone.bars = bars;
            } else if (zone.unit == "quarterly") {

                var quarter = ["q1", "q2", "q3", "q4"];
                var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
                date = new Date(zone.start + '-20');

                var segments = 3 * 9; // 10 quarters

                milestoneGap = segments * 30;

                for (var _i2 = 0; _i2 < segments; _i2++) {
                    milestones.push({ "label": "" + months[date.getMonth()] + "", "position": 120 + (_i2 * 30 + 15) });
                    if (date.getMonth() % 3 == 0) {
                        bars.push({ "width": 90, "class": quarter[Math.floor(date.getMonth() / 3)], "position": 120 + bars.length * 90 });
                        if (date.getMonth() % 12 == 0) {
                            if (segments - _i2 < 12) {
                                level1.push({ "width": 30 * (segments - _i2) - 5, "label": date.getFullYear(), "class": "level1", "position": 120 + (bars.length - 1) * 90 });
                            } else {
                                level1.push({ "width": 30 * 3 * 4 - 5, "label": date.getFullYear(), "class": "level1", "position": 120 + (bars.length - 1) * 90 });
                            }
                        }
                    }
                    addMonths(date, 1);
                }

                zone.bars = bars;
            }

            obj.selectAll("text").data(milestones).enter().append("text").attr("text-anchor", "middle").attr("transform", function (d, i) {
                return "translate(" + d.position + ",40)";
            }).text(function (d) {
                return d.label;
            });

            var count = 0;
            obj.selectAll("line").data(zone.milestones).enter().append("line").attr("x1", function (d, i) {
                return 120 + milestoneGap * i;
            }).attr("x2", function (d, i) {
                return 120 + milestoneGap * i;
            }).attr("y1", zone.calculatedRectangle[1]).attr("y2", zone.calculatedRectangle[3]).attr("stroke", "gray").attr("stroke-width", "1");

            var banner = obj.append("g").selectAll("rect").data(level1).enter();

            banner.append("rect").attr("rx", 8).attr("width", function (d) {
                return d.width;
            }).attr("height", 18).attr("class", function (d) {
                return d.class;
            }).attr("y", 12).attr("x", function (d, i) {
                return d.position;
            });

            banner.append("text").attr("text-anchor", "middle").classed("banner", true).attr("transform", function (d, i) {
                return "translate(" + (d.width / 2 + d.position) + ",25)";
            }).text(function (d) {
                return d.label;
            });

            obj.append("g").selectAll("rect").data(bars).enter().append("rect").attr("width", function (d) {
                return d.width;
            }).attr("height", zone.calculatedRectangle[3] - 40).attr("class", function (d) {
                return d.class;
            }).attr("y", zone.calculatedRectangle[1] + 40).attr("x", function (d, i) {
                return d.position;
            });
        }
    }]);
    return Layout;
}(Layout$1);

__$styleInject("._carousel{fill:#ccc}.toc{fill:#000;font-size:.7em;font-family:Open Sans}.selected{fill:blue!important;font-weight:700!important}text.carousel_down,text.carousel_up{font-size:4em;fill:#999}", undefined);

var Layout$4 = function (_Default) {
    inherits(Layout, _Default);

    function Layout(domainType) {
        classCallCheck(this, Layout);

        var _this = possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, domainType));

        _this.domainType = domainType;
        return _this;
    }

    createClass(Layout, [{
        key: 'build',
        value: function build(d3visual, zone, root) {
            var self = this;
            var obj = root.append("g");

            obj.classed("_zone", true);
            obj.classed("_" + this.domainType, true);

            var vb = d3visual.attr("viewBox").split(' ');

            var viewWidth = vb[2];
            var viewHeight = vb[3];

            for (var v = 0; v < 4; v++) {
                if (typeof zone.rectangle[v] == "string") {
                    //console.log("Evaluating: " + zone.rectangle[v]);
                    //console.log(" ... to : " + eval(zone.rectangle[v]));
                    zone.rectangle[v] = eval(zone.rectangle[v]);
                }
            }

            var rect = obj.append("rect").style("filter", "url(#drop-shadow)").attr("x", zone.rectangle[0]).attr("y", zone.rectangle[1]).attr("width", zone.rectangle[2]).attr("height", zone.rectangle[3]); //fff899
        }
    }, {
        key: 'enrichData',
        value: function enrichData(nodes) {
            //
            //      let segs = [];
            //      for ( let x = 0 ; x < graph.nodes.length; x++) {
            //          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //          if (seg != "" && segs.indexOf(seg) < 0) {
            //              segs.push(seg);
            //          }
            //      }

            this.recurse(nodes, 0);

            //      // calculate the segment layout
            //      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
            //      for ( let p = 0; p < segs.length; p++) {
            //          let tot = 0;
            //          let x,num;
            //          for ( x = 0 ; x < graph.nodes.length; x++) {
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            //              if (seg == segs[p]) {
            //                  tot++;
            //              }
            //              if (graph.nodes[x].children) {
            //                  let ls = graph.nodes[x].children;
            //                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            //                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            //                      if (seg == segs[p]) {
            //                        tot++;
            //                      }
            //
            //                      if (ls[xL1].children) {
            //                          let lsL2 = ls[xL1].children;
            //                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            //                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            //                              if (seg == segs[p]) {
            //                                tot++;
            //                              }
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
            ////              this.recurse(graph.nodes, 1);
            //              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
            ////              if (seg == segs[p]) {
            ////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
            ////                  num++;
            ////              }
            ////              this.recurse(graph.nodes[x], 1);
            ////              if (graph.nodes[x].children) {
            ////                  let ls = graph.nodes[x].children;
            ////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
            ////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
            ////                      if (seg == segs[p]) {
            ////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
            ////                          numL1++;
            ////                      }
            ////                      this.recurse(ls[xL1], 2);
            //
            ////                      if (ls[xL1].children) {
            ////                          let lsL2 = ls[xL1].children;
            ////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
            ////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
            ////                              if (seg == segs[p]) {
            ////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
            ////                                  numL2++;
            ////                              }
            ////                          }
            ////                      }
            //
            //           //       }
            //            //  }
            //          }
            //      }
        }
    }, {
        key: 'recurse',
        value: function recurse(nodes, level) {
            var tot = nodes.length;
            for (var num = 0, x = 0; x < nodes.length; x++) {
                var nd = nodes[x];
                if (nd.type == "panel" || level > 0) {
                    nd.layout = { "seq": num, "total": tot, "seg": "SEG" + level, "segnum": level, "level": level };
                    num++;
                    if (nd.children) {
                        this.recurse(nd.children, level + 1);
                    }
                }
            }
        }
    }, {
        key: 'configEvents',
        value: function configEvents(d3visual, newNodesTotal, zones, viewer) {
            var self = this;

            var newNodes = newNodesTotal.filter(function (d) {
                return d.type == 'icon';
            });

            var force = viewer.force;

            // Use x and y if it was provided, otherwise stick it in the center of the zone
            newNodesTotal.attr("cx", function (d) {
                var dim = self.getDim(d, zones);return d.x = d.x ? d.x : dim.x1 + (dim.x2 - dim.x1) / 2;
            }).attr("cy", function (d) {
                var dim = self.getDim(d, zones);return d.y = d.y ? d.y : dim.y1 + (dim.y2 - dim.y1) / 2;
            });

            //nodeSet, index
            newNodes.on('mouseenter', function (e) {
                //            $(this).addClass("node-hover");
                //            console.log("NODE: " +JSON.stringify(nodeSet));
            }).on('mouseleave', function (e) {
                //$(this).removeClass("node-hover");
            }).on('click', function (e) {
                d3.event.stopPropagation();
                self.togglePanel(viewer, this.parentNode, e.name == "down"); /*self.toggleNavigation(viewer, e, this); */
            });

            function dragstarted(d) {
                if (!d3.event.active) force.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
                viewer.downX = Math.round(d.x + d.y);
            }

            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function dragended(d) {
                if (!d3.event.active) force.alphaTarget(0);
                d.fx = null;
                d.fy = null;
                setTimeout(function () {
                    viewer.trigger('change');
                }, 200);
            }

            newNodesTotal.call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

            newNodes.each(function (d, i) {
                if (i == 0) {
                    self.togglePanel(viewer, this.parentNode, true);
                }
            });
        }
    }, {
        key: 'getDim',
        value: function getDim(d, zones) {
            var index = d.boundary.charCodeAt(0) - 65;
            if (zones.length <= index) {
                //console.log("INVALID BOUNDARY: " + d.boundary);
            }
            var rect = zones[index].rectangle;
            var dim = { x1: rect[0], y1: rect[1], x2: rect[0] + rect[2], y2: rect[1] + rect[3] };
            //console.log("K: " + JSON.stringify(dim, null, 2));
            return dim;
        }
    }, {
        key: 'togglePanel',
        value: function togglePanel(viewer, root, down) {
            var panels = [];
            d3.select(root).selectAll("g._panel").each(function (d) {
                panels.push(d);
                d3.select(root).selectAll("g.node_" + d.name).style("display", "none");
            });

            if (viewer.chosenPanel >= 0) {
                var _data = panels[viewer.chosenPanel];
                d3.select(root).selectAll("g.node_" + _data.name).style("display", "none");
            } else {
                viewer.chosenPanel = -1;
            }

            if (down) {
                viewer.chosenPanel--;
                if (viewer.chosenPanel < 0) {
                    viewer.chosenPanel = 0; //(panels.length - 1);
                }
            } else {
                viewer.chosenPanel++;
                if (viewer.chosenPanel >= panels.length) {
                    viewer.chosenPanel = panels.length - 1;
                }
            }
            var data = panels[viewer.chosenPanel];
            d3.select(root).selectAll("g.node_" + data.name).style("display", "block");

            this.updateTableOfContents(viewer, root, panels);
        }
    }, {
        key: 'updateTableOfContents',
        value: function updateTableOfContents(viewer, root, panels) {
            d3.select(root).select("g._carousel").remove();

            d3.select(root).append("g").classed("_carousel", true).selectAll(".toc").data(panels).enter().append("text").classed("toc", true).style("fill", "black").classed("selected", function (x, i) {
                return i == viewer.chosenPanel ? true : false;
            }).attr("transform", function (x, i) {
                return "translate(" + 15 + "," + (25 + i * 14) + ")";
            }).text(function (x) {
                return x.label;
            });

            d3.select(root).selectAll(".toc").data(panels).exit().remove();
        }
    }, {
        key: 'toggleNavigation',
        value: function toggleNavigation(viewer, e, object) {
            d3.event.stopPropagation();

            if (viewer.downX == Math.round(e.x + e.y)) {
                viewer.navigation.toggle(e, d3.select(object));
            } else {
                viewer.navigation.close(e);
            }
        }
    }, {
        key: 'organize',
        value: function organize(d3visual, zoneIndex, zones) {}
    }]);
    return Layout;
}(Layout$1);

//export * from './ViewerCanvas';
//export * from './ShapeFactory';
//export * from './LayoutFactory';
function yell(name) {
    return 'HEY ' + name.toUpperCase() + '!!';
}

var DUDE = function DUDE() {
    classCallCheck(this, DUDE);
};

ShapeFactory.register(new Shape("comment"));

ShapeFactory.register(new RectangleShape("application"));

ShapeFactory.register(new Shape$1("icon"));
ShapeFactory.register(new Shape$2("image"));

ShapeFactory.register(new Shape$3("circle"));
ShapeFactory.register(new Shape$3("circle2"));
ShapeFactory.register(new Shape$3("circle3"));

ShapeFactory.register(new Shape$4("milestone"));

ShapeFactory.register(new Shape$5("button"));

ShapeFactory.register(new Shape$6("timeline"));

ShapeFactory.register(new Shape$7("timeline2"));

ShapeFactory.register(new Shape$8("panel"));

ShapeFactory.register(new Shape$9("box"));

ShapeFactory.register(new Shape$10("tab"));

LayoutFactory.register(new Layout("swimlane"));

LayoutFactory.register(new Layout$1("default"));

LayoutFactory.register(new Layout$2("tabs"));

LayoutFactory.register(new Layout$3("timelines"));

LayoutFactory.register(new Layout$4("carousel"));

exports.yell = yell;
exports['default'] = DUDE;
exports.ShapeFactory = ShapeFactory;
exports.LayoutFactory = LayoutFactory;
exports.ViewerCanvas = ViewerCanvas;
exports.version = version;

}((this.iksplor = this.iksplor || {}),d3));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWtzcGxvci5qcyIsInNvdXJjZXMiOlsiLi4vc3JjL1NoYXBlRmFjdG9yeS5qcyIsIi4uL3NyYy9OYXZpZ2F0aW9uSG9yaXpvbnRhbC5qcyIsIi4uL3NyYy9MYXlvdXRGYWN0b3J5LmpzIiwiLi4vc3JjL1ZpZXdlckNhbnZhcy5qcyIsInBhY2thZ2UuanMiLCIuLi9zcmMvdXRpbHMvVGV4dFV0aWxzLmpzIiwiLi4vc3JjL3NoYXBlcy9jb21tZW50L3NoYXBlLmpzIiwiLi4vc3JjL3NoYXBlcy9hcHBsaWNhdGlvbi9zaGFwZS5qcyIsIi4uL3NyYy9zaGFwZXMvaWNvbi9zaGFwZS5qcyIsIi4uL3NyYy9zaGFwZXMvaW1hZ2Uvc2hhcGUuanMiLCIuLi9zcmMvc2hhcGVzL2NpcmNsZS9zaGFwZS5qcyIsIi4uL3NyYy9zaGFwZXMvbWlsZXN0b25lL3NoYXBlLmpzIiwiLi4vc3JjL3NoYXBlcy9idXR0b24vc2hhcGUuanMiLCIuLi9zcmMvc2hhcGVzL3RpbWVsaW5lL3NoYXBlLmpzIiwiLi4vc3JjL3NoYXBlcy90aW1lbGluZTIvc2hhcGUuanMiLCIuLi9zcmMvc2hhcGVzL3BhbmVsL3NoYXBlLmpzIiwiLi4vc3JjL3NoYXBlcy9ib3gvc2hhcGUuanMiLCIuLi9zcmMvc2hhcGVzL3RhYi9zaGFwZS5qcyIsIi4uL3NyYy9sYXlvdXRzL2RlZmF1bHQuanMiLCIuLi9zcmMvbGF5b3V0cy9zd2ltbGFuZS9sYXlvdXQuanMiLCIuLi9zcmMvbGF5b3V0cy90YWJzL3RhYnMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9ob29rcy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2lzLWFycmF5LmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvaXMtb2JqZWN0LmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvaXMtb2JqZWN0LWVtcHR5LmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvaXMtbnVtYmVyLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvaXMtZGF0ZS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL21hcC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2hhcy1vd24tcHJvcC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2V4dGVuZC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2NyZWF0ZS91dGMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9jcmVhdGUvcGFyc2luZy1mbGFncy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL3NvbWUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9jcmVhdGUvdmFsaWQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9pcy11bmRlZmluZWQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvY29uc3RydWN0b3IuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9hYnMtZmxvb3IuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy90by1pbnQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9jb21wYXJlLWFycmF5cy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2RlcHJlY2F0ZS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2lzLWZ1bmN0aW9uLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbG9jYWxlL3NldC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2xvY2FsZS9jb25zdHJ1Y3Rvci5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2tleXMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9sb2NhbGUvY2FsZW5kYXIuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9sb2NhbGUvZm9ybWF0cy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2xvY2FsZS9pbnZhbGlkLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbG9jYWxlL29yZGluYWwuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9sb2NhbGUvcmVsYXRpdmUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy9hbGlhc2VzLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdW5pdHMvcHJpb3JpdGllcy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL21vbWVudC9nZXQtc2V0LmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvemVyby1maWxsLmpzIiwiLi4vc3JjL21vbWVudC9saWIvZm9ybWF0L2Zvcm1hdC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3BhcnNlL3JlZ2V4LmpzIiwiLi4vc3JjL21vbWVudC9saWIvcGFyc2UvdG9rZW4uanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy9jb25zdGFudHMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9pbmRleC1vZi5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL21vbnRoLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdW5pdHMveWVhci5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2NyZWF0ZS9kYXRlLWZyb20tYXJyYXkuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy93ZWVrLWNhbGVuZGFyLXV0aWxzLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdW5pdHMvd2Vlay5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL2RheS1vZi13ZWVrLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdW5pdHMvaG91ci5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2xvY2FsZS9iYXNlLWNvbmZpZy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2xvY2FsZS9sb2NhbGVzLmpzIiwiLi4vc3JjL21vbWVudC9saWIvY3JlYXRlL2NoZWNrLW92ZXJmbG93LmpzIiwiLi4vc3JjL21vbWVudC9saWIvY3JlYXRlL2Zyb20tc3RyaW5nLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdXRpbHMvZGVmYXVsdHMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9jcmVhdGUvZnJvbS1hcnJheS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2NyZWF0ZS9mcm9tLXN0cmluZy1hbmQtZm9ybWF0LmpzIiwiLi4vc3JjL21vbWVudC9saWIvY3JlYXRlL2Zyb20tc3RyaW5nLWFuZC1hcnJheS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2NyZWF0ZS9mcm9tLW9iamVjdC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2NyZWF0ZS9mcm9tLWFueXRoaW5nLmpzIiwiLi4vc3JjL21vbWVudC9saWIvY3JlYXRlL2xvY2FsLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L21pbi1tYXguanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvbm93LmpzIiwiLi4vc3JjL21vbWVudC9saWIvZHVyYXRpb24vY29uc3RydWN0b3IuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91dGlscy9hYnMtcm91bmQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy9vZmZzZXQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9kdXJhdGlvbi9jcmVhdGUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvYWRkLXN1YnRyYWN0LmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L2NhbGVuZGFyLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L2Nsb25lLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L2NvbXBhcmUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvZGlmZi5qcyIsIi4uL3NyYy9tb21lbnQvbGliL21vbWVudC9mb3JtYXQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvZnJvbS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL21vbWVudC90by5qcyIsIi4uL3NyYy9tb21lbnQvbGliL21vbWVudC9sb2NhbGUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvc3RhcnQtZW5kLW9mLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L3RvLXR5cGUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvdmFsaWQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvY3JlYXRpb24tZGF0YS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL3dlZWsteWVhci5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL3F1YXJ0ZXIuanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy9kYXktb2YtbW9udGguanMiLCIuLi9zcmMvbW9tZW50L2xpYi91bml0cy9kYXktb2YteWVhci5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL21pbnV0ZS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL3NlY29uZC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL21pbGxpc2Vjb25kLmpzIiwiLi4vc3JjL21vbWVudC9saWIvdW5pdHMvdGltZXpvbmUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9tb21lbnQvcHJvdG90eXBlLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbW9tZW50L21vbWVudC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2xvY2FsZS9wcmUtcG9zdC1mb3JtYXQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9sb2NhbGUvcHJvdG90eXBlLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbG9jYWxlL2xpc3RzLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbG9jYWxlL2VuLmpzIiwiLi4vc3JjL21vbWVudC9saWIvbG9jYWxlL2xvY2FsZS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2R1cmF0aW9uL2Ficy5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2R1cmF0aW9uL2FkZC1zdWJ0cmFjdC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3V0aWxzL2Ficy1jZWlsLmpzIiwiLi4vc3JjL21vbWVudC9saWIvZHVyYXRpb24vYnViYmxlLmpzIiwiLi4vc3JjL21vbWVudC9saWIvZHVyYXRpb24vYXMuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9kdXJhdGlvbi9nZXQuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9kdXJhdGlvbi9odW1hbml6ZS5qcyIsIi4uL3NyYy9tb21lbnQvbGliL2R1cmF0aW9uL2lzby1zdHJpbmcuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9kdXJhdGlvbi9wcm90b3R5cGUuanMiLCIuLi9zcmMvbW9tZW50L2xpYi9kdXJhdGlvbi9kdXJhdGlvbi5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL3RpbWVzdGFtcC5qcyIsIi4uL3NyYy9tb21lbnQvbGliL3VuaXRzL3VuaXRzLmpzIiwiLi4vc3JjL21vbWVudC9tb21lbnQuanMiLCIuLi9zcmMvbGF5b3V0cy90aW1lbGluZS90aW1lbGluZS5qcyIsIi4uL3NyYy9sYXlvdXRzL2Nhcm91c2VsL2Nhcm91c2VsLmpzIiwiLi4vc3JjL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vaW1wb3J0IEJ1aWxkaW5nQmxvY2tTaGFwZSBmcm9tICcuL3NoYXBlLWNhdGFsb2cvQnVpbGRpbmdCbG9jayc7XG5cbmNsYXNzIF9TaGFwZUZhY3Rvcnkge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMuc2hhcGVzID0ge307XG4gICAgfVxuXG4gICAgZ2V0U2hhcGVOYW1lcygpIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKHRoaXMuc2hhcGVzKTtcbiAgICB9XG5cbiAgICBnZXRTaGFwZSAoc2hhcGUpIHtcbiAgICAgICAgaWYgKCF0aGlzLnNoYXBlcy5oYXNPd25Qcm9wZXJ0eShzaGFwZSkpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLnNoYXBlc1tzaGFwZV07XG4gICAgfVxuXG4gICAgcmVnaXN0ZXIgKG9iamVjdCkge1xuICAgICAgICB0aGlzLnNoYXBlc1tvYmplY3QuZG9tYWluVHlwZV0gPSBvYmplY3Q7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbn1cblxuZXhwb3J0IGxldCBTaGFwZUZhY3RvcnkgPSBuZXcgX1NoYXBlRmFjdG9yeSgpO1xuIiwiXG5cbmltcG9ydCAqIGFzIGQzIGZyb20gXCJkM1wiO1xuXG5jbGFzcyBOYXZpZ2F0aW9uIHtcblxuICBhdHRhY2ggKHN2Zywgdmlld2VyLCBjb250ZW50Tm9kZSkge1xuICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgIHRoaXMuc3ZnID0gc3ZnO1xuICAgICB0aGlzLmNvbnRlbnROb2RlID0gY29udGVudE5vZGU7XG4gICAgIHRoaXMudmlld2VyID0gdmlld2VyO1xuXG4gICAgIHN2Zy5hcHBlbmQoXCJnXCIpLmF0dHIoXCJjbGFzc1wiLCBcIm5hdlwiKTtcbiAgICAgdGhpcy5zdGF0ZSA9IHsgXCJtb2RlXCI6XCJjbG9zZWRcIiwgXCJzZWxlY3RlZFwiOi0xIH07XG5cbiAgICAgdGhpcy5tZW51SXRlbXMgPSBbXTtcblxuICAgICB2aWV3ZXIub24oXCJjbG9zZU5hdmlnYXRpb25cIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgc2VsZi5jbG9zZSgpO1xuICAgICB9KTtcbiAgfVxuXG4gIGFkZE1lbnVJdGVtIChpdGVtKSB7XG4gICAgIHRoaXMubWVudUl0ZW1zLnB1c2goaXRlbSk7XG4gIH1cblxuICB1cGRhdGUgKCkge1xuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgbGV0IHZpcyA9IHRoaXMuc3ZnO1xuXG5cbiAgICAgIHRoaXMucm9vdCA9IHZpcy5zZWxlY3QoXCJnLm5hdlwiKS5hcHBlbmQoXCJnXCIpO1xuXG4gICAgICB0aGlzLmJhY2tkcm9wID0gdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdChcImdcIikuYXBwZW5kKFwicmVjdFwiKVxuICAgICAgICAuYXR0cihcImZpbGxcIiwgXCIjOTk5OTk5XCIpXG4gICAgICAgIC5hdHRyKFwieFwiLCAwKVxuICAgICAgICAuYXR0cihcInlcIiwgMClcbiAgICAgICAgLmF0dHIoXCJyeFwiLCA2KVxuICAgICAgICAuYXR0cihcInJ5XCIsIDYpXG4gICAgICAgIC5hdHRyKFwid2lkdGhcIiwgXCIyODBcIilcbiAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgXCI4MFwiKVxuICAgICAgICAuc3R5bGUoXCJ2aXNpYmlsaXR5XCIsIFwiaGlkZGVuXCIpO1xuXG4gICAgICBjb25zdCBtZW51Tm9kZXMgPSB2aXMuc2VsZWN0KFwiZy5uYXZcIikuc2VsZWN0KFwiZ1wiKS5zZWxlY3RBbGwoXCJnLm5hdkRldGFpbFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZGF0YSh0aGlzLm1lbnVJdGVtcyk7XG5cbiAgICAgIGNvbnN0IG5hdk5vZGVzID0gbWVudU5vZGVzLmVudGVyKClcbiAgICAgICAgICAuYXBwZW5kKFwiZ1wiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJuYXZEZXRhaWxcIilcbiAgICAgICAgICAuc3R5bGUoXCJ2aXNpYmlsaXR5XCIsIFwiaGlkZGVuXCIpO1xuXG4gICAgICBtZW51Tm9kZXMuZXhpdCgpLnJlbW92ZSgpO1xuXG4gICAgICBuYXZOb2Rlcy5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgIC5hdHRyKFwiZmlsbFwiLCBcIndoaXRlXCIpXG4gICAgICAgIC5hdHRyKFwieFwiLCAyKVxuICAgICAgICAuYXR0cihcInlcIiwgNClcbiAgICAgICAgLmF0dHIoXCJyeFwiLCA2KVxuICAgICAgICAuYXR0cihcInJ5XCIsIDYpXG4gICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIFwicm90YXRlKDM1KVwiKVxuICAgICAgICAuYXR0cihcIndpZHRoXCIsIFwiNTVcIilcbiAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgXCIxMlwiKTtcblxuICAgICAgbmF2Tm9kZXMuYXBwZW5kKFwiY2lyY2xlXCIpXG4gICAgICAgIC5zdHlsZShcImN1cnNvclwiLCBcImRlZmF1bHRcIilcbiAgICAgICAgLmF0dHIoXCJmaWxsXCIsIFwiYmxhY2tcIilcbiAgICAgICAgLmF0dHIoXCJzdHJva2VcIiwgXCJ3aGl0ZVwiKVxuICAgICAgICAuYXR0cihcInN0cm9rZS13aWR0aFwiLCBcIjJcIilcbiAgICAgICAgLmF0dHIoXCJyXCIsIFwiMTBcIik7XG5cbiAgICAgIG5hdk5vZGVzLmFwcGVuZChcInRleHRcIilcbiAgICAgICAgLnN0eWxlKFwiY3Vyc29yXCIsIFwiZGVmYXVsdFwiKVxuICAgICAgICAuYXR0cihcImR4XCIsIDApXG4gICAgICAgIC5hdHRyKFwiZHlcIiwgXCIwXCIpXG4gICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgLmF0dHIoJ2RvbWluYW50LWJhc2VsaW5lJywgJ2NlbnRyYWwnKVxuICAgICAgICAuc3R5bGUoJ2ZvbnQtZmFtaWx5JywgJ0ZvbnRBd2Vzb21lJylcbiAgICAgICAgLnN0eWxlKCdmb250LXNpemUnLCAnMTBweCcpXG4gICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJ3aGl0ZVwiKVxuICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7cmV0dXJuIGQuaWNvbjsgfSk7XG5cbiAgICAgIG5hdk5vZGVzLmFwcGVuZChcInRleHRcIilcbiAgICAgICAgLnN0eWxlKFwiY3Vyc29yXCIsIFwiZGVmYXVsdFwiKVxuICAgICAgICAuYXR0cihcImR4XCIsIDgpXG4gICAgICAgIC5hdHRyKFwiZHlcIiwgMTIpXG4gICAgICAgIC5hdHRyKFwiZmlsbFwiLCBcImJsYWNrXCIpXG4gICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJsZWZ0XCIpXG4gICAgICAgIC5zdHlsZSgnZm9udC1zaXplJywgJzAuNGVtJylcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgXCJyb3RhdGUoMzUpXCIpXG4gICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQudGl0bGU7IH0pO1xuXG4gICAgICB0aGlzLmJhY2tkcm9wXG4gICAgICAgIC5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgc2VsZi5jbG9zZSgpO1xuICAgICAgICB9KTtcblxuICAgICAgbmF2Tm9kZXNcbiAgICAgICAgLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBzZWxmLnZpZXdlci50cmlnZ2VyKFwic2hhcGVOYXZDbGlja1wiLCB7XCJkYXRhXCI6c2VsZi5zdGF0ZS5kYXRhLCBcIm1lbnVJdGVtXCI6ZX0pO1xuICAgICAgICAgICAgLy9zZWxmLmNsb3NlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICBuYXZOb2Rlcy5zZWxlY3RBbGwoJ2NpcmNsZSx0ZXh0LHJlY3QnKVxuICAgICAgICAub24oJ21vdXNlZW50ZXInLCBmdW5jdGlvbiAobm9kZVNldCwgaW5kZXgpIHtcbiAgICAgICAgICAgIGQzLnNlbGVjdCh0aGlzLnBhcmVudE5vZGUpLnNlbGVjdEFsbCgnY2lyY2xlLHJlY3QnKS5jbGFzc2VkKFwibmF2LWhvdmVyXCIsIHRydWUpO1xuICAgICAgICB9KVxuICAgICAgICAub24oJ21vdXNlbGVhdmUnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZDMuc2VsZWN0KHRoaXMucGFyZW50Tm9kZSkuc2VsZWN0QWxsKCdjaXJjbGUscmVjdCcpLmNsYXNzZWQoXCJuYXYtaG92ZXJcIiwgZmFsc2UpO1xuLy8gICAgICAgICAgICB2aXMuc2VsZWN0KHRoaXMpLnNlbGVjdEFsbCgnY2lyY2xlJykucmVtb3ZlQ2xhc3MoXCJuYXYtaG92ZXJcIik7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgdG9nZ2xlIChlLCBtYXRjaGVkSXRlbSkge1xuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgbGV0IHBvcyA9IG51bGw7Ly97eDplLngseTplLnl9O1xuICAgICAgbGV0IHN0YXRlID0gc2VsZi5zdGF0ZTtcblxuICAgICAgaWYgKHR5cGVvZiBzdGF0ZS5uZXdOb2RlICE9IFwidW5kZWZpbmVkXCIgJiYgc3RhdGUubmV3Tm9kZSAhPSBudWxsKSB7XG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcIlJFTU9WSU5HOiBcIiArIHN0YXRlLm5ld05vZGUpO1xuICAgICAgICAgIHN0YXRlLm5ld05vZGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdGF0ZS5uZXdOb2RlKTtcbiAgICAgICAgICBzdGF0ZS5uZXdOb2RlID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgbGV0IG5vdGVzID0gW107XG4gICAgICBpZiAobWF0Y2hlZEl0ZW0pIHtcbiAgICAgICAgbWF0Y2hlZEl0ZW0uZWFjaChmdW5jdGlvbiAoZCkge1xuICAgICAgICAgIGxldCBib3VuZGVkID0gdGhpcy5nZXRCQm94KCk7XG4gICAgICAgICAgc2VsZi5zdGF0ZS5uZXdOb2RlID0gdGhpcy5jbG9uZU5vZGUodHJ1ZSk7XG5cbiAgICAgICAgICBzZWxmLnN2Zy5zZWxlY3QoXCJnLm5hdlwiKS5ub2RlKCkuYXBwZW5kQ2hpbGQoc2VsZi5zdGF0ZS5uZXdOb2RlKTtcbiAgICAgICAgICBkMy5zZWxlY3Qoc2VsZi5zdGF0ZS5uZXdOb2RlKVxuICAgICAgICAgICAgLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgc2VsZi5jbG9zZSgpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgcG9zID0ge2xlZnQ6ZS54ICsgYm91bmRlZC54LCB0b3A6ZS55ICsgYm91bmRlZC55LCB3aWR0aDpib3VuZGVkLndpZHRoLCBoZWlnaHQ6Ym91bmRlZC5oZWlnaHQsIGJvdW5kaW5nOnRoaXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCl9O1xuXG4gICAgICAgICAgbm90ZXMgPSAoIGQuaGFzT3duUHJvcGVydHkoJ25vdGVzJykgPyBkLm5vdGVzIDogW10gKTtcbiAgICAgICAgICBzZWxmLnN0YXRlLmRhdGEgPSBkO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnN0YXRlLm1vZGUgPT0gXCJjbG9zZWRcIikge1xuICAgICAgICB0aGlzLmRvT3BlbihlLCBwb3MsIG5vdGVzKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlbGVjdGVkICE9IGUuaW5kZXgpIHtcbiAgICAgICAgICB0aGlzLmRvT3BlbihlLCBwb3MsIG5vdGVzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmNsb3NlKGUpO1xuICAgICAgICB9XG4gICAgICB9XG4gIH1cblxuICBvcGVuIChlKSB7XG4gICAgdGhpcy5kb09wZW4gKGUsIHt4OmUueCwgeTplLnl9KTtcbiAgfVxuXG4gIGRvT3BlbiAoZSwgcG9zaXRpb24sIG5vdGVzKSB7XG4gICAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAgIHRoaXMuc3RhdGUueCA9IHBvc2l0aW9uLmxlZnQ7XG4gICAgICB0aGlzLnN0YXRlLnkgPSBwb3NpdGlvbi50b3A7XG4gICAgICB0aGlzLnN0YXRlLnNlbGVjdGVkID0gZS5pbmRleDtcbiAgICAgIHRoaXMuc3RhdGUubW9kZSA9IFwib3BlblwiO1xuXG4gICAgICBsZXQgc2NhbGUgPSAxLjY7XG4gICAgICBsZXQgZGVsdGEgPSAwO1xuXG4gICAgICBjb25zdCBzdGFydCA9IDE5ODtcbiAgICAgIC8vIGNhbGN1bGF0ZSBwb3NpdGlvbnMgb2YgbWVudSBpdGVtc1xuICAgICAgbGV0IHBvc2l0aW9ucyA9IFswXTtcbiAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IDEwOyBpKyspIHtcbiAgICAgICAgaWYgKChzdGFydCsoMzYqaSkpID4gMzYwKSB7XG4gICAgICAgICAgcG9zaXRpb25zLnB1c2goc3RhcnQgKyAoMzYgKiBpKSAtIDM2MCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcG9zaXRpb25zLnB1c2goc3RhcnQgKyAoMzYgKiBpKSApO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGxldCB2aXMgPSB0aGlzLnN2ZztcblxuICAgICAgLy8gRGV0ZXJtaW5lIGhvdyBiaWcgdGhlIGhlaWdodCBzaG91bGQgYmUgYmFzZWQgb24gdGhlIFwibm90ZXNcIiB0aGF0IGFyZSBwcm92aWRlZFxuICAgICAgLy8gTW92ZSBhIG5hdmlnYXRpb24gRElWIHRvIHRoZSBzYW1lIHBvc2l0aW9uIGZvciBkaXNwbGF5aW5nXG4gICAgICBsZXQgZWxlbWVudCA9IHRoaXMuY29udGVudE5vZGUucGFyZW50Tm9kZTtcbiAgICAgIGxldCBib2R5UmVjdCA9IGRvY3VtZW50LmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICAgICAgZWxlbVJlY3QgPSBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLFxuICAgICAgICAgIG9mZnNldCAgID0gZWxlbVJlY3QudG9wIC0gYm9keVJlY3QudG9wO1xuXG4gICAgICB0aGlzLnByaW50UmVjdChwb3NpdGlvbik7XG4gICAgICB0aGlzLnByaW50UmVjdChlbGVtUmVjdCk7XG4gICAgICB0aGlzLnByaW50UmVjdChib2R5UmVjdCk7XG5cbiAgICAgIGxldCBtaW5IZWlnaHQgPSBNYXRoLm1heChwb3NpdGlvbi5ib3VuZGluZy5oZWlnaHQgKyA1LCAoc2VsZi5tZW51SXRlbXMgPT0gMCA/IDUwIDogMTQ1KSk7XG5cbiAgICAgIGQzLnNlbGVjdCh0aGlzLmNvbnRlbnROb2RlKVxuICAgICAgICAgICAgLmNsYXNzZWQoXCJjb250ZW50XCIsIHRydWUpXG4gICAgICAgICAgICAuc3R5bGUoXCJkaXNwbGF5XCIsIFwiYmxvY2tcIilcbiAgICAgICAgICAgIC5zdHlsZShcInBvc2l0aW9uXCIsIFwiYWJzb2x1dGVcIilcbiAgICAgICAgICAgIC5zdHlsZShcIndpZHRoXCIsIFwiXCIgKyAoKHNlbGYubWVudUl0ZW1zLmxlbmd0aCAqIDM4KSArIChwb3NpdGlvbi5ib3VuZGluZy53aWR0aCkpICsgXCJweFwiKVxuICAgICAgICAgICAgLnN0eWxlKFwibGVmdFwiLCBcIlwiKyAocG9zaXRpb24uYm91bmRpbmcubGVmdCkgKyBcInB4XCIpXG4gICAgICAgICAgICAuc3R5bGUoXCJ0b3BcIiwgXCJcIiArICgtZWxlbVJlY3QudG9wICsgcG9zaXRpb24uYm91bmRpbmcudG9wICsgbWluSGVpZ2h0KSAgKyBcInB4XCIpO1xuXG4gICAgICBkMy5zZWxlY3QodGhpcy5jb250ZW50Tm9kZSlcbiAgICAgICAgICAgLnNlbGVjdEFsbChcInRhYmxlXCIpXG4gICAgICAgICAgIC5yZW1vdmUoKTtcblxuICAgICAgbGV0IG5vdGVOb2RlID0gZDMuc2VsZWN0KHRoaXMuY29udGVudE5vZGUpLmFwcGVuZChcInRhYmxlXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNlbGVjdEFsbChcInRyXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmRhdGEobm90ZXMpO1xuICAgICAgbm90ZU5vZGUuZW50ZXIoKVxuICAgICAgICAgICAuYXBwZW5kKFwidHJcIilcbiAgICAgICAgICAgICAgIC5odG1sKGZ1bmN0aW9uKGQpIHsgaWYgKHR5cGVvZiBkID09IFwic3RyaW5nXCIpIHsgcmV0dXJuIGQ7IH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIjx0ZCBjbGFzcz0na2V5Jz5cIiArIGQua2V5ICsgXCI8L3RkPjx0ZCBjbGFzcz0ndmFsdWUnPlwiICsgZC52YWx1ZSArIFwiPC90ZD5cIjtcbiAgICAgICAgICAgICAgIH19KTtcblxuXG4gICAgICBsZXQgZGltcyA9IHRoaXMuY29udGVudE5vZGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICB0aGlzLnByaW50UmVjdChkaW1zKTtcblxuICAgICAgbGV0IG9mZnNldDIgPSBkaW1zLnRvcCAtIGVsZW1SZWN0LnRvcDtcblxuXG4gICAgICAvL2NvbnNvbGUubG9nKFwiT0ZGU0VUID0gXCIrb2Zmc2V0MitcIiwgXCIrZGltcy50b3ApO1xuICAgICAgbGV0IGNvbnRlbnRIZWlnaHQgPSBkaW1zLmhlaWdodDtcblxuICAgICAgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdEFsbCgnZy5ub2RlJylcbiAgICAgICAgLy8uYXR0cihcInRyYW5zZm9ybVwiLCBcInRyYW5zbGF0ZSgwLDApXCIpXG4gICAgICAgIC5zdHlsZShcInBvaW50ZXItZXZlbnRzXCIsIFwibm9uZVwiKTtcblxuICAgICAgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdChcImdcIilcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oZCkge1xuICAgICAgICAgICByZXR1cm4gXCJ0cmFuc2xhdGUoXCIgKyAocG9zaXRpb24ubGVmdCArIChwb3NpdGlvbi53aWR0aC8yKSkgKyBcIixcIiArIChwb3NpdGlvbi50b3AgKyAocG9zaXRpb24uaGVpZ2h0LzIpKSArIFwiKVwiO1xuICAgICAgICB9KTtcbiAgICAgIHZpcy5zZWxlY3QoXCJnLm5hdlwiKS5zZWxlY3QoJ3JlY3QnKVxuICAgICAgICAuYXR0cihcIm9wYWNpdHlcIiwgMClcbiAgICAgICAgLnN0eWxlKFwidmlzaWJpbGl0eVwiLCBcInZpc2libGVcIilcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oZCkge1xuICAgICAgICAgIHJldHVybiBcInRyYW5zbGF0ZShcIiArICgoLXBvc2l0aW9uLndpZHRoLzIpIC0gNSkgKyBcIixcIiArICgoLXBvc2l0aW9uLmhlaWdodC8yKSAtIDUpICsgXCIpXCI7XG4gICAgICAgIH0pXG4gICAgICAgIC5hdHRyKFwid2lkdGhcIiwgZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICByZXR1cm4gKHNlbGYubWVudUl0ZW1zLmxlbmd0aCA/IDA6MCkgKyAoc2VsZi5tZW51SXRlbXMubGVuZ3RoICogMzgpICsgcG9zaXRpb24ud2lkdGggKyAxMDtcbiAgICAgICAgfSlcbiAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICByZXR1cm4gMCArIE1hdGgubWF4KHBvc2l0aW9uLmhlaWdodCArIGNvbnRlbnRIZWlnaHQsIDYwKSAgKyAwO1xuICAgICAgICB9KVxuICAgICAgICAudHJhbnNpdGlvbigpXG4gICAgICAgICAgLmF0dHIoXCJvcGFjaXR5XCIsIDAuOSkub24oXCJlbmRcIiwgZnVuY3Rpb24oKSB7IHJldHVybjsgfSk7XG5cbiAgICAgIGxldCBtZW51UG9zaXRpb24gPSAwO1xuICAgICAgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdEFsbCgnZy5uYXZEZXRhaWwnKVxuICAgICAgICAuc3R5bGUoXCJvcGFjaXR5XCIsIDApXG4gICAgICAgIC5zdHlsZShcInZpc2liaWxpdHlcIiwgXCJ2aXNpYmxlXCIpXG4gICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgcmV0dXJuIFwidHJhbnNsYXRlKFwiICsgMCArIFwiLFwiICsgKDIwICsgZGVsdGEpICsgXCIpIHNjYWxlKFwiICsgc2NhbGUgKyBcIilcIjtcbiAgICAgICAgfSlcbiAgICAgICAgLnRyYW5zaXRpb24oKVxuICAgICAgICAgIC5zdHlsZShcIm9wYWNpdHlcIiwgMTAwKVxuICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgY29uc3QgeSA9ICgtcG9zaXRpb24uaGVpZ2h0LzIpICsgMjAgKyBkZWx0YTtcbiAgICAgICAgICAgY29uc3QgeCA9IChwb3NpdGlvbi53aWR0aC8yKSArICgoMjIrMSkgKiAobWVudVBvc2l0aW9uKyspICogc2NhbGUpICsgMjU7XG4gICAgICAgICAgIHJldHVybiBcInRyYW5zbGF0ZShcIiArIHggKyBcIixcIiArIHkgKyBcIikgc2NhbGUoXCIgKyBzY2FsZSArIFwiKVwiO1xuICAgICAgICB9KTtcblxuICB9XG5cbiAgY2xvc2UgKCkge1xuICAgICAgbGV0IHN0YXRlID0gdGhpcy5zdGF0ZTtcbiAgICAgIGxldCB2aXMgPSB0aGlzLnN2ZztcblxuICAgICAgaWYgKHN0YXRlLm1vZGUgPT0gXCJjbG9zZWRcIikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBzdGF0ZS5tb2RlID0gXCJjbG9zZWRcIjtcblxuICAgICAgZDMuc2VsZWN0KHRoaXMuY29udGVudE5vZGUpXG4gICAgICAgICAgICAuc3R5bGUoXCJkaXNwbGF5XCIsIFwibm9uZVwiKTtcblxuICAgICAgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdEFsbCgnZy5uYXZEZXRhaWwnKVxuICAgICAgICAudHJhbnNpdGlvbigpXG4gICAgICAgIC5zdHlsZShcIm9wYWNpdHlcIiwgMClcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oKSB7IHJldHVybiBcInRyYW5zbGF0ZShcIiArIHN0YXRlLnggKyBcIixcIiArIChzdGF0ZS55ICsgNDApICtcIilcIjsgIH0gKVxuICAgICAgICAub24oXCJlbmRcIiwgZnVuY3Rpb24oKSB7IHZpcy5zZWxlY3QoXCJnLm5hdlwiKS5zZWxlY3RBbGwoJ2cubmF2RGV0YWlsJykuc3R5bGUoXCJ2aXNpYmlsaXR5XCIsIFwiaGlkZGVuXCIpOyB9ICk7XG5cbiAgICAgIGlmIChzdGF0ZS5uZXdOb2RlKSB7XG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcIlJFTU9WSU5HOiBcIiArIHN0YXRlLm5ld05vZGUpO1xuICAgICAgICAgIHN0YXRlLm5ld05vZGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdGF0ZS5uZXdOb2RlKTtcbiAgICAgICAgICBzdGF0ZS5uZXdOb2RlID0gbnVsbDtcbiAgICAgICAgICBzdGF0ZS5kYXRhID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdCgncmVjdCcpXG4gICAgICAgIC50cmFuc2l0aW9uKClcbiAgICAgICAgLmF0dHIoXCJvcGFjaXR5XCIsIDApXG4gICAgICAgIC5vbihcImVuZFwiLCBmdW5jdGlvbigpIHsgdmlzLnNlbGVjdChcImcubmF2XCIpLnNlbGVjdCgncmVjdCcpLnN0eWxlKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTsgfSApO1xuICB9XG5cblxuICBwcmludFJlY3QoZWxlbVJlY3QpIHtcbiAgICAgY29uc29sZS5sb2coXCJFOiBUb3A9XCIgKyBlbGVtUmVjdC50b3ArXCIsTGVmdD1cIitlbGVtUmVjdC5sZWZ0K1wiLFdpZHRoPVwiK2VsZW1SZWN0LndpZHRoK1wiLEhlaWdodD1cIitlbGVtUmVjdC5oZWlnaHQpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IE5hdmlnYXRpb247XG4iLCIvL2ltcG9ydCBCdWlsZGluZ0Jsb2NrU2hhcGUgZnJvbSAnLi9zaGFwZS1jYXRhbG9nL0J1aWxkaW5nQmxvY2snO1xuXG5jbGFzcyBfTGF5b3V0RmFjdG9yeSB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5sYXlvdXRzID0ge307XG4gICAgfVxuXG4gICAgZ2V0TGF5b3V0TmFtZXMoKSB7XG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmxheW91dHMpO1xuICAgIH1cblxuICAgIGdldExheW91dCAobGF5b3V0KSB7XG4gICAgICAgIGlmICghdGhpcy5sYXlvdXRzLmhhc093blByb3BlcnR5KGxheW91dCkpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmxheW91dHNbbGF5b3V0XTtcbiAgICB9XG5cbiAgICByZWdpc3RlciAob2JqZWN0KSB7XG4gICAgICAgIHRoaXMubGF5b3V0c1tvYmplY3QuZG9tYWluVHlwZV0gPSBvYmplY3Q7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbn1cblxuZXhwb3J0IGxldCBMYXlvdXRGYWN0b3J5ID0gbmV3IF9MYXlvdXRGYWN0b3J5KCk7XG4iLCJpbXBvcnQgKiBhcyBkMyBmcm9tIFwiZDNcIjtcbmltcG9ydCBOYXZpZ2F0aW9uIGZyb20gJy4vTmF2aWdhdGlvbkhvcml6b250YWwuanMnO1xuXG5pbXBvcnQgZmEgZnJvbSAnZm9udGF3ZXNvbWUnO1xuXG5pbXBvcnQge1NoYXBlRmFjdG9yeX0gZnJvbSAnLi9TaGFwZUZhY3RvcnknO1xuaW1wb3J0IHtMYXlvdXRGYWN0b3J5fSBmcm9tICcuL0xheW91dEZhY3RvcnknO1xuXG5leHBvcnQgY2xhc3MgVmlld2VyQ2FudmFzIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5kZWZhdWx0Q29uZmlnID0ge1xuICAgICAgIFwiZW5hYmxlX3pvbmVfYm91bmRhcmllc1wiOiBmYWxzZSxcbiAgICAgICBcImVuYWJsZV9jb2xsaXNpb25fYXZvaWRhbmNlXCI6IGZhbHNlLFxuICAgICAgIFwicHJlZmVycmVkX3JhdGlvXCI6IDAuNjY2NjY2NixcbiAgICAgICBcImJhY2tncm91bmRfY29sb3JcIjogXCJibGFja1wiLFxuICAgICAgIFwiZml4ZWRfd2lkdGhcIjogZmFsc2UsXG4gICAgICAgXCJwYXBlcl93aWR0aFwiOiAxMDAwLFxuICAgICAgIFwiZm9yY2VfY29sbGlkZVwiOiAwXG4gICAgfTtcbiAgfVxuXG4gIG1vdW50Tm9kZShyb290KSB7XG4gICAgdGhpcy5jbGllbnRXaWR0aCA9IHJvb3QuY2xpZW50V2lkdGg7XG4gICAgdGhpcy5kb21Ob2RlID0gZDMuc2VsZWN0KHJvb3QpLmFwcGVuZChcInN2Z1wiKS5ub2RlKCk7XG4gICAgdGhpcy5jb250ZW50Tm9kZSA9IGQzLnNlbGVjdChyb290KS5hcHBlbmQoXCJkaXZcIikubm9kZSgpO1xuICAgIHRoaXMuaW5pdENhbnZhcygpO1xuICB9XG5cbiAgc2V0U3RhdGUoc3RhdGUpIHtcbiAgICB0aGlzLnN0YXRlID0gc3RhdGU7XG4gIH1cblxuICB0cmlnZ2VyIChldmVudCwgZGF0YSwgZG9jID0gdGhpcy5zdGF0ZSkge1xuICAgIGlmICh0aGlzLmV2ZW50cyAmJiB0aGlzLmV2ZW50c1tldmVudF0pIHtcbiAgICAgICAgbGV0IGZ1bmMgPSB0aGlzLmV2ZW50c1tldmVudF1bMF07XG4gICAgICAgIGZvciAobGV0IGZ1bmNJbmRleCA9IDA7IGZ1bmNJbmRleCA8IHRoaXMuZXZlbnRzW2V2ZW50XS5sZW5ndGg7IGZ1bmNJbmRleCsrICkge1xuICAgICAgICAgICAgbGV0IGZ1bmMgPSB0aGlzLmV2ZW50c1tldmVudF1bZnVuY0luZGV4XTtcbiAgICAgICAgICAgIGZ1bmMoZXZlbnQsIGRhdGEsIGRvYyk7XG4gICAgICAgIH1cbiAgICB9XG4gIH1cblxuICBvbiAoZXZlbnQsIGZ1bmMpIHtcbiAgICBpZiAodHlwZW9mIHRoaXMuZXZlbnRzID09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgdGhpcy5ldmVudHMgPSBbXTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIHRoaXMuZXZlbnRzW2V2ZW50XSA9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgIHRoaXMuZXZlbnRzW2V2ZW50XSA9IFtdO1xuICAgIH1cbiAgICB0aGlzLmV2ZW50c1tldmVudF0ucHVzaChmdW5jKTtcbiAgfVxuXG4gIGluaXRDYW52YXMoKSB7XG4gICAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAgIGNvbnN0IGVsID0gdGhpcy5kb21Ob2RlO1xuXG4gICAgICAvL3RoaXMuem9uZXMgPSB0aGlzLnN0YXRlLnpvbmVzO1xuXG4gICAgICBjb25zdCBwYXBlcldpZHRoID0gdGhpcy5nZXRDb25maWcoXCJwYXBlcl93aWR0aFwiKTtcbiAgICAgIGxldCByYXRpbyA9IHRoaXMuZ2V0Q29uZmlnKFwicHJlZmVycmVkX3JhdGlvXCIpO1xuICAgICAgY29uc3QgZml4ZWRXaWR0aCA9IHRoaXMuZ2V0Q29uZmlnKFwiZml4ZWRfd2lkdGhcIik7XG5cblxuICAgICAgbGV0IGZ1bGwgPSBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgIHJhdGlvID0gZnVsbC5oZWlnaHQvZnVsbC53aWR0aDtcblxuICAgICAgY29uc3Qgb3cgPSAoZml4ZWRXaWR0aCA/IHBhcGVyV2lkdGggOiBlbC5wYXJlbnROb2RlLmNsaWVudFdpZHRoKTtcbiAgICAgIGNvbnN0IG9oID0gb3cgKiByYXRpbztcblxuXG4gICAgICBjb25zb2xlLmxvZyhcIlcgPSBcIitvdyArXCIsIEggPSBcIiArIG9oICsgXCIgOiBcIiArIGVsLnBhcmVudE5vZGUuY2xpZW50V2lkdGggKyBcIiAob3JpZ2luYWwgPSBcIiArIHNlbGYuY2xpZW50V2lkdGgrXCIpXCIpO1xuXG4gICAgICBsZXQgdmlzID0gZDMuc2VsZWN0KGVsKVxuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgb3cpO1xuXG4gICAgICBsZXQgcmVjdCA9IFswLCAwLCBwYXBlcldpZHRoLCBwYXBlcldpZHRoICogcmF0aW9dO1xuXG4gICAgICBjb25zdCB6b25lID0gXCJcIiArIChyZWN0WzBdKSArIFwiIFwiICsgcmVjdFsxXSArIFwiIFwiICsgcmVjdFsyXSArIFwiIFwiICsgcmVjdFszXTtcblxuICAgICAgdmlzLmF0dHIoXCJwcmVzZXJ2ZUFzcGVjdFJhdGlvXCIsIFwieE1pbllNaW5cIik7XG4gICAgICB2aXMuYXR0cihcInZpZXdCb3hcIiwgem9uZSk7IC8vIFwiMCAwIDEwMDAgODAwXCJcblxuICAgICAgY29uc3QgZGVmcyA9IHZpcy5hcHBlbmQoXCJkZWZzXCIpO1xuICAgICAgZGVmcy5zZWxlY3RBbGwoXCJtYXJrZXJcIilcbiAgICAgICAgICAuZGF0YShbXCJzdWl0XCIsIFwibGljZW5zaW5nXCIsIFwicmVzb2x2ZWRcIiwgXCJsaWZlY3ljbGVcIl0pXG4gICAgICAgIC5lbnRlcigpLmFwcGVuZChcIm1hcmtlclwiKVxuICAgICAgICAgIC5hdHRyKFwiaWRcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gZDsgfSlcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIFwiX2VuZF9cIiArIGQ7IH0pXG4gICAgICAgICAgLmF0dHIoXCJ2aWV3Qm94XCIsIFwiMCAtNSAxMCAxMFwiKVxuICAgICAgICAgIC5hdHRyKFwicmVmWFwiLCAwKVxuICAgICAgICAgIC5hdHRyKFwicmVmWVwiLCAwKVxuICAgICAgICAgIC5hdHRyKFwibWFya2VyV2lkdGhcIiwgNSlcbiAgICAgICAgICAuYXR0cihcIm1hcmtlckhlaWdodFwiLCA1KVxuICAgICAgICAgIC5hdHRyKFwib3JpZW50XCIsIFwiYXV0b1wiKVxuICAgICAgICAuYXBwZW5kKFwicGF0aFwiKVxuICAgICAgICAgIC5hdHRyKFwiZFwiLCBcIk0wLC01TDEwLDBMMCw1XCIpO1xuXG4gICAgICBjb25zdCBmaWx0ZXIgPSBkZWZzLmFwcGVuZChcImZpbHRlclwiKS5hdHRyKFwiaWRcIiwgXCJkcm9wLXNoYWRvd1wiKS5hdHRyKFwieFwiLCAwKS5hdHRyKFwieVwiLCAwKS5hdHRyKFwiaGVpZ2h0XCIsIFwiMTUwJVwiKS5hdHRyKFwid2lkdGhcIiwgXCIxNTAlXCIpO1xuICAgICAgZmlsdGVyLmFwcGVuZChcImZlT2Zmc2V0XCIpLmF0dHIoXCJpblwiLCBcIlNvdXJjZUFscGhhXCIpLmF0dHIoXCJkeFwiLCAzKS5hdHRyKFwiZHlcIiwgMykuYXR0cihcInJlc3VsdFwiLCBcIm9mZk91dFwiKTtcbiAgICAgIGZpbHRlci5hcHBlbmQoXCJmZUdhdXNzaWFuQmx1clwiKS5hdHRyKFwiaW5cIiwgXCJvZmZPdXRcIikuYXR0cihcInN0ZERldmlhdGlvblwiLCAxKS5hdHRyKFwicmVzdWx0XCIsIFwiYmx1ck91dFwiKTtcbiAgICAgIGZpbHRlci5hcHBlbmQoXCJmZUJsZW5kXCIpLmF0dHIoXCJpblwiLCBcIlNvdXJjZUdyYXBoaWNcIikuYXR0cihcImluMlwiLCBcImJsdXJPdXRcIikuYXR0cihcIm1vZGVcIiwgXCJub3JtYWxcIik7XG4vLyAgICAgIGNvbnN0IGZlTWVyZ2UgPSBmaWx0ZXIuYXBwZW5kKFwiZmVNZXJnZVwiKTtcbi8vICAgICAgZmVNZXJnZS5hcHBlbmQoXCJmZU1lcmdlTm9kZVwiKS5hdHRyKFwiaW5cIiwgXCJvZmZzZXRCbHVyXCIpO1xuLy8gICAgICBmZU1lcmdlLmFwcGVuZChcImZlTWVyZ2VOb2RlXCIpLmF0dHIoXCJpblwiLCBcIlNvdXJjZUdyYXBoaWNcIik7XG5cblxuLy8gICAgICBsZXQgYWxsID0gdmlzLmFwcGVuZChcInJlY3RcIilcbi8vICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgdGhpcy5nZXRDb25maWcoXCJiYWNrZ3JvdW5kX2NvbG9yXCIpKVxuLy8gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBcIjEwMCVcIilcbi8vICAgICAgICAgIC5hdHRyKFwiaGVpZ2h0XCIsIFwiMTAwJVwiKVxuLy8gICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4vLyAgICAgICAgICAuYXR0cihcInlcIiwgMCk7XG5cbiAgICAgIGxldCB6b25lUm9vdCA9IHZpcy5hcHBlbmQoXCJnXCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcInpvbmVzXCIpO1xuXG4gICAgICB2aXMuYXBwZW5kKFwiZ1wiKS5hdHRyKFwiY2xhc3NcIiwgXCJhbGxfbGluZXNcIik7XG5cbiAgICAgIHZpcy5hcHBlbmQoXCJnXCIpLmF0dHIoXCJjbGFzc1wiLCBcIm5vZGVTZXRcIik7XG5cblxuICAgICAgY29uc3QgY2FsY0xpbmtEaXN0YW5jZSA9IGZ1bmN0aW9uIChhLCBiKSB7XG4vLyAgICAgICAgICByZXR1cm4gTWF0aC5hYnMoTWF0aC5zcXJ0KE1hdGgucG93KGEueCAtIGIueCwgMikgKyBNYXRoLnBvdyhhLnkgLSBiLnksIDIpKSk7XG5cbiAgICAgICAgIGlmIChhLnNvdXJjZS5ib3VuZGFyeSAhPSBhLnRhcmdldC5ib3VuZGFyeSkge1xuICAgICAgICAgICAgcmV0dXJuIG93LzQ7XG4gICAgICAgICB9IGVsc2UgaWYgKGEudHlwZSA9PSBcInN0ZXAtYWZ0ZXJcIikge1xuICAgICAgICAgICAgcmV0dXJuIChhLmhpbnQgPyBhLmhpbnQgOiAxNTApO1xuICAgICAgICAgfSBlbHNlIGlmIChhLnR5cGUgPT0gXCJzdHJhaWdodFwiIHx8IGEudHlwZSA9PSBcInN0ZXAtYmVmb3JlXCIpIHtcbi8vICAgICAgICAgICAgY29uc29sZS5sb2coXCJQT1NJVElPTlM6IFwiICsgSlNPTi5zdHJpbmdpZnkoYS5zb3VyY2UpKTtcbi8vICAgICAgICAgICAgY29uc29sZS5sb2coXCIgQU5EIFwiICsgSlNPTi5zdHJpbmdpZnkoYS50YXJnZXQpKTtcbi8vICAgICAgICAgICAgY29uc29sZS5sb2coXCIgLi5YIFwiICsgYS5zb3VyY2UueCArXCIsIFwiK2EudGFyZ2V0LngpO1xuLy8gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiAuLlkgXCIgKyBhLnNvdXJjZS55ICtcIiwgXCIrYS50YXJnZXQueSk7XG4vLyAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIC4uWSBcIiArIGEuc291cmNlLnkgK1wiLCBcIithLnRhcmdldC55KTtcbi8vICAgICAgICAgICAgY29uc29sZS5sb2coXCIgLi5DWCBcIiArICgoYS5zb3VyY2UueCAtIGEudGFyZ2V0LngpICogMikpO1xuLy8gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiAuLkNZIFwiICsgKChhLnNvdXJjZS55IC0gYS50YXJnZXQueSkgKiAyKSk7XG4vLyAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIC4uQW5zd2VyIFwiICsgKE1hdGguYWJzKChhLnNvdXJjZS54IC0gYS50YXJnZXQueCkgKiAyKSArIE1hdGguYWJzKChhLnNvdXJjZS55IC0gYS50YXJnZXQueSkgKiAyKSkpO1xuLy8gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNRUlQgOiBcIiArIE1hdGguc3FydChNYXRoLmFicygoYS5zb3VyY2UueCAtIGEudGFyZ2V0LngpICogMikgKyBNYXRoLmFicygoYS5zb3VyY2UueSAtIGEudGFyZ2V0LnkpICogMikpKTtcbiAgICAgICAgICAgIHJldHVybiAoYS5oaW50ID8gYS5oaW50IDogMTUwKTtcbiAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gKGEuaGludCA/IGEuaGludCA6IDgwKTtcbiAgICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGxldCBzaW11bGF0aW9uID0gZDMuZm9yY2VTaW11bGF0aW9uKClcbiAgICAgICAgICAuZm9yY2UoXCJsaW5rXCIsIGQzLmZvcmNlTGluayhbXSkuZGlzdGFuY2UoY2FsY0xpbmtEaXN0YW5jZSkuc3RyZW5ndGgoMC41KSlcbiAgICAgICAgICAuZm9yY2UoXCJjb2xsaWRlXCIsZDMuZm9yY2VDb2xsaWRlKCBmdW5jdGlvbihkKXsgcmV0dXJuIHNlbGYuZ2V0Q29uZmlnKFwiZm9yY2VfY29sbGlkZVwiKTsgfSkuaXRlcmF0aW9ucygxKSApXG4gICAgICAgICAgLmZvcmNlKFwiY2hhcmdlXCIsIGQzLmZvcmNlTWFueUJvZHkoKS5zdHJlbmd0aCgwKSk7XG5cbiAgICAgIGxldCBmb3JjZSA9IHNpbXVsYXRpb247XG4vLyAgICAgICAgICAubGlua3MoW10pXG4vLyAgICAgICAgICAubm9kZXMoW10pXG4vLyAgICAgICAgICAuZ3Jhdml0eSgwKVxuLy8gICAgICAgICAgLmNoYXJnZSgwKVxuLy8gICAgICAgICAgLmxpbmtEaXN0YW5jZShjYWxjTGlua0Rpc3RhbmNlKVxuLy8gICAgICAgICAgLnNpemUoW293LCBvaF0pO1xuXG4gICAgICB0aGlzLnZpcyA9IHZpcztcbiAgICAgIHRoaXMuZm9yY2UgPSBmb3JjZTtcblxuICAgICAgLy8gZmEtYm9vaywgZmEtdGgsIGZhLXNoYXJlLXNxdWFyZS1vLCBmYS1jYW1lcmEtcmV0cm9cbiAgICAgIGNvbnN0IG5hdkRhdGEgPSBbXG4gICAgICAgIHtcImlkXCI6XCIxXCIsIFwidGl0bGVcIjpcIkRvY3VtZW50YXRpb25cIiwgXCJpY29uXCI6ZmEuYm9va30sXG4gICAgICAgIHtcImlkXCI6XCIyXCIsIFwidGl0bGVcIjpcIkNvbmZpZ3VyYXRpb25cIiwgXCJpY29uXCI6ZmEoJ3Rhc2tzJyl9LFxuICAgICAgICB7XCJpZFwiOlwiM1wiLCBcInRpdGxlXCI6XCJUb3BvbG9neVwiLCBcImljb25cIjpmYS50aCB9LFxuICAgICAgICB7XCJpZFwiOlwiNFwiLCBcInRpdGxlXCI6XCJBdWRpdFwiLCBcImljb25cIjpmYSgnY2FtZXJhLXJldHJvJykgfVxuICAgICAgXTtcblxuICAgICAgdGhpcy5uYXZpZ2F0aW9uID0gbmV3IE5hdmlnYXRpb24oKTtcblxuICAgICAgdGhpcy5uYXZpZ2F0aW9uLmF0dGFjaCh2aXMsIHRoaXMsIHRoaXMuY29udGVudE5vZGUpO1xuIC8vICAgICB0aGlzLm5hdmlnYXRpb24uYWRkTWVudUl0ZW0obmF2RGF0YVswXSk7XG4vLyAgICAgIHRoaXMubmF2aWdhdGlvbi5hZGRNZW51SXRlbShuYXZEYXRhWzFdKTtcbi8vICAgICAgdGhpcy5uYXZpZ2F0aW9uLmFkZE1lbnVJdGVtKG5hdkRhdGFbMl0pO1xuLy8gICAgICB0aGlzLm5hdmlnYXRpb24uYWRkTWVudUl0ZW0obmF2RGF0YVszXSk7XG5cbiAgICAgIHRoaXMubmF2aWdhdGlvbi51cGRhdGUoKTtcbi8vY29uc29sZS5sb2coXCJOQVYgPSBcIitmYUljb25DaGFyc1swXS51bmljb2RlKTtcblxuLy8gICAgICBhbGwub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoZCkge1xuLy8gICAgICAgICBzZWxmLmNsb3NlUG9wdXBzKCk7XG4vLyAgICAgIH0pO1xuICB9XG5cbiAgY2xvc2VQb3B1cHMgKCkge1xuICAgICB0aGlzLm5hdmlnYXRpb24uY2xvc2UoKTtcbiAgfVxuXG4gIHJlZnJlc2hab25lcyAoem9uZXMsIGNvbmZpZykge1xuXG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICBsZXQgdmlzID0gdGhpcy52aXM7XG5cbiAgICAgIGxldCBwYWdlSGVpZ2h0ID0gdmlzLm5vZGUoKS5wYXJlbnROb2RlLmNsaWVudEhlaWdodDtcbiAgICAgIGxldCBwYWdlV2lkdGggPSB2aXMubm9kZSgpLnBhcmVudE5vZGUuY2xpZW50V2lkdGg7XG5cbiAgICAgIGxldCB6b25lUm9vdCA9IHZpcy5zZWxlY3QoXCJnLnpvbmVzXCIpO1xuXG4gICAgICB6b25lUm9vdC5zZWxlY3RBbGwoXCJnXCIpLnJlbW92ZSgpO1xuXG4gICAgICB2aXMuYXR0cihcInBhZ2VXaWR0aFwiLCBwYWdlV2lkdGgpO1xuICAgICAgdmlzLmF0dHIoXCJwYWdlSGVpZ2h0XCIsIHBhZ2VIZWlnaHQpO1xuXG4gICAgICB0aGlzLnpvbmVzID0gem9uZXM7XG5cbiAgICAgIHpvbmVzLmZvckVhY2goZnVuY3Rpb24gKHpvbmUsIHpvbmVJbmRleCkge1xuXG4gICAgICAgIGxldCBsYXlvdXQgPSBMYXlvdXRGYWN0b3J5LmdldExheW91dCh6b25lLnR5cGUpO1xuICAgICAgICBsYXlvdXQuYnVpbGQodmlzLCB6b25lLCB6b25lUm9vdCwgc2VsZik7XG5cbiAgICAgICAgbGF5b3V0Lm9yZ2FuaXplKHZpcywgem9uZUluZGV4LCB6b25lcyk7XG4gICAgICB9KTtcblxuICAgICAgem9uZVJvb3Quc2VsZWN0QWxsKFwiZ1wiKVxuICAgICAgICAub24oJ21vdXNlZW50ZXInLCBmdW5jdGlvbiAobm9kZVNldCwgaW5kZXgpIHtcbiAgICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5jbGFzc2VkKFwiem9uZS1ob3ZlclwiLCB0cnVlKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm9uKCdtb3VzZWxlYXZlJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5jbGFzc2VkKFwiem9uZS1ob3ZlclwiLCBmYWxzZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICB6b25lUm9vdC5zZWxlY3RBbGwoXCJnXCIpLnNlbGVjdEFsbChcInJlY3RcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoZCkge1xuICAgICAgICAgc2VsZi5jbG9zZVBvcHVwcygpO1xuICAgICAgfSk7XG5cbiAgfVxuXG5cbiAgcmVmcmVzaFNpemUoKSB7XG4gICAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAgIGNvbnN0IGVsID0gdGhpcy5kb21Ob2RlO1xuICAgICAgbGV0IHZpcyA9IHRoaXMudmlzO1xuXG4gICAgICBjb25zdCBwYXBlcldpZHRoID0gdGhpcy5nZXRDb25maWcoXCJwYXBlcl93aWR0aFwiKTtcbiAgICAgIGNvbnN0IHJhdGlvID0gdGhpcy5nZXRDb25maWcoXCJwcmVmZXJyZWRfcmF0aW9cIik7XG4gICAgICBjb25zdCBmaXhlZFdpZHRoID0gdGhpcy5nZXRDb25maWcoXCJmaXhlZF93aWR0aFwiKTtcblxuICAgICAgY29uc3Qgb3cgPSAoZml4ZWRXaWR0aCA9PSBmYWxzZSA/IGVsLnBhcmVudE5vZGUuY2xpZW50V2lkdGggOiBwYXBlcldpZHRoKTtcbiAgICAgIGNvbnN0IG9oID0gKHJhdGlvID09IDAgPyBlbC5wYXJlbnROb2RlLmNsaWVudEhlaWdodDpvdypyYXRpbyk7IC8vZWwucGFyZW50Tm9kZS5jbGllbnRIZWlnaHQ7IC8vb3cgKiByYXRpbztcblxuXG4gICAgICBjb25zb2xlLmxvZyhcInJlZnJlc2hTaXplKCkgOiBTVkcgW1cgPSBcIitvdyArXCIsIEggPSBcIiArIG9oICsgXCJdIDo6IFBhcmVudCBjbGllbnRIZWlnaHQ9XCIrICBlbC5wYXJlbnROb2RlLmNsaWVudEhlaWdodCk7XG4gICAgICBjb25zb2xlLmxvZyhcInJlZnJlc2hTaXplKCkgOiBCb2R5OiBXID0gXCIrIGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggK1wiLCBIID0gXCIgKyBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodCk7XG5cbiAgICAgIHZpc1xuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgb3cpXG4gICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgb2gpO1xuXG4gICAgICBkMy5zZWxlY3QodGhpcy5kb21Ob2RlLnBhcmVudE5vZGUpLnN0eWxlKFwiYmFja2dyb3VuZC1jb2xvclwiLCB0aGlzLmdldENvbmZpZyhcImJhY2tncm91bmRfY29sb3JcIikpO1xuXG5cbiAgICAgIC8vIHNjYWxlIHRoZSB2aWV3Ym94IHRvIG1heGltaXplIHRoZSB6b29tXG4gICAgICAvLyBBZGp1c3QgdGhlIGhlaWdodCB0byBub3RoaW5nIGxhcmdlciB0aGFuIHdoYXQgdGhlIHpvbmVzIGFyZSB1c2luZ1xuICAgICAgdmlzLnNlbGVjdChcImcuem9uZXNcIikuZWFjaChmdW5jdGlvbiAoZCkge1xuXG4gICAgICAgICAgLy8gaWYgcmF0aW8gaXMgMCwgdGhlbiBzZXQgdGhlIGhlaWdodCB0byB0aGUgaGVpZ2h0IG9mIGFsbCB6b25lc1xuICAgICAgICAgIC8vXG4gICAgICAgICAgbGV0IGJib3ggPSB0aGlzLmdldEJCb3goKTtcbiAgICAgICAgICBsZXQgcGFyZW50QmJveCA9IHRoaXMucGFyZW50Tm9kZS5nZXRCQm94KCk7XG5cbiAgICAgICAgICBpZiAocmF0aW8gIT0gMCkge1xuICAgICAgICAgICAgICBsZXQgcmVjdCA9IFswLCAwLCBwYXBlcldpZHRoLCBwYXBlcldpZHRoICogcmF0aW9dO1xuICAgICAgICAgICAgICBsZXQgem9uZSA9IFwiXCIgKyAocmVjdFswXSkgKyBcIiBcIiArIHJlY3RbMV0gKyBcIiBcIiArIHJlY3RbMl0gKyBcIiBcIiArIHJlY3RbM107XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU2V0dGluZyBWaWV3Qm94IChoYXMgcmF0aW8pIHRvIFwiICsgem9uZSk7XG4gICAgICAgICAgICAgIHZpcy5hdHRyKFwidmlld0JveFwiLCB6b25lKTtcbiAgICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgc2VsZi56b25lcy5mb3JFYWNoKGZ1bmN0aW9uICh6b25lLCB6b25lSW5kZXgpIHtcblxuICAgICAgICBsZXQgbGF5b3V0ID0gTGF5b3V0RmFjdG9yeS5nZXRMYXlvdXQoem9uZS50eXBlKTtcbiAgICAgICAgaWYgKHpvbmUudHlwZSA9PSBcInRhYnNcIiB8fCB6b25lLnR5cGUgPT0gXCJkZWZhdWx0XCIgfHwgem9uZS50eXBlID09IFwiY2Fyb3VzZWxcIikge1xuICAgICAgICAgICAgbGF5b3V0LnJlZnJlc2hTaXplKHNlbGYsIHpvbmUsIHZpcy5zZWxlY3QoXCJnLnpvbmVzXCIpLmZpbHRlcihmdW5jdGlvbiAoZCwgaSkgeyByZXR1cm4gaSA9PSB6b25lSW5kZXg7IH0pKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIHZpcy5zZWxlY3QoXCJnLnpvbmVzXCIpLmVhY2goZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgLy8gaWYgcmF0aW8gaXMgMCwgdGhlbiBzZXQgdGhlIGhlaWdodCB0byB0aGUgaGVpZ2h0IG9mIGFsbCB6b25lc1xuICAgICAgICAgIC8vXG4gICAgICAgICAgbGV0IGJib3ggPSB0aGlzLmdldEJCb3goKTtcbiAgICAgICAgICBsZXQgcGFyZW50QmJveCA9IHRoaXMucGFyZW50Tm9kZS5nZXRCQm94KCk7XG5cbiAgICAgICAgICBpZiAocmF0aW8gPT0gMCkge1xuICAgICAgICAgICAgICBsZXQgcmVjdCA9IFswLCAwLCBwYXBlcldpZHRoLCAoYmJveC5oZWlnaHQpXTtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJyZWZyZXNoU2l6ZSgpIDogQkJPWDogVyA9IFwiKyBiYm94LndpZHRoICtcIiwgSCA9IFwiICsgYmJveC5oZWlnaHQpO1xuXG4gICAgICAgICAgICAgIGxldCB6b25lID0gXCJcIiArIChyZWN0WzBdKSArIFwiIFwiICsgcmVjdFsxXSArIFwiIFwiICsgcmVjdFsyXSArIFwiIFwiICsgcmVjdFszXTtcblxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNldHRpbmcgVmlld0JveCAocmF0aW89MCkgdG8gXCIgKyB6b25lKTtcbiAgICAgICAgICAgICAgdmlzLmF0dHIoXCJ2aWV3Qm94XCIsIHpvbmUpO1xuICAgICAgICAgIH1cbiAgICAgIH0pO1xuXG5cbi8vXG4vLyAgICAgIGxldCByZWN0ID0gWzAsIDAsIG93LCBvdyAqIHJhdGlvXTtcbi8vXG4vLyAgICAgIGNvbnN0IHpvbmUgPSBcIlwiICsgKHJlY3RbMF0pICsgXCIgXCIgKyByZWN0WzFdICsgXCIgXCIgKyByZWN0WzJdICsgXCIgXCIgKyByZWN0WzNdO1xuLy9cbi8vICAgICAgdmlzLmF0dHIoXCJwcmVzZXJ2ZUFzcGVjdFJhdGlvXCIsIFwieE1pbllNaW5cIilcbiAgICAgIC8vdmlzLmF0dHIoXCJ2aWV3Qm94XCIsIHpvbmUpOyAvLyBcIjAgMCAxMDAwIDgwMFwiXG4gIH1cblxuICBnZXREaW0oZCkge1xuICAgICAgY29uc3QgaW5kZXggPSBkLmJvdW5kYXJ5LmNoYXJDb2RlQXQoMCkgLSA2NTtcbiAgICAgIGlmICh0aGlzLnpvbmVzLmxlbmd0aCA8PSBpbmRleCkge1xuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJJTlZBTElEIEJPVU5EQVJZOiBcIiArIGQuYm91bmRhcnkpO1xuICAgICAgfVxuICAgICAgY29uc3QgcmVjdCA9ICh0aGlzLnpvbmVzW2luZGV4XS5oYXNPd25Qcm9wZXJ0eSgnY2FsY3VsYXRlZFJlY3RhbmdsZScpID8gdGhpcy56b25lc1tpbmRleF0uY2FsY3VsYXRlZFJlY3RhbmdsZSA6IHRoaXMuem9uZXNbaW5kZXhdLnJlY3RhbmdsZSk7XG4gICAgICBjb25zdCBkaW0gPSB7eDE6cmVjdFswXSx5MTpyZWN0WzFdLHgyOnJlY3RbMF0gKyByZWN0WzJdLHkyOnJlY3RbMV0gKyByZWN0WzNdfTtcbiAgICAgIC8vY29uc29sZS5sb2coXCJLOiBcIiArIEpTT04uc3RyaW5naWZ5KGRpbSwgbnVsbCwgMikpO1xuICAgICAgcmV0dXJuIGRpbTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgICAgLy9jb25zb2xlLmxvZyhcIkNMRUFSSU5HXCIpO1xuICAgICAgdGhpcy52aXMuc2VsZWN0KFwiZy5ub2RlU2V0XCIpLnNlbGVjdEFsbChcImcubm9kZVwiKS5yZW1vdmUoKTtcbiAgfVxuXG4gIGdldENvbmZpZyhjKSB7XG4gICAgICBsZXQgY29uZmlnID0gKHRoaXMuc3RhdGUgPyB0aGlzLnN0YXRlLmNvbmZpZyA6IG51bGwpO1xuICAgICAgaWYgKGNvbmZpZyAmJiBjb25maWcuaGFzT3duUHJvcGVydHkoYykpIHtcbiAgICAgICAgcmV0dXJuIGNvbmZpZ1tjXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRDb25maWdbY107XG4gICAgICB9XG4gIH1cblxuICB0cmF2ZXJzZUNoaWxkcmVuIChuZXdOb2Rlcykge1xuICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgICBjb25zdCB2aXMgPSB0aGlzLnN2ZztcblxuICAgICAgbmV3Tm9kZXMuZWFjaChmdW5jdGlvbiAoZCkge1xuXG4gICAgICAgICAgaWYgKGQuY2hpbGRyZW4pIHtcblxuICAgICAgICAgICAgICBsZXQgbm9kZVNldCA9IGQzLnNlbGVjdCh0aGlzLnBhcmVudE5vZGUpLnNlbGVjdEFsbChcImcubm9kZV9cIiArIGQubmFtZSlcbiAgICAgICAgICAgICAgICAgIC5kYXRhKGQuY2hpbGRyZW4pO1xuXG4gICAgICAgICAgICAgIGxldCBuZXdOb2Rlc0MgPSBub2RlU2V0LmVudGVyKCkuYXBwZW5kKFwiZ1wiKVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJub2RlX1wiICsgZC5uYW1lICsgXCIgbm9kZWNcIik7XG5cbiAgICAgICAgICAgICAgbGV0IG5hbWVzID0gU2hhcGVGYWN0b3J5LmdldFNoYXBlTmFtZXMoKTtcbiAgICAgICAgICAgICAgZm9yICggbGV0IG5tIGluIG5hbWVzKSB7XG4gICAgICAgICAgICAgICAgICBsZXQgc2hhcCA9IFNoYXBlRmFjdG9yeS5nZXRTaGFwZShuYW1lc1tubV0pO1xuICAgICAgICAgICAgICAgICAgc2hhcC5idWlsZChuZXdOb2Rlc0MuZmlsdGVyKGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQudHlwZSA9PSBuYW1lc1tubV07IH0pLmNsYXNzZWQoXCJfXCIgKyBuYW1lc1tubV0sIHRydWUpKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIG5ld05vZGVzQ1xuICAgICAgICAgICAgICAgICAgLmF0dHIoXCJjeFwiLCBmdW5jdGlvbihkKSB7IGNvbnN0IGRpbSA9IHNlbGYuZ2V0RGltKGQpOyBjb25zdCB4ID0gKGQueCA/IGQueCA6IChkaW0ueDEgKyAoZGltLngyLWRpbS54MSkvMikpOyBkLnggPSB4OyBkLmZ4ID0oZC5maXhlZCA/IHg6bnVsbCk7IH0pXG4gICAgICAgICAgICAgICAgICAuYXR0cihcImN5XCIsIGZ1bmN0aW9uKGQpIHsgY29uc3QgZGltID0gc2VsZi5nZXREaW0oZCk7IGNvbnN0IHkgPSAoZC55ID8gZC55IDogKGRpbS55MSArIChkaW0ueTItZGltLnkxKS8yKSk7IGQueSA9IHk7IGQuZnkgPShkLmZpeGVkID8geTpudWxsKTsgfSk7XG5cbiAgICAgICAgICAgICAgc2VsZi56b25lcy5mb3JFYWNoKGZ1bmN0aW9uICh6b25lLCBpbmRleCkge1xuICAgICAgICAgICAgICAgIGxldCBsYXlvdXQgPSBMYXlvdXRGYWN0b3J5LmdldExheW91dCh6b25lLnR5cGUpO1xuICAgICAgICAgICAgICAgIGxldCBmaWx0ZXJlZCA9IG5ld05vZGVzQy5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgY29uc3QgaSA9IGQuYm91bmRhcnkuY2hhckNvZGVBdCgwKSAtIDY1OyByZXR1cm4gaW5kZXggPT0gaTsgfSk7XG4gICAgICAgICAgICAgICAgbGF5b3V0LmNvbmZpZ0V2ZW50cyh2aXMsIGZpbHRlcmVkLCBzZWxmLnpvbmVzLCBzZWxmKTtcbiAgICAgICAgICAgICAgfSk7XG5cbi8vICAgICAgICAgICAgICAvLyBBdHRhY2ggRXZlbnRzXG4vLyAgICAgICAgICAgICAgc2VsZi56b25lcy5mb3JFYWNoKGZ1bmN0aW9uICh6b25lLCBpbmRleCkge1xuLy8gICAgICAgICAgICAgICAgbGV0IGxheW91dCA9IExheW91dEZhY3RvcnkuZ2V0TGF5b3V0KHpvbmUudHlwZSk7XG4vLyAgICAgICAgICAgICAgICAvL2xheW91dC5jb25maWdFdmVudHModmlzLCBuZXdOb2Rlc0MsIHNlbGYuem9uZXMsIHNlbGYpO1xuLy8gICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgIHNlbGYudHJhdmVyc2VDaGlsZHJlbiAobmV3Tm9kZXNDKTtcbiAgICAgICAgICB9XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZSgpIHtcbiAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgICAgY29uc3QgdmlzID0gdGhpcy52aXM7XG5cbiAgICAgIGNvbnN0IGZvcmNlID0gdGhpcy5mb3JjZTtcblxuICAgICAgc2VsZi5yZWdpc3RlckxpbmtzKCk7XG4gICAgICBzZWxmLnJlZnJlc2hab25lcyAodGhpcy5zdGF0ZS56b25lcywge30pO1xuXG4gICAgICBjb25zdCBsaW5rSW5mbyA9IHZpcy5zZWxlY3QoXCJnLmFsbF9saW5lc1wiKVxuICAgICAgICAgICAgICAgIC5zZWxlY3RBbGwoXCJnXCIpXG4gICAgICAgICAgICAgICAgLmRhdGEodGhpcy5zdGF0ZS5saW5rcyk7XG5cbiAgICAgIGNvbnN0IGxpbmtHcm91cCA9IGxpbmtJbmZvLmVudGVyKClcbiAgICAgICAgLmluc2VydChcImdcIilcbiAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gKGQuaGFzT3duUHJvcGVydHkoXCJjbGFzc1wiKSA/IFwiX1wiK2QudHlwZSArIFwiIF9cIiArIGQuY2xhc3MgOiBcIl9cIiArIGQudHlwZSk7IH0pO1xuXG4gICAgICBsaW5rR3JvdXAuYXBwZW5kKFwicGF0aFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJsaW5rXCIpO1xuXG4gICAgICBsaW5rR3JvdXAuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmVuZHM7IH0pLmFwcGVuZChcImNpcmNsZVwiKVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiclwiLCA1KVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiZmlsbFwiLCBcIiMyOThFRkVcIilcbiAgICAgICAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwiY2lyY2xlU291cmNlXCIpO1xuXG4gICAgICBsaW5rR3JvdXAuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmVuZHM7IH0pLmFwcGVuZChcImNpcmNsZVwiKVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiclwiLCA1KVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiZmlsbFwiLCBcIiMyOThFRkVcIilcbiAgICAgICAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwiY2lyY2xlVGFyZ2V0XCIpO1xuXG4gICAgICBsaW5rR3JvdXAuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJsZWZ0XCIpXG4gICAgICAgICAgICAgICAgLnRleHQoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIFwiTGluayBUZXh0XCI7IH0pXG4gICAgICAgICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImxpbmtUZXh0XCIpO1xuXG5cbiAgICAgIGxpbmtJbmZvLmV4aXQoKS5yZW1vdmUoKTtcblxuICAgICAgY29uc3QgbGlua3MgPSBsaW5rR3JvdXAuc2VsZWN0QWxsKFwicGF0aFwiKTtcblxuICAgICAgbGV0IG5vZGVTZXQgPSB2aXMuc2VsZWN0KFwiZy5ub2RlU2V0XCIpLnNlbGVjdEFsbChcImcubm9kZVwiKVxuICAgICAgICAgIC5kYXRhKHRoaXMuc3RhdGUubm9kZXMpO1xuXG4gICAgICBjb25zdCBuZXdOb2RlcyA9IG5vZGVTZXQuZW50ZXIoKS5hcHBlbmQoXCJnXCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcIm5vZGVcIik7XG5cbiAgICAgIGxldCBuYW1lcyA9IFNoYXBlRmFjdG9yeS5nZXRTaGFwZU5hbWVzKCk7XG4gICAgICBmb3IgKCBsZXQgbm0gaW4gbmFtZXMpIHtcbiAgICAgICAgICBsZXQgc2hhcCA9IFNoYXBlRmFjdG9yeS5nZXRTaGFwZShuYW1lc1tubV0pO1xuICAgICAgICAgIHNoYXAuYnVpbGQobmV3Tm9kZXMuZmlsdGVyKGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQudHlwZSA9PSBuYW1lc1tubV07IH0pLmNsYXNzZWQoXCJfXCIgKyBuYW1lc1tubV0sIHRydWUpKTtcbiAgICAgIH1cblxuICAgICAgbmV3Tm9kZXNcbiAgICAgICAgICAuYXR0cihcImN4XCIsIGZ1bmN0aW9uKGQpIHsgY29uc3QgZGltID0gc2VsZi5nZXREaW0oZCk7IGNvbnN0IHggPSAoZC54ID8gZC54IDogKGRpbS54MSArIChkaW0ueDItZGltLngxKS8yKSk7IGQueCA9IHg7IGQuZnggPShkLmZpeGVkID8geDpudWxsKTsgfSlcbiAgICAgICAgICAuYXR0cihcImN5XCIsIGZ1bmN0aW9uKGQpIHsgY29uc3QgZGltID0gc2VsZi5nZXREaW0oZCk7IGNvbnN0IHkgPSAoZC55ID8gZC55IDogKGRpbS55MSArIChkaW0ueTItZGltLnkxKS8yKSk7IGQueSA9IHk7IGQuZnkgPShkLmZpeGVkID8geTpudWxsKTsgfSk7XG5cbiAgICAgIC8vIEFkZCBjaGlsZHJlbiAoYXR0YWNoIHRhYiBuYXZpZ2F0aW9uKVxuICAgICAgLy8gQ2FsY3VsYXRlIHRoZSBncm91cGluZyBtZXRhZGF0YSBmb3IgdGhlIGxheW91dFxuICAgICAgLy8gT3JnYW5pemUgdGhlIHRhYiBoaWVyYXJjaHkgYnkgYWRqdXN0aW5nIHRoZSB4LHksd2lkdGggY29vcmRpbmF0ZXNcblxuICAgICAgLy8gR2V0IHRoZSBuYXZpZ2F0aW9uIHRvIHdvcmsgd2l0aCB0aGUgY2hpbGRyZW5cblxuXG4gICAgICB0aGlzLnRyYXZlcnNlQ2hpbGRyZW4obmV3Tm9kZXMpO1xuXG5cbiAgICAgIHRoaXMuem9uZXMuZm9yRWFjaChmdW5jdGlvbiAoem9uZSwgaW5kZXgpIHtcbiAgICAgICAgbGV0IGxheW91dCA9IExheW91dEZhY3RvcnkuZ2V0TGF5b3V0KHpvbmUudHlwZSk7XG4gICAgICAgIGxheW91dC5lbnJpY2hEYXRhKHNlbGYuc3RhdGUubm9kZXMpO1xuICAgICAgfSk7XG5cblxuICAgICAgdGhpcy56b25lcy5mb3JFYWNoKGZ1bmN0aW9uICh6b25lLCBpbmRleCkge1xuICAgICAgICBsZXQgbGF5b3V0ID0gTGF5b3V0RmFjdG9yeS5nZXRMYXlvdXQoem9uZS50eXBlKTtcbiAgICAgICAgbGV0IGZpbHRlcmVkID0gbmV3Tm9kZXMuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IGNvbnN0IGkgPSBkLmJvdW5kYXJ5LmNoYXJDb2RlQXQoMCkgLSA2NTsgcmV0dXJuIGluZGV4ID09IGk7IH0pO1xuICAgICAgICBsYXlvdXQuY29uZmlnRXZlbnRzKHZpcywgZmlsdGVyZWQsIHNlbGYuem9uZXMsIHNlbGYpO1xuICAgICAgfSk7XG5cbiAgICAgIC8vIE9yZ2FuaXplOlxuICAgICAgdGhpcy56b25lcy5mb3JFYWNoKGZ1bmN0aW9uICh6b25lLCBpbmRleCkge1xuICAgICAgICBsZXQgbGF5b3V0ID0gTGF5b3V0RmFjdG9yeS5nZXRMYXlvdXQoem9uZS50eXBlKTtcbiAgICAgICAgbGF5b3V0Lm9yZ2FuaXplKHZpcywgaW5kZXgsIHNlbGYuem9uZXMpO1xuICAgICAgfSk7XG5cblxuXG4gICAgICBub2RlU2V0LmV4aXQoKS5yZW1vdmUoKTtcblxuICAgICAgbm9kZVNldCA9IHZpcy5zZWxlY3QoXCJnLm5vZGVTZXRcIikuc2VsZWN0QWxsKFwiZy5ub2RlYywgZy5ub2RlXCIpXG5cbiAgICAgIGxldCB0aWNrZWQgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICAgIGNvbnN0IHJhZGl1cyA9IDIwO1xuICAgICAgICAgIGNvbnN0IHcgPSB2aXMuYXR0cihcIndpZHRoXCIpO1xuICAgICAgICAgIGNvbnN0IGggPSB2aXMuYXR0cihcImhlaWdodFwiKTtcblxuICAgICAgICAgIC8vIGVuZm9yY2Ugem9uZSBib3VuZGFyaWVzXG4gICAgICAgICAgaWYgKHNlbGYuZ2V0Q29uZmlnKCdlbmFibGVfem9uZV9ib3VuZGFyaWVzJykpIHtcbiAgICAgICAgICAgIG5vZGVTZXRcbiAgICAgICAgICAgICAgICAuYXR0cihcImN4XCIsIGZ1bmN0aW9uKGQpIHsgY29uc3QgZGltID0gc2VsZi5nZXREaW0oZCk7IGQueCA9IE1hdGgubWF4KGRpbS54MSArIHJhZGl1cywgTWF0aC5taW4oZGltLngyIC0gcmFkaXVzLCBkLngpKTsgcmV0dXJuIGQueDt9KVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiY3lcIiwgZnVuY3Rpb24oZCkgeyBjb25zdCBkaW0gPSBzZWxmLmdldERpbShkKTsgZC55ID0gTWF0aC5tYXgoZGltLnkxICsgcmFkaXVzLCBNYXRoLm1pbihkaW0ueTIgLSByYWRpdXMsIGQueSkpOyByZXR1cm4gZC55O30pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChzZWxmLmdldENvbmZpZygnZW5hYmxlX2NvbGxpc2lvbl9hdm9pZGFuY2UnKSkge1xuICAgICAgICAgICAgbm9kZVNldC5lYWNoKHNlbGYuY29sbGlkZSgwLjUpKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBub2RlU2V0XG4gICAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBcInRyYW5zbGF0ZShcIiArIGQueCArIFwiLFwiICsgZC55ICsgXCIpXCI7XG4gICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgbGV0IGxuID0gZDMubGluZSgpXG4gICAgICAgICAgICAgIC54KGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLng7IH0pXG4gICAgICAgICAgICAgIC55KGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLnk7IH0pXG4gICAgICAgICAgICAgIC5jdXJ2ZShkMy5jdXJ2ZUxpbmVhcik7IC8vIHN0ZXAsIGxpbmVhclxuXG4gICAgICAgICAgbGV0IHN0ZXBCZWZvcmUgPSBkMy5saW5lKClcbiAgICAgICAgICAgICAgLngoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQueDsgfSlcbiAgICAgICAgICAgICAgLnkoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQueTsgfSlcbiAgICAgICAgICAgICAgLmN1cnZlKGQzLmN1cnZlU3RlcEJlZm9yZSk7IC8vIHN0ZXAsIHN0ZXAtYWZ0ZXIsIHN0ZXBcblxuICAgICAgICAgIGxldCBzdGVwQWZ0ZXIgPSBkMy5saW5lKClcbiAgICAgICAgICAgICAgLngoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQueDsgfSlcbiAgICAgICAgICAgICAgLnkoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQueTsgfSlcbiAgICAgICAgICAgICAgLmN1cnZlKGQzLmN1cnZlU3RlcEFmdGVyKTsgLy8gc3RlcCwgc3RlcC1hZnRlciwgc3RlcFxuXG4gICAgICAgICAgbGV0IGNhcmRpbmFsID0gZDMubGluZSgpXG4gICAgICAgICAgICAgIC54KGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLng7IH0pXG4gICAgICAgICAgICAgIC55KGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLnk7IH0pXG4gICAgICAgICAgICAgIC5jdXJ2ZShkMy5jdXJ2ZUNhcmRpbmFsKTsgLy8gc3RlcCwgc3RlcC1hZnRlciwgc3RlcFxuXG5cbiAgICAgICAgICBmdW5jdGlvbiBsYXlvdXQgKGQpIHtcbiAgICAgICAgICAgICAgbGV0IGxheSA9IFwic291dGhcIjtcbiAgICAgICAgICAgICAgbGV0IHAxID0gZC5nZXRQb2ludEF0TGVuZ3RoKDApO1xuICAgICAgICAgICAgICBsZXQgcDIgPSBkLmdldFBvaW50QXRMZW5ndGgoZC5nZXRUb3RhbExlbmd0aCgpIC8gMik7XG4gICAgICAgICAgICAgIGxldCBhbmdsZSA9IE1hdGguYXRhbjIocDIueSAtIHAxLnksIHAyLnggLSBwMS54KSAqIDE4MCAvIE1hdGguUEk7XG4gICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJBbnN3ZXIgPSBcIiArIGFuZ2xlKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgLmF0dHIoXCJkXCIsIGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICAgIGxldCBzb3VyY2VQb2ludHMgPSB7XCJ4XCI6ZC5zb3VyY2UueCwgXCJ5XCI6ZC5zb3VyY2UueX07XG4gICAgICAgICAgICAgIGxldCB0YXJnZXRQb2ludHMgPSB7XCJ4XCI6ZC50YXJnZXQueCwgXCJ5XCI6ZC50YXJnZXQueX07XG4gICAgICAgICAgICAgIGxldCBhID0gMDtcblxuICAgICAgICAgICAgICAvLyBTb3VyY2VcbiAgICAgICAgICAgICAgaWYgKGQuaGFzT3duUHJvcGVydHkoJ2VuZHMnKSkge1xuICAgICAgICAgICAgICAgICAgbGV0IGRlbHRhWCA9IDA7XG4gICAgICAgICAgICAgICAgICBsZXQgZGVsdGFZID0gMDtcblxuICAgICAgICAgICAgICAgICAgLy8gRmluZCB0aGUgc291cmNlIG5vZGUgYW5kIGJhc2VkIG9uIGl0cyBib3VuZGluZyBib3gsIHNldCB0aGUgcG9zaXRpb25cblxuICAgICAgICAgICAgICAgICAgbGV0IHMxID0gbm9kZVNldC5maWx0ZXIoZnVuY3Rpb24gKGluZiwgaW5kKSB7IHJldHVybiBkLnNvdXJjZS5pbmRleCA9PSBpbmQ7IH0pO1xuICAgICAgICAgICAgICAgICAgbGV0IHQxID0gbm9kZVNldC5maWx0ZXIoZnVuY3Rpb24gKGluZiwgaW5kKSB7IHJldHVybiBkLnRhcmdldC5pbmRleCA9PSBpbmQ7IH0pO1xuXG4gICAgICAgICAgICAgICAgICBsZXQgc291cmNlRW5kID0gZC5lbmRzLnN1YnN0cmluZygwLDEpO1xuICAgICAgICAgICAgICAgICAgbGV0IHRhcmdldEVuZCA9IGQuZW5kcy5zdWJzdHJpbmcoMiwzKTtcblxuICAgICAgICAgICAgICAgICAgaWYgKGQuZW5kcyA9PSBcImF1dG9cIiAmJiAoZC50eXBlID09IFwic3RyYWlnaHRcIiB8fCBkLnR5cGUgPT0gXCJzdGVwLWJlZm9yZVwiKSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoc291cmNlUG9pbnRzLnggPCB0YXJnZXRQb2ludHMueCAmJiBzb3VyY2VQb2ludHMueSA8IHRhcmdldFBvaW50cy55KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzb3VyY2VFbmQgPSBcIlNcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldEVuZCA9IFwiV1wiO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHNvdXJjZVBvaW50cy54ID49IHRhcmdldFBvaW50cy54ICYmIHNvdXJjZVBvaW50cy55IDwgdGFyZ2V0UG9pbnRzLnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZUVuZCA9IFwiU1wiO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0RW5kID0gXCJFXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc291cmNlUG9pbnRzLnggPj0gdGFyZ2V0UG9pbnRzLnggJiYgc291cmNlUG9pbnRzLnkgPj0gdGFyZ2V0UG9pbnRzLnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZUVuZCA9IFwiTlwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0RW5kID0gXCJFXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzb3VyY2VFbmQgPSBcIk5cIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldEVuZCA9IFwiV1wiO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgIGlmIChkLmVuZHMgPT0gXCJhdXRvXCIgJiYgZC50eXBlID09IFwic3RlcC1hZnRlclwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzb3VyY2VQb2ludHMueCA8IHRhcmdldFBvaW50cy54ICYmIHNvdXJjZVBvaW50cy55IDwgdGFyZ2V0UG9pbnRzLnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZUVuZCA9IFwiRVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0RW5kID0gXCJOXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc291cmNlUG9pbnRzLnggPj0gdGFyZ2V0UG9pbnRzLnggJiYgc291cmNlUG9pbnRzLnkgPCB0YXJnZXRQb2ludHMueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc291cmNlRW5kID0gXCJXXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRFbmQgPSBcIk5cIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzb3VyY2VQb2ludHMueCA+PSB0YXJnZXRQb2ludHMueCAmJiBzb3VyY2VQb2ludHMueSA+PSB0YXJnZXRQb2ludHMueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc291cmNlRW5kID0gXCJXXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRFbmQgPSBcIlNcIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZUVuZCA9IFwiRVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0RW5kID0gXCJTXCI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgczEuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKHNvdXJjZUVuZCA9PSBcIldcIikgeyBkZWx0YVggPSAtdGhpcy5nZXRCQm94KCkud2lkdGgvMjsgfVxuICAgICAgICAgICAgICAgICAgICAgIGlmIChzb3VyY2VFbmQgPT0gXCJFXCIpIHsgZGVsdGFYID0gdGhpcy5nZXRCQm94KCkud2lkdGgvMjsgfVxuICAgICAgICAgICAgICAgICAgICAgIGlmIChzb3VyY2VFbmQgPT0gXCJOXCIpIHsgZGVsdGFZID0gLXRoaXMuZ2V0QkJveCgpLmhlaWdodC8yOyB9XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKHNvdXJjZUVuZCA9PSBcIlNcIikgeyBkZWx0YVkgPSB0aGlzLmdldEJCb3goKS5oZWlnaHQvMjsgfVxuICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgIHNvdXJjZVBvaW50cyA9IHsgXCJ4XCI6ZC5zb3VyY2UueCArIGRlbHRhWCwgXCJ5XCI6ZC5zb3VyY2UueSArIGRlbHRhWSB9O1xuXG4gICAgICAgICAgICAgICAgICBkZWx0YVggPSAwO1xuICAgICAgICAgICAgICAgICAgZGVsdGFZID0gMDtcbiAgICAgICAgICAgICAgICAgIHQxLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgIGlmICh0YXJnZXRFbmQgPT0gXCJXXCIpIHsgZGVsdGFYID0gLXRoaXMuZ2V0QkJveCgpLndpZHRoLzI7IH1cbiAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0RW5kID09IFwiRVwiKSB7IGRlbHRhWCA9IHRoaXMuZ2V0QkJveCgpLndpZHRoLzI7IH1cbiAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0RW5kID09IFwiTlwiKSB7IGRlbHRhWSA9IC10aGlzLmdldEJCb3goKS5oZWlnaHQvMjsgfVxuICAgICAgICAgICAgICAgICAgICAgIGlmICh0YXJnZXRFbmQgPT0gXCJTXCIpIHsgZGVsdGFZID0gdGhpcy5nZXRCQm94KCkuaGVpZ2h0LzI7IH1cbiAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICB0YXJnZXRQb2ludHMgPSB7IFwieFwiOmQudGFyZ2V0LnggKyBkZWx0YVgsIFwieVwiOmQudGFyZ2V0LnkgKyBkZWx0YVkgfTtcblxuICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICBjb25zdCBsaW5lRGF0YSA9IFtzb3VyY2VQb2ludHMsIHRhcmdldFBvaW50c107XG4gICAgICAgICAgICAgIGlmIChkLnR5cGUgPT0gXCJzdHJhaWdodFwiIHx8IGQudHlwZSA9PSBcInN0ZXAtYmVmb3JlXCIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gc3RlcEJlZm9yZShsaW5lRGF0YSk7XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZC50eXBlID09IFwic3RlcC1hZnRlclwiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN0ZXBBZnRlcihsaW5lRGF0YSk7XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZC50eXBlID09IFwiY2FyZGluYWxcIikge1xuICAgICAgICAgICAgICAgIHJldHVybiBjYXJkaW5hbChsaW5lRGF0YSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGxuKGxpbmVEYXRhKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgLy8gZGlzcGxheSB0ZXh0XG4gICAgICAgICAgbGlua3MuZWFjaCAoZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiVG90YWwgbGVuZ3RoID0gXCIgKyB0aGlzLmdldFRvdGFsTGVuZ3RoKCkpO1xuICAgICAgICAgICAgICAgIGxldCBsYWJlbFBvc2l0aW9uID0gNTA7XG4gICAgICAgICAgICAgICAgaWYgKGQuaGFzT3duUHJvcGVydHkoXCJsYWJlbFBvc2l0aW9uXCIpKSB7XG4gICAgICAgICAgICAgICAgICBsYWJlbFBvc2l0aW9uID0gZC5sYWJlbFBvc2l0aW9uO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsZXQgcG9pbnQgPSB0aGlzLmdldFBvaW50QXRMZW5ndGgodGhpcy5nZXRUb3RhbExlbmd0aCgpICogKGxhYmVsUG9zaXRpb24vMTAwKSk7XG4gICAgICAgICAgICAgICAgbGV0IGxhYmVsID0gZDMuc2VsZWN0KHRoaXMucGFyZW50Tm9kZSkuc2VsZWN0KFwidGV4dFwiKTtcbiAgICAgICAgICAgICAgICBsYWJlbC50ZXh0KGZ1bmN0aW9uIChsZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG4gICAgICAgICAgICAgICAgbGFiZWwuYXR0cihcInRyYW5zZm9ybVwiLCBcInRyYW5zbGF0ZShcIiArIChwb2ludC54IC0gNSkrXCIsXCIrKHBvaW50LnktNSkrXCIpXCIpO1xuICAgICAgICAgICAgICAgIGxldCBjaXJjbGVTb3VyY2UgPSBkMy5zZWxlY3QodGhpcy5wYXJlbnROb2RlKS5zZWxlY3QoXCJjaXJjbGUuY2lyY2xlU291cmNlXCIpO1xuICAgICAgICAgICAgICAgIGxldCBzb3VyY2VQb2ludCA9IHRoaXMuZ2V0UG9pbnRBdExlbmd0aCgwKTtcbiAgICAgICAgICAgICAgICBjaXJjbGVTb3VyY2UuYXR0cihcInRyYW5zZm9ybVwiLCBcInRyYW5zbGF0ZShcIiArIHNvdXJjZVBvaW50LngrXCIsXCIrc291cmNlUG9pbnQueStcIilcIik7XG4gICAgICAgICAgICAgICAgbGV0IGNpcmNsZVRhcmdldCA9IGQzLnNlbGVjdCh0aGlzLnBhcmVudE5vZGUpLnNlbGVjdChcImNpcmNsZS5jaXJjbGVUYXJnZXRcIik7XG4gICAgICAgICAgICAgICAgbGV0IHRhcmdldFBvaW50ID0gdGhpcy5nZXRQb2ludEF0TGVuZ3RoKHRoaXMuZ2V0VG90YWxMZW5ndGgoKS0wKTtcbiAgICAgICAgICAgICAgICBjaXJjbGVUYXJnZXQuYXR0cihcInRyYW5zZm9ybVwiLCBcInRyYW5zbGF0ZShcIiArIHRhcmdldFBvaW50LngrXCIsXCIrdGFyZ2V0UG9pbnQueStcIilcIik7XG4gICAgICAgICAgICAgICAgLy9sYXlvdXQgKHRoaXMpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICB9O1xuXG4gICAgICAvLyBSZXN0YXJ0IHRoZSBmb3JjZSBsYXlvdXRcbiAgICAgIGxldCBub2RlTGlzdCA9IFtdO1xuICAgICAgZm9yIChsZXQgYSBpbiB0aGlzLnN0YXRlLm5vZGVzKSB7IG5vZGVMaXN0LnB1c2godGhpcy5zdGF0ZS5ub2Rlc1thXSk7IH07XG5cbiAgICAgIGZvciAoIGxldCBpID0gMCA7IGkgPCB0aGlzLnN0YXRlLm5vZGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgaWYgKHRoaXMuc3RhdGUubm9kZXNbaV0uY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgZm9yICggbGV0IGogPSAwIDsgaiA8IHRoaXMuc3RhdGUubm9kZXNbaV0uY2hpbGRyZW4ubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICAgICAgICAgbm9kZUxpc3QucHVzaCh0aGlzLnN0YXRlLm5vZGVzW2ldLmNoaWxkcmVuW2pdKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgfVxuICAgICAgfVxuXG5cbiAgICAgIGZvcmNlXG4gICAgICAgIC5ub2Rlcyhub2RlTGlzdClcbiAgICAgICAgLm9uKFwidGlja1wiLCB0aWNrZWQpO1xuXG4gICAgICBmb3JjZVxuICAgICAgICAuZm9yY2UoXCJsaW5rXCIpLmxpbmtzKHRoaXMuc3RhdGUubGlua3MpO1xuICB9XG5cblxuICBkZXN0cm95KCkge1xuICAgICAgdGhpcy5mb3JjZS5vbihcInRpY2tcIiwgbnVsbCk7XG4gICAgICBkMy5zZWxlY3QodGhpcy5kb21Ob2RlKS5yZW1vdmUoKTtcbiAgICAgIGQzLnNlbGVjdCh0aGlzLmNvbnRlbnROb2RlKS5yZW1vdmUoKTtcbiAgfVxuXG4gIC8vIFJlc29sdmVzIGNvbGxpc2lvbnMgYmV0d2VlbiBkIGFuZCBhbGwgb3RoZXIgbm9kZXMuXG4gIGNvbGxpZGUoYWxwaGEpIHtcblxuXG4gICAgY29uc3QgcGFkZGluZyA9IDEuNSxcbiAgICAgICAgICBtYXhSYWRpdXMgPSAxMjtcbiAgICBjb25zdCBxdWFkdHJlZSA9IGQzLnF1YWR0cmVlKHRoaXMuc3RhdGUubm9kZXMpO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uKGQpIHtcbiAgICAgIGQucmFkaXVzID0gMjU7XG4gICAgICBsZXQgciA9IGQucmFkaXVzICsgbWF4UmFkaXVzICsgcGFkZGluZyxcbiAgICAgICAgICBueDEgPSBkLnggLSByLFxuICAgICAgICAgIG54MiA9IGQueCArIHIsXG4gICAgICAgICAgbnkxID0gZC55IC0gcixcbiAgICAgICAgICBueTIgPSBkLnkgKyByO1xuXG4gICAgICBpZiAoZC54ID09IG51bGwpIHtcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiSWdub3JpbmcuLiBcIiArIGQubmFtZSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgcXVhZHRyZWUudmlzaXQoZnVuY3Rpb24ocXVhZCwgeDEsIHkxLCB4MiwgeTIpIHtcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIk1hdGNoOiBcIiArIHgxK1wiLFwiK3kxK1wiLFwiK3gyK1wiLFwiK3kyICsgXCIgOiBcIiArIHF1YWQucG9pbnQpO1xuICAgICAgICAvL2lmIChxdWFkLmxlYWYgPT0gZmFsc2UpIHtcbiAgICAgICAgLy8gICAgcmV0dXJuIHRydWU7XG4gICAgICAgIC8vfVxuICAgICAgICAvLyBxdWFkLnBvaW50IGhhcyB0aGUgb2JqZWN0IGRldGFpbHNcbiAgICAgICAgaWYgKHF1YWQucG9pbnQgJiYgKHF1YWQucG9pbnQgIT09IGQpKSB7XG5cbiAgICAgICAgICBpZiAocXVhZC5wb2ludC5ib3VuZGFyeSAhPSBkLmJvdW5kYXJ5KSB7XG4gICAgICAgICAgICAgIC8vcmV0dXJuIHgxID4gbngyIHx8IHgyIDwgbngxIHx8IHkxID4gbnkyIHx8IHkyIDwgbnkxO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvL2lmIChkLnggPT0gbnVsbCkge1xuICAgICAgICAgIC8vICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAvL31cbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiRCA9IFwiK0pTT04uc3RyaW5naWZ5KGQsIG51bGwsIDIpKTtcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiUXVhZCA9IFwiK0pTT04uc3RyaW5naWZ5KHF1YWQsIG51bGwsIDIpKTtcbiAgICAgICAgICBxdWFkLnBvaW50LnJhZGl1cyA9IDI1O1xuICAgICAgICAgIGxldCB4ID0gZC54IC0gcXVhZC5wb2ludC54LFxuICAgICAgICAgICAgICB5ID0gZC55IC0gcXVhZC5wb2ludC55LFxuICAgICAgICAgICAgICBsID0gTWF0aC5hYnMoTWF0aC5zcXJ0KHggKiB4ICsgeSAqIHkpKSxcbiAgICAgICAgICAgICAgciA9IGQucmFkaXVzICsgcXVhZC5wb2ludC5yYWRpdXMgKyBwYWRkaW5nO1xuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJYPVwiK3grXCIsWT1cIit5KTtcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiTD1cIitsK1wiLFI9XCIrcik7XG4gICAgICAgICAgaWYgKGwgPCByKSB7XG4gICAgICAgICAgICBsID0gKGwgLSByKSAvIGwgKiBhbHBoYTtcblxuICAgICAgICAgICAgLy8gTmVlZCB0byBtYWtlIHN1cmUgd2UgYXJlIG5vdCBtb3ZpbmcgaXQgb3V0c2lkZSBvZiBib3VuZGFyeVxuICAgICAgICAgICAgeCA9IHggKiBsO1xuICAgICAgICAgICAgeSA9IHkgKiBsO1xuICAgICAgICAgICAgaWYgKGlzTmFOKHgpIHx8IGlzTmFOKHkpKSB7XG4gICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJJTExFR0FMIFZBTFVFIVwiKTtcbiAgICAgICAgICAgICAgeCA9IDAuMDA7XG4gICAgICAgICAgICAgIHkgPSAwLjAxO1xuICAgICAgICAgICAgICAvL3JldHVybiB4MSA+IG54MiB8fCB4MiA8IG54MSB8fCB5MSA+IG55MiB8fCB5MiA8IG55MTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJDaGFuZ2luZy4uLlwiICsgeCtcIiwgXCIreStcIiwgZHggPVwiK2QueCtcIiwgXCIrZC55KTtcbiAgICAgICAgICAgIGQueCA9IGQueCAtIHg7XG4gICAgICAgICAgICBkLnkgPSBkLnkgLSB5O1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkFOU1dFUjogZD1cIitkLngrXCIsIFwiK2QueSk7XG4gICAgICAgICAgICBxdWFkLnBvaW50LnggKz0geDtcbiAgICAgICAgICAgIHF1YWQucG9pbnQueSArPSB5O1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkNoYW5nZSB0bzogXCIgK2QueCtcIiwgXCIrZC55K1wiIDo6IFwiICsgcXVhZC5wb2ludC54K1wiLCBcIitxdWFkLnBvaW50LnkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvL2NvbnNvbGUubG9nKFwiQW5zd2VyOiBcIiAreDEgPiBueDIgfHwgeDIgPCBueDEgfHwgeTEgPiBueTIgfHwgeTIgPCBueTEpO1xuICAgICAgICByZXR1cm4geDEgPiBueDIgfHwgeDIgPCBueDEgfHwgeTEgPiBueTIgfHwgeTIgPCBueTE7XG4gICAgICB9KTtcbiAgICB9O1xuICB9XG5cbiAgYWRkTm9kZShuYW1lLCBmaXhlZCwgdHlwZSwgYm91bmRhcnkpIHtcbiAgICAgIGNvbnN0IG5kID0ge1wibmFtZVwiOm5hbWUsIFwiZml4ZWRcIjooZml4ZWQgPyB0cnVlOmZhbHNlKSwgXCJ0eXBlXCI6dHlwZSwgXCJib3VuZGFyeVwiOmJvdW5kYXJ5IH07XG4gICAgICB0aGlzLnN0YXRlLm5vZGVzLnB1c2gobmQpO1xuICAgICAgdGhpcy51cGRhdGUoKTtcbiAgfVxuXG4gIHJlbW92ZU5vZGUobmFtZSkge1xuXG4gICAgICB0aGlzLnN0YXRlLm5vZGVzID0gdGhpcy5zdGF0ZS5ub2Rlcy5maWx0ZXIoZnVuY3Rpb24obm9kZSkge3JldHVybiAobm9kZVtcIm5hbWVcIl0gIT0gbmFtZSk7IH0pO1xuICAgICAgdGhpcy5zdGF0ZS5saW5rcyA9IHRoaXMuc3RhdGUubGlua3MuZmlsdGVyKGZ1bmN0aW9uKGxpbmspIHtyZXR1cm4gKChsaW5rW1wic291cmNlXCJdW1wibmFtZVwiXSAhPSBuYW1lKSYmKGxpbmtbXCJ0YXJnZXRcIl1bXCJuYW1lXCJdICE9IG5hbWUpKTsgfSk7XG4gICAgICB0aGlzLnVwZGF0ZSgpO1xuICB9XG5cbiAgZmluZE5vZGUobmFtZSkge1xuICAgICAgZm9yIChsZXQgaSBpbiB0aGlzLnN0YXRlLm5vZGVzKSBpZiAodGhpcy5zdGF0ZS5ub2Rlc1tpXVtcIm5hbWVcIl0gPT09IG5hbWUpIHJldHVybiB0aGlzLnN0YXRlLm5vZGVzW2ldO1xuICB9XG5cbiAgcmVnaXN0ZXJMaW5rcyAoKSB7XG4gICAgICBjb25zdCBsaW5rcyA9IHRoaXMuc3RhdGUubGlua3M7XG4gICAgICBmb3IgKGxldCBpID0gMCA7IGkgPCBsaW5rcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgY29uc3QgbGluayA9IGxpbmtzW2ldO1xuICAgICAgICAgaWYgKCh0eXBlb2YgbGluay5zb3VyY2UpID09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICBsaW5rLnNvdXJjZSA9IHRoaXMuZmluZE5vZGUobGluay5zb3VyY2UpO1xuICAgICAgICAgfVxuICAgICAgICAgaWYgKCh0eXBlb2YgbGluay50YXJnZXQpID09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICBsaW5rLnRhcmdldCA9IHRoaXMuZmluZE5vZGUobGluay50YXJnZXQpO1xuICAgICAgICAgfVxuICAgICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGlmICh0aGlzLnN0YXRlKSB7XG4gICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgIH1cbiAgICAvL3ZhciBub2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN2Z1wiKTtcbiAgICAvL3RoaXMuZG9tTm9kZS5hcHBlbmRDaGlsZChub2RlKTtcbiAgfVxufVxuIiwiZXhwb3J0IHZhciBuYW1lID0gXCJpa3NwbG9yXCI7XG5leHBvcnQgdmFyIHZlcnNpb24gPSBcIjAuMC4xMlwiO1xuZXhwb3J0IHZhciBkZXNjcmlwdGlvbiA9IFwiQSBmcmFtZXdvcmsgZm9yIHZpc3VhbGl6aW5nIGFuZCBlZGl0aW5nIG1ldGFkYXRhLlwiO1xuZXhwb3J0IHZhciBtYWluID0gXCJidWlsZC9pa3NwbG9yLmpzXCI7XG5leHBvcnQgdmFyIHNjcmlwdHMgPSB7XCJ0ZXN0XCI6XCJlY2hvIFxcXCJFcnJvcjogbm8gdGVzdCBzcGVjaWZpZWRcXFwiICYmIGV4aXQgMVwiLFwiYnVpbGRcIjpcImJhYmVsIHNyYyAtLXByZXNldHMgYmFiZWwtcHJlc2V0LWVzMjAxNSAtLW91dC1kaXIgZGlzdFwiLFwiT0xEcHJlcHVibGlzaFwiOlwibnBtIHJ1biBidWlsZFwiLFwic3RhcnQtbWVzc2FnZVwiOlwiYmFiZWwtbm9kZSB0b29scy9zdGFydE1lc3NhZ2UuanNcIixcInByZXN0YXJ0XCI6XCJucG0tcnVuLWFsbCAtLXBhcmFsbGVsIHN0YXJ0LW1lc3NhZ2UgcmVtb3ZlLWRpc3QgbGludDp0b29sc1wiLFwibGludDp0b29sc1wiOlwiZXNsaW50IHdlYnBhY2suY29uZmlnLmpzIHRvb2xzXCIsXCJjbGVhbi1kaXN0XCI6XCJucG0gcnVuIHJlbW92ZS1kaXN0ICYmIG1rZGlyIGRpc3RcIixcInJlbW92ZS1kaXN0XCI6XCJub2RlX21vZHVsZXMvLmJpbi9yaW1yYWYgLi9kaXN0XCIsXCJzdGFydFwiOlwibnBtLXJ1bi1hbGwgLS1wYXJhbGxlbCBvcGVuOnNyY1wiLFwib3BlbjpzcmNcIjpcImJhYmVsLW5vZGUgdG9vbHMvc3JjU2VydmVyLmpzXCIsXCJidWlsZGRpc3RcIjpcImJhYmVsLW5vZGUgdG9vbHMvYnVpbGQuanMgXCIsXCJYcHJldGVzdFwiOlwicmltcmFmIGJ1aWxkICYmIG1rZGlyIGJ1aWxkICYmIGpzb24ybW9kdWxlIHBhY2thZ2UuanNvbiA+IGJ1aWxkL3BhY2thZ2UuanMgJiYgbm9kZSByb2xsdXAubm9kZVwiLFwiWHRlc3RcIjpcInRhcGUgJ3Rlc3QvKiovKi10ZXN0LmpzJ1wiLFwiWHByZXB1Ymxpc2hcIjpcImpzb24ybW9kdWxlIHBhY2thZ2UuanNvbiA+IGJ1aWxkL3BhY2thZ2UuanMgJiYgcm9sbHVwIC1jXCIsXCJYZGV2XCI6XCJqc29uMm1vZHVsZSBwYWNrYWdlLmpzb24gPiBidWlsZC9wYWNrYWdlLmpzICYmIHJvbGx1cCAtYyAtLXdhdGNoXCIsXCJwdWJsaXNoXCI6XCJqc29uMm1vZHVsZSBwYWNrYWdlLmpzb24gPiBidWlsZC9wYWNrYWdlLmpzICYmIHJvbGx1cCAtLWNvbmZpZyByb2xsdXAuY29uZmlnLnByb2QuanMgLS1nbG9iYWxzIHJlYWN0OlJlYWN0LGQzOmQzLHJlYWN0LWRvbTpSZWFjdERPTVwiLFwiWWRldlwiOlwicm9sbHVwIC0tY29uZmlnIHJvbGx1cC5jb25maWcuZGV2LmpzIC0td2F0Y2ggLS1nbG9iYWxzIHJlYWN0OlJlYWN0LGQzOmQzLHJlYWN0LWRvbTpSZWFjdERPTVwiLFwiWXJlbG9hZFwiOlwibGl2ZXJlbG9hZCAnYnVpbGQvJ1wiLFwiWXNlcnZlXCI6XCJzZXJ2ZVwiLFwid2F0Y2hcIjpcIm5wbS1ydW4tYWxsIC0tcGFyYWxsZWwgWXJlbG9hZCBZZGV2XCJ9O1xuZXhwb3J0IHZhciBrZXl3b3JkcyA9IFtdO1xuZXhwb3J0IHZhciBhdXRob3IgPSBcIjxpa2V0aGVjb2RlckBjYW56ZWEuY29tPlwiO1xuZXhwb3J0IHZhciBsaWNlbnNlID0gXCJNSVRcIjtcbmV4cG9ydCB2YXIgZGVwZW5kZW5jaWVzID0ge1wiZDNcIjpcIjQuNC4yXCIsXCJmb250YXdlc29tZVwiOlwiXjQuNy4wXCJ9O1xuZXhwb3J0IHZhciByZXBvc2l0b3J5ID0ge1widHlwZVwiOlwiZ2l0XCIsXCJ1cmxcIjpcImdpdEBnaXRsYWIuY29tOmNhbnplYS9pay1zcGxvci1jb3JlLmdpdFwifTtcbmV4cG9ydCB2YXIgZGV2RGVwZW5kZW5jaWVzID0ge1wicmVhY3RcIjpcIjE1LjAuMVwiLFwicmVhY3QtZG9tXCI6XCIxNS4wLjFcIixcImJhYmVsLWNsaVwiOlwiNi43LjVcIixcImJhYmVsLWNvcmVcIjpcIjYuNy42XCIsXCJiYWJlbC1lc2xpbnRcIjpcIjYuMC4yXCIsXCJiYWJlbC1wbHVnaW4tcmVhY3QtZGlzcGxheS1uYW1lXCI6XCIyLjAuMFwiLFwiYmFiZWwtcHJlc2V0LWVzMjAxNVwiOlwiNi4xOC4wXCIsXCJiYWJlbC1wcmVzZXQtZXMyMDE1LXJvbGx1cFwiOlwiMy4wLjBcIixcImJhYmVsLXByZXNldC1yZWFjdFwiOlwiNi41LjBcIixcImJhYmVsLXByZXNldC1yZWFjdC1obXJlXCI6XCIxLjEuMVwiLFwiYmFiZWwtcHJlc2V0LXN0YWdlLTFcIjpcIjYuNS4wXCIsXCJicm93c2VyLXN5bmNcIjpcIjIuMTEuMlwiLFwiY2hhaVwiOlwiMy41LjBcIixcImNoZWVyaW9cIjpcIjAuMjAuMFwiLFwiY29sb3JzXCI6XCIxLjEuMlwiLFwiY3Jvc3MtZW52XCI6XCIxLjAuN1wiLFwiY3NzLWxvYWRlclwiOlwiMC4yMy4xXCIsXCJjc3NuYW5vXCI6XCIzLjEwLjBcIixcImVuenltZVwiOlwiMi4yLjBcIixcImVzbGludFwiOlwiMi43LjBcIixcImVzbGludC1sb2FkZXJcIjpcIjEuMy4wXCIsXCJlc2xpbnQtcGx1Z2luLWltcG9ydFwiOlwiMS40LjBcIixcImVzbGludC1wbHVnaW4tcmVhY3RcIjpcIjQuMy4wXCIsXCJmaWxlLWxvYWRlclwiOlwiMC44LjVcIixcImpzb24ybW9kdWxlXCI6XCIwLjBcIixcImxpdmVyZWxvYWRcIjpcIl4wLjYuMFwiLFwibW9jaGFcIjpcIjIuNC41XCIsXCJucG0tcnVuLWFsbFwiOlwiXjEuNy4wXCIsXCJwYWNrYWdlLXByZWFtYmxlXCI6XCIwLjBcIixcInBvc3Rjc3MtY3NzbmV4dFwiOlwiMi45LjBcIixcInBvc3Rjc3MtbmVzdGVkXCI6XCIwLjMuMlwiLFwicG9zdGNzcy1zaW1wbGUtdmFyc1wiOlwiMC4zLjBcIixcInJlYWN0LWFkZG9ucy10ZXN0LXV0aWxzXCI6XCIxNS4wLjFcIixcInJpbXJhZlwiOlwiMi41LjJcIixcInJvbGx1cFwiOlwiMC40MFwiLFwicm9sbHVwLXBsdWdpbi1hc2NpaVwiOlwiMC4wXCIsXCJyb2xsdXAtcGx1Z2luLWJhYmVsXCI6XCIyLjcuMVwiLFwicm9sbHVwLXBsdWdpbi1jb21tb25qc1wiOlwiMy4wLjBcIixcInJvbGx1cC1wbHVnaW4tZXNsaW50XCI6XCIzLjAuMFwiLFwicm9sbHVwLXBsdWdpbi1ub2RlLXJlc29sdmVcIjpcIjIuMFwiLFwicm9sbHVwLXBsdWdpbi1wb3N0Y3NzXCI6XCIwLjIuMFwiLFwicm9sbHVwLXBsdWdpbi1yZXBsYWNlXCI6XCJeMS4xLjFcIixcInJvbGx1cC1wbHVnaW4tc2VydmVcIjpcIl4wLjEuMFwiLFwicm9sbHVwLXBsdWdpbi11Z2xpZnlcIjpcIjEuMC4xXCIsXCJyb2xsdXAtd2F0Y2hcIjpcIl4zLjIuMlwiLFwic2lub25cIjpcIjEuMTcuM1wiLFwic2lub24tY2hhaVwiOlwiMi44LjBcIixcInN0eWxlLWxvYWRlclwiOlwiMC4xMy4xXCIsXCJ0YXBlXCI6XCI0LjBcIixcInVnbGlmeS1qc1wiOlwiXjIuMFwifTtcbiIsIlxuY2xhc3MgVGV4dFV0aWxzIHtcblxuICAgIHN0YXRpYyBzcGxpdEJ5TkwgKG5hbWUpIHtcbiAgICAgICAgLy8gbG9vayBmb3IgYSBcXG5cbiAgICAgICAgbGV0IGluZCA9IG5hbWUuaW5kZXhPZihcIlxcblwiKTtcbiAgICAgICAgaWYgKGluZCAhPSAtMSkge1xuICAgICAgICAgICAgcmV0dXJuIFtuYW1lLnN1YnN0cigwLCBpbmQpLCBuYW1lLnN1YnN0cihpbmQpXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBbbmFtZV07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgc3BsaXRCeVdpZHRoKGNhcHRpb24sIG1heFdpZHRoLCB3aWR0aCkge1xuICAgICAgICBpZiAobWF4V2lkdGggPCB3aWR0aCkge1xuICAgICAgICAgICAgcmV0dXJuIFtjYXB0aW9uXTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCByYXRpbyA9IE1hdGgucm91bmQoY2FwdGlvbi5sZW5ndGggKiAod2lkdGgvbWF4V2lkdGgpKTtcbiAgICAgICAgY29uc29sZS5sb2coXCJNYXggPSBcIittYXhXaWR0aCtcIiwgXCIrd2lkdGggKyBcIiBSQVRJTzogXCIgKyByYXRpbyArIFwiIDogXCIgKyBjYXB0aW9uLmxlbmd0aCk7XG4gICAgICAgIHJldHVybiBUZXh0VXRpbHMuc3BsaXRCeVdvcmRzIChjYXB0aW9uLCByYXRpbyk7XG4gICAgfVxuXG4gICAgc3RhdGljIHNwbGl0QnlXb3JkcyhjYXB0aW9uLCBtYXhDaGFyc1BlckxpbmUpIHtcblxuICAgICAgICB2YXIgd29yZHMgPSBjYXB0aW9uLnNwbGl0KCcgJyk7XG4gICAgICAgIHZhciBsaW5lID0gXCJcIjtcblxuICAgICAgICB2YXIgbGluZXMgPSBbXTtcbiAgICAgICAgZm9yICh2YXIgbiA9IDA7IG4gPCB3b3Jkcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgdmFyIHRlc3RMaW5lID0gbGluZSArIHdvcmRzW25dICsgXCIgXCI7XG4gICAgICAgICAgICBpZiAodGVzdExpbmUubGVuZ3RoID4gbWF4Q2hhcnNQZXJMaW5lICYmIGxpbmUubGVuZ3RoID4gMClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsaW5lcy5wdXNoKGxpbmUpO1xuXG4gICAgICAgICAgICAgICAgbGluZSA9IHdvcmRzW25dICsgXCIgXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBsaW5lID0gdGVzdExpbmU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgbGluZXMucHVzaChsaW5lKTtcblxuICAgICAgICByZXR1cm4gbGluZXM7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUZXh0VXRpbHM7IiwiXG5pbXBvcnQgZmEgZnJvbSAnZm9udGF3ZXNvbWUnO1xuaW1wb3J0ICogYXMgZDMgZnJvbSAnZDMnO1xuXG5pbXBvcnQgJy4vc3R5bGUuc2Nzcyc7XG5pbXBvcnQgVGV4dFV0aWxzIGZyb20gJy4uLy4uL3V0aWxzL1RleHRVdGlscyc7XG5cbmNsYXNzIFNoYXBlIHtcbiAgY29uc3RydWN0b3IoZG9tYWluVHlwZSkge1xuICAgIHRoaXMuZG9tYWluVHlwZSA9IGRvbWFpblR5cGU7XG4gIH1cblxuICBidWlsZChjaGdTZXQpIHtcbiAgICAgIGxldCB0bXBUeHROb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMClcbiAgICAgICAgICAuYXR0cihcImR5XCIsIDApXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbihkKSB7IHJldHVybiAoZC5jbGFzcyA/IGQuY2xhc3M6XCJsYWJlbFwiKTsgfSlcbiAgICAgICAgICAudGV4dChmdW5jdGlvbiAoZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgICAgIGxldCBiYm94ID0gW107XG4gICAgICB0bXBUeHROb2RlLmVhY2goZnVuY3Rpb24gKGQsaSkgeyBiYm94W2ldID0gdGhpcy5nZXRCQm94KCk7IH0pO1xuXG4gICAgICBjb25zdCB0eHROb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMClcbiAgICAgICAgICAuYXR0cihcImR5XCIsIDApXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbihkKSB7IHJldHVybiAoZC5jbGFzcyA/IGQuY2xhc3M6XCJsYWJlbFwiKTsgfSlcbiAgICAgICAgICAuZWFjaChmdW5jdGlvbihkLCB0aSkge1xuICAgICAgICAgICAgICBkMy5zZWxlY3QodGhpcykuc2VsZWN0QWxsKCd0c3BhbicpLmRhdGEoVGV4dFV0aWxzLnNwbGl0QnlXaWR0aChkLmxhYmVsLCBiYm94W3RpXS53aWR0aCwgKGQud2lkdGggPyBkLndpZHRoOjk5OTk5OSkpKVxuICAgICAgICAgICAgICAuZW50ZXIoKS5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgICAgICAgICAuYXR0cihcInhcIiwgMClcbiAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJ5XCIsIGZ1bmN0aW9uIChkLCBpKSB7IHJldHVybiAoaSpiYm94W3RpXS5oZWlnaHQpOyB9KVxuICAgICAgICAgICAgICAgICAgICAudGV4dChmdW5jdGlvbiAobGluZSkgeyByZXR1cm4gbGluZTsgfSk7XG4gICAgICAgICAgfSk7XG5cbiAgICAgIHRtcFR4dE5vZGUuZWFjaChmdW5jdGlvbiAoZCkgeyB0aGlzLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcyk7IH0pO1xuXG4gICAgICBjaGdTZXQuZmlsdGVyKGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQuaGFzT3duUHJvcGVydHkoJ2ljb24nKTsgfSkuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJpY29uXCIpXG4gICAgICAgICAgLmF0dHIoXCJ0ZXh0LWFuY2hvclwiLCBcIm1pZGRsZVwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgLTEwKVxuICAgICAgICAgIC5hdHRyKFwiZHlcIiwgMClcbiAgICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7IHJldHVybiBmYShkLmljb24pOyB9KTtcblxuXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2hhcGU7XG4iLCJcbmltcG9ydCAqIGFzIGQzIGZyb20gJ2QzJztcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcbmltcG9ydCBmYSBmcm9tIFwiZm9udGF3ZXNvbWVcIjtcblxuaW1wb3J0IFRleHRVdGlscyBmcm9tICcuLi8uLi91dGlscy9UZXh0VXRpbHMnO1xuXG5jbGFzcyBSZWN0YW5nbGVTaGFwZSB7XG5cbiAgY29uc3RydWN0b3IoZG9tYWluVHlwZSkge1xuICAgIHRoaXMuZG9tYWluVHlwZSA9IGRvbWFpblR5cGU7XG4gIH1cblxuXG4gIGJ1aWxkKGNoZ1NldCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICBsZXQgeCA9IDEyMDtcbiAgICAgIGxldCB5ID0gNjA7XG5cbiAgICAgIGNoZ1NldC5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIC14LzIpXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsIC15LzIpXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCB4KVxuICAgICAgICAgIC5hdHRyKFwiaGVpZ2h0XCIsIHkpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgICByZXR1cm4gZC5zdGF0ZTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLy5zdHlsZShcInRyYW5zZm9ybVwiLCBcInRyYW5zbGF0ZSgtNTAlLCAtNTAlKVwiKVxuICAgICAgICAgIC8vLnN0eWxlKFwiZmlsdGVyXCIsIFwidXJsKCNkcm9wLXNoYWRvdylcIilcblxuICAgICAgY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwiYXBwSWRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAwKVxuICAgICAgICAgIC5hdHRyKFwiZHlcIiwgMTYpXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5hcHBJZDsgfSk7XG5cbiAgICAgIGNvbnN0IGFwcE5hbWVOb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwibGFiZWxcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpO1xuXG4gICAgICBhcHBOYW1lTm9kZS5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgLTQpXG4gICAgICAgICAgICAudGV4dChmdW5jdGlvbiAoZCkgeyBjb25zdCBsaW5lcyA9IFRleHRVdGlscy5zcGxpdEJ5V29yZHMoZC5sYWJlbCwgMjApOyByZXR1cm4gbGluZXNbMF07IH0pO1xuXG4gICAgICBhcHBOYW1lTm9kZS5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgNilcbiAgICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uIChkKSB7IGNvbnN0IGxpbmVzID0gVGV4dFV0aWxzLnNwbGl0QnlXb3JkcyhkLmxhYmVsLCAyMCk7IHJldHVybiAobGluZXMubGVuZ3RoID09IDEgPyBcIlwiOmxpbmVzWzFdKTsgfSk7XG5cblxuICAgICAgY2hnU2V0LmZpbHRlcihmdW5jdGlvbiAoZCkgeyByZXR1cm4gZC5sYWJlbHMuaGFzT3duUHJvcGVydHkoXCJ0b3BMZWZ0XCIpOyB9KVxuICAgICAgICAgIC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIC14LzIgKyAyKVxuICAgICAgICAgIC5hdHRyKFwieVwiLCAteS8yICsgOClcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwidG9wTGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJzdGFydFwiKVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQubGFiZWxzLnRvcExlZnQ7IH0pO1xuXG4gICAgICBjaGdTZXQuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmxhYmVscy5oYXNPd25Qcm9wZXJ0eShcInRvcFJpZ2h0XCIpOyB9KVxuICAgICAgICAgIC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIHgvMiAtIDIpXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsIC15LzIgKyA4KVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJ0b3BSaWdodFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJlbmRcIilcbiAgICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7IHJldHVybiBkLmxhYmVscy50b3BSaWdodDsgfSk7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQubGFiZWxzLmhhc093blByb3BlcnR5KFwiYm90dG9tTGVmdFwiKTsgfSlcbiAgICAgICAgICAuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCAteC8yICsgMilcbiAgICAgICAgICAuYXR0cihcInlcIiwgeS8yIC0gNClcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwiYm90dG9tTGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJzdGFydFwiKVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQubGFiZWxzLmJvdHRvbUxlZnQ7IH0pO1xuXG4gICAgICBjaGdTZXQuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmxhYmVscy5oYXNPd25Qcm9wZXJ0eShcImJvdHRvbVJpZ2h0XCIpOyB9KVxuICAgICAgICAgIC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIHgvMiAtIDIpXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsIHkvMiAtIDQpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImJvdHRvbVJpZ2h0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ0ZXh0LWFuY2hvclwiLCBcImVuZFwiKVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQubGFiZWxzLmJvdHRvbVJpZ2h0OyB9KTtcblxuXG4gICAgICBjaGdTZXQuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmhhc093blByb3BlcnR5KFwiaW1hZ2VcIik7IH0pXG4gICAgICAgICAgLmFwcGVuZChcImltYWdlXCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsICh4LzIpLTIwLTIpXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsICh5LzIpLTEwLTIpXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiAyMDsgfSlcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiAxMDsgfSlcbiAgICAgICAgICAuYXR0cihcInhsaW5rOmhyZWZcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5pbWFnZTsgfSk7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuICh0eXBlb2YgZC5hbGVydHMgIT0gXCJ1bmRlZmluZWRcIik7IH0pLmVhY2goZnVuY3Rpb24gKGQpIHtcblxuICAgICAgICAgICAgbGV0IGFsZXJ0Tm9kZXMgPSBkMy5zZWxlY3QodGhpcykuc2VsZWN0QWxsKFwidGV4dC5hbGVydHNcIikuZGF0YShkLmFsZXJ0cyk7XG5cbiAgICAgICAgICAgIGFsZXJ0Tm9kZXMuZW50ZXIoKVxuICAgICAgICAgICAgICAgIC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbiAodCkgeyByZXR1cm4gKHQuY2xhc3MgPyBcImljb24gXCIgKyB0LmNsYXNzOlwiaWNvblwiKTsgfSlcbiAgICAgICAgICAgICAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwiZW5kXCIpXG4gICAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJkeFwiLCBmdW5jdGlvbiAoYSxpKSB7IHJldHVybiAoLXgvMikgKyAxNSArIChpKjIwKTsgfSApXG4gICAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJkeVwiLCAoLXkvMikgLSAyKVxuICAgICAgICAgICAgICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uICh0KSB7IHJldHVybiBmYSh0Lmljb24pOyB9KVxuICAgICAgICAgICAgICAgICAgICAgIC5vbihcImNsaWNrXCIsIGZ1bmN0aW9uIChlLCBpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZDMuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGVydChcImNsaWNrZWQgXCIgKyBpICsgXCIgOiBcIiArIEpTT04uc3RyaW5naWZ5KGUpICsgXCIgT0JKOiBcIiArIGQubmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgIH0pO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFJlY3RhbmdsZVNoYXBlO1xuXG4iLCJcbmltcG9ydCBmYSBmcm9tIFwiZm9udGF3ZXNvbWVcIjtcbmltcG9ydCAqIGFzIGQzIGZyb20gJ2QzJztcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcblxuY2xhc3MgU2hhcGUge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKGNoZ1NldCkge1xuXG4gICAgICBjaGdTZXQuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBcImljb24gXCIgKyBkLmNsYXNzOlwiaWNvblwiKTsgfSlcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAwKVxuICAgICAgICAgIC5hdHRyKFwiZHlcIiwgNSlcbiAgICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7IHJldHVybiBmYShkLmljb24pOyB9KTtcblxuICAgICAgY2hnU2V0LmFwcGVuZChcInRleHRcIikuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmxhYmVsOyB9KVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBkLmNsYXNzOlwibGFiZWxcIik7IH0pXG4gICAgICAgICAgLmF0dHIoXCJ0ZXh0LWFuY2hvclwiLCBcIm1pZGRsZVwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMClcbiAgICAgICAgICAuYXR0cihcImR5XCIsIFwiMi4wZW1cIilcbiAgICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7IHJldHVybiBkLmxhYmVsOyB9KTtcblxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNoYXBlO1xuIiwiXG5pbXBvcnQgKiBhcyBkMyBmcm9tICdkMyc7XG5pbXBvcnQgJy4vc3R5bGUuc2Nzcyc7XG5cbmNsYXNzIFNoYXBlIHtcbiAgY29uc3RydWN0b3IoZG9tYWluVHlwZSkge1xuICAgIHRoaXMuZG9tYWluVHlwZSA9IGRvbWFpblR5cGU7XG4gIH1cblxuICBidWlsZChjaGdTZXQpIHtcbiAgICAgIGNoZ1NldC5hcHBlbmQoXCJpbWFnZVwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gLWQud2lkdGgvMjsgfSlcbiAgICAgICAgICAuYXR0cihcInlcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIC1kLmhlaWdodC8yOyB9KVxuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gZC53aWR0aDsgfSlcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiBkLmhlaWdodDsgfSlcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIChkLmNsYXNzID8gZC5jbGFzczpcImltYWdlXCIpOyB9KVxuICAgICAgICAgIC5hdHRyKFwieGxpbms6aHJlZlwiLCBmdW5jdGlvbihkKSB7IHJldHVybiBkLmltYWdlOyB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTaGFwZTtcbiIsImltcG9ydCAnLi9zdHlsZS5zY3NzJztcblxuaW1wb3J0ICogYXMgZDMgZnJvbSAnZDMnO1xuXG5jbGFzcyBTaGFwZSB7XG4gIGNvbnN0cnVjdG9yKGRvbWFpblR5cGUpIHtcbiAgICB0aGlzLmRvbWFpblR5cGUgPSBkb21haW5UeXBlO1xuICB9XG5cbiAgYnVpbGQoY2hnU2V0KSB7XG5cbiAgICAgIGNoZ1NldC5hcHBlbmQoXCJjaXJjbGVcIik7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuICFkLmhhc093blByb3BlcnR5KFwicG9zaXRpb25cIikgfHwgZC5wb3NpdGlvbiA9PSBcInJpZ2h0XCI7IH0pXG4gICAgICAgICAgLmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgZnVuY3Rpb24gKGQpIHsgY29uc3QgY2lyY2xlID0gZDMuc2VsZWN0KHRoaXMucGFyZW50Tm9kZSkuc2VsZWN0KCdjaXJjbGUnKS5ub2RlKCk7IHJldHVybiAyICsgKGNpcmNsZS5nZXRCQm94KCkud2lkdGgvMik7IH0pXG4gICAgICAgICAgLmF0dHIoXCJkeVwiLCA1KVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBkLmNsYXNzOlwibGFiZWxcIik7IH0pXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQucG9zaXRpb24gPT0gXCJjZW50ZXJcIjsgfSlcbiAgICAgICAgICAuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgICAuYXR0cihcImRvbWluYW50LWJhc2VsaW5lXCIsIFwiY2VudHJhbFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBkLmNsYXNzOlwibGFiZWxcIik7IH0pXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTaGFwZTtcbiIsIlxuaW1wb3J0IGZhIGZyb20gJ2ZvbnRhd2Vzb21lJztcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcblxuY2xhc3MgU2hhcGUge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKGNoZ1NldCkge1xuXG4gICAgICBjb25zdCB0eHROb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMTIpXG4gICAgICAgICAgLmF0dHIoXCJkeVwiLCA0KVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBkLmNsYXNzOlwibGFiZWxcIik7IH0pXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgICAgIGNoZ1NldC5hcHBlbmQoXCJjaXJjbGVcIilcbiAgICAgICAgICAuYXR0cihcInJcIiwgNilcbiAgICAgICAgICAuc3R5bGUoXCJzdHJva2VcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5oYXNPd25Qcm9wZXJ0eShcImNvbG9yXCIpID8gZC5jb2xvcjpcImdyZXlcIjsgfSlcbiAgICAgICAgICAuc3R5bGUoXCJzdHJva2Utd2lkdGhcIiwgXCIuM2VtXCIpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNoYXBlO1xuIiwiXG5pbXBvcnQgKiBhcyBkMyBmcm9tIFwiZDNcIjtcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcbmltcG9ydCB7Vmlld2VyQ2FudmFzfSBmcm9tIFwiLi4vLi4vVmlld2VyQ2FudmFzXCI7XG5pbXBvcnQgVGV4dFV0aWxzIGZyb20gJy4uLy4uL3V0aWxzL1RleHRVdGlscyc7XG5cbmNsYXNzIFNoYXBlIHtcbiAgY29uc3RydWN0b3IoZG9tYWluVHlwZSkge1xuICAgIHRoaXMuZG9tYWluVHlwZSA9IGRvbWFpblR5cGU7XG4gIH1cblxuICBidWlsZChjaGdTZXQpIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgICAgY2hnU2V0LmNsYXNzZWQoXCJjbGlja2FibGVcIiwgdHJ1ZSk7XG5cbiAgICAgIC8vIFdpZHRoIGNhbGN1bGF0ZWQgYnkgdGhlIGxheW91dFxuICAgICAgY2hnU2V0LmFwcGVuZChcInJlY3RcIilcbiAgICAgICAgICAuYXR0cihcInhcIiwgLTE1MC8yKVxuICAgICAgICAgIC5hdHRyKFwieVwiLCAtNDAvMilcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCA0MClcbiAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIDE1MCk7XG5cbiAgICAgIGNvbnN0IHRleHROb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwibGFiZWxcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpO1xuXG4gICAgICB0ZXh0Tm9kZS5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgZnVuY3Rpb24gKGQpIHsgY29uc3QgbGluZXMgPSBUZXh0VXRpbHMuc3BsaXRCeU5MKGQubGFiZWwpOyByZXR1cm4gKGxpbmVzLmxlbmd0aCA9PSAxID8gNTotMyk7IH0pXG4gICAgICAgICAgICAudGV4dChmdW5jdGlvbiAoZCkgeyBjb25zdCBsaW5lcyA9IFRleHRVdGlscy5zcGxpdEJ5TkwoZC5sYWJlbCk7IHJldHVybiBsaW5lc1swXTsgfSk7XG5cbiAgICAgIHRleHROb2RlLmFwcGVuZChcInRzcGFuXCIpXG4gICAgICAgICAgICAuYXR0cihcInhcIiwgMClcbiAgICAgICAgICAgIC5hdHRyKFwieVwiLCBmdW5jdGlvbiAoZCkgeyBjb25zdCBiYm94ID0gdGhpcy5wYXJlbnROb2RlLmdldEJCb3goKTsgcmV0dXJuIGJib3guaGVpZ2h0IC0gMzsgfSlcbiAgICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uIChkKSB7IGNvbnN0IGxpbmVzID0gVGV4dFV0aWxzLnNwbGl0QnlOTChkLmxhYmVsKTsgcmV0dXJuIChsaW5lcy5sZW5ndGggPT0gMSA/IFwiXCI6bGluZXNbMV0pOyB9KTtcblxuICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2hhcGU7XG4iLCJcbmltcG9ydCBmYSBmcm9tICdmb250YXdlc29tZSc7XG5pbXBvcnQgJy4vc3R5bGUuc2Nzcyc7XG5cbmNsYXNzIFNoYXBlIHtcbiAgY29uc3RydWN0b3IoZG9tYWluVHlwZSkge1xuICAgIHRoaXMuZG9tYWluVHlwZSA9IGRvbWFpblR5cGU7XG4gIH1cblxuICBidWlsZChjaGdTZXQpIHtcbiAgICAgIGNvbnN0IHR4dE5vZGUgPSBjaGdTZXQuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJsZWZ0XCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAxMilcbiAgICAgICAgICAuYXR0cihcImR5XCIsIC01KVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24oZCkgeyByZXR1cm4gKGQuY2xhc3MgPyBkLmNsYXNzOlwibGFiZWxcIik7IH0pXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5oYXNPd25Qcm9wZXJ0eSgnaWNvbicpOyB9KS5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImljb25cIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpXG4gICAgICAgICAgLnN0eWxlKFwiZmlsbFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiBkLmNvbG9yOyB9KVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgNSlcbiAgICAgICAgICAuYXR0cihcImR5XCIsIC01KVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGZhKGQuaWNvbik7IH0pO1xuXG4gICAgICBjaGdTZXQuYXBwZW5kKFwiY2lyY2xlXCIpXG4gICAgICAgICAgLmF0dHIoXCJyXCIsIDYpXG4gICAgICAgICAgLnN0eWxlKFwic3Ryb2tlXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIChkLmhhc093blByb3BlcnR5KFwiY29sb3JcIikgPyBkLmNvbG9yOlwiYmx1ZVwiKTsgfSlcbiAgICAgICAgICAuc3R5bGUoXCJzdHJva2Utd2lkdGhcIiwgXCIuM2VtXCIpXG4gICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24gKGQpIHsgY29uc3Qgd2lkdGggPSAoZC5oYXNPd25Qcm9wZXJ0eShcIndpZHRoXCIpID8gZC53aWR0aCA6IDIwMCk7IHJldHVybiBcInRyYW5zbGF0ZShcIiArICh3aWR0aCArIDYpICsgXCIsIDMpXCI7IH0pO1xuXG4gICAgICBjaGdTZXQuYXBwZW5kKFwicmVjdFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJsaW5lXCIpXG4gICAgICAgICAgLnN0eWxlKFwiZmlsbFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiAoZC5oYXNPd25Qcm9wZXJ0eShcImNvbG9yXCIpID8gZC5jb2xvcjpcImJsdWVcIik7IH0pXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gKGQuaGFzT3duUHJvcGVydHkoXCJ3aWR0aFwiKSA/IGQud2lkdGggOiAyMDApOyB9KVxuICAgICAgICAgIC5hdHRyKFwiaGVpZ2h0XCIsIDYpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNoYXBlO1xuIiwiXG5pbXBvcnQgZmEgZnJvbSAnZm9udGF3ZXNvbWUnO1xuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xuXG5jbGFzcyBTaGFwZSB7XG4gIGNvbnN0cnVjdG9yKGRvbWFpblR5cGUpIHtcbiAgICB0aGlzLmRvbWFpblR5cGUgPSBkb21haW5UeXBlO1xuICB9XG5cbiAgYnVpbGQoY2hnU2V0KSB7XG4gICAgICBjb25zdCBzaGFwZSA9IHRoaXMuZG9tYWluVHlwZTtcblxuICAgICAgY2hnU2V0LmFwcGVuZChcInJlY3RcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwibGluZVwiKVxuICAgICAgICAgIC5hdHRyKFwicnhcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIHNoYXBlID09IFwidGltZWxpbmUyXCIgPyA4OjA7IH0pXG4gICAgICAgICAgLmF0dHIoXCJyeVwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gc2hhcGUgPT0gXCJ0aW1lbGluZTJcIiA/IDg6MDsgfSlcbiAgICAgICAgICAuc3R5bGUoXCJmaWxsXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIChkLmhhc093blByb3BlcnR5KFwiY29sb3JcIikgPyBkLmNvbG9yOlwiYmx1ZVwiKTsgfSlcbiAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIGZ1bmN0aW9uIChkKSB7IHJldHVybiAoZC5oYXNPd25Qcm9wZXJ0eShcIndpZHRoXCIpID8gZC53aWR0aCA6IDIwMCk7IH0pXG4gICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgMjApO1xuXG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5oYXNPd25Qcm9wZXJ0eSgnaWNvbicpOyB9KS5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImljb25cIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAxMClcbiAgICAgICAgICAuYXR0cihcImR5XCIsIDE0KVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGZhKGQuaWNvbik7IH0pO1xuXG4gICAgICBjb25zdCB0eHROb2RlID0gY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibGVmdFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMTgpXG4gICAgICAgICAgLmF0dHIoXCJkeVwiLCAxNClcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIChkLmNsYXNzID8gZC5jbGFzczpcImxhYmVsXCIpOyB9KVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQubGFiZWw7IH0pO1xuXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2hhcGU7XG4iLCJcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcblxuY2xhc3MgU2hhcGUge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKG5ld05vZGVzKSB7XG4gICAgbmV3Tm9kZXMuZWFjaChmdW5jdGlvbiAoZCkge1xuICAgICAgICBpZiAoIWQuaGFzT3duUHJvcGVydHkoXCJuYW1lXCIpKSB7XG4gICAgICAgICAgICBhbGVydChcIlBhbmVsIHJlcXVpcmVzIGEgbmFtZSEgXCIgKyBKU09OLnN0cmluZ2lmeShkKSk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTaGFwZTtcbiIsIlxuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xuaW1wb3J0IGZhIGZyb20gJ2ZvbnRhd2Vzb21lJztcblxuY2xhc3MgU2hhcGUge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKGNoZ1NldCkge1xuICAgICAgbGV0IHggPSAxMDA7XG4gICAgICBsZXQgeSA9IDUwO1xuXG4gICAgICBjaGdTZXQuYXBwZW5kKFwicmVjdFwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCAteC8yKVxuICAgICAgICAgIC5hdHRyKFwieVwiLCAteS8yKVxuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgeClcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCB5KTtcblxuICAgICAgY2hnU2V0LmFwcGVuZChcInRleHRcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIChkLmNsYXNzID8gZC5jbGFzczpcImxhYmVsXCIpOyB9KVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgICAuYXR0cihcImRvbWluYW50LWJhc2VsaW5lXCIsIFwiY2VudHJhbFwiKVxuICAgICAgICAgIC5hdHRyKFwiZHhcIiwgMClcbiAgICAgICAgICAuYXR0cihcImR5XCIsIDApXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5oYXNPd25Qcm9wZXJ0eSgnaWNvbicpOyB9KS5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImljb25cIilcbiAgICAgICAgICAuYXR0cihcInRleHQtYW5jaG9yXCIsIFwibWlkZGxlXCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAoeC8yKSAtIDEyKVxuICAgICAgICAgIC5hdHRyKFwiZHlcIiwgLSh5LzIpICsgMTUpXG4gICAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZmEoZC5pY29uKTsgfSk7XG5cbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTaGFwZTtcbiIsIlxuaW1wb3J0ICogYXMgZDMgZnJvbSBcImQzXCI7XG5pbXBvcnQgJy4vc3R5bGUuc2Nzcyc7XG5pbXBvcnQge1ZpZXdlckNhbnZhc30gZnJvbSBcIi4uLy4uL1ZpZXdlckNhbnZhc1wiO1xuaW1wb3J0IFRleHRVdGlscyBmcm9tICcuLi8uLi91dGlscy9UZXh0VXRpbHMnO1xuXG5jbGFzcyBTaGFwZSB7XG4gIGNvbnN0cnVjdG9yKGRvbWFpblR5cGUpIHtcbiAgICB0aGlzLmRvbWFpblR5cGUgPSBkb21haW5UeXBlO1xuICB9XG5cbiAgYnVpbGQoY2hnU2V0KSB7XG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XG5cbiAgICAgIGNoZ1NldC5jbGFzc2VkKFwidGFiXCIsIHRydWUpO1xuXG4gICAgICAvLyBXaWR0aCBjYWxjdWxhdGVkIGJ5IHRoZSBsYXlvdXRcbiAgICAgIGNoZ1NldC5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIC0yMDAvMilcbiAgICAgICAgICAuYXR0cihcInlcIiwgLTUwLzIpXG4gICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgNTApXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCAyMDApO1xuXG4gICAgICBjb25zdCB0ZXh0Tm9kZSA9IGNoZ1NldC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImxhYmVsXCIpXG4gICAgICAgICAgLmF0dHIoXCJ0ZXh0LWFuY2hvclwiLCBcIm1pZGRsZVwiKTtcblxuICAgICAgdGV4dE5vZGUuYXBwZW5kKFwidHNwYW5cIilcbiAgICAgICAgICAgIC5hdHRyKFwieFwiLCAwKVxuICAgICAgICAgICAgLnRleHQoZnVuY3Rpb24gKGQpIHsgY29uc3QgbGluZXMgPSBUZXh0VXRpbHMuc3BsaXRCeU5MKGQubGFiZWwpOyByZXR1cm4gbGluZXNbMF07IH0pO1xuXG4gICAgICB0ZXh0Tm9kZS5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgZnVuY3Rpb24gKGQpIHsgY29uc3QgYmJveCA9IHRoaXMucGFyZW50Tm9kZS5nZXRCQm94KCk7IHJldHVybiBiYm94LmhlaWdodDsgfSlcbiAgICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uIChkKSB7IGNvbnN0IGxpbmVzID0gVGV4dFV0aWxzLnNwbGl0QnlOTChkLmxhYmVsKTsgcmV0dXJuIChsaW5lcy5sZW5ndGggPT0gMSA/IFwiXCI6bGluZXNbMV0pOyB9KTtcblxuICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2hhcGU7XG4iLCJcbmltcG9ydCAqIGFzIGQzIGZyb20gXCJkM1wiO1xuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xuXG5jbGFzcyBMYXlvdXQge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGVucmljaERhdGEgKCkge1xuICB9XG5cbiAgb3JnYW5pemUoZDN2aXN1YWwsIHpvbmUsIHJvb3QpIHtcbiAgfVxuXG4gIGNvbmZpZ0V2ZW50cyhkM3Zpc3VhbCwgbmV3Tm9kZXMsIHpvbmVzLCB2aWV3ZXIpIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgICAgbGV0IGZvcmNlID0gdmlld2VyLmZvcmNlO1xuXG4gICAgICAvLyBVc2UgeCBhbmQgeSBpZiBpdCB3YXMgcHJvdmlkZWQsIG90aGVyd2lzZSBzdGljayBpdCBpbiB0aGUgY2VudGVyIG9mIHRoZSB6b25lXG4gICAgICBuZXdOb2Rlc1xuICAgICAgICAgIC5hdHRyKFwiY3hcIiwgZnVuY3Rpb24oZCkgeyBjb25zdCBkaW0gPSBzZWxmLmdldERpbShkLCB6b25lcyk7IHJldHVybiBkLnggPSAoZC54ID8gZC54IDogKGRpbS54MSArIChkaW0ueDItZGltLngxKS8yKSk7IH0pXG4gICAgICAgICAgLmF0dHIoXCJjeVwiLCBmdW5jdGlvbihkKSB7IGNvbnN0IGRpbSA9IHNlbGYuZ2V0RGltKGQsIHpvbmVzKTsgcmV0dXJuIGQueSA9IChkLnkgPyBkLnkgOiAoZGltLnkxICsgKGRpbS55Mi1kaW0ueTEpLzIpKTsgfSk7XG5cbi8vbm9kZVNldCwgaW5kZXhcbiAgICAgIG5ld05vZGVzXG4gICAgICAgIC5vbignbW91c2VlbnRlcicsIGZ1bmN0aW9uIChlKSB7XG4vLyAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJub2RlLWhvdmVyXCIpO1xuLy8gICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5PREU6IFwiICtKU09OLnN0cmluZ2lmeShub2RlU2V0KSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbignbW91c2VsZWF2ZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAvLyQodGhpcykucmVtb3ZlQ2xhc3MoXCJub2RlLWhvdmVyXCIpO1xuICAgICAgICB9KVxuICAgICAgICAub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIC8vY29uc3Qgc2V0ID0gdmlzLnNlbGVjdChcImcubm9kZVNldFwiKS5zZWxlY3RBbGwoXCJnLm5vZGVcIik7XG4gICAgICAgICAgICBpZiAodmlld2VyLmRvd25YID09IE1hdGgucm91bmQoZS54ICsgZS55KSkge1xuICAgICAgICAgICAgICAvL2xldCBtYXRjaGVkSXRlbSA9IHNldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIChlLmluZGV4ID09IGQuaW5kZXgpOyB9KTtcbiAgICAgICAgICAgICAgdmlld2VyLm5hdmlnYXRpb24udG9nZ2xlKGUsIGQzLnNlbGVjdCh0aGlzKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICB2aWV3ZXIubmF2aWdhdGlvbi5jbG9zZShlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cblxuICAgICAgICAgIGZ1bmN0aW9uIGRyYWdzdGFydGVkKGQpIHtcbiAgICAgICAgICAgICAgICBkMy5zZWxlY3QodGhpcykuY2xhc3NlZChcImZpeGVkXCIsIGQuZml4ZWQgPT0gdHJ1ZSk7XG4gICAgICAgICAgICAgICAgaWYgKCFkMy5ldmVudC5hY3RpdmUpIGZvcmNlLmFscGhhVGFyZ2V0KDAuMykucmVzdGFydCgpO1xuICAgICAgICAgICAgICAgIGQuZnggPSBkLng7XG4gICAgICAgICAgICAgICAgZC5meSA9IGQueTtcbiAgICAgICAgICAgICAgICB2aWV3ZXIuZG93blggPSBNYXRoLnJvdW5kKGQueCArIGQueSk7XG5cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBmdW5jdGlvbiBkcmFnZ2VkKGQpIHtcbiAgICAgICAgICAgICAgICBkLmZ4ID0gZDMuZXZlbnQueDtcbiAgICAgICAgICAgICAgICBkLmZ5ID0gZDMuZXZlbnQueTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBmdW5jdGlvbiBkcmFnZW5kZWQoZCkge1xuICAgICAgICAgICAgICAgIGlmICghZDMuZXZlbnQuYWN0aXZlKSBmb3JjZS5hbHBoYVRhcmdldCgwKTtcblxuICAgICAgICAgICAgICAgIGlmICghZC5maXhlZCkge1xuICAgICAgICAgICAgICAgICAgICBkLmZ4ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgZC5meSA9IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQgKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmlld2VyLnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgICAgICAgICAgIH0sIDIwMCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgbmV3Tm9kZXMuY2FsbChkMy5kcmFnKClcbiAgICAgICAgICAgICAgLm9uKFwic3RhcnRcIiwgZHJhZ3N0YXJ0ZWQpXG4gICAgICAgICAgICAgIC5vbihcImRyYWdcIiwgZHJhZ2dlZClcbiAgICAgICAgICAgICAgLm9uKFwiZW5kXCIsIGRyYWdlbmRlZCkpO1xuXG5cbiAgICAgIGxldCBjbGlja2FibGUgPSBuZXdOb2Rlcy5maWx0ZXIoZnVuY3Rpb24oZCkgeyByZXR1cm4gZDMuc2VsZWN0KHRoaXMpLmNsYXNzZWQoJ2NsaWNrYWJsZScpOyB9KTtcbiAgICAgIGNsaWNrYWJsZVxuICAgICAgICAub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHsgZDMuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7IHZpZXdlci50cmlnZ2VyKFwic2hhcGVDbGlja1wiLCBlKTsgLypzZWxmLnRvZ2dsZVRhYih2aWV3ZXIsIGUsIHRoaXMpOyovIC8qc2VsZi50b2dnbGVOYXZpZ2F0aW9uKHZpZXdlciwgZSwgdGhpcyk7ICovfSk7XG4gIH1cblxuICBnZXREaW0oZCwgem9uZXMpIHtcbiAgICAgIGNvbnN0IGluZGV4ID0gZC5ib3VuZGFyeS5jaGFyQ29kZUF0KDApIC0gNjU7XG4gICAgICBpZiAoem9uZXMubGVuZ3RoIDw9IGluZGV4KSB7XG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcIklOVkFMSUQgQk9VTkRBUlk6IFwiICsgZC5ib3VuZGFyeSk7XG4gICAgICB9XG4gICAgICBjb25zdCByZWN0ID0gKHpvbmVzW2luZGV4XS5oYXNPd25Qcm9wZXJ0eSgnY2FsY3VsYXRlZFJlY3RhbmdsZScpID8gem9uZXNbaW5kZXhdLmNhbGN1bGF0ZWRSZWN0YW5nbGUgOiB6b25lc1tpbmRleF0ucmVjdGFuZ2xlKTtcbiAgICAgIGNvbnN0IGRpbSA9IHt4MTpyZWN0WzBdLHkxOnJlY3RbMV0seDI6cmVjdFswXSArIHJlY3RbMl0seTI6cmVjdFsxXSArIHJlY3RbM119O1xuICAgICAgLy9jb25zb2xlLmxvZyhcIks6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGltLCBudWxsLCAyKSk7XG4gICAgICByZXR1cm4gZGltO1xuICB9XG5cbiAgYnVpbGQoZDN2aXN1YWwsIHpvbmUsIHJvb3QsIHZpZXdlcikge1xuICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgICBsZXQgb2JqID0gcm9vdC5hcHBlbmQoXCJnXCIpO1xuXG4gICAgICBvYmouY2xhc3NlZChcIl96b25lXCIsIHRydWUpO1xuICAgICAgb2JqLmNsYXNzZWQoXCJfXCIgKyB0aGlzLmRvbWFpblR5cGUsIHRydWUpO1xuXG4gICAgICBvYmouYXBwZW5kKCdyZWN0Jyk7XG5cbiAgICAgIHRoaXMucmVmcmVzaFNpemUodmlld2VyLCB6b25lLCBvYmopO1xuXG4gIH1cblxuICByZWZyZXNoU2l6ZSAodmlld2VyLCB6b25lLCB6b25lTm9kZSkge1xuICAgICAgbGV0IGQzdmlzdWFsID0gdmlld2VyLnZpcztcblxuICAgICAgdGhpcy5jYWxjdWxhdGVSZWN0YW5nbGUgKGQzdmlzdWFsLCB6b25lKTtcblxuICAgICAgbGV0IHJlY3QgPSB6b25lTm9kZS5zZWxlY3QoXCJyZWN0XCIpXG4gICAgICAgICAgLnN0eWxlKFwiZmlsdGVyXCIsIFwidXJsKCNkcm9wLXNoYWRvdylcIilcbiAgICAgICAgICAuYXR0cihcInhcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzBdKVxuICAgICAgICAgIC5hdHRyKFwieVwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbMV0pXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbMl0pXG4gICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzNdKTtcbiAgfVxuXG4gIGNhbGN1bGF0ZVJlY3RhbmdsZSAoZDN2aXN1YWwsIHpvbmUpIHtcbiAgICAgIGxldCB2YiA9IGQzdmlzdWFsLmF0dHIoXCJ2aWV3Qm94XCIpLnNwbGl0KCcgJyk7XG5cbiAgICAgIGxldCB2aWV3V2lkdGggPSB2YlsyXTtcbiAgICAgIGxldCB2aWV3SGVpZ2h0ID0gdmJbM107XG5cbiAgICAgIGNvbnNvbGUubG9nKFwiX2RlZmF1bHQ6IFJlc2l6aW5nIHpvbmUgOiBXPVwiK3ZpZXdXaWR0aCtcIixIPVwiK3ZpZXdIZWlnaHQpO1xuXG4gICAgICBsZXQgcGFnZVdpZHRoID0gZDN2aXN1YWwuYXR0cihcIndpZHRoXCIpO1xuICAgICAgbGV0IHBhZ2VIZWlnaHQgPSBkM3Zpc3VhbC5hdHRyKFwiaGVpZ2h0XCIpO1xuICAgICAgLy8gVEhpcyBuZWVkcyB0byBiZSBhIGNhbGN1bGF0aW9uIGJhc2VkIG9uIGEgcmF0aW9cblxuICAgICAgbGV0IHBhZ2VIZWlnaHRTZXQgPSAodHlwZW9mIHpvbmUucmVjdGFuZ2xlWzNdID09IFwic3RyaW5nXCIgJiYgem9uZS5yZWN0YW5nbGVbM10uaW5kZXhPZihcInBhZ2VIZWlnaHRcIikgIT0gLTEpO1xuXG4gICAgICBsZXQgcmVjdERpbWVuc2lvbiA9IFtdO1xuXG5cbiAgICAgIGZvciAoIGxldCB2ID0gMDsgdiA8IDQ7IHYrKykge1xuICAgICAgICAgIGlmICh0eXBlb2Ygem9uZS5yZWN0YW5nbGVbdl0gPT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkV2YWx1YXRpbmc6IFwiICsgem9uZS5yZWN0YW5nbGVbdl0pO1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIiAuLi4gdG8gOiBcIiArIGV2YWwoem9uZS5yZWN0YW5nbGVbdl0pKTtcbiAgICAgICAgICAgIHJlY3REaW1lbnNpb24ucHVzaChldmFsKHpvbmUucmVjdGFuZ2xlW3ZdKSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJlY3REaW1lbnNpb24ucHVzaCh6b25lLnJlY3RhbmdsZVt2XSk7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKHBhZ2VIZWlnaHRTZXQpIHtcbiAgICAgICAgICByZWN0RGltZW5zaW9uWzNdID0gcGFnZUhlaWdodCAtIDIwOyAvLygoem9uZS5yZWN0YW5nbGVbMF0rem9uZS5yZWN0YW5nbGVbMl0pICogcGFnZUhlaWdodC9wYWdlV2lkdGgpIC0gMjA7XG4gICAgICB9XG5cbiAgICAgIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZSA9IHJlY3REaW1lbnNpb247XG4gICAgICBjb25zb2xlLmxvZyhcIl9kZWZhdWx0OiBSZXNpemVkIHpvbmUgOiBXPVwiK3JlY3REaW1lbnNpb25bMl0rXCIsSD1cIityZWN0RGltZW5zaW9uWzNdICApO1xuXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0OyIsImltcG9ydCAnLi9zdHlsZS5jc3MnO1xuaW1wb3J0IERlZmF1bHQgZnJvbSAnLi4vZGVmYXVsdC5qcyc7XG5cbmNsYXNzIExheW91dCBleHRlbmRzIERlZmF1bHQge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgc3VwZXIoZG9tYWluVHlwZSk7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKGQzdmlzdWFsLCB6b25lLCByb290KSB7XG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgICBsZXQgb2JqID0gcm9vdC5hcHBlbmQoXCJnXCIpO1xuXG4gICAgICBvYmouY2xhc3NlZChcIl96b25lXCIsIHRydWUpO1xuICAgICAgb2JqLmNsYXNzZWQoXCJfXCIgKyB0aGlzLmRvbWFpblR5cGUsIHRydWUpO1xuXG4vLyAgICAgIGxldCB2YiA9IGQzdmlzdWFsLmF0dHIoXCJ2aWV3Qm94XCIpLnNwbGl0KCcgJyk7XG4vL1xuLy8gICAgICBsZXQgdmlld1dpZHRoID0gdmJbMl07XG4vLyAgICAgIGxldCB2aWV3SGVpZ2h0ID0gdmJbM107XG4vL1xuLy8gICAgICBjb25zb2xlLmxvZyhcIl9zd2ltbGFuZTogUmVzaXppbmcgem9uZSA6IFc9XCIrdmlld1dpZHRoK1wiLEg9XCIrdmlld0hlaWdodCk7XG4vL1xuLy8gICAgICBsZXQgcGFnZVdpZHRoID0gZDN2aXN1YWwuYXR0cihcIndpZHRoXCIpO1xuLy8gICAgICBsZXQgcGFnZUhlaWdodCA9IGQzdmlzdWFsLmF0dHIoXCJoZWlnaHRcIik7XG4vLyAgICAgIC8vIFRIaXMgbmVlZHMgdG8gYmUgYSBjYWxjdWxhdGlvbiBiYXNlZCBvbiBhIHJhdGlvXG4vL1xuLy8gICAgICBsZXQgcGFnZUhlaWdodFNldCA9ICh0eXBlb2Ygem9uZS5yZWN0YW5nbGVbM10gPT0gXCJzdHJpbmdcIiAmJiB6b25lLnJlY3RhbmdsZVszXS5pbmRleE9mKFwicGFnZUhlaWdodFwiKSAhPSAtMSk7XG4vL1xuLy8gICAgICBsZXQgcmVjdERpbWVuc2lvbiA9IFtdO1xuLy9cbi8vICAgICAgZm9yICggbGV0IHYgPSAwOyB2IDwgNDsgdisrKSB7XG4vLyAgICAgICAgICBpZiAodHlwZW9mIHpvbmUucmVjdGFuZ2xlW3ZdID09IFwic3RyaW5nXCIpIHtcbi8vICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkV2YWx1YXRpbmc6IFwiICsgem9uZS5yZWN0YW5nbGVbdl0pO1xuLy8gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiIC4uLiB0byA6IFwiICsgZXZhbCh6b25lLnJlY3RhbmdsZVt2XSkpO1xuLy8gICAgICAgICAgICByZWN0RGltZW5zaW9uLnB1c2goZXZhbCh6b25lLnJlY3RhbmdsZVt2XSkpO1xuLy8gICAgICAgICAgfSBlbHNlIHtcbi8vICAgICAgICAgICAgcmVjdERpbWVuc2lvbi5wdXNoKHpvbmUucmVjdGFuZ2xlW3ZdKTtcbi8vICAgICAgICAgIH1cbi8vICAgICAgfVxuLy8gICAgICB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGUgPSByZWN0RGltZW5zaW9uO1xuLy9cbi8vICAgICAgaWYgKHBhZ2VIZWlnaHRTZXQpIHtcbi8vICAgICAgICAgIHJlY3REaW1lbnNpb25bM10gPSBwYWdlSGVpZ2h0IC0gMjA7IC8vKCh6b25lLnJlY3RhbmdsZVswXSt6b25lLnJlY3RhbmdsZVsyXSkgKiBwYWdlSGVpZ2h0L3BhZ2VXaWR0aCkgLSAyMDtcbi8vICAgICAgfVxuXG4gICAgICB0aGlzLmNhbGN1bGF0ZVJlY3RhbmdsZSAoZDN2aXN1YWwsIHpvbmUpO1xuXG4gICAgICBsZXQgcmVjdCA9IG9iai5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgICAgLy8uc3R5bGUoXCJmaWx0ZXJcIiwgXCJ1cmwoI2Ryb3Atc2hhZG93KVwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbMF0pXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsxXSlcbiAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsyXSlcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbM10pXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBcImxhbmVcIik7IC8vZmZmODk5XG5cbiAgICAgIG9iai5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ4XCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVswXSlcbiAgICAgICAgICAuYXR0cihcInlcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzFdKVxuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIHpvbmUub3JpZW50YXRpb24gPT0gXCJob3Jpem9udGFsXCIgPyA4MDogNDA7IH0pXG4gICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzNdKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJsYW5lTGFiZWxcIik7IC8vZmZmODk5XG5cbiAgICAgIGxldCB0cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZShcIiArIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVswXSArIFwiLFwiICsgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzFdICsgXCIpIHJvdGF0ZSgtOTApIHRyYW5zbGF0ZShcIiArICgtICh6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbM10gLyAyKSkgKyBcIixcIiArIFwiMTVcIiArIFwiKVwiO1xuICAgICAgaWYgKHpvbmUub3JpZW50YXRpb24gPT0gXCJob3Jpem9udGFsXCIpIHtcbiAgICAgICAgdHJhbnNmb3JtID0gXCJ0cmFuc2xhdGUoXCIgKyAoem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzBdICsgKDgwLzIpKSArIFwiLFwiICsgKHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsxXSArICh6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbM10vMikpICsgXCIpXCI7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHRleHROb2RlcyA9IG9ialxuICAgICAgICAgIC5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgdHJhbnNmb3JtKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgICAvLy5hdHRyKFwiZG9taW5hbnQtYmFzZWxpbmVcIiwgXCJjZW50cmFsXCIpXG4gICAgICAgICAgLmF0dHIoXCJkeFwiLCAwKVxuICAgICAgICAgIC5hdHRyKFwiZHlcIiwgMCk7XG5cbiAgICAgIHRleHROb2Rlcy5hcHBlbmQoXCJ0c3BhblwiKVxuICAgICAgICAgICAgLmF0dHIoXCJ4XCIsIDApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgMClcbiAgICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uIChkKSB7IGNvbnN0IGxpbmVzID0gc2VsZi5zcGxpdENhcHRpb24oem9uZS50aXRsZSwgMjApOyByZXR1cm4gbGluZXNbMF07IH0pO1xuXG4gICAgICB0ZXh0Tm9kZXMuYXBwZW5kKFwidHNwYW5cIilcbiAgICAgICAgICAgIC5hdHRyKFwieFwiLCAwKVxuICAgICAgICAgICAgLmF0dHIoXCJ5XCIsIDE1KVxuICAgICAgICAgICAgLnRleHQoZnVuY3Rpb24gKGQpIHsgY29uc3QgbGluZXMgPSBzZWxmLnNwbGl0Q2FwdGlvbih6b25lLnRpdGxlLCAyMCk7IHJldHVybiAobGluZXMubGVuZ3RoID09IDEgPyBcIlwiOmxpbmVzWzFdKTsgfSk7XG5cbiAgICAgIGlmICh6b25lLmhhc093blByb3BlcnR5KFwiaW1hZ2VcIikpIHtcbiAgICAgICAgICB0cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZShcIiArICh6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbMF0gKyAoMTApKSArIFwiLFwiICsgKHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsxXSArICh6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbM10vMikgLSA2MCkgKyBcIilcIjtcblxuICAgICAgICAgIG9iai5hcHBlbmQoXCJpbWFnZVwiKVxuICAgICAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIDYwOyB9KVxuICAgICAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCBmdW5jdGlvbihkKSB7IHJldHVybiA0MDsgfSlcbiAgICAgICAgICAgICAgLmF0dHIoXCJ4bGluazpocmVmXCIsIGZ1bmN0aW9uKGQpIHsgcmV0dXJuIHpvbmUuaW1hZ2U7IH0pXG4gICAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIHRyYW5zZm9ybSk7XG4gICAgICB9XG4vKlxuICAgICAgbGV0IGJveCA9IG51bGw7XG5cbiAgICAgIHJlY3QuZWFjaCAoZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgYm94ID0gdGhpcy5nZXRCQm94KCk7XG4gICAgICB9KTtcblxuICAgICAgICAgICovXG4gIH1cblxuICBzcGxpdENhcHRpb24oY2FwdGlvbiwgTUFYSU1VTV9DSEFSU19QRVJfTElORT02KSB7XG5cbiAgICB2YXIgd29yZHMgPSBjYXB0aW9uLnNwbGl0KCcgJyk7XG4gICAgdmFyIGxpbmUgPSBcIlwiO1xuXG4gICAgdmFyIGxpbmVzID0gW107XG4gICAgZm9yICh2YXIgbiA9IDA7IG4gPCB3b3Jkcy5sZW5ndGg7IG4rKykge1xuICAgICAgICB2YXIgdGVzdExpbmUgPSBsaW5lICsgd29yZHNbbl0gKyBcIiBcIjtcbiAgICAgICAgaWYgKHRlc3RMaW5lLmxlbmd0aCA+IE1BWElNVU1fQ0hBUlNfUEVSX0xJTkUgJiYgbGluZS5sZW5ndGggPiAwKVxuICAgICAgICB7XG4gICAgICAgICAgICBsaW5lcy5wdXNoKGxpbmUpO1xuXG4gICAgICAgICAgICBsaW5lID0gd29yZHNbbl0gKyBcIiBcIjtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGxpbmUgPSB0ZXN0TGluZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBsaW5lcy5wdXNoKGxpbmUpO1xuXG4gICAgcmV0dXJuIGxpbmVzO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IExheW91dDsiLCJcbmltcG9ydCBmYSBmcm9tICdmb250YXdlc29tZSc7XG5pbXBvcnQgKiBhcyBkMyBmcm9tIFwiZDNcIjtcbmltcG9ydCAnLi9zdHlsZS5zY3NzJztcbmltcG9ydCB7Vmlld2VyQ2FudmFzfSBmcm9tICcuLi8uLi9WaWV3ZXJDYW52YXMnO1xuXG5jbGFzcyBMYXlvdXQge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIGJ1aWxkKGQzdmlzdWFsLCB6b25lLCByb290LCB2aWV3ZXIpIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgIHZpZXdlci5vbihcInRhYkNsaWNrXCIsIGZ1bmN0aW9uIChldmVudCwgZSkge1xuICAgICAgICAgIGxldCB0YWJzVG9PcGVuID0gW107XG4gICAgICAgICAgbGV0IHRhYlRvT3BlbiA9IGUubmFtZTtcbiAgICAgICAgICB3aGlsZSAodGFiVG9PcGVuICE9IG51bGwgKSB7XG4gICAgICAgICAgICAgIGxldCB0YWJOb2RlID0gZDN2aXN1YWwuc2VsZWN0QWxsKFwiZy50YWJcIilcbiAgICAgICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGQubmFtZSA9PSB0YWJUb09wZW47XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICBpZiAodGFiTm9kZS5jbGFzc2VkKFwic2VsZWN0ZWRfdGFiXCIpID09IGZhbHNlIHx8IGUubmFtZSA9PSB0YWJOb2RlLmRhdHVtKCkubmFtZSkge1xuICAgICAgICAgICAgICAgICAgdGFic1RvT3Blbi5wdXNoKHRhYk5vZGUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRhYlRvT3BlbiA9IHRhYk5vZGUuZGF0dW0oKS5sYXlvdXQucGFyZW50O1xuICAgICAgICAgIH1cbiAgICAgICAgICBsZXQgdG8gPSB0YWJzVG9PcGVuLnJldmVyc2UoKTtcbiAgICAgICAgICBmb3IgKGxldCBpbmQgaW4gdG8pIHtcbiAgICAgICAgICAgICAgbGV0IGNsaWNrZWRPYmplY3QgPSB0b1tpbmRdO1xuICAgICAgICAgICAgICBzZWxmLnRvZ2dsZVRhYih2aWV3ZXIsIGNsaWNrZWRPYmplY3QuZGF0dW0oKSwgY2xpY2tlZE9iamVjdC5ub2RlKCkpO1xuICAgICAgICAgIH1cbiAgICAgIH0pO1xuICB9XG5cbiAgZW5yaWNoRGF0YSAobm9kZXMpIHtcbi8vXG4vLyAgICAgIGxldCBzZWdzID0gW107XG4vLyAgICAgIGZvciAoIGxldCB4ID0gMCA7IHggPCBncmFwaC5ub2Rlcy5sZW5ndGg7IHgrKykge1xuLy8gICAgICAgICAgbGV0IHNlZyA9IChncmFwaC5ub2Rlc1t4XS50aXRsZSAmJiBncmFwaC5ub2Rlc1t4XS50aXRsZS5zZWdtZW50ID8gZ3JhcGgubm9kZXNbeF0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8gICAgICAgICAgaWYgKHNlZyAhPSBcIlwiICYmIHNlZ3MuaW5kZXhPZihzZWcpIDwgMCkge1xuLy8gICAgICAgICAgICAgIHNlZ3MucHVzaChzZWcpO1xuLy8gICAgICAgICAgfVxuLy8gICAgICB9XG4gICAgICBsZXQgYmFkTm9kZXMgPSBbXTtcbiAgICAgIGxldCBmdWxsTm9kZUxpc3QgPSBbXTtcblxuICAgICAgdGhpcy5yZWN1cnNlKG51bGwsIG5vZGVzLCBcIlwiLCAwLCBiYWROb2RlcywgZnVsbE5vZGVMaXN0KTtcblxuLy8gICAgICAvLyBjYWxjdWxhdGUgdGhlIHNlZ21lbnQgbGF5b3V0XG4vLyAgICAgIC8vdmFyIHNlZ3MgPSBbXCJKYXZhU3RhY2tcIiwgXCJSZWFjdFN0YWNrXCIsXCJDb25maWd1cmF0aW9uXCIsIFwiQXBwXCIsXCJEYXRhYmFzZVwiLFwiTW9uaXRvcmluZ1wiLFwiQ29uZmlnXCIsXCJTZWN1cml0eVwiLFwiRGVwbG95XCIsXCJSdW50aW1lXCIsXCJJbmZyYXN0cnVjdHVyZVwiLFwiRGV2ZWxvcFwiLFwiUmVnaXN0cnlcIl1cbi8vICAgICAgZm9yICggbGV0IHAgPSAwOyBwIDwgc2Vncy5sZW5ndGg7IHArKykge1xuLy8gICAgICAgICAgbGV0IHRvdCA9IDA7XG4vLyAgICAgICAgICBsZXQgeCxudW07XG4vLyAgICAgICAgICBmb3IgKCB4ID0gMCA7IHggPCBncmFwaC5ub2Rlcy5sZW5ndGg7IHgrKykge1xuLy8gICAgICAgICAgICAgIGxldCBzZWcgPSAoZ3JhcGgubm9kZXNbeF0udGl0bGUgJiYgZ3JhcGgubm9kZXNbeF0udGl0bGUuc2VnbWVudCA/IGdyYXBoLm5vZGVzW3hdLnRpdGxlLnNlZ21lbnQgOiBcIlwiKTtcbi8vICAgICAgICAgICAgICBpZiAoc2VnID09IHNlZ3NbcF0pIHtcbi8vICAgICAgICAgICAgICAgICAgdG90Kys7XG4vLyAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgIGlmIChncmFwaC5ub2Rlc1t4XS5jaGlsZHJlbikge1xuLy8gICAgICAgICAgICAgICAgICBsZXQgbHMgPSBncmFwaC5ub2Rlc1t4XS5jaGlsZHJlbjtcbi8vICAgICAgICAgICAgICAgICAgZm9yICggbGV0IG51bUwxID0gMCx4TDEgPSAwIDsgeEwxIDwgbHMubGVuZ3RoOyB4TDErKykge1xuLy8gICAgICAgICAgICAgICAgICAgICAgbGV0IHNlZyA9IChsc1t4TDFdLnRpdGxlICYmIGxzW3hMMV0udGl0bGUuc2VnbWVudCA/IGxzW3hMMV0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8gICAgICAgICAgICAgICAgICAgICAgaWYgKHNlZyA9PSBzZWdzW3BdKSB7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgIHRvdCsrO1xuLy8gICAgICAgICAgICAgICAgICAgICAgfVxuLy9cbi8vICAgICAgICAgICAgICAgICAgICAgIGlmIChsc1t4TDFdLmNoaWxkcmVuKSB7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGxzTDIgPSBsc1t4TDFdLmNoaWxkcmVuO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoIGxldCBudW1MMiA9IDAseEwyID0gMCA7IHhMMiA8IGxzTDIubGVuZ3RoOyB4TDIrKykge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VnID0gKGxzTDJbeEwyXS50aXRsZSAmJiBsc0wyW3hMMl0udGl0bGUuc2VnbWVudCA/IGxzTDJbeEwyXS50aXRsZS5zZWdtZW50IDogXCJcIik7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWcgPT0gc2Vnc1twXSkge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdCsrO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgfVxuLy8gICAgICAgICAgZm9yICggbnVtID0gMCx4ID0gMCA7IHggPCBncmFwaC5ub2Rlcy5sZW5ndGg7IHgrKykge1xuLy8vLyAgICAgICAgICAgICAgdGhpcy5yZWN1cnNlKGdyYXBoLm5vZGVzLCAxKTtcbi8vICAgICAgICAgICAgICBsZXQgc2VnID0gKGdyYXBoLm5vZGVzW3hdLnRpdGxlICYmIGdyYXBoLm5vZGVzW3hdLnRpdGxlLnNlZ21lbnQgPyBncmFwaC5ub2Rlc1t4XS50aXRsZS5zZWdtZW50IDogXCJcIik7XG4vLy8vICAgICAgICAgICAgICBpZiAoc2VnID09IHNlZ3NbcF0pIHtcbi8vLy8gICAgICAgICAgICAgICAgICBncmFwaC5ub2Rlc1t4XS5sYXlvdXQgPSB7IFwic2VxXCI6bnVtLFwidG90YWxcIjp0b3QsXCJzZWdcIjpzZWdzW3BdLCBcInNlZ251bVwiOnAsIFwibGV2ZWxcIjowIH07XG4vLy8vICAgICAgICAgICAgICAgICAgbnVtKys7XG4vLy8vICAgICAgICAgICAgICB9XG4vLy8vICAgICAgICAgICAgICB0aGlzLnJlY3Vyc2UoZ3JhcGgubm9kZXNbeF0sIDEpO1xuLy8vLyAgICAgICAgICAgICAgaWYgKGdyYXBoLm5vZGVzW3hdLmNoaWxkcmVuKSB7XG4vLy8vICAgICAgICAgICAgICAgICAgbGV0IGxzID0gZ3JhcGgubm9kZXNbeF0uY2hpbGRyZW47XG4vLy8vICAgICAgICAgICAgICAgICAgZm9yICggbGV0IG51bUwxID0gMCx4TDEgPSAwIDsgeEwxIDwgbHMubGVuZ3RoOyB4TDErKykge1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VnID0gKGxzW3hMMV0udGl0bGUgJiYgbHNbeEwxXS50aXRsZS5zZWdtZW50ID8gbHNbeEwxXS50aXRsZS5zZWdtZW50IDogXCJcIik7XG4vLy8vICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWcgPT0gc2Vnc1twXSkge1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICAgICAgbHNbeEwxXS5sYXlvdXQgPSB7IFwic2VxXCI6bnVtTDEsXCJ0b3RhbFwiOnRvdCxcInNlZ1wiOnNlZ3NbcF0sIFwic2VnbnVtXCI6cCwgXCJsZXZlbFwiOjEgfTtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgIG51bUwxKys7XG4vLy8vICAgICAgICAgICAgICAgICAgICAgIH1cbi8vLy8gICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWN1cnNlKGxzW3hMMV0sIDIpO1xuLy9cbi8vLy8gICAgICAgICAgICAgICAgICAgICAgaWYgKGxzW3hMMV0uY2hpbGRyZW4pIHtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBsc0wyID0gbHNbeEwxXS5jaGlsZHJlbjtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoIGxldCBudW1MMiA9IDAseEwyID0gMCA7IHhMMiA8IGxzTDIubGVuZ3RoOyB4TDIrKykge1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZWcgPSAobHNMMlt4TDJdLnRpdGxlICYmIGxzTDJbeEwyXS50aXRsZS5zZWdtZW50ID8gbHNMMlt4TDJdLnRpdGxlLnNlZ21lbnQgOiBcIlwiKTtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VnID09IHNlZ3NbcF0pIHtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbHNMMlt4TDJdLmxheW91dCA9IHsgXCJzZXFcIjpudW1MMixcInRvdGFsXCI6dG90LFwic2VnXCI6c2Vnc1twXSwgXCJzZWdudW1cIjpwLCBcImxldmVsXCI6MiB9O1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudW1MMisrO1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbi8vLy8gICAgICAgICAgICAgICAgICAgICAgfVxuLy9cbi8vICAgICAgICAgICAvLyAgICAgICB9XG4vLyAgICAgICAgICAgIC8vICB9XG4vLyAgICAgICAgICB9XG4vLyAgICAgIH1cbiAgfVxuXG4gIHJlY3Vyc2UgKHBhcmVudCwgbm9kZXMsIHJvb3RUYWcsIGxldmVsLCBiYWROb2RlcywgZnVsbE5vZGVMaXN0KSB7XG4gICAgICBsZXQgdG90ID0gMDtcbiAgICAgIGZvciAoIGxldCBudW0gPSAwLHggPSAwIDsgeCA8IG5vZGVzLmxlbmd0aDsgeCsrKSB7XG4gICAgICAgICAgY29uc3QgbmQgPSBub2Rlc1t4XTtcbiAgICAgICAgICBpZiAobmQudHlwZSA9PSBcInRhYlwiKSB7XG4gICAgICAgICAgICB0b3QrKztcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICBmb3IgKCBsZXQgbnVtID0gMCx4ID0gMCA7IHggPCBub2Rlcy5sZW5ndGg7IHgrKykge1xuICAgICAgICAgIGNvbnN0IG5kID0gbm9kZXNbeF07XG4gICAgICAgICAgaWYgKG5kLnR5cGUgPT0gXCJ0YWJcIikge1xuICAgICAgICAgICAgICBuZC5sYXlvdXQgPSB7IFwic2VxXCI6bnVtLFwidG90YWxcIjp0b3QsXCJzZWdcIjogcm9vdFRhZywgXCJzZWdudW1cIjpsZXZlbCwgXCJsZXZlbFwiOmxldmVsLCBcInBhcmVudFwiOiAocGFyZW50ID09IG51bGwgPyBudWxsOnBhcmVudC5uYW1lKSB9O1xuICAgICAgICAgICAgICBudW0rKztcblxuICAgICAgICAgICAgICBpZiAobmQuaGFzT3duUHJvcGVydHkoXCJuYW1lXCIpID09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoXCJXYXJuaW5nOiBUYWIgbm9kZXMgcmVxdWlyZSBuYW1lIFwiICsgSlNPTi5zdHJpbmdpZnkobmQpKTtcbiAgICAgICAgICAgICAgICBiYWROb2Rlcy5wdXNoKG5kKTtcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChuZC5uYW1lIGluIGZ1bGxOb2RlTGlzdCkge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiV2FybmluZzogVGFiIG5vZGUgbmFtZXMgbXVzdCBiZSB1bmlxdWUgXCIgKyBKU09OLnN0cmluZ2lmeShuZCkpO1xuICAgICAgICAgICAgICAgIGJhZE5vZGVzLnB1c2gobmQpO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGZ1bGxOb2RlTGlzdFtuZC5uYW1lXSA9IG5kO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgaWYgKG5kLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLnJlY3Vyc2UobmQsIG5kLmNoaWxkcmVuLCByb290VGFnICsgXCJfTFwiICsgKGxldmVsKSArIFwiVFwiICsgKG51bS0xKSwgbGV2ZWwrMSwgYmFkTm9kZXMsIGZ1bGxOb2RlTGlzdCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICB9XG4gIH1cblxuICBjb25maWdFdmVudHMoZDN2aXN1YWwsIG5ld05vZGVzVG90YWwsIHpvbmVzLCB2aWV3ZXIpIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgICAgbGV0IG5ld05vZGVzID0gbmV3Tm9kZXNUb3RhbC5maWx0ZXIoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC50eXBlID09ICd0YWInOyB9KTtcblxuICAgICAgbGV0IGZvcmNlID0gdmlld2VyLmZvcmNlO1xuXG4gICAgICAvLyBVc2UgeCBhbmQgeSBpZiBpdCB3YXMgcHJvdmlkZWQsIG90aGVyd2lzZSBzdGljayBpdCBpbiB0aGUgY2VudGVyIG9mIHRoZSB6b25lXG4gICAgICBuZXdOb2Rlc1xuICAgICAgICAgIC5hdHRyKFwiY3hcIiwgZnVuY3Rpb24oZCkgeyBjb25zdCBkaW0gPSBzZWxmLmdldERpbShkLCB6b25lcyk7IHJldHVybiBkLnggPSAoZC54ID8gZC54IDogKGRpbS54MSArIChkaW0ueDItZGltLngxKS8yKSk7IH0pXG4gICAgICAgICAgLmF0dHIoXCJjeVwiLCBmdW5jdGlvbihkKSB7IGNvbnN0IGRpbSA9IHNlbGYuZ2V0RGltKGQsIHpvbmVzKTsgcmV0dXJuIGQueSA9IChkLnkgPyBkLnkgOiAoZGltLnkxICsgKGRpbS55Mi1kaW0ueTEpLzIpKTsgfSk7XG5cbi8vbm9kZVNldCwgaW5kZXhcbiAgICAgIG5ld05vZGVzXG4gICAgICAgIC5vbignbW91c2VlbnRlcicsIGZ1bmN0aW9uIChlKSB7XG4vLyAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJub2RlLWhvdmVyXCIpO1xuLy8gICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5PREU6IFwiICtKU09OLnN0cmluZ2lmeShub2RlU2V0KSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbignbW91c2VsZWF2ZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAvLyQodGhpcykucmVtb3ZlQ2xhc3MoXCJub2RlLWhvdmVyXCIpO1xuICAgICAgICB9KVxuICAgICAgICAub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHsgdmlld2VyLnRyaWdnZXIoXCJ0YWJDbGlja1wiLCBlKTsgLypzZWxmLnRvZ2dsZVRhYih2aWV3ZXIsIGUsIHRoaXMpOyovIC8qc2VsZi50b2dnbGVOYXZpZ2F0aW9uKHZpZXdlciwgZSwgdGhpcyk7ICovfSk7XG5cbiAgICAgICAgLy8gTk8gRFJBRy9EUk9QIHN1cHBvcnQgZm9yIHRhYnNcbiAgICAgICAgZnVuY3Rpb24gZHJhZ3N0YXJ0ZWQoZCkge1xuICAgICAgICAgICAgdmlld2VyLmRvd25YID0gTWF0aC5yb3VuZChkLnggKyBkLnkpO1xuICAgICAgICB9XG4gICAgICAgIG5ld05vZGVzLmNhbGwoZDMuZHJhZygpXG4gICAgICAgICAgICAub24oXCJzdGFydFwiLCBkcmFnc3RhcnRlZCkpO1xuXG4gICAgICAgIHNlbGYuYXR0YWNoSW5mb0NvbnRyb2xsZXIodmlld2VyLCBuZXdOb2Rlcyk7XG4gIH1cblxuICBhdHRhY2hJbmZvQ29udHJvbGxlcih2aWV3ZXIsIGNoZ1NldCkge1xuICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAgIGNoZ1NldC5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQuaGFzT3duUHJvcGVydHkoXCJub3Rlc1wiKTsgfSkuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgICAudGV4dChmdW5jdGlvbihkKSB7IHJldHVybiBmYSgnaW5mby1jaXJjbGUnKTsgfSlcbiAgICAgICAgICAuY2xhc3NlZChcImRlY29yYXRpb25cIiwgdHJ1ZSlcbiAgICAgICAgICAuY2xhc3NlZChcIm9wdGlvbl94XCIsIHRydWUpXG4gICAgICAgICAgLmNsYXNzZWQoXCJpY29uXCIsIHRydWUpXG4gICAgICAgICAgLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSkgeyBzZWxmLnRvZ2dsZU5hdmlnYXRpb24odmlld2VyLCBlLCB0aGlzLnBhcmVudE5vZGUpOyB9KTtcbiAgfVxuXG5cbiAgZ2V0RGltKGQsIHpvbmVzKSB7XG4gICAgICBjb25zdCBpbmRleCA9IGQuYm91bmRhcnkuY2hhckNvZGVBdCgwKSAtIDY1O1xuICAgICAgaWYgKHpvbmVzLmxlbmd0aCA8PSBpbmRleCkge1xuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJJTlZBTElEIEJPVU5EQVJZOiBcIiArIGQuYm91bmRhcnkpO1xuICAgICAgfVxuICAgICAgY29uc3QgcmVjdCA9ICh6b25lc1tpbmRleF0uaGFzT3duUHJvcGVydHkoJ2NhbGN1bGF0ZWRSZWN0YW5nbGUnKSA/IHpvbmVzW2luZGV4XS5jYWxjdWxhdGVkUmVjdGFuZ2xlIDogem9uZXNbaW5kZXhdLnJlY3RhbmdsZSk7XG4gICAgICBjb25zdCBkaW0gPSB7eDE6cmVjdFswXSx5MTpyZWN0WzFdLHgyOnJlY3RbMF0gKyByZWN0WzJdLHkyOnJlY3RbMV0gKyByZWN0WzNdfTtcbiAgICAgIC8vY29uc29sZS5sb2coXCJLOiBcIiArIEpTT04uc3RyaW5naWZ5KGRpbSwgbnVsbCwgMikpO1xuICAgICAgcmV0dXJuIGRpbTtcbiAgfVxuXG4gIG9yZ2FuaXplKGQzdmlzdWFsLCB6b25lSW5kZXgsIHpvbmVzKSB7XG5cbiAgICAgICBsZXQgdmIgPSBkM3Zpc3VhbC5hdHRyKFwidmlld0JveFwiKS5zcGxpdCgnICcpO1xuXG4gICAgICAgbGV0IHZpZXdXaWR0aCA9IHZiWzJdO1xuXG4gICAgICAgbGV0IG1heFdpZHRoID0gdmlld1dpZHRoIC0gMDtcblxuICAgICAgIGQzdmlzdWFsLnNlbGVjdChcImcubm9kZVNldFwiKS5zZWxlY3RBbGwoXCJnLm5vZGUsIGcubm9kZWNcIikuZmlsdGVyKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgIGNvbnN0IGluZGV4ID0gZC5ib3VuZGFyeS5jaGFyQ29kZUF0KDApIC0gNjU7XG4gICAgICAgICAgIHJldHVybiAoaW5kZXggPT0gem9uZUluZGV4KTtcbiAgICAgICB9KVxuICAgICAgIC5lYWNoKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgIGQueCA9ICgoKG1heFdpZHRoL2QubGF5b3V0LnRvdGFsKSAtIDUpLzIpICsgKGQubGF5b3V0LnNlcSAqIChtYXhXaWR0aC9kLmxheW91dC50b3RhbCkpO1xuICAgICAgICAgICBkLnkgPSAoNTAvMikgKyAoZC5sYXlvdXQuc2VnbnVtICogNTUpO1xuICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5zZWxlY3QoXCJyZWN0XCIpXG4gICAgICAgICAgICAuYXR0cihcInhcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIC0oKG1heFdpZHRoL2QubGF5b3V0LnRvdGFsKSAtIDUpLzI7IH0gKVxuICAgICAgICAgICAgLmF0dHIoXCJ5XCIsIGZ1bmN0aW9uIChkKSB7IHJldHVybiAtZDMuc2VsZWN0KHRoaXMpLmF0dHIoXCJoZWlnaHRcIikgLyAyOyB9IClcbiAgICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIChtYXhXaWR0aC9kLmxheW91dC50b3RhbCk7IH0gKVxuXG4gICAgICAgICAgZDMuc2VsZWN0KHRoaXMpLnNlbGVjdChcInRleHQub3B0aW9uX3hcIikuYXR0cihcInRyYW5zZm9ybVwiLCBmdW5jdGlvbihhKSB7XG4gICAgICAgICAgICByZXR1cm4gXCJ0cmFuc2xhdGUoXCIgKyAoKChtYXhXaWR0aC9kLmxheW91dC50b3RhbCkvMiktMTUpICsgXCIsIC0xMClcIjtcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5zZWxlY3QoXCJ0ZXh0XCIpLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oYSkge1xuICAgICAgICAgICAgY29uc3QgYmJveCA9IHRoaXMuZ2V0QkJveCgpO1xuICAgICAgICAgICAgY29uc3QgcGJib3ggPSBkMy5zZWxlY3QodGhpcy5wYXJlbnROb2RlKS5zZWxlY3QoXCJyZWN0XCIpLm5vZGUoKS5nZXRCQm94KCk7XG4gICAgICAgICAgICBsZXQgc2NhbGUgPSAxO1xuICAgICAgICAgICAgaWYgKChwYmJveC5oZWlnaHQgLyBiYm94LmhlaWdodCkgPCBwYmJveC53aWR0aCAvIChiYm94LndpZHRoICsgMjApICYmIChwYmJveC5oZWlnaHQgLyBiYm94LmhlaWdodCkgPCAxKSB7XG4gICAgICAgICAgICAgICAgc2NhbGUgPSAocGJib3guaGVpZ2h0IC8gYmJveC5oZWlnaHQpID09IDAgPyAxOihwYmJveC5oZWlnaHQgLyBiYm94LmhlaWdodCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHNjYWxlID0gTWF0aC5taW4ocGJib3gud2lkdGggLyAoYmJveC53aWR0aCArIDIwKSwgMSkgPT0gMCA/IDE6TWF0aC5taW4ocGJib3gud2lkdGggLyAoYmJveC53aWR0aCArIDIwKSwgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gXCJzY2FsZShcIiArIHNjYWxlICsgXCIpXCI7XG5cbiAgICAgICAgICB9KTtcbiAgICAgICB9KTtcblxuICAgICAgICBkM3Zpc3VhbC5zZWxlY3QoXCJnLm5vZGVTZXRcIikuc2VsZWN0QWxsKFwiZy50YWJcIikuZmlsdGVyKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgIHJldHVybiAoZC5sYXlvdXQubGV2ZWwgIT0gMCk7XG4gICAgICAgIH0pLnN0eWxlKFwib3BhY2l0eVwiLCAwKS5zdHlsZShcImRpc3BsYXlcIixcIm5vbmVcIik7XG5cbi8qXG4gICAgICAgLy9tYXhXaWR0aCA9IDgwMDtcbiAgICAgICBkM3Zpc3VhbC5zZWxlY3QoXCJnLm5vZGVTZXRcIikuc2VsZWN0QWxsKFwiZy5ub2RlY1wiKS5maWx0ZXIoZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgY29uc3QgaW5kZXggPSBkLmJvdW5kYXJ5LmNoYXJDb2RlQXQoMCkgLSA2NTtcbiAgICAgICAgICAgcmV0dXJuIChpbmRleCA9PSB6b25lSW5kZXgpO1xuICAgICAgIH0pXG4gICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oZCkge1xuICAgICAgICAgICAgbGV0IG9mZnNldFggPSBkLmxheW91dC5zZXE7XG4gICAgICAgICAgICByZXR1cm4gXCJ0cmFuc2xhdGUoXCIgKygoKG1heFdpZHRoL2QubGF5b3V0LnRvdGFsKS8yKSsob2Zmc2V0WCAqIChtYXhXaWR0aC9kLmxheW91dC50b3RhbCkpKSArIFwiLCBcIiArICg1MCsoZC5sYXlvdXQubGV2ZWwgKiA1MikpICsgXCIpXCI7IH0gKVxuICAgICAgIC5lYWNoKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgIGQueCA9ICgobWF4V2lkdGgvZC5sYXlvdXQudG90YWwpLzIpICsgKGQubGF5b3V0LnNlcSAqIChtYXhXaWR0aC9kLmxheW91dC50b3RhbCkpO1xuICAgICAgICAgICBkLnkgPSA1MCArIChkLmxheW91dC5zZWdudW0gKiA1NSk7XG5cbi8vICAgICAgICAgICBpZiAoIWQubGF5b3V0KSB7XG4vLyAgICAgICAgICAgICAgICBkLmxheW91dCA9IHtcInNlZ251bVwiOjIsIFwidG90YWxcIjozfTtcbi8vICAgICAgICAgICB9XG4gICAgICAgICAgIC8vZC54ID0gMTA1ICsgKHNlbGYuYWFbZC5sYXlvdXQuc2VnbnVtXSAqIChtYXhXaWR0aC9kLmxheW91dC50b3RhbCkpO1xuICAgICAgICAgICAvL2QueSA9IDcwICsgKGQubGF5b3V0LnNlZ251bSAqIDc1KTtcbi8vICAgICAgICAgICBzZWxmLmFhW2QubGF5b3V0LnNlZ251bV0gPSBzZWxmLmFhW2QubGF5b3V0LnNlZ251bV0gKyAxO1xuLy8gICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5hdHRyKFwidHJhbnNmb3JtXCIsIFwidHJhbnNsYXRlKFwiICsgZC54ICsgXCIsXCIgKyBkLnkgKyBcIilcIik7XG4gICAgICAgICAgZDMuc2VsZWN0KHRoaXMpLnNlbGVjdChcInJlY3RcIilcbiAgICAgICAgICAgIC5hdHRyKFwieFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gLSgobWF4V2lkdGgvZC5sYXlvdXQudG90YWwpIC0gNSkvMjsgfSApXG4gICAgICAgICAgICAuYXR0cihcInlcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIC1kMy5zZWxlY3QodGhpcykuYXR0cihcImhlaWdodFwiKSAvIDI7IH0gKVxuICAgICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gKG1heFdpZHRoL2QubGF5b3V0LnRvdGFsKSAtIDU7IH0gKTtcblxuICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5zZWxlY3QoXCJ0ZXh0Lm9wdGlvbl94XCIpLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24oYSkge1xuICAgICAgICAgICAgcmV0dXJuIFwidHJhbnNsYXRlKFwiICsgKCgobWF4V2lkdGgvZC5sYXlvdXQudG90YWwpLzIpLTE1KSArIFwiLCAtMTApXCI7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICBkMy5zZWxlY3QodGhpcykuc2VsZWN0KFwidGV4dFwiKS5hdHRyKFwidHJhbnNmb3JtXCIsIGZ1bmN0aW9uKGEpIHtcbiAgICAgICAgICAgIGNvbnN0IGJib3ggPSB0aGlzLmdldEJCb3goKTsgY29uc3QgcGJib3ggPSBkMy5zZWxlY3QodGhpcy5wYXJlbnROb2RlKS5zZWxlY3QoXCJyZWN0XCIpLm5vZGUoKS5nZXRCQm94KCk7XG4gICAgICAgICAgICByZXR1cm4gXCJzY2FsZShcIiArIE1hdGgubWluKHBiYm94LndpZHRoIC8gKGJib3gud2lkdGggKyAyMCksIDEpICsgXCIpXCI7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLy9kMy5zZWxlY3QodGhpcykuc2VsZWN0KFwicmVjdFwiKS5hdHRyKFwid2lkdGhcIiwgKHNlbGYud2lkdGggLSA1KSk7XG4gICAgICAgfSk7XG4gICAgICAgKi9cbiAgfVxuXG5cbiAgdG9nZ2xlTmF2aWdhdGlvbiAodmlld2VyLCBlLCBvYmplY3QpIHtcbiAgICAgZDMuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgIGlmICh2aWV3ZXIuZG93blggPT0gTWF0aC5yb3VuZChlLnggKyBlLnkpKSB7XG4gICAgICAgIHZpZXdlci5uYXZpZ2F0aW9uLnRvZ2dsZShlLCBkMy5zZWxlY3Qob2JqZWN0KSk7XG4gICAgIH0gZWxzZSB7XG4gICAgICAgIHZpZXdlci5uYXZpZ2F0aW9uLmNsb3NlKGUpO1xuICAgICB9XG4gIH1cblxuICB0b2dnbGVUYWIgKHZpZXdlciwgZCwgY3VycmVudFRhYikge1xuICAgICBsZXQgc2VsZiA9IHRoaXM7XG5cbiAgICAgLy9kMy5ldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgICAvLyBnZXQgdGhlIGNoaWxkIHRhYnNcbiAgICAgbGV0IGNoaWxkcmVuID0gZDMuc2VsZWN0KGN1cnJlbnRUYWIucGFyZW50Tm9kZSkuc2VsZWN0QWxsKFwiZy5ub2RlX1wiK2QubmFtZSk7XG5cblxuICAgICBpZiAoY2hpbGRyZW4uc2l6ZSgpID4gMCkge1xuXG4gICAgICAgICBsZXQgdG9nZ2xlT24gPSBjaGlsZHJlbi5zdHlsZShcImRpc3BsYXlcIikgPT0gXCJub25lXCI7XG5cbiAgICAgICAgIHRoaXMudHVybk9mZlByZXZpb3VzVGFiKGQsIGN1cnJlbnRUYWIpO1xuXG4vLyAgICAgICAgIGlmICh0b2dnbGVPbikge1xuICAgICAgICAgICAgIGNoaWxkcmVuLnN0eWxlKFwib3BhY2l0eVwiLCAwKS50cmFuc2l0aW9uKCkuZHVyYXRpb24oMTAwMCkuc3R5bGUoXCJvcGFjaXR5XCIsIDEpLnN0eWxlKFwiZGlzcGxheVwiLFwiYmxvY2tcIik7XG4gICAgICAgICAgICAgZDMuc2VsZWN0KGN1cnJlbnRUYWIpLmNsYXNzZWQoXCJzZWxlY3RlZF90YWJcIiwgdHJ1ZSk7XG4gICAgICAgICAgICAgLy9kMy5zZWxlY3QoY3VycmVudFRhYi5wYXJlbnROb2RlKS5zZWxlY3QoXCJ0ZXh0Lm9wdGlvbl94XCIpLnRleHQoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIFwiLVwiOyB9KTtcbi8vICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAvL2NoaWxkcmVuLnRyYW5zaXRpb24oKS5kdXJhdGlvbigxMDAwKS5zdHlsZShcIm9wYWNpdHlcIiwgMCkuc3R5bGUoXCJkaXNwbGF5XCIsXCJub25lXCIpOyAvLy5lYWNoKFwiZW5kXCIsIGZ1bmN0aW9uIChlKSB7IGQzLnNlbGVjdCh0aGlzKS5zdHlsZShcImRpc3BsYXlcIixcIm5vbmVcIilcbiAgICAgICAgICAgICAvL2QzLnNlbGVjdChjdXJyZW50VGFiKS5jbGFzc2VkKFwic2VsZWN0ZWRfdGFiXCIsIGZhbHNlKTtcbiAgICAgICAgICAgICAvL2QzLnNlbGVjdChjdXJyZW50VGFiLnBhcmVudE5vZGUpLnNlbGVjdChcInRleHQub3B0aW9uX3hcIikudGV4dChmdW5jdGlvbiAoZCkgeyByZXR1cm4gXCIrXCI7IH0pO1xuLy8gICAgICAgICB9XG4gICAgIH0gZWxzZSB7XG5cbiAgICAgICAgLy8gaWYgdGhlcmUgYXJlIG5vIGNoaWxkIHRhYnMsIGxvb2sgZm9yIG90aGVyIGNvbnRlbnQsIHN1Y2ggYXMgYW5vdGhlciBzdmcvc2NyZWVuc2hvdC9ldGNcbiAgICAgICAgdGhpcy50dXJuT2ZmUHJldmlvdXNUYWIoZCwgY3VycmVudFRhYik7XG5cbiAgICAgICAgaWYgKGQzLnNlbGVjdChcIi5wYWdlXCIpLnNpemUoKSA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLnR1cm5PZmZQcmV2aW91c1RhYihkLCBjdXJyZW50VGFiKTtcbiAgICAgICAgICAgIGQzLnNlbGVjdChjdXJyZW50VGFiKS5jbGFzc2VkKFwic2VsZWN0ZWRfdGFiXCIsIHRydWUpO1xuXG4gICAgICAgICAgICBpZiAoIWQuY29udGVudCkge1xuICAgICAgICAgICAgICAgIGxldCBjb250ZW50ID0gZDMuc2VsZWN0KHZpZXdlci5kb21Ob2RlLnBhcmVudE5vZGUpLmFwcGVuZChcImRpdlwiKS5jbGFzc2VkKFwicGFnZVwiLCB0cnVlKTtcbiAgICAgICAgICAgICAgICBjb250ZW50LnN0eWxlKFwidG9wXCIsIFwiMHB4XCIpO1xuICAgICAgICAgICAgICAgIGNvbnRlbnQuaHRtbChcIm5vIGNvbnRlbnRcIik7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgdXJsID0gKGQuY29udGVudC51cmwuaW5kZXhPZignPycpID09IC0xID8gZC5jb250ZW50LnVybCArIFwiP3I9XCIgKyBNYXRoLnJhbmRvbSgpIDogZC5jb250ZW50LnVybCArIFwiJnI9XCIgKyBNYXRoLnJhbmRvbSgpKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT3BlbmluZzogXCIgK3VybCk7XG4gICAgICAgICAgICBkMy5qc29uKHVybCwgZnVuY3Rpb24oanNvbikge1xuXG5cbiAgICAgICAgICAgICAgICBsZXQgY29udGVudCA9IGQzLnNlbGVjdCh2aWV3ZXIuZG9tTm9kZS5wYXJlbnROb2RlKS5hcHBlbmQoXCJkaXZcIikuY2xhc3NlZChcInBhZ2VcIiwgdHJ1ZSk7XG5cbiAgICAgICAgICAgICAgICAvL2NvbnRlbnQuY2xhc3NlZChcImxldmVsX1wiICsgZC5sYXlvdXQubGV2ZWwsIFwidHJ1ZVwiKTtcblxuICAgICAgICAgICAgICAgIC8vY29udGVudC5zdHlsZShcIm92ZXJmbG93XCIsIFwiaGlkZGVuXCIpO1xuXG4gICAgICAgICAgICAgICAgc2VsZi5yZWZyZXNoU2l6ZSAodmlld2VyKTtcblxuICAgICAgICAgICAgICAgIGxldCB2YyA9IG5ldyBWaWV3ZXJDYW52YXMoKTtcbiAgICAgICAgICAgICAgICB2Yy5vbihcImNoYW5nZVwiLCBmdW5jdGlvbihldmVudCwgX2lycmVsZXZhbnRfZGF0YSwgZG9jKSB7IHZpZXdlci50cmlnZ2VyKGV2ZW50LCBkLCBkb2MpOyB9KTtcbiAgICAgICAgICAgICAgICB2Yy5tb3VudE5vZGUgKGNvbnRlbnQubm9kZSgpKTtcbiAgICAgICAgICAgICAgICB2Yy5zZXRTdGF0ZShqc29uKTtcbiAgICAgICAgICAgICAgICB2Yy5yZW5kZXIoKTtcbiAgICAgICAgICAgICAgICB2Yy5yZWZyZXNoU2l6ZSgpO1xuXG4gICAgICAgICAgICAgICAgc2VsZi52YyA9IHZjO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgfVxuICB9XG5cbiAgcmVmcmVzaFNpemUgKHZpZXdlcikge1xuICAgIGxldCB2YiA9IGQzLnNlbGVjdCh2aWV3ZXIuZG9tTm9kZSkuYXR0cihcInZpZXdCb3hcIikuc3BsaXQoJyAnKTtcblxuICAgIGxldCB0YWIgPSBkMy5zZWxlY3Qodmlld2VyLmRvbU5vZGUpLnNlbGVjdCgnZy5ub2RlU2V0Jykubm9kZSgpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgIGxldCBib2R5UmVjdCA9IGRvY3VtZW50LmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICBsZXQgdmlld1dpZHRoID0gdmJbMl07XG4gICAgbGV0IHZpZXdIZWlnaHQgPSB2YlszXTtcblxuICAgIGxldCBwYWdlV2lkdGggPSBkMy5zZWxlY3Qodmlld2VyLmRvbU5vZGUpLmF0dHIoXCJ3aWR0aFwiKTtcbiAgICBsZXQgcGFnZUhlaWdodCA9IGQzLnNlbGVjdCh2aWV3ZXIuZG9tTm9kZSkuYXR0cihcImhlaWdodFwiKTtcblxuICAgIHBhZ2VXaWR0aCA9IGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGg7XG4gICAgcGFnZUhlaWdodCA9IGRvY3VtZW50LmJvZHkuY2xpZW50SGVpZ2h0O1xuXG4gICAgbGV0IHRvcCA9IE1hdGguY2VpbCgodGFiLnRvcCAtIGJvZHlSZWN0LnRvcCkgKyB0YWIuaGVpZ2h0ICsgMik7XG5cbiAgICBsZXQgZnVsbCA9IHtcImhlaWdodFwiOnBhZ2VIZWlnaHQsIFwid2lkdGhcIjpwYWdlV2lkdGh9O1xuXG4gICAgbGV0IGNvbnRlbnQgPSBkMy5zZWxlY3Qodmlld2VyLmRvbU5vZGUucGFyZW50Tm9kZSkuc2VsZWN0KCcucGFnZScpO1xuXG4gICAgY29udGVudC5zdHlsZShcInRvcFwiLCB0b3AgKyBcInB4XCIpO1xuICAgIGNvbnRlbnQuc3R5bGUoXCJsZWZ0XCIsIFwiMHB4XCIpO1xuICAgIGNvbnRlbnQuc3R5bGUoXCJ3aWR0aFwiLCAoZnVsbC53aWR0aCkgKyBcInB4XCIpO1xuICAgIGNvbnRlbnQuc3R5bGUoXCJoZWlnaHRcIiwgKGZ1bGwuaGVpZ2h0ICAtICh0b3ApKSArIFwicHhcIik7XG5cbiAgICBpZiAodGhpcy52Yykge1xuICAgICAgICB0aGlzLnZjLnJlZnJlc2hTaXplKCk7XG4gICAgfVxuICB9XG5cbiAgdHVybk9mZlByZXZpb3VzVGFiKHRhYkRhdGEsIGN1cnJlbnRUYWIpIHtcbiAgICAgLy8gdHVybiBvZmYgYW55IHByZXZpb3VzbHkgc2VsZWN0ZWQgdGFiXG4gICAgIGQzLnNlbGVjdChjdXJyZW50VGFiLnBhcmVudE5vZGUpLnNlbGVjdEFsbChcImcuc2VsZWN0ZWRfdGFiXCIpLmVhY2goZnVuY3Rpb24gKHApIHtcbiAgICAgICAgIC8vIEZvciBhbGwgdGFicyB0aGF0IGFyZSBzZWxlY3RlZCBhdCBuZXcgdGFiIGxldmVsIG9yIGhpZ2hlciAobG93ZXIgaW4gcG9zaXRpb24pXG4gICAgICAgICBpZiAodGFiRGF0YS5sYXlvdXQubGV2ZWwgPD0gcC5sYXlvdXQubGV2ZWwpIHtcbiAgICAgICAgICAgICBkMy5zZWxlY3QodGhpcykuY2xhc3NlZChcInNlbGVjdGVkX3RhYlwiLCBmYWxzZSk7XG4gICAgICAgICAgICAgbGV0IHNlbGVjdGVkQ2hpbGRyZW4gPSBkMy5zZWxlY3QoY3VycmVudFRhYi5wYXJlbnROb2RlKS5zZWxlY3RBbGwoXCJnLm5vZGVfXCIrcC5uYW1lKTtcbiAgICAgICAgICAgICBzZWxlY3RlZENoaWxkcmVuLnRyYW5zaXRpb24oKS5zdHlsZShcIm9wYWNpdHlcIiwgMCkuc3R5bGUoXCJkaXNwbGF5XCIsXCJub25lXCIpO1xuICAgICAgICAgfVxuICAgICB9KTtcbiAgICAgLy9kMy5zZWxlY3QoY3VycmVudFRhYikuY2xhc3NlZChcInNlbGVjdGVkX3RhYlwiLCBmYWxzZSk7XG5cbi8vICAgICBsZXQgc2VsZiA9IHRoaXM7XG4vLyAgICAgaWYgKHR5cGVvZiBzZWxmLnZjICE9IFwidW5kZWZpbmVkXCIgJiYgc2VsZi52YyAhPSBudWxsKSB7XG4vLyAgICAgICAgY29uc29sZS5sb2coXCJkZXN0cm95XCIpO1xuLy8gICAgICAgIHNlbGYudmMuZGVzdHJveSgpO1xuLy8gICAgICAgIHNlbGYudmMgPSBudWxsO1xuLy8gICAgIH1cbi8vXG4gICAgIC8vZDMuc2VsZWN0KFwiI2Jhc2VcIikuc2VsZWN0QWxsKFwiLnBhZ2VcIikuc2VsZWN0QWxsKCkucmVtb3ZlKCk7XG4gICAgIGQzLnNlbGVjdChjdXJyZW50VGFiLnBhcmVudE5vZGUucGFyZW50Tm9kZS5wYXJlbnROb2RlKS5zZWxlY3RBbGwoXCIucGFnZVwiKS5yZW1vdmUoKTtcblxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IExheW91dDsiLCJleHBvcnQgeyBob29rcywgc2V0SG9va0NhbGxiYWNrIH07XG5cbnZhciBob29rQ2FsbGJhY2s7XG5cbmZ1bmN0aW9uIGhvb2tzICgpIHtcbiAgICByZXR1cm4gaG9va0NhbGxiYWNrLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG59XG5cbi8vIFRoaXMgaXMgZG9uZSB0byByZWdpc3RlciB0aGUgbWV0aG9kIGNhbGxlZCB3aXRoIG1vbWVudCgpXG4vLyB3aXRob3V0IGNyZWF0aW5nIGNpcmN1bGFyIGRlcGVuZGVuY2llcy5cbmZ1bmN0aW9uIHNldEhvb2tDYWxsYmFjayAoY2FsbGJhY2spIHtcbiAgICBob29rQ2FsbGJhY2sgPSBjYWxsYmFjaztcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGlzQXJyYXkoaW5wdXQpIHtcbiAgICByZXR1cm4gaW5wdXQgaW5zdGFuY2VvZiBBcnJheSB8fCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoaW5wdXQpID09PSAnW29iamVjdCBBcnJheV0nO1xufVxuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaXNPYmplY3QoaW5wdXQpIHtcbiAgICAvLyBJRTggd2lsbCB0cmVhdCB1bmRlZmluZWQgYW5kIG51bGwgYXMgb2JqZWN0IGlmIGl0IHdhc24ndCBmb3JcbiAgICAvLyBpbnB1dCAhPSBudWxsXG4gICAgcmV0dXJuIGlucHV0ICE9IG51bGwgJiYgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGlucHV0KSA9PT0gJ1tvYmplY3QgT2JqZWN0XSc7XG59XG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpc09iamVjdEVtcHR5KG9iaikge1xuICAgIHZhciBrO1xuICAgIGZvciAoayBpbiBvYmopIHtcbiAgICAgICAgLy8gZXZlbiBpZiBpdHMgbm90IG93biBwcm9wZXJ0eSBJJ2Qgc3RpbGwgY2FsbCBpdCBub24tZW1wdHlcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGlzTnVtYmVyKGlucHV0KSB7XG4gICAgcmV0dXJuIHR5cGVvZiBpbnB1dCA9PT0gJ251bWJlcicgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGlucHV0KSA9PT0gJ1tvYmplY3QgTnVtYmVyXSc7XG59XG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpc0RhdGUoaW5wdXQpIHtcbiAgICByZXR1cm4gaW5wdXQgaW5zdGFuY2VvZiBEYXRlIHx8IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChpbnB1dCkgPT09ICdbb2JqZWN0IERhdGVdJztcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG1hcChhcnIsIGZuKSB7XG4gICAgdmFyIHJlcyA9IFtdLCBpO1xuICAgIGZvciAoaSA9IDA7IGkgPCBhcnIubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgcmVzLnB1c2goZm4oYXJyW2ldLCBpKSk7XG4gICAgfVxuICAgIHJldHVybiByZXM7XG59XG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBoYXNPd25Qcm9wKGEsIGIpIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGEsIGIpO1xufVxuIiwiaW1wb3J0IGhhc093blByb3AgZnJvbSAnLi9oYXMtb3duLXByb3AnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBleHRlbmQoYSwgYikge1xuICAgIGZvciAodmFyIGkgaW4gYikge1xuICAgICAgICBpZiAoaGFzT3duUHJvcChiLCBpKSkge1xuICAgICAgICAgICAgYVtpXSA9IGJbaV07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoaGFzT3duUHJvcChiLCAndG9TdHJpbmcnKSkge1xuICAgICAgICBhLnRvU3RyaW5nID0gYi50b1N0cmluZztcbiAgICB9XG5cbiAgICBpZiAoaGFzT3duUHJvcChiLCAndmFsdWVPZicpKSB7XG4gICAgICAgIGEudmFsdWVPZiA9IGIudmFsdWVPZjtcbiAgICB9XG5cbiAgICByZXR1cm4gYTtcbn1cbiIsImltcG9ydCB7IGNyZWF0ZUxvY2FsT3JVVEMgfSBmcm9tICcuL2Zyb20tYW55dGhpbmcnO1xuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlVVRDIChpbnB1dCwgZm9ybWF0LCBsb2NhbGUsIHN0cmljdCkge1xuICAgIHJldHVybiBjcmVhdGVMb2NhbE9yVVRDKGlucHV0LCBmb3JtYXQsIGxvY2FsZSwgc3RyaWN0LCB0cnVlKS51dGMoKTtcbn1cbiIsImZ1bmN0aW9uIGRlZmF1bHRQYXJzaW5nRmxhZ3MoKSB7XG4gICAgLy8gV2UgbmVlZCB0byBkZWVwIGNsb25lIHRoaXMgb2JqZWN0LlxuICAgIHJldHVybiB7XG4gICAgICAgIGVtcHR5ICAgICAgICAgICA6IGZhbHNlLFxuICAgICAgICB1bnVzZWRUb2tlbnMgICAgOiBbXSxcbiAgICAgICAgdW51c2VkSW5wdXQgICAgIDogW10sXG4gICAgICAgIG92ZXJmbG93ICAgICAgICA6IC0yLFxuICAgICAgICBjaGFyc0xlZnRPdmVyICAgOiAwLFxuICAgICAgICBudWxsSW5wdXQgICAgICAgOiBmYWxzZSxcbiAgICAgICAgaW52YWxpZE1vbnRoICAgIDogbnVsbCxcbiAgICAgICAgaW52YWxpZEZvcm1hdCAgIDogZmFsc2UsXG4gICAgICAgIHVzZXJJbnZhbGlkYXRlZCA6IGZhbHNlLFxuICAgICAgICBpc28gICAgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgcGFyc2VkRGF0ZVBhcnRzIDogW10sXG4gICAgICAgIG1lcmlkaWVtICAgICAgICA6IG51bGxcbiAgICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBnZXRQYXJzaW5nRmxhZ3MobSkge1xuICAgIGlmIChtLl9wZiA9PSBudWxsKSB7XG4gICAgICAgIG0uX3BmID0gZGVmYXVsdFBhcnNpbmdGbGFncygpO1xuICAgIH1cbiAgICByZXR1cm4gbS5fcGY7XG59XG4iLCJ2YXIgc29tZTtcbmlmIChBcnJheS5wcm90b3R5cGUuc29tZSkge1xuICAgIHNvbWUgPSBBcnJheS5wcm90b3R5cGUuc29tZTtcbn0gZWxzZSB7XG4gICAgc29tZSA9IGZ1bmN0aW9uIChmdW4pIHtcbiAgICAgICAgdmFyIHQgPSBPYmplY3QodGhpcyk7XG4gICAgICAgIHZhciBsZW4gPSB0Lmxlbmd0aCA+Pj4gMDtcblxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoaSBpbiB0ICYmIGZ1bi5jYWxsKHRoaXMsIHRbaV0sIGksIHQpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbn1cblxuZXhwb3J0IHsgc29tZSBhcyBkZWZhdWx0IH07XG4iLCJpbXBvcnQgZXh0ZW5kIGZyb20gJy4uL3V0aWxzL2V4dGVuZCc7XG5pbXBvcnQgeyBjcmVhdGVVVEMgfSBmcm9tICcuL3V0Yyc7XG5pbXBvcnQgZ2V0UGFyc2luZ0ZsYWdzIGZyb20gJy4uL2NyZWF0ZS9wYXJzaW5nLWZsYWdzJztcbmltcG9ydCBzb21lIGZyb20gJy4uL3V0aWxzL3NvbWUnO1xuXG5leHBvcnQgZnVuY3Rpb24gaXNWYWxpZChtKSB7XG4gICAgaWYgKG0uX2lzVmFsaWQgPT0gbnVsbCkge1xuICAgICAgICB2YXIgZmxhZ3MgPSBnZXRQYXJzaW5nRmxhZ3MobSk7XG4gICAgICAgIHZhciBwYXJzZWRQYXJ0cyA9IHNvbWUuY2FsbChmbGFncy5wYXJzZWREYXRlUGFydHMsIGZ1bmN0aW9uIChpKSB7XG4gICAgICAgICAgICByZXR1cm4gaSAhPSBudWxsO1xuICAgICAgICB9KTtcbiAgICAgICAgdmFyIGlzTm93VmFsaWQgPSAhaXNOYU4obS5fZC5nZXRUaW1lKCkpICYmXG4gICAgICAgICAgICBmbGFncy5vdmVyZmxvdyA8IDAgJiZcbiAgICAgICAgICAgICFmbGFncy5lbXB0eSAmJlxuICAgICAgICAgICAgIWZsYWdzLmludmFsaWRNb250aCAmJlxuICAgICAgICAgICAgIWZsYWdzLmludmFsaWRXZWVrZGF5ICYmXG4gICAgICAgICAgICAhZmxhZ3MubnVsbElucHV0ICYmXG4gICAgICAgICAgICAhZmxhZ3MuaW52YWxpZEZvcm1hdCAmJlxuICAgICAgICAgICAgIWZsYWdzLnVzZXJJbnZhbGlkYXRlZCAmJlxuICAgICAgICAgICAgKCFmbGFncy5tZXJpZGllbSB8fCAoZmxhZ3MubWVyaWRpZW0gJiYgcGFyc2VkUGFydHMpKTtcblxuICAgICAgICBpZiAobS5fc3RyaWN0KSB7XG4gICAgICAgICAgICBpc05vd1ZhbGlkID0gaXNOb3dWYWxpZCAmJlxuICAgICAgICAgICAgICAgIGZsYWdzLmNoYXJzTGVmdE92ZXIgPT09IDAgJiZcbiAgICAgICAgICAgICAgICBmbGFncy51bnVzZWRUb2tlbnMubGVuZ3RoID09PSAwICYmXG4gICAgICAgICAgICAgICAgZmxhZ3MuYmlnSG91ciA9PT0gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKE9iamVjdC5pc0Zyb3plbiA9PSBudWxsIHx8ICFPYmplY3QuaXNGcm96ZW4obSkpIHtcbiAgICAgICAgICAgIG0uX2lzVmFsaWQgPSBpc05vd1ZhbGlkO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGlzTm93VmFsaWQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG0uX2lzVmFsaWQ7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVJbnZhbGlkIChmbGFncykge1xuICAgIHZhciBtID0gY3JlYXRlVVRDKE5hTik7XG4gICAgaWYgKGZsYWdzICE9IG51bGwpIHtcbiAgICAgICAgZXh0ZW5kKGdldFBhcnNpbmdGbGFncyhtKSwgZmxhZ3MpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgZ2V0UGFyc2luZ0ZsYWdzKG0pLnVzZXJJbnZhbGlkYXRlZCA9IHRydWU7XG4gICAgfVxuXG4gICAgcmV0dXJuIG07XG59XG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpc1VuZGVmaW5lZChpbnB1dCkge1xuICAgIHJldHVybiBpbnB1dCA9PT0gdm9pZCAwO1xufVxuIiwiaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5pbXBvcnQgaGFzT3duUHJvcCBmcm9tICcuLi91dGlscy9oYXMtb3duLXByb3AnO1xuaW1wb3J0IGlzVW5kZWZpbmVkIGZyb20gJy4uL3V0aWxzL2lzLXVuZGVmaW5lZCc7XG5pbXBvcnQgZ2V0UGFyc2luZ0ZsYWdzIGZyb20gJy4uL2NyZWF0ZS9wYXJzaW5nLWZsYWdzJztcblxuLy8gUGx1Z2lucyB0aGF0IGFkZCBwcm9wZXJ0aWVzIHNob3VsZCBhbHNvIGFkZCB0aGUga2V5IGhlcmUgKG51bGwgdmFsdWUpLFxuLy8gc28gd2UgY2FuIHByb3Blcmx5IGNsb25lIG91cnNlbHZlcy5cbnZhciBtb21lbnRQcm9wZXJ0aWVzID0gaG9va3MubW9tZW50UHJvcGVydGllcyA9IFtdO1xuXG5leHBvcnQgZnVuY3Rpb24gY29weUNvbmZpZyh0bywgZnJvbSkge1xuICAgIHZhciBpLCBwcm9wLCB2YWw7XG5cbiAgICBpZiAoIWlzVW5kZWZpbmVkKGZyb20uX2lzQU1vbWVudE9iamVjdCkpIHtcbiAgICAgICAgdG8uX2lzQU1vbWVudE9iamVjdCA9IGZyb20uX2lzQU1vbWVudE9iamVjdDtcbiAgICB9XG4gICAgaWYgKCFpc1VuZGVmaW5lZChmcm9tLl9pKSkge1xuICAgICAgICB0by5faSA9IGZyb20uX2k7XG4gICAgfVxuICAgIGlmICghaXNVbmRlZmluZWQoZnJvbS5fZikpIHtcbiAgICAgICAgdG8uX2YgPSBmcm9tLl9mO1xuICAgIH1cbiAgICBpZiAoIWlzVW5kZWZpbmVkKGZyb20uX2wpKSB7XG4gICAgICAgIHRvLl9sID0gZnJvbS5fbDtcbiAgICB9XG4gICAgaWYgKCFpc1VuZGVmaW5lZChmcm9tLl9zdHJpY3QpKSB7XG4gICAgICAgIHRvLl9zdHJpY3QgPSBmcm9tLl9zdHJpY3Q7XG4gICAgfVxuICAgIGlmICghaXNVbmRlZmluZWQoZnJvbS5fdHptKSkge1xuICAgICAgICB0by5fdHptID0gZnJvbS5fdHptO1xuICAgIH1cbiAgICBpZiAoIWlzVW5kZWZpbmVkKGZyb20uX2lzVVRDKSkge1xuICAgICAgICB0by5faXNVVEMgPSBmcm9tLl9pc1VUQztcbiAgICB9XG4gICAgaWYgKCFpc1VuZGVmaW5lZChmcm9tLl9vZmZzZXQpKSB7XG4gICAgICAgIHRvLl9vZmZzZXQgPSBmcm9tLl9vZmZzZXQ7XG4gICAgfVxuICAgIGlmICghaXNVbmRlZmluZWQoZnJvbS5fcGYpKSB7XG4gICAgICAgIHRvLl9wZiA9IGdldFBhcnNpbmdGbGFncyhmcm9tKTtcbiAgICB9XG4gICAgaWYgKCFpc1VuZGVmaW5lZChmcm9tLl9sb2NhbGUpKSB7XG4gICAgICAgIHRvLl9sb2NhbGUgPSBmcm9tLl9sb2NhbGU7XG4gICAgfVxuXG4gICAgaWYgKG1vbWVudFByb3BlcnRpZXMubGVuZ3RoID4gMCkge1xuICAgICAgICBmb3IgKGkgaW4gbW9tZW50UHJvcGVydGllcykge1xuICAgICAgICAgICAgcHJvcCA9IG1vbWVudFByb3BlcnRpZXNbaV07XG4gICAgICAgICAgICB2YWwgPSBmcm9tW3Byb3BdO1xuICAgICAgICAgICAgaWYgKCFpc1VuZGVmaW5lZCh2YWwpKSB7XG4gICAgICAgICAgICAgICAgdG9bcHJvcF0gPSB2YWw7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdG87XG59XG5cbnZhciB1cGRhdGVJblByb2dyZXNzID0gZmFsc2U7XG5cbi8vIE1vbWVudCBwcm90b3R5cGUgb2JqZWN0XG5leHBvcnQgZnVuY3Rpb24gTW9tZW50KGNvbmZpZykge1xuICAgIGNvcHlDb25maWcodGhpcywgY29uZmlnKTtcbiAgICB0aGlzLl9kID0gbmV3IERhdGUoY29uZmlnLl9kICE9IG51bGwgPyBjb25maWcuX2QuZ2V0VGltZSgpIDogTmFOKTtcbiAgICBpZiAoIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgIHRoaXMuX2QgPSBuZXcgRGF0ZShOYU4pO1xuICAgIH1cbiAgICAvLyBQcmV2ZW50IGluZmluaXRlIGxvb3AgaW4gY2FzZSB1cGRhdGVPZmZzZXQgY3JlYXRlcyBuZXcgbW9tZW50XG4gICAgLy8gb2JqZWN0cy5cbiAgICBpZiAodXBkYXRlSW5Qcm9ncmVzcyA9PT0gZmFsc2UpIHtcbiAgICAgICAgdXBkYXRlSW5Qcm9ncmVzcyA9IHRydWU7XG4gICAgICAgIGhvb2tzLnVwZGF0ZU9mZnNldCh0aGlzKTtcbiAgICAgICAgdXBkYXRlSW5Qcm9ncmVzcyA9IGZhbHNlO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzTW9tZW50IChvYmopIHtcbiAgICByZXR1cm4gb2JqIGluc3RhbmNlb2YgTW9tZW50IHx8IChvYmogIT0gbnVsbCAmJiBvYmouX2lzQU1vbWVudE9iamVjdCAhPSBudWxsKTtcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGFic0Zsb29yIChudW1iZXIpIHtcbiAgICBpZiAobnVtYmVyIDwgMCkge1xuICAgICAgICAvLyAtMCAtPiAwXG4gICAgICAgIHJldHVybiBNYXRoLmNlaWwobnVtYmVyKSB8fCAwO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKG51bWJlcik7XG4gICAgfVxufVxuIiwiaW1wb3J0IGFic0Zsb29yIGZyb20gJy4vYWJzLWZsb29yJztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gdG9JbnQoYXJndW1lbnRGb3JDb2VyY2lvbikge1xuICAgIHZhciBjb2VyY2VkTnVtYmVyID0gK2FyZ3VtZW50Rm9yQ29lcmNpb24sXG4gICAgICAgIHZhbHVlID0gMDtcblxuICAgIGlmIChjb2VyY2VkTnVtYmVyICE9PSAwICYmIGlzRmluaXRlKGNvZXJjZWROdW1iZXIpKSB7XG4gICAgICAgIHZhbHVlID0gYWJzRmxvb3IoY29lcmNlZE51bWJlcik7XG4gICAgfVxuXG4gICAgcmV0dXJuIHZhbHVlO1xufVxuIiwiaW1wb3J0IHRvSW50IGZyb20gJy4vdG8taW50JztcblxuLy8gY29tcGFyZSB0d28gYXJyYXlzLCByZXR1cm4gdGhlIG51bWJlciBvZiBkaWZmZXJlbmNlc1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY29tcGFyZUFycmF5cyhhcnJheTEsIGFycmF5MiwgZG9udENvbnZlcnQpIHtcbiAgICB2YXIgbGVuID0gTWF0aC5taW4oYXJyYXkxLmxlbmd0aCwgYXJyYXkyLmxlbmd0aCksXG4gICAgICAgIGxlbmd0aERpZmYgPSBNYXRoLmFicyhhcnJheTEubGVuZ3RoIC0gYXJyYXkyLmxlbmd0aCksXG4gICAgICAgIGRpZmZzID0gMCxcbiAgICAgICAgaTtcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgaWYgKChkb250Q29udmVydCAmJiBhcnJheTFbaV0gIT09IGFycmF5MltpXSkgfHxcbiAgICAgICAgICAgICghZG9udENvbnZlcnQgJiYgdG9JbnQoYXJyYXkxW2ldKSAhPT0gdG9JbnQoYXJyYXkyW2ldKSkpIHtcbiAgICAgICAgICAgIGRpZmZzKys7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGRpZmZzICsgbGVuZ3RoRGlmZjtcbn1cbiIsImltcG9ydCBleHRlbmQgZnJvbSAnLi9leHRlbmQnO1xuaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuL2hvb2tzJztcbmltcG9ydCBpc1VuZGVmaW5lZCBmcm9tICcuL2lzLXVuZGVmaW5lZCc7XG5cbmZ1bmN0aW9uIHdhcm4obXNnKSB7XG4gICAgaWYgKGhvb2tzLnN1cHByZXNzRGVwcmVjYXRpb25XYXJuaW5ncyA9PT0gZmFsc2UgJiZcbiAgICAgICAgICAgICh0eXBlb2YgY29uc29sZSAhPT0gICd1bmRlZmluZWQnKSAmJiBjb25zb2xlLndhcm4pIHtcbiAgICAgICAgY29uc29sZS53YXJuKCdEZXByZWNhdGlvbiB3YXJuaW5nOiAnICsgbXNnKTtcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBkZXByZWNhdGUobXNnLCBmbikge1xuICAgIHZhciBmaXJzdFRpbWUgPSB0cnVlO1xuXG4gICAgcmV0dXJuIGV4dGVuZChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChob29rcy5kZXByZWNhdGlvbkhhbmRsZXIgIT0gbnVsbCkge1xuICAgICAgICAgICAgaG9va3MuZGVwcmVjYXRpb25IYW5kbGVyKG51bGwsIG1zZyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGZpcnN0VGltZSkge1xuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgIHZhciBhcmc7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGFyZyA9ICcnO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgYXJndW1lbnRzW2ldID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgICAgICBhcmcgKz0gJ1xcblsnICsgaSArICddICc7XG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGtleSBpbiBhcmd1bWVudHNbMF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyZyArPSBrZXkgKyAnOiAnICsgYXJndW1lbnRzWzBdW2tleV0gKyAnLCAnO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGFyZyA9IGFyZy5zbGljZSgwLCAtMik7IC8vIFJlbW92ZSB0cmFpbGluZyBjb21tYSBhbmQgc3BhY2VcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBhcmcgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGFyZ3MucHVzaChhcmcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgd2Fybihtc2cgKyAnXFxuQXJndW1lbnRzOiAnICsgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJncykuam9pbignJykgKyAnXFxuJyArIChuZXcgRXJyb3IoKSkuc3RhY2spO1xuICAgICAgICAgICAgZmlyc3RUaW1lID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZuLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfSwgZm4pO1xufVxuXG52YXIgZGVwcmVjYXRpb25zID0ge307XG5cbmV4cG9ydCBmdW5jdGlvbiBkZXByZWNhdGVTaW1wbGUobmFtZSwgbXNnKSB7XG4gICAgaWYgKGhvb2tzLmRlcHJlY2F0aW9uSGFuZGxlciAhPSBudWxsKSB7XG4gICAgICAgIGhvb2tzLmRlcHJlY2F0aW9uSGFuZGxlcihuYW1lLCBtc2cpO1xuICAgIH1cbiAgICBpZiAoIWRlcHJlY2F0aW9uc1tuYW1lXSkge1xuICAgICAgICB3YXJuKG1zZyk7XG4gICAgICAgIGRlcHJlY2F0aW9uc1tuYW1lXSA9IHRydWU7XG4gICAgfVxufVxuXG5ob29rcy5zdXBwcmVzc0RlcHJlY2F0aW9uV2FybmluZ3MgPSBmYWxzZTtcbmhvb2tzLmRlcHJlY2F0aW9uSGFuZGxlciA9IG51bGw7XG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpc0Z1bmN0aW9uKGlucHV0KSB7XG4gICAgcmV0dXJuIGlucHV0IGluc3RhbmNlb2YgRnVuY3Rpb24gfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGlucHV0KSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcbn1cbiIsImltcG9ydCBpc0Z1bmN0aW9uIGZyb20gJy4uL3V0aWxzL2lzLWZ1bmN0aW9uJztcbmltcG9ydCBleHRlbmQgZnJvbSAnLi4vdXRpbHMvZXh0ZW5kJztcbmltcG9ydCBpc09iamVjdCBmcm9tICcuLi91dGlscy9pcy1vYmplY3QnO1xuaW1wb3J0IGhhc093blByb3AgZnJvbSAnLi4vdXRpbHMvaGFzLW93bi1wcm9wJztcblxuZXhwb3J0IGZ1bmN0aW9uIHNldCAoY29uZmlnKSB7XG4gICAgdmFyIHByb3AsIGk7XG4gICAgZm9yIChpIGluIGNvbmZpZykge1xuICAgICAgICBwcm9wID0gY29uZmlnW2ldO1xuICAgICAgICBpZiAoaXNGdW5jdGlvbihwcm9wKSkge1xuICAgICAgICAgICAgdGhpc1tpXSA9IHByb3A7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzWydfJyArIGldID0gcHJvcDtcbiAgICAgICAgfVxuICAgIH1cbiAgICB0aGlzLl9jb25maWcgPSBjb25maWc7XG4gICAgLy8gTGVuaWVudCBvcmRpbmFsIHBhcnNpbmcgYWNjZXB0cyBqdXN0IGEgbnVtYmVyIGluIGFkZGl0aW9uIHRvXG4gICAgLy8gbnVtYmVyICsgKHBvc3NpYmx5KSBzdHVmZiBjb21pbmcgZnJvbSBfb3JkaW5hbFBhcnNlTGVuaWVudC5cbiAgICB0aGlzLl9vcmRpbmFsUGFyc2VMZW5pZW50ID0gbmV3IFJlZ0V4cCh0aGlzLl9vcmRpbmFsUGFyc2Uuc291cmNlICsgJ3wnICsgKC9cXGR7MSwyfS8pLnNvdXJjZSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBtZXJnZUNvbmZpZ3MocGFyZW50Q29uZmlnLCBjaGlsZENvbmZpZykge1xuICAgIHZhciByZXMgPSBleHRlbmQoe30sIHBhcmVudENvbmZpZyksIHByb3A7XG4gICAgZm9yIChwcm9wIGluIGNoaWxkQ29uZmlnKSB7XG4gICAgICAgIGlmIChoYXNPd25Qcm9wKGNoaWxkQ29uZmlnLCBwcm9wKSkge1xuICAgICAgICAgICAgaWYgKGlzT2JqZWN0KHBhcmVudENvbmZpZ1twcm9wXSkgJiYgaXNPYmplY3QoY2hpbGRDb25maWdbcHJvcF0pKSB7XG4gICAgICAgICAgICAgICAgcmVzW3Byb3BdID0ge307XG4gICAgICAgICAgICAgICAgZXh0ZW5kKHJlc1twcm9wXSwgcGFyZW50Q29uZmlnW3Byb3BdKTtcbiAgICAgICAgICAgICAgICBleHRlbmQocmVzW3Byb3BdLCBjaGlsZENvbmZpZ1twcm9wXSk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNoaWxkQ29uZmlnW3Byb3BdICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZXNbcHJvcF0gPSBjaGlsZENvbmZpZ1twcm9wXTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZGVsZXRlIHJlc1twcm9wXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICBmb3IgKHByb3AgaW4gcGFyZW50Q29uZmlnKSB7XG4gICAgICAgIGlmIChoYXNPd25Qcm9wKHBhcmVudENvbmZpZywgcHJvcCkgJiZcbiAgICAgICAgICAgICAgICAhaGFzT3duUHJvcChjaGlsZENvbmZpZywgcHJvcCkgJiZcbiAgICAgICAgICAgICAgICBpc09iamVjdChwYXJlbnRDb25maWdbcHJvcF0pKSB7XG4gICAgICAgICAgICAvLyBtYWtlIHN1cmUgY2hhbmdlcyB0byBwcm9wZXJ0aWVzIGRvbid0IG1vZGlmeSBwYXJlbnQgY29uZmlnXG4gICAgICAgICAgICByZXNbcHJvcF0gPSBleHRlbmQoe30sIHJlc1twcm9wXSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlcztcbn1cbiIsImV4cG9ydCBmdW5jdGlvbiBMb2NhbGUoY29uZmlnKSB7XG4gICAgaWYgKGNvbmZpZyAhPSBudWxsKSB7XG4gICAgICAgIHRoaXMuc2V0KGNvbmZpZyk7XG4gICAgfVxufVxuIiwiaW1wb3J0IGhhc093blByb3AgZnJvbSAnLi9oYXMtb3duLXByb3AnO1xuXG52YXIga2V5cztcblxuaWYgKE9iamVjdC5rZXlzKSB7XG4gICAga2V5cyA9IE9iamVjdC5rZXlzO1xufSBlbHNlIHtcbiAgICBrZXlzID0gZnVuY3Rpb24gKG9iaikge1xuICAgICAgICB2YXIgaSwgcmVzID0gW107XG4gICAgICAgIGZvciAoaSBpbiBvYmopIHtcbiAgICAgICAgICAgIGlmIChoYXNPd25Qcm9wKG9iaiwgaSkpIHtcbiAgICAgICAgICAgICAgICByZXMucHVzaChpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzO1xuICAgIH07XG59XG5cbmV4cG9ydCB7IGtleXMgYXMgZGVmYXVsdCB9O1xuIiwiZXhwb3J0IHZhciBkZWZhdWx0Q2FsZW5kYXIgPSB7XG4gICAgc2FtZURheSA6ICdbVG9kYXkgYXRdIExUJyxcbiAgICBuZXh0RGF5IDogJ1tUb21vcnJvdyBhdF0gTFQnLFxuICAgIG5leHRXZWVrIDogJ2RkZGQgW2F0XSBMVCcsXG4gICAgbGFzdERheSA6ICdbWWVzdGVyZGF5IGF0XSBMVCcsXG4gICAgbGFzdFdlZWsgOiAnW0xhc3RdIGRkZGQgW2F0XSBMVCcsXG4gICAgc2FtZUVsc2UgOiAnTCdcbn07XG5cbmltcG9ydCBpc0Z1bmN0aW9uIGZyb20gJy4uL3V0aWxzL2lzLWZ1bmN0aW9uJztcblxuZXhwb3J0IGZ1bmN0aW9uIGNhbGVuZGFyIChrZXksIG1vbSwgbm93KSB7XG4gICAgdmFyIG91dHB1dCA9IHRoaXMuX2NhbGVuZGFyW2tleV0gfHwgdGhpcy5fY2FsZW5kYXJbJ3NhbWVFbHNlJ107XG4gICAgcmV0dXJuIGlzRnVuY3Rpb24ob3V0cHV0KSA/IG91dHB1dC5jYWxsKG1vbSwgbm93KSA6IG91dHB1dDtcbn1cbiIsImV4cG9ydCB2YXIgZGVmYXVsdExvbmdEYXRlRm9ybWF0ID0ge1xuICAgIExUUyAgOiAnaDptbTpzcyBBJyxcbiAgICBMVCAgIDogJ2g6bW0gQScsXG4gICAgTCAgICA6ICdNTS9ERC9ZWVlZJyxcbiAgICBMTCAgIDogJ01NTU0gRCwgWVlZWScsXG4gICAgTExMICA6ICdNTU1NIEQsIFlZWVkgaDptbSBBJyxcbiAgICBMTExMIDogJ2RkZGQsIE1NTU0gRCwgWVlZWSBoOm1tIEEnXG59O1xuXG5leHBvcnQgZnVuY3Rpb24gbG9uZ0RhdGVGb3JtYXQgKGtleSkge1xuICAgIHZhciBmb3JtYXQgPSB0aGlzLl9sb25nRGF0ZUZvcm1hdFtrZXldLFxuICAgICAgICBmb3JtYXRVcHBlciA9IHRoaXMuX2xvbmdEYXRlRm9ybWF0W2tleS50b1VwcGVyQ2FzZSgpXTtcblxuICAgIGlmIChmb3JtYXQgfHwgIWZvcm1hdFVwcGVyKSB7XG4gICAgICAgIHJldHVybiBmb3JtYXQ7XG4gICAgfVxuXG4gICAgdGhpcy5fbG9uZ0RhdGVGb3JtYXRba2V5XSA9IGZvcm1hdFVwcGVyLnJlcGxhY2UoL01NTU18TU18RER8ZGRkZC9nLCBmdW5jdGlvbiAodmFsKSB7XG4gICAgICAgIHJldHVybiB2YWwuc2xpY2UoMSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5fbG9uZ0RhdGVGb3JtYXRba2V5XTtcbn1cbiIsImV4cG9ydCB2YXIgZGVmYXVsdEludmFsaWREYXRlID0gJ0ludmFsaWQgZGF0ZSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBpbnZhbGlkRGF0ZSAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2ludmFsaWREYXRlO1xufVxuIiwiZXhwb3J0IHZhciBkZWZhdWx0T3JkaW5hbCA9ICclZCc7XG5leHBvcnQgdmFyIGRlZmF1bHRPcmRpbmFsUGFyc2UgPSAvXFxkezEsMn0vO1xuXG5leHBvcnQgZnVuY3Rpb24gb3JkaW5hbCAobnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMuX29yZGluYWwucmVwbGFjZSgnJWQnLCBudW1iZXIpO1xufVxuXG4iLCJleHBvcnQgdmFyIGRlZmF1bHRSZWxhdGl2ZVRpbWUgPSB7XG4gICAgZnV0dXJlIDogJ2luICVzJyxcbiAgICBwYXN0ICAgOiAnJXMgYWdvJyxcbiAgICBzICA6ICdhIGZldyBzZWNvbmRzJyxcbiAgICBtICA6ICdhIG1pbnV0ZScsXG4gICAgbW0gOiAnJWQgbWludXRlcycsXG4gICAgaCAgOiAnYW4gaG91cicsXG4gICAgaGggOiAnJWQgaG91cnMnLFxuICAgIGQgIDogJ2EgZGF5JyxcbiAgICBkZCA6ICclZCBkYXlzJyxcbiAgICBNICA6ICdhIG1vbnRoJyxcbiAgICBNTSA6ICclZCBtb250aHMnLFxuICAgIHkgIDogJ2EgeWVhcicsXG4gICAgeXkgOiAnJWQgeWVhcnMnXG59O1xuXG5pbXBvcnQgaXNGdW5jdGlvbiBmcm9tICcuLi91dGlscy9pcy1mdW5jdGlvbic7XG5cbmV4cG9ydCBmdW5jdGlvbiByZWxhdGl2ZVRpbWUgKG51bWJlciwgd2l0aG91dFN1ZmZpeCwgc3RyaW5nLCBpc0Z1dHVyZSkge1xuICAgIHZhciBvdXRwdXQgPSB0aGlzLl9yZWxhdGl2ZVRpbWVbc3RyaW5nXTtcbiAgICByZXR1cm4gKGlzRnVuY3Rpb24ob3V0cHV0KSkgP1xuICAgICAgICBvdXRwdXQobnVtYmVyLCB3aXRob3V0U3VmZml4LCBzdHJpbmcsIGlzRnV0dXJlKSA6XG4gICAgICAgIG91dHB1dC5yZXBsYWNlKC8lZC9pLCBudW1iZXIpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcGFzdEZ1dHVyZSAoZGlmZiwgb3V0cHV0KSB7XG4gICAgdmFyIGZvcm1hdCA9IHRoaXMuX3JlbGF0aXZlVGltZVtkaWZmID4gMCA/ICdmdXR1cmUnIDogJ3Bhc3QnXTtcbiAgICByZXR1cm4gaXNGdW5jdGlvbihmb3JtYXQpID8gZm9ybWF0KG91dHB1dCkgOiBmb3JtYXQucmVwbGFjZSgvJXMvaSwgb3V0cHV0KTtcbn1cbiIsImltcG9ydCBoYXNPd25Qcm9wIGZyb20gJy4uL3V0aWxzL2hhcy1vd24tcHJvcCc7XG5cbnZhciBhbGlhc2VzID0ge307XG5cbmV4cG9ydCBmdW5jdGlvbiBhZGRVbml0QWxpYXMgKHVuaXQsIHNob3J0aGFuZCkge1xuICAgIHZhciBsb3dlckNhc2UgPSB1bml0LnRvTG93ZXJDYXNlKCk7XG4gICAgYWxpYXNlc1tsb3dlckNhc2VdID0gYWxpYXNlc1tsb3dlckNhc2UgKyAncyddID0gYWxpYXNlc1tzaG9ydGhhbmRdID0gdW5pdDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG5vcm1hbGl6ZVVuaXRzKHVuaXRzKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB1bml0cyA9PT0gJ3N0cmluZycgPyBhbGlhc2VzW3VuaXRzXSB8fCBhbGlhc2VzW3VuaXRzLnRvTG93ZXJDYXNlKCldIDogdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbm9ybWFsaXplT2JqZWN0VW5pdHMoaW5wdXRPYmplY3QpIHtcbiAgICB2YXIgbm9ybWFsaXplZElucHV0ID0ge30sXG4gICAgICAgIG5vcm1hbGl6ZWRQcm9wLFxuICAgICAgICBwcm9wO1xuXG4gICAgZm9yIChwcm9wIGluIGlucHV0T2JqZWN0KSB7XG4gICAgICAgIGlmIChoYXNPd25Qcm9wKGlucHV0T2JqZWN0LCBwcm9wKSkge1xuICAgICAgICAgICAgbm9ybWFsaXplZFByb3AgPSBub3JtYWxpemVVbml0cyhwcm9wKTtcbiAgICAgICAgICAgIGlmIChub3JtYWxpemVkUHJvcCkge1xuICAgICAgICAgICAgICAgIG5vcm1hbGl6ZWRJbnB1dFtub3JtYWxpemVkUHJvcF0gPSBpbnB1dE9iamVjdFtwcm9wXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBub3JtYWxpemVkSW5wdXQ7XG59XG5cbiIsInZhciBwcmlvcml0aWVzID0ge307XG5cbmV4cG9ydCBmdW5jdGlvbiBhZGRVbml0UHJpb3JpdHkodW5pdCwgcHJpb3JpdHkpIHtcbiAgICBwcmlvcml0aWVzW3VuaXRdID0gcHJpb3JpdHk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRQcmlvcml0aXplZFVuaXRzKHVuaXRzT2JqKSB7XG4gICAgdmFyIHVuaXRzID0gW107XG4gICAgZm9yICh2YXIgdSBpbiB1bml0c09iaikge1xuICAgICAgICB1bml0cy5wdXNoKHt1bml0OiB1LCBwcmlvcml0eTogcHJpb3JpdGllc1t1XX0pO1xuICAgIH1cbiAgICB1bml0cy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgIHJldHVybiBhLnByaW9yaXR5IC0gYi5wcmlvcml0eTtcbiAgICB9KTtcbiAgICByZXR1cm4gdW5pdHM7XG59XG4iLCJpbXBvcnQgeyBub3JtYWxpemVVbml0cywgbm9ybWFsaXplT2JqZWN0VW5pdHMgfSBmcm9tICcuLi91bml0cy9hbGlhc2VzJztcbmltcG9ydCB7IGdldFByaW9yaXRpemVkVW5pdHMgfSBmcm9tICcuLi91bml0cy9wcmlvcml0aWVzJztcbmltcG9ydCB7IGhvb2tzIH0gZnJvbSAnLi4vdXRpbHMvaG9va3MnO1xuaW1wb3J0IGlzRnVuY3Rpb24gZnJvbSAnLi4vdXRpbHMvaXMtZnVuY3Rpb24nO1xuXG5cbmV4cG9ydCBmdW5jdGlvbiBtYWtlR2V0U2V0ICh1bml0LCBrZWVwVGltZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgaWYgKHZhbHVlICE9IG51bGwpIHtcbiAgICAgICAgICAgIHNldCh0aGlzLCB1bml0LCB2YWx1ZSk7XG4gICAgICAgICAgICBob29rcy51cGRhdGVPZmZzZXQodGhpcywga2VlcFRpbWUpO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gZ2V0KHRoaXMsIHVuaXQpO1xuICAgICAgICB9XG4gICAgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldCAobW9tLCB1bml0KSB7XG4gICAgcmV0dXJuIG1vbS5pc1ZhbGlkKCkgP1xuICAgICAgICBtb20uX2RbJ2dldCcgKyAobW9tLl9pc1VUQyA/ICdVVEMnIDogJycpICsgdW5pdF0oKSA6IE5hTjtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldCAobW9tLCB1bml0LCB2YWx1ZSkge1xuICAgIGlmIChtb20uaXNWYWxpZCgpKSB7XG4gICAgICAgIG1vbS5fZFsnc2V0JyArIChtb20uX2lzVVRDID8gJ1VUQycgOiAnJykgKyB1bml0XSh2YWx1ZSk7XG4gICAgfVxufVxuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCBmdW5jdGlvbiBzdHJpbmdHZXQgKHVuaXRzKSB7XG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG4gICAgaWYgKGlzRnVuY3Rpb24odGhpc1t1bml0c10pKSB7XG4gICAgICAgIHJldHVybiB0aGlzW3VuaXRzXSgpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gc3RyaW5nU2V0ICh1bml0cywgdmFsdWUpIHtcbiAgICBpZiAodHlwZW9mIHVuaXRzID09PSAnb2JqZWN0Jykge1xuICAgICAgICB1bml0cyA9IG5vcm1hbGl6ZU9iamVjdFVuaXRzKHVuaXRzKTtcbiAgICAgICAgdmFyIHByaW9yaXRpemVkID0gZ2V0UHJpb3JpdGl6ZWRVbml0cyh1bml0cyk7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJpb3JpdGl6ZWQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXNbcHJpb3JpdGl6ZWRbaV0udW5pdF0odW5pdHNbcHJpb3JpdGl6ZWRbaV0udW5pdF0pO1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG4gICAgICAgIGlmIChpc0Z1bmN0aW9uKHRoaXNbdW5pdHNdKSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXNbdW5pdHNdKHZhbHVlKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHplcm9GaWxsKG51bWJlciwgdGFyZ2V0TGVuZ3RoLCBmb3JjZVNpZ24pIHtcbiAgICB2YXIgYWJzTnVtYmVyID0gJycgKyBNYXRoLmFicyhudW1iZXIpLFxuICAgICAgICB6ZXJvc1RvRmlsbCA9IHRhcmdldExlbmd0aCAtIGFic051bWJlci5sZW5ndGgsXG4gICAgICAgIHNpZ24gPSBudW1iZXIgPj0gMDtcbiAgICByZXR1cm4gKHNpZ24gPyAoZm9yY2VTaWduID8gJysnIDogJycpIDogJy0nKSArXG4gICAgICAgIE1hdGgucG93KDEwLCBNYXRoLm1heCgwLCB6ZXJvc1RvRmlsbCkpLnRvU3RyaW5nKCkuc3Vic3RyKDEpICsgYWJzTnVtYmVyO1xufVxuIiwiaW1wb3J0IHplcm9GaWxsIGZyb20gJy4uL3V0aWxzL3plcm8tZmlsbCc7XG5cbmV4cG9ydCB2YXIgZm9ybWF0dGluZ1Rva2VucyA9IC8oXFxbW15cXFtdKlxcXSl8KFxcXFwpPyhbSGhdbW0oc3MpP3xNb3xNTT9NP00/fERvfERERG98REQ/RD9EP3xkZGQ/ZD98ZG8/fHdbb3x3XT98V1tvfFddP3xRbz98WVlZWVlZfFlZWVlZfFlZWVl8WVl8Z2coZ2dnPyk/fEdHKEdHRz8pP3xlfEV8YXxBfGhoP3xISD98a2s/fG1tP3xzcz98U3sxLDl9fHh8WHx6ej98Wlo/fC4pL2c7XG5cbnZhciBsb2NhbEZvcm1hdHRpbmdUb2tlbnMgPSAvKFxcW1teXFxbXSpcXF0pfChcXFxcKT8oTFRTfExUfExMP0w/TD98bHsxLDR9KS9nO1xuXG52YXIgZm9ybWF0RnVuY3Rpb25zID0ge307XG5cbmV4cG9ydCB2YXIgZm9ybWF0VG9rZW5GdW5jdGlvbnMgPSB7fTtcblxuLy8gdG9rZW46ICAgICdNJ1xuLy8gcGFkZGVkOiAgIFsnTU0nLCAyXVxuLy8gb3JkaW5hbDogICdNbydcbi8vIGNhbGxiYWNrOiBmdW5jdGlvbiAoKSB7IHRoaXMubW9udGgoKSArIDEgfVxuZXhwb3J0IGZ1bmN0aW9uIGFkZEZvcm1hdFRva2VuICh0b2tlbiwgcGFkZGVkLCBvcmRpbmFsLCBjYWxsYmFjaykge1xuICAgIHZhciBmdW5jID0gY2FsbGJhY2s7XG4gICAgaWYgKHR5cGVvZiBjYWxsYmFjayA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgZnVuYyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzW2NhbGxiYWNrXSgpO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBpZiAodG9rZW4pIHtcbiAgICAgICAgZm9ybWF0VG9rZW5GdW5jdGlvbnNbdG9rZW5dID0gZnVuYztcbiAgICB9XG4gICAgaWYgKHBhZGRlZCkge1xuICAgICAgICBmb3JtYXRUb2tlbkZ1bmN0aW9uc1twYWRkZWRbMF1dID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIHplcm9GaWxsKGZ1bmMuYXBwbHkodGhpcywgYXJndW1lbnRzKSwgcGFkZGVkWzFdLCBwYWRkZWRbMl0pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBpZiAob3JkaW5hbCkge1xuICAgICAgICBmb3JtYXRUb2tlbkZ1bmN0aW9uc1tvcmRpbmFsXSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEoKS5vcmRpbmFsKGZ1bmMuYXBwbHkodGhpcywgYXJndW1lbnRzKSwgdG9rZW4pO1xuICAgICAgICB9O1xuICAgIH1cbn1cblxuZnVuY3Rpb24gcmVtb3ZlRm9ybWF0dGluZ1Rva2VucyhpbnB1dCkge1xuICAgIGlmIChpbnB1dC5tYXRjaCgvXFxbW1xcc1xcU10vKSkge1xuICAgICAgICByZXR1cm4gaW5wdXQucmVwbGFjZSgvXlxcW3xcXF0kL2csICcnKTtcbiAgICB9XG4gICAgcmV0dXJuIGlucHV0LnJlcGxhY2UoL1xcXFwvZywgJycpO1xufVxuXG5mdW5jdGlvbiBtYWtlRm9ybWF0RnVuY3Rpb24oZm9ybWF0KSB7XG4gICAgdmFyIGFycmF5ID0gZm9ybWF0Lm1hdGNoKGZvcm1hdHRpbmdUb2tlbnMpLCBpLCBsZW5ndGg7XG5cbiAgICBmb3IgKGkgPSAwLCBsZW5ndGggPSBhcnJheS5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoZm9ybWF0VG9rZW5GdW5jdGlvbnNbYXJyYXlbaV1dKSB7XG4gICAgICAgICAgICBhcnJheVtpXSA9IGZvcm1hdFRva2VuRnVuY3Rpb25zW2FycmF5W2ldXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFycmF5W2ldID0gcmVtb3ZlRm9ybWF0dGluZ1Rva2VucyhhcnJheVtpXSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKG1vbSkge1xuICAgICAgICB2YXIgb3V0cHV0ID0gJycsIGk7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgb3V0cHV0ICs9IGFycmF5W2ldIGluc3RhbmNlb2YgRnVuY3Rpb24gPyBhcnJheVtpXS5jYWxsKG1vbSwgZm9ybWF0KSA6IGFycmF5W2ldO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvdXRwdXQ7XG4gICAgfTtcbn1cblxuLy8gZm9ybWF0IGRhdGUgdXNpbmcgbmF0aXZlIGRhdGUgb2JqZWN0XG5leHBvcnQgZnVuY3Rpb24gZm9ybWF0TW9tZW50KG0sIGZvcm1hdCkge1xuICAgIGlmICghbS5pc1ZhbGlkKCkpIHtcbiAgICAgICAgcmV0dXJuIG0ubG9jYWxlRGF0YSgpLmludmFsaWREYXRlKCk7XG4gICAgfVxuXG4gICAgZm9ybWF0ID0gZXhwYW5kRm9ybWF0KGZvcm1hdCwgbS5sb2NhbGVEYXRhKCkpO1xuICAgIGZvcm1hdEZ1bmN0aW9uc1tmb3JtYXRdID0gZm9ybWF0RnVuY3Rpb25zW2Zvcm1hdF0gfHwgbWFrZUZvcm1hdEZ1bmN0aW9uKGZvcm1hdCk7XG5cbiAgICByZXR1cm4gZm9ybWF0RnVuY3Rpb25zW2Zvcm1hdF0obSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBleHBhbmRGb3JtYXQoZm9ybWF0LCBsb2NhbGUpIHtcbiAgICB2YXIgaSA9IDU7XG5cbiAgICBmdW5jdGlvbiByZXBsYWNlTG9uZ0RhdGVGb3JtYXRUb2tlbnMoaW5wdXQpIHtcbiAgICAgICAgcmV0dXJuIGxvY2FsZS5sb25nRGF0ZUZvcm1hdChpbnB1dCkgfHwgaW5wdXQ7XG4gICAgfVxuXG4gICAgbG9jYWxGb3JtYXR0aW5nVG9rZW5zLmxhc3RJbmRleCA9IDA7XG4gICAgd2hpbGUgKGkgPj0gMCAmJiBsb2NhbEZvcm1hdHRpbmdUb2tlbnMudGVzdChmb3JtYXQpKSB7XG4gICAgICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKGxvY2FsRm9ybWF0dGluZ1Rva2VucywgcmVwbGFjZUxvbmdEYXRlRm9ybWF0VG9rZW5zKTtcbiAgICAgICAgbG9jYWxGb3JtYXR0aW5nVG9rZW5zLmxhc3RJbmRleCA9IDA7XG4gICAgICAgIGkgLT0gMTtcbiAgICB9XG5cbiAgICByZXR1cm4gZm9ybWF0O1xufVxuIiwiZXhwb3J0IHZhciBtYXRjaDEgICAgICAgICA9IC9cXGQvOyAgICAgICAgICAgIC8vICAgICAgIDAgLSA5XG5leHBvcnQgdmFyIG1hdGNoMiAgICAgICAgID0gL1xcZFxcZC87ICAgICAgICAgIC8vICAgICAgMDAgLSA5OVxuZXhwb3J0IHZhciBtYXRjaDMgICAgICAgICA9IC9cXGR7M30vOyAgICAgICAgIC8vICAgICAwMDAgLSA5OTlcbmV4cG9ydCB2YXIgbWF0Y2g0ICAgICAgICAgPSAvXFxkezR9LzsgICAgICAgICAvLyAgICAwMDAwIC0gOTk5OVxuZXhwb3J0IHZhciBtYXRjaDYgICAgICAgICA9IC9bKy1dP1xcZHs2fS87ICAgIC8vIC05OTk5OTkgLSA5OTk5OTlcbmV4cG9ydCB2YXIgbWF0Y2gxdG8yICAgICAgPSAvXFxkXFxkPy87ICAgICAgICAgLy8gICAgICAgMCAtIDk5XG5leHBvcnQgdmFyIG1hdGNoM3RvNCAgICAgID0gL1xcZFxcZFxcZFxcZD8vOyAgICAgLy8gICAgIDk5OSAtIDk5OTlcbmV4cG9ydCB2YXIgbWF0Y2g1dG82ICAgICAgPSAvXFxkXFxkXFxkXFxkXFxkXFxkPy87IC8vICAgOTk5OTkgLSA5OTk5OTlcbmV4cG9ydCB2YXIgbWF0Y2gxdG8zICAgICAgPSAvXFxkezEsM30vOyAgICAgICAvLyAgICAgICAwIC0gOTk5XG5leHBvcnQgdmFyIG1hdGNoMXRvNCAgICAgID0gL1xcZHsxLDR9LzsgICAgICAgLy8gICAgICAgMCAtIDk5OTlcbmV4cG9ydCB2YXIgbWF0Y2gxdG82ICAgICAgPSAvWystXT9cXGR7MSw2fS87ICAvLyAtOTk5OTk5IC0gOTk5OTk5XG5cbmV4cG9ydCB2YXIgbWF0Y2hVbnNpZ25lZCAgPSAvXFxkKy87ICAgICAgICAgICAvLyAgICAgICAwIC0gaW5mXG5leHBvcnQgdmFyIG1hdGNoU2lnbmVkICAgID0gL1srLV0/XFxkKy87ICAgICAgLy8gICAgLWluZiAtIGluZlxuXG5leHBvcnQgdmFyIG1hdGNoT2Zmc2V0ICAgID0gL1p8WystXVxcZFxcZDo/XFxkXFxkL2dpOyAvLyArMDA6MDAgLTAwOjAwICswMDAwIC0wMDAwIG9yIFpcbmV4cG9ydCB2YXIgbWF0Y2hTaG9ydE9mZnNldCA9IC9afFsrLV1cXGRcXGQoPzo6P1xcZFxcZCk/L2dpOyAvLyArMDAgLTAwICswMDowMCAtMDA6MDAgKzAwMDAgLTAwMDAgb3IgWlxuXG5leHBvcnQgdmFyIG1hdGNoVGltZXN0YW1wID0gL1srLV0/XFxkKyhcXC5cXGR7MSwzfSk/LzsgLy8gMTIzNDU2Nzg5IDEyMzQ1Njc4OS4xMjNcblxuLy8gYW55IHdvcmQgKG9yIHR3bykgY2hhcmFjdGVycyBvciBudW1iZXJzIGluY2x1ZGluZyB0d28vdGhyZWUgd29yZCBtb250aCBpbiBhcmFiaWMuXG4vLyBpbmNsdWRlcyBzY290dGlzaCBnYWVsaWMgdHdvIHdvcmQgYW5kIGh5cGhlbmF0ZWQgbW9udGhzXG5leHBvcnQgdmFyIG1hdGNoV29yZCA9IC9bMC05XSpbJ2EtelxcdTAwQTAtXFx1MDVGRlxcdTA3MDAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0rfFtcXHUwNjAwLVxcdTA2RkZcXC9dKyhcXHMqP1tcXHUwNjAwLVxcdTA2RkZdKyl7MSwyfS9pO1xuXG5cbmltcG9ydCBoYXNPd25Qcm9wIGZyb20gJy4uL3V0aWxzL2hhcy1vd24tcHJvcCc7XG5pbXBvcnQgaXNGdW5jdGlvbiBmcm9tICcuLi91dGlscy9pcy1mdW5jdGlvbic7XG5cbnZhciByZWdleGVzID0ge307XG5cbmV4cG9ydCBmdW5jdGlvbiBhZGRSZWdleFRva2VuICh0b2tlbiwgcmVnZXgsIHN0cmljdFJlZ2V4KSB7XG4gICAgcmVnZXhlc1t0b2tlbl0gPSBpc0Z1bmN0aW9uKHJlZ2V4KSA/IHJlZ2V4IDogZnVuY3Rpb24gKGlzU3RyaWN0LCBsb2NhbGVEYXRhKSB7XG4gICAgICAgIHJldHVybiAoaXNTdHJpY3QgJiYgc3RyaWN0UmVnZXgpID8gc3RyaWN0UmVnZXggOiByZWdleDtcbiAgICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGFyc2VSZWdleEZvclRva2VuICh0b2tlbiwgY29uZmlnKSB7XG4gICAgaWYgKCFoYXNPd25Qcm9wKHJlZ2V4ZXMsIHRva2VuKSkge1xuICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCh1bmVzY2FwZUZvcm1hdCh0b2tlbikpO1xuICAgIH1cblxuICAgIHJldHVybiByZWdleGVzW3Rva2VuXShjb25maWcuX3N0cmljdCwgY29uZmlnLl9sb2NhbGUpO1xufVxuXG4vLyBDb2RlIGZyb20gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8zNTYxNDkzL2lzLXRoZXJlLWEtcmVnZXhwLWVzY2FwZS1mdW5jdGlvbi1pbi1qYXZhc2NyaXB0XG5mdW5jdGlvbiB1bmVzY2FwZUZvcm1hdChzKSB7XG4gICAgcmV0dXJuIHJlZ2V4RXNjYXBlKHMucmVwbGFjZSgnXFxcXCcsICcnKS5yZXBsYWNlKC9cXFxcKFxcWyl8XFxcXChcXF0pfFxcWyhbXlxcXVxcW10qKVxcXXxcXFxcKC4pL2csIGZ1bmN0aW9uIChtYXRjaGVkLCBwMSwgcDIsIHAzLCBwNCkge1xuICAgICAgICByZXR1cm4gcDEgfHwgcDIgfHwgcDMgfHwgcDQ7XG4gICAgfSkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVnZXhFc2NhcGUocykge1xuICAgIHJldHVybiBzLnJlcGxhY2UoL1stXFwvXFxcXF4kKis/LigpfFtcXF17fV0vZywgJ1xcXFwkJicpO1xufVxuIiwiaW1wb3J0IGhhc093blByb3AgZnJvbSAnLi4vdXRpbHMvaGFzLW93bi1wcm9wJztcbmltcG9ydCBpc051bWJlciBmcm9tICcuLi91dGlscy9pcy1udW1iZXInO1xuaW1wb3J0IHRvSW50IGZyb20gJy4uL3V0aWxzL3RvLWludCc7XG5cbnZhciB0b2tlbnMgPSB7fTtcblxuZXhwb3J0IGZ1bmN0aW9uIGFkZFBhcnNlVG9rZW4gKHRva2VuLCBjYWxsYmFjaykge1xuICAgIHZhciBpLCBmdW5jID0gY2FsbGJhY2s7XG4gICAgaWYgKHR5cGVvZiB0b2tlbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgdG9rZW4gPSBbdG9rZW5dO1xuICAgIH1cbiAgICBpZiAoaXNOdW1iZXIoY2FsbGJhY2spKSB7XG4gICAgICAgIGZ1bmMgPSBmdW5jdGlvbiAoaW5wdXQsIGFycmF5KSB7XG4gICAgICAgICAgICBhcnJheVtjYWxsYmFja10gPSB0b0ludChpbnB1dCk7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGZvciAoaSA9IDA7IGkgPCB0b2tlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICB0b2tlbnNbdG9rZW5baV1dID0gZnVuYztcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBhZGRXZWVrUGFyc2VUb2tlbiAodG9rZW4sIGNhbGxiYWNrKSB7XG4gICAgYWRkUGFyc2VUb2tlbih0b2tlbiwgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnLCB0b2tlbikge1xuICAgICAgICBjb25maWcuX3cgPSBjb25maWcuX3cgfHwge307XG4gICAgICAgIGNhbGxiYWNrKGlucHV0LCBjb25maWcuX3csIGNvbmZpZywgdG9rZW4pO1xuICAgIH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gYWRkVGltZVRvQXJyYXlGcm9tVG9rZW4odG9rZW4sIGlucHV0LCBjb25maWcpIHtcbiAgICBpZiAoaW5wdXQgIT0gbnVsbCAmJiBoYXNPd25Qcm9wKHRva2VucywgdG9rZW4pKSB7XG4gICAgICAgIHRva2Vuc1t0b2tlbl0oaW5wdXQsIGNvbmZpZy5fYSwgY29uZmlnLCB0b2tlbik7XG4gICAgfVxufVxuIiwiZXhwb3J0IHZhciBZRUFSID0gMDtcbmV4cG9ydCB2YXIgTU9OVEggPSAxO1xuZXhwb3J0IHZhciBEQVRFID0gMjtcbmV4cG9ydCB2YXIgSE9VUiA9IDM7XG5leHBvcnQgdmFyIE1JTlVURSA9IDQ7XG5leHBvcnQgdmFyIFNFQ09ORCA9IDU7XG5leHBvcnQgdmFyIE1JTExJU0VDT05EID0gNjtcbmV4cG9ydCB2YXIgV0VFSyA9IDc7XG5leHBvcnQgdmFyIFdFRUtEQVkgPSA4O1xuIiwidmFyIGluZGV4T2Y7XG5cbmlmIChBcnJheS5wcm90b3R5cGUuaW5kZXhPZikge1xuICAgIGluZGV4T2YgPSBBcnJheS5wcm90b3R5cGUuaW5kZXhPZjtcbn0gZWxzZSB7XG4gICAgaW5kZXhPZiA9IGZ1bmN0aW9uIChvKSB7XG4gICAgICAgIC8vIEkga25vd1xuICAgICAgICB2YXIgaTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgIGlmICh0aGlzW2ldID09PSBvKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH07XG59XG5cbmV4cG9ydCB7IGluZGV4T2YgYXMgZGVmYXVsdCB9O1xuIiwiaW1wb3J0IHsgZ2V0IH0gZnJvbSAnLi4vbW9tZW50L2dldC1zZXQnO1xuaW1wb3J0IGhhc093blByb3AgZnJvbSAnLi4vdXRpbHMvaGFzLW93bi1wcm9wJztcbmltcG9ydCB7IGFkZEZvcm1hdFRva2VuIH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBhZGRVbml0QWxpYXMgfSBmcm9tICcuL2FsaWFzZXMnO1xuaW1wb3J0IHsgYWRkVW5pdFByaW9yaXR5IH0gZnJvbSAnLi9wcmlvcml0aWVzJztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoMXRvMiwgbWF0Y2gyLCBtYXRjaFdvcmQsIHJlZ2V4RXNjYXBlIH0gZnJvbSAnLi4vcGFyc2UvcmVnZXgnO1xuaW1wb3J0IHsgYWRkUGFyc2VUb2tlbiB9IGZyb20gJy4uL3BhcnNlL3Rva2VuJztcbmltcG9ydCB7IGhvb2tzIH0gZnJvbSAnLi4vdXRpbHMvaG9va3MnO1xuaW1wb3J0IHsgTU9OVEggfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcbmltcG9ydCBpc0FycmF5IGZyb20gJy4uL3V0aWxzL2lzLWFycmF5JztcbmltcG9ydCBpc051bWJlciBmcm9tICcuLi91dGlscy9pcy1udW1iZXInO1xuaW1wb3J0IGluZGV4T2YgZnJvbSAnLi4vdXRpbHMvaW5kZXgtb2YnO1xuaW1wb3J0IHsgY3JlYXRlVVRDIH0gZnJvbSAnLi4vY3JlYXRlL3V0Yyc7XG5pbXBvcnQgZ2V0UGFyc2luZ0ZsYWdzIGZyb20gJy4uL2NyZWF0ZS9wYXJzaW5nLWZsYWdzJztcblxuZXhwb3J0IGZ1bmN0aW9uIGRheXNJbk1vbnRoKHllYXIsIG1vbnRoKSB7XG4gICAgcmV0dXJuIG5ldyBEYXRlKERhdGUuVVRDKHllYXIsIG1vbnRoICsgMSwgMCkpLmdldFVUQ0RhdGUoKTtcbn1cblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbignTScsIFsnTU0nLCAyXSwgJ01vJywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1vbnRoKCkgKyAxO1xufSk7XG5cbmFkZEZvcm1hdFRva2VuKCdNTU0nLCAwLCAwLCBmdW5jdGlvbiAoZm9ybWF0KSB7XG4gICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YSgpLm1vbnRoc1Nob3J0KHRoaXMsIGZvcm1hdCk7XG59KTtcblxuYWRkRm9ybWF0VG9rZW4oJ01NTU0nLCAwLCAwLCBmdW5jdGlvbiAoZm9ybWF0KSB7XG4gICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YSgpLm1vbnRocyh0aGlzLCBmb3JtYXQpO1xufSk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCdtb250aCcsICdNJyk7XG5cbi8vIFBSSU9SSVRZXG5cbmFkZFVuaXRQcmlvcml0eSgnbW9udGgnLCA4KTtcblxuLy8gUEFSU0lOR1xuXG5hZGRSZWdleFRva2VuKCdNJywgICAgbWF0Y2gxdG8yKTtcbmFkZFJlZ2V4VG9rZW4oJ01NJywgICBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdNTU0nLCAgZnVuY3Rpb24gKGlzU3RyaWN0LCBsb2NhbGUpIHtcbiAgICByZXR1cm4gbG9jYWxlLm1vbnRoc1Nob3J0UmVnZXgoaXNTdHJpY3QpO1xufSk7XG5hZGRSZWdleFRva2VuKCdNTU1NJywgZnVuY3Rpb24gKGlzU3RyaWN0LCBsb2NhbGUpIHtcbiAgICByZXR1cm4gbG9jYWxlLm1vbnRoc1JlZ2V4KGlzU3RyaWN0KTtcbn0pO1xuXG5hZGRQYXJzZVRva2VuKFsnTScsICdNTSddLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5KSB7XG4gICAgYXJyYXlbTU9OVEhdID0gdG9JbnQoaW5wdXQpIC0gMTtcbn0pO1xuXG5hZGRQYXJzZVRva2VuKFsnTU1NJywgJ01NTU0nXSwgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnLCB0b2tlbikge1xuICAgIHZhciBtb250aCA9IGNvbmZpZy5fbG9jYWxlLm1vbnRoc1BhcnNlKGlucHV0LCB0b2tlbiwgY29uZmlnLl9zdHJpY3QpO1xuICAgIC8vIGlmIHdlIGRpZG4ndCBmaW5kIGEgbW9udGggbmFtZSwgbWFyayB0aGUgZGF0ZSBhcyBpbnZhbGlkLlxuICAgIGlmIChtb250aCAhPSBudWxsKSB7XG4gICAgICAgIGFycmF5W01PTlRIXSA9IG1vbnRoO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLmludmFsaWRNb250aCA9IGlucHV0O1xuICAgIH1cbn0pO1xuXG4vLyBMT0NBTEVTXG5cbnZhciBNT05USFNfSU5fRk9STUFUID0gL0Rbb0RdPyhcXFtbXlxcW1xcXV0qXFxdfFxccykrTU1NTT8vO1xuZXhwb3J0IHZhciBkZWZhdWx0TG9jYWxlTW9udGhzID0gJ0phbnVhcnlfRmVicnVhcnlfTWFyY2hfQXByaWxfTWF5X0p1bmVfSnVseV9BdWd1c3RfU2VwdGVtYmVyX09jdG9iZXJfTm92ZW1iZXJfRGVjZW1iZXInLnNwbGl0KCdfJyk7XG5leHBvcnQgZnVuY3Rpb24gbG9jYWxlTW9udGhzIChtLCBmb3JtYXQpIHtcbiAgICBpZiAoIW0pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRocztcbiAgICB9XG4gICAgcmV0dXJuIGlzQXJyYXkodGhpcy5fbW9udGhzKSA/IHRoaXMuX21vbnRoc1ttLm1vbnRoKCldIDpcbiAgICAgICAgdGhpcy5fbW9udGhzWyh0aGlzLl9tb250aHMuaXNGb3JtYXQgfHwgTU9OVEhTX0lOX0ZPUk1BVCkudGVzdChmb3JtYXQpID8gJ2Zvcm1hdCcgOiAnc3RhbmRhbG9uZSddW20ubW9udGgoKV07XG59XG5cbmV4cG9ydCB2YXIgZGVmYXVsdExvY2FsZU1vbnRoc1Nob3J0ID0gJ0phbl9GZWJfTWFyX0Fwcl9NYXlfSnVuX0p1bF9BdWdfU2VwX09jdF9Ob3ZfRGVjJy5zcGxpdCgnXycpO1xuZXhwb3J0IGZ1bmN0aW9uIGxvY2FsZU1vbnRoc1Nob3J0IChtLCBmb3JtYXQpIHtcbiAgICBpZiAoIW0pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRoc1Nob3J0O1xuICAgIH1cbiAgICByZXR1cm4gaXNBcnJheSh0aGlzLl9tb250aHNTaG9ydCkgPyB0aGlzLl9tb250aHNTaG9ydFttLm1vbnRoKCldIDpcbiAgICAgICAgdGhpcy5fbW9udGhzU2hvcnRbTU9OVEhTX0lOX0ZPUk1BVC50ZXN0KGZvcm1hdCkgPyAnZm9ybWF0JyA6ICdzdGFuZGFsb25lJ11bbS5tb250aCgpXTtcbn1cblxuZnVuY3Rpb24gaGFuZGxlU3RyaWN0UGFyc2UobW9udGhOYW1lLCBmb3JtYXQsIHN0cmljdCkge1xuICAgIHZhciBpLCBpaSwgbW9tLCBsbGMgPSBtb250aE5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKTtcbiAgICBpZiAoIXRoaXMuX21vbnRoc1BhcnNlKSB7XG4gICAgICAgIC8vIHRoaXMgaXMgbm90IHVzZWRcbiAgICAgICAgdGhpcy5fbW9udGhzUGFyc2UgPSBbXTtcbiAgICAgICAgdGhpcy5fbG9uZ01vbnRoc1BhcnNlID0gW107XG4gICAgICAgIHRoaXMuX3Nob3J0TW9udGhzUGFyc2UgPSBbXTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IDEyOyArK2kpIHtcbiAgICAgICAgICAgIG1vbSA9IGNyZWF0ZVVUQyhbMjAwMCwgaV0pO1xuICAgICAgICAgICAgdGhpcy5fc2hvcnRNb250aHNQYXJzZVtpXSA9IHRoaXMubW9udGhzU2hvcnQobW9tLCAnJykudG9Mb2NhbGVMb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHRoaXMuX2xvbmdNb250aHNQYXJzZVtpXSA9IHRoaXMubW9udGhzKG1vbSwgJycpLnRvTG9jYWxlTG93ZXJDYXNlKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoc3RyaWN0KSB7XG4gICAgICAgIGlmIChmb3JtYXQgPT09ICdNTU0nKSB7XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl9zaG9ydE1vbnRoc1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgcmV0dXJuIGlpICE9PSAtMSA/IGlpIDogbnVsbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX2xvbmdNb250aHNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIHJldHVybiBpaSAhPT0gLTEgPyBpaSA6IG51bGw7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoZm9ybWF0ID09PSAnTU1NJykge1xuICAgICAgICAgICAgaWkgPSBpbmRleE9mLmNhbGwodGhpcy5fc2hvcnRNb250aHNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIGlmIChpaSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaWk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl9sb25nTW9udGhzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICByZXR1cm4gaWkgIT09IC0xID8gaWkgOiBudWxsO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWkgPSBpbmRleE9mLmNhbGwodGhpcy5fbG9uZ01vbnRoc1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgaWYgKGlpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX3Nob3J0TW9udGhzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICByZXR1cm4gaWkgIT09IC0xID8gaWkgOiBudWxsO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9jYWxlTW9udGhzUGFyc2UgKG1vbnRoTmFtZSwgZm9ybWF0LCBzdHJpY3QpIHtcbiAgICB2YXIgaSwgbW9tLCByZWdleDtcblxuICAgIGlmICh0aGlzLl9tb250aHNQYXJzZUV4YWN0KSB7XG4gICAgICAgIHJldHVybiBoYW5kbGVTdHJpY3RQYXJzZS5jYWxsKHRoaXMsIG1vbnRoTmFtZSwgZm9ybWF0LCBzdHJpY3QpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5fbW9udGhzUGFyc2UpIHtcbiAgICAgICAgdGhpcy5fbW9udGhzUGFyc2UgPSBbXTtcbiAgICAgICAgdGhpcy5fbG9uZ01vbnRoc1BhcnNlID0gW107XG4gICAgICAgIHRoaXMuX3Nob3J0TW9udGhzUGFyc2UgPSBbXTtcbiAgICB9XG5cbiAgICAvLyBUT0RPOiBhZGQgc29ydGluZ1xuICAgIC8vIFNvcnRpbmcgbWFrZXMgc3VyZSBpZiBvbmUgbW9udGggKG9yIGFiYnIpIGlzIGEgcHJlZml4IG9mIGFub3RoZXJcbiAgICAvLyBzZWUgc29ydGluZyBpbiBjb21wdXRlTW9udGhzUGFyc2VcbiAgICBmb3IgKGkgPSAwOyBpIDwgMTI7IGkrKykge1xuICAgICAgICAvLyBtYWtlIHRoZSByZWdleCBpZiB3ZSBkb24ndCBoYXZlIGl0IGFscmVhZHlcbiAgICAgICAgbW9tID0gY3JlYXRlVVRDKFsyMDAwLCBpXSk7XG4gICAgICAgIGlmIChzdHJpY3QgJiYgIXRoaXMuX2xvbmdNb250aHNQYXJzZVtpXSkge1xuICAgICAgICAgICAgdGhpcy5fbG9uZ01vbnRoc1BhcnNlW2ldID0gbmV3IFJlZ0V4cCgnXicgKyB0aGlzLm1vbnRocyhtb20sICcnKS5yZXBsYWNlKCcuJywgJycpICsgJyQnLCAnaScpO1xuICAgICAgICAgICAgdGhpcy5fc2hvcnRNb250aHNQYXJzZVtpXSA9IG5ldyBSZWdFeHAoJ14nICsgdGhpcy5tb250aHNTaG9ydChtb20sICcnKS5yZXBsYWNlKCcuJywgJycpICsgJyQnLCAnaScpO1xuICAgICAgICB9XG4gICAgICAgIGlmICghc3RyaWN0ICYmICF0aGlzLl9tb250aHNQYXJzZVtpXSkge1xuICAgICAgICAgICAgcmVnZXggPSAnXicgKyB0aGlzLm1vbnRocyhtb20sICcnKSArICd8XicgKyB0aGlzLm1vbnRoc1Nob3J0KG1vbSwgJycpO1xuICAgICAgICAgICAgdGhpcy5fbW9udGhzUGFyc2VbaV0gPSBuZXcgUmVnRXhwKHJlZ2V4LnJlcGxhY2UoJy4nLCAnJyksICdpJyk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gdGVzdCB0aGUgcmVnZXhcbiAgICAgICAgaWYgKHN0cmljdCAmJiBmb3JtYXQgPT09ICdNTU1NJyAmJiB0aGlzLl9sb25nTW9udGhzUGFyc2VbaV0udGVzdChtb250aE5hbWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gaTtcbiAgICAgICAgfSBlbHNlIGlmIChzdHJpY3QgJiYgZm9ybWF0ID09PSAnTU1NJyAmJiB0aGlzLl9zaG9ydE1vbnRoc1BhcnNlW2ldLnRlc3QobW9udGhOYW1lKSkge1xuICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgIH0gZWxzZSBpZiAoIXN0cmljdCAmJiB0aGlzLl9tb250aHNQYXJzZVtpXS50ZXN0KG1vbnRoTmFtZSkpIHtcbiAgICAgICAgICAgIHJldHVybiBpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRNb250aCAobW9tLCB2YWx1ZSkge1xuICAgIHZhciBkYXlPZk1vbnRoO1xuXG4gICAgaWYgKCFtb20uaXNWYWxpZCgpKSB7XG4gICAgICAgIC8vIE5vIG9wXG4gICAgICAgIHJldHVybiBtb207XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgaWYgKC9eXFxkKyQvLnRlc3QodmFsdWUpKSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHRvSW50KHZhbHVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHZhbHVlID0gbW9tLmxvY2FsZURhdGEoKS5tb250aHNQYXJzZSh2YWx1ZSk7XG4gICAgICAgICAgICAvLyBUT0RPOiBBbm90aGVyIHNpbGVudCBmYWlsdXJlP1xuICAgICAgICAgICAgaWYgKCFpc051bWJlcih2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9tO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZGF5T2ZNb250aCA9IE1hdGgubWluKG1vbS5kYXRlKCksIGRheXNJbk1vbnRoKG1vbS55ZWFyKCksIHZhbHVlKSk7XG4gICAgbW9tLl9kWydzZXQnICsgKG1vbS5faXNVVEMgPyAnVVRDJyA6ICcnKSArICdNb250aCddKHZhbHVlLCBkYXlPZk1vbnRoKTtcbiAgICByZXR1cm4gbW9tO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0U2V0TW9udGggKHZhbHVlKSB7XG4gICAgaWYgKHZhbHVlICE9IG51bGwpIHtcbiAgICAgICAgc2V0TW9udGgodGhpcywgdmFsdWUpO1xuICAgICAgICBob29rcy51cGRhdGVPZmZzZXQodGhpcywgdHJ1ZSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBnZXQodGhpcywgJ01vbnRoJyk7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RGF5c0luTW9udGggKCkge1xuICAgIHJldHVybiBkYXlzSW5Nb250aCh0aGlzLnllYXIoKSwgdGhpcy5tb250aCgpKTtcbn1cblxudmFyIGRlZmF1bHRNb250aHNTaG9ydFJlZ2V4ID0gbWF0Y2hXb3JkO1xuZXhwb3J0IGZ1bmN0aW9uIG1vbnRoc1Nob3J0UmVnZXggKGlzU3RyaWN0KSB7XG4gICAgaWYgKHRoaXMuX21vbnRoc1BhcnNlRXhhY3QpIHtcbiAgICAgICAgaWYgKCFoYXNPd25Qcm9wKHRoaXMsICdfbW9udGhzUmVnZXgnKSkge1xuICAgICAgICAgICAgY29tcHV0ZU1vbnRoc1BhcnNlLmNhbGwodGhpcyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzU3RyaWN0KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fbW9udGhzU2hvcnRTdHJpY3RSZWdleDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9tb250aHNTaG9ydFJlZ2V4O1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFoYXNPd25Qcm9wKHRoaXMsICdfbW9udGhzU2hvcnRSZWdleCcpKSB7XG4gICAgICAgICAgICB0aGlzLl9tb250aHNTaG9ydFJlZ2V4ID0gZGVmYXVsdE1vbnRoc1Nob3J0UmVnZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRoc1Nob3J0U3RyaWN0UmVnZXggJiYgaXNTdHJpY3QgP1xuICAgICAgICAgICAgdGhpcy5fbW9udGhzU2hvcnRTdHJpY3RSZWdleCA6IHRoaXMuX21vbnRoc1Nob3J0UmVnZXg7XG4gICAgfVxufVxuXG52YXIgZGVmYXVsdE1vbnRoc1JlZ2V4ID0gbWF0Y2hXb3JkO1xuZXhwb3J0IGZ1bmN0aW9uIG1vbnRoc1JlZ2V4IChpc1N0cmljdCkge1xuICAgIGlmICh0aGlzLl9tb250aHNQYXJzZUV4YWN0KSB7XG4gICAgICAgIGlmICghaGFzT3duUHJvcCh0aGlzLCAnX21vbnRoc1JlZ2V4JykpIHtcbiAgICAgICAgICAgIGNvbXB1dGVNb250aHNQYXJzZS5jYWxsKHRoaXMpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpc1N0cmljdCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRoc1N0cmljdFJlZ2V4O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRoc1JlZ2V4O1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFoYXNPd25Qcm9wKHRoaXMsICdfbW9udGhzUmVnZXgnKSkge1xuICAgICAgICAgICAgdGhpcy5fbW9udGhzUmVnZXggPSBkZWZhdWx0TW9udGhzUmVnZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX21vbnRoc1N0cmljdFJlZ2V4ICYmIGlzU3RyaWN0ID9cbiAgICAgICAgICAgIHRoaXMuX21vbnRoc1N0cmljdFJlZ2V4IDogdGhpcy5fbW9udGhzUmVnZXg7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBjb21wdXRlTW9udGhzUGFyc2UgKCkge1xuICAgIGZ1bmN0aW9uIGNtcExlblJldihhLCBiKSB7XG4gICAgICAgIHJldHVybiBiLmxlbmd0aCAtIGEubGVuZ3RoO1xuICAgIH1cblxuICAgIHZhciBzaG9ydFBpZWNlcyA9IFtdLCBsb25nUGllY2VzID0gW10sIG1peGVkUGllY2VzID0gW10sXG4gICAgICAgIGksIG1vbTtcbiAgICBmb3IgKGkgPSAwOyBpIDwgMTI7IGkrKykge1xuICAgICAgICAvLyBtYWtlIHRoZSByZWdleCBpZiB3ZSBkb24ndCBoYXZlIGl0IGFscmVhZHlcbiAgICAgICAgbW9tID0gY3JlYXRlVVRDKFsyMDAwLCBpXSk7XG4gICAgICAgIHNob3J0UGllY2VzLnB1c2godGhpcy5tb250aHNTaG9ydChtb20sICcnKSk7XG4gICAgICAgIGxvbmdQaWVjZXMucHVzaCh0aGlzLm1vbnRocyhtb20sICcnKSk7XG4gICAgICAgIG1peGVkUGllY2VzLnB1c2godGhpcy5tb250aHMobW9tLCAnJykpO1xuICAgICAgICBtaXhlZFBpZWNlcy5wdXNoKHRoaXMubW9udGhzU2hvcnQobW9tLCAnJykpO1xuICAgIH1cbiAgICAvLyBTb3J0aW5nIG1ha2VzIHN1cmUgaWYgb25lIG1vbnRoIChvciBhYmJyKSBpcyBhIHByZWZpeCBvZiBhbm90aGVyIGl0XG4gICAgLy8gd2lsbCBtYXRjaCB0aGUgbG9uZ2VyIHBpZWNlLlxuICAgIHNob3J0UGllY2VzLnNvcnQoY21wTGVuUmV2KTtcbiAgICBsb25nUGllY2VzLnNvcnQoY21wTGVuUmV2KTtcbiAgICBtaXhlZFBpZWNlcy5zb3J0KGNtcExlblJldik7XG4gICAgZm9yIChpID0gMDsgaSA8IDEyOyBpKyspIHtcbiAgICAgICAgc2hvcnRQaWVjZXNbaV0gPSByZWdleEVzY2FwZShzaG9ydFBpZWNlc1tpXSk7XG4gICAgICAgIGxvbmdQaWVjZXNbaV0gPSByZWdleEVzY2FwZShsb25nUGllY2VzW2ldKTtcbiAgICB9XG4gICAgZm9yIChpID0gMDsgaSA8IDI0OyBpKyspIHtcbiAgICAgICAgbWl4ZWRQaWVjZXNbaV0gPSByZWdleEVzY2FwZShtaXhlZFBpZWNlc1tpXSk7XG4gICAgfVxuXG4gICAgdGhpcy5fbW9udGhzUmVnZXggPSBuZXcgUmVnRXhwKCdeKCcgKyBtaXhlZFBpZWNlcy5qb2luKCd8JykgKyAnKScsICdpJyk7XG4gICAgdGhpcy5fbW9udGhzU2hvcnRSZWdleCA9IHRoaXMuX21vbnRoc1JlZ2V4O1xuICAgIHRoaXMuX21vbnRoc1N0cmljdFJlZ2V4ID0gbmV3IFJlZ0V4cCgnXignICsgbG9uZ1BpZWNlcy5qb2luKCd8JykgKyAnKScsICdpJyk7XG4gICAgdGhpcy5fbW9udGhzU2hvcnRTdHJpY3RSZWdleCA9IG5ldyBSZWdFeHAoJ14oJyArIHNob3J0UGllY2VzLmpvaW4oJ3wnKSArICcpJywgJ2knKTtcbn1cbiIsImltcG9ydCB7IG1ha2VHZXRTZXQgfSBmcm9tICcuLi9tb21lbnQvZ2V0LXNldCc7XG5pbXBvcnQgeyBhZGRGb3JtYXRUb2tlbiB9IGZyb20gJy4uL2Zvcm1hdC9mb3JtYXQnO1xuaW1wb3J0IHsgYWRkVW5pdEFsaWFzIH0gZnJvbSAnLi9hbGlhc2VzJztcbmltcG9ydCB7IGFkZFVuaXRQcmlvcml0eSB9IGZyb20gJy4vcHJpb3JpdGllcyc7XG5pbXBvcnQgeyBhZGRSZWdleFRva2VuLCBtYXRjaDF0bzIsIG1hdGNoMXRvNCwgbWF0Y2gxdG82LCBtYXRjaDIsIG1hdGNoNCwgbWF0Y2g2LCBtYXRjaFNpZ25lZCB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFBhcnNlVG9rZW4gfSBmcm9tICcuLi9wYXJzZS90b2tlbic7XG5pbXBvcnQgeyBob29rcyB9IGZyb20gJy4uL3V0aWxzL2hvb2tzJztcbmltcG9ydCB7IFlFQVIgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbignWScsIDAsIDAsIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgeSA9IHRoaXMueWVhcigpO1xuICAgIHJldHVybiB5IDw9IDk5OTkgPyAnJyArIHkgOiAnKycgKyB5O1xufSk7XG5cbmFkZEZvcm1hdFRva2VuKDAsIFsnWVknLCAyXSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnllYXIoKSAlIDEwMDtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbigwLCBbJ1lZWVknLCAgIDRdLCAgICAgICAwLCAneWVhcicpO1xuYWRkRm9ybWF0VG9rZW4oMCwgWydZWVlZWScsICA1XSwgICAgICAgMCwgJ3llYXInKTtcbmFkZEZvcm1hdFRva2VuKDAsIFsnWVlZWVlZJywgNiwgdHJ1ZV0sIDAsICd5ZWFyJyk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCd5ZWFyJywgJ3knKTtcblxuLy8gUFJJT1JJVElFU1xuXG5hZGRVbml0UHJpb3JpdHkoJ3llYXInLCAxKTtcblxuLy8gUEFSU0lOR1xuXG5hZGRSZWdleFRva2VuKCdZJywgICAgICBtYXRjaFNpZ25lZCk7XG5hZGRSZWdleFRva2VuKCdZWScsICAgICBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdZWVlZJywgICBtYXRjaDF0bzQsIG1hdGNoNCk7XG5hZGRSZWdleFRva2VuKCdZWVlZWScsICBtYXRjaDF0bzYsIG1hdGNoNik7XG5hZGRSZWdleFRva2VuKCdZWVlZWVknLCBtYXRjaDF0bzYsIG1hdGNoNik7XG5cbmFkZFBhcnNlVG9rZW4oWydZWVlZWScsICdZWVlZWVknXSwgWUVBUik7XG5hZGRQYXJzZVRva2VuKCdZWVlZJywgZnVuY3Rpb24gKGlucHV0LCBhcnJheSkge1xuICAgIGFycmF5W1lFQVJdID0gaW5wdXQubGVuZ3RoID09PSAyID8gaG9va3MucGFyc2VUd29EaWdpdFllYXIoaW5wdXQpIDogdG9JbnQoaW5wdXQpO1xufSk7XG5hZGRQYXJzZVRva2VuKCdZWScsIGZ1bmN0aW9uIChpbnB1dCwgYXJyYXkpIHtcbiAgICBhcnJheVtZRUFSXSA9IGhvb2tzLnBhcnNlVHdvRGlnaXRZZWFyKGlucHV0KTtcbn0pO1xuYWRkUGFyc2VUb2tlbignWScsIGZ1bmN0aW9uIChpbnB1dCwgYXJyYXkpIHtcbiAgICBhcnJheVtZRUFSXSA9IHBhcnNlSW50KGlucHV0LCAxMCk7XG59KTtcblxuLy8gSEVMUEVSU1xuXG5leHBvcnQgZnVuY3Rpb24gZGF5c0luWWVhcih5ZWFyKSB7XG4gICAgcmV0dXJuIGlzTGVhcFllYXIoeWVhcikgPyAzNjYgOiAzNjU7XG59XG5cbmZ1bmN0aW9uIGlzTGVhcFllYXIoeWVhcikge1xuICAgIHJldHVybiAoeWVhciAlIDQgPT09IDAgJiYgeWVhciAlIDEwMCAhPT0gMCkgfHwgeWVhciAlIDQwMCA9PT0gMDtcbn1cblxuLy8gSE9PS1NcblxuaG9va3MucGFyc2VUd29EaWdpdFllYXIgPSBmdW5jdGlvbiAoaW5wdXQpIHtcbiAgICByZXR1cm4gdG9JbnQoaW5wdXQpICsgKHRvSW50KGlucHV0KSA+IDY4ID8gMTkwMCA6IDIwMDApO1xufTtcblxuLy8gTU9NRU5UU1xuXG5leHBvcnQgdmFyIGdldFNldFllYXIgPSBtYWtlR2V0U2V0KCdGdWxsWWVhcicsIHRydWUpO1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SXNMZWFwWWVhciAoKSB7XG4gICAgcmV0dXJuIGlzTGVhcFllYXIodGhpcy55ZWFyKCkpO1xufVxuIiwiZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZURhdGUgKHksIG0sIGQsIGgsIE0sIHMsIG1zKSB7XG4gICAgLy9jYW4ndCBqdXN0IGFwcGx5KCkgdG8gY3JlYXRlIGEgZGF0ZTpcbiAgICAvL2h0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMTgxMzQ4L2luc3RhbnRpYXRpbmctYS1qYXZhc2NyaXB0LW9iamVjdC1ieS1jYWxsaW5nLXByb3RvdHlwZS1jb25zdHJ1Y3Rvci1hcHBseVxuICAgIHZhciBkYXRlID0gbmV3IERhdGUoeSwgbSwgZCwgaCwgTSwgcywgbXMpO1xuXG4gICAgLy90aGUgZGF0ZSBjb25zdHJ1Y3RvciByZW1hcHMgeWVhcnMgMC05OSB0byAxOTAwLTE5OTlcbiAgICBpZiAoeSA8IDEwMCAmJiB5ID49IDAgJiYgaXNGaW5pdGUoZGF0ZS5nZXRGdWxsWWVhcigpKSkge1xuICAgICAgICBkYXRlLnNldEZ1bGxZZWFyKHkpO1xuICAgIH1cbiAgICByZXR1cm4gZGF0ZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZVVUQ0RhdGUgKHkpIHtcbiAgICB2YXIgZGF0ZSA9IG5ldyBEYXRlKERhdGUuVVRDLmFwcGx5KG51bGwsIGFyZ3VtZW50cykpO1xuXG4gICAgLy90aGUgRGF0ZS5VVEMgZnVuY3Rpb24gcmVtYXBzIHllYXJzIDAtOTkgdG8gMTkwMC0xOTk5XG4gICAgaWYgKHkgPCAxMDAgJiYgeSA+PSAwICYmIGlzRmluaXRlKGRhdGUuZ2V0VVRDRnVsbFllYXIoKSkpIHtcbiAgICAgICAgZGF0ZS5zZXRVVENGdWxsWWVhcih5KTtcbiAgICB9XG4gICAgcmV0dXJuIGRhdGU7XG59XG4iLCJpbXBvcnQgeyBkYXlzSW5ZZWFyIH0gZnJvbSAnLi95ZWFyJztcbmltcG9ydCB7IGNyZWF0ZUxvY2FsIH0gZnJvbSAnLi4vY3JlYXRlL2xvY2FsJztcbmltcG9ydCB7IGNyZWF0ZVVUQ0RhdGUgfSBmcm9tICcuLi9jcmVhdGUvZGF0ZS1mcm9tLWFycmF5JztcblxuLy8gc3RhcnQtb2YtZmlyc3Qtd2VlayAtIHN0YXJ0LW9mLXllYXJcbmZ1bmN0aW9uIGZpcnN0V2Vla09mZnNldCh5ZWFyLCBkb3csIGRveSkge1xuICAgIHZhciAvLyBmaXJzdC13ZWVrIGRheSAtLSB3aGljaCBqYW51YXJ5IGlzIGFsd2F5cyBpbiB0aGUgZmlyc3Qgd2VlayAoNCBmb3IgaXNvLCAxIGZvciBvdGhlcilcbiAgICAgICAgZndkID0gNyArIGRvdyAtIGRveSxcbiAgICAgICAgLy8gZmlyc3Qtd2VlayBkYXkgbG9jYWwgd2Vla2RheSAtLSB3aGljaCBsb2NhbCB3ZWVrZGF5IGlzIGZ3ZFxuICAgICAgICBmd2RsdyA9ICg3ICsgY3JlYXRlVVRDRGF0ZSh5ZWFyLCAwLCBmd2QpLmdldFVUQ0RheSgpIC0gZG93KSAlIDc7XG5cbiAgICByZXR1cm4gLWZ3ZGx3ICsgZndkIC0gMTtcbn1cblxuLy9odHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT193ZWVrX2RhdGUjQ2FsY3VsYXRpbmdfYV9kYXRlX2dpdmVuX3RoZV95ZWFyLjJDX3dlZWtfbnVtYmVyX2FuZF93ZWVrZGF5XG5leHBvcnQgZnVuY3Rpb24gZGF5T2ZZZWFyRnJvbVdlZWtzKHllYXIsIHdlZWssIHdlZWtkYXksIGRvdywgZG95KSB7XG4gICAgdmFyIGxvY2FsV2Vla2RheSA9ICg3ICsgd2Vla2RheSAtIGRvdykgJSA3LFxuICAgICAgICB3ZWVrT2Zmc2V0ID0gZmlyc3RXZWVrT2Zmc2V0KHllYXIsIGRvdywgZG95KSxcbiAgICAgICAgZGF5T2ZZZWFyID0gMSArIDcgKiAod2VlayAtIDEpICsgbG9jYWxXZWVrZGF5ICsgd2Vla09mZnNldCxcbiAgICAgICAgcmVzWWVhciwgcmVzRGF5T2ZZZWFyO1xuXG4gICAgaWYgKGRheU9mWWVhciA8PSAwKSB7XG4gICAgICAgIHJlc1llYXIgPSB5ZWFyIC0gMTtcbiAgICAgICAgcmVzRGF5T2ZZZWFyID0gZGF5c0luWWVhcihyZXNZZWFyKSArIGRheU9mWWVhcjtcbiAgICB9IGVsc2UgaWYgKGRheU9mWWVhciA+IGRheXNJblllYXIoeWVhcikpIHtcbiAgICAgICAgcmVzWWVhciA9IHllYXIgKyAxO1xuICAgICAgICByZXNEYXlPZlllYXIgPSBkYXlPZlllYXIgLSBkYXlzSW5ZZWFyKHllYXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc1llYXIgPSB5ZWFyO1xuICAgICAgICByZXNEYXlPZlllYXIgPSBkYXlPZlllYXI7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgeWVhcjogcmVzWWVhcixcbiAgICAgICAgZGF5T2ZZZWFyOiByZXNEYXlPZlllYXJcbiAgICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gd2Vla09mWWVhcihtb20sIGRvdywgZG95KSB7XG4gICAgdmFyIHdlZWtPZmZzZXQgPSBmaXJzdFdlZWtPZmZzZXQobW9tLnllYXIoKSwgZG93LCBkb3kpLFxuICAgICAgICB3ZWVrID0gTWF0aC5mbG9vcigobW9tLmRheU9mWWVhcigpIC0gd2Vla09mZnNldCAtIDEpIC8gNykgKyAxLFxuICAgICAgICByZXNXZWVrLCByZXNZZWFyO1xuXG4gICAgaWYgKHdlZWsgPCAxKSB7XG4gICAgICAgIHJlc1llYXIgPSBtb20ueWVhcigpIC0gMTtcbiAgICAgICAgcmVzV2VlayA9IHdlZWsgKyB3ZWVrc0luWWVhcihyZXNZZWFyLCBkb3csIGRveSk7XG4gICAgfSBlbHNlIGlmICh3ZWVrID4gd2Vla3NJblllYXIobW9tLnllYXIoKSwgZG93LCBkb3kpKSB7XG4gICAgICAgIHJlc1dlZWsgPSB3ZWVrIC0gd2Vla3NJblllYXIobW9tLnllYXIoKSwgZG93LCBkb3kpO1xuICAgICAgICByZXNZZWFyID0gbW9tLnllYXIoKSArIDE7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmVzWWVhciA9IG1vbS55ZWFyKCk7XG4gICAgICAgIHJlc1dlZWsgPSB3ZWVrO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICAgIHdlZWs6IHJlc1dlZWssXG4gICAgICAgIHllYXI6IHJlc1llYXJcbiAgICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gd2Vla3NJblllYXIoeWVhciwgZG93LCBkb3kpIHtcbiAgICB2YXIgd2Vla09mZnNldCA9IGZpcnN0V2Vla09mZnNldCh5ZWFyLCBkb3csIGRveSksXG4gICAgICAgIHdlZWtPZmZzZXROZXh0ID0gZmlyc3RXZWVrT2Zmc2V0KHllYXIgKyAxLCBkb3csIGRveSk7XG4gICAgcmV0dXJuIChkYXlzSW5ZZWFyKHllYXIpIC0gd2Vla09mZnNldCArIHdlZWtPZmZzZXROZXh0KSAvIDc7XG59XG4iLCJpbXBvcnQgeyBhZGRGb3JtYXRUb2tlbiB9IGZyb20gJy4uL2Zvcm1hdC9mb3JtYXQnO1xuaW1wb3J0IHsgYWRkVW5pdEFsaWFzIH0gZnJvbSAnLi9hbGlhc2VzJztcbmltcG9ydCB7IGFkZFVuaXRQcmlvcml0eSB9IGZyb20gJy4vcHJpb3JpdGllcyc7XG5pbXBvcnQgeyBhZGRSZWdleFRva2VuLCBtYXRjaDF0bzIsIG1hdGNoMiB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFdlZWtQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHRvSW50IGZyb20gJy4uL3V0aWxzL3RvLWludCc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyB3ZWVrT2ZZZWFyIH0gZnJvbSAnLi93ZWVrLWNhbGVuZGFyLXV0aWxzJztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbigndycsIFsnd3cnLCAyXSwgJ3dvJywgJ3dlZWsnKTtcbmFkZEZvcm1hdFRva2VuKCdXJywgWydXVycsIDJdLCAnV28nLCAnaXNvV2VlaycpO1xuXG4vLyBBTElBU0VTXG5cbmFkZFVuaXRBbGlhcygnd2VlaycsICd3Jyk7XG5hZGRVbml0QWxpYXMoJ2lzb1dlZWsnLCAnVycpO1xuXG4vLyBQUklPUklUSUVTXG5cbmFkZFVuaXRQcmlvcml0eSgnd2VlaycsIDUpO1xuYWRkVW5pdFByaW9yaXR5KCdpc29XZWVrJywgNSk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbigndycsICBtYXRjaDF0bzIpO1xuYWRkUmVnZXhUb2tlbignd3cnLCBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdXJywgIG1hdGNoMXRvMik7XG5hZGRSZWdleFRva2VuKCdXVycsIG1hdGNoMXRvMiwgbWF0Y2gyKTtcblxuYWRkV2Vla1BhcnNlVG9rZW4oWyd3JywgJ3d3JywgJ1cnLCAnV1cnXSwgZnVuY3Rpb24gKGlucHV0LCB3ZWVrLCBjb25maWcsIHRva2VuKSB7XG4gICAgd2Vla1t0b2tlbi5zdWJzdHIoMCwgMSldID0gdG9JbnQoaW5wdXQpO1xufSk7XG5cbi8vIEhFTFBFUlNcblxuLy8gTE9DQUxFU1xuXG5leHBvcnQgZnVuY3Rpb24gbG9jYWxlV2VlayAobW9tKSB7XG4gICAgcmV0dXJuIHdlZWtPZlllYXIobW9tLCB0aGlzLl93ZWVrLmRvdywgdGhpcy5fd2Vlay5kb3kpLndlZWs7XG59XG5cbmV4cG9ydCB2YXIgZGVmYXVsdExvY2FsZVdlZWsgPSB7XG4gICAgZG93IDogMCwgLy8gU3VuZGF5IGlzIHRoZSBmaXJzdCBkYXkgb2YgdGhlIHdlZWsuXG4gICAgZG95IDogNiAgLy8gVGhlIHdlZWsgdGhhdCBjb250YWlucyBKYW4gMXN0IGlzIHRoZSBmaXJzdCB3ZWVrIG9mIHRoZSB5ZWFyLlxufTtcblxuZXhwb3J0IGZ1bmN0aW9uIGxvY2FsZUZpcnN0RGF5T2ZXZWVrICgpIHtcbiAgICByZXR1cm4gdGhpcy5fd2Vlay5kb3c7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2NhbGVGaXJzdERheU9mWWVhciAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3dlZWsuZG95O1xufVxuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTZXRXZWVrIChpbnB1dCkge1xuICAgIHZhciB3ZWVrID0gdGhpcy5sb2NhbGVEYXRhKCkud2Vlayh0aGlzKTtcbiAgICByZXR1cm4gaW5wdXQgPT0gbnVsbCA/IHdlZWsgOiB0aGlzLmFkZCgoaW5wdXQgLSB3ZWVrKSAqIDcsICdkJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTZXRJU09XZWVrIChpbnB1dCkge1xuICAgIHZhciB3ZWVrID0gd2Vla09mWWVhcih0aGlzLCAxLCA0KS53ZWVrO1xuICAgIHJldHVybiBpbnB1dCA9PSBudWxsID8gd2VlayA6IHRoaXMuYWRkKChpbnB1dCAtIHdlZWspICogNywgJ2QnKTtcbn1cbiIsImltcG9ydCB7IGFkZEZvcm1hdFRva2VuIH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBhZGRVbml0QWxpYXMgfSBmcm9tICcuL2FsaWFzZXMnO1xuaW1wb3J0IHsgYWRkVW5pdFByaW9yaXR5IH0gZnJvbSAnLi9wcmlvcml0aWVzJztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoMXRvMiwgbWF0Y2hXb3JkLCByZWdleEVzY2FwZSB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFdlZWtQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHRvSW50IGZyb20gJy4uL3V0aWxzL3RvLWludCc7XG5pbXBvcnQgaXNBcnJheSBmcm9tICcuLi91dGlscy9pcy1hcnJheSc7XG5pbXBvcnQgaW5kZXhPZiBmcm9tICcuLi91dGlscy9pbmRleC1vZic7XG5pbXBvcnQgaGFzT3duUHJvcCBmcm9tICcuLi91dGlscy9oYXMtb3duLXByb3AnO1xuaW1wb3J0IHsgY3JlYXRlVVRDIH0gZnJvbSAnLi4vY3JlYXRlL3V0Yyc7XG5pbXBvcnQgZ2V0UGFyc2luZ0ZsYWdzIGZyb20gJy4uL2NyZWF0ZS9wYXJzaW5nLWZsYWdzJztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbignZCcsIDAsICdkbycsICdkYXknKTtcblxuYWRkRm9ybWF0VG9rZW4oJ2RkJywgMCwgMCwgZnVuY3Rpb24gKGZvcm1hdCkge1xuICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEoKS53ZWVrZGF5c01pbih0aGlzLCBmb3JtYXQpO1xufSk7XG5cbmFkZEZvcm1hdFRva2VuKCdkZGQnLCAwLCAwLCBmdW5jdGlvbiAoZm9ybWF0KSB7XG4gICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YSgpLndlZWtkYXlzU2hvcnQodGhpcywgZm9ybWF0KTtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbignZGRkZCcsIDAsIDAsIGZ1bmN0aW9uIChmb3JtYXQpIHtcbiAgICByZXR1cm4gdGhpcy5sb2NhbGVEYXRhKCkud2Vla2RheXModGhpcywgZm9ybWF0KTtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbignZScsIDAsIDAsICd3ZWVrZGF5Jyk7XG5hZGRGb3JtYXRUb2tlbignRScsIDAsIDAsICdpc29XZWVrZGF5Jyk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCdkYXknLCAnZCcpO1xuYWRkVW5pdEFsaWFzKCd3ZWVrZGF5JywgJ2UnKTtcbmFkZFVuaXRBbGlhcygnaXNvV2Vla2RheScsICdFJyk7XG5cbi8vIFBSSU9SSVRZXG5hZGRVbml0UHJpb3JpdHkoJ2RheScsIDExKTtcbmFkZFVuaXRQcmlvcml0eSgnd2Vla2RheScsIDExKTtcbmFkZFVuaXRQcmlvcml0eSgnaXNvV2Vla2RheScsIDExKTtcblxuLy8gUEFSU0lOR1xuXG5hZGRSZWdleFRva2VuKCdkJywgICAgbWF0Y2gxdG8yKTtcbmFkZFJlZ2V4VG9rZW4oJ2UnLCAgICBtYXRjaDF0bzIpO1xuYWRkUmVnZXhUb2tlbignRScsICAgIG1hdGNoMXRvMik7XG5hZGRSZWdleFRva2VuKCdkZCcsICAgZnVuY3Rpb24gKGlzU3RyaWN0LCBsb2NhbGUpIHtcbiAgICByZXR1cm4gbG9jYWxlLndlZWtkYXlzTWluUmVnZXgoaXNTdHJpY3QpO1xufSk7XG5hZGRSZWdleFRva2VuKCdkZGQnLCAgIGZ1bmN0aW9uIChpc1N0cmljdCwgbG9jYWxlKSB7XG4gICAgcmV0dXJuIGxvY2FsZS53ZWVrZGF5c1Nob3J0UmVnZXgoaXNTdHJpY3QpO1xufSk7XG5hZGRSZWdleFRva2VuKCdkZGRkJywgICBmdW5jdGlvbiAoaXNTdHJpY3QsIGxvY2FsZSkge1xuICAgIHJldHVybiBsb2NhbGUud2Vla2RheXNSZWdleChpc1N0cmljdCk7XG59KTtcblxuYWRkV2Vla1BhcnNlVG9rZW4oWydkZCcsICdkZGQnLCAnZGRkZCddLCBmdW5jdGlvbiAoaW5wdXQsIHdlZWssIGNvbmZpZywgdG9rZW4pIHtcbiAgICB2YXIgd2Vla2RheSA9IGNvbmZpZy5fbG9jYWxlLndlZWtkYXlzUGFyc2UoaW5wdXQsIHRva2VuLCBjb25maWcuX3N0cmljdCk7XG4gICAgLy8gaWYgd2UgZGlkbid0IGdldCBhIHdlZWtkYXkgbmFtZSwgbWFyayB0aGUgZGF0ZSBhcyBpbnZhbGlkXG4gICAgaWYgKHdlZWtkYXkgIT0gbnVsbCkge1xuICAgICAgICB3ZWVrLmQgPSB3ZWVrZGF5O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLmludmFsaWRXZWVrZGF5ID0gaW5wdXQ7XG4gICAgfVxufSk7XG5cbmFkZFdlZWtQYXJzZVRva2VuKFsnZCcsICdlJywgJ0UnXSwgZnVuY3Rpb24gKGlucHV0LCB3ZWVrLCBjb25maWcsIHRva2VuKSB7XG4gICAgd2Vla1t0b2tlbl0gPSB0b0ludChpbnB1dCk7XG59KTtcblxuLy8gSEVMUEVSU1xuXG5mdW5jdGlvbiBwYXJzZVdlZWtkYXkoaW5wdXQsIGxvY2FsZSkge1xuICAgIGlmICh0eXBlb2YgaW5wdXQgIT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiBpbnB1dDtcbiAgICB9XG5cbiAgICBpZiAoIWlzTmFOKGlucHV0KSkge1xuICAgICAgICByZXR1cm4gcGFyc2VJbnQoaW5wdXQsIDEwKTtcbiAgICB9XG5cbiAgICBpbnB1dCA9IGxvY2FsZS53ZWVrZGF5c1BhcnNlKGlucHV0KTtcbiAgICBpZiAodHlwZW9mIGlucHV0ID09PSAnbnVtYmVyJykge1xuICAgICAgICByZXR1cm4gaW5wdXQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIG51bGw7XG59XG5cbmZ1bmN0aW9uIHBhcnNlSXNvV2Vla2RheShpbnB1dCwgbG9jYWxlKSB7XG4gICAgaWYgKHR5cGVvZiBpbnB1dCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcmV0dXJuIGxvY2FsZS53ZWVrZGF5c1BhcnNlKGlucHV0KSAlIDcgfHwgNztcbiAgICB9XG4gICAgcmV0dXJuIGlzTmFOKGlucHV0KSA/IG51bGwgOiBpbnB1dDtcbn1cblxuLy8gTE9DQUxFU1xuXG5leHBvcnQgdmFyIGRlZmF1bHRMb2NhbGVXZWVrZGF5cyA9ICdTdW5kYXlfTW9uZGF5X1R1ZXNkYXlfV2VkbmVzZGF5X1RodXJzZGF5X0ZyaWRheV9TYXR1cmRheScuc3BsaXQoJ18nKTtcbmV4cG9ydCBmdW5jdGlvbiBsb2NhbGVXZWVrZGF5cyAobSwgZm9ybWF0KSB7XG4gICAgaWYgKCFtKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl93ZWVrZGF5cztcbiAgICB9XG4gICAgcmV0dXJuIGlzQXJyYXkodGhpcy5fd2Vla2RheXMpID8gdGhpcy5fd2Vla2RheXNbbS5kYXkoKV0gOlxuICAgICAgICB0aGlzLl93ZWVrZGF5c1t0aGlzLl93ZWVrZGF5cy5pc0Zvcm1hdC50ZXN0KGZvcm1hdCkgPyAnZm9ybWF0JyA6ICdzdGFuZGFsb25lJ11bbS5kYXkoKV07XG59XG5cbmV4cG9ydCB2YXIgZGVmYXVsdExvY2FsZVdlZWtkYXlzU2hvcnQgPSAnU3VuX01vbl9UdWVfV2VkX1RodV9GcmlfU2F0Jy5zcGxpdCgnXycpO1xuZXhwb3J0IGZ1bmN0aW9uIGxvY2FsZVdlZWtkYXlzU2hvcnQgKG0pIHtcbiAgICByZXR1cm4gKG0pID8gdGhpcy5fd2Vla2RheXNTaG9ydFttLmRheSgpXSA6IHRoaXMuX3dlZWtkYXlzU2hvcnQ7XG59XG5cbmV4cG9ydCB2YXIgZGVmYXVsdExvY2FsZVdlZWtkYXlzTWluID0gJ1N1X01vX1R1X1dlX1RoX0ZyX1NhJy5zcGxpdCgnXycpO1xuZXhwb3J0IGZ1bmN0aW9uIGxvY2FsZVdlZWtkYXlzTWluIChtKSB7XG4gICAgcmV0dXJuIChtKSA/IHRoaXMuX3dlZWtkYXlzTWluW20uZGF5KCldIDogdGhpcy5fd2Vla2RheXNNaW47XG59XG5cbmZ1bmN0aW9uIGhhbmRsZVN0cmljdFBhcnNlKHdlZWtkYXlOYW1lLCBmb3JtYXQsIHN0cmljdCkge1xuICAgIHZhciBpLCBpaSwgbW9tLCBsbGMgPSB3ZWVrZGF5TmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xuICAgIGlmICghdGhpcy5fd2Vla2RheXNQYXJzZSkge1xuICAgICAgICB0aGlzLl93ZWVrZGF5c1BhcnNlID0gW107XG4gICAgICAgIHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZSA9IFtdO1xuICAgICAgICB0aGlzLl9taW5XZWVrZGF5c1BhcnNlID0gW107XG5cbiAgICAgICAgZm9yIChpID0gMDsgaSA8IDc7ICsraSkge1xuICAgICAgICAgICAgbW9tID0gY3JlYXRlVVRDKFsyMDAwLCAxXSkuZGF5KGkpO1xuICAgICAgICAgICAgdGhpcy5fbWluV2Vla2RheXNQYXJzZVtpXSA9IHRoaXMud2Vla2RheXNNaW4obW9tLCAnJykudG9Mb2NhbGVMb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZVtpXSA9IHRoaXMud2Vla2RheXNTaG9ydChtb20sICcnKS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgdGhpcy5fd2Vla2RheXNQYXJzZVtpXSA9IHRoaXMud2Vla2RheXMobW9tLCAnJykudG9Mb2NhbGVMb3dlckNhc2UoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlmIChzdHJpY3QpIHtcbiAgICAgICAgaWYgKGZvcm1hdCA9PT0gJ2RkZGQnKSB7XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl93ZWVrZGF5c1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgcmV0dXJuIGlpICE9PSAtMSA/IGlpIDogbnVsbDtcbiAgICAgICAgfSBlbHNlIGlmIChmb3JtYXQgPT09ICdkZGQnKSB7XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl9zaG9ydFdlZWtkYXlzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICByZXR1cm4gaWkgIT09IC0xID8gaWkgOiBudWxsO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWkgPSBpbmRleE9mLmNhbGwodGhpcy5fbWluV2Vla2RheXNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIHJldHVybiBpaSAhPT0gLTEgPyBpaSA6IG51bGw7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoZm9ybWF0ID09PSAnZGRkZCcpIHtcbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX3dlZWtkYXlzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICBpZiAoaWkgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGlpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWkgPSBpbmRleE9mLmNhbGwodGhpcy5fc2hvcnRXZWVrZGF5c1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgaWYgKGlpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX21pbldlZWtkYXlzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICByZXR1cm4gaWkgIT09IC0xID8gaWkgOiBudWxsO1xuICAgICAgICB9IGVsc2UgaWYgKGZvcm1hdCA9PT0gJ2RkZCcpIHtcbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIGlmIChpaSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaWk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl93ZWVrZGF5c1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgaWYgKGlpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX21pbldlZWtkYXlzUGFyc2UsIGxsYyk7XG4gICAgICAgICAgICByZXR1cm4gaWkgIT09IC0xID8gaWkgOiBudWxsO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWkgPSBpbmRleE9mLmNhbGwodGhpcy5fbWluV2Vla2RheXNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIGlmIChpaSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaWk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpaSA9IGluZGV4T2YuY2FsbCh0aGlzLl93ZWVrZGF5c1BhcnNlLCBsbGMpO1xuICAgICAgICAgICAgaWYgKGlpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlpID0gaW5kZXhPZi5jYWxsKHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZSwgbGxjKTtcbiAgICAgICAgICAgIHJldHVybiBpaSAhPT0gLTEgPyBpaSA6IG51bGw7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2NhbGVXZWVrZGF5c1BhcnNlICh3ZWVrZGF5TmFtZSwgZm9ybWF0LCBzdHJpY3QpIHtcbiAgICB2YXIgaSwgbW9tLCByZWdleDtcblxuICAgIGlmICh0aGlzLl93ZWVrZGF5c1BhcnNlRXhhY3QpIHtcbiAgICAgICAgcmV0dXJuIGhhbmRsZVN0cmljdFBhcnNlLmNhbGwodGhpcywgd2Vla2RheU5hbWUsIGZvcm1hdCwgc3RyaWN0KTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMuX3dlZWtkYXlzUGFyc2UpIHtcbiAgICAgICAgdGhpcy5fd2Vla2RheXNQYXJzZSA9IFtdO1xuICAgICAgICB0aGlzLl9taW5XZWVrZGF5c1BhcnNlID0gW107XG4gICAgICAgIHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZSA9IFtdO1xuICAgICAgICB0aGlzLl9mdWxsV2Vla2RheXNQYXJzZSA9IFtdO1xuICAgIH1cblxuICAgIGZvciAoaSA9IDA7IGkgPCA3OyBpKyspIHtcbiAgICAgICAgLy8gbWFrZSB0aGUgcmVnZXggaWYgd2UgZG9uJ3QgaGF2ZSBpdCBhbHJlYWR5XG5cbiAgICAgICAgbW9tID0gY3JlYXRlVVRDKFsyMDAwLCAxXSkuZGF5KGkpO1xuICAgICAgICBpZiAoc3RyaWN0ICYmICF0aGlzLl9mdWxsV2Vla2RheXNQYXJzZVtpXSkge1xuICAgICAgICAgICAgdGhpcy5fZnVsbFdlZWtkYXlzUGFyc2VbaV0gPSBuZXcgUmVnRXhwKCdeJyArIHRoaXMud2Vla2RheXMobW9tLCAnJykucmVwbGFjZSgnLicsICdcXC4/JykgKyAnJCcsICdpJyk7XG4gICAgICAgICAgICB0aGlzLl9zaG9ydFdlZWtkYXlzUGFyc2VbaV0gPSBuZXcgUmVnRXhwKCdeJyArIHRoaXMud2Vla2RheXNTaG9ydChtb20sICcnKS5yZXBsYWNlKCcuJywgJ1xcLj8nKSArICckJywgJ2knKTtcbiAgICAgICAgICAgIHRoaXMuX21pbldlZWtkYXlzUGFyc2VbaV0gPSBuZXcgUmVnRXhwKCdeJyArIHRoaXMud2Vla2RheXNNaW4obW9tLCAnJykucmVwbGFjZSgnLicsICdcXC4/JykgKyAnJCcsICdpJyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLl93ZWVrZGF5c1BhcnNlW2ldKSB7XG4gICAgICAgICAgICByZWdleCA9ICdeJyArIHRoaXMud2Vla2RheXMobW9tLCAnJykgKyAnfF4nICsgdGhpcy53ZWVrZGF5c1Nob3J0KG1vbSwgJycpICsgJ3xeJyArIHRoaXMud2Vla2RheXNNaW4obW9tLCAnJyk7XG4gICAgICAgICAgICB0aGlzLl93ZWVrZGF5c1BhcnNlW2ldID0gbmV3IFJlZ0V4cChyZWdleC5yZXBsYWNlKCcuJywgJycpLCAnaScpO1xuICAgICAgICB9XG4gICAgICAgIC8vIHRlc3QgdGhlIHJlZ2V4XG4gICAgICAgIGlmIChzdHJpY3QgJiYgZm9ybWF0ID09PSAnZGRkZCcgJiYgdGhpcy5fZnVsbFdlZWtkYXlzUGFyc2VbaV0udGVzdCh3ZWVrZGF5TmFtZSkpIHtcbiAgICAgICAgICAgIHJldHVybiBpO1xuICAgICAgICB9IGVsc2UgaWYgKHN0cmljdCAmJiBmb3JtYXQgPT09ICdkZGQnICYmIHRoaXMuX3Nob3J0V2Vla2RheXNQYXJzZVtpXS50ZXN0KHdlZWtkYXlOYW1lKSkge1xuICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgIH0gZWxzZSBpZiAoc3RyaWN0ICYmIGZvcm1hdCA9PT0gJ2RkJyAmJiB0aGlzLl9taW5XZWVrZGF5c1BhcnNlW2ldLnRlc3Qod2Vla2RheU5hbWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gaTtcbiAgICAgICAgfSBlbHNlIGlmICghc3RyaWN0ICYmIHRoaXMuX3dlZWtkYXlzUGFyc2VbaV0udGVzdCh3ZWVrZGF5TmFtZSkpIHtcbiAgICAgICAgICAgIHJldHVybiBpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTZXREYXlPZldlZWsgKGlucHV0KSB7XG4gICAgaWYgKCF0aGlzLmlzVmFsaWQoKSkge1xuICAgICAgICByZXR1cm4gaW5wdXQgIT0gbnVsbCA/IHRoaXMgOiBOYU47XG4gICAgfVxuICAgIHZhciBkYXkgPSB0aGlzLl9pc1VUQyA/IHRoaXMuX2QuZ2V0VVRDRGF5KCkgOiB0aGlzLl9kLmdldERheSgpO1xuICAgIGlmIChpbnB1dCAhPSBudWxsKSB7XG4gICAgICAgIGlucHV0ID0gcGFyc2VXZWVrZGF5KGlucHV0LCB0aGlzLmxvY2FsZURhdGEoKSk7XG4gICAgICAgIHJldHVybiB0aGlzLmFkZChpbnB1dCAtIGRheSwgJ2QnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZGF5O1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFNldExvY2FsZURheU9mV2VlayAoaW5wdXQpIHtcbiAgICBpZiAoIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgIHJldHVybiBpbnB1dCAhPSBudWxsID8gdGhpcyA6IE5hTjtcbiAgICB9XG4gICAgdmFyIHdlZWtkYXkgPSAodGhpcy5kYXkoKSArIDcgLSB0aGlzLmxvY2FsZURhdGEoKS5fd2Vlay5kb3cpICUgNztcbiAgICByZXR1cm4gaW5wdXQgPT0gbnVsbCA/IHdlZWtkYXkgOiB0aGlzLmFkZChpbnB1dCAtIHdlZWtkYXksICdkJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTZXRJU09EYXlPZldlZWsgKGlucHV0KSB7XG4gICAgaWYgKCF0aGlzLmlzVmFsaWQoKSkge1xuICAgICAgICByZXR1cm4gaW5wdXQgIT0gbnVsbCA/IHRoaXMgOiBOYU47XG4gICAgfVxuXG4gICAgLy8gYmVoYXZlcyB0aGUgc2FtZSBhcyBtb21lbnQjZGF5IGV4Y2VwdFxuICAgIC8vIGFzIGEgZ2V0dGVyLCByZXR1cm5zIDcgaW5zdGVhZCBvZiAwICgxLTcgcmFuZ2UgaW5zdGVhZCBvZiAwLTYpXG4gICAgLy8gYXMgYSBzZXR0ZXIsIHN1bmRheSBzaG91bGQgYmVsb25nIHRvIHRoZSBwcmV2aW91cyB3ZWVrLlxuXG4gICAgaWYgKGlucHV0ICE9IG51bGwpIHtcbiAgICAgICAgdmFyIHdlZWtkYXkgPSBwYXJzZUlzb1dlZWtkYXkoaW5wdXQsIHRoaXMubG9jYWxlRGF0YSgpKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF5KHRoaXMuZGF5KCkgJSA3ID8gd2Vla2RheSA6IHdlZWtkYXkgLSA3KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5kYXkoKSB8fCA3O1xuICAgIH1cbn1cblxudmFyIGRlZmF1bHRXZWVrZGF5c1JlZ2V4ID0gbWF0Y2hXb3JkO1xuZXhwb3J0IGZ1bmN0aW9uIHdlZWtkYXlzUmVnZXggKGlzU3RyaWN0KSB7XG4gICAgaWYgKHRoaXMuX3dlZWtkYXlzUGFyc2VFeGFjdCkge1xuICAgICAgICBpZiAoIWhhc093blByb3AodGhpcywgJ193ZWVrZGF5c1JlZ2V4JykpIHtcbiAgICAgICAgICAgIGNvbXB1dGVXZWVrZGF5c1BhcnNlLmNhbGwodGhpcyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzU3RyaWN0KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fd2Vla2RheXNTdHJpY3RSZWdleDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl93ZWVrZGF5c1JlZ2V4O1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFoYXNPd25Qcm9wKHRoaXMsICdfd2Vla2RheXNSZWdleCcpKSB7XG4gICAgICAgICAgICB0aGlzLl93ZWVrZGF5c1JlZ2V4ID0gZGVmYXVsdFdlZWtkYXlzUmVnZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3dlZWtkYXlzU3RyaWN0UmVnZXggJiYgaXNTdHJpY3QgP1xuICAgICAgICAgICAgdGhpcy5fd2Vla2RheXNTdHJpY3RSZWdleCA6IHRoaXMuX3dlZWtkYXlzUmVnZXg7XG4gICAgfVxufVxuXG52YXIgZGVmYXVsdFdlZWtkYXlzU2hvcnRSZWdleCA9IG1hdGNoV29yZDtcbmV4cG9ydCBmdW5jdGlvbiB3ZWVrZGF5c1Nob3J0UmVnZXggKGlzU3RyaWN0KSB7XG4gICAgaWYgKHRoaXMuX3dlZWtkYXlzUGFyc2VFeGFjdCkge1xuICAgICAgICBpZiAoIWhhc093blByb3AodGhpcywgJ193ZWVrZGF5c1JlZ2V4JykpIHtcbiAgICAgICAgICAgIGNvbXB1dGVXZWVrZGF5c1BhcnNlLmNhbGwodGhpcyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzU3RyaWN0KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fd2Vla2RheXNTaG9ydFN0cmljdFJlZ2V4O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3dlZWtkYXlzU2hvcnRSZWdleDtcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICghaGFzT3duUHJvcCh0aGlzLCAnX3dlZWtkYXlzU2hvcnRSZWdleCcpKSB7XG4gICAgICAgICAgICB0aGlzLl93ZWVrZGF5c1Nob3J0UmVnZXggPSBkZWZhdWx0V2Vla2RheXNTaG9ydFJlZ2V4O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl93ZWVrZGF5c1Nob3J0U3RyaWN0UmVnZXggJiYgaXNTdHJpY3QgP1xuICAgICAgICAgICAgdGhpcy5fd2Vla2RheXNTaG9ydFN0cmljdFJlZ2V4IDogdGhpcy5fd2Vla2RheXNTaG9ydFJlZ2V4O1xuICAgIH1cbn1cblxudmFyIGRlZmF1bHRXZWVrZGF5c01pblJlZ2V4ID0gbWF0Y2hXb3JkO1xuZXhwb3J0IGZ1bmN0aW9uIHdlZWtkYXlzTWluUmVnZXggKGlzU3RyaWN0KSB7XG4gICAgaWYgKHRoaXMuX3dlZWtkYXlzUGFyc2VFeGFjdCkge1xuICAgICAgICBpZiAoIWhhc093blByb3AodGhpcywgJ193ZWVrZGF5c1JlZ2V4JykpIHtcbiAgICAgICAgICAgIGNvbXB1dGVXZWVrZGF5c1BhcnNlLmNhbGwodGhpcyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzU3RyaWN0KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fd2Vla2RheXNNaW5TdHJpY3RSZWdleDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl93ZWVrZGF5c01pblJlZ2V4O1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFoYXNPd25Qcm9wKHRoaXMsICdfd2Vla2RheXNNaW5SZWdleCcpKSB7XG4gICAgICAgICAgICB0aGlzLl93ZWVrZGF5c01pblJlZ2V4ID0gZGVmYXVsdFdlZWtkYXlzTWluUmVnZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3dlZWtkYXlzTWluU3RyaWN0UmVnZXggJiYgaXNTdHJpY3QgP1xuICAgICAgICAgICAgdGhpcy5fd2Vla2RheXNNaW5TdHJpY3RSZWdleCA6IHRoaXMuX3dlZWtkYXlzTWluUmVnZXg7XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIGNvbXB1dGVXZWVrZGF5c1BhcnNlICgpIHtcbiAgICBmdW5jdGlvbiBjbXBMZW5SZXYoYSwgYikge1xuICAgICAgICByZXR1cm4gYi5sZW5ndGggLSBhLmxlbmd0aDtcbiAgICB9XG5cbiAgICB2YXIgbWluUGllY2VzID0gW10sIHNob3J0UGllY2VzID0gW10sIGxvbmdQaWVjZXMgPSBbXSwgbWl4ZWRQaWVjZXMgPSBbXSxcbiAgICAgICAgaSwgbW9tLCBtaW5wLCBzaG9ydHAsIGxvbmdwO1xuICAgIGZvciAoaSA9IDA7IGkgPCA3OyBpKyspIHtcbiAgICAgICAgLy8gbWFrZSB0aGUgcmVnZXggaWYgd2UgZG9uJ3QgaGF2ZSBpdCBhbHJlYWR5XG4gICAgICAgIG1vbSA9IGNyZWF0ZVVUQyhbMjAwMCwgMV0pLmRheShpKTtcbiAgICAgICAgbWlucCA9IHRoaXMud2Vla2RheXNNaW4obW9tLCAnJyk7XG4gICAgICAgIHNob3J0cCA9IHRoaXMud2Vla2RheXNTaG9ydChtb20sICcnKTtcbiAgICAgICAgbG9uZ3AgPSB0aGlzLndlZWtkYXlzKG1vbSwgJycpO1xuICAgICAgICBtaW5QaWVjZXMucHVzaChtaW5wKTtcbiAgICAgICAgc2hvcnRQaWVjZXMucHVzaChzaG9ydHApO1xuICAgICAgICBsb25nUGllY2VzLnB1c2gobG9uZ3ApO1xuICAgICAgICBtaXhlZFBpZWNlcy5wdXNoKG1pbnApO1xuICAgICAgICBtaXhlZFBpZWNlcy5wdXNoKHNob3J0cCk7XG4gICAgICAgIG1peGVkUGllY2VzLnB1c2gobG9uZ3ApO1xuICAgIH1cbiAgICAvLyBTb3J0aW5nIG1ha2VzIHN1cmUgaWYgb25lIHdlZWtkYXkgKG9yIGFiYnIpIGlzIGEgcHJlZml4IG9mIGFub3RoZXIgaXRcbiAgICAvLyB3aWxsIG1hdGNoIHRoZSBsb25nZXIgcGllY2UuXG4gICAgbWluUGllY2VzLnNvcnQoY21wTGVuUmV2KTtcbiAgICBzaG9ydFBpZWNlcy5zb3J0KGNtcExlblJldik7XG4gICAgbG9uZ1BpZWNlcy5zb3J0KGNtcExlblJldik7XG4gICAgbWl4ZWRQaWVjZXMuc29ydChjbXBMZW5SZXYpO1xuICAgIGZvciAoaSA9IDA7IGkgPCA3OyBpKyspIHtcbiAgICAgICAgc2hvcnRQaWVjZXNbaV0gPSByZWdleEVzY2FwZShzaG9ydFBpZWNlc1tpXSk7XG4gICAgICAgIGxvbmdQaWVjZXNbaV0gPSByZWdleEVzY2FwZShsb25nUGllY2VzW2ldKTtcbiAgICAgICAgbWl4ZWRQaWVjZXNbaV0gPSByZWdleEVzY2FwZShtaXhlZFBpZWNlc1tpXSk7XG4gICAgfVxuXG4gICAgdGhpcy5fd2Vla2RheXNSZWdleCA9IG5ldyBSZWdFeHAoJ14oJyArIG1peGVkUGllY2VzLmpvaW4oJ3wnKSArICcpJywgJ2knKTtcbiAgICB0aGlzLl93ZWVrZGF5c1Nob3J0UmVnZXggPSB0aGlzLl93ZWVrZGF5c1JlZ2V4O1xuICAgIHRoaXMuX3dlZWtkYXlzTWluUmVnZXggPSB0aGlzLl93ZWVrZGF5c1JlZ2V4O1xuXG4gICAgdGhpcy5fd2Vla2RheXNTdHJpY3RSZWdleCA9IG5ldyBSZWdFeHAoJ14oJyArIGxvbmdQaWVjZXMuam9pbignfCcpICsgJyknLCAnaScpO1xuICAgIHRoaXMuX3dlZWtkYXlzU2hvcnRTdHJpY3RSZWdleCA9IG5ldyBSZWdFeHAoJ14oJyArIHNob3J0UGllY2VzLmpvaW4oJ3wnKSArICcpJywgJ2knKTtcbiAgICB0aGlzLl93ZWVrZGF5c01pblN0cmljdFJlZ2V4ID0gbmV3IFJlZ0V4cCgnXignICsgbWluUGllY2VzLmpvaW4oJ3wnKSArICcpJywgJ2knKTtcbn1cbiIsImltcG9ydCB7IG1ha2VHZXRTZXQgfSBmcm9tICcuLi9tb21lbnQvZ2V0LXNldCc7XG5pbXBvcnQgeyBhZGRGb3JtYXRUb2tlbiB9IGZyb20gJy4uL2Zvcm1hdC9mb3JtYXQnO1xuaW1wb3J0IHsgYWRkVW5pdEFsaWFzIH0gZnJvbSAnLi9hbGlhc2VzJztcbmltcG9ydCB7IGFkZFVuaXRQcmlvcml0eSB9IGZyb20gJy4vcHJpb3JpdGllcyc7XG5pbXBvcnQgeyBhZGRSZWdleFRva2VuLCBtYXRjaDF0bzIsIG1hdGNoMiwgbWF0Y2gzdG80LCBtYXRjaDV0bzYgfSBmcm9tICcuLi9wYXJzZS9yZWdleCc7XG5pbXBvcnQgeyBhZGRQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHsgSE9VUiwgTUlOVVRFLCBTRUNPTkQgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcbmltcG9ydCB6ZXJvRmlsbCBmcm9tICcuLi91dGlscy96ZXJvLWZpbGwnO1xuaW1wb3J0IGdldFBhcnNpbmdGbGFncyBmcm9tICcuLi9jcmVhdGUvcGFyc2luZy1mbGFncyc7XG5cbi8vIEZPUk1BVFRJTkdcblxuZnVuY3Rpb24gaEZvcm1hdCgpIHtcbiAgICByZXR1cm4gdGhpcy5ob3VycygpICUgMTIgfHwgMTI7XG59XG5cbmZ1bmN0aW9uIGtGb3JtYXQoKSB7XG4gICAgcmV0dXJuIHRoaXMuaG91cnMoKSB8fCAyNDtcbn1cblxuYWRkRm9ybWF0VG9rZW4oJ0gnLCBbJ0hIJywgMl0sIDAsICdob3VyJyk7XG5hZGRGb3JtYXRUb2tlbignaCcsIFsnaGgnLCAyXSwgMCwgaEZvcm1hdCk7XG5hZGRGb3JtYXRUb2tlbignaycsIFsna2snLCAyXSwgMCwga0Zvcm1hdCk7XG5cbmFkZEZvcm1hdFRva2VuKCdobW0nLCAwLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuICcnICsgaEZvcm1hdC5hcHBseSh0aGlzKSArIHplcm9GaWxsKHRoaXMubWludXRlcygpLCAyKTtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbignaG1tc3MnLCAwLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuICcnICsgaEZvcm1hdC5hcHBseSh0aGlzKSArIHplcm9GaWxsKHRoaXMubWludXRlcygpLCAyKSArXG4gICAgICAgIHplcm9GaWxsKHRoaXMuc2Vjb25kcygpLCAyKTtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbignSG1tJywgMCwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiAnJyArIHRoaXMuaG91cnMoKSArIHplcm9GaWxsKHRoaXMubWludXRlcygpLCAyKTtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbignSG1tc3MnLCAwLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuICcnICsgdGhpcy5ob3VycygpICsgemVyb0ZpbGwodGhpcy5taW51dGVzKCksIDIpICtcbiAgICAgICAgemVyb0ZpbGwodGhpcy5zZWNvbmRzKCksIDIpO1xufSk7XG5cbmZ1bmN0aW9uIG1lcmlkaWVtICh0b2tlbiwgbG93ZXJjYXNlKSB7XG4gICAgYWRkRm9ybWF0VG9rZW4odG9rZW4sIDAsIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YSgpLm1lcmlkaWVtKHRoaXMuaG91cnMoKSwgdGhpcy5taW51dGVzKCksIGxvd2VyY2FzZSk7XG4gICAgfSk7XG59XG5cbm1lcmlkaWVtKCdhJywgdHJ1ZSk7XG5tZXJpZGllbSgnQScsIGZhbHNlKTtcblxuLy8gQUxJQVNFU1xuXG5hZGRVbml0QWxpYXMoJ2hvdXInLCAnaCcpO1xuXG4vLyBQUklPUklUWVxuYWRkVW5pdFByaW9yaXR5KCdob3VyJywgMTMpO1xuXG4vLyBQQVJTSU5HXG5cbmZ1bmN0aW9uIG1hdGNoTWVyaWRpZW0gKGlzU3RyaWN0LCBsb2NhbGUpIHtcbiAgICByZXR1cm4gbG9jYWxlLl9tZXJpZGllbVBhcnNlO1xufVxuXG5hZGRSZWdleFRva2VuKCdhJywgIG1hdGNoTWVyaWRpZW0pO1xuYWRkUmVnZXhUb2tlbignQScsICBtYXRjaE1lcmlkaWVtKTtcbmFkZFJlZ2V4VG9rZW4oJ0gnLCAgbWF0Y2gxdG8yKTtcbmFkZFJlZ2V4VG9rZW4oJ2gnLCAgbWF0Y2gxdG8yKTtcbmFkZFJlZ2V4VG9rZW4oJ0hIJywgbWF0Y2gxdG8yLCBtYXRjaDIpO1xuYWRkUmVnZXhUb2tlbignaGgnLCBtYXRjaDF0bzIsIG1hdGNoMik7XG5cbmFkZFJlZ2V4VG9rZW4oJ2htbScsIG1hdGNoM3RvNCk7XG5hZGRSZWdleFRva2VuKCdobW1zcycsIG1hdGNoNXRvNik7XG5hZGRSZWdleFRva2VuKCdIbW0nLCBtYXRjaDN0bzQpO1xuYWRkUmVnZXhUb2tlbignSG1tc3MnLCBtYXRjaDV0bzYpO1xuXG5hZGRQYXJzZVRva2VuKFsnSCcsICdISCddLCBIT1VSKTtcbmFkZFBhcnNlVG9rZW4oWydhJywgJ0EnXSwgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnKSB7XG4gICAgY29uZmlnLl9pc1BtID0gY29uZmlnLl9sb2NhbGUuaXNQTShpbnB1dCk7XG4gICAgY29uZmlnLl9tZXJpZGllbSA9IGlucHV0O1xufSk7XG5hZGRQYXJzZVRva2VuKFsnaCcsICdoaCddLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICBhcnJheVtIT1VSXSA9IHRvSW50KGlucHV0KTtcbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5iaWdIb3VyID0gdHJ1ZTtcbn0pO1xuYWRkUGFyc2VUb2tlbignaG1tJywgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnKSB7XG4gICAgdmFyIHBvcyA9IGlucHV0Lmxlbmd0aCAtIDI7XG4gICAgYXJyYXlbSE9VUl0gPSB0b0ludChpbnB1dC5zdWJzdHIoMCwgcG9zKSk7XG4gICAgYXJyYXlbTUlOVVRFXSA9IHRvSW50KGlucHV0LnN1YnN0cihwb3MpKTtcbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5iaWdIb3VyID0gdHJ1ZTtcbn0pO1xuYWRkUGFyc2VUb2tlbignaG1tc3MnLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICB2YXIgcG9zMSA9IGlucHV0Lmxlbmd0aCAtIDQ7XG4gICAgdmFyIHBvczIgPSBpbnB1dC5sZW5ndGggLSAyO1xuICAgIGFycmF5W0hPVVJdID0gdG9JbnQoaW5wdXQuc3Vic3RyKDAsIHBvczEpKTtcbiAgICBhcnJheVtNSU5VVEVdID0gdG9JbnQoaW5wdXQuc3Vic3RyKHBvczEsIDIpKTtcbiAgICBhcnJheVtTRUNPTkRdID0gdG9JbnQoaW5wdXQuc3Vic3RyKHBvczIpKTtcbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5iaWdIb3VyID0gdHJ1ZTtcbn0pO1xuYWRkUGFyc2VUb2tlbignSG1tJywgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnKSB7XG4gICAgdmFyIHBvcyA9IGlucHV0Lmxlbmd0aCAtIDI7XG4gICAgYXJyYXlbSE9VUl0gPSB0b0ludChpbnB1dC5zdWJzdHIoMCwgcG9zKSk7XG4gICAgYXJyYXlbTUlOVVRFXSA9IHRvSW50KGlucHV0LnN1YnN0cihwb3MpKTtcbn0pO1xuYWRkUGFyc2VUb2tlbignSG1tc3MnLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICB2YXIgcG9zMSA9IGlucHV0Lmxlbmd0aCAtIDQ7XG4gICAgdmFyIHBvczIgPSBpbnB1dC5sZW5ndGggLSAyO1xuICAgIGFycmF5W0hPVVJdID0gdG9JbnQoaW5wdXQuc3Vic3RyKDAsIHBvczEpKTtcbiAgICBhcnJheVtNSU5VVEVdID0gdG9JbnQoaW5wdXQuc3Vic3RyKHBvczEsIDIpKTtcbiAgICBhcnJheVtTRUNPTkRdID0gdG9JbnQoaW5wdXQuc3Vic3RyKHBvczIpKTtcbn0pO1xuXG4vLyBMT0NBTEVTXG5cbmV4cG9ydCBmdW5jdGlvbiBsb2NhbGVJc1BNIChpbnB1dCkge1xuICAgIC8vIElFOCBRdWlya3MgTW9kZSAmIElFNyBTdGFuZGFyZHMgTW9kZSBkbyBub3QgYWxsb3cgYWNjZXNzaW5nIHN0cmluZ3MgbGlrZSBhcnJheXNcbiAgICAvLyBVc2luZyBjaGFyQXQgc2hvdWxkIGJlIG1vcmUgY29tcGF0aWJsZS5cbiAgICByZXR1cm4gKChpbnB1dCArICcnKS50b0xvd2VyQ2FzZSgpLmNoYXJBdCgwKSA9PT0gJ3AnKTtcbn1cblxuZXhwb3J0IHZhciBkZWZhdWx0TG9jYWxlTWVyaWRpZW1QYXJzZSA9IC9bYXBdXFwuP20/XFwuPy9pO1xuZXhwb3J0IGZ1bmN0aW9uIGxvY2FsZU1lcmlkaWVtIChob3VycywgbWludXRlcywgaXNMb3dlcikge1xuICAgIGlmIChob3VycyA+IDExKSB7XG4gICAgICAgIHJldHVybiBpc0xvd2VyID8gJ3BtJyA6ICdQTSc7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGlzTG93ZXIgPyAnYW0nIDogJ0FNJztcbiAgICB9XG59XG5cblxuLy8gTU9NRU5UU1xuXG4vLyBTZXR0aW5nIHRoZSBob3VyIHNob3VsZCBrZWVwIHRoZSB0aW1lLCBiZWNhdXNlIHRoZSB1c2VyIGV4cGxpY2l0bHlcbi8vIHNwZWNpZmllZCB3aGljaCBob3VyIGhlIHdhbnRzLiBTbyB0cnlpbmcgdG8gbWFpbnRhaW4gdGhlIHNhbWUgaG91ciAoaW5cbi8vIGEgbmV3IHRpbWV6b25lKSBtYWtlcyBzZW5zZS4gQWRkaW5nL3N1YnRyYWN0aW5nIGhvdXJzIGRvZXMgbm90IGZvbGxvd1xuLy8gdGhpcyBydWxlLlxuZXhwb3J0IHZhciBnZXRTZXRIb3VyID0gbWFrZUdldFNldCgnSG91cnMnLCB0cnVlKTtcbiIsImltcG9ydCB7IGRlZmF1bHRDYWxlbmRhciB9IGZyb20gJy4vY2FsZW5kYXInO1xuaW1wb3J0IHsgZGVmYXVsdExvbmdEYXRlRm9ybWF0IH0gZnJvbSAnLi9mb3JtYXRzJztcbmltcG9ydCB7IGRlZmF1bHRJbnZhbGlkRGF0ZSB9IGZyb20gJy4vaW52YWxpZCc7XG5pbXBvcnQgeyBkZWZhdWx0T3JkaW5hbCwgZGVmYXVsdE9yZGluYWxQYXJzZSB9IGZyb20gJy4vb3JkaW5hbCc7XG5pbXBvcnQgeyBkZWZhdWx0UmVsYXRpdmVUaW1lIH0gZnJvbSAnLi9yZWxhdGl2ZSc7XG5cbi8vIG1vbnRoc1xuaW1wb3J0IHtcbiAgICBkZWZhdWx0TG9jYWxlTW9udGhzLFxuICAgIGRlZmF1bHRMb2NhbGVNb250aHNTaG9ydCxcbn0gZnJvbSAnLi4vdW5pdHMvbW9udGgnO1xuXG4vLyB3ZWVrXG5pbXBvcnQgeyBkZWZhdWx0TG9jYWxlV2VlayB9IGZyb20gJy4uL3VuaXRzL3dlZWsnO1xuXG4vLyB3ZWVrZGF5c1xuaW1wb3J0IHtcbiAgICBkZWZhdWx0TG9jYWxlV2Vla2RheXMsXG4gICAgZGVmYXVsdExvY2FsZVdlZWtkYXlzTWluLFxuICAgIGRlZmF1bHRMb2NhbGVXZWVrZGF5c1Nob3J0LFxufSBmcm9tICcuLi91bml0cy9kYXktb2Ytd2Vlayc7XG5cbi8vIG1lcmlkaWVtXG5pbXBvcnQgeyBkZWZhdWx0TG9jYWxlTWVyaWRpZW1QYXJzZSB9IGZyb20gJy4uL3VuaXRzL2hvdXInO1xuXG5leHBvcnQgdmFyIGJhc2VDb25maWcgPSB7XG4gICAgY2FsZW5kYXI6IGRlZmF1bHRDYWxlbmRhcixcbiAgICBsb25nRGF0ZUZvcm1hdDogZGVmYXVsdExvbmdEYXRlRm9ybWF0LFxuICAgIGludmFsaWREYXRlOiBkZWZhdWx0SW52YWxpZERhdGUsXG4gICAgb3JkaW5hbDogZGVmYXVsdE9yZGluYWwsXG4gICAgb3JkaW5hbFBhcnNlOiBkZWZhdWx0T3JkaW5hbFBhcnNlLFxuICAgIHJlbGF0aXZlVGltZTogZGVmYXVsdFJlbGF0aXZlVGltZSxcblxuICAgIG1vbnRoczogZGVmYXVsdExvY2FsZU1vbnRocyxcbiAgICBtb250aHNTaG9ydDogZGVmYXVsdExvY2FsZU1vbnRoc1Nob3J0LFxuXG4gICAgd2VlazogZGVmYXVsdExvY2FsZVdlZWssXG5cbiAgICB3ZWVrZGF5czogZGVmYXVsdExvY2FsZVdlZWtkYXlzLFxuICAgIHdlZWtkYXlzTWluOiBkZWZhdWx0TG9jYWxlV2Vla2RheXNNaW4sXG4gICAgd2Vla2RheXNTaG9ydDogZGVmYXVsdExvY2FsZVdlZWtkYXlzU2hvcnQsXG5cbiAgICBtZXJpZGllbVBhcnNlOiBkZWZhdWx0TG9jYWxlTWVyaWRpZW1QYXJzZVxufTtcbiIsImltcG9ydCBpc0FycmF5IGZyb20gJy4uL3V0aWxzL2lzLWFycmF5JztcbmltcG9ydCBoYXNPd25Qcm9wIGZyb20gJy4uL3V0aWxzL2hhcy1vd24tcHJvcCc7XG5pbXBvcnQgaXNVbmRlZmluZWQgZnJvbSAnLi4vdXRpbHMvaXMtdW5kZWZpbmVkJztcbmltcG9ydCBjb21wYXJlQXJyYXlzIGZyb20gJy4uL3V0aWxzL2NvbXBhcmUtYXJyYXlzJztcbmltcG9ydCB7IGRlcHJlY2F0ZVNpbXBsZSB9IGZyb20gJy4uL3V0aWxzL2RlcHJlY2F0ZSc7XG5pbXBvcnQgeyBtZXJnZUNvbmZpZ3MgfSBmcm9tICcuL3NldCc7XG5pbXBvcnQgeyBMb2NhbGUgfSBmcm9tICcuL2NvbnN0cnVjdG9yJztcbmltcG9ydCBrZXlzIGZyb20gJy4uL3V0aWxzL2tleXMnO1xuXG5pbXBvcnQgeyBiYXNlQ29uZmlnIH0gZnJvbSAnLi9iYXNlLWNvbmZpZyc7XG5cbi8vIGludGVybmFsIHN0b3JhZ2UgZm9yIGxvY2FsZSBjb25maWcgZmlsZXNcbnZhciBsb2NhbGVzID0ge307XG52YXIgbG9jYWxlRmFtaWxpZXMgPSB7fTtcbnZhciBnbG9iYWxMb2NhbGU7XG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUxvY2FsZShrZXkpIHtcbiAgICByZXR1cm4ga2V5ID8ga2V5LnRvTG93ZXJDYXNlKCkucmVwbGFjZSgnXycsICctJykgOiBrZXk7XG59XG5cbi8vIHBpY2sgdGhlIGxvY2FsZSBmcm9tIHRoZSBhcnJheVxuLy8gdHJ5IFsnZW4tYXUnLCAnZW4tZ2InXSBhcyAnZW4tYXUnLCAnZW4tZ2InLCAnZW4nLCBhcyBpbiBtb3ZlIHRocm91Z2ggdGhlIGxpc3QgdHJ5aW5nIGVhY2hcbi8vIHN1YnN0cmluZyBmcm9tIG1vc3Qgc3BlY2lmaWMgdG8gbGVhc3QsIGJ1dCBtb3ZlIHRvIHRoZSBuZXh0IGFycmF5IGl0ZW0gaWYgaXQncyBhIG1vcmUgc3BlY2lmaWMgdmFyaWFudCB0aGFuIHRoZSBjdXJyZW50IHJvb3RcbmZ1bmN0aW9uIGNob29zZUxvY2FsZShuYW1lcykge1xuICAgIHZhciBpID0gMCwgaiwgbmV4dCwgbG9jYWxlLCBzcGxpdDtcblxuICAgIHdoaWxlIChpIDwgbmFtZXMubGVuZ3RoKSB7XG4gICAgICAgIHNwbGl0ID0gbm9ybWFsaXplTG9jYWxlKG5hbWVzW2ldKS5zcGxpdCgnLScpO1xuICAgICAgICBqID0gc3BsaXQubGVuZ3RoO1xuICAgICAgICBuZXh0ID0gbm9ybWFsaXplTG9jYWxlKG5hbWVzW2kgKyAxXSk7XG4gICAgICAgIG5leHQgPSBuZXh0ID8gbmV4dC5zcGxpdCgnLScpIDogbnVsbDtcbiAgICAgICAgd2hpbGUgKGogPiAwKSB7XG4gICAgICAgICAgICBsb2NhbGUgPSBsb2FkTG9jYWxlKHNwbGl0LnNsaWNlKDAsIGopLmpvaW4oJy0nKSk7XG4gICAgICAgICAgICBpZiAobG9jYWxlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGxvY2FsZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChuZXh0ICYmIG5leHQubGVuZ3RoID49IGogJiYgY29tcGFyZUFycmF5cyhzcGxpdCwgbmV4dCwgdHJ1ZSkgPj0gaiAtIDEpIHtcbiAgICAgICAgICAgICAgICAvL3RoZSBuZXh0IGFycmF5IGl0ZW0gaXMgYmV0dGVyIHRoYW4gYSBzaGFsbG93ZXIgc3Vic3RyaW5nIG9mIHRoaXMgb25lXG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBqLS07XG4gICAgICAgIH1cbiAgICAgICAgaSsrO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbn1cblxuZnVuY3Rpb24gbG9hZExvY2FsZShuYW1lKSB7XG4gICAgdmFyIG9sZExvY2FsZSA9IG51bGw7XG4gICAgLy8gVE9ETzogRmluZCBhIGJldHRlciB3YXkgdG8gcmVnaXN0ZXIgYW5kIGxvYWQgYWxsIHRoZSBsb2NhbGVzIGluIE5vZGVcbiAgICBpZiAoIWxvY2FsZXNbbmFtZV0gJiYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnKSAmJlxuICAgICAgICAgICAgbW9kdWxlICYmIG1vZHVsZS5leHBvcnRzKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBvbGRMb2NhbGUgPSBnbG9iYWxMb2NhbGUuX2FiYnI7XG4gICAgICAgICAgICByZXF1aXJlKCcuL2xvY2FsZS8nICsgbmFtZSk7XG4gICAgICAgICAgICAvLyBiZWNhdXNlIGRlZmluZUxvY2FsZSBjdXJyZW50bHkgYWxzbyBzZXRzIHRoZSBnbG9iYWwgbG9jYWxlLCB3ZVxuICAgICAgICAgICAgLy8gd2FudCB0byB1bmRvIHRoYXQgZm9yIGxhenkgbG9hZGVkIGxvY2FsZXNcbiAgICAgICAgICAgIGdldFNldEdsb2JhbExvY2FsZShvbGRMb2NhbGUpO1xuICAgICAgICB9IGNhdGNoIChlKSB7IH1cbiAgICB9XG4gICAgcmV0dXJuIGxvY2FsZXNbbmFtZV07XG59XG5cbi8vIFRoaXMgZnVuY3Rpb24gd2lsbCBsb2FkIGxvY2FsZSBhbmQgdGhlbiBzZXQgdGhlIGdsb2JhbCBsb2NhbGUuICBJZlxuLy8gbm8gYXJndW1lbnRzIGFyZSBwYXNzZWQgaW4sIGl0IHdpbGwgc2ltcGx5IHJldHVybiB0aGUgY3VycmVudCBnbG9iYWxcbi8vIGxvY2FsZSBrZXkuXG5leHBvcnQgZnVuY3Rpb24gZ2V0U2V0R2xvYmFsTG9jYWxlIChrZXksIHZhbHVlcykge1xuICAgIHZhciBkYXRhO1xuICAgIGlmIChrZXkpIHtcbiAgICAgICAgaWYgKGlzVW5kZWZpbmVkKHZhbHVlcykpIHtcbiAgICAgICAgICAgIGRhdGEgPSBnZXRMb2NhbGUoa2V5KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGRhdGEgPSBkZWZpbmVMb2NhbGUoa2V5LCB2YWx1ZXMpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGRhdGEpIHtcbiAgICAgICAgICAgIC8vIG1vbWVudC5kdXJhdGlvbi5fbG9jYWxlID0gbW9tZW50Ll9sb2NhbGUgPSBkYXRhO1xuICAgICAgICAgICAgZ2xvYmFsTG9jYWxlID0gZGF0YTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBnbG9iYWxMb2NhbGUuX2FiYnI7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBkZWZpbmVMb2NhbGUgKG5hbWUsIGNvbmZpZykge1xuICAgIGlmIChjb25maWcgIT09IG51bGwpIHtcbiAgICAgICAgdmFyIHBhcmVudENvbmZpZyA9IGJhc2VDb25maWc7XG4gICAgICAgIGNvbmZpZy5hYmJyID0gbmFtZTtcbiAgICAgICAgaWYgKGxvY2FsZXNbbmFtZV0gIT0gbnVsbCkge1xuICAgICAgICAgICAgZGVwcmVjYXRlU2ltcGxlKCdkZWZpbmVMb2NhbGVPdmVycmlkZScsXG4gICAgICAgICAgICAgICAgICAgICd1c2UgbW9tZW50LnVwZGF0ZUxvY2FsZShsb2NhbGVOYW1lLCBjb25maWcpIHRvIGNoYW5nZSAnICtcbiAgICAgICAgICAgICAgICAgICAgJ2FuIGV4aXN0aW5nIGxvY2FsZS4gbW9tZW50LmRlZmluZUxvY2FsZShsb2NhbGVOYW1lLCAnICtcbiAgICAgICAgICAgICAgICAgICAgJ2NvbmZpZykgc2hvdWxkIG9ubHkgYmUgdXNlZCBmb3IgY3JlYXRpbmcgYSBuZXcgbG9jYWxlICcgK1xuICAgICAgICAgICAgICAgICAgICAnU2VlIGh0dHA6Ly9tb21lbnRqcy5jb20vZ3VpZGVzLyMvd2FybmluZ3MvZGVmaW5lLWxvY2FsZS8gZm9yIG1vcmUgaW5mby4nKTtcbiAgICAgICAgICAgIHBhcmVudENvbmZpZyA9IGxvY2FsZXNbbmFtZV0uX2NvbmZpZztcbiAgICAgICAgfSBlbHNlIGlmIChjb25maWcucGFyZW50TG9jYWxlICE9IG51bGwpIHtcbiAgICAgICAgICAgIGlmIChsb2NhbGVzW2NvbmZpZy5wYXJlbnRMb2NhbGVdICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICBwYXJlbnRDb25maWcgPSBsb2NhbGVzW2NvbmZpZy5wYXJlbnRMb2NhbGVdLl9jb25maWc7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmICghbG9jYWxlRmFtaWxpZXNbY29uZmlnLnBhcmVudExvY2FsZV0pIHtcbiAgICAgICAgICAgICAgICAgICAgbG9jYWxlRmFtaWxpZXNbY29uZmlnLnBhcmVudExvY2FsZV0gPSBbXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbG9jYWxlRmFtaWxpZXNbY29uZmlnLnBhcmVudExvY2FsZV0ucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUsXG4gICAgICAgICAgICAgICAgICAgIGNvbmZpZzogY29uZmlnXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgbG9jYWxlc1tuYW1lXSA9IG5ldyBMb2NhbGUobWVyZ2VDb25maWdzKHBhcmVudENvbmZpZywgY29uZmlnKSk7XG5cbiAgICAgICAgaWYgKGxvY2FsZUZhbWlsaWVzW25hbWVdKSB7XG4gICAgICAgICAgICBsb2NhbGVGYW1pbGllc1tuYW1lXS5mb3JFYWNoKGZ1bmN0aW9uICh4KSB7XG4gICAgICAgICAgICAgICAgZGVmaW5lTG9jYWxlKHgubmFtZSwgeC5jb25maWcpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBiYWNrd2FyZHMgY29tcGF0IGZvciBub3c6IGFsc28gc2V0IHRoZSBsb2NhbGVcbiAgICAgICAgLy8gbWFrZSBzdXJlIHdlIHNldCB0aGUgbG9jYWxlIEFGVEVSIGFsbCBjaGlsZCBsb2NhbGVzIGhhdmUgYmVlblxuICAgICAgICAvLyBjcmVhdGVkLCBzbyB3ZSB3b24ndCBlbmQgdXAgd2l0aCB0aGUgY2hpbGQgbG9jYWxlIHNldC5cbiAgICAgICAgZ2V0U2V0R2xvYmFsTG9jYWxlKG5hbWUpO1xuXG5cbiAgICAgICAgcmV0dXJuIGxvY2FsZXNbbmFtZV07XG4gICAgfSBlbHNlIHtcbiAgICAgICAgLy8gdXNlZnVsIGZvciB0ZXN0aW5nXG4gICAgICAgIGRlbGV0ZSBsb2NhbGVzW25hbWVdO1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB1cGRhdGVMb2NhbGUobmFtZSwgY29uZmlnKSB7XG4gICAgaWYgKGNvbmZpZyAhPSBudWxsKSB7XG4gICAgICAgIHZhciBsb2NhbGUsIHBhcmVudENvbmZpZyA9IGJhc2VDb25maWc7XG4gICAgICAgIC8vIE1FUkdFXG4gICAgICAgIGlmIChsb2NhbGVzW25hbWVdICE9IG51bGwpIHtcbiAgICAgICAgICAgIHBhcmVudENvbmZpZyA9IGxvY2FsZXNbbmFtZV0uX2NvbmZpZztcbiAgICAgICAgfVxuICAgICAgICBjb25maWcgPSBtZXJnZUNvbmZpZ3MocGFyZW50Q29uZmlnLCBjb25maWcpO1xuICAgICAgICBsb2NhbGUgPSBuZXcgTG9jYWxlKGNvbmZpZyk7XG4gICAgICAgIGxvY2FsZS5wYXJlbnRMb2NhbGUgPSBsb2NhbGVzW25hbWVdO1xuICAgICAgICBsb2NhbGVzW25hbWVdID0gbG9jYWxlO1xuXG4gICAgICAgIC8vIGJhY2t3YXJkcyBjb21wYXQgZm9yIG5vdzogYWxzbyBzZXQgdGhlIGxvY2FsZVxuICAgICAgICBnZXRTZXRHbG9iYWxMb2NhbGUobmFtZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgLy8gcGFzcyBudWxsIGZvciBjb25maWcgdG8gdW51cGRhdGUsIHVzZWZ1bCBmb3IgdGVzdHNcbiAgICAgICAgaWYgKGxvY2FsZXNbbmFtZV0gIT0gbnVsbCkge1xuICAgICAgICAgICAgaWYgKGxvY2FsZXNbbmFtZV0ucGFyZW50TG9jYWxlICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICBsb2NhbGVzW25hbWVdID0gbG9jYWxlc1tuYW1lXS5wYXJlbnRMb2NhbGU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGxvY2FsZXNbbmFtZV0gIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGRlbGV0ZSBsb2NhbGVzW25hbWVdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiBsb2NhbGVzW25hbWVdO1xufVxuXG4vLyByZXR1cm5zIGxvY2FsZSBkYXRhXG5leHBvcnQgZnVuY3Rpb24gZ2V0TG9jYWxlIChrZXkpIHtcbiAgICB2YXIgbG9jYWxlO1xuXG4gICAgaWYgKGtleSAmJiBrZXkuX2xvY2FsZSAmJiBrZXkuX2xvY2FsZS5fYWJicikge1xuICAgICAgICBrZXkgPSBrZXkuX2xvY2FsZS5fYWJicjtcbiAgICB9XG5cbiAgICBpZiAoIWtleSkge1xuICAgICAgICByZXR1cm4gZ2xvYmFsTG9jYWxlO1xuICAgIH1cblxuICAgIGlmICghaXNBcnJheShrZXkpKSB7XG4gICAgICAgIC8vc2hvcnQtY2lyY3VpdCBldmVyeXRoaW5nIGVsc2VcbiAgICAgICAgbG9jYWxlID0gbG9hZExvY2FsZShrZXkpO1xuICAgICAgICBpZiAobG9jYWxlKSB7XG4gICAgICAgICAgICByZXR1cm4gbG9jYWxlO1xuICAgICAgICB9XG4gICAgICAgIGtleSA9IFtrZXldO1xuICAgIH1cblxuICAgIHJldHVybiBjaG9vc2VMb2NhbGUoa2V5KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxpc3RMb2NhbGVzKCkge1xuICAgIHJldHVybiBrZXlzKGxvY2FsZXMpO1xufVxuIiwiaW1wb3J0IHsgZGF5c0luTW9udGggfSBmcm9tICcuLi91bml0cy9tb250aCc7XG5pbXBvcnQgeyBZRUFSLCBNT05USCwgREFURSwgSE9VUiwgTUlOVVRFLCBTRUNPTkQsIE1JTExJU0VDT05ELCBXRUVLLCBXRUVLREFZIH0gZnJvbSAnLi4vdW5pdHMvY29uc3RhbnRzJztcbmltcG9ydCBnZXRQYXJzaW5nRmxhZ3MgZnJvbSAnLi4vY3JlYXRlL3BhcnNpbmctZmxhZ3MnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBjaGVja092ZXJmbG93IChtKSB7XG4gICAgdmFyIG92ZXJmbG93O1xuICAgIHZhciBhID0gbS5fYTtcblxuICAgIGlmIChhICYmIGdldFBhcnNpbmdGbGFncyhtKS5vdmVyZmxvdyA9PT0gLTIpIHtcbiAgICAgICAgb3ZlcmZsb3cgPVxuICAgICAgICAgICAgYVtNT05USF0gICAgICAgPCAwIHx8IGFbTU9OVEhdICAgICAgID4gMTEgID8gTU9OVEggOlxuICAgICAgICAgICAgYVtEQVRFXSAgICAgICAgPCAxIHx8IGFbREFURV0gICAgICAgID4gZGF5c0luTW9udGgoYVtZRUFSXSwgYVtNT05USF0pID8gREFURSA6XG4gICAgICAgICAgICBhW0hPVVJdICAgICAgICA8IDAgfHwgYVtIT1VSXSAgICAgICAgPiAyNCB8fCAoYVtIT1VSXSA9PT0gMjQgJiYgKGFbTUlOVVRFXSAhPT0gMCB8fCBhW1NFQ09ORF0gIT09IDAgfHwgYVtNSUxMSVNFQ09ORF0gIT09IDApKSA/IEhPVVIgOlxuICAgICAgICAgICAgYVtNSU5VVEVdICAgICAgPCAwIHx8IGFbTUlOVVRFXSAgICAgID4gNTkgID8gTUlOVVRFIDpcbiAgICAgICAgICAgIGFbU0VDT05EXSAgICAgIDwgMCB8fCBhW1NFQ09ORF0gICAgICA+IDU5ICA/IFNFQ09ORCA6XG4gICAgICAgICAgICBhW01JTExJU0VDT05EXSA8IDAgfHwgYVtNSUxMSVNFQ09ORF0gPiA5OTkgPyBNSUxMSVNFQ09ORCA6XG4gICAgICAgICAgICAtMTtcblxuICAgICAgICBpZiAoZ2V0UGFyc2luZ0ZsYWdzKG0pLl9vdmVyZmxvd0RheU9mWWVhciAmJiAob3ZlcmZsb3cgPCBZRUFSIHx8IG92ZXJmbG93ID4gREFURSkpIHtcbiAgICAgICAgICAgIG92ZXJmbG93ID0gREFURTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZ2V0UGFyc2luZ0ZsYWdzKG0pLl9vdmVyZmxvd1dlZWtzICYmIG92ZXJmbG93ID09PSAtMSkge1xuICAgICAgICAgICAgb3ZlcmZsb3cgPSBXRUVLO1xuICAgICAgICB9XG4gICAgICAgIGlmIChnZXRQYXJzaW5nRmxhZ3MobSkuX292ZXJmbG93V2Vla2RheSAmJiBvdmVyZmxvdyA9PT0gLTEpIHtcbiAgICAgICAgICAgIG92ZXJmbG93ID0gV0VFS0RBWTtcbiAgICAgICAgfVxuXG4gICAgICAgIGdldFBhcnNpbmdGbGFncyhtKS5vdmVyZmxvdyA9IG92ZXJmbG93O1xuICAgIH1cblxuICAgIHJldHVybiBtO1xufVxuXG4iLCJpbXBvcnQgeyBjb25maWdGcm9tU3RyaW5nQW5kRm9ybWF0IH0gZnJvbSAnLi9mcm9tLXN0cmluZy1hbmQtZm9ybWF0JztcbmltcG9ydCB7IGhvb2tzIH0gZnJvbSAnLi4vdXRpbHMvaG9va3MnO1xuaW1wb3J0IHsgZGVwcmVjYXRlIH0gZnJvbSAnLi4vdXRpbHMvZGVwcmVjYXRlJztcbmltcG9ydCBnZXRQYXJzaW5nRmxhZ3MgZnJvbSAnLi9wYXJzaW5nLWZsYWdzJztcblxuLy8gaXNvIDg2MDEgcmVnZXhcbi8vIDAwMDAtMDAtMDAgMDAwMC1XMDAgb3IgMDAwMC1XMDAtMCArIFQgKyAwMCBvciAwMDowMCBvciAwMDowMDowMCBvciAwMDowMDowMC4wMDAgKyArMDA6MDAgb3IgKzAwMDAgb3IgKzAwKVxudmFyIGV4dGVuZGVkSXNvUmVnZXggPSAvXlxccyooKD86WystXVxcZHs2fXxcXGR7NH0pLSg/OlxcZFxcZC1cXGRcXGR8V1xcZFxcZC1cXGR8V1xcZFxcZHxcXGRcXGRcXGR8XFxkXFxkKSkoPzooVHwgKShcXGRcXGQoPzo6XFxkXFxkKD86OlxcZFxcZCg/OlsuLF1cXGQrKT8pPyk/KShbXFwrXFwtXVxcZFxcZCg/Ojo/XFxkXFxkKT98XFxzKlopPyk/JC87XG52YXIgYmFzaWNJc29SZWdleCA9IC9eXFxzKigoPzpbKy1dXFxkezZ9fFxcZHs0fSkoPzpcXGRcXGRcXGRcXGR8V1xcZFxcZFxcZHxXXFxkXFxkfFxcZFxcZFxcZHxcXGRcXGQpKSg/OihUfCApKFxcZFxcZCg/OlxcZFxcZCg/OlxcZFxcZCg/OlsuLF1cXGQrKT8pPyk/KShbXFwrXFwtXVxcZFxcZCg/Ojo/XFxkXFxkKT98XFxzKlopPyk/JC87XG5cbnZhciB0elJlZ2V4ID0gL1p8WystXVxcZFxcZCg/Ojo/XFxkXFxkKT8vO1xuXG52YXIgaXNvRGF0ZXMgPSBbXG4gICAgWydZWVlZWVktTU0tREQnLCAvWystXVxcZHs2fS1cXGRcXGQtXFxkXFxkL10sXG4gICAgWydZWVlZLU1NLUREJywgL1xcZHs0fS1cXGRcXGQtXFxkXFxkL10sXG4gICAgWydHR0dHLVtXXVdXLUUnLCAvXFxkezR9LVdcXGRcXGQtXFxkL10sXG4gICAgWydHR0dHLVtXXVdXJywgL1xcZHs0fS1XXFxkXFxkLywgZmFsc2VdLFxuICAgIFsnWVlZWS1EREQnLCAvXFxkezR9LVxcZHszfS9dLFxuICAgIFsnWVlZWS1NTScsIC9cXGR7NH0tXFxkXFxkLywgZmFsc2VdLFxuICAgIFsnWVlZWVlZTU1ERCcsIC9bKy1dXFxkezEwfS9dLFxuICAgIFsnWVlZWU1NREQnLCAvXFxkezh9L10sXG4gICAgLy8gWVlZWU1NIGlzIE5PVCBhbGxvd2VkIGJ5IHRoZSBzdGFuZGFyZFxuICAgIFsnR0dHR1tXXVdXRScsIC9cXGR7NH1XXFxkezN9L10sXG4gICAgWydHR0dHW1ddV1cnLCAvXFxkezR9V1xcZHsyfS8sIGZhbHNlXSxcbiAgICBbJ1lZWVlEREQnLCAvXFxkezd9L11cbl07XG5cbi8vIGlzbyB0aW1lIGZvcm1hdHMgYW5kIHJlZ2V4ZXNcbnZhciBpc29UaW1lcyA9IFtcbiAgICBbJ0hIOm1tOnNzLlNTU1MnLCAvXFxkXFxkOlxcZFxcZDpcXGRcXGRcXC5cXGQrL10sXG4gICAgWydISDptbTpzcyxTU1NTJywgL1xcZFxcZDpcXGRcXGQ6XFxkXFxkLFxcZCsvXSxcbiAgICBbJ0hIOm1tOnNzJywgL1xcZFxcZDpcXGRcXGQ6XFxkXFxkL10sXG4gICAgWydISDptbScsIC9cXGRcXGQ6XFxkXFxkL10sXG4gICAgWydISG1tc3MuU1NTUycsIC9cXGRcXGRcXGRcXGRcXGRcXGRcXC5cXGQrL10sXG4gICAgWydISG1tc3MsU1NTUycsIC9cXGRcXGRcXGRcXGRcXGRcXGQsXFxkKy9dLFxuICAgIFsnSEhtbXNzJywgL1xcZFxcZFxcZFxcZFxcZFxcZC9dLFxuICAgIFsnSEhtbScsIC9cXGRcXGRcXGRcXGQvXSxcbiAgICBbJ0hIJywgL1xcZFxcZC9dXG5dO1xuXG52YXIgYXNwTmV0SnNvblJlZ2V4ID0gL15cXC8/RGF0ZVxcKChcXC0/XFxkKykvaTtcblxuLy8gZGF0ZSBmcm9tIGlzbyBmb3JtYXRcbmV4cG9ydCBmdW5jdGlvbiBjb25maWdGcm9tSVNPKGNvbmZpZykge1xuICAgIHZhciBpLCBsLFxuICAgICAgICBzdHJpbmcgPSBjb25maWcuX2ksXG4gICAgICAgIG1hdGNoID0gZXh0ZW5kZWRJc29SZWdleC5leGVjKHN0cmluZykgfHwgYmFzaWNJc29SZWdleC5leGVjKHN0cmluZyksXG4gICAgICAgIGFsbG93VGltZSwgZGF0ZUZvcm1hdCwgdGltZUZvcm1hdCwgdHpGb3JtYXQ7XG5cbiAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgZ2V0UGFyc2luZ0ZsYWdzKGNvbmZpZykuaXNvID0gdHJ1ZTtcblxuICAgICAgICBmb3IgKGkgPSAwLCBsID0gaXNvRGF0ZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoaXNvRGF0ZXNbaV1bMV0uZXhlYyhtYXRjaFsxXSkpIHtcbiAgICAgICAgICAgICAgICBkYXRlRm9ybWF0ID0gaXNvRGF0ZXNbaV1bMF07XG4gICAgICAgICAgICAgICAgYWxsb3dUaW1lID0gaXNvRGF0ZXNbaV1bMl0gIT09IGZhbHNlO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmIChkYXRlRm9ybWF0ID09IG51bGwpIHtcbiAgICAgICAgICAgIGNvbmZpZy5faXNWYWxpZCA9IGZhbHNlO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtYXRjaFszXSkge1xuICAgICAgICAgICAgZm9yIChpID0gMCwgbCA9IGlzb1RpbWVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmIChpc29UaW1lc1tpXVsxXS5leGVjKG1hdGNoWzNdKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBtYXRjaFsyXSBzaG91bGQgYmUgJ1QnIG9yIHNwYWNlXG4gICAgICAgICAgICAgICAgICAgIHRpbWVGb3JtYXQgPSAobWF0Y2hbMl0gfHwgJyAnKSArIGlzb1RpbWVzW2ldWzBdO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGltZUZvcm1hdCA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgY29uZmlnLl9pc1ZhbGlkID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICghYWxsb3dUaW1lICYmIHRpbWVGb3JtYXQgIT0gbnVsbCkge1xuICAgICAgICAgICAgY29uZmlnLl9pc1ZhbGlkID0gZmFsc2U7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1hdGNoWzRdKSB7XG4gICAgICAgICAgICBpZiAodHpSZWdleC5leGVjKG1hdGNoWzRdKSkge1xuICAgICAgICAgICAgICAgIHR6Rm9ybWF0ID0gJ1onO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25maWcuX2lzVmFsaWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY29uZmlnLl9mID0gZGF0ZUZvcm1hdCArICh0aW1lRm9ybWF0IHx8ICcnKSArICh0ekZvcm1hdCB8fCAnJyk7XG4gICAgICAgIGNvbmZpZ0Zyb21TdHJpbmdBbmRGb3JtYXQoY29uZmlnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBjb25maWcuX2lzVmFsaWQgPSBmYWxzZTtcbiAgICB9XG59XG5cbi8vIGRhdGUgZnJvbSBpc28gZm9ybWF0IG9yIGZhbGxiYWNrXG5leHBvcnQgZnVuY3Rpb24gY29uZmlnRnJvbVN0cmluZyhjb25maWcpIHtcbiAgICB2YXIgbWF0Y2hlZCA9IGFzcE5ldEpzb25SZWdleC5leGVjKGNvbmZpZy5faSk7XG5cbiAgICBpZiAobWF0Y2hlZCAhPT0gbnVsbCkge1xuICAgICAgICBjb25maWcuX2QgPSBuZXcgRGF0ZSgrbWF0Y2hlZFsxXSk7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25maWdGcm9tSVNPKGNvbmZpZyk7XG4gICAgaWYgKGNvbmZpZy5faXNWYWxpZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgZGVsZXRlIGNvbmZpZy5faXNWYWxpZDtcbiAgICAgICAgaG9va3MuY3JlYXRlRnJvbUlucHV0RmFsbGJhY2soY29uZmlnKTtcbiAgICB9XG59XG5cbmhvb2tzLmNyZWF0ZUZyb21JbnB1dEZhbGxiYWNrID0gZGVwcmVjYXRlKFxuICAgICd2YWx1ZSBwcm92aWRlZCBpcyBub3QgaW4gYSByZWNvZ25pemVkIElTTyBmb3JtYXQuIG1vbWVudCBjb25zdHJ1Y3Rpb24gZmFsbHMgYmFjayB0byBqcyBEYXRlKCksICcgK1xuICAgICd3aGljaCBpcyBub3QgcmVsaWFibGUgYWNyb3NzIGFsbCBicm93c2VycyBhbmQgdmVyc2lvbnMuIE5vbiBJU08gZGF0ZSBmb3JtYXRzIGFyZSAnICtcbiAgICAnZGlzY291cmFnZWQgYW5kIHdpbGwgYmUgcmVtb3ZlZCBpbiBhbiB1cGNvbWluZyBtYWpvciByZWxlYXNlLiBQbGVhc2UgcmVmZXIgdG8gJyArXG4gICAgJ2h0dHA6Ly9tb21lbnRqcy5jb20vZ3VpZGVzLyMvd2FybmluZ3MvanMtZGF0ZS8gZm9yIG1vcmUgaW5mby4nLFxuICAgIGZ1bmN0aW9uIChjb25maWcpIHtcbiAgICAgICAgY29uZmlnLl9kID0gbmV3IERhdGUoY29uZmlnLl9pICsgKGNvbmZpZy5fdXNlVVRDID8gJyBVVEMnIDogJycpKTtcbiAgICB9XG4pO1xuIiwiLy8gUGljayB0aGUgZmlyc3QgZGVmaW5lZCBvZiB0d28gb3IgdGhyZWUgYXJndW1lbnRzLlxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gZGVmYXVsdHMoYSwgYiwgYykge1xuICAgIGlmIChhICE9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIGE7XG4gICAgfVxuICAgIGlmIChiICE9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIGI7XG4gICAgfVxuICAgIHJldHVybiBjO1xufVxuIiwiaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5pbXBvcnQgeyBjcmVhdGVEYXRlLCBjcmVhdGVVVENEYXRlIH0gZnJvbSAnLi9kYXRlLWZyb20tYXJyYXknO1xuaW1wb3J0IHsgZGF5c0luWWVhciB9IGZyb20gJy4uL3VuaXRzL3llYXInO1xuaW1wb3J0IHsgd2Vla09mWWVhciwgd2Vla3NJblllYXIsIGRheU9mWWVhckZyb21XZWVrcyB9IGZyb20gJy4uL3VuaXRzL3dlZWstY2FsZW5kYXItdXRpbHMnO1xuaW1wb3J0IHsgWUVBUiwgTU9OVEgsIERBVEUsIEhPVVIsIE1JTlVURSwgU0VDT05ELCBNSUxMSVNFQ09ORCB9IGZyb20gJy4uL3VuaXRzL2NvbnN0YW50cyc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4vbG9jYWwnO1xuaW1wb3J0IGRlZmF1bHRzIGZyb20gJy4uL3V0aWxzL2RlZmF1bHRzJztcbmltcG9ydCBnZXRQYXJzaW5nRmxhZ3MgZnJvbSAnLi9wYXJzaW5nLWZsYWdzJztcblxuZnVuY3Rpb24gY3VycmVudERhdGVBcnJheShjb25maWcpIHtcbiAgICAvLyBob29rcyBpcyBhY3R1YWxseSB0aGUgZXhwb3J0ZWQgbW9tZW50IG9iamVjdFxuICAgIHZhciBub3dWYWx1ZSA9IG5ldyBEYXRlKGhvb2tzLm5vdygpKTtcbiAgICBpZiAoY29uZmlnLl91c2VVVEMpIHtcbiAgICAgICAgcmV0dXJuIFtub3dWYWx1ZS5nZXRVVENGdWxsWWVhcigpLCBub3dWYWx1ZS5nZXRVVENNb250aCgpLCBub3dWYWx1ZS5nZXRVVENEYXRlKCldO1xuICAgIH1cbiAgICByZXR1cm4gW25vd1ZhbHVlLmdldEZ1bGxZZWFyKCksIG5vd1ZhbHVlLmdldE1vbnRoKCksIG5vd1ZhbHVlLmdldERhdGUoKV07XG59XG5cbi8vIGNvbnZlcnQgYW4gYXJyYXkgdG8gYSBkYXRlLlxuLy8gdGhlIGFycmF5IHNob3VsZCBtaXJyb3IgdGhlIHBhcmFtZXRlcnMgYmVsb3dcbi8vIG5vdGU6IGFsbCB2YWx1ZXMgcGFzdCB0aGUgeWVhciBhcmUgb3B0aW9uYWwgYW5kIHdpbGwgZGVmYXVsdCB0byB0aGUgbG93ZXN0IHBvc3NpYmxlIHZhbHVlLlxuLy8gW3llYXIsIG1vbnRoLCBkYXkgLCBob3VyLCBtaW51dGUsIHNlY29uZCwgbWlsbGlzZWNvbmRdXG5leHBvcnQgZnVuY3Rpb24gY29uZmlnRnJvbUFycmF5IChjb25maWcpIHtcbiAgICB2YXIgaSwgZGF0ZSwgaW5wdXQgPSBbXSwgY3VycmVudERhdGUsIHllYXJUb1VzZTtcblxuICAgIGlmIChjb25maWcuX2QpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGN1cnJlbnREYXRlID0gY3VycmVudERhdGVBcnJheShjb25maWcpO1xuXG4gICAgLy9jb21wdXRlIGRheSBvZiB0aGUgeWVhciBmcm9tIHdlZWtzIGFuZCB3ZWVrZGF5c1xuICAgIGlmIChjb25maWcuX3cgJiYgY29uZmlnLl9hW0RBVEVdID09IG51bGwgJiYgY29uZmlnLl9hW01PTlRIXSA9PSBudWxsKSB7XG4gICAgICAgIGRheU9mWWVhckZyb21XZWVrSW5mbyhjb25maWcpO1xuICAgIH1cblxuICAgIC8vaWYgdGhlIGRheSBvZiB0aGUgeWVhciBpcyBzZXQsIGZpZ3VyZSBvdXQgd2hhdCBpdCBpc1xuICAgIGlmIChjb25maWcuX2RheU9mWWVhcikge1xuICAgICAgICB5ZWFyVG9Vc2UgPSBkZWZhdWx0cyhjb25maWcuX2FbWUVBUl0sIGN1cnJlbnREYXRlW1lFQVJdKTtcblxuICAgICAgICBpZiAoY29uZmlnLl9kYXlPZlllYXIgPiBkYXlzSW5ZZWFyKHllYXJUb1VzZSkpIHtcbiAgICAgICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLl9vdmVyZmxvd0RheU9mWWVhciA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBkYXRlID0gY3JlYXRlVVRDRGF0ZSh5ZWFyVG9Vc2UsIDAsIGNvbmZpZy5fZGF5T2ZZZWFyKTtcbiAgICAgICAgY29uZmlnLl9hW01PTlRIXSA9IGRhdGUuZ2V0VVRDTW9udGgoKTtcbiAgICAgICAgY29uZmlnLl9hW0RBVEVdID0gZGF0ZS5nZXRVVENEYXRlKCk7XG4gICAgfVxuXG4gICAgLy8gRGVmYXVsdCB0byBjdXJyZW50IGRhdGUuXG4gICAgLy8gKiBpZiBubyB5ZWFyLCBtb250aCwgZGF5IG9mIG1vbnRoIGFyZSBnaXZlbiwgZGVmYXVsdCB0byB0b2RheVxuICAgIC8vICogaWYgZGF5IG9mIG1vbnRoIGlzIGdpdmVuLCBkZWZhdWx0IG1vbnRoIGFuZCB5ZWFyXG4gICAgLy8gKiBpZiBtb250aCBpcyBnaXZlbiwgZGVmYXVsdCBvbmx5IHllYXJcbiAgICAvLyAqIGlmIHllYXIgaXMgZ2l2ZW4sIGRvbid0IGRlZmF1bHQgYW55dGhpbmdcbiAgICBmb3IgKGkgPSAwOyBpIDwgMyAmJiBjb25maWcuX2FbaV0gPT0gbnVsbDsgKytpKSB7XG4gICAgICAgIGNvbmZpZy5fYVtpXSA9IGlucHV0W2ldID0gY3VycmVudERhdGVbaV07XG4gICAgfVxuXG4gICAgLy8gWmVybyBvdXQgd2hhdGV2ZXIgd2FzIG5vdCBkZWZhdWx0ZWQsIGluY2x1ZGluZyB0aW1lXG4gICAgZm9yICg7IGkgPCA3OyBpKyspIHtcbiAgICAgICAgY29uZmlnLl9hW2ldID0gaW5wdXRbaV0gPSAoY29uZmlnLl9hW2ldID09IG51bGwpID8gKGkgPT09IDIgPyAxIDogMCkgOiBjb25maWcuX2FbaV07XG4gICAgfVxuXG4gICAgLy8gQ2hlY2sgZm9yIDI0OjAwOjAwLjAwMFxuICAgIGlmIChjb25maWcuX2FbSE9VUl0gPT09IDI0ICYmXG4gICAgICAgICAgICBjb25maWcuX2FbTUlOVVRFXSA9PT0gMCAmJlxuICAgICAgICAgICAgY29uZmlnLl9hW1NFQ09ORF0gPT09IDAgJiZcbiAgICAgICAgICAgIGNvbmZpZy5fYVtNSUxMSVNFQ09ORF0gPT09IDApIHtcbiAgICAgICAgY29uZmlnLl9uZXh0RGF5ID0gdHJ1ZTtcbiAgICAgICAgY29uZmlnLl9hW0hPVVJdID0gMDtcbiAgICB9XG5cbiAgICBjb25maWcuX2QgPSAoY29uZmlnLl91c2VVVEMgPyBjcmVhdGVVVENEYXRlIDogY3JlYXRlRGF0ZSkuYXBwbHkobnVsbCwgaW5wdXQpO1xuICAgIC8vIEFwcGx5IHRpbWV6b25lIG9mZnNldCBmcm9tIGlucHV0LiBUaGUgYWN0dWFsIHV0Y09mZnNldCBjYW4gYmUgY2hhbmdlZFxuICAgIC8vIHdpdGggcGFyc2Vab25lLlxuICAgIGlmIChjb25maWcuX3R6bSAhPSBudWxsKSB7XG4gICAgICAgIGNvbmZpZy5fZC5zZXRVVENNaW51dGVzKGNvbmZpZy5fZC5nZXRVVENNaW51dGVzKCkgLSBjb25maWcuX3R6bSk7XG4gICAgfVxuXG4gICAgaWYgKGNvbmZpZy5fbmV4dERheSkge1xuICAgICAgICBjb25maWcuX2FbSE9VUl0gPSAyNDtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRheU9mWWVhckZyb21XZWVrSW5mbyhjb25maWcpIHtcbiAgICB2YXIgdywgd2Vla1llYXIsIHdlZWssIHdlZWtkYXksIGRvdywgZG95LCB0ZW1wLCB3ZWVrZGF5T3ZlcmZsb3c7XG5cbiAgICB3ID0gY29uZmlnLl93O1xuICAgIGlmICh3LkdHICE9IG51bGwgfHwgdy5XICE9IG51bGwgfHwgdy5FICE9IG51bGwpIHtcbiAgICAgICAgZG93ID0gMTtcbiAgICAgICAgZG95ID0gNDtcblxuICAgICAgICAvLyBUT0RPOiBXZSBuZWVkIHRvIHRha2UgdGhlIGN1cnJlbnQgaXNvV2Vla1llYXIsIGJ1dCB0aGF0IGRlcGVuZHMgb25cbiAgICAgICAgLy8gaG93IHdlIGludGVycHJldCBub3cgKGxvY2FsLCB1dGMsIGZpeGVkIG9mZnNldCkuIFNvIGNyZWF0ZVxuICAgICAgICAvLyBhIG5vdyB2ZXJzaW9uIG9mIGN1cnJlbnQgY29uZmlnICh0YWtlIGxvY2FsL3V0Yy9vZmZzZXQgZmxhZ3MsIGFuZFxuICAgICAgICAvLyBjcmVhdGUgbm93KS5cbiAgICAgICAgd2Vla1llYXIgPSBkZWZhdWx0cyh3LkdHLCBjb25maWcuX2FbWUVBUl0sIHdlZWtPZlllYXIoY3JlYXRlTG9jYWwoKSwgMSwgNCkueWVhcik7XG4gICAgICAgIHdlZWsgPSBkZWZhdWx0cyh3LlcsIDEpO1xuICAgICAgICB3ZWVrZGF5ID0gZGVmYXVsdHMody5FLCAxKTtcbiAgICAgICAgaWYgKHdlZWtkYXkgPCAxIHx8IHdlZWtkYXkgPiA3KSB7XG4gICAgICAgICAgICB3ZWVrZGF5T3ZlcmZsb3cgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgZG93ID0gY29uZmlnLl9sb2NhbGUuX3dlZWsuZG93O1xuICAgICAgICBkb3kgPSBjb25maWcuX2xvY2FsZS5fd2Vlay5kb3k7XG5cbiAgICAgICAgdmFyIGN1cldlZWsgPSB3ZWVrT2ZZZWFyKGNyZWF0ZUxvY2FsKCksIGRvdywgZG95KTtcblxuICAgICAgICB3ZWVrWWVhciA9IGRlZmF1bHRzKHcuZ2csIGNvbmZpZy5fYVtZRUFSXSwgY3VyV2Vlay55ZWFyKTtcblxuICAgICAgICAvLyBEZWZhdWx0IHRvIGN1cnJlbnQgd2Vlay5cbiAgICAgICAgd2VlayA9IGRlZmF1bHRzKHcudywgY3VyV2Vlay53ZWVrKTtcblxuICAgICAgICBpZiAody5kICE9IG51bGwpIHtcbiAgICAgICAgICAgIC8vIHdlZWtkYXkgLS0gbG93IGRheSBudW1iZXJzIGFyZSBjb25zaWRlcmVkIG5leHQgd2Vla1xuICAgICAgICAgICAgd2Vla2RheSA9IHcuZDtcbiAgICAgICAgICAgIGlmICh3ZWVrZGF5IDwgMCB8fCB3ZWVrZGF5ID4gNikge1xuICAgICAgICAgICAgICAgIHdlZWtkYXlPdmVyZmxvdyA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAody5lICE9IG51bGwpIHtcbiAgICAgICAgICAgIC8vIGxvY2FsIHdlZWtkYXkgLS0gY291bnRpbmcgc3RhcnRzIGZyb20gYmVnaW5pbmcgb2Ygd2Vla1xuICAgICAgICAgICAgd2Vla2RheSA9IHcuZSArIGRvdztcbiAgICAgICAgICAgIGlmICh3LmUgPCAwIHx8IHcuZSA+IDYpIHtcbiAgICAgICAgICAgICAgICB3ZWVrZGF5T3ZlcmZsb3cgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gZGVmYXVsdCB0byBiZWdpbmluZyBvZiB3ZWVrXG4gICAgICAgICAgICB3ZWVrZGF5ID0gZG93O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlmICh3ZWVrIDwgMSB8fCB3ZWVrID4gd2Vla3NJblllYXIod2Vla1llYXIsIGRvdywgZG95KSkge1xuICAgICAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5fb3ZlcmZsb3dXZWVrcyA9IHRydWU7XG4gICAgfSBlbHNlIGlmICh3ZWVrZGF5T3ZlcmZsb3cgIT0gbnVsbCkge1xuICAgICAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5fb3ZlcmZsb3dXZWVrZGF5ID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0ZW1wID0gZGF5T2ZZZWFyRnJvbVdlZWtzKHdlZWtZZWFyLCB3ZWVrLCB3ZWVrZGF5LCBkb3csIGRveSk7XG4gICAgICAgIGNvbmZpZy5fYVtZRUFSXSA9IHRlbXAueWVhcjtcbiAgICAgICAgY29uZmlnLl9kYXlPZlllYXIgPSB0ZW1wLmRheU9mWWVhcjtcbiAgICB9XG59XG4iLCJpbXBvcnQgeyBjb25maWdGcm9tSVNPIH0gZnJvbSAnLi9mcm9tLXN0cmluZyc7XG5pbXBvcnQgeyBjb25maWdGcm9tQXJyYXkgfSBmcm9tICcuL2Zyb20tYXJyYXknO1xuaW1wb3J0IHsgZ2V0UGFyc2VSZWdleEZvclRva2VuIH0gICBmcm9tICcuLi9wYXJzZS9yZWdleCc7XG5pbXBvcnQgeyBhZGRUaW1lVG9BcnJheUZyb21Ub2tlbiB9IGZyb20gJy4uL3BhcnNlL3Rva2VuJztcbmltcG9ydCB7IGV4cGFuZEZvcm1hdCwgZm9ybWF0VG9rZW5GdW5jdGlvbnMsIGZvcm1hdHRpbmdUb2tlbnMgfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcbmltcG9ydCBjaGVja092ZXJmbG93IGZyb20gJy4vY2hlY2stb3ZlcmZsb3cnO1xuaW1wb3J0IHsgSE9VUiB9IGZyb20gJy4uL3VuaXRzL2NvbnN0YW50cyc7XG5pbXBvcnQgeyBob29rcyB9IGZyb20gJy4uL3V0aWxzL2hvb2tzJztcbmltcG9ydCBnZXRQYXJzaW5nRmxhZ3MgZnJvbSAnLi9wYXJzaW5nLWZsYWdzJztcblxuLy8gY29uc3RhbnQgdGhhdCByZWZlcnMgdG8gdGhlIElTTyBzdGFuZGFyZFxuaG9va3MuSVNPXzg2MDEgPSBmdW5jdGlvbiAoKSB7fTtcblxuLy8gZGF0ZSBmcm9tIHN0cmluZyBhbmQgZm9ybWF0IHN0cmluZ1xuZXhwb3J0IGZ1bmN0aW9uIGNvbmZpZ0Zyb21TdHJpbmdBbmRGb3JtYXQoY29uZmlnKSB7XG4gICAgLy8gVE9ETzogTW92ZSB0aGlzIHRvIGFub3RoZXIgcGFydCBvZiB0aGUgY3JlYXRpb24gZmxvdyB0byBwcmV2ZW50IGNpcmN1bGFyIGRlcHNcbiAgICBpZiAoY29uZmlnLl9mID09PSBob29rcy5JU09fODYwMSkge1xuICAgICAgICBjb25maWdGcm9tSVNPKGNvbmZpZyk7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25maWcuX2EgPSBbXTtcbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5lbXB0eSA9IHRydWU7XG5cbiAgICAvLyBUaGlzIGFycmF5IGlzIHVzZWQgdG8gbWFrZSBhIERhdGUsIGVpdGhlciB3aXRoIGBuZXcgRGF0ZWAgb3IgYERhdGUuVVRDYFxuICAgIHZhciBzdHJpbmcgPSAnJyArIGNvbmZpZy5faSxcbiAgICAgICAgaSwgcGFyc2VkSW5wdXQsIHRva2VucywgdG9rZW4sIHNraXBwZWQsXG4gICAgICAgIHN0cmluZ0xlbmd0aCA9IHN0cmluZy5sZW5ndGgsXG4gICAgICAgIHRvdGFsUGFyc2VkSW5wdXRMZW5ndGggPSAwO1xuXG4gICAgdG9rZW5zID0gZXhwYW5kRm9ybWF0KGNvbmZpZy5fZiwgY29uZmlnLl9sb2NhbGUpLm1hdGNoKGZvcm1hdHRpbmdUb2tlbnMpIHx8IFtdO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgICAgICB0b2tlbiA9IHRva2Vuc1tpXTtcbiAgICAgICAgcGFyc2VkSW5wdXQgPSAoc3RyaW5nLm1hdGNoKGdldFBhcnNlUmVnZXhGb3JUb2tlbih0b2tlbiwgY29uZmlnKSkgfHwgW10pWzBdO1xuICAgICAgICAvLyBjb25zb2xlLmxvZygndG9rZW4nLCB0b2tlbiwgJ3BhcnNlZElucHV0JywgcGFyc2VkSW5wdXQsXG4gICAgICAgIC8vICAgICAgICAgJ3JlZ2V4JywgZ2V0UGFyc2VSZWdleEZvclRva2VuKHRva2VuLCBjb25maWcpKTtcbiAgICAgICAgaWYgKHBhcnNlZElucHV0KSB7XG4gICAgICAgICAgICBza2lwcGVkID0gc3RyaW5nLnN1YnN0cigwLCBzdHJpbmcuaW5kZXhPZihwYXJzZWRJbnB1dCkpO1xuICAgICAgICAgICAgaWYgKHNraXBwZWQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLnVudXNlZElucHV0LnB1c2goc2tpcHBlZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdHJpbmcgPSBzdHJpbmcuc2xpY2Uoc3RyaW5nLmluZGV4T2YocGFyc2VkSW5wdXQpICsgcGFyc2VkSW5wdXQubGVuZ3RoKTtcbiAgICAgICAgICAgIHRvdGFsUGFyc2VkSW5wdXRMZW5ndGggKz0gcGFyc2VkSW5wdXQubGVuZ3RoO1xuICAgICAgICB9XG4gICAgICAgIC8vIGRvbid0IHBhcnNlIGlmIGl0J3Mgbm90IGEga25vd24gdG9rZW5cbiAgICAgICAgaWYgKGZvcm1hdFRva2VuRnVuY3Rpb25zW3Rva2VuXSkge1xuICAgICAgICAgICAgaWYgKHBhcnNlZElucHV0KSB7XG4gICAgICAgICAgICAgICAgZ2V0UGFyc2luZ0ZsYWdzKGNvbmZpZykuZW1wdHkgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLnVudXNlZFRva2Vucy5wdXNoKHRva2VuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFkZFRpbWVUb0FycmF5RnJvbVRva2VuKHRva2VuLCBwYXJzZWRJbnB1dCwgY29uZmlnKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChjb25maWcuX3N0cmljdCAmJiAhcGFyc2VkSW5wdXQpIHtcbiAgICAgICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLnVudXNlZFRva2Vucy5wdXNoKHRva2VuKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIGFkZCByZW1haW5pbmcgdW5wYXJzZWQgaW5wdXQgbGVuZ3RoIHRvIHRoZSBzdHJpbmdcbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5jaGFyc0xlZnRPdmVyID0gc3RyaW5nTGVuZ3RoIC0gdG90YWxQYXJzZWRJbnB1dExlbmd0aDtcbiAgICBpZiAoc3RyaW5nLmxlbmd0aCA+IDApIHtcbiAgICAgICAgZ2V0UGFyc2luZ0ZsYWdzKGNvbmZpZykudW51c2VkSW5wdXQucHVzaChzdHJpbmcpO1xuICAgIH1cblxuICAgIC8vIGNsZWFyIF8xMmggZmxhZyBpZiBob3VyIGlzIDw9IDEyXG4gICAgaWYgKGNvbmZpZy5fYVtIT1VSXSA8PSAxMiAmJlxuICAgICAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5iaWdIb3VyID09PSB0cnVlICYmXG4gICAgICAgIGNvbmZpZy5fYVtIT1VSXSA+IDApIHtcbiAgICAgICAgZ2V0UGFyc2luZ0ZsYWdzKGNvbmZpZykuYmlnSG91ciA9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXRQYXJzaW5nRmxhZ3MoY29uZmlnKS5wYXJzZWREYXRlUGFydHMgPSBjb25maWcuX2Euc2xpY2UoMCk7XG4gICAgZ2V0UGFyc2luZ0ZsYWdzKGNvbmZpZykubWVyaWRpZW0gPSBjb25maWcuX21lcmlkaWVtO1xuICAgIC8vIGhhbmRsZSBtZXJpZGllbVxuICAgIGNvbmZpZy5fYVtIT1VSXSA9IG1lcmlkaWVtRml4V3JhcChjb25maWcuX2xvY2FsZSwgY29uZmlnLl9hW0hPVVJdLCBjb25maWcuX21lcmlkaWVtKTtcblxuICAgIGNvbmZpZ0Zyb21BcnJheShjb25maWcpO1xuICAgIGNoZWNrT3ZlcmZsb3coY29uZmlnKTtcbn1cblxuXG5mdW5jdGlvbiBtZXJpZGllbUZpeFdyYXAgKGxvY2FsZSwgaG91ciwgbWVyaWRpZW0pIHtcbiAgICB2YXIgaXNQbTtcblxuICAgIGlmIChtZXJpZGllbSA9PSBudWxsKSB7XG4gICAgICAgIC8vIG5vdGhpbmcgdG8gZG9cbiAgICAgICAgcmV0dXJuIGhvdXI7XG4gICAgfVxuICAgIGlmIChsb2NhbGUubWVyaWRpZW1Ib3VyICE9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIGxvY2FsZS5tZXJpZGllbUhvdXIoaG91ciwgbWVyaWRpZW0pO1xuICAgIH0gZWxzZSBpZiAobG9jYWxlLmlzUE0gIT0gbnVsbCkge1xuICAgICAgICAvLyBGYWxsYmFja1xuICAgICAgICBpc1BtID0gbG9jYWxlLmlzUE0obWVyaWRpZW0pO1xuICAgICAgICBpZiAoaXNQbSAmJiBob3VyIDwgMTIpIHtcbiAgICAgICAgICAgIGhvdXIgKz0gMTI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCFpc1BtICYmIGhvdXIgPT09IDEyKSB7XG4gICAgICAgICAgICBob3VyID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaG91cjtcbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyB0aGlzIGlzIG5vdCBzdXBwb3NlZCB0byBoYXBwZW5cbiAgICAgICAgcmV0dXJuIGhvdXI7XG4gICAgfVxufVxuIiwiaW1wb3J0IHsgY29weUNvbmZpZyB9IGZyb20gJy4uL21vbWVudC9jb25zdHJ1Y3Rvcic7XG5pbXBvcnQgeyBjb25maWdGcm9tU3RyaW5nQW5kRm9ybWF0IH0gZnJvbSAnLi9mcm9tLXN0cmluZy1hbmQtZm9ybWF0JztcbmltcG9ydCBnZXRQYXJzaW5nRmxhZ3MgZnJvbSAnLi9wYXJzaW5nLWZsYWdzJztcbmltcG9ydCB7IGlzVmFsaWQgfSBmcm9tICcuL3ZhbGlkJztcbmltcG9ydCBleHRlbmQgZnJvbSAnLi4vdXRpbHMvZXh0ZW5kJztcblxuLy8gZGF0ZSBmcm9tIHN0cmluZyBhbmQgYXJyYXkgb2YgZm9ybWF0IHN0cmluZ3NcbmV4cG9ydCBmdW5jdGlvbiBjb25maWdGcm9tU3RyaW5nQW5kQXJyYXkoY29uZmlnKSB7XG4gICAgdmFyIHRlbXBDb25maWcsXG4gICAgICAgIGJlc3RNb21lbnQsXG5cbiAgICAgICAgc2NvcmVUb0JlYXQsXG4gICAgICAgIGksXG4gICAgICAgIGN1cnJlbnRTY29yZTtcblxuICAgIGlmIChjb25maWcuX2YubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIGdldFBhcnNpbmdGbGFncyhjb25maWcpLmludmFsaWRGb3JtYXQgPSB0cnVlO1xuICAgICAgICBjb25maWcuX2QgPSBuZXcgRGF0ZShOYU4pO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgZm9yIChpID0gMDsgaSA8IGNvbmZpZy5fZi5sZW5ndGg7IGkrKykge1xuICAgICAgICBjdXJyZW50U2NvcmUgPSAwO1xuICAgICAgICB0ZW1wQ29uZmlnID0gY29weUNvbmZpZyh7fSwgY29uZmlnKTtcbiAgICAgICAgaWYgKGNvbmZpZy5fdXNlVVRDICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRlbXBDb25maWcuX3VzZVVUQyA9IGNvbmZpZy5fdXNlVVRDO1xuICAgICAgICB9XG4gICAgICAgIHRlbXBDb25maWcuX2YgPSBjb25maWcuX2ZbaV07XG4gICAgICAgIGNvbmZpZ0Zyb21TdHJpbmdBbmRGb3JtYXQodGVtcENvbmZpZyk7XG5cbiAgICAgICAgaWYgKCFpc1ZhbGlkKHRlbXBDb25maWcpKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGlmIHRoZXJlIGlzIGFueSBpbnB1dCB0aGF0IHdhcyBub3QgcGFyc2VkIGFkZCBhIHBlbmFsdHkgZm9yIHRoYXQgZm9ybWF0XG4gICAgICAgIGN1cnJlbnRTY29yZSArPSBnZXRQYXJzaW5nRmxhZ3ModGVtcENvbmZpZykuY2hhcnNMZWZ0T3ZlcjtcblxuICAgICAgICAvL29yIHRva2Vuc1xuICAgICAgICBjdXJyZW50U2NvcmUgKz0gZ2V0UGFyc2luZ0ZsYWdzKHRlbXBDb25maWcpLnVudXNlZFRva2Vucy5sZW5ndGggKiAxMDtcblxuICAgICAgICBnZXRQYXJzaW5nRmxhZ3ModGVtcENvbmZpZykuc2NvcmUgPSBjdXJyZW50U2NvcmU7XG5cbiAgICAgICAgaWYgKHNjb3JlVG9CZWF0ID09IG51bGwgfHwgY3VycmVudFNjb3JlIDwgc2NvcmVUb0JlYXQpIHtcbiAgICAgICAgICAgIHNjb3JlVG9CZWF0ID0gY3VycmVudFNjb3JlO1xuICAgICAgICAgICAgYmVzdE1vbWVudCA9IHRlbXBDb25maWc7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBleHRlbmQoY29uZmlnLCBiZXN0TW9tZW50IHx8IHRlbXBDb25maWcpO1xufVxuIiwiaW1wb3J0IHsgbm9ybWFsaXplT2JqZWN0VW5pdHMgfSBmcm9tICcuLi91bml0cy9hbGlhc2VzJztcbmltcG9ydCB7IGNvbmZpZ0Zyb21BcnJheSB9IGZyb20gJy4vZnJvbS1hcnJheSc7XG5pbXBvcnQgbWFwIGZyb20gJy4uL3V0aWxzL21hcCc7XG5cbmV4cG9ydCBmdW5jdGlvbiBjb25maWdGcm9tT2JqZWN0KGNvbmZpZykge1xuICAgIGlmIChjb25maWcuX2QpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBpID0gbm9ybWFsaXplT2JqZWN0VW5pdHMoY29uZmlnLl9pKTtcbiAgICBjb25maWcuX2EgPSBtYXAoW2kueWVhciwgaS5tb250aCwgaS5kYXkgfHwgaS5kYXRlLCBpLmhvdXIsIGkubWludXRlLCBpLnNlY29uZCwgaS5taWxsaXNlY29uZF0sIGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgcmV0dXJuIG9iaiAmJiBwYXJzZUludChvYmosIDEwKTtcbiAgICB9KTtcblxuICAgIGNvbmZpZ0Zyb21BcnJheShjb25maWcpO1xufVxuIiwiaW1wb3J0IGlzQXJyYXkgZnJvbSAnLi4vdXRpbHMvaXMtYXJyYXknO1xuaW1wb3J0IGlzT2JqZWN0IGZyb20gJy4uL3V0aWxzL2lzLW9iamVjdCc7XG5pbXBvcnQgaXNPYmplY3RFbXB0eSBmcm9tICcuLi91dGlscy9pcy1vYmplY3QtZW1wdHknO1xuaW1wb3J0IGlzTnVtYmVyIGZyb20gJy4uL3V0aWxzL2lzLW51bWJlcic7XG5pbXBvcnQgaXNEYXRlIGZyb20gJy4uL3V0aWxzL2lzLWRhdGUnO1xuaW1wb3J0IG1hcCBmcm9tICcuLi91dGlscy9tYXAnO1xuaW1wb3J0IHsgY3JlYXRlSW52YWxpZCB9IGZyb20gJy4vdmFsaWQnO1xuaW1wb3J0IHsgTW9tZW50LCBpc01vbWVudCB9IGZyb20gJy4uL21vbWVudC9jb25zdHJ1Y3Rvcic7XG5pbXBvcnQgeyBnZXRMb2NhbGUgfSBmcm9tICcuLi9sb2NhbGUvbG9jYWxlcyc7XG5pbXBvcnQgeyBob29rcyB9IGZyb20gJy4uL3V0aWxzL2hvb2tzJztcbmltcG9ydCBjaGVja092ZXJmbG93IGZyb20gJy4vY2hlY2stb3ZlcmZsb3cnO1xuaW1wb3J0IHsgaXNWYWxpZCB9IGZyb20gJy4vdmFsaWQnO1xuXG5pbXBvcnQgeyBjb25maWdGcm9tU3RyaW5nQW5kQXJyYXkgfSAgZnJvbSAnLi9mcm9tLXN0cmluZy1hbmQtYXJyYXknO1xuaW1wb3J0IHsgY29uZmlnRnJvbVN0cmluZ0FuZEZvcm1hdCB9IGZyb20gJy4vZnJvbS1zdHJpbmctYW5kLWZvcm1hdCc7XG5pbXBvcnQgeyBjb25maWdGcm9tU3RyaW5nIH0gICAgICAgICAgZnJvbSAnLi9mcm9tLXN0cmluZyc7XG5pbXBvcnQgeyBjb25maWdGcm9tQXJyYXkgfSAgICAgICAgICAgZnJvbSAnLi9mcm9tLWFycmF5JztcbmltcG9ydCB7IGNvbmZpZ0Zyb21PYmplY3QgfSAgICAgICAgICBmcm9tICcuL2Zyb20tb2JqZWN0JztcblxuZnVuY3Rpb24gY3JlYXRlRnJvbUNvbmZpZyAoY29uZmlnKSB7XG4gICAgdmFyIHJlcyA9IG5ldyBNb21lbnQoY2hlY2tPdmVyZmxvdyhwcmVwYXJlQ29uZmlnKGNvbmZpZykpKTtcbiAgICBpZiAocmVzLl9uZXh0RGF5KSB7XG4gICAgICAgIC8vIEFkZGluZyBpcyBzbWFydCBlbm91Z2ggYXJvdW5kIERTVFxuICAgICAgICByZXMuYWRkKDEsICdkJyk7XG4gICAgICAgIHJlcy5fbmV4dERheSA9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcHJlcGFyZUNvbmZpZyAoY29uZmlnKSB7XG4gICAgdmFyIGlucHV0ID0gY29uZmlnLl9pLFxuICAgICAgICBmb3JtYXQgPSBjb25maWcuX2Y7XG5cbiAgICBjb25maWcuX2xvY2FsZSA9IGNvbmZpZy5fbG9jYWxlIHx8IGdldExvY2FsZShjb25maWcuX2wpO1xuXG4gICAgaWYgKGlucHV0ID09PSBudWxsIHx8IChmb3JtYXQgPT09IHVuZGVmaW5lZCAmJiBpbnB1dCA9PT0gJycpKSB7XG4gICAgICAgIHJldHVybiBjcmVhdGVJbnZhbGlkKHtudWxsSW5wdXQ6IHRydWV9KTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIGlucHV0ID09PSAnc3RyaW5nJykge1xuICAgICAgICBjb25maWcuX2kgPSBpbnB1dCA9IGNvbmZpZy5fbG9jYWxlLnByZXBhcnNlKGlucHV0KTtcbiAgICB9XG5cbiAgICBpZiAoaXNNb21lbnQoaW5wdXQpKSB7XG4gICAgICAgIHJldHVybiBuZXcgTW9tZW50KGNoZWNrT3ZlcmZsb3coaW5wdXQpKTtcbiAgICB9IGVsc2UgaWYgKGlzRGF0ZShpbnB1dCkpIHtcbiAgICAgICAgY29uZmlnLl9kID0gaW5wdXQ7XG4gICAgfSBlbHNlIGlmIChpc0FycmF5KGZvcm1hdCkpIHtcbiAgICAgICAgY29uZmlnRnJvbVN0cmluZ0FuZEFycmF5KGNvbmZpZyk7XG4gICAgfSBlbHNlIGlmIChmb3JtYXQpIHtcbiAgICAgICAgY29uZmlnRnJvbVN0cmluZ0FuZEZvcm1hdChjb25maWcpO1xuICAgIH0gIGVsc2Uge1xuICAgICAgICBjb25maWdGcm9tSW5wdXQoY29uZmlnKTtcbiAgICB9XG5cbiAgICBpZiAoIWlzVmFsaWQoY29uZmlnKSkge1xuICAgICAgICBjb25maWcuX2QgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBjb25maWc7XG59XG5cbmZ1bmN0aW9uIGNvbmZpZ0Zyb21JbnB1dChjb25maWcpIHtcbiAgICB2YXIgaW5wdXQgPSBjb25maWcuX2k7XG4gICAgaWYgKGlucHV0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY29uZmlnLl9kID0gbmV3IERhdGUoaG9va3Mubm93KCkpO1xuICAgIH0gZWxzZSBpZiAoaXNEYXRlKGlucHV0KSkge1xuICAgICAgICBjb25maWcuX2QgPSBuZXcgRGF0ZShpbnB1dC52YWx1ZU9mKCkpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGlucHV0ID09PSAnc3RyaW5nJykge1xuICAgICAgICBjb25maWdGcm9tU3RyaW5nKGNvbmZpZyk7XG4gICAgfSBlbHNlIGlmIChpc0FycmF5KGlucHV0KSkge1xuICAgICAgICBjb25maWcuX2EgPSBtYXAoaW5wdXQuc2xpY2UoMCksIGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgICAgIHJldHVybiBwYXJzZUludChvYmosIDEwKTtcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbmZpZ0Zyb21BcnJheShjb25maWcpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mKGlucHV0KSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgY29uZmlnRnJvbU9iamVjdChjb25maWcpO1xuICAgIH0gZWxzZSBpZiAoaXNOdW1iZXIoaW5wdXQpKSB7XG4gICAgICAgIC8vIGZyb20gbWlsbGlzZWNvbmRzXG4gICAgICAgIGNvbmZpZy5fZCA9IG5ldyBEYXRlKGlucHV0KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBob29rcy5jcmVhdGVGcm9tSW5wdXRGYWxsYmFjayhjb25maWcpO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUxvY2FsT3JVVEMgKGlucHV0LCBmb3JtYXQsIGxvY2FsZSwgc3RyaWN0LCBpc1VUQykge1xuICAgIHZhciBjID0ge307XG5cbiAgICBpZiAobG9jYWxlID09PSB0cnVlIHx8IGxvY2FsZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgc3RyaWN0ID0gbG9jYWxlO1xuICAgICAgICBsb2NhbGUgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgaWYgKChpc09iamVjdChpbnB1dCkgJiYgaXNPYmplY3RFbXB0eShpbnB1dCkpIHx8XG4gICAgICAgICAgICAoaXNBcnJheShpbnB1dCkgJiYgaW5wdXQubGVuZ3RoID09PSAwKSkge1xuICAgICAgICBpbnB1dCA9IHVuZGVmaW5lZDtcbiAgICB9XG4gICAgLy8gb2JqZWN0IGNvbnN0cnVjdGlvbiBtdXN0IGJlIGRvbmUgdGhpcyB3YXkuXG4gICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL21vbWVudC9tb21lbnQvaXNzdWVzLzE0MjNcbiAgICBjLl9pc0FNb21lbnRPYmplY3QgPSB0cnVlO1xuICAgIGMuX3VzZVVUQyA9IGMuX2lzVVRDID0gaXNVVEM7XG4gICAgYy5fbCA9IGxvY2FsZTtcbiAgICBjLl9pID0gaW5wdXQ7XG4gICAgYy5fZiA9IGZvcm1hdDtcbiAgICBjLl9zdHJpY3QgPSBzdHJpY3Q7XG5cbiAgICByZXR1cm4gY3JlYXRlRnJvbUNvbmZpZyhjKTtcbn1cbiIsImltcG9ydCB7IGNyZWF0ZUxvY2FsT3JVVEMgfSBmcm9tICcuL2Zyb20tYW55dGhpbmcnO1xuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlTG9jYWwgKGlucHV0LCBmb3JtYXQsIGxvY2FsZSwgc3RyaWN0KSB7XG4gICAgcmV0dXJuIGNyZWF0ZUxvY2FsT3JVVEMoaW5wdXQsIGZvcm1hdCwgbG9jYWxlLCBzdHJpY3QsIGZhbHNlKTtcbn1cbiIsImltcG9ydCB7IGRlcHJlY2F0ZSB9IGZyb20gJy4uL3V0aWxzL2RlcHJlY2F0ZSc7XG5pbXBvcnQgaXNBcnJheSBmcm9tICcuLi91dGlscy9pcy1hcnJheSc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyBjcmVhdGVJbnZhbGlkIH0gZnJvbSAnLi4vY3JlYXRlL3ZhbGlkJztcblxuZXhwb3J0IHZhciBwcm90b3R5cGVNaW4gPSBkZXByZWNhdGUoXG4gICAgJ21vbWVudCgpLm1pbiBpcyBkZXByZWNhdGVkLCB1c2UgbW9tZW50Lm1heCBpbnN0ZWFkLiBodHRwOi8vbW9tZW50anMuY29tL2d1aWRlcy8jL3dhcm5pbmdzL21pbi1tYXgvJyxcbiAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvdGhlciA9IGNyZWF0ZUxvY2FsLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQoKSAmJiBvdGhlci5pc1ZhbGlkKCkpIHtcbiAgICAgICAgICAgIHJldHVybiBvdGhlciA8IHRoaXMgPyB0aGlzIDogb3RoZXI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gY3JlYXRlSW52YWxpZCgpO1xuICAgICAgICB9XG4gICAgfVxuKTtcblxuZXhwb3J0IHZhciBwcm90b3R5cGVNYXggPSBkZXByZWNhdGUoXG4gICAgJ21vbWVudCgpLm1heCBpcyBkZXByZWNhdGVkLCB1c2UgbW9tZW50Lm1pbiBpbnN0ZWFkLiBodHRwOi8vbW9tZW50anMuY29tL2d1aWRlcy8jL3dhcm5pbmdzL21pbi1tYXgvJyxcbiAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvdGhlciA9IGNyZWF0ZUxvY2FsLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQoKSAmJiBvdGhlci5pc1ZhbGlkKCkpIHtcbiAgICAgICAgICAgIHJldHVybiBvdGhlciA+IHRoaXMgPyB0aGlzIDogb3RoZXI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gY3JlYXRlSW52YWxpZCgpO1xuICAgICAgICB9XG4gICAgfVxuKTtcblxuLy8gUGljayBhIG1vbWVudCBtIGZyb20gbW9tZW50cyBzbyB0aGF0IG1bZm5dKG90aGVyKSBpcyB0cnVlIGZvciBhbGxcbi8vIG90aGVyLiBUaGlzIHJlbGllcyBvbiB0aGUgZnVuY3Rpb24gZm4gdG8gYmUgdHJhbnNpdGl2ZS5cbi8vXG4vLyBtb21lbnRzIHNob3VsZCBlaXRoZXIgYmUgYW4gYXJyYXkgb2YgbW9tZW50IG9iamVjdHMgb3IgYW4gYXJyYXksIHdob3NlXG4vLyBmaXJzdCBlbGVtZW50IGlzIGFuIGFycmF5IG9mIG1vbWVudCBvYmplY3RzLlxuZnVuY3Rpb24gcGlja0J5KGZuLCBtb21lbnRzKSB7XG4gICAgdmFyIHJlcywgaTtcbiAgICBpZiAobW9tZW50cy5sZW5ndGggPT09IDEgJiYgaXNBcnJheShtb21lbnRzWzBdKSkge1xuICAgICAgICBtb21lbnRzID0gbW9tZW50c1swXTtcbiAgICB9XG4gICAgaWYgKCFtb21lbnRzLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gY3JlYXRlTG9jYWwoKTtcbiAgICB9XG4gICAgcmVzID0gbW9tZW50c1swXTtcbiAgICBmb3IgKGkgPSAxOyBpIDwgbW9tZW50cy5sZW5ndGg7ICsraSkge1xuICAgICAgICBpZiAoIW1vbWVudHNbaV0uaXNWYWxpZCgpIHx8IG1vbWVudHNbaV1bZm5dKHJlcykpIHtcbiAgICAgICAgICAgIHJlcyA9IG1vbWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlcztcbn1cblxuLy8gVE9ETzogVXNlIFtdLnNvcnQgaW5zdGVhZD9cbmV4cG9ydCBmdW5jdGlvbiBtaW4gKCkge1xuICAgIHZhciBhcmdzID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDApO1xuXG4gICAgcmV0dXJuIHBpY2tCeSgnaXNCZWZvcmUnLCBhcmdzKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG1heCAoKSB7XG4gICAgdmFyIGFyZ3MgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMCk7XG5cbiAgICByZXR1cm4gcGlja0J5KCdpc0FmdGVyJywgYXJncyk7XG59XG4iLCJleHBvcnQgdmFyIG5vdyA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gRGF0ZS5ub3cgPyBEYXRlLm5vdygpIDogKyhuZXcgRGF0ZSgpKTtcbn07XG4iLCJpbXBvcnQgeyBub3JtYWxpemVPYmplY3RVbml0cyB9IGZyb20gJy4uL3VuaXRzL2FsaWFzZXMnO1xuaW1wb3J0IHsgZ2V0TG9jYWxlIH0gZnJvbSAnLi4vbG9jYWxlL2xvY2FsZXMnO1xuXG5leHBvcnQgZnVuY3Rpb24gRHVyYXRpb24gKGR1cmF0aW9uKSB7XG4gICAgdmFyIG5vcm1hbGl6ZWRJbnB1dCA9IG5vcm1hbGl6ZU9iamVjdFVuaXRzKGR1cmF0aW9uKSxcbiAgICAgICAgeWVhcnMgPSBub3JtYWxpemVkSW5wdXQueWVhciB8fCAwLFxuICAgICAgICBxdWFydGVycyA9IG5vcm1hbGl6ZWRJbnB1dC5xdWFydGVyIHx8IDAsXG4gICAgICAgIG1vbnRocyA9IG5vcm1hbGl6ZWRJbnB1dC5tb250aCB8fCAwLFxuICAgICAgICB3ZWVrcyA9IG5vcm1hbGl6ZWRJbnB1dC53ZWVrIHx8IDAsXG4gICAgICAgIGRheXMgPSBub3JtYWxpemVkSW5wdXQuZGF5IHx8IDAsXG4gICAgICAgIGhvdXJzID0gbm9ybWFsaXplZElucHV0LmhvdXIgfHwgMCxcbiAgICAgICAgbWludXRlcyA9IG5vcm1hbGl6ZWRJbnB1dC5taW51dGUgfHwgMCxcbiAgICAgICAgc2Vjb25kcyA9IG5vcm1hbGl6ZWRJbnB1dC5zZWNvbmQgfHwgMCxcbiAgICAgICAgbWlsbGlzZWNvbmRzID0gbm9ybWFsaXplZElucHV0Lm1pbGxpc2Vjb25kIHx8IDA7XG5cbiAgICAvLyByZXByZXNlbnRhdGlvbiBmb3IgZGF0ZUFkZFJlbW92ZVxuICAgIHRoaXMuX21pbGxpc2Vjb25kcyA9ICttaWxsaXNlY29uZHMgK1xuICAgICAgICBzZWNvbmRzICogMWUzICsgLy8gMTAwMFxuICAgICAgICBtaW51dGVzICogNmU0ICsgLy8gMTAwMCAqIDYwXG4gICAgICAgIGhvdXJzICogMTAwMCAqIDYwICogNjA7IC8vdXNpbmcgMTAwMCAqIDYwICogNjAgaW5zdGVhZCBvZiAzNmU1IHRvIGF2b2lkIGZsb2F0aW5nIHBvaW50IHJvdW5kaW5nIGVycm9ycyBodHRwczovL2dpdGh1Yi5jb20vbW9tZW50L21vbWVudC9pc3N1ZXMvMjk3OFxuICAgIC8vIEJlY2F1c2Ugb2YgZGF0ZUFkZFJlbW92ZSB0cmVhdHMgMjQgaG91cnMgYXMgZGlmZmVyZW50IGZyb20gYVxuICAgIC8vIGRheSB3aGVuIHdvcmtpbmcgYXJvdW5kIERTVCwgd2UgbmVlZCB0byBzdG9yZSB0aGVtIHNlcGFyYXRlbHlcbiAgICB0aGlzLl9kYXlzID0gK2RheXMgK1xuICAgICAgICB3ZWVrcyAqIDc7XG4gICAgLy8gSXQgaXMgaW1wb3NzaWJsZSB0cmFuc2xhdGUgbW9udGhzIGludG8gZGF5cyB3aXRob3V0IGtub3dpbmdcbiAgICAvLyB3aGljaCBtb250aHMgeW91IGFyZSBhcmUgdGFsa2luZyBhYm91dCwgc28gd2UgaGF2ZSB0byBzdG9yZVxuICAgIC8vIGl0IHNlcGFyYXRlbHkuXG4gICAgdGhpcy5fbW9udGhzID0gK21vbnRocyArXG4gICAgICAgIHF1YXJ0ZXJzICogMyArXG4gICAgICAgIHllYXJzICogMTI7XG5cbiAgICB0aGlzLl9kYXRhID0ge307XG5cbiAgICB0aGlzLl9sb2NhbGUgPSBnZXRMb2NhbGUoKTtcblxuICAgIHRoaXMuX2J1YmJsZSgpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNEdXJhdGlvbiAob2JqKSB7XG4gICAgcmV0dXJuIG9iaiBpbnN0YW5jZW9mIER1cmF0aW9uO1xufVxuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gYWJzUm91bmQgKG51bWJlcikge1xuICAgIGlmIChudW1iZXIgPCAwKSB7XG4gICAgICAgIHJldHVybiBNYXRoLnJvdW5kKC0xICogbnVtYmVyKSAqIC0xO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBNYXRoLnJvdW5kKG51bWJlcik7XG4gICAgfVxufVxuIiwiaW1wb3J0IHplcm9GaWxsIGZyb20gJy4uL3V0aWxzL3plcm8tZmlsbCc7XG5pbXBvcnQgeyBjcmVhdGVEdXJhdGlvbiB9IGZyb20gJy4uL2R1cmF0aW9uL2NyZWF0ZSc7XG5pbXBvcnQgeyBhZGRTdWJ0cmFjdCB9IGZyb20gJy4uL21vbWVudC9hZGQtc3VidHJhY3QnO1xuaW1wb3J0IHsgaXNNb21lbnQsIGNvcHlDb25maWcgfSBmcm9tICcuLi9tb21lbnQvY29uc3RydWN0b3InO1xuaW1wb3J0IHsgYWRkRm9ybWF0VG9rZW4gfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoT2Zmc2V0LCBtYXRjaFNob3J0T2Zmc2V0IH0gZnJvbSAnLi4vcGFyc2UvcmVnZXgnO1xuaW1wb3J0IHsgYWRkUGFyc2VUb2tlbiB9IGZyb20gJy4uL3BhcnNlL3Rva2VuJztcbmltcG9ydCB7IGNyZWF0ZUxvY2FsIH0gZnJvbSAnLi4vY3JlYXRlL2xvY2FsJztcbmltcG9ydCB7IHByZXBhcmVDb25maWcgfSBmcm9tICcuLi9jcmVhdGUvZnJvbS1hbnl0aGluZyc7XG5pbXBvcnQgeyBjcmVhdGVVVEMgfSBmcm9tICcuLi9jcmVhdGUvdXRjJztcbmltcG9ydCBpc0RhdGUgZnJvbSAnLi4vdXRpbHMvaXMtZGF0ZSc7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcbmltcG9ydCBpc1VuZGVmaW5lZCBmcm9tICcuLi91dGlscy9pcy11bmRlZmluZWQnO1xuaW1wb3J0IGNvbXBhcmVBcnJheXMgZnJvbSAnLi4vdXRpbHMvY29tcGFyZS1hcnJheXMnO1xuaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5cbi8vIEZPUk1BVFRJTkdcblxuZnVuY3Rpb24gb2Zmc2V0ICh0b2tlbiwgc2VwYXJhdG9yKSB7XG4gICAgYWRkRm9ybWF0VG9rZW4odG9rZW4sIDAsIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG9mZnNldCA9IHRoaXMudXRjT2Zmc2V0KCk7XG4gICAgICAgIHZhciBzaWduID0gJysnO1xuICAgICAgICBpZiAob2Zmc2V0IDwgMCkge1xuICAgICAgICAgICAgb2Zmc2V0ID0gLW9mZnNldDtcbiAgICAgICAgICAgIHNpZ24gPSAnLSc7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNpZ24gKyB6ZXJvRmlsbCh+fihvZmZzZXQgLyA2MCksIDIpICsgc2VwYXJhdG9yICsgemVyb0ZpbGwofn4ob2Zmc2V0KSAlIDYwLCAyKTtcbiAgICB9KTtcbn1cblxub2Zmc2V0KCdaJywgJzonKTtcbm9mZnNldCgnWlonLCAnJyk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbignWicsICBtYXRjaFNob3J0T2Zmc2V0KTtcbmFkZFJlZ2V4VG9rZW4oJ1paJywgbWF0Y2hTaG9ydE9mZnNldCk7XG5hZGRQYXJzZVRva2VuKFsnWicsICdaWiddLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICBjb25maWcuX3VzZVVUQyA9IHRydWU7XG4gICAgY29uZmlnLl90em0gPSBvZmZzZXRGcm9tU3RyaW5nKG1hdGNoU2hvcnRPZmZzZXQsIGlucHV0KTtcbn0pO1xuXG4vLyBIRUxQRVJTXG5cbi8vIHRpbWV6b25lIGNodW5rZXJcbi8vICcrMTA6MDAnID4gWycxMCcsICAnMDAnXVxuLy8gJy0xNTMwJyAgPiBbJy0xNScsICczMCddXG52YXIgY2h1bmtPZmZzZXQgPSAvKFtcXCtcXC1dfFxcZFxcZCkvZ2k7XG5cbmZ1bmN0aW9uIG9mZnNldEZyb21TdHJpbmcobWF0Y2hlciwgc3RyaW5nKSB7XG4gICAgdmFyIG1hdGNoZXMgPSAoc3RyaW5nIHx8ICcnKS5tYXRjaChtYXRjaGVyKTtcblxuICAgIGlmIChtYXRjaGVzID09PSBudWxsKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBjaHVuayAgID0gbWF0Y2hlc1ttYXRjaGVzLmxlbmd0aCAtIDFdIHx8IFtdO1xuICAgIHZhciBwYXJ0cyAgID0gKGNodW5rICsgJycpLm1hdGNoKGNodW5rT2Zmc2V0KSB8fCBbJy0nLCAwLCAwXTtcbiAgICB2YXIgbWludXRlcyA9ICsocGFydHNbMV0gKiA2MCkgKyB0b0ludChwYXJ0c1syXSk7XG5cbiAgICByZXR1cm4gbWludXRlcyA9PT0gMCA/XG4gICAgICAwIDpcbiAgICAgIHBhcnRzWzBdID09PSAnKycgPyBtaW51dGVzIDogLW1pbnV0ZXM7XG59XG5cbi8vIFJldHVybiBhIG1vbWVudCBmcm9tIGlucHV0LCB0aGF0IGlzIGxvY2FsL3V0Yy96b25lIGVxdWl2YWxlbnQgdG8gbW9kZWwuXG5leHBvcnQgZnVuY3Rpb24gY2xvbmVXaXRoT2Zmc2V0KGlucHV0LCBtb2RlbCkge1xuICAgIHZhciByZXMsIGRpZmY7XG4gICAgaWYgKG1vZGVsLl9pc1VUQykge1xuICAgICAgICByZXMgPSBtb2RlbC5jbG9uZSgpO1xuICAgICAgICBkaWZmID0gKGlzTW9tZW50KGlucHV0KSB8fCBpc0RhdGUoaW5wdXQpID8gaW5wdXQudmFsdWVPZigpIDogY3JlYXRlTG9jYWwoaW5wdXQpLnZhbHVlT2YoKSkgLSByZXMudmFsdWVPZigpO1xuICAgICAgICAvLyBVc2UgbG93LWxldmVsIGFwaSwgYmVjYXVzZSB0aGlzIGZuIGlzIGxvdy1sZXZlbCBhcGkuXG4gICAgICAgIHJlcy5fZC5zZXRUaW1lKHJlcy5fZC52YWx1ZU9mKCkgKyBkaWZmKTtcbiAgICAgICAgaG9va3MudXBkYXRlT2Zmc2V0KHJlcywgZmFsc2UpO1xuICAgICAgICByZXR1cm4gcmVzO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBjcmVhdGVMb2NhbChpbnB1dCkubG9jYWwoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGdldERhdGVPZmZzZXQgKG0pIHtcbiAgICAvLyBPbiBGaXJlZm94LjI0IERhdGUjZ2V0VGltZXpvbmVPZmZzZXQgcmV0dXJucyBhIGZsb2F0aW5nIHBvaW50LlxuICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tb21lbnQvbW9tZW50L3B1bGwvMTg3MVxuICAgIHJldHVybiAtTWF0aC5yb3VuZChtLl9kLmdldFRpbWV6b25lT2Zmc2V0KCkgLyAxNSkgKiAxNTtcbn1cblxuLy8gSE9PS1NcblxuLy8gVGhpcyBmdW5jdGlvbiB3aWxsIGJlIGNhbGxlZCB3aGVuZXZlciBhIG1vbWVudCBpcyBtdXRhdGVkLlxuLy8gSXQgaXMgaW50ZW5kZWQgdG8ga2VlcCB0aGUgb2Zmc2V0IGluIHN5bmMgd2l0aCB0aGUgdGltZXpvbmUuXG5ob29rcy51cGRhdGVPZmZzZXQgPSBmdW5jdGlvbiAoKSB7fTtcblxuLy8gTU9NRU5UU1xuXG4vLyBrZWVwTG9jYWxUaW1lID0gdHJ1ZSBtZWFucyBvbmx5IGNoYW5nZSB0aGUgdGltZXpvbmUsIHdpdGhvdXRcbi8vIGFmZmVjdGluZyB0aGUgbG9jYWwgaG91ci4gU28gNTozMToyNiArMDMwMCAtLVt1dGNPZmZzZXQoMiwgdHJ1ZSldLS0+XG4vLyA1OjMxOjI2ICswMjAwIEl0IGlzIHBvc3NpYmxlIHRoYXQgNTozMToyNiBkb2Vzbid0IGV4aXN0IHdpdGggb2Zmc2V0XG4vLyArMDIwMCwgc28gd2UgYWRqdXN0IHRoZSB0aW1lIGFzIG5lZWRlZCwgdG8gYmUgdmFsaWQuXG4vL1xuLy8gS2VlcGluZyB0aGUgdGltZSBhY3R1YWxseSBhZGRzL3N1YnRyYWN0cyAob25lIGhvdXIpXG4vLyBmcm9tIHRoZSBhY3R1YWwgcmVwcmVzZW50ZWQgdGltZS4gVGhhdCBpcyB3aHkgd2UgY2FsbCB1cGRhdGVPZmZzZXRcbi8vIGEgc2Vjb25kIHRpbWUuIEluIGNhc2UgaXQgd2FudHMgdXMgdG8gY2hhbmdlIHRoZSBvZmZzZXQgYWdhaW5cbi8vIF9jaGFuZ2VJblByb2dyZXNzID09IHRydWUgY2FzZSwgdGhlbiB3ZSBoYXZlIHRvIGFkanVzdCwgYmVjYXVzZVxuLy8gdGhlcmUgaXMgbm8gc3VjaCB0aW1lIGluIHRoZSBnaXZlbiB0aW1lem9uZS5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTZXRPZmZzZXQgKGlucHV0LCBrZWVwTG9jYWxUaW1lKSB7XG4gICAgdmFyIG9mZnNldCA9IHRoaXMuX29mZnNldCB8fCAwLFxuICAgICAgICBsb2NhbEFkanVzdDtcbiAgICBpZiAoIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgIHJldHVybiBpbnB1dCAhPSBudWxsID8gdGhpcyA6IE5hTjtcbiAgICB9XG4gICAgaWYgKGlucHV0ICE9IG51bGwpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBpbnB1dCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGlucHV0ID0gb2Zmc2V0RnJvbVN0cmluZyhtYXRjaFNob3J0T2Zmc2V0LCBpbnB1dCk7XG4gICAgICAgICAgICBpZiAoaW5wdXQgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChNYXRoLmFicyhpbnB1dCkgPCAxNikge1xuICAgICAgICAgICAgaW5wdXQgPSBpbnB1dCAqIDYwO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5faXNVVEMgJiYga2VlcExvY2FsVGltZSkge1xuICAgICAgICAgICAgbG9jYWxBZGp1c3QgPSBnZXREYXRlT2Zmc2V0KHRoaXMpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX29mZnNldCA9IGlucHV0O1xuICAgICAgICB0aGlzLl9pc1VUQyA9IHRydWU7XG4gICAgICAgIGlmIChsb2NhbEFkanVzdCAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmFkZChsb2NhbEFkanVzdCwgJ20nKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAob2Zmc2V0ICE9PSBpbnB1dCkge1xuICAgICAgICAgICAgaWYgKCFrZWVwTG9jYWxUaW1lIHx8IHRoaXMuX2NoYW5nZUluUHJvZ3Jlc3MpIHtcbiAgICAgICAgICAgICAgICBhZGRTdWJ0cmFjdCh0aGlzLCBjcmVhdGVEdXJhdGlvbihpbnB1dCAtIG9mZnNldCwgJ20nKSwgMSwgZmFsc2UpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICghdGhpcy5fY2hhbmdlSW5Qcm9ncmVzcykge1xuICAgICAgICAgICAgICAgIHRoaXMuX2NoYW5nZUluUHJvZ3Jlc3MgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGhvb2tzLnVwZGF0ZU9mZnNldCh0aGlzLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB0aGlzLl9jaGFuZ2VJblByb2dyZXNzID0gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNVVEMgPyBvZmZzZXQgOiBnZXREYXRlT2Zmc2V0KHRoaXMpO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFNldFpvbmUgKGlucHV0LCBrZWVwTG9jYWxUaW1lKSB7XG4gICAgaWYgKGlucHV0ICE9IG51bGwpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBpbnB1dCAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGlucHV0ID0gLWlucHV0O1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy51dGNPZmZzZXQoaW5wdXQsIGtlZXBMb2NhbFRpbWUpO1xuXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAtdGhpcy51dGNPZmZzZXQoKTtcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRPZmZzZXRUb1VUQyAoa2VlcExvY2FsVGltZSkge1xuICAgIHJldHVybiB0aGlzLnV0Y09mZnNldCgwLCBrZWVwTG9jYWxUaW1lKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldE9mZnNldFRvTG9jYWwgKGtlZXBMb2NhbFRpbWUpIHtcbiAgICBpZiAodGhpcy5faXNVVEMpIHtcbiAgICAgICAgdGhpcy51dGNPZmZzZXQoMCwga2VlcExvY2FsVGltZSk7XG4gICAgICAgIHRoaXMuX2lzVVRDID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKGtlZXBMb2NhbFRpbWUpIHtcbiAgICAgICAgICAgIHRoaXMuc3VidHJhY3QoZ2V0RGF0ZU9mZnNldCh0aGlzKSwgJ20nKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNldE9mZnNldFRvUGFyc2VkT2Zmc2V0ICgpIHtcbiAgICBpZiAodGhpcy5fdHptICE9IG51bGwpIHtcbiAgICAgICAgdGhpcy51dGNPZmZzZXQodGhpcy5fdHptKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB0aGlzLl9pID09PSAnc3RyaW5nJykge1xuICAgICAgICB2YXIgdFpvbmUgPSBvZmZzZXRGcm9tU3RyaW5nKG1hdGNoT2Zmc2V0LCB0aGlzLl9pKTtcbiAgICAgICAgaWYgKHRab25lICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMudXRjT2Zmc2V0KHRab25lKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMudXRjT2Zmc2V0KDAsIHRydWUpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiB0aGlzO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFzQWxpZ25lZEhvdXJPZmZzZXQgKGlucHV0KSB7XG4gICAgaWYgKCF0aGlzLmlzVmFsaWQoKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGlucHV0ID0gaW5wdXQgPyBjcmVhdGVMb2NhbChpbnB1dCkudXRjT2Zmc2V0KCkgOiAwO1xuXG4gICAgcmV0dXJuICh0aGlzLnV0Y09mZnNldCgpIC0gaW5wdXQpICUgNjAgPT09IDA7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc0RheWxpZ2h0U2F2aW5nVGltZSAoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgICAgdGhpcy51dGNPZmZzZXQoKSA+IHRoaXMuY2xvbmUoKS5tb250aCgwKS51dGNPZmZzZXQoKSB8fFxuICAgICAgICB0aGlzLnV0Y09mZnNldCgpID4gdGhpcy5jbG9uZSgpLm1vbnRoKDUpLnV0Y09mZnNldCgpXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzRGF5bGlnaHRTYXZpbmdUaW1lU2hpZnRlZCAoKSB7XG4gICAgaWYgKCFpc1VuZGVmaW5lZCh0aGlzLl9pc0RTVFNoaWZ0ZWQpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0RTVFNoaWZ0ZWQ7XG4gICAgfVxuXG4gICAgdmFyIGMgPSB7fTtcblxuICAgIGNvcHlDb25maWcoYywgdGhpcyk7XG4gICAgYyA9IHByZXBhcmVDb25maWcoYyk7XG5cbiAgICBpZiAoYy5fYSkge1xuICAgICAgICB2YXIgb3RoZXIgPSBjLl9pc1VUQyA/IGNyZWF0ZVVUQyhjLl9hKSA6IGNyZWF0ZUxvY2FsKGMuX2EpO1xuICAgICAgICB0aGlzLl9pc0RTVFNoaWZ0ZWQgPSB0aGlzLmlzVmFsaWQoKSAmJlxuICAgICAgICAgICAgY29tcGFyZUFycmF5cyhjLl9hLCBvdGhlci50b0FycmF5KCkpID4gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLl9pc0RTVFNoaWZ0ZWQgPSBmYWxzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5faXNEU1RTaGlmdGVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNMb2NhbCAoKSB7XG4gICAgcmV0dXJuIHRoaXMuaXNWYWxpZCgpID8gIXRoaXMuX2lzVVRDIDogZmFsc2U7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1V0Y09mZnNldCAoKSB7XG4gICAgcmV0dXJuIHRoaXMuaXNWYWxpZCgpID8gdGhpcy5faXNVVEMgOiBmYWxzZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzVXRjICgpIHtcbiAgICByZXR1cm4gdGhpcy5pc1ZhbGlkKCkgPyB0aGlzLl9pc1VUQyAmJiB0aGlzLl9vZmZzZXQgPT09IDAgOiBmYWxzZTtcbn1cbiIsImltcG9ydCB7IER1cmF0aW9uLCBpc0R1cmF0aW9uIH0gZnJvbSAnLi9jb25zdHJ1Y3Rvcic7XG5pbXBvcnQgaXNOdW1iZXIgZnJvbSAnLi4vdXRpbHMvaXMtbnVtYmVyJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuaW1wb3J0IGFic1JvdW5kIGZyb20gJy4uL3V0aWxzL2Ficy1yb3VuZCc7XG5pbXBvcnQgaGFzT3duUHJvcCBmcm9tICcuLi91dGlscy9oYXMtb3duLXByb3AnO1xuaW1wb3J0IHsgREFURSwgSE9VUiwgTUlOVVRFLCBTRUNPTkQsIE1JTExJU0VDT05EIH0gZnJvbSAnLi4vdW5pdHMvY29uc3RhbnRzJztcbmltcG9ydCB7IGNsb25lV2l0aE9mZnNldCB9IGZyb20gJy4uL3VuaXRzL29mZnNldCc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5cbi8vIEFTUC5ORVQganNvbiBkYXRlIGZvcm1hdCByZWdleFxudmFyIGFzcE5ldFJlZ2V4ID0gL14oXFwtKT8oPzooXFxkKilbLiBdKT8oXFxkKylcXDooXFxkKykoPzpcXDooXFxkKykoXFwuXFxkKik/KT8kLztcblxuLy8gZnJvbSBodHRwOi8vZG9jcy5jbG9zdXJlLWxpYnJhcnkuZ29vZ2xlY29kZS5jb20vZ2l0L2Nsb3N1cmVfZ29vZ19kYXRlX2RhdGUuanMuc291cmNlLmh0bWxcbi8vIHNvbWV3aGF0IG1vcmUgaW4gbGluZSB3aXRoIDQuNC4zLjIgMjAwNCBzcGVjLCBidXQgYWxsb3dzIGRlY2ltYWwgYW55d2hlcmVcbi8vIGFuZCBmdXJ0aGVyIG1vZGlmaWVkIHRvIGFsbG93IGZvciBzdHJpbmdzIGNvbnRhaW5pbmcgYm90aCB3ZWVrIGFuZCBkYXlcbnZhciBpc29SZWdleCA9IC9eKC0pP1AoPzooLT9bMC05LC5dKilZKT8oPzooLT9bMC05LC5dKilNKT8oPzooLT9bMC05LC5dKilXKT8oPzooLT9bMC05LC5dKilEKT8oPzpUKD86KC0/WzAtOSwuXSopSCk/KD86KC0/WzAtOSwuXSopTSk/KD86KC0/WzAtOSwuXSopUyk/KT8kLztcblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUR1cmF0aW9uIChpbnB1dCwga2V5KSB7XG4gICAgdmFyIGR1cmF0aW9uID0gaW5wdXQsXG4gICAgICAgIC8vIG1hdGNoaW5nIGFnYWluc3QgcmVnZXhwIGlzIGV4cGVuc2l2ZSwgZG8gaXQgb24gZGVtYW5kXG4gICAgICAgIG1hdGNoID0gbnVsbCxcbiAgICAgICAgc2lnbixcbiAgICAgICAgcmV0LFxuICAgICAgICBkaWZmUmVzO1xuXG4gICAgaWYgKGlzRHVyYXRpb24oaW5wdXQpKSB7XG4gICAgICAgIGR1cmF0aW9uID0ge1xuICAgICAgICAgICAgbXMgOiBpbnB1dC5fbWlsbGlzZWNvbmRzLFxuICAgICAgICAgICAgZCAgOiBpbnB1dC5fZGF5cyxcbiAgICAgICAgICAgIE0gIDogaW5wdXQuX21vbnRoc1xuICAgICAgICB9O1xuICAgIH0gZWxzZSBpZiAoaXNOdW1iZXIoaW5wdXQpKSB7XG4gICAgICAgIGR1cmF0aW9uID0ge307XG4gICAgICAgIGlmIChrZXkpIHtcbiAgICAgICAgICAgIGR1cmF0aW9uW2tleV0gPSBpbnB1dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGR1cmF0aW9uLm1pbGxpc2Vjb25kcyA9IGlucHV0O1xuICAgICAgICB9XG4gICAgfSBlbHNlIGlmICghIShtYXRjaCA9IGFzcE5ldFJlZ2V4LmV4ZWMoaW5wdXQpKSkge1xuICAgICAgICBzaWduID0gKG1hdGNoWzFdID09PSAnLScpID8gLTEgOiAxO1xuICAgICAgICBkdXJhdGlvbiA9IHtcbiAgICAgICAgICAgIHkgIDogMCxcbiAgICAgICAgICAgIGQgIDogdG9JbnQobWF0Y2hbREFURV0pICAgICAgICAgICAgICAgICAgICAgICAgICogc2lnbixcbiAgICAgICAgICAgIGggIDogdG9JbnQobWF0Y2hbSE9VUl0pICAgICAgICAgICAgICAgICAgICAgICAgICogc2lnbixcbiAgICAgICAgICAgIG0gIDogdG9JbnQobWF0Y2hbTUlOVVRFXSkgICAgICAgICAgICAgICAgICAgICAgICogc2lnbixcbiAgICAgICAgICAgIHMgIDogdG9JbnQobWF0Y2hbU0VDT05EXSkgICAgICAgICAgICAgICAgICAgICAgICogc2lnbixcbiAgICAgICAgICAgIG1zIDogdG9JbnQoYWJzUm91bmQobWF0Y2hbTUlMTElTRUNPTkRdICogMTAwMCkpICogc2lnbiAvLyB0aGUgbWlsbGlzZWNvbmQgZGVjaW1hbCBwb2ludCBpcyBpbmNsdWRlZCBpbiB0aGUgbWF0Y2hcbiAgICAgICAgfTtcbiAgICB9IGVsc2UgaWYgKCEhKG1hdGNoID0gaXNvUmVnZXguZXhlYyhpbnB1dCkpKSB7XG4gICAgICAgIHNpZ24gPSAobWF0Y2hbMV0gPT09ICctJykgPyAtMSA6IDE7XG4gICAgICAgIGR1cmF0aW9uID0ge1xuICAgICAgICAgICAgeSA6IHBhcnNlSXNvKG1hdGNoWzJdLCBzaWduKSxcbiAgICAgICAgICAgIE0gOiBwYXJzZUlzbyhtYXRjaFszXSwgc2lnbiksXG4gICAgICAgICAgICB3IDogcGFyc2VJc28obWF0Y2hbNF0sIHNpZ24pLFxuICAgICAgICAgICAgZCA6IHBhcnNlSXNvKG1hdGNoWzVdLCBzaWduKSxcbiAgICAgICAgICAgIGggOiBwYXJzZUlzbyhtYXRjaFs2XSwgc2lnbiksXG4gICAgICAgICAgICBtIDogcGFyc2VJc28obWF0Y2hbN10sIHNpZ24pLFxuICAgICAgICAgICAgcyA6IHBhcnNlSXNvKG1hdGNoWzhdLCBzaWduKVxuICAgICAgICB9O1xuICAgIH0gZWxzZSBpZiAoZHVyYXRpb24gPT0gbnVsbCkgey8vIGNoZWNrcyBmb3IgbnVsbCBvciB1bmRlZmluZWRcbiAgICAgICAgZHVyYXRpb24gPSB7fTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBkdXJhdGlvbiA9PT0gJ29iamVjdCcgJiYgKCdmcm9tJyBpbiBkdXJhdGlvbiB8fCAndG8nIGluIGR1cmF0aW9uKSkge1xuICAgICAgICBkaWZmUmVzID0gbW9tZW50c0RpZmZlcmVuY2UoY3JlYXRlTG9jYWwoZHVyYXRpb24uZnJvbSksIGNyZWF0ZUxvY2FsKGR1cmF0aW9uLnRvKSk7XG5cbiAgICAgICAgZHVyYXRpb24gPSB7fTtcbiAgICAgICAgZHVyYXRpb24ubXMgPSBkaWZmUmVzLm1pbGxpc2Vjb25kcztcbiAgICAgICAgZHVyYXRpb24uTSA9IGRpZmZSZXMubW9udGhzO1xuICAgIH1cblxuICAgIHJldCA9IG5ldyBEdXJhdGlvbihkdXJhdGlvbik7XG5cbiAgICBpZiAoaXNEdXJhdGlvbihpbnB1dCkgJiYgaGFzT3duUHJvcChpbnB1dCwgJ19sb2NhbGUnKSkge1xuICAgICAgICByZXQuX2xvY2FsZSA9IGlucHV0Ll9sb2NhbGU7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJldDtcbn1cblxuY3JlYXRlRHVyYXRpb24uZm4gPSBEdXJhdGlvbi5wcm90b3R5cGU7XG5cbmZ1bmN0aW9uIHBhcnNlSXNvIChpbnAsIHNpZ24pIHtcbiAgICAvLyBXZSdkIG5vcm1hbGx5IHVzZSB+fmlucCBmb3IgdGhpcywgYnV0IHVuZm9ydHVuYXRlbHkgaXQgYWxzb1xuICAgIC8vIGNvbnZlcnRzIGZsb2F0cyB0byBpbnRzLlxuICAgIC8vIGlucCBtYXkgYmUgdW5kZWZpbmVkLCBzbyBjYXJlZnVsIGNhbGxpbmcgcmVwbGFjZSBvbiBpdC5cbiAgICB2YXIgcmVzID0gaW5wICYmIHBhcnNlRmxvYXQoaW5wLnJlcGxhY2UoJywnLCAnLicpKTtcbiAgICAvLyBhcHBseSBzaWduIHdoaWxlIHdlJ3JlIGF0IGl0XG4gICAgcmV0dXJuIChpc05hTihyZXMpID8gMCA6IHJlcykgKiBzaWduO1xufVxuXG5mdW5jdGlvbiBwb3NpdGl2ZU1vbWVudHNEaWZmZXJlbmNlKGJhc2UsIG90aGVyKSB7XG4gICAgdmFyIHJlcyA9IHttaWxsaXNlY29uZHM6IDAsIG1vbnRoczogMH07XG5cbiAgICByZXMubW9udGhzID0gb3RoZXIubW9udGgoKSAtIGJhc2UubW9udGgoKSArXG4gICAgICAgIChvdGhlci55ZWFyKCkgLSBiYXNlLnllYXIoKSkgKiAxMjtcbiAgICBpZiAoYmFzZS5jbG9uZSgpLmFkZChyZXMubW9udGhzLCAnTScpLmlzQWZ0ZXIob3RoZXIpKSB7XG4gICAgICAgIC0tcmVzLm1vbnRocztcbiAgICB9XG5cbiAgICByZXMubWlsbGlzZWNvbmRzID0gK290aGVyIC0gKyhiYXNlLmNsb25lKCkuYWRkKHJlcy5tb250aHMsICdNJykpO1xuXG4gICAgcmV0dXJuIHJlcztcbn1cblxuZnVuY3Rpb24gbW9tZW50c0RpZmZlcmVuY2UoYmFzZSwgb3RoZXIpIHtcbiAgICB2YXIgcmVzO1xuICAgIGlmICghKGJhc2UuaXNWYWxpZCgpICYmIG90aGVyLmlzVmFsaWQoKSkpIHtcbiAgICAgICAgcmV0dXJuIHttaWxsaXNlY29uZHM6IDAsIG1vbnRoczogMH07XG4gICAgfVxuXG4gICAgb3RoZXIgPSBjbG9uZVdpdGhPZmZzZXQob3RoZXIsIGJhc2UpO1xuICAgIGlmIChiYXNlLmlzQmVmb3JlKG90aGVyKSkge1xuICAgICAgICByZXMgPSBwb3NpdGl2ZU1vbWVudHNEaWZmZXJlbmNlKGJhc2UsIG90aGVyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXMgPSBwb3NpdGl2ZU1vbWVudHNEaWZmZXJlbmNlKG90aGVyLCBiYXNlKTtcbiAgICAgICAgcmVzLm1pbGxpc2Vjb25kcyA9IC1yZXMubWlsbGlzZWNvbmRzO1xuICAgICAgICByZXMubW9udGhzID0gLXJlcy5tb250aHM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlcztcbn1cbiIsImltcG9ydCB7IGdldCwgc2V0IH0gZnJvbSAnLi9nZXQtc2V0JztcbmltcG9ydCB7IHNldE1vbnRoIH0gZnJvbSAnLi4vdW5pdHMvbW9udGgnO1xuaW1wb3J0IHsgY3JlYXRlRHVyYXRpb24gfSBmcm9tICcuLi9kdXJhdGlvbi9jcmVhdGUnO1xuaW1wb3J0IHsgZGVwcmVjYXRlU2ltcGxlIH0gZnJvbSAnLi4vdXRpbHMvZGVwcmVjYXRlJztcbmltcG9ydCB7IGhvb2tzIH0gZnJvbSAnLi4vdXRpbHMvaG9va3MnO1xuaW1wb3J0IGFic1JvdW5kIGZyb20gJy4uL3V0aWxzL2Ficy1yb3VuZCc7XG5cblxuLy8gVE9ETzogcmVtb3ZlICduYW1lJyBhcmcgYWZ0ZXIgZGVwcmVjYXRpb24gaXMgcmVtb3ZlZFxuZnVuY3Rpb24gY3JlYXRlQWRkZXIoZGlyZWN0aW9uLCBuYW1lKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh2YWwsIHBlcmlvZCkge1xuICAgICAgICB2YXIgZHVyLCB0bXA7XG4gICAgICAgIC8vaW52ZXJ0IHRoZSBhcmd1bWVudHMsIGJ1dCBjb21wbGFpbiBhYm91dCBpdFxuICAgICAgICBpZiAocGVyaW9kICE9PSBudWxsICYmICFpc05hTigrcGVyaW9kKSkge1xuICAgICAgICAgICAgZGVwcmVjYXRlU2ltcGxlKG5hbWUsICdtb21lbnQoKS4nICsgbmFtZSAgKyAnKHBlcmlvZCwgbnVtYmVyKSBpcyBkZXByZWNhdGVkLiBQbGVhc2UgdXNlIG1vbWVudCgpLicgKyBuYW1lICsgJyhudW1iZXIsIHBlcmlvZCkuICcgK1xuICAgICAgICAgICAgJ1NlZSBodHRwOi8vbW9tZW50anMuY29tL2d1aWRlcy8jL3dhcm5pbmdzL2FkZC1pbnZlcnRlZC1wYXJhbS8gZm9yIG1vcmUgaW5mby4nKTtcbiAgICAgICAgICAgIHRtcCA9IHZhbDsgdmFsID0gcGVyaW9kOyBwZXJpb2QgPSB0bXA7XG4gICAgICAgIH1cblxuICAgICAgICB2YWwgPSB0eXBlb2YgdmFsID09PSAnc3RyaW5nJyA/ICt2YWwgOiB2YWw7XG4gICAgICAgIGR1ciA9IGNyZWF0ZUR1cmF0aW9uKHZhbCwgcGVyaW9kKTtcbiAgICAgICAgYWRkU3VidHJhY3QodGhpcywgZHVyLCBkaXJlY3Rpb24pO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gYWRkU3VidHJhY3QgKG1vbSwgZHVyYXRpb24sIGlzQWRkaW5nLCB1cGRhdGVPZmZzZXQpIHtcbiAgICB2YXIgbWlsbGlzZWNvbmRzID0gZHVyYXRpb24uX21pbGxpc2Vjb25kcyxcbiAgICAgICAgZGF5cyA9IGFic1JvdW5kKGR1cmF0aW9uLl9kYXlzKSxcbiAgICAgICAgbW9udGhzID0gYWJzUm91bmQoZHVyYXRpb24uX21vbnRocyk7XG5cbiAgICBpZiAoIW1vbS5pc1ZhbGlkKCkpIHtcbiAgICAgICAgLy8gTm8gb3BcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHVwZGF0ZU9mZnNldCA9IHVwZGF0ZU9mZnNldCA9PSBudWxsID8gdHJ1ZSA6IHVwZGF0ZU9mZnNldDtcblxuICAgIGlmIChtaWxsaXNlY29uZHMpIHtcbiAgICAgICAgbW9tLl9kLnNldFRpbWUobW9tLl9kLnZhbHVlT2YoKSArIG1pbGxpc2Vjb25kcyAqIGlzQWRkaW5nKTtcbiAgICB9XG4gICAgaWYgKGRheXMpIHtcbiAgICAgICAgc2V0KG1vbSwgJ0RhdGUnLCBnZXQobW9tLCAnRGF0ZScpICsgZGF5cyAqIGlzQWRkaW5nKTtcbiAgICB9XG4gICAgaWYgKG1vbnRocykge1xuICAgICAgICBzZXRNb250aChtb20sIGdldChtb20sICdNb250aCcpICsgbW9udGhzICogaXNBZGRpbmcpO1xuICAgIH1cbiAgICBpZiAodXBkYXRlT2Zmc2V0KSB7XG4gICAgICAgIGhvb2tzLnVwZGF0ZU9mZnNldChtb20sIGRheXMgfHwgbW9udGhzKTtcbiAgICB9XG59XG5cbmV4cG9ydCB2YXIgYWRkICAgICAgPSBjcmVhdGVBZGRlcigxLCAnYWRkJyk7XG5leHBvcnQgdmFyIHN1YnRyYWN0ID0gY3JlYXRlQWRkZXIoLTEsICdzdWJ0cmFjdCcpO1xuXG4iLCJpbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyBjbG9uZVdpdGhPZmZzZXQgfSBmcm9tICcuLi91bml0cy9vZmZzZXQnO1xuaW1wb3J0IGlzRnVuY3Rpb24gZnJvbSAnLi4vdXRpbHMvaXMtZnVuY3Rpb24nO1xuaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDYWxlbmRhckZvcm1hdChteU1vbWVudCwgbm93KSB7XG4gICAgdmFyIGRpZmYgPSBteU1vbWVudC5kaWZmKG5vdywgJ2RheXMnLCB0cnVlKTtcbiAgICByZXR1cm4gZGlmZiA8IC02ID8gJ3NhbWVFbHNlJyA6XG4gICAgICAgICAgICBkaWZmIDwgLTEgPyAnbGFzdFdlZWsnIDpcbiAgICAgICAgICAgIGRpZmYgPCAwID8gJ2xhc3REYXknIDpcbiAgICAgICAgICAgIGRpZmYgPCAxID8gJ3NhbWVEYXknIDpcbiAgICAgICAgICAgIGRpZmYgPCAyID8gJ25leHREYXknIDpcbiAgICAgICAgICAgIGRpZmYgPCA3ID8gJ25leHRXZWVrJyA6ICdzYW1lRWxzZSc7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjYWxlbmRhciAodGltZSwgZm9ybWF0cykge1xuICAgIC8vIFdlIHdhbnQgdG8gY29tcGFyZSB0aGUgc3RhcnQgb2YgdG9kYXksIHZzIHRoaXMuXG4gICAgLy8gR2V0dGluZyBzdGFydC1vZi10b2RheSBkZXBlbmRzIG9uIHdoZXRoZXIgd2UncmUgbG9jYWwvdXRjL29mZnNldCBvciBub3QuXG4gICAgdmFyIG5vdyA9IHRpbWUgfHwgY3JlYXRlTG9jYWwoKSxcbiAgICAgICAgc29kID0gY2xvbmVXaXRoT2Zmc2V0KG5vdywgdGhpcykuc3RhcnRPZignZGF5JyksXG4gICAgICAgIGZvcm1hdCA9IGhvb2tzLmNhbGVuZGFyRm9ybWF0KHRoaXMsIHNvZCkgfHwgJ3NhbWVFbHNlJztcblxuICAgIHZhciBvdXRwdXQgPSBmb3JtYXRzICYmIChpc0Z1bmN0aW9uKGZvcm1hdHNbZm9ybWF0XSkgPyBmb3JtYXRzW2Zvcm1hdF0uY2FsbCh0aGlzLCBub3cpIDogZm9ybWF0c1tmb3JtYXRdKTtcblxuICAgIHJldHVybiB0aGlzLmZvcm1hdChvdXRwdXQgfHwgdGhpcy5sb2NhbGVEYXRhKCkuY2FsZW5kYXIoZm9ybWF0LCB0aGlzLCBjcmVhdGVMb2NhbChub3cpKSk7XG59XG4iLCJpbXBvcnQgeyBNb21lbnQgfSBmcm9tICcuL2NvbnN0cnVjdG9yJztcblxuZXhwb3J0IGZ1bmN0aW9uIGNsb25lICgpIHtcbiAgICByZXR1cm4gbmV3IE1vbWVudCh0aGlzKTtcbn1cbiIsImltcG9ydCB7IGlzTW9tZW50IH0gZnJvbSAnLi9jb25zdHJ1Y3Rvcic7XG5pbXBvcnQgeyBub3JtYWxpemVVbml0cyB9IGZyb20gJy4uL3VuaXRzL2FsaWFzZXMnO1xuaW1wb3J0IHsgY3JlYXRlTG9jYWwgfSBmcm9tICcuLi9jcmVhdGUvbG9jYWwnO1xuaW1wb3J0IGlzVW5kZWZpbmVkIGZyb20gJy4uL3V0aWxzL2lzLXVuZGVmaW5lZCc7XG5cbmV4cG9ydCBmdW5jdGlvbiBpc0FmdGVyIChpbnB1dCwgdW5pdHMpIHtcbiAgICB2YXIgbG9jYWxJbnB1dCA9IGlzTW9tZW50KGlucHV0KSA/IGlucHV0IDogY3JlYXRlTG9jYWwoaW5wdXQpO1xuICAgIGlmICghKHRoaXMuaXNWYWxpZCgpICYmIGxvY2FsSW5wdXQuaXNWYWxpZCgpKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHVuaXRzID0gbm9ybWFsaXplVW5pdHMoIWlzVW5kZWZpbmVkKHVuaXRzKSA/IHVuaXRzIDogJ21pbGxpc2Vjb25kJyk7XG4gICAgaWYgKHVuaXRzID09PSAnbWlsbGlzZWNvbmQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZhbHVlT2YoKSA+IGxvY2FsSW5wdXQudmFsdWVPZigpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBsb2NhbElucHV0LnZhbHVlT2YoKSA8IHRoaXMuY2xvbmUoKS5zdGFydE9mKHVuaXRzKS52YWx1ZU9mKCk7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNCZWZvcmUgKGlucHV0LCB1bml0cykge1xuICAgIHZhciBsb2NhbElucHV0ID0gaXNNb21lbnQoaW5wdXQpID8gaW5wdXQgOiBjcmVhdGVMb2NhbChpbnB1dCk7XG4gICAgaWYgKCEodGhpcy5pc1ZhbGlkKCkgJiYgbG9jYWxJbnB1dC5pc1ZhbGlkKCkpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyghaXNVbmRlZmluZWQodW5pdHMpID8gdW5pdHMgOiAnbWlsbGlzZWNvbmQnKTtcbiAgICBpZiAodW5pdHMgPT09ICdtaWxsaXNlY29uZCcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWVPZigpIDwgbG9jYWxJbnB1dC52YWx1ZU9mKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2xvbmUoKS5lbmRPZih1bml0cykudmFsdWVPZigpIDwgbG9jYWxJbnB1dC52YWx1ZU9mKCk7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNCZXR3ZWVuIChmcm9tLCB0bywgdW5pdHMsIGluY2x1c2l2aXR5KSB7XG4gICAgaW5jbHVzaXZpdHkgPSBpbmNsdXNpdml0eSB8fCAnKCknO1xuICAgIHJldHVybiAoaW5jbHVzaXZpdHlbMF0gPT09ICcoJyA/IHRoaXMuaXNBZnRlcihmcm9tLCB1bml0cykgOiAhdGhpcy5pc0JlZm9yZShmcm9tLCB1bml0cykpICYmXG4gICAgICAgIChpbmNsdXNpdml0eVsxXSA9PT0gJyknID8gdGhpcy5pc0JlZm9yZSh0bywgdW5pdHMpIDogIXRoaXMuaXNBZnRlcih0bywgdW5pdHMpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzU2FtZSAoaW5wdXQsIHVuaXRzKSB7XG4gICAgdmFyIGxvY2FsSW5wdXQgPSBpc01vbWVudChpbnB1dCkgPyBpbnB1dCA6IGNyZWF0ZUxvY2FsKGlucHV0KSxcbiAgICAgICAgaW5wdXRNcztcbiAgICBpZiAoISh0aGlzLmlzVmFsaWQoKSAmJiBsb2NhbElucHV0LmlzVmFsaWQoKSkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICB1bml0cyA9IG5vcm1hbGl6ZVVuaXRzKHVuaXRzIHx8ICdtaWxsaXNlY29uZCcpO1xuICAgIGlmICh1bml0cyA9PT0gJ21pbGxpc2Vjb25kJykge1xuICAgICAgICByZXR1cm4gdGhpcy52YWx1ZU9mKCkgPT09IGxvY2FsSW5wdXQudmFsdWVPZigpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGlucHV0TXMgPSBsb2NhbElucHV0LnZhbHVlT2YoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2xvbmUoKS5zdGFydE9mKHVuaXRzKS52YWx1ZU9mKCkgPD0gaW5wdXRNcyAmJiBpbnB1dE1zIDw9IHRoaXMuY2xvbmUoKS5lbmRPZih1bml0cykudmFsdWVPZigpO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzU2FtZU9yQWZ0ZXIgKGlucHV0LCB1bml0cykge1xuICAgIHJldHVybiB0aGlzLmlzU2FtZShpbnB1dCwgdW5pdHMpIHx8IHRoaXMuaXNBZnRlcihpbnB1dCx1bml0cyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1NhbWVPckJlZm9yZSAoaW5wdXQsIHVuaXRzKSB7XG4gICAgcmV0dXJuIHRoaXMuaXNTYW1lKGlucHV0LCB1bml0cykgfHwgdGhpcy5pc0JlZm9yZShpbnB1dCx1bml0cyk7XG59XG4iLCJpbXBvcnQgYWJzRmxvb3IgZnJvbSAnLi4vdXRpbHMvYWJzLWZsb29yJztcbmltcG9ydCB7IGNsb25lV2l0aE9mZnNldCB9IGZyb20gJy4uL3VuaXRzL29mZnNldCc7XG5pbXBvcnQgeyBub3JtYWxpemVVbml0cyB9IGZyb20gJy4uL3VuaXRzL2FsaWFzZXMnO1xuXG5leHBvcnQgZnVuY3Rpb24gZGlmZiAoaW5wdXQsIHVuaXRzLCBhc0Zsb2F0KSB7XG4gICAgdmFyIHRoYXQsXG4gICAgICAgIHpvbmVEZWx0YSxcbiAgICAgICAgZGVsdGEsIG91dHB1dDtcblxuICAgIGlmICghdGhpcy5pc1ZhbGlkKCkpIHtcbiAgICAgICAgcmV0dXJuIE5hTjtcbiAgICB9XG5cbiAgICB0aGF0ID0gY2xvbmVXaXRoT2Zmc2V0KGlucHV0LCB0aGlzKTtcblxuICAgIGlmICghdGhhdC5pc1ZhbGlkKCkpIHtcbiAgICAgICAgcmV0dXJuIE5hTjtcbiAgICB9XG5cbiAgICB6b25lRGVsdGEgPSAodGhhdC51dGNPZmZzZXQoKSAtIHRoaXMudXRjT2Zmc2V0KCkpICogNmU0O1xuXG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG5cbiAgICBpZiAodW5pdHMgPT09ICd5ZWFyJyB8fCB1bml0cyA9PT0gJ21vbnRoJyB8fCB1bml0cyA9PT0gJ3F1YXJ0ZXInKSB7XG4gICAgICAgIG91dHB1dCA9IG1vbnRoRGlmZih0aGlzLCB0aGF0KTtcbiAgICAgICAgaWYgKHVuaXRzID09PSAncXVhcnRlcicpIHtcbiAgICAgICAgICAgIG91dHB1dCA9IG91dHB1dCAvIDM7XG4gICAgICAgIH0gZWxzZSBpZiAodW5pdHMgPT09ICd5ZWFyJykge1xuICAgICAgICAgICAgb3V0cHV0ID0gb3V0cHV0IC8gMTI7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICBkZWx0YSA9IHRoaXMgLSB0aGF0O1xuICAgICAgICBvdXRwdXQgPSB1bml0cyA9PT0gJ3NlY29uZCcgPyBkZWx0YSAvIDFlMyA6IC8vIDEwMDBcbiAgICAgICAgICAgIHVuaXRzID09PSAnbWludXRlJyA/IGRlbHRhIC8gNmU0IDogLy8gMTAwMCAqIDYwXG4gICAgICAgICAgICB1bml0cyA9PT0gJ2hvdXInID8gZGVsdGEgLyAzNmU1IDogLy8gMTAwMCAqIDYwICogNjBcbiAgICAgICAgICAgIHVuaXRzID09PSAnZGF5JyA/IChkZWx0YSAtIHpvbmVEZWx0YSkgLyA4NjRlNSA6IC8vIDEwMDAgKiA2MCAqIDYwICogMjQsIG5lZ2F0ZSBkc3RcbiAgICAgICAgICAgIHVuaXRzID09PSAnd2VlaycgPyAoZGVsdGEgLSB6b25lRGVsdGEpIC8gNjA0OGU1IDogLy8gMTAwMCAqIDYwICogNjAgKiAyNCAqIDcsIG5lZ2F0ZSBkc3RcbiAgICAgICAgICAgIGRlbHRhO1xuICAgIH1cbiAgICByZXR1cm4gYXNGbG9hdCA/IG91dHB1dCA6IGFic0Zsb29yKG91dHB1dCk7XG59XG5cbmZ1bmN0aW9uIG1vbnRoRGlmZiAoYSwgYikge1xuICAgIC8vIGRpZmZlcmVuY2UgaW4gbW9udGhzXG4gICAgdmFyIHdob2xlTW9udGhEaWZmID0gKChiLnllYXIoKSAtIGEueWVhcigpKSAqIDEyKSArIChiLm1vbnRoKCkgLSBhLm1vbnRoKCkpLFxuICAgICAgICAvLyBiIGlzIGluIChhbmNob3IgLSAxIG1vbnRoLCBhbmNob3IgKyAxIG1vbnRoKVxuICAgICAgICBhbmNob3IgPSBhLmNsb25lKCkuYWRkKHdob2xlTW9udGhEaWZmLCAnbW9udGhzJyksXG4gICAgICAgIGFuY2hvcjIsIGFkanVzdDtcblxuICAgIGlmIChiIC0gYW5jaG9yIDwgMCkge1xuICAgICAgICBhbmNob3IyID0gYS5jbG9uZSgpLmFkZCh3aG9sZU1vbnRoRGlmZiAtIDEsICdtb250aHMnKTtcbiAgICAgICAgLy8gbGluZWFyIGFjcm9zcyB0aGUgbW9udGhcbiAgICAgICAgYWRqdXN0ID0gKGIgLSBhbmNob3IpIC8gKGFuY2hvciAtIGFuY2hvcjIpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGFuY2hvcjIgPSBhLmNsb25lKCkuYWRkKHdob2xlTW9udGhEaWZmICsgMSwgJ21vbnRocycpO1xuICAgICAgICAvLyBsaW5lYXIgYWNyb3NzIHRoZSBtb250aFxuICAgICAgICBhZGp1c3QgPSAoYiAtIGFuY2hvcikgLyAoYW5jaG9yMiAtIGFuY2hvcik7XG4gICAgfVxuXG4gICAgLy9jaGVjayBmb3IgbmVnYXRpdmUgemVybywgcmV0dXJuIHplcm8gaWYgbmVnYXRpdmUgemVyb1xuICAgIHJldHVybiAtKHdob2xlTW9udGhEaWZmICsgYWRqdXN0KSB8fCAwO1xufVxuIiwiaW1wb3J0IHsgZm9ybWF0TW9tZW50IH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBob29rcyB9IGZyb20gJy4uL3V0aWxzL2hvb2tzJztcbmltcG9ydCBpc0Z1bmN0aW9uIGZyb20gJy4uL3V0aWxzL2lzLWZ1bmN0aW9uJztcblxuaG9va3MuZGVmYXVsdEZvcm1hdCA9ICdZWVlZLU1NLUREVEhIOm1tOnNzWic7XG5ob29rcy5kZWZhdWx0Rm9ybWF0VXRjID0gJ1lZWVktTU0tRERUSEg6bW06c3NbWl0nO1xuXG5leHBvcnQgZnVuY3Rpb24gdG9TdHJpbmcgKCkge1xuICAgIHJldHVybiB0aGlzLmNsb25lKCkubG9jYWxlKCdlbicpLmZvcm1hdCgnZGRkIE1NTSBERCBZWVlZIEhIOm1tOnNzIFtHTVRdWlonKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHRvSVNPU3RyaW5nICgpIHtcbiAgICB2YXIgbSA9IHRoaXMuY2xvbmUoKS51dGMoKTtcbiAgICBpZiAoMCA8IG0ueWVhcigpICYmIG0ueWVhcigpIDw9IDk5OTkpIHtcbiAgICAgICAgaWYgKGlzRnVuY3Rpb24oRGF0ZS5wcm90b3R5cGUudG9JU09TdHJpbmcpKSB7XG4gICAgICAgICAgICAvLyBuYXRpdmUgaW1wbGVtZW50YXRpb24gaXMgfjUweCBmYXN0ZXIsIHVzZSBpdCB3aGVuIHdlIGNhblxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudG9EYXRlKCkudG9JU09TdHJpbmcoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBmb3JtYXRNb21lbnQobSwgJ1lZWVktTU0tRERbVF1ISDptbTpzcy5TU1NbWl0nKTtcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBmb3JtYXRNb21lbnQobSwgJ1lZWVlZWS1NTS1ERFtUXUhIOm1tOnNzLlNTU1taXScpO1xuICAgIH1cbn1cblxuLyoqXG4gKiBSZXR1cm4gYSBodW1hbiByZWFkYWJsZSByZXByZXNlbnRhdGlvbiBvZiBhIG1vbWVudCB0aGF0IGNhblxuICogYWxzbyBiZSBldmFsdWF0ZWQgdG8gZ2V0IGEgbmV3IG1vbWVudCB3aGljaCBpcyB0aGUgc2FtZVxuICpcbiAqIEBsaW5rIGh0dHBzOi8vbm9kZWpzLm9yZy9kaXN0L2xhdGVzdC9kb2NzL2FwaS91dGlsLmh0bWwjdXRpbF9jdXN0b21faW5zcGVjdF9mdW5jdGlvbl9vbl9vYmplY3RzXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBpbnNwZWN0ICgpIHtcbiAgICBpZiAoIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgIHJldHVybiAnbW9tZW50LmludmFsaWQoLyogJyArIHRoaXMuX2kgKyAnICovKSc7XG4gICAgfVxuICAgIHZhciBmdW5jID0gJ21vbWVudCc7XG4gICAgdmFyIHpvbmUgPSAnJztcbiAgICBpZiAoIXRoaXMuaXNMb2NhbCgpKSB7XG4gICAgICAgIGZ1bmMgPSB0aGlzLnV0Y09mZnNldCgpID09PSAwID8gJ21vbWVudC51dGMnIDogJ21vbWVudC5wYXJzZVpvbmUnO1xuICAgICAgICB6b25lID0gJ1onO1xuICAgIH1cbiAgICB2YXIgcHJlZml4ID0gJ1snICsgZnVuYyArICcoXCJdJztcbiAgICB2YXIgeWVhciA9ICgwIDwgdGhpcy55ZWFyKCkgJiYgdGhpcy55ZWFyKCkgPD0gOTk5OSkgPyAnWVlZWScgOiAnWVlZWVlZJztcbiAgICB2YXIgZGF0ZXRpbWUgPSAnLU1NLUREW1RdSEg6bW06c3MuU1NTJztcbiAgICB2YXIgc3VmZml4ID0gem9uZSArICdbXCIpXSc7XG5cbiAgICByZXR1cm4gdGhpcy5mb3JtYXQocHJlZml4ICsgeWVhciArIGRhdGV0aW1lICsgc3VmZml4KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGZvcm1hdCAoaW5wdXRTdHJpbmcpIHtcbiAgICBpZiAoIWlucHV0U3RyaW5nKSB7XG4gICAgICAgIGlucHV0U3RyaW5nID0gdGhpcy5pc1V0YygpID8gaG9va3MuZGVmYXVsdEZvcm1hdFV0YyA6IGhvb2tzLmRlZmF1bHRGb3JtYXQ7XG4gICAgfVxuICAgIHZhciBvdXRwdXQgPSBmb3JtYXRNb21lbnQodGhpcywgaW5wdXRTdHJpbmcpO1xuICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEoKS5wb3N0Zm9ybWF0KG91dHB1dCk7XG59XG4iLCJpbXBvcnQgeyBjcmVhdGVEdXJhdGlvbiB9IGZyb20gJy4uL2R1cmF0aW9uL2NyZWF0ZSc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyBpc01vbWVudCB9IGZyb20gJy4uL21vbWVudC9jb25zdHJ1Y3Rvcic7XG5cbmV4cG9ydCBmdW5jdGlvbiBmcm9tICh0aW1lLCB3aXRob3V0U3VmZml4KSB7XG4gICAgaWYgKHRoaXMuaXNWYWxpZCgpICYmXG4gICAgICAgICAgICAoKGlzTW9tZW50KHRpbWUpICYmIHRpbWUuaXNWYWxpZCgpKSB8fFxuICAgICAgICAgICAgIGNyZWF0ZUxvY2FsKHRpbWUpLmlzVmFsaWQoKSkpIHtcbiAgICAgICAgcmV0dXJuIGNyZWF0ZUR1cmF0aW9uKHt0bzogdGhpcywgZnJvbTogdGltZX0pLmxvY2FsZSh0aGlzLmxvY2FsZSgpKS5odW1hbml6ZSghd2l0aG91dFN1ZmZpeCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YSgpLmludmFsaWREYXRlKCk7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZnJvbU5vdyAod2l0aG91dFN1ZmZpeCkge1xuICAgIHJldHVybiB0aGlzLmZyb20oY3JlYXRlTG9jYWwoKSwgd2l0aG91dFN1ZmZpeCk7XG59XG4iLCJpbXBvcnQgeyBjcmVhdGVEdXJhdGlvbiB9IGZyb20gJy4uL2R1cmF0aW9uL2NyZWF0ZSc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyBpc01vbWVudCB9IGZyb20gJy4uL21vbWVudC9jb25zdHJ1Y3Rvcic7XG5cbmV4cG9ydCBmdW5jdGlvbiB0byAodGltZSwgd2l0aG91dFN1ZmZpeCkge1xuICAgIGlmICh0aGlzLmlzVmFsaWQoKSAmJlxuICAgICAgICAgICAgKChpc01vbWVudCh0aW1lKSAmJiB0aW1lLmlzVmFsaWQoKSkgfHxcbiAgICAgICAgICAgICBjcmVhdGVMb2NhbCh0aW1lKS5pc1ZhbGlkKCkpKSB7XG4gICAgICAgIHJldHVybiBjcmVhdGVEdXJhdGlvbih7ZnJvbTogdGhpcywgdG86IHRpbWV9KS5sb2NhbGUodGhpcy5sb2NhbGUoKSkuaHVtYW5pemUoIXdpdGhvdXRTdWZmaXgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEoKS5pbnZhbGlkRGF0ZSgpO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHRvTm93ICh3aXRob3V0U3VmZml4KSB7XG4gICAgcmV0dXJuIHRoaXMudG8oY3JlYXRlTG9jYWwoKSwgd2l0aG91dFN1ZmZpeCk7XG59XG4iLCJpbXBvcnQgeyBnZXRMb2NhbGUgfSBmcm9tICcuLi9sb2NhbGUvbG9jYWxlcyc7XG5pbXBvcnQgeyBkZXByZWNhdGUgfSBmcm9tICcuLi91dGlscy9kZXByZWNhdGUnO1xuXG4vLyBJZiBwYXNzZWQgYSBsb2NhbGUga2V5LCBpdCB3aWxsIHNldCB0aGUgbG9jYWxlIGZvciB0aGlzXG4vLyBpbnN0YW5jZS4gIE90aGVyd2lzZSwgaXQgd2lsbCByZXR1cm4gdGhlIGxvY2FsZSBjb25maWd1cmF0aW9uXG4vLyB2YXJpYWJsZXMgZm9yIHRoaXMgaW5zdGFuY2UuXG5leHBvcnQgZnVuY3Rpb24gbG9jYWxlIChrZXkpIHtcbiAgICB2YXIgbmV3TG9jYWxlRGF0YTtcblxuICAgIGlmIChrZXkgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fbG9jYWxlLl9hYmJyO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIG5ld0xvY2FsZURhdGEgPSBnZXRMb2NhbGUoa2V5KTtcbiAgICAgICAgaWYgKG5ld0xvY2FsZURhdGEgIT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5fbG9jYWxlID0gbmV3TG9jYWxlRGF0YTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG59XG5cbmV4cG9ydCB2YXIgbGFuZyA9IGRlcHJlY2F0ZShcbiAgICAnbW9tZW50KCkubGFuZygpIGlzIGRlcHJlY2F0ZWQuIEluc3RlYWQsIHVzZSBtb21lbnQoKS5sb2NhbGVEYXRhKCkgdG8gZ2V0IHRoZSBsYW5ndWFnZSBjb25maWd1cmF0aW9uLiBVc2UgbW9tZW50KCkubG9jYWxlKCkgdG8gY2hhbmdlIGxhbmd1YWdlcy4nLFxuICAgIGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgaWYgKGtleSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbGVEYXRhKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbGUoa2V5KTtcbiAgICAgICAgfVxuICAgIH1cbik7XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2NhbGVEYXRhICgpIHtcbiAgICByZXR1cm4gdGhpcy5fbG9jYWxlO1xufVxuIiwiaW1wb3J0IHsgbm9ybWFsaXplVW5pdHMgfSBmcm9tICcuLi91bml0cy9hbGlhc2VzJztcblxuZXhwb3J0IGZ1bmN0aW9uIHN0YXJ0T2YgKHVuaXRzKSB7XG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG4gICAgLy8gdGhlIGZvbGxvd2luZyBzd2l0Y2ggaW50ZW50aW9uYWxseSBvbWl0cyBicmVhayBrZXl3b3Jkc1xuICAgIC8vIHRvIHV0aWxpemUgZmFsbGluZyB0aHJvdWdoIHRoZSBjYXNlcy5cbiAgICBzd2l0Y2ggKHVuaXRzKSB7XG4gICAgICAgIGNhc2UgJ3llYXInOlxuICAgICAgICAgICAgdGhpcy5tb250aCgwKTtcbiAgICAgICAgICAgIC8qIGZhbGxzIHRocm91Z2ggKi9cbiAgICAgICAgY2FzZSAncXVhcnRlcic6XG4gICAgICAgIGNhc2UgJ21vbnRoJzpcbiAgICAgICAgICAgIHRoaXMuZGF0ZSgxKTtcbiAgICAgICAgICAgIC8qIGZhbGxzIHRocm91Z2ggKi9cbiAgICAgICAgY2FzZSAnd2Vlayc6XG4gICAgICAgIGNhc2UgJ2lzb1dlZWsnOlxuICAgICAgICBjYXNlICdkYXknOlxuICAgICAgICBjYXNlICdkYXRlJzpcbiAgICAgICAgICAgIHRoaXMuaG91cnMoMCk7XG4gICAgICAgICAgICAvKiBmYWxscyB0aHJvdWdoICovXG4gICAgICAgIGNhc2UgJ2hvdXInOlxuICAgICAgICAgICAgdGhpcy5taW51dGVzKDApO1xuICAgICAgICAgICAgLyogZmFsbHMgdGhyb3VnaCAqL1xuICAgICAgICBjYXNlICdtaW51dGUnOlxuICAgICAgICAgICAgdGhpcy5zZWNvbmRzKDApO1xuICAgICAgICAgICAgLyogZmFsbHMgdGhyb3VnaCAqL1xuICAgICAgICBjYXNlICdzZWNvbmQnOlxuICAgICAgICAgICAgdGhpcy5taWxsaXNlY29uZHMoMCk7XG4gICAgfVxuXG4gICAgLy8gd2Vla3MgYXJlIGEgc3BlY2lhbCBjYXNlXG4gICAgaWYgKHVuaXRzID09PSAnd2VlaycpIHtcbiAgICAgICAgdGhpcy53ZWVrZGF5KDApO1xuICAgIH1cbiAgICBpZiAodW5pdHMgPT09ICdpc29XZWVrJykge1xuICAgICAgICB0aGlzLmlzb1dlZWtkYXkoMSk7XG4gICAgfVxuXG4gICAgLy8gcXVhcnRlcnMgYXJlIGFsc28gc3BlY2lhbFxuICAgIGlmICh1bml0cyA9PT0gJ3F1YXJ0ZXInKSB7XG4gICAgICAgIHRoaXMubW9udGgoTWF0aC5mbG9vcih0aGlzLm1vbnRoKCkgLyAzKSAqIDMpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZW5kT2YgKHVuaXRzKSB7XG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG4gICAgaWYgKHVuaXRzID09PSB1bmRlZmluZWQgfHwgdW5pdHMgPT09ICdtaWxsaXNlY29uZCcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLy8gJ2RhdGUnIGlzIGFuIGFsaWFzIGZvciAnZGF5Jywgc28gaXQgc2hvdWxkIGJlIGNvbnNpZGVyZWQgYXMgc3VjaC5cbiAgICBpZiAodW5pdHMgPT09ICdkYXRlJykge1xuICAgICAgICB1bml0cyA9ICdkYXknO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLnN0YXJ0T2YodW5pdHMpLmFkZCgxLCAodW5pdHMgPT09ICdpc29XZWVrJyA/ICd3ZWVrJyA6IHVuaXRzKSkuc3VidHJhY3QoMSwgJ21zJyk7XG59XG4iLCJleHBvcnQgZnVuY3Rpb24gdmFsdWVPZiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2QudmFsdWVPZigpIC0gKCh0aGlzLl9vZmZzZXQgfHwgMCkgKiA2MDAwMCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB1bml4ICgpIHtcbiAgICByZXR1cm4gTWF0aC5mbG9vcih0aGlzLnZhbHVlT2YoKSAvIDEwMDApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdG9EYXRlICgpIHtcbiAgICByZXR1cm4gbmV3IERhdGUodGhpcy52YWx1ZU9mKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdG9BcnJheSAoKSB7XG4gICAgdmFyIG0gPSB0aGlzO1xuICAgIHJldHVybiBbbS55ZWFyKCksIG0ubW9udGgoKSwgbS5kYXRlKCksIG0uaG91cigpLCBtLm1pbnV0ZSgpLCBtLnNlY29uZCgpLCBtLm1pbGxpc2Vjb25kKCldO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdG9PYmplY3QgKCkge1xuICAgIHZhciBtID0gdGhpcztcbiAgICByZXR1cm4ge1xuICAgICAgICB5ZWFyczogbS55ZWFyKCksXG4gICAgICAgIG1vbnRoczogbS5tb250aCgpLFxuICAgICAgICBkYXRlOiBtLmRhdGUoKSxcbiAgICAgICAgaG91cnM6IG0uaG91cnMoKSxcbiAgICAgICAgbWludXRlczogbS5taW51dGVzKCksXG4gICAgICAgIHNlY29uZHM6IG0uc2Vjb25kcygpLFxuICAgICAgICBtaWxsaXNlY29uZHM6IG0ubWlsbGlzZWNvbmRzKClcbiAgICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdG9KU09OICgpIHtcbiAgICAvLyBuZXcgRGF0ZShOYU4pLnRvSlNPTigpID09PSBudWxsXG4gICAgcmV0dXJuIHRoaXMuaXNWYWxpZCgpID8gdGhpcy50b0lTT1N0cmluZygpIDogbnVsbDtcbn1cbiIsImltcG9ydCB7IGlzVmFsaWQgYXMgX2lzVmFsaWQgfSBmcm9tICcuLi9jcmVhdGUvdmFsaWQnO1xuaW1wb3J0IGV4dGVuZCBmcm9tICcuLi91dGlscy9leHRlbmQnO1xuaW1wb3J0IGdldFBhcnNpbmdGbGFncyBmcm9tICcuLi9jcmVhdGUvcGFyc2luZy1mbGFncyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1ZhbGlkICgpIHtcbiAgICByZXR1cm4gX2lzVmFsaWQodGhpcyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBwYXJzaW5nRmxhZ3MgKCkge1xuICAgIHJldHVybiBleHRlbmQoe30sIGdldFBhcnNpbmdGbGFncyh0aGlzKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbnZhbGlkQXQgKCkge1xuICAgIHJldHVybiBnZXRQYXJzaW5nRmxhZ3ModGhpcykub3ZlcmZsb3c7XG59XG4iLCJleHBvcnQgZnVuY3Rpb24gY3JlYXRpb25EYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIGlucHV0OiB0aGlzLl9pLFxuICAgICAgICBmb3JtYXQ6IHRoaXMuX2YsXG4gICAgICAgIGxvY2FsZTogdGhpcy5fbG9jYWxlLFxuICAgICAgICBpc1VUQzogdGhpcy5faXNVVEMsXG4gICAgICAgIHN0cmljdDogdGhpcy5fc3RyaWN0XG4gICAgfTtcbn1cbiIsImltcG9ydCB7IGFkZEZvcm1hdFRva2VuIH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBhZGRVbml0QWxpYXMgfSBmcm9tICcuL2FsaWFzZXMnO1xuaW1wb3J0IHsgYWRkVW5pdFByaW9yaXR5IH0gZnJvbSAnLi9wcmlvcml0aWVzJztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoMXRvMiwgbWF0Y2gxdG80LCBtYXRjaDF0bzYsIG1hdGNoMiwgbWF0Y2g0LCBtYXRjaDYsIG1hdGNoU2lnbmVkIH0gZnJvbSAnLi4vcGFyc2UvcmVnZXgnO1xuaW1wb3J0IHsgYWRkV2Vla1BhcnNlVG9rZW4gfSBmcm9tICcuLi9wYXJzZS90b2tlbic7XG5pbXBvcnQgeyB3ZWVrT2ZZZWFyLCB3ZWVrc0luWWVhciwgZGF5T2ZZZWFyRnJvbVdlZWtzIH0gZnJvbSAnLi93ZWVrLWNhbGVuZGFyLXV0aWxzJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5pbXBvcnQgeyBjcmVhdGVMb2NhbCB9IGZyb20gJy4uL2NyZWF0ZS9sb2NhbCc7XG5pbXBvcnQgeyBjcmVhdGVVVENEYXRlIH0gZnJvbSAnLi4vY3JlYXRlL2RhdGUtZnJvbS1hcnJheSc7XG5cbi8vIEZPUk1BVFRJTkdcblxuYWRkRm9ybWF0VG9rZW4oMCwgWydnZycsIDJdLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMud2Vla1llYXIoKSAlIDEwMDtcbn0pO1xuXG5hZGRGb3JtYXRUb2tlbigwLCBbJ0dHJywgMl0sIDAsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5pc29XZWVrWWVhcigpICUgMTAwO1xufSk7XG5cbmZ1bmN0aW9uIGFkZFdlZWtZZWFyRm9ybWF0VG9rZW4gKHRva2VuLCBnZXR0ZXIpIHtcbiAgICBhZGRGb3JtYXRUb2tlbigwLCBbdG9rZW4sIHRva2VuLmxlbmd0aF0sIDAsIGdldHRlcik7XG59XG5cbmFkZFdlZWtZZWFyRm9ybWF0VG9rZW4oJ2dnZ2cnLCAgICAgJ3dlZWtZZWFyJyk7XG5hZGRXZWVrWWVhckZvcm1hdFRva2VuKCdnZ2dnZycsICAgICd3ZWVrWWVhcicpO1xuYWRkV2Vla1llYXJGb3JtYXRUb2tlbignR0dHRycsICAnaXNvV2Vla1llYXInKTtcbmFkZFdlZWtZZWFyRm9ybWF0VG9rZW4oJ0dHR0dHJywgJ2lzb1dlZWtZZWFyJyk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCd3ZWVrWWVhcicsICdnZycpO1xuYWRkVW5pdEFsaWFzKCdpc29XZWVrWWVhcicsICdHRycpO1xuXG4vLyBQUklPUklUWVxuXG5hZGRVbml0UHJpb3JpdHkoJ3dlZWtZZWFyJywgMSk7XG5hZGRVbml0UHJpb3JpdHkoJ2lzb1dlZWtZZWFyJywgMSk7XG5cblxuLy8gUEFSU0lOR1xuXG5hZGRSZWdleFRva2VuKCdHJywgICAgICBtYXRjaFNpZ25lZCk7XG5hZGRSZWdleFRva2VuKCdnJywgICAgICBtYXRjaFNpZ25lZCk7XG5hZGRSZWdleFRva2VuKCdHRycsICAgICBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdnZycsICAgICBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdHR0dHJywgICBtYXRjaDF0bzQsIG1hdGNoNCk7XG5hZGRSZWdleFRva2VuKCdnZ2dnJywgICBtYXRjaDF0bzQsIG1hdGNoNCk7XG5hZGRSZWdleFRva2VuKCdHR0dHRycsICBtYXRjaDF0bzYsIG1hdGNoNik7XG5hZGRSZWdleFRva2VuKCdnZ2dnZycsICBtYXRjaDF0bzYsIG1hdGNoNik7XG5cbmFkZFdlZWtQYXJzZVRva2VuKFsnZ2dnZycsICdnZ2dnZycsICdHR0dHJywgJ0dHR0dHJ10sIGZ1bmN0aW9uIChpbnB1dCwgd2VlaywgY29uZmlnLCB0b2tlbikge1xuICAgIHdlZWtbdG9rZW4uc3Vic3RyKDAsIDIpXSA9IHRvSW50KGlucHV0KTtcbn0pO1xuXG5hZGRXZWVrUGFyc2VUb2tlbihbJ2dnJywgJ0dHJ10sIGZ1bmN0aW9uIChpbnB1dCwgd2VlaywgY29uZmlnLCB0b2tlbikge1xuICAgIHdlZWtbdG9rZW5dID0gaG9va3MucGFyc2VUd29EaWdpdFllYXIoaW5wdXQpO1xufSk7XG5cbi8vIE1PTUVOVFNcblxuZXhwb3J0IGZ1bmN0aW9uIGdldFNldFdlZWtZZWFyIChpbnB1dCkge1xuICAgIHJldHVybiBnZXRTZXRXZWVrWWVhckhlbHBlci5jYWxsKHRoaXMsXG4gICAgICAgICAgICBpbnB1dCxcbiAgICAgICAgICAgIHRoaXMud2VlaygpLFxuICAgICAgICAgICAgdGhpcy53ZWVrZGF5KCksXG4gICAgICAgICAgICB0aGlzLmxvY2FsZURhdGEoKS5fd2Vlay5kb3csXG4gICAgICAgICAgICB0aGlzLmxvY2FsZURhdGEoKS5fd2Vlay5kb3kpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0U2V0SVNPV2Vla1llYXIgKGlucHV0KSB7XG4gICAgcmV0dXJuIGdldFNldFdlZWtZZWFySGVscGVyLmNhbGwodGhpcyxcbiAgICAgICAgICAgIGlucHV0LCB0aGlzLmlzb1dlZWsoKSwgdGhpcy5pc29XZWVrZGF5KCksIDEsIDQpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0SVNPV2Vla3NJblllYXIgKCkge1xuICAgIHJldHVybiB3ZWVrc0luWWVhcih0aGlzLnllYXIoKSwgMSwgNCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRXZWVrc0luWWVhciAoKSB7XG4gICAgdmFyIHdlZWtJbmZvID0gdGhpcy5sb2NhbGVEYXRhKCkuX3dlZWs7XG4gICAgcmV0dXJuIHdlZWtzSW5ZZWFyKHRoaXMueWVhcigpLCB3ZWVrSW5mby5kb3csIHdlZWtJbmZvLmRveSk7XG59XG5cbmZ1bmN0aW9uIGdldFNldFdlZWtZZWFySGVscGVyKGlucHV0LCB3ZWVrLCB3ZWVrZGF5LCBkb3csIGRveSkge1xuICAgIHZhciB3ZWVrc1RhcmdldDtcbiAgICBpZiAoaW5wdXQgPT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gd2Vla09mWWVhcih0aGlzLCBkb3csIGRveSkueWVhcjtcbiAgICB9IGVsc2Uge1xuICAgICAgICB3ZWVrc1RhcmdldCA9IHdlZWtzSW5ZZWFyKGlucHV0LCBkb3csIGRveSk7XG4gICAgICAgIGlmICh3ZWVrID4gd2Vla3NUYXJnZXQpIHtcbiAgICAgICAgICAgIHdlZWsgPSB3ZWVrc1RhcmdldDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc2V0V2Vla0FsbC5jYWxsKHRoaXMsIGlucHV0LCB3ZWVrLCB3ZWVrZGF5LCBkb3csIGRveSk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBzZXRXZWVrQWxsKHdlZWtZZWFyLCB3ZWVrLCB3ZWVrZGF5LCBkb3csIGRveSkge1xuICAgIHZhciBkYXlPZlllYXJEYXRhID0gZGF5T2ZZZWFyRnJvbVdlZWtzKHdlZWtZZWFyLCB3ZWVrLCB3ZWVrZGF5LCBkb3csIGRveSksXG4gICAgICAgIGRhdGUgPSBjcmVhdGVVVENEYXRlKGRheU9mWWVhckRhdGEueWVhciwgMCwgZGF5T2ZZZWFyRGF0YS5kYXlPZlllYXIpO1xuXG4gICAgdGhpcy55ZWFyKGRhdGUuZ2V0VVRDRnVsbFllYXIoKSk7XG4gICAgdGhpcy5tb250aChkYXRlLmdldFVUQ01vbnRoKCkpO1xuICAgIHRoaXMuZGF0ZShkYXRlLmdldFVUQ0RhdGUoKSk7XG4gICAgcmV0dXJuIHRoaXM7XG59XG4iLCJpbXBvcnQgeyBhZGRGb3JtYXRUb2tlbiB9IGZyb20gJy4uL2Zvcm1hdC9mb3JtYXQnO1xuaW1wb3J0IHsgYWRkVW5pdEFsaWFzIH0gZnJvbSAnLi9hbGlhc2VzJztcbmltcG9ydCB7IGFkZFVuaXRQcmlvcml0eSB9IGZyb20gJy4vcHJpb3JpdGllcyc7XG5pbXBvcnQgeyBhZGRSZWdleFRva2VuLCBtYXRjaDEgfSBmcm9tICcuLi9wYXJzZS9yZWdleCc7XG5pbXBvcnQgeyBhZGRQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHsgTU9OVEggfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbignUScsIDAsICdRbycsICdxdWFydGVyJyk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCdxdWFydGVyJywgJ1EnKTtcblxuLy8gUFJJT1JJVFlcblxuYWRkVW5pdFByaW9yaXR5KCdxdWFydGVyJywgNyk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbignUScsIG1hdGNoMSk7XG5hZGRQYXJzZVRva2VuKCdRJywgZnVuY3Rpb24gKGlucHV0LCBhcnJheSkge1xuICAgIGFycmF5W01PTlRIXSA9ICh0b0ludChpbnB1dCkgLSAxKSAqIDM7XG59KTtcblxuLy8gTU9NRU5UU1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0U2V0UXVhcnRlciAoaW5wdXQpIHtcbiAgICByZXR1cm4gaW5wdXQgPT0gbnVsbCA/IE1hdGguY2VpbCgodGhpcy5tb250aCgpICsgMSkgLyAzKSA6IHRoaXMubW9udGgoKGlucHV0IC0gMSkgKiAzICsgdGhpcy5tb250aCgpICUgMyk7XG59XG4iLCJpbXBvcnQgeyBtYWtlR2V0U2V0IH0gZnJvbSAnLi4vbW9tZW50L2dldC1zZXQnO1xuaW1wb3J0IHsgYWRkRm9ybWF0VG9rZW4gfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcbmltcG9ydCB7IGFkZFVuaXRBbGlhcyB9IGZyb20gJy4vYWxpYXNlcyc7XG5pbXBvcnQgeyBhZGRVbml0UHJpb3JpdHkgfSBmcm9tICcuL3ByaW9yaXRpZXMnO1xuaW1wb3J0IHsgYWRkUmVnZXhUb2tlbiwgbWF0Y2gxdG8yLCBtYXRjaDIgfSBmcm9tICcuLi9wYXJzZS9yZWdleCc7XG5pbXBvcnQgeyBhZGRQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHsgREFURSB9IGZyb20gJy4vY29uc3RhbnRzJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuXG4vLyBGT1JNQVRUSU5HXG5cbmFkZEZvcm1hdFRva2VuKCdEJywgWydERCcsIDJdLCAnRG8nLCAnZGF0ZScpO1xuXG4vLyBBTElBU0VTXG5cbmFkZFVuaXRBbGlhcygnZGF0ZScsICdEJyk7XG5cbi8vIFBSSU9ST0lUWVxuYWRkVW5pdFByaW9yaXR5KCdkYXRlJywgOSk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbignRCcsICBtYXRjaDF0bzIpO1xuYWRkUmVnZXhUb2tlbignREQnLCBtYXRjaDF0bzIsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdEbycsIGZ1bmN0aW9uIChpc1N0cmljdCwgbG9jYWxlKSB7XG4gICAgcmV0dXJuIGlzU3RyaWN0ID8gbG9jYWxlLl9vcmRpbmFsUGFyc2UgOiBsb2NhbGUuX29yZGluYWxQYXJzZUxlbmllbnQ7XG59KTtcblxuYWRkUGFyc2VUb2tlbihbJ0QnLCAnREQnXSwgREFURSk7XG5hZGRQYXJzZVRva2VuKCdEbycsIGZ1bmN0aW9uIChpbnB1dCwgYXJyYXkpIHtcbiAgICBhcnJheVtEQVRFXSA9IHRvSW50KGlucHV0Lm1hdGNoKG1hdGNoMXRvMilbMF0sIDEwKTtcbn0pO1xuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCB2YXIgZ2V0U2V0RGF5T2ZNb250aCA9IG1ha2VHZXRTZXQoJ0RhdGUnLCB0cnVlKTtcbiIsImltcG9ydCB7IGFkZEZvcm1hdFRva2VuIH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBhZGRVbml0QWxpYXMgfSBmcm9tICcuL2FsaWFzZXMnO1xuaW1wb3J0IHsgYWRkVW5pdFByaW9yaXR5IH0gZnJvbSAnLi9wcmlvcml0aWVzJztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoMywgbWF0Y2gxdG8zIH0gZnJvbSAnLi4vcGFyc2UvcmVnZXgnO1xuaW1wb3J0IHsgZGF5c0luWWVhciB9IGZyb20gJy4veWVhcic7XG5pbXBvcnQgeyBjcmVhdGVVVENEYXRlIH0gZnJvbSAnLi4vY3JlYXRlL2RhdGUtZnJvbS1hcnJheSc7XG5pbXBvcnQgeyBhZGRQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHRvSW50IGZyb20gJy4uL3V0aWxzL3RvLWludCc7XG5cbi8vIEZPUk1BVFRJTkdcblxuYWRkRm9ybWF0VG9rZW4oJ0RERCcsIFsnRERERCcsIDNdLCAnREREbycsICdkYXlPZlllYXInKTtcblxuLy8gQUxJQVNFU1xuXG5hZGRVbml0QWxpYXMoJ2RheU9mWWVhcicsICdEREQnKTtcblxuLy8gUFJJT1JJVFlcbmFkZFVuaXRQcmlvcml0eSgnZGF5T2ZZZWFyJywgNCk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbignREREJywgIG1hdGNoMXRvMyk7XG5hZGRSZWdleFRva2VuKCdEREREJywgbWF0Y2gzKTtcbmFkZFBhcnNlVG9rZW4oWydEREQnLCAnRERERCddLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICBjb25maWcuX2RheU9mWWVhciA9IHRvSW50KGlucHV0KTtcbn0pO1xuXG4vLyBIRUxQRVJTXG5cbi8vIE1PTUVOVFNcblxuZXhwb3J0IGZ1bmN0aW9uIGdldFNldERheU9mWWVhciAoaW5wdXQpIHtcbiAgICB2YXIgZGF5T2ZZZWFyID0gTWF0aC5yb3VuZCgodGhpcy5jbG9uZSgpLnN0YXJ0T2YoJ2RheScpIC0gdGhpcy5jbG9uZSgpLnN0YXJ0T2YoJ3llYXInKSkgLyA4NjRlNSkgKyAxO1xuICAgIHJldHVybiBpbnB1dCA9PSBudWxsID8gZGF5T2ZZZWFyIDogdGhpcy5hZGQoKGlucHV0IC0gZGF5T2ZZZWFyKSwgJ2QnKTtcbn1cbiIsImltcG9ydCB7IG1ha2VHZXRTZXQgfSBmcm9tICcuLi9tb21lbnQvZ2V0LXNldCc7XG5pbXBvcnQgeyBhZGRGb3JtYXRUb2tlbiB9IGZyb20gJy4uL2Zvcm1hdC9mb3JtYXQnO1xuaW1wb3J0IHsgYWRkVW5pdEFsaWFzIH0gZnJvbSAnLi9hbGlhc2VzJztcbmltcG9ydCB7IGFkZFVuaXRQcmlvcml0eSB9IGZyb20gJy4vcHJpb3JpdGllcyc7XG5pbXBvcnQgeyBhZGRSZWdleFRva2VuLCBtYXRjaDF0bzIsIG1hdGNoMiB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFBhcnNlVG9rZW4gfSBmcm9tICcuLi9wYXJzZS90b2tlbic7XG5pbXBvcnQgeyBNSU5VVEUgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5cbi8vIEZPUk1BVFRJTkdcblxuYWRkRm9ybWF0VG9rZW4oJ20nLCBbJ21tJywgMl0sIDAsICdtaW51dGUnKTtcblxuLy8gQUxJQVNFU1xuXG5hZGRVbml0QWxpYXMoJ21pbnV0ZScsICdtJyk7XG5cbi8vIFBSSU9SSVRZXG5cbmFkZFVuaXRQcmlvcml0eSgnbWludXRlJywgMTQpO1xuXG4vLyBQQVJTSU5HXG5cbmFkZFJlZ2V4VG9rZW4oJ20nLCAgbWF0Y2gxdG8yKTtcbmFkZFJlZ2V4VG9rZW4oJ21tJywgbWF0Y2gxdG8yLCBtYXRjaDIpO1xuYWRkUGFyc2VUb2tlbihbJ20nLCAnbW0nXSwgTUlOVVRFKTtcblxuLy8gTU9NRU5UU1xuXG5leHBvcnQgdmFyIGdldFNldE1pbnV0ZSA9IG1ha2VHZXRTZXQoJ01pbnV0ZXMnLCBmYWxzZSk7XG4iLCJpbXBvcnQgeyBtYWtlR2V0U2V0IH0gZnJvbSAnLi4vbW9tZW50L2dldC1zZXQnO1xuaW1wb3J0IHsgYWRkRm9ybWF0VG9rZW4gfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcbmltcG9ydCB7IGFkZFVuaXRBbGlhcyB9IGZyb20gJy4vYWxpYXNlcyc7XG5pbXBvcnQgeyBhZGRVbml0UHJpb3JpdHkgfSBmcm9tICcuL3ByaW9yaXRpZXMnO1xuaW1wb3J0IHsgYWRkUmVnZXhUb2tlbiwgbWF0Y2gxdG8yLCBtYXRjaDIgfSBmcm9tICcuLi9wYXJzZS9yZWdleCc7XG5pbXBvcnQgeyBhZGRQYXJzZVRva2VuIH0gZnJvbSAnLi4vcGFyc2UvdG9rZW4nO1xuaW1wb3J0IHsgU0VDT05EIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuXG4vLyBGT1JNQVRUSU5HXG5cbmFkZEZvcm1hdFRva2VuKCdzJywgWydzcycsIDJdLCAwLCAnc2Vjb25kJyk7XG5cbi8vIEFMSUFTRVNcblxuYWRkVW5pdEFsaWFzKCdzZWNvbmQnLCAncycpO1xuXG4vLyBQUklPUklUWVxuXG5hZGRVbml0UHJpb3JpdHkoJ3NlY29uZCcsIDE1KTtcblxuLy8gUEFSU0lOR1xuXG5hZGRSZWdleFRva2VuKCdzJywgIG1hdGNoMXRvMik7XG5hZGRSZWdleFRva2VuKCdzcycsIG1hdGNoMXRvMiwgbWF0Y2gyKTtcbmFkZFBhcnNlVG9rZW4oWydzJywgJ3NzJ10sIFNFQ09ORCk7XG5cbi8vIE1PTUVOVFNcblxuZXhwb3J0IHZhciBnZXRTZXRTZWNvbmQgPSBtYWtlR2V0U2V0KCdTZWNvbmRzJywgZmFsc2UpO1xuIiwiaW1wb3J0IHsgbWFrZUdldFNldCB9IGZyb20gJy4uL21vbWVudC9nZXQtc2V0JztcbmltcG9ydCB7IGFkZEZvcm1hdFRva2VuIH0gZnJvbSAnLi4vZm9ybWF0L2Zvcm1hdCc7XG5pbXBvcnQgeyBhZGRVbml0QWxpYXMgfSBmcm9tICcuL2FsaWFzZXMnO1xuaW1wb3J0IHsgYWRkVW5pdFByaW9yaXR5IH0gZnJvbSAnLi9wcmlvcml0aWVzJztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoMSwgbWF0Y2gyLCBtYXRjaDMsIG1hdGNoMXRvMywgbWF0Y2hVbnNpZ25lZCB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFBhcnNlVG9rZW4gfSBmcm9tICcuLi9wYXJzZS90b2tlbic7XG5pbXBvcnQgeyBNSUxMSVNFQ09ORCB9IGZyb20gJy4vY29uc3RhbnRzJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuXG4vLyBGT1JNQVRUSU5HXG5cbmFkZEZvcm1hdFRva2VuKCdTJywgMCwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB+fih0aGlzLm1pbGxpc2Vjb25kKCkgLyAxMDApO1xufSk7XG5cbmFkZEZvcm1hdFRva2VuKDAsIFsnU1MnLCAyXSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB+fih0aGlzLm1pbGxpc2Vjb25kKCkgLyAxMCk7XG59KTtcblxuYWRkRm9ybWF0VG9rZW4oMCwgWydTU1MnLCAzXSwgMCwgJ21pbGxpc2Vjb25kJyk7XG5hZGRGb3JtYXRUb2tlbigwLCBbJ1NTU1MnLCA0XSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1pbGxpc2Vjb25kKCkgKiAxMDtcbn0pO1xuYWRkRm9ybWF0VG9rZW4oMCwgWydTU1NTUycsIDVdLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMubWlsbGlzZWNvbmQoKSAqIDEwMDtcbn0pO1xuYWRkRm9ybWF0VG9rZW4oMCwgWydTU1NTU1MnLCA2XSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1pbGxpc2Vjb25kKCkgKiAxMDAwO1xufSk7XG5hZGRGb3JtYXRUb2tlbigwLCBbJ1NTU1NTU1MnLCA3XSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1pbGxpc2Vjb25kKCkgKiAxMDAwMDtcbn0pO1xuYWRkRm9ybWF0VG9rZW4oMCwgWydTU1NTU1NTUycsIDhdLCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMubWlsbGlzZWNvbmQoKSAqIDEwMDAwMDtcbn0pO1xuYWRkRm9ybWF0VG9rZW4oMCwgWydTU1NTU1NTU1MnLCA5XSwgMCwgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLm1pbGxpc2Vjb25kKCkgKiAxMDAwMDAwO1xufSk7XG5cblxuLy8gQUxJQVNFU1xuXG5hZGRVbml0QWxpYXMoJ21pbGxpc2Vjb25kJywgJ21zJyk7XG5cbi8vIFBSSU9SSVRZXG5cbmFkZFVuaXRQcmlvcml0eSgnbWlsbGlzZWNvbmQnLCAxNik7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbignUycsICAgIG1hdGNoMXRvMywgbWF0Y2gxKTtcbmFkZFJlZ2V4VG9rZW4oJ1NTJywgICBtYXRjaDF0bzMsIG1hdGNoMik7XG5hZGRSZWdleFRva2VuKCdTU1MnLCAgbWF0Y2gxdG8zLCBtYXRjaDMpO1xuXG52YXIgdG9rZW47XG5mb3IgKHRva2VuID0gJ1NTU1MnOyB0b2tlbi5sZW5ndGggPD0gOTsgdG9rZW4gKz0gJ1MnKSB7XG4gICAgYWRkUmVnZXhUb2tlbih0b2tlbiwgbWF0Y2hVbnNpZ25lZCk7XG59XG5cbmZ1bmN0aW9uIHBhcnNlTXMoaW5wdXQsIGFycmF5KSB7XG4gICAgYXJyYXlbTUlMTElTRUNPTkRdID0gdG9JbnQoKCcwLicgKyBpbnB1dCkgKiAxMDAwKTtcbn1cblxuZm9yICh0b2tlbiA9ICdTJzsgdG9rZW4ubGVuZ3RoIDw9IDk7IHRva2VuICs9ICdTJykge1xuICAgIGFkZFBhcnNlVG9rZW4odG9rZW4sIHBhcnNlTXMpO1xufVxuLy8gTU9NRU5UU1xuXG5leHBvcnQgdmFyIGdldFNldE1pbGxpc2Vjb25kID0gbWFrZUdldFNldCgnTWlsbGlzZWNvbmRzJywgZmFsc2UpO1xuIiwiaW1wb3J0IHsgYWRkRm9ybWF0VG9rZW4gfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbigneicsICAwLCAwLCAnem9uZUFiYnInKTtcbmFkZEZvcm1hdFRva2VuKCd6eicsIDAsIDAsICd6b25lTmFtZScpO1xuXG4vLyBNT01FTlRTXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRab25lQWJiciAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2lzVVRDID8gJ1VUQycgOiAnJztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFpvbmVOYW1lICgpIHtcbiAgICByZXR1cm4gdGhpcy5faXNVVEMgPyAnQ29vcmRpbmF0ZWQgVW5pdmVyc2FsIFRpbWUnIDogJyc7XG59XG4iLCJpbXBvcnQgeyBNb21lbnQgfSBmcm9tICcuL2NvbnN0cnVjdG9yJztcblxudmFyIHByb3RvID0gTW9tZW50LnByb3RvdHlwZTtcblxuaW1wb3J0IHsgYWRkLCBzdWJ0cmFjdCB9IGZyb20gJy4vYWRkLXN1YnRyYWN0JztcbmltcG9ydCB7IGNhbGVuZGFyLCBnZXRDYWxlbmRhckZvcm1hdCB9IGZyb20gJy4vY2FsZW5kYXInO1xuaW1wb3J0IHsgY2xvbmUgfSBmcm9tICcuL2Nsb25lJztcbmltcG9ydCB7IGlzQmVmb3JlLCBpc0JldHdlZW4sIGlzU2FtZSwgaXNBZnRlciwgaXNTYW1lT3JBZnRlciwgaXNTYW1lT3JCZWZvcmUgfSBmcm9tICcuL2NvbXBhcmUnO1xuaW1wb3J0IHsgZGlmZiB9IGZyb20gJy4vZGlmZic7XG5pbXBvcnQgeyBmb3JtYXQsIHRvU3RyaW5nLCB0b0lTT1N0cmluZywgaW5zcGVjdCB9IGZyb20gJy4vZm9ybWF0JztcbmltcG9ydCB7IGZyb20sIGZyb21Ob3cgfSBmcm9tICcuL2Zyb20nO1xuaW1wb3J0IHsgdG8sIHRvTm93IH0gZnJvbSAnLi90byc7XG5pbXBvcnQgeyBzdHJpbmdHZXQsIHN0cmluZ1NldCB9IGZyb20gJy4vZ2V0LXNldCc7XG5pbXBvcnQgeyBsb2NhbGUsIGxvY2FsZURhdGEsIGxhbmcgfSBmcm9tICcuL2xvY2FsZSc7XG5pbXBvcnQgeyBwcm90b3R5cGVNaW4sIHByb3RvdHlwZU1heCB9IGZyb20gJy4vbWluLW1heCc7XG5pbXBvcnQgeyBzdGFydE9mLCBlbmRPZiB9IGZyb20gJy4vc3RhcnQtZW5kLW9mJztcbmltcG9ydCB7IHZhbHVlT2YsIHRvRGF0ZSwgdG9BcnJheSwgdG9PYmplY3QsIHRvSlNPTiwgdW5peCB9IGZyb20gJy4vdG8tdHlwZSc7XG5pbXBvcnQgeyBpc1ZhbGlkLCBwYXJzaW5nRmxhZ3MsIGludmFsaWRBdCB9IGZyb20gJy4vdmFsaWQnO1xuaW1wb3J0IHsgY3JlYXRpb25EYXRhIH0gZnJvbSAnLi9jcmVhdGlvbi1kYXRhJztcblxucHJvdG8uYWRkICAgICAgICAgICAgICAgPSBhZGQ7XG5wcm90by5jYWxlbmRhciAgICAgICAgICA9IGNhbGVuZGFyO1xucHJvdG8uY2xvbmUgICAgICAgICAgICAgPSBjbG9uZTtcbnByb3RvLmRpZmYgICAgICAgICAgICAgID0gZGlmZjtcbnByb3RvLmVuZE9mICAgICAgICAgICAgID0gZW5kT2Y7XG5wcm90by5mb3JtYXQgICAgICAgICAgICA9IGZvcm1hdDtcbnByb3RvLmZyb20gICAgICAgICAgICAgID0gZnJvbTtcbnByb3RvLmZyb21Ob3cgICAgICAgICAgID0gZnJvbU5vdztcbnByb3RvLnRvICAgICAgICAgICAgICAgID0gdG87XG5wcm90by50b05vdyAgICAgICAgICAgICA9IHRvTm93O1xucHJvdG8uZ2V0ICAgICAgICAgICAgICAgPSBzdHJpbmdHZXQ7XG5wcm90by5pbnZhbGlkQXQgICAgICAgICA9IGludmFsaWRBdDtcbnByb3RvLmlzQWZ0ZXIgICAgICAgICAgID0gaXNBZnRlcjtcbnByb3RvLmlzQmVmb3JlICAgICAgICAgID0gaXNCZWZvcmU7XG5wcm90by5pc0JldHdlZW4gICAgICAgICA9IGlzQmV0d2VlbjtcbnByb3RvLmlzU2FtZSAgICAgICAgICAgID0gaXNTYW1lO1xucHJvdG8uaXNTYW1lT3JBZnRlciAgICAgPSBpc1NhbWVPckFmdGVyO1xucHJvdG8uaXNTYW1lT3JCZWZvcmUgICAgPSBpc1NhbWVPckJlZm9yZTtcbnByb3RvLmlzVmFsaWQgICAgICAgICAgID0gaXNWYWxpZDtcbnByb3RvLmxhbmcgICAgICAgICAgICAgID0gbGFuZztcbnByb3RvLmxvY2FsZSAgICAgICAgICAgID0gbG9jYWxlO1xucHJvdG8ubG9jYWxlRGF0YSAgICAgICAgPSBsb2NhbGVEYXRhO1xucHJvdG8ubWF4ICAgICAgICAgICAgICAgPSBwcm90b3R5cGVNYXg7XG5wcm90by5taW4gICAgICAgICAgICAgICA9IHByb3RvdHlwZU1pbjtcbnByb3RvLnBhcnNpbmdGbGFncyAgICAgID0gcGFyc2luZ0ZsYWdzO1xucHJvdG8uc2V0ICAgICAgICAgICAgICAgPSBzdHJpbmdTZXQ7XG5wcm90by5zdGFydE9mICAgICAgICAgICA9IHN0YXJ0T2Y7XG5wcm90by5zdWJ0cmFjdCAgICAgICAgICA9IHN1YnRyYWN0O1xucHJvdG8udG9BcnJheSAgICAgICAgICAgPSB0b0FycmF5O1xucHJvdG8udG9PYmplY3QgICAgICAgICAgPSB0b09iamVjdDtcbnByb3RvLnRvRGF0ZSAgICAgICAgICAgID0gdG9EYXRlO1xucHJvdG8udG9JU09TdHJpbmcgICAgICAgPSB0b0lTT1N0cmluZztcbnByb3RvLmluc3BlY3QgICAgICAgICAgID0gaW5zcGVjdDtcbnByb3RvLnRvSlNPTiAgICAgICAgICAgID0gdG9KU09OO1xucHJvdG8udG9TdHJpbmcgICAgICAgICAgPSB0b1N0cmluZztcbnByb3RvLnVuaXggICAgICAgICAgICAgID0gdW5peDtcbnByb3RvLnZhbHVlT2YgICAgICAgICAgID0gdmFsdWVPZjtcbnByb3RvLmNyZWF0aW9uRGF0YSAgICAgID0gY3JlYXRpb25EYXRhO1xuXG4vLyBZZWFyXG5pbXBvcnQgeyBnZXRTZXRZZWFyLCBnZXRJc0xlYXBZZWFyIH0gZnJvbSAnLi4vdW5pdHMveWVhcic7XG5wcm90by55ZWFyICAgICAgID0gZ2V0U2V0WWVhcjtcbnByb3RvLmlzTGVhcFllYXIgPSBnZXRJc0xlYXBZZWFyO1xuXG4vLyBXZWVrIFllYXJcbmltcG9ydCB7IGdldFNldFdlZWtZZWFyLCBnZXRTZXRJU09XZWVrWWVhciwgZ2V0V2Vla3NJblllYXIsIGdldElTT1dlZWtzSW5ZZWFyIH0gZnJvbSAnLi4vdW5pdHMvd2Vlay15ZWFyJztcbnByb3RvLndlZWtZZWFyICAgID0gZ2V0U2V0V2Vla1llYXI7XG5wcm90by5pc29XZWVrWWVhciA9IGdldFNldElTT1dlZWtZZWFyO1xuXG4vLyBRdWFydGVyXG5pbXBvcnQgeyBnZXRTZXRRdWFydGVyIH0gZnJvbSAnLi4vdW5pdHMvcXVhcnRlcic7XG5wcm90by5xdWFydGVyID0gcHJvdG8ucXVhcnRlcnMgPSBnZXRTZXRRdWFydGVyO1xuXG4vLyBNb250aFxuaW1wb3J0IHsgZ2V0U2V0TW9udGgsIGdldERheXNJbk1vbnRoIH0gZnJvbSAnLi4vdW5pdHMvbW9udGgnO1xucHJvdG8ubW9udGggICAgICAgPSBnZXRTZXRNb250aDtcbnByb3RvLmRheXNJbk1vbnRoID0gZ2V0RGF5c0luTW9udGg7XG5cbi8vIFdlZWtcbmltcG9ydCB7IGdldFNldFdlZWssIGdldFNldElTT1dlZWsgfSBmcm9tICcuLi91bml0cy93ZWVrJztcbnByb3RvLndlZWsgICAgICAgICAgID0gcHJvdG8ud2Vla3MgICAgICAgID0gZ2V0U2V0V2VlaztcbnByb3RvLmlzb1dlZWsgICAgICAgID0gcHJvdG8uaXNvV2Vla3MgICAgID0gZ2V0U2V0SVNPV2VlaztcbnByb3RvLndlZWtzSW5ZZWFyICAgID0gZ2V0V2Vla3NJblllYXI7XG5wcm90by5pc29XZWVrc0luWWVhciA9IGdldElTT1dlZWtzSW5ZZWFyO1xuXG4vLyBEYXlcbmltcG9ydCB7IGdldFNldERheU9mTW9udGggfSBmcm9tICcuLi91bml0cy9kYXktb2YtbW9udGgnO1xuaW1wb3J0IHsgZ2V0U2V0RGF5T2ZXZWVrLCBnZXRTZXRJU09EYXlPZldlZWssIGdldFNldExvY2FsZURheU9mV2VlayB9IGZyb20gJy4uL3VuaXRzL2RheS1vZi13ZWVrJztcbmltcG9ydCB7IGdldFNldERheU9mWWVhciB9IGZyb20gJy4uL3VuaXRzL2RheS1vZi15ZWFyJztcbnByb3RvLmRhdGUgICAgICAgPSBnZXRTZXREYXlPZk1vbnRoO1xucHJvdG8uZGF5ICAgICAgICA9IHByb3RvLmRheXMgICAgICAgICAgICAgPSBnZXRTZXREYXlPZldlZWs7XG5wcm90by53ZWVrZGF5ICAgID0gZ2V0U2V0TG9jYWxlRGF5T2ZXZWVrO1xucHJvdG8uaXNvV2Vla2RheSA9IGdldFNldElTT0RheU9mV2VlaztcbnByb3RvLmRheU9mWWVhciAgPSBnZXRTZXREYXlPZlllYXI7XG5cbi8vIEhvdXJcbmltcG9ydCB7IGdldFNldEhvdXIgfSBmcm9tICcuLi91bml0cy9ob3VyJztcbnByb3RvLmhvdXIgPSBwcm90by5ob3VycyA9IGdldFNldEhvdXI7XG5cbi8vIE1pbnV0ZVxuaW1wb3J0IHsgZ2V0U2V0TWludXRlIH0gZnJvbSAnLi4vdW5pdHMvbWludXRlJztcbnByb3RvLm1pbnV0ZSA9IHByb3RvLm1pbnV0ZXMgPSBnZXRTZXRNaW51dGU7XG5cbi8vIFNlY29uZFxuaW1wb3J0IHsgZ2V0U2V0U2Vjb25kIH0gZnJvbSAnLi4vdW5pdHMvc2Vjb25kJztcbnByb3RvLnNlY29uZCA9IHByb3RvLnNlY29uZHMgPSBnZXRTZXRTZWNvbmQ7XG5cbi8vIE1pbGxpc2Vjb25kXG5pbXBvcnQgeyBnZXRTZXRNaWxsaXNlY29uZCB9IGZyb20gJy4uL3VuaXRzL21pbGxpc2Vjb25kJztcbnByb3RvLm1pbGxpc2Vjb25kID0gcHJvdG8ubWlsbGlzZWNvbmRzID0gZ2V0U2V0TWlsbGlzZWNvbmQ7XG5cbi8vIE9mZnNldFxuaW1wb3J0IHtcbiAgICBnZXRTZXRPZmZzZXQsXG4gICAgc2V0T2Zmc2V0VG9VVEMsXG4gICAgc2V0T2Zmc2V0VG9Mb2NhbCxcbiAgICBzZXRPZmZzZXRUb1BhcnNlZE9mZnNldCxcbiAgICBoYXNBbGlnbmVkSG91ck9mZnNldCxcbiAgICBpc0RheWxpZ2h0U2F2aW5nVGltZSxcbiAgICBpc0RheWxpZ2h0U2F2aW5nVGltZVNoaWZ0ZWQsXG4gICAgZ2V0U2V0Wm9uZSxcbiAgICBpc0xvY2FsLFxuICAgIGlzVXRjT2Zmc2V0LFxuICAgIGlzVXRjXG59IGZyb20gJy4uL3VuaXRzL29mZnNldCc7XG5wcm90by51dGNPZmZzZXQgICAgICAgICAgICA9IGdldFNldE9mZnNldDtcbnByb3RvLnV0YyAgICAgICAgICAgICAgICAgID0gc2V0T2Zmc2V0VG9VVEM7XG5wcm90by5sb2NhbCAgICAgICAgICAgICAgICA9IHNldE9mZnNldFRvTG9jYWw7XG5wcm90by5wYXJzZVpvbmUgICAgICAgICAgICA9IHNldE9mZnNldFRvUGFyc2VkT2Zmc2V0O1xucHJvdG8uaGFzQWxpZ25lZEhvdXJPZmZzZXQgPSBoYXNBbGlnbmVkSG91ck9mZnNldDtcbnByb3RvLmlzRFNUICAgICAgICAgICAgICAgID0gaXNEYXlsaWdodFNhdmluZ1RpbWU7XG5wcm90by5pc0xvY2FsICAgICAgICAgICAgICA9IGlzTG9jYWw7XG5wcm90by5pc1V0Y09mZnNldCAgICAgICAgICA9IGlzVXRjT2Zmc2V0O1xucHJvdG8uaXNVdGMgICAgICAgICAgICAgICAgPSBpc1V0YztcbnByb3RvLmlzVVRDICAgICAgICAgICAgICAgID0gaXNVdGM7XG5cbi8vIFRpbWV6b25lXG5pbXBvcnQgeyBnZXRab25lQWJiciwgZ2V0Wm9uZU5hbWUgfSBmcm9tICcuLi91bml0cy90aW1lem9uZSc7XG5wcm90by56b25lQWJiciA9IGdldFpvbmVBYmJyO1xucHJvdG8uem9uZU5hbWUgPSBnZXRab25lTmFtZTtcblxuLy8gRGVwcmVjYXRpb25zXG5pbXBvcnQgeyBkZXByZWNhdGUgfSBmcm9tICcuLi91dGlscy9kZXByZWNhdGUnO1xucHJvdG8uZGF0ZXMgID0gZGVwcmVjYXRlKCdkYXRlcyBhY2Nlc3NvciBpcyBkZXByZWNhdGVkLiBVc2UgZGF0ZSBpbnN0ZWFkLicsIGdldFNldERheU9mTW9udGgpO1xucHJvdG8ubW9udGhzID0gZGVwcmVjYXRlKCdtb250aHMgYWNjZXNzb3IgaXMgZGVwcmVjYXRlZC4gVXNlIG1vbnRoIGluc3RlYWQnLCBnZXRTZXRNb250aCk7XG5wcm90by55ZWFycyAgPSBkZXByZWNhdGUoJ3llYXJzIGFjY2Vzc29yIGlzIGRlcHJlY2F0ZWQuIFVzZSB5ZWFyIGluc3RlYWQnLCBnZXRTZXRZZWFyKTtcbnByb3RvLnpvbmUgICA9IGRlcHJlY2F0ZSgnbW9tZW50KCkuem9uZSBpcyBkZXByZWNhdGVkLCB1c2UgbW9tZW50KCkudXRjT2Zmc2V0IGluc3RlYWQuIGh0dHA6Ly9tb21lbnRqcy5jb20vZ3VpZGVzLyMvd2FybmluZ3Mvem9uZS8nLCBnZXRTZXRab25lKTtcbnByb3RvLmlzRFNUU2hpZnRlZCA9IGRlcHJlY2F0ZSgnaXNEU1RTaGlmdGVkIGlzIGRlcHJlY2F0ZWQuIFNlZSBodHRwOi8vbW9tZW50anMuY29tL2d1aWRlcy8jL3dhcm5pbmdzL2RzdC1zaGlmdGVkLyBmb3IgbW9yZSBpbmZvcm1hdGlvbicsIGlzRGF5bGlnaHRTYXZpbmdUaW1lU2hpZnRlZCk7XG5cbmV4cG9ydCBkZWZhdWx0IHByb3RvO1xuIiwiaW1wb3J0IHsgY3JlYXRlTG9jYWwgfSBmcm9tICcuLi9jcmVhdGUvbG9jYWwnO1xuaW1wb3J0IHsgY3JlYXRlVVRDIH0gZnJvbSAnLi4vY3JlYXRlL3V0Yyc7XG5pbXBvcnQgeyBjcmVhdGVJbnZhbGlkIH0gZnJvbSAnLi4vY3JlYXRlL3ZhbGlkJztcbmltcG9ydCB7IGlzTW9tZW50IH0gZnJvbSAnLi9jb25zdHJ1Y3Rvcic7XG5pbXBvcnQgeyBtaW4sIG1heCB9IGZyb20gJy4vbWluLW1heCc7XG5pbXBvcnQgeyBub3cgfSBmcm9tICcuL25vdyc7XG5pbXBvcnQgbW9tZW50UHJvdG90eXBlIGZyb20gJy4vcHJvdG90eXBlJztcblxuZnVuY3Rpb24gY3JlYXRlVW5peCAoaW5wdXQpIHtcbiAgICByZXR1cm4gY3JlYXRlTG9jYWwoaW5wdXQgKiAxMDAwKTtcbn1cblxuZnVuY3Rpb24gY3JlYXRlSW5ab25lICgpIHtcbiAgICByZXR1cm4gY3JlYXRlTG9jYWwuYXBwbHkobnVsbCwgYXJndW1lbnRzKS5wYXJzZVpvbmUoKTtcbn1cblxuZXhwb3J0IHtcbiAgICBub3csXG4gICAgbWluLFxuICAgIG1heCxcbiAgICBpc01vbWVudCxcbiAgICBjcmVhdGVVVEMsXG4gICAgY3JlYXRlVW5peCxcbiAgICBjcmVhdGVMb2NhbCxcbiAgICBjcmVhdGVJblpvbmUsXG4gICAgY3JlYXRlSW52YWxpZCxcbiAgICBtb21lbnRQcm90b3R5cGVcbn07XG4iLCJleHBvcnQgZnVuY3Rpb24gcHJlUGFyc2VQb3N0Rm9ybWF0IChzdHJpbmcpIHtcbiAgICByZXR1cm4gc3RyaW5nO1xufVxuIiwiaW1wb3J0IHsgTG9jYWxlIH0gZnJvbSAnLi9jb25zdHJ1Y3Rvcic7XG5cbnZhciBwcm90byA9IExvY2FsZS5wcm90b3R5cGU7XG5cbmltcG9ydCB7IGNhbGVuZGFyIH0gZnJvbSAnLi9jYWxlbmRhcic7XG5pbXBvcnQgeyBsb25nRGF0ZUZvcm1hdCB9IGZyb20gJy4vZm9ybWF0cyc7XG5pbXBvcnQgeyBpbnZhbGlkRGF0ZSB9IGZyb20gJy4vaW52YWxpZCc7XG5pbXBvcnQgeyBvcmRpbmFsIH0gZnJvbSAnLi9vcmRpbmFsJztcbmltcG9ydCB7IHByZVBhcnNlUG9zdEZvcm1hdCB9IGZyb20gJy4vcHJlLXBvc3QtZm9ybWF0JztcbmltcG9ydCB7IHJlbGF0aXZlVGltZSwgcGFzdEZ1dHVyZSB9IGZyb20gJy4vcmVsYXRpdmUnO1xuaW1wb3J0IHsgc2V0IH0gZnJvbSAnLi9zZXQnO1xuXG5wcm90by5jYWxlbmRhciAgICAgICAgPSBjYWxlbmRhcjtcbnByb3RvLmxvbmdEYXRlRm9ybWF0ICA9IGxvbmdEYXRlRm9ybWF0O1xucHJvdG8uaW52YWxpZERhdGUgICAgID0gaW52YWxpZERhdGU7XG5wcm90by5vcmRpbmFsICAgICAgICAgPSBvcmRpbmFsO1xucHJvdG8ucHJlcGFyc2UgICAgICAgID0gcHJlUGFyc2VQb3N0Rm9ybWF0O1xucHJvdG8ucG9zdGZvcm1hdCAgICAgID0gcHJlUGFyc2VQb3N0Rm9ybWF0O1xucHJvdG8ucmVsYXRpdmVUaW1lICAgID0gcmVsYXRpdmVUaW1lO1xucHJvdG8ucGFzdEZ1dHVyZSAgICAgID0gcGFzdEZ1dHVyZTtcbnByb3RvLnNldCAgICAgICAgICAgICA9IHNldDtcblxuLy8gTW9udGhcbmltcG9ydCB7XG4gICAgbG9jYWxlTW9udGhzUGFyc2UsXG4gICAgbG9jYWxlTW9udGhzLFxuICAgIGxvY2FsZU1vbnRoc1Nob3J0LFxuICAgIG1vbnRoc1JlZ2V4LFxuICAgIG1vbnRoc1Nob3J0UmVnZXhcbn0gZnJvbSAnLi4vdW5pdHMvbW9udGgnO1xuXG5wcm90by5tb250aHMgICAgICAgICAgICA9ICAgICAgICBsb2NhbGVNb250aHM7XG5wcm90by5tb250aHNTaG9ydCAgICAgICA9ICAgICAgICBsb2NhbGVNb250aHNTaG9ydDtcbnByb3RvLm1vbnRoc1BhcnNlICAgICAgID0gICAgICAgIGxvY2FsZU1vbnRoc1BhcnNlO1xucHJvdG8ubW9udGhzUmVnZXggICAgICAgPSBtb250aHNSZWdleDtcbnByb3RvLm1vbnRoc1Nob3J0UmVnZXggID0gbW9udGhzU2hvcnRSZWdleDtcblxuLy8gV2Vla1xuaW1wb3J0IHsgbG9jYWxlV2VlaywgbG9jYWxlRmlyc3REYXlPZlllYXIsIGxvY2FsZUZpcnN0RGF5T2ZXZWVrIH0gZnJvbSAnLi4vdW5pdHMvd2Vlayc7XG5wcm90by53ZWVrID0gbG9jYWxlV2VlaztcbnByb3RvLmZpcnN0RGF5T2ZZZWFyID0gbG9jYWxlRmlyc3REYXlPZlllYXI7XG5wcm90by5maXJzdERheU9mV2VlayA9IGxvY2FsZUZpcnN0RGF5T2ZXZWVrO1xuXG4vLyBEYXkgb2YgV2Vla1xuaW1wb3J0IHtcbiAgICBsb2NhbGVXZWVrZGF5c1BhcnNlLFxuICAgIGxvY2FsZVdlZWtkYXlzLFxuICAgIGxvY2FsZVdlZWtkYXlzTWluLFxuICAgIGxvY2FsZVdlZWtkYXlzU2hvcnQsXG5cbiAgICB3ZWVrZGF5c1JlZ2V4LFxuICAgIHdlZWtkYXlzU2hvcnRSZWdleCxcbiAgICB3ZWVrZGF5c01pblJlZ2V4XG59IGZyb20gJy4uL3VuaXRzL2RheS1vZi13ZWVrJztcblxucHJvdG8ud2Vla2RheXMgICAgICAgPSAgICAgICAgbG9jYWxlV2Vla2RheXM7XG5wcm90by53ZWVrZGF5c01pbiAgICA9ICAgICAgICBsb2NhbGVXZWVrZGF5c01pbjtcbnByb3RvLndlZWtkYXlzU2hvcnQgID0gICAgICAgIGxvY2FsZVdlZWtkYXlzU2hvcnQ7XG5wcm90by53ZWVrZGF5c1BhcnNlICA9ICAgICAgICBsb2NhbGVXZWVrZGF5c1BhcnNlO1xuXG5wcm90by53ZWVrZGF5c1JlZ2V4ICAgICAgID0gICAgICAgIHdlZWtkYXlzUmVnZXg7XG5wcm90by53ZWVrZGF5c1Nob3J0UmVnZXggID0gICAgICAgIHdlZWtkYXlzU2hvcnRSZWdleDtcbnByb3RvLndlZWtkYXlzTWluUmVnZXggICAgPSAgICAgICAgd2Vla2RheXNNaW5SZWdleDtcblxuLy8gSG91cnNcbmltcG9ydCB7IGxvY2FsZUlzUE0sIGxvY2FsZU1lcmlkaWVtIH0gZnJvbSAnLi4vdW5pdHMvaG91cic7XG5cbnByb3RvLmlzUE0gPSBsb2NhbGVJc1BNO1xucHJvdG8ubWVyaWRpZW0gPSBsb2NhbGVNZXJpZGllbTtcbiIsImltcG9ydCBpc051bWJlciBmcm9tICcuLi91dGlscy9pcy1udW1iZXInO1xuaW1wb3J0IHsgZ2V0TG9jYWxlIH0gZnJvbSAnLi9sb2NhbGVzJztcbmltcG9ydCB7IGNyZWF0ZVVUQyB9IGZyb20gJy4uL2NyZWF0ZS91dGMnO1xuXG5mdW5jdGlvbiBnZXQgKGZvcm1hdCwgaW5kZXgsIGZpZWxkLCBzZXR0ZXIpIHtcbiAgICB2YXIgbG9jYWxlID0gZ2V0TG9jYWxlKCk7XG4gICAgdmFyIHV0YyA9IGNyZWF0ZVVUQygpLnNldChzZXR0ZXIsIGluZGV4KTtcbiAgICByZXR1cm4gbG9jYWxlW2ZpZWxkXSh1dGMsIGZvcm1hdCk7XG59XG5cbmZ1bmN0aW9uIGxpc3RNb250aHNJbXBsIChmb3JtYXQsIGluZGV4LCBmaWVsZCkge1xuICAgIGlmIChpc051bWJlcihmb3JtYXQpKSB7XG4gICAgICAgIGluZGV4ID0gZm9ybWF0O1xuICAgICAgICBmb3JtYXQgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgZm9ybWF0ID0gZm9ybWF0IHx8ICcnO1xuXG4gICAgaWYgKGluZGV4ICE9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIGdldChmb3JtYXQsIGluZGV4LCBmaWVsZCwgJ21vbnRoJyk7XG4gICAgfVxuXG4gICAgdmFyIGk7XG4gICAgdmFyIG91dCA9IFtdO1xuICAgIGZvciAoaSA9IDA7IGkgPCAxMjsgaSsrKSB7XG4gICAgICAgIG91dFtpXSA9IGdldChmb3JtYXQsIGksIGZpZWxkLCAnbW9udGgnKTtcbiAgICB9XG4gICAgcmV0dXJuIG91dDtcbn1cblxuLy8gKClcbi8vICg1KVxuLy8gKGZtdCwgNSlcbi8vIChmbXQpXG4vLyAodHJ1ZSlcbi8vICh0cnVlLCA1KVxuLy8gKHRydWUsIGZtdCwgNSlcbi8vICh0cnVlLCBmbXQpXG5mdW5jdGlvbiBsaXN0V2Vla2RheXNJbXBsIChsb2NhbGVTb3J0ZWQsIGZvcm1hdCwgaW5kZXgsIGZpZWxkKSB7XG4gICAgaWYgKHR5cGVvZiBsb2NhbGVTb3J0ZWQgPT09ICdib29sZWFuJykge1xuICAgICAgICBpZiAoaXNOdW1iZXIoZm9ybWF0KSkge1xuICAgICAgICAgICAgaW5kZXggPSBmb3JtYXQ7XG4gICAgICAgICAgICBmb3JtYXQgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBmb3JtYXQgPSBmb3JtYXQgfHwgJyc7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgZm9ybWF0ID0gbG9jYWxlU29ydGVkO1xuICAgICAgICBpbmRleCA9IGZvcm1hdDtcbiAgICAgICAgbG9jYWxlU29ydGVkID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKGlzTnVtYmVyKGZvcm1hdCkpIHtcbiAgICAgICAgICAgIGluZGV4ID0gZm9ybWF0O1xuICAgICAgICAgICAgZm9ybWF0ID0gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgZm9ybWF0ID0gZm9ybWF0IHx8ICcnO1xuICAgIH1cblxuICAgIHZhciBsb2NhbGUgPSBnZXRMb2NhbGUoKSxcbiAgICAgICAgc2hpZnQgPSBsb2NhbGVTb3J0ZWQgPyBsb2NhbGUuX3dlZWsuZG93IDogMDtcblxuICAgIGlmIChpbmRleCAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiBnZXQoZm9ybWF0LCAoaW5kZXggKyBzaGlmdCkgJSA3LCBmaWVsZCwgJ2RheScpO1xuICAgIH1cblxuICAgIHZhciBpO1xuICAgIHZhciBvdXQgPSBbXTtcbiAgICBmb3IgKGkgPSAwOyBpIDwgNzsgaSsrKSB7XG4gICAgICAgIG91dFtpXSA9IGdldChmb3JtYXQsIChpICsgc2hpZnQpICUgNywgZmllbGQsICdkYXknKTtcbiAgICB9XG4gICAgcmV0dXJuIG91dDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxpc3RNb250aHMgKGZvcm1hdCwgaW5kZXgpIHtcbiAgICByZXR1cm4gbGlzdE1vbnRoc0ltcGwoZm9ybWF0LCBpbmRleCwgJ21vbnRocycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbGlzdE1vbnRoc1Nob3J0IChmb3JtYXQsIGluZGV4KSB7XG4gICAgcmV0dXJuIGxpc3RNb250aHNJbXBsKGZvcm1hdCwgaW5kZXgsICdtb250aHNTaG9ydCcpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbGlzdFdlZWtkYXlzIChsb2NhbGVTb3J0ZWQsIGZvcm1hdCwgaW5kZXgpIHtcbiAgICByZXR1cm4gbGlzdFdlZWtkYXlzSW1wbChsb2NhbGVTb3J0ZWQsIGZvcm1hdCwgaW5kZXgsICd3ZWVrZGF5cycpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbGlzdFdlZWtkYXlzU2hvcnQgKGxvY2FsZVNvcnRlZCwgZm9ybWF0LCBpbmRleCkge1xuICAgIHJldHVybiBsaXN0V2Vla2RheXNJbXBsKGxvY2FsZVNvcnRlZCwgZm9ybWF0LCBpbmRleCwgJ3dlZWtkYXlzU2hvcnQnKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxpc3RXZWVrZGF5c01pbiAobG9jYWxlU29ydGVkLCBmb3JtYXQsIGluZGV4KSB7XG4gICAgcmV0dXJuIGxpc3RXZWVrZGF5c0ltcGwobG9jYWxlU29ydGVkLCBmb3JtYXQsIGluZGV4LCAnd2Vla2RheXNNaW4nKTtcbn1cbiIsImltcG9ydCAnLi9wcm90b3R5cGUnO1xuaW1wb3J0IHsgZ2V0U2V0R2xvYmFsTG9jYWxlIH0gZnJvbSAnLi9sb2NhbGVzJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuXG5nZXRTZXRHbG9iYWxMb2NhbGUoJ2VuJywge1xuICAgIG9yZGluYWxQYXJzZTogL1xcZHsxLDJ9KHRofHN0fG5kfHJkKS8sXG4gICAgb3JkaW5hbCA6IGZ1bmN0aW9uIChudW1iZXIpIHtcbiAgICAgICAgdmFyIGIgPSBudW1iZXIgJSAxMCxcbiAgICAgICAgICAgIG91dHB1dCA9ICh0b0ludChudW1iZXIgJSAxMDAgLyAxMCkgPT09IDEpID8gJ3RoJyA6XG4gICAgICAgICAgICAoYiA9PT0gMSkgPyAnc3QnIDpcbiAgICAgICAgICAgIChiID09PSAyKSA/ICduZCcgOlxuICAgICAgICAgICAgKGIgPT09IDMpID8gJ3JkJyA6ICd0aCc7XG4gICAgICAgIHJldHVybiBudW1iZXIgKyBvdXRwdXQ7XG4gICAgfVxufSk7XG4iLCIvLyBTaWRlIGVmZmVjdCBpbXBvcnRzXG5pbXBvcnQgJy4vcHJvdG90eXBlJztcblxuaW1wb3J0IHtcbiAgICBnZXRTZXRHbG9iYWxMb2NhbGUsXG4gICAgZGVmaW5lTG9jYWxlLFxuICAgIHVwZGF0ZUxvY2FsZSxcbiAgICBnZXRMb2NhbGUsXG4gICAgbGlzdExvY2FsZXNcbn0gZnJvbSAnLi9sb2NhbGVzJztcblxuaW1wb3J0IHtcbiAgICBsaXN0TW9udGhzLFxuICAgIGxpc3RNb250aHNTaG9ydCxcbiAgICBsaXN0V2Vla2RheXMsXG4gICAgbGlzdFdlZWtkYXlzU2hvcnQsXG4gICAgbGlzdFdlZWtkYXlzTWluXG59IGZyb20gJy4vbGlzdHMnO1xuXG5leHBvcnQge1xuICAgIGdldFNldEdsb2JhbExvY2FsZSxcbiAgICBkZWZpbmVMb2NhbGUsXG4gICAgdXBkYXRlTG9jYWxlLFxuICAgIGdldExvY2FsZSxcbiAgICBsaXN0TG9jYWxlcyxcbiAgICBsaXN0TW9udGhzLFxuICAgIGxpc3RNb250aHNTaG9ydCxcbiAgICBsaXN0V2Vla2RheXMsXG4gICAgbGlzdFdlZWtkYXlzU2hvcnQsXG4gICAgbGlzdFdlZWtkYXlzTWluXG59O1xuXG5pbXBvcnQgeyBkZXByZWNhdGUgfSBmcm9tICcuLi91dGlscy9kZXByZWNhdGUnO1xuaW1wb3J0IHsgaG9va3MgfSBmcm9tICcuLi91dGlscy9ob29rcyc7XG5cbmhvb2tzLmxhbmcgPSBkZXByZWNhdGUoJ21vbWVudC5sYW5nIGlzIGRlcHJlY2F0ZWQuIFVzZSBtb21lbnQubG9jYWxlIGluc3RlYWQuJywgZ2V0U2V0R2xvYmFsTG9jYWxlKTtcbmhvb2tzLmxhbmdEYXRhID0gZGVwcmVjYXRlKCdtb21lbnQubGFuZ0RhdGEgaXMgZGVwcmVjYXRlZC4gVXNlIG1vbWVudC5sb2NhbGVEYXRhIGluc3RlYWQuJywgZ2V0TG9jYWxlKTtcblxuaW1wb3J0ICcuL2VuJztcbiIsInZhciBtYXRoQWJzID0gTWF0aC5hYnM7XG5cbmV4cG9ydCBmdW5jdGlvbiBhYnMgKCkge1xuICAgIHZhciBkYXRhICAgICAgICAgICA9IHRoaXMuX2RhdGE7XG5cbiAgICB0aGlzLl9taWxsaXNlY29uZHMgPSBtYXRoQWJzKHRoaXMuX21pbGxpc2Vjb25kcyk7XG4gICAgdGhpcy5fZGF5cyAgICAgICAgID0gbWF0aEFicyh0aGlzLl9kYXlzKTtcbiAgICB0aGlzLl9tb250aHMgICAgICAgPSBtYXRoQWJzKHRoaXMuX21vbnRocyk7XG5cbiAgICBkYXRhLm1pbGxpc2Vjb25kcyAgPSBtYXRoQWJzKGRhdGEubWlsbGlzZWNvbmRzKTtcbiAgICBkYXRhLnNlY29uZHMgICAgICAgPSBtYXRoQWJzKGRhdGEuc2Vjb25kcyk7XG4gICAgZGF0YS5taW51dGVzICAgICAgID0gbWF0aEFicyhkYXRhLm1pbnV0ZXMpO1xuICAgIGRhdGEuaG91cnMgICAgICAgICA9IG1hdGhBYnMoZGF0YS5ob3Vycyk7XG4gICAgZGF0YS5tb250aHMgICAgICAgID0gbWF0aEFicyhkYXRhLm1vbnRocyk7XG4gICAgZGF0YS55ZWFycyAgICAgICAgID0gbWF0aEFicyhkYXRhLnllYXJzKTtcblxuICAgIHJldHVybiB0aGlzO1xufVxuIiwiaW1wb3J0IHsgY3JlYXRlRHVyYXRpb24gfSBmcm9tICcuL2NyZWF0ZSc7XG5cbmZ1bmN0aW9uIGFkZFN1YnRyYWN0IChkdXJhdGlvbiwgaW5wdXQsIHZhbHVlLCBkaXJlY3Rpb24pIHtcbiAgICB2YXIgb3RoZXIgPSBjcmVhdGVEdXJhdGlvbihpbnB1dCwgdmFsdWUpO1xuXG4gICAgZHVyYXRpb24uX21pbGxpc2Vjb25kcyArPSBkaXJlY3Rpb24gKiBvdGhlci5fbWlsbGlzZWNvbmRzO1xuICAgIGR1cmF0aW9uLl9kYXlzICAgICAgICAgKz0gZGlyZWN0aW9uICogb3RoZXIuX2RheXM7XG4gICAgZHVyYXRpb24uX21vbnRocyAgICAgICArPSBkaXJlY3Rpb24gKiBvdGhlci5fbW9udGhzO1xuXG4gICAgcmV0dXJuIGR1cmF0aW9uLl9idWJibGUoKTtcbn1cblxuLy8gc3VwcG9ydHMgb25seSAyLjAtc3R5bGUgYWRkKDEsICdzJykgb3IgYWRkKGR1cmF0aW9uKVxuZXhwb3J0IGZ1bmN0aW9uIGFkZCAoaW5wdXQsIHZhbHVlKSB7XG4gICAgcmV0dXJuIGFkZFN1YnRyYWN0KHRoaXMsIGlucHV0LCB2YWx1ZSwgMSk7XG59XG5cbi8vIHN1cHBvcnRzIG9ubHkgMi4wLXN0eWxlIHN1YnRyYWN0KDEsICdzJykgb3Igc3VidHJhY3QoZHVyYXRpb24pXG5leHBvcnQgZnVuY3Rpb24gc3VidHJhY3QgKGlucHV0LCB2YWx1ZSkge1xuICAgIHJldHVybiBhZGRTdWJ0cmFjdCh0aGlzLCBpbnB1dCwgdmFsdWUsIC0xKTtcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGFic0NlaWwgKG51bWJlcikge1xuICAgIGlmIChudW1iZXIgPCAwKSB7XG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKG51bWJlcik7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIE1hdGguY2VpbChudW1iZXIpO1xuICAgIH1cbn1cbiIsImltcG9ydCBhYnNGbG9vciBmcm9tICcuLi91dGlscy9hYnMtZmxvb3InO1xuaW1wb3J0IGFic0NlaWwgZnJvbSAnLi4vdXRpbHMvYWJzLWNlaWwnO1xuaW1wb3J0IHsgY3JlYXRlVVRDRGF0ZSB9IGZyb20gJy4uL2NyZWF0ZS9kYXRlLWZyb20tYXJyYXknO1xuXG5leHBvcnQgZnVuY3Rpb24gYnViYmxlICgpIHtcbiAgICB2YXIgbWlsbGlzZWNvbmRzID0gdGhpcy5fbWlsbGlzZWNvbmRzO1xuICAgIHZhciBkYXlzICAgICAgICAgPSB0aGlzLl9kYXlzO1xuICAgIHZhciBtb250aHMgICAgICAgPSB0aGlzLl9tb250aHM7XG4gICAgdmFyIGRhdGEgICAgICAgICA9IHRoaXMuX2RhdGE7XG4gICAgdmFyIHNlY29uZHMsIG1pbnV0ZXMsIGhvdXJzLCB5ZWFycywgbW9udGhzRnJvbURheXM7XG5cbiAgICAvLyBpZiB3ZSBoYXZlIGEgbWl4IG9mIHBvc2l0aXZlIGFuZCBuZWdhdGl2ZSB2YWx1ZXMsIGJ1YmJsZSBkb3duIGZpcnN0XG4gICAgLy8gY2hlY2s6IGh0dHBzOi8vZ2l0aHViLmNvbS9tb21lbnQvbW9tZW50L2lzc3Vlcy8yMTY2XG4gICAgaWYgKCEoKG1pbGxpc2Vjb25kcyA+PSAwICYmIGRheXMgPj0gMCAmJiBtb250aHMgPj0gMCkgfHxcbiAgICAgICAgICAgIChtaWxsaXNlY29uZHMgPD0gMCAmJiBkYXlzIDw9IDAgJiYgbW9udGhzIDw9IDApKSkge1xuICAgICAgICBtaWxsaXNlY29uZHMgKz0gYWJzQ2VpbChtb250aHNUb0RheXMobW9udGhzKSArIGRheXMpICogODY0ZTU7XG4gICAgICAgIGRheXMgPSAwO1xuICAgICAgICBtb250aHMgPSAwO1xuICAgIH1cblxuICAgIC8vIFRoZSBmb2xsb3dpbmcgY29kZSBidWJibGVzIHVwIHZhbHVlcywgc2VlIHRoZSB0ZXN0cyBmb3JcbiAgICAvLyBleGFtcGxlcyBvZiB3aGF0IHRoYXQgbWVhbnMuXG4gICAgZGF0YS5taWxsaXNlY29uZHMgPSBtaWxsaXNlY29uZHMgJSAxMDAwO1xuXG4gICAgc2Vjb25kcyAgICAgICAgICAgPSBhYnNGbG9vcihtaWxsaXNlY29uZHMgLyAxMDAwKTtcbiAgICBkYXRhLnNlY29uZHMgICAgICA9IHNlY29uZHMgJSA2MDtcblxuICAgIG1pbnV0ZXMgICAgICAgICAgID0gYWJzRmxvb3Ioc2Vjb25kcyAvIDYwKTtcbiAgICBkYXRhLm1pbnV0ZXMgICAgICA9IG1pbnV0ZXMgJSA2MDtcblxuICAgIGhvdXJzICAgICAgICAgICAgID0gYWJzRmxvb3IobWludXRlcyAvIDYwKTtcbiAgICBkYXRhLmhvdXJzICAgICAgICA9IGhvdXJzICUgMjQ7XG5cbiAgICBkYXlzICs9IGFic0Zsb29yKGhvdXJzIC8gMjQpO1xuXG4gICAgLy8gY29udmVydCBkYXlzIHRvIG1vbnRoc1xuICAgIG1vbnRoc0Zyb21EYXlzID0gYWJzRmxvb3IoZGF5c1RvTW9udGhzKGRheXMpKTtcbiAgICBtb250aHMgKz0gbW9udGhzRnJvbURheXM7XG4gICAgZGF5cyAtPSBhYnNDZWlsKG1vbnRoc1RvRGF5cyhtb250aHNGcm9tRGF5cykpO1xuXG4gICAgLy8gMTIgbW9udGhzIC0+IDEgeWVhclxuICAgIHllYXJzID0gYWJzRmxvb3IobW9udGhzIC8gMTIpO1xuICAgIG1vbnRocyAlPSAxMjtcblxuICAgIGRhdGEuZGF5cyAgID0gZGF5cztcbiAgICBkYXRhLm1vbnRocyA9IG1vbnRocztcbiAgICBkYXRhLnllYXJzICA9IHllYXJzO1xuXG4gICAgcmV0dXJuIHRoaXM7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBkYXlzVG9Nb250aHMgKGRheXMpIHtcbiAgICAvLyA0MDAgeWVhcnMgaGF2ZSAxNDYwOTcgZGF5cyAodGFraW5nIGludG8gYWNjb3VudCBsZWFwIHllYXIgcnVsZXMpXG4gICAgLy8gNDAwIHllYXJzIGhhdmUgMTIgbW9udGhzID09PSA0ODAwXG4gICAgcmV0dXJuIGRheXMgKiA0ODAwIC8gMTQ2MDk3O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbW9udGhzVG9EYXlzIChtb250aHMpIHtcbiAgICAvLyB0aGUgcmV2ZXJzZSBvZiBkYXlzVG9Nb250aHNcbiAgICByZXR1cm4gbW9udGhzICogMTQ2MDk3IC8gNDgwMDtcbn1cbiIsImltcG9ydCB7IGRheXNUb01vbnRocywgbW9udGhzVG9EYXlzIH0gZnJvbSAnLi9idWJibGUnO1xuaW1wb3J0IHsgbm9ybWFsaXplVW5pdHMgfSBmcm9tICcuLi91bml0cy9hbGlhc2VzJztcbmltcG9ydCB0b0ludCBmcm9tICcuLi91dGlscy90by1pbnQnO1xuXG5leHBvcnQgZnVuY3Rpb24gYXMgKHVuaXRzKSB7XG4gICAgdmFyIGRheXM7XG4gICAgdmFyIG1vbnRocztcbiAgICB2YXIgbWlsbGlzZWNvbmRzID0gdGhpcy5fbWlsbGlzZWNvbmRzO1xuXG4gICAgdW5pdHMgPSBub3JtYWxpemVVbml0cyh1bml0cyk7XG5cbiAgICBpZiAodW5pdHMgPT09ICdtb250aCcgfHwgdW5pdHMgPT09ICd5ZWFyJykge1xuICAgICAgICBkYXlzICAgPSB0aGlzLl9kYXlzICAgKyBtaWxsaXNlY29uZHMgLyA4NjRlNTtcbiAgICAgICAgbW9udGhzID0gdGhpcy5fbW9udGhzICsgZGF5c1RvTW9udGhzKGRheXMpO1xuICAgICAgICByZXR1cm4gdW5pdHMgPT09ICdtb250aCcgPyBtb250aHMgOiBtb250aHMgLyAxMjtcbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBoYW5kbGUgbWlsbGlzZWNvbmRzIHNlcGFyYXRlbHkgYmVjYXVzZSBvZiBmbG9hdGluZyBwb2ludCBtYXRoIGVycm9ycyAoaXNzdWUgIzE4NjcpXG4gICAgICAgIGRheXMgPSB0aGlzLl9kYXlzICsgTWF0aC5yb3VuZChtb250aHNUb0RheXModGhpcy5fbW9udGhzKSk7XG4gICAgICAgIHN3aXRjaCAodW5pdHMpIHtcbiAgICAgICAgICAgIGNhc2UgJ3dlZWsnICAgOiByZXR1cm4gZGF5cyAvIDcgICAgICsgbWlsbGlzZWNvbmRzIC8gNjA0OGU1O1xuICAgICAgICAgICAgY2FzZSAnZGF5JyAgICA6IHJldHVybiBkYXlzICAgICAgICAgKyBtaWxsaXNlY29uZHMgLyA4NjRlNTtcbiAgICAgICAgICAgIGNhc2UgJ2hvdXInICAgOiByZXR1cm4gZGF5cyAqIDI0ICAgICsgbWlsbGlzZWNvbmRzIC8gMzZlNTtcbiAgICAgICAgICAgIGNhc2UgJ21pbnV0ZScgOiByZXR1cm4gZGF5cyAqIDE0NDAgICsgbWlsbGlzZWNvbmRzIC8gNmU0O1xuICAgICAgICAgICAgY2FzZSAnc2Vjb25kJyA6IHJldHVybiBkYXlzICogODY0MDAgKyBtaWxsaXNlY29uZHMgLyAxMDAwO1xuICAgICAgICAgICAgLy8gTWF0aC5mbG9vciBwcmV2ZW50cyBmbG9hdGluZyBwb2ludCBtYXRoIGVycm9ycyBoZXJlXG4gICAgICAgICAgICBjYXNlICdtaWxsaXNlY29uZCc6IHJldHVybiBNYXRoLmZsb29yKGRheXMgKiA4NjRlNSkgKyBtaWxsaXNlY29uZHM7XG4gICAgICAgICAgICBkZWZhdWx0OiB0aHJvdyBuZXcgRXJyb3IoJ1Vua25vd24gdW5pdCAnICsgdW5pdHMpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4vLyBUT0RPOiBVc2UgdGhpcy5hcygnbXMnKT9cbmV4cG9ydCBmdW5jdGlvbiB2YWx1ZU9mICgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgICB0aGlzLl9taWxsaXNlY29uZHMgK1xuICAgICAgICB0aGlzLl9kYXlzICogODY0ZTUgK1xuICAgICAgICAodGhpcy5fbW9udGhzICUgMTIpICogMjU5MmU2ICtcbiAgICAgICAgdG9JbnQodGhpcy5fbW9udGhzIC8gMTIpICogMzE1MzZlNlxuICAgICk7XG59XG5cbmZ1bmN0aW9uIG1ha2VBcyAoYWxpYXMpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5hcyhhbGlhcyk7XG4gICAgfTtcbn1cblxuZXhwb3J0IHZhciBhc01pbGxpc2Vjb25kcyA9IG1ha2VBcygnbXMnKTtcbmV4cG9ydCB2YXIgYXNTZWNvbmRzICAgICAgPSBtYWtlQXMoJ3MnKTtcbmV4cG9ydCB2YXIgYXNNaW51dGVzICAgICAgPSBtYWtlQXMoJ20nKTtcbmV4cG9ydCB2YXIgYXNIb3VycyAgICAgICAgPSBtYWtlQXMoJ2gnKTtcbmV4cG9ydCB2YXIgYXNEYXlzICAgICAgICAgPSBtYWtlQXMoJ2QnKTtcbmV4cG9ydCB2YXIgYXNXZWVrcyAgICAgICAgPSBtYWtlQXMoJ3cnKTtcbmV4cG9ydCB2YXIgYXNNb250aHMgICAgICAgPSBtYWtlQXMoJ00nKTtcbmV4cG9ydCB2YXIgYXNZZWFycyAgICAgICAgPSBtYWtlQXMoJ3knKTtcbiIsImltcG9ydCB7IG5vcm1hbGl6ZVVuaXRzIH0gZnJvbSAnLi4vdW5pdHMvYWxpYXNlcyc7XG5pbXBvcnQgYWJzRmxvb3IgZnJvbSAnLi4vdXRpbHMvYWJzLWZsb29yJztcblxuZXhwb3J0IGZ1bmN0aW9uIGdldCAodW5pdHMpIHtcbiAgICB1bml0cyA9IG5vcm1hbGl6ZVVuaXRzKHVuaXRzKTtcbiAgICByZXR1cm4gdGhpc1t1bml0cyArICdzJ10oKTtcbn1cblxuZnVuY3Rpb24gbWFrZUdldHRlcihuYW1lKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RhdGFbbmFtZV07XG4gICAgfTtcbn1cblxuZXhwb3J0IHZhciBtaWxsaXNlY29uZHMgPSBtYWtlR2V0dGVyKCdtaWxsaXNlY29uZHMnKTtcbmV4cG9ydCB2YXIgc2Vjb25kcyAgICAgID0gbWFrZUdldHRlcignc2Vjb25kcycpO1xuZXhwb3J0IHZhciBtaW51dGVzICAgICAgPSBtYWtlR2V0dGVyKCdtaW51dGVzJyk7XG5leHBvcnQgdmFyIGhvdXJzICAgICAgICA9IG1ha2VHZXR0ZXIoJ2hvdXJzJyk7XG5leHBvcnQgdmFyIGRheXMgICAgICAgICA9IG1ha2VHZXR0ZXIoJ2RheXMnKTtcbmV4cG9ydCB2YXIgbW9udGhzICAgICAgID0gbWFrZUdldHRlcignbW9udGhzJyk7XG5leHBvcnQgdmFyIHllYXJzICAgICAgICA9IG1ha2VHZXR0ZXIoJ3llYXJzJyk7XG5cbmV4cG9ydCBmdW5jdGlvbiB3ZWVrcyAoKSB7XG4gICAgcmV0dXJuIGFic0Zsb29yKHRoaXMuZGF5cygpIC8gNyk7XG59XG4iLCJpbXBvcnQgeyBjcmVhdGVEdXJhdGlvbiB9IGZyb20gJy4vY3JlYXRlJztcblxudmFyIHJvdW5kID0gTWF0aC5yb3VuZDtcbnZhciB0aHJlc2hvbGRzID0ge1xuICAgIHM6IDQ1LCAgLy8gc2Vjb25kcyB0byBtaW51dGVcbiAgICBtOiA0NSwgIC8vIG1pbnV0ZXMgdG8gaG91clxuICAgIGg6IDIyLCAgLy8gaG91cnMgdG8gZGF5XG4gICAgZDogMjYsICAvLyBkYXlzIHRvIG1vbnRoXG4gICAgTTogMTEgICAvLyBtb250aHMgdG8geWVhclxufTtcblxuLy8gaGVscGVyIGZ1bmN0aW9uIGZvciBtb21lbnQuZm4uZnJvbSwgbW9tZW50LmZuLmZyb21Ob3csIGFuZCBtb21lbnQuZHVyYXRpb24uZm4uaHVtYW5pemVcbmZ1bmN0aW9uIHN1YnN0aXR1dGVUaW1lQWdvKHN0cmluZywgbnVtYmVyLCB3aXRob3V0U3VmZml4LCBpc0Z1dHVyZSwgbG9jYWxlKSB7XG4gICAgcmV0dXJuIGxvY2FsZS5yZWxhdGl2ZVRpbWUobnVtYmVyIHx8IDEsICEhd2l0aG91dFN1ZmZpeCwgc3RyaW5nLCBpc0Z1dHVyZSk7XG59XG5cbmZ1bmN0aW9uIHJlbGF0aXZlVGltZSAocG9zTmVnRHVyYXRpb24sIHdpdGhvdXRTdWZmaXgsIGxvY2FsZSkge1xuICAgIHZhciBkdXJhdGlvbiA9IGNyZWF0ZUR1cmF0aW9uKHBvc05lZ0R1cmF0aW9uKS5hYnMoKTtcbiAgICB2YXIgc2Vjb25kcyAgPSByb3VuZChkdXJhdGlvbi5hcygncycpKTtcbiAgICB2YXIgbWludXRlcyAgPSByb3VuZChkdXJhdGlvbi5hcygnbScpKTtcbiAgICB2YXIgaG91cnMgICAgPSByb3VuZChkdXJhdGlvbi5hcygnaCcpKTtcbiAgICB2YXIgZGF5cyAgICAgPSByb3VuZChkdXJhdGlvbi5hcygnZCcpKTtcbiAgICB2YXIgbW9udGhzICAgPSByb3VuZChkdXJhdGlvbi5hcygnTScpKTtcbiAgICB2YXIgeWVhcnMgICAgPSByb3VuZChkdXJhdGlvbi5hcygneScpKTtcblxuICAgIHZhciBhID0gc2Vjb25kcyA8IHRocmVzaG9sZHMucyAmJiBbJ3MnLCBzZWNvbmRzXSAgfHxcbiAgICAgICAgICAgIG1pbnV0ZXMgPD0gMSAgICAgICAgICAgJiYgWydtJ10gICAgICAgICAgIHx8XG4gICAgICAgICAgICBtaW51dGVzIDwgdGhyZXNob2xkcy5tICYmIFsnbW0nLCBtaW51dGVzXSB8fFxuICAgICAgICAgICAgaG91cnMgICA8PSAxICAgICAgICAgICAmJiBbJ2gnXSAgICAgICAgICAgfHxcbiAgICAgICAgICAgIGhvdXJzICAgPCB0aHJlc2hvbGRzLmggJiYgWydoaCcsIGhvdXJzXSAgIHx8XG4gICAgICAgICAgICBkYXlzICAgIDw9IDEgICAgICAgICAgICYmIFsnZCddICAgICAgICAgICB8fFxuICAgICAgICAgICAgZGF5cyAgICA8IHRocmVzaG9sZHMuZCAmJiBbJ2RkJywgZGF5c10gICAgfHxcbiAgICAgICAgICAgIG1vbnRocyAgPD0gMSAgICAgICAgICAgJiYgWydNJ10gICAgICAgICAgIHx8XG4gICAgICAgICAgICBtb250aHMgIDwgdGhyZXNob2xkcy5NICYmIFsnTU0nLCBtb250aHNdICB8fFxuICAgICAgICAgICAgeWVhcnMgICA8PSAxICAgICAgICAgICAmJiBbJ3knXSAgICAgICAgICAgfHwgWyd5eScsIHllYXJzXTtcblxuICAgIGFbMl0gPSB3aXRob3V0U3VmZml4O1xuICAgIGFbM10gPSArcG9zTmVnRHVyYXRpb24gPiAwO1xuICAgIGFbNF0gPSBsb2NhbGU7XG4gICAgcmV0dXJuIHN1YnN0aXR1dGVUaW1lQWdvLmFwcGx5KG51bGwsIGEpO1xufVxuXG4vLyBUaGlzIGZ1bmN0aW9uIGFsbG93cyB5b3UgdG8gc2V0IHRoZSByb3VuZGluZyBmdW5jdGlvbiBmb3IgcmVsYXRpdmUgdGltZSBzdHJpbmdzXG5leHBvcnQgZnVuY3Rpb24gZ2V0U2V0UmVsYXRpdmVUaW1lUm91bmRpbmcgKHJvdW5kaW5nRnVuY3Rpb24pIHtcbiAgICBpZiAocm91bmRpbmdGdW5jdGlvbiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiByb3VuZDtcbiAgICB9XG4gICAgaWYgKHR5cGVvZihyb3VuZGluZ0Z1bmN0aW9uKSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICByb3VuZCA9IHJvdW5kaW5nRnVuY3Rpb247XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG59XG5cbi8vIFRoaXMgZnVuY3Rpb24gYWxsb3dzIHlvdSB0byBzZXQgYSB0aHJlc2hvbGQgZm9yIHJlbGF0aXZlIHRpbWUgc3RyaW5nc1xuZXhwb3J0IGZ1bmN0aW9uIGdldFNldFJlbGF0aXZlVGltZVRocmVzaG9sZCAodGhyZXNob2xkLCBsaW1pdCkge1xuICAgIGlmICh0aHJlc2hvbGRzW3RocmVzaG9sZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGlmIChsaW1pdCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiB0aHJlc2hvbGRzW3RocmVzaG9sZF07XG4gICAgfVxuICAgIHRocmVzaG9sZHNbdGhyZXNob2xkXSA9IGxpbWl0O1xuICAgIHJldHVybiB0cnVlO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaHVtYW5pemUgKHdpdGhTdWZmaXgpIHtcbiAgICB2YXIgbG9jYWxlID0gdGhpcy5sb2NhbGVEYXRhKCk7XG4gICAgdmFyIG91dHB1dCA9IHJlbGF0aXZlVGltZSh0aGlzLCAhd2l0aFN1ZmZpeCwgbG9jYWxlKTtcblxuICAgIGlmICh3aXRoU3VmZml4KSB7XG4gICAgICAgIG91dHB1dCA9IGxvY2FsZS5wYXN0RnV0dXJlKCt0aGlzLCBvdXRwdXQpO1xuICAgIH1cblxuICAgIHJldHVybiBsb2NhbGUucG9zdGZvcm1hdChvdXRwdXQpO1xufVxuIiwiaW1wb3J0IGFic0Zsb29yIGZyb20gJy4uL3V0aWxzL2Ficy1mbG9vcic7XG52YXIgYWJzID0gTWF0aC5hYnM7XG5cbmV4cG9ydCBmdW5jdGlvbiB0b0lTT1N0cmluZygpIHtcbiAgICAvLyBmb3IgSVNPIHN0cmluZ3Mgd2UgZG8gbm90IHVzZSB0aGUgbm9ybWFsIGJ1YmJsaW5nIHJ1bGVzOlxuICAgIC8vICAqIG1pbGxpc2Vjb25kcyBidWJibGUgdXAgdW50aWwgdGhleSBiZWNvbWUgaG91cnNcbiAgICAvLyAgKiBkYXlzIGRvIG5vdCBidWJibGUgYXQgYWxsXG4gICAgLy8gICogbW9udGhzIGJ1YmJsZSB1cCB1bnRpbCB0aGV5IGJlY29tZSB5ZWFyc1xuICAgIC8vIFRoaXMgaXMgYmVjYXVzZSB0aGVyZSBpcyBubyBjb250ZXh0LWZyZWUgY29udmVyc2lvbiBiZXR3ZWVuIGhvdXJzIGFuZCBkYXlzXG4gICAgLy8gKHRoaW5rIG9mIGNsb2NrIGNoYW5nZXMpXG4gICAgLy8gYW5kIGFsc28gbm90IGJldHdlZW4gZGF5cyBhbmQgbW9udGhzICgyOC0zMSBkYXlzIHBlciBtb250aClcbiAgICB2YXIgc2Vjb25kcyA9IGFicyh0aGlzLl9taWxsaXNlY29uZHMpIC8gMTAwMDtcbiAgICB2YXIgZGF5cyAgICAgICAgID0gYWJzKHRoaXMuX2RheXMpO1xuICAgIHZhciBtb250aHMgICAgICAgPSBhYnModGhpcy5fbW9udGhzKTtcbiAgICB2YXIgbWludXRlcywgaG91cnMsIHllYXJzO1xuXG4gICAgLy8gMzYwMCBzZWNvbmRzIC0+IDYwIG1pbnV0ZXMgLT4gMSBob3VyXG4gICAgbWludXRlcyAgICAgICAgICAgPSBhYnNGbG9vcihzZWNvbmRzIC8gNjApO1xuICAgIGhvdXJzICAgICAgICAgICAgID0gYWJzRmxvb3IobWludXRlcyAvIDYwKTtcbiAgICBzZWNvbmRzICU9IDYwO1xuICAgIG1pbnV0ZXMgJT0gNjA7XG5cbiAgICAvLyAxMiBtb250aHMgLT4gMSB5ZWFyXG4gICAgeWVhcnMgID0gYWJzRmxvb3IobW9udGhzIC8gMTIpO1xuICAgIG1vbnRocyAlPSAxMjtcblxuXG4gICAgLy8gaW5zcGlyZWQgYnkgaHR0cHM6Ly9naXRodWIuY29tL2RvcmRpbGxlL21vbWVudC1pc29kdXJhdGlvbi9ibG9iL21hc3Rlci9tb21lbnQuaXNvZHVyYXRpb24uanNcbiAgICB2YXIgWSA9IHllYXJzO1xuICAgIHZhciBNID0gbW9udGhzO1xuICAgIHZhciBEID0gZGF5cztcbiAgICB2YXIgaCA9IGhvdXJzO1xuICAgIHZhciBtID0gbWludXRlcztcbiAgICB2YXIgcyA9IHNlY29uZHM7XG4gICAgdmFyIHRvdGFsID0gdGhpcy5hc1NlY29uZHMoKTtcblxuICAgIGlmICghdG90YWwpIHtcbiAgICAgICAgLy8gdGhpcyBpcyB0aGUgc2FtZSBhcyBDIydzIChOb2RhKSBhbmQgcHl0aG9uIChpc29kYXRlKS4uLlxuICAgICAgICAvLyBidXQgbm90IG90aGVyIEpTIChnb29nLmRhdGUpXG4gICAgICAgIHJldHVybiAnUDBEJztcbiAgICB9XG5cbiAgICByZXR1cm4gKHRvdGFsIDwgMCA/ICctJyA6ICcnKSArXG4gICAgICAgICdQJyArXG4gICAgICAgIChZID8gWSArICdZJyA6ICcnKSArXG4gICAgICAgIChNID8gTSArICdNJyA6ICcnKSArXG4gICAgICAgIChEID8gRCArICdEJyA6ICcnKSArXG4gICAgICAgICgoaCB8fCBtIHx8IHMpID8gJ1QnIDogJycpICtcbiAgICAgICAgKGggPyBoICsgJ0gnIDogJycpICtcbiAgICAgICAgKG0gPyBtICsgJ00nIDogJycpICtcbiAgICAgICAgKHMgPyBzICsgJ1MnIDogJycpO1xufVxuIiwiaW1wb3J0IHsgRHVyYXRpb24gfSBmcm9tICcuL2NvbnN0cnVjdG9yJztcblxudmFyIHByb3RvID0gRHVyYXRpb24ucHJvdG90eXBlO1xuXG5pbXBvcnQgeyBhYnMgfSBmcm9tICcuL2Ficyc7XG5pbXBvcnQgeyBhZGQsIHN1YnRyYWN0IH0gZnJvbSAnLi9hZGQtc3VidHJhY3QnO1xuaW1wb3J0IHsgYXMsIGFzTWlsbGlzZWNvbmRzLCBhc1NlY29uZHMsIGFzTWludXRlcywgYXNIb3VycywgYXNEYXlzLCBhc1dlZWtzLCBhc01vbnRocywgYXNZZWFycywgdmFsdWVPZiB9IGZyb20gJy4vYXMnO1xuaW1wb3J0IHsgYnViYmxlIH0gZnJvbSAnLi9idWJibGUnO1xuaW1wb3J0IHsgZ2V0LCBtaWxsaXNlY29uZHMsIHNlY29uZHMsIG1pbnV0ZXMsIGhvdXJzLCBkYXlzLCBtb250aHMsIHllYXJzLCB3ZWVrcyB9IGZyb20gJy4vZ2V0JztcbmltcG9ydCB7IGh1bWFuaXplIH0gZnJvbSAnLi9odW1hbml6ZSc7XG5pbXBvcnQgeyB0b0lTT1N0cmluZyB9IGZyb20gJy4vaXNvLXN0cmluZyc7XG5pbXBvcnQgeyBsYW5nLCBsb2NhbGUsIGxvY2FsZURhdGEgfSBmcm9tICcuLi9tb21lbnQvbG9jYWxlJztcblxucHJvdG8uYWJzICAgICAgICAgICAgPSBhYnM7XG5wcm90by5hZGQgICAgICAgICAgICA9IGFkZDtcbnByb3RvLnN1YnRyYWN0ICAgICAgID0gc3VidHJhY3Q7XG5wcm90by5hcyAgICAgICAgICAgICA9IGFzO1xucHJvdG8uYXNNaWxsaXNlY29uZHMgPSBhc01pbGxpc2Vjb25kcztcbnByb3RvLmFzU2Vjb25kcyAgICAgID0gYXNTZWNvbmRzO1xucHJvdG8uYXNNaW51dGVzICAgICAgPSBhc01pbnV0ZXM7XG5wcm90by5hc0hvdXJzICAgICAgICA9IGFzSG91cnM7XG5wcm90by5hc0RheXMgICAgICAgICA9IGFzRGF5cztcbnByb3RvLmFzV2Vla3MgICAgICAgID0gYXNXZWVrcztcbnByb3RvLmFzTW9udGhzICAgICAgID0gYXNNb250aHM7XG5wcm90by5hc1llYXJzICAgICAgICA9IGFzWWVhcnM7XG5wcm90by52YWx1ZU9mICAgICAgICA9IHZhbHVlT2Y7XG5wcm90by5fYnViYmxlICAgICAgICA9IGJ1YmJsZTtcbnByb3RvLmdldCAgICAgICAgICAgID0gZ2V0O1xucHJvdG8ubWlsbGlzZWNvbmRzICAgPSBtaWxsaXNlY29uZHM7XG5wcm90by5zZWNvbmRzICAgICAgICA9IHNlY29uZHM7XG5wcm90by5taW51dGVzICAgICAgICA9IG1pbnV0ZXM7XG5wcm90by5ob3VycyAgICAgICAgICA9IGhvdXJzO1xucHJvdG8uZGF5cyAgICAgICAgICAgPSBkYXlzO1xucHJvdG8ud2Vla3MgICAgICAgICAgPSB3ZWVrcztcbnByb3RvLm1vbnRocyAgICAgICAgID0gbW9udGhzO1xucHJvdG8ueWVhcnMgICAgICAgICAgPSB5ZWFycztcbnByb3RvLmh1bWFuaXplICAgICAgID0gaHVtYW5pemU7XG5wcm90by50b0lTT1N0cmluZyAgICA9IHRvSVNPU3RyaW5nO1xucHJvdG8udG9TdHJpbmcgICAgICAgPSB0b0lTT1N0cmluZztcbnByb3RvLnRvSlNPTiAgICAgICAgID0gdG9JU09TdHJpbmc7XG5wcm90by5sb2NhbGUgICAgICAgICA9IGxvY2FsZTtcbnByb3RvLmxvY2FsZURhdGEgICAgID0gbG9jYWxlRGF0YTtcblxuLy8gRGVwcmVjYXRpb25zXG5pbXBvcnQgeyBkZXByZWNhdGUgfSBmcm9tICcuLi91dGlscy9kZXByZWNhdGUnO1xuXG5wcm90by50b0lzb1N0cmluZyA9IGRlcHJlY2F0ZSgndG9Jc29TdHJpbmcoKSBpcyBkZXByZWNhdGVkLiBQbGVhc2UgdXNlIHRvSVNPU3RyaW5nKCkgaW5zdGVhZCAobm90aWNlIHRoZSBjYXBpdGFscyknLCB0b0lTT1N0cmluZyk7XG5wcm90by5sYW5nID0gbGFuZztcbiIsIi8vIFNpZGUgZWZmZWN0IGltcG9ydHNcbmltcG9ydCAnLi9wcm90b3R5cGUnO1xuXG5pbXBvcnQgeyBjcmVhdGVEdXJhdGlvbiB9IGZyb20gJy4vY3JlYXRlJztcbmltcG9ydCB7IGlzRHVyYXRpb24gfSBmcm9tICcuL2NvbnN0cnVjdG9yJztcbmltcG9ydCB7XG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lUm91bmRpbmcsXG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lVGhyZXNob2xkXG59IGZyb20gJy4vaHVtYW5pemUnO1xuXG5leHBvcnQge1xuICAgIGNyZWF0ZUR1cmF0aW9uLFxuICAgIGlzRHVyYXRpb24sXG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lUm91bmRpbmcsXG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lVGhyZXNob2xkXG59O1xuIiwiaW1wb3J0IHsgYWRkRm9ybWF0VG9rZW4gfSBmcm9tICcuLi9mb3JtYXQvZm9ybWF0JztcbmltcG9ydCB7IGFkZFJlZ2V4VG9rZW4sIG1hdGNoVGltZXN0YW1wLCBtYXRjaFNpZ25lZCB9IGZyb20gJy4uL3BhcnNlL3JlZ2V4JztcbmltcG9ydCB7IGFkZFBhcnNlVG9rZW4gfSBmcm9tICcuLi9wYXJzZS90b2tlbic7XG5pbXBvcnQgdG9JbnQgZnJvbSAnLi4vdXRpbHMvdG8taW50JztcblxuLy8gRk9STUFUVElOR1xuXG5hZGRGb3JtYXRUb2tlbignWCcsIDAsIDAsICd1bml4Jyk7XG5hZGRGb3JtYXRUb2tlbigneCcsIDAsIDAsICd2YWx1ZU9mJyk7XG5cbi8vIFBBUlNJTkdcblxuYWRkUmVnZXhUb2tlbigneCcsIG1hdGNoU2lnbmVkKTtcbmFkZFJlZ2V4VG9rZW4oJ1gnLCBtYXRjaFRpbWVzdGFtcCk7XG5hZGRQYXJzZVRva2VuKCdYJywgZnVuY3Rpb24gKGlucHV0LCBhcnJheSwgY29uZmlnKSB7XG4gICAgY29uZmlnLl9kID0gbmV3IERhdGUocGFyc2VGbG9hdChpbnB1dCwgMTApICogMTAwMCk7XG59KTtcbmFkZFBhcnNlVG9rZW4oJ3gnLCBmdW5jdGlvbiAoaW5wdXQsIGFycmF5LCBjb25maWcpIHtcbiAgICBjb25maWcuX2QgPSBuZXcgRGF0ZSh0b0ludChpbnB1dCkpO1xufSk7XG4iLCIvLyBTaWRlIGVmZmVjdCBpbXBvcnRzXG5pbXBvcnQgJy4vZGF5LW9mLW1vbnRoJztcbmltcG9ydCAnLi9kYXktb2Ytd2Vlayc7XG5pbXBvcnQgJy4vZGF5LW9mLXllYXInO1xuaW1wb3J0ICcuL2hvdXInO1xuaW1wb3J0ICcuL21pbGxpc2Vjb25kJztcbmltcG9ydCAnLi9taW51dGUnO1xuaW1wb3J0ICcuL21vbnRoJztcbmltcG9ydCAnLi9vZmZzZXQnO1xuaW1wb3J0ICcuL3F1YXJ0ZXInO1xuaW1wb3J0ICcuL3NlY29uZCc7XG5pbXBvcnQgJy4vdGltZXN0YW1wJztcbmltcG9ydCAnLi90aW1lem9uZSc7XG5pbXBvcnQgJy4vd2Vlay15ZWFyJztcbmltcG9ydCAnLi93ZWVrJztcbmltcG9ydCAnLi95ZWFyJztcblxuaW1wb3J0IHsgbm9ybWFsaXplVW5pdHMgfSBmcm9tICcuL2FsaWFzZXMnO1xuXG5leHBvcnQgeyBub3JtYWxpemVVbml0cyB9O1xuIiwiLy8hIG1vbWVudC5qc1xuLy8hIHZlcnNpb24gOiAyLjE3LjFcbi8vISBhdXRob3JzIDogVGltIFdvb2QsIElza3JlbiBDaGVybmV2LCBNb21lbnQuanMgY29udHJpYnV0b3JzXG4vLyEgbGljZW5zZSA6IE1JVFxuLy8hIG1vbWVudGpzLmNvbVxuXG5pbXBvcnQgeyBob29rcyBhcyBtb21lbnQsIHNldEhvb2tDYWxsYmFjayB9IGZyb20gJy4vbGliL3V0aWxzL2hvb2tzJztcblxubW9tZW50LnZlcnNpb24gPSAnMi4xNy4xJztcblxuaW1wb3J0IHtcbiAgICBtaW4sXG4gICAgbWF4LFxuICAgIG5vdyxcbiAgICBpc01vbWVudCxcbiAgICBtb21lbnRQcm90b3R5cGUgYXMgZm4sXG4gICAgY3JlYXRlVVRDICAgICAgIGFzIHV0YyxcbiAgICBjcmVhdGVVbml4ICAgICAgYXMgdW5peCxcbiAgICBjcmVhdGVMb2NhbCAgICAgYXMgbG9jYWwsXG4gICAgY3JlYXRlSW52YWxpZCAgIGFzIGludmFsaWQsXG4gICAgY3JlYXRlSW5ab25lICAgIGFzIHBhcnNlWm9uZVxufSBmcm9tICcuL2xpYi9tb21lbnQvbW9tZW50JztcblxuaW1wb3J0IHtcbiAgICBnZXRDYWxlbmRhckZvcm1hdFxufSBmcm9tICcuL2xpYi9tb21lbnQvY2FsZW5kYXInO1xuXG5pbXBvcnQge1xuICAgIGRlZmluZUxvY2FsZSxcbiAgICB1cGRhdGVMb2NhbGUsXG4gICAgZ2V0U2V0R2xvYmFsTG9jYWxlIGFzIGxvY2FsZSxcbiAgICBnZXRMb2NhbGUgICAgICAgICAgYXMgbG9jYWxlRGF0YSxcbiAgICBsaXN0TG9jYWxlcyAgICAgICAgYXMgbG9jYWxlcyxcbiAgICBsaXN0TW9udGhzICAgICAgICAgYXMgbW9udGhzLFxuICAgIGxpc3RNb250aHNTaG9ydCAgICBhcyBtb250aHNTaG9ydCxcbiAgICBsaXN0V2Vla2RheXMgICAgICAgYXMgd2Vla2RheXMsXG4gICAgbGlzdFdlZWtkYXlzTWluICAgIGFzIHdlZWtkYXlzTWluLFxuICAgIGxpc3RXZWVrZGF5c1Nob3J0ICBhcyB3ZWVrZGF5c1Nob3J0XG59IGZyb20gJy4vbGliL2xvY2FsZS9sb2NhbGUnO1xuXG5pbXBvcnQge1xuICAgIGlzRHVyYXRpb24sXG4gICAgY3JlYXRlRHVyYXRpb24gYXMgZHVyYXRpb24sXG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lUm91bmRpbmcgYXMgcmVsYXRpdmVUaW1lUm91bmRpbmcsXG4gICAgZ2V0U2V0UmVsYXRpdmVUaW1lVGhyZXNob2xkIGFzIHJlbGF0aXZlVGltZVRocmVzaG9sZFxufSBmcm9tICcuL2xpYi9kdXJhdGlvbi9kdXJhdGlvbic7XG5cbmltcG9ydCB7IG5vcm1hbGl6ZVVuaXRzIH0gZnJvbSAnLi9saWIvdW5pdHMvdW5pdHMnO1xuXG5pbXBvcnQgaXNEYXRlIGZyb20gJy4vbGliL3V0aWxzL2lzLWRhdGUnO1xuXG5zZXRIb29rQ2FsbGJhY2sobG9jYWwpO1xuXG5tb21lbnQuZm4gICAgICAgICAgICAgICAgICAgID0gZm47XG5tb21lbnQubWluICAgICAgICAgICAgICAgICAgID0gbWluO1xubW9tZW50Lm1heCAgICAgICAgICAgICAgICAgICA9IG1heDtcbm1vbWVudC5ub3cgICAgICAgICAgICAgICAgICAgPSBub3c7XG5tb21lbnQudXRjICAgICAgICAgICAgICAgICAgID0gdXRjO1xubW9tZW50LnVuaXggICAgICAgICAgICAgICAgICA9IHVuaXg7XG5tb21lbnQubW9udGhzICAgICAgICAgICAgICAgID0gbW9udGhzO1xubW9tZW50LmlzRGF0ZSAgICAgICAgICAgICAgICA9IGlzRGF0ZTtcbm1vbWVudC5sb2NhbGUgICAgICAgICAgICAgICAgPSBsb2NhbGU7XG5tb21lbnQuaW52YWxpZCAgICAgICAgICAgICAgID0gaW52YWxpZDtcbm1vbWVudC5kdXJhdGlvbiAgICAgICAgICAgICAgPSBkdXJhdGlvbjtcbm1vbWVudC5pc01vbWVudCAgICAgICAgICAgICAgPSBpc01vbWVudDtcbm1vbWVudC53ZWVrZGF5cyAgICAgICAgICAgICAgPSB3ZWVrZGF5cztcbm1vbWVudC5wYXJzZVpvbmUgICAgICAgICAgICAgPSBwYXJzZVpvbmU7XG5tb21lbnQubG9jYWxlRGF0YSAgICAgICAgICAgID0gbG9jYWxlRGF0YTtcbm1vbWVudC5pc0R1cmF0aW9uICAgICAgICAgICAgPSBpc0R1cmF0aW9uO1xubW9tZW50Lm1vbnRoc1Nob3J0ICAgICAgICAgICA9IG1vbnRoc1Nob3J0O1xubW9tZW50LndlZWtkYXlzTWluICAgICAgICAgICA9IHdlZWtkYXlzTWluO1xubW9tZW50LmRlZmluZUxvY2FsZSAgICAgICAgICA9IGRlZmluZUxvY2FsZTtcbm1vbWVudC51cGRhdGVMb2NhbGUgICAgICAgICAgPSB1cGRhdGVMb2NhbGU7XG5tb21lbnQubG9jYWxlcyAgICAgICAgICAgICAgID0gbG9jYWxlcztcbm1vbWVudC53ZWVrZGF5c1Nob3J0ICAgICAgICAgPSB3ZWVrZGF5c1Nob3J0O1xubW9tZW50Lm5vcm1hbGl6ZVVuaXRzICAgICAgICA9IG5vcm1hbGl6ZVVuaXRzO1xubW9tZW50LnJlbGF0aXZlVGltZVJvdW5kaW5nID0gcmVsYXRpdmVUaW1lUm91bmRpbmc7XG5tb21lbnQucmVsYXRpdmVUaW1lVGhyZXNob2xkID0gcmVsYXRpdmVUaW1lVGhyZXNob2xkO1xubW9tZW50LmNhbGVuZGFyRm9ybWF0ICAgICAgICA9IGdldENhbGVuZGFyRm9ybWF0O1xubW9tZW50LnByb3RvdHlwZSAgICAgICAgICAgICA9IGZuO1xuXG5leHBvcnQgZGVmYXVsdCBtb21lbnQ7XG4iLCJcbmltcG9ydCAqIGFzIGQzIGZyb20gXCJkM1wiO1xuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xuXG5pbXBvcnQgbW9tZW50IGZyb20gJy4uLy4uL21vbWVudC9tb21lbnQnO1xuaW1wb3J0IERlZmF1bHQgZnJvbSAnLi4vZGVmYXVsdC5qcyc7XG5cbmNsYXNzIExheW91dCBleHRlbmRzIERlZmF1bHQge1xuICBjb25zdHJ1Y3Rvcihkb21haW5UeXBlKSB7XG4gICAgc3VwZXIoZG9tYWluVHlwZSk7XG4gICAgdGhpcy5kb21haW5UeXBlID0gZG9tYWluVHlwZTtcbiAgfVxuXG4gIG9yZ2FuaXplKGQzdmlzdWFsLCB6b25lSW5kZXgsIHpvbmVzKSB7XG4gICAgICBsZXQgem9uZSA9IHpvbmVzW3pvbmVJbmRleF07XG5cbiAgICAgIGxldCBiYXNlID0gbmV3IERhdGUoem9uZS5zdGFydCArICctMDEnKTtcbiAgICAgIGxldCBvZmZzZXQgPSBiYXNlLmdldERheSgpO1xuXG4gICAgICAvLyAgbW9tZW50KCkuZGlmZihkYXRlX3RpbWUsICdtaW51dGVzJylcbiAgICAgIGQzdmlzdWFsLnNlbGVjdChcImcubm9kZVNldFwiKS5zZWxlY3RBbGwoXCJnLm5vZGUsIGcubm9kZWNcIikuZmlsdGVyKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICBjb25zdCBpbmRleCA9IGQuYm91bmRhcnkuY2hhckNvZGVBdCgwKSAtIDY1O1xuICAgICAgICAgcmV0dXJuIChpbmRleCA9PSB6b25lSW5kZXgpO1xuICAgICAgfSlcbiAgICAgIC5lYWNoKGZ1bmN0aW9uIChkLCBpKSB7XG4gICAgICAgICBpZiAoem9uZS51bml0ID09IFwibW9udGhseVwiICYmIGQuaGFzT3duUHJvcGVydHkoXCJzdGFydERhdGVcIikpIHtcbiAgICAgICAgICAgIGxldCBkYXlzID0gbW9tZW50KGJhc2UpLmFkZChOdW1iZXIob2Zmc2V0KSArIE51bWJlcihkLnN0YXJ0RGF0ZSksICdkYXlzJykuZGlmZihiYXNlLCAnZGF5cycpO1xuXG4gICAgICAgICAgICBkLnggPSAxMDAgKyAoZGF5cyAqIDIyKTtcbiAgICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5zZWxlY3QoXCJyZWN0XCIpXG4gICAgICAgICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gKGQuaGFzT3duUHJvcGVydHkoXCJkdXJhdGlvblwiKSA/IGQuZHVyYXRpb24gKiAyMiA6IDEwMCk7IH0gKVxuICAgICAgICAgfVxuICAgICAgICAgaWYgKHpvbmUudW5pdCA9PSBcInF1YXJ0ZXJseVwiICYmIGQuaGFzT3duUHJvcGVydHkoXCJzdGFydERhdGVcIikpIHtcbiAgICAgICAgICAgIGxldCBtb250aHMgPSBtb21lbnQoYmFzZSkuYWRkKE51bWJlcihkLnN0YXJ0RGF0ZSksICdtb250aHMnKS5kaWZmKGJhc2UsICdtb250aHMnKTtcblxuICAgICAgICAgICAgZC54ID0gMTAwICsgKG1vbnRocyAqIDMwKTtcblxuICAgICAgICAgICAgZDMuc2VsZWN0KHRoaXMpLnNlbGVjdChcInJlY3RcIilcbiAgICAgICAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIGZ1bmN0aW9uIChkKSB7IHJldHVybiAoZC5oYXNPd25Qcm9wZXJ0eShcImR1cmF0aW9uXCIpID8gZC5kdXJhdGlvbiAqIDMwIDogMTAwKTsgfSApXG4gICAgICAgICAgICBkMy5zZWxlY3QodGhpcykuc2VsZWN0KFwiY2lyY2xlXCIpXG4gICAgICAgICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIFwidHJhbnNsYXRlKFwiICsgKGQuaGFzT3duUHJvcGVydHkoXCJkdXJhdGlvblwiKSA/IChkLmR1cmF0aW9uICogMzApICsgNiA6IDEwMCkgKyBcIixcIiArIDMgKyBcIilcIjsgfSk7XG4gICAgICAgICB9XG4gICAgICB9KTtcbiAgfVxuXG4gIGJ1aWxkKGQzdmlzdWFsLCB6b25lLCByb290KSB7XG5cbiAgICAgIGZ1bmN0aW9uIGFkZE1vbnRocyhkYXRlT2JqLCBudW0pIHtcblxuICAgICAgICB2YXIgY3VycmVudE1vbnRoID0gZGF0ZU9iai5nZXRNb250aCgpO1xuICAgICAgICBkYXRlT2JqLnNldE1vbnRoKGRhdGVPYmouZ2V0TW9udGgoKSArIG51bSlcblxuICAgICAgICBpZiAoZGF0ZU9iai5nZXRNb250aCgpICE9ICgoY3VycmVudE1vbnRoICsgbnVtKSAlIDEyKSl7XG4gICAgICAgICAgICBkYXRlT2JqLnNldERhdGUoMCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhdGVPYmo7XG4gICAgICB9XG5cblxuICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgICBsZXQgb2JqID0gcm9vdC5hcHBlbmQoXCJnXCIpO1xuXG4gICAgICBvYmouY2xhc3NlZChcIl96b25lXCIsIHRydWUpO1xuICAgICAgb2JqLmNsYXNzZWQoXCJfXCIgKyB0aGlzLmRvbWFpblR5cGUsIHRydWUpO1xuXG4vLyAgICAgIGxldCB2YiA9IGQzdmlzdWFsLmF0dHIoXCJ2aWV3Qm94XCIpLnNwbGl0KCcgJyk7XG4vL1xuLy8gICAgICBsZXQgdmlld1dpZHRoID0gdmJbMl07XG4vLyAgICAgIGxldCB2aWV3SGVpZ2h0ID0gdmJbM107XG4vL1xuLy8gICAgICBsZXQgcGFnZUhlaWdodCA9IGQzdmlzdWFsLm5vZGUoKS5wYXJlbnROb2RlLmNsaWVudEhlaWdodDtcbi8vICAgICAgbGV0IHBhZ2VXaWR0aCA9IGQzdmlzdWFsLm5vZGUoKS5wYXJlbnROb2RlLmNsaWVudFdpZHRoO1xuLy8gICAgICAvLyBUSGlzIG5lZWRzIHRvIGJlIGEgY2FsY3VsYXRpb24gYmFzZWQgb24gYSByYXRpb1xuLy9cbi8vICAgICAgbGV0IHBhZ2VIZWlnaHRTZXQgPSAodHlwZW9mIHpvbmUucmVjdGFuZ2xlWzNdID09IFwic3RyaW5nXCIgJiYgem9uZS5yZWN0YW5nbGVbM10uaW5kZXhPZihcInBhZ2VIZWlnaHRcIikgIT0gLTEpO1xuLy9cbi8vICAgICAgZm9yICggbGV0IHYgPSAwOyB2IDwgNDsgdisrKSB7XG4vLyAgICAgICAgICBpZiAodHlwZW9mIHpvbmUucmVjdGFuZ2xlW3ZdID09IFwic3RyaW5nXCIpIHtcbi8vICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkV2YWx1YXRpbmc6IFwiICsgem9uZS5yZWN0YW5nbGVbdl0pO1xuLy8gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiIC4uLiB0byA6IFwiICsgZXZhbCh6b25lLnJlY3RhbmdsZVt2XSkpO1xuLy8gICAgICAgICAgICB6b25lLnJlY3RhbmdsZVt2XSA9IGV2YWwoem9uZS5yZWN0YW5nbGVbdl0pO1xuLy8gICAgICAgICAgfVxuLy8gICAgICB9XG4vL1xuLy8gICAgICBpZiAocGFnZUhlaWdodFNldCkge1xuLy8gICAgICAgICAgem9uZS5yZWN0YW5nbGVbM10gPSAoKHpvbmUucmVjdGFuZ2xlWzJdK3pvbmUucmVjdGFuZ2xlWzBdKSAqIHBhZ2VIZWlnaHQvcGFnZVdpZHRoKSAtIDIwO1xuLy8gICAgICB9XG5cbiAgICAgIHRoaXMuY2FsY3VsYXRlUmVjdGFuZ2xlIChkM3Zpc3VhbCwgem9uZSk7XG5cbiAgICAgIGxldCByZWN0ID0gb2JqLmFwcGVuZChcInJlY3RcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIFwiYmFja2dyb3VuZFwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbMF0pXG4gICAgICAgICAgLmF0dHIoXCJ5XCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsxXSlcbiAgICAgICAgICAuYXR0cihcInJ4XCIsIDgpXG4gICAgICAgICAgLmF0dHIoXCJyeVwiLCA4KVxuICAgICAgICAgIC5hdHRyKFwid2lkdGhcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzJdKVxuICAgICAgICAgIC5hdHRyKFwiaGVpZ2h0XCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVszXSk7Ly9mZmY4OTlcblxuXG4gICAgICAvLyAyNiB3ZWVrc1xuICAgICAgLy8gOTAqNCA9IDM2MFxuXG4gICAgICAvLyB6b25lLnVuaXQgPT0gcXVhcnRlcmx5IDogc2hvdyB0aGUgbW9udGggbGluZXNcbiAgICAgIC8vIHpvbmUudW5pdCA9PSBtb250aGx5IDogc2hvdyB0aGUgd2Vla2x5IGxpbmVzICh3aXRoIHdlZWtlbmQpXG5cbiAgICAgIGxldCBtaWxlc3RvbmVHYXAgPSAwO1xuICAgICAgbGV0IG1pbGVzdG9uZXMgPSBbXTtcbiAgICAgIGxldCBiYXJzID0gW107XG4gICAgICBsZXQgbGV2ZWwxID0gW107XG4gICAgICBpZiAoem9uZS51bml0ID09IFwibW9udGhseVwiKSB7XG4gICAgICAgIGNvbnN0IGRheXMgPSBbXCJzdW5cIixcIm1vblwiLFwidHVlXCIsXCJ3ZWRcIixcInRodVwiLFwiZnJpXCIsXCJzYXRcIl07XG4gICAgICAgIHZhciBkYXRlID0gbmV3IERhdGUoem9uZS5zdGFydCArICctMDEnKTtcblxuICAgICAgICBsZXQgc2VnbWVudFdpZHRoID0gMjI7XG4gICAgICAgIG1pbGVzdG9uZUdhcCA9IDIxMDtcblxuICAgICAgICBsZXQgb2Zmc2V0ID0gZGF0ZS5nZXREYXkoKTtcbiAgICAgICAgaWYgKG9mZnNldCA+PSAwICYmIG9mZnNldCA8PSA2KSB7XG4gICAgICAgICAgICBkYXRlLnNldERhdGUoZGF0ZS5nZXREYXRlKCkgLSBvZmZzZXQpO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPD0gb2Zmc2V0OyBpKyspIHtcbiAgICAgICAgICAgICAgICBtaWxlc3RvbmVzLnB1c2goe1wibGFiZWxcIjpcIlwiICsgZGF0ZS5nZXREYXRlKCkgKyBcIlwiLCBcInBvc2l0aW9uXCI6MTAwKyhpKnNlZ21lbnRXaWR0aCl9KTtcbiAgICAgICAgICAgICAgICBiYXJzLnB1c2goe1wid2lkdGhcIjpzZWdtZW50V2lkdGgsIFwiY2xhc3NcIjpkYXlzW2RhdGUuZ2V0RGF5KCldLCBcInBvc2l0aW9uXCI6MTAwICsgKGJhcnMubGVuZ3RoKnNlZ21lbnRXaWR0aCkgLSAoc2VnbWVudFdpZHRoLzIpIH0pO1xuICAgICAgICAgICAgICAgIGRhdGUuc2V0RGF0ZShkYXRlLmdldERhdGUoKSArIDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSAob2Zmc2V0KzEpOyBpIDwgNDA7IGkrKykge1xuICAgICAgICAgICAgbWlsZXN0b25lcy5wdXNoKHtcImxhYmVsXCI6XCJcIiArIGRhdGUuZ2V0RGF0ZSgpICsgXCJcIiwgXCJwb3NpdGlvblwiOjEwMCsoaSpzZWdtZW50V2lkdGgpfSk7XG4gICAgICAgICAgICBiYXJzLnB1c2goe1wid2lkdGhcIjpzZWdtZW50V2lkdGgsIFwiY2xhc3NcIjpkYXlzW2RhdGUuZ2V0RGF5KCldLCBcInBvc2l0aW9uXCI6MTAwICsgKGJhcnMubGVuZ3RoKnNlZ21lbnRXaWR0aCkgLSAoc2VnbWVudFdpZHRoLzIpIH0pO1xuICAgICAgICAgICAgZGF0ZS5zZXREYXRlKGRhdGUuZ2V0RGF0ZSgpICsgMSk7XG4gICAgICAgICAgICBpZiAoZGF0ZS5nZXREYXRlKCkgPT0gMSkge1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICgob2Zmc2V0KSA+IDApIHtcbiAgICAgICAgICAgIGxldCB0aXRsZSA9IG1vbWVudChkYXRlKS5zdWJ0cmFjdCgxLCAnbW9udGgnKS5mb3JtYXQoJ01NTU0gWVlZWScpO1xuICAgICAgICAgICAgbGV2ZWwxLnB1c2goe1wid2lkdGhcIjooc2VnbWVudFdpZHRoKihvZmZzZXQrMSkpLCBcImxhYmVsXCI6IHRpdGxlLCBcImNsYXNzXCI6XCJsZXZlbDFcIiwgXCJwb3NpdGlvblwiOjEwMCAtIChzZWdtZW50V2lkdGgvMikgfSk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHRpdGxlID0gbW9tZW50KGRhdGUpLmZvcm1hdCgnTU1NTSBZWVlZJyk7XG5cbiAgICAgICAgbGV2ZWwxLnB1c2goe1wid2lkdGhcIjooc2VnbWVudFdpZHRoKihiYXJzLmxlbmd0aC1vZmZzZXQtMSkpLCBcImxhYmVsXCI6IHRpdGxlLCBcImNsYXNzXCI6XCJsZXZlbDFcIiwgXCJwb3NpdGlvblwiOjEwMCArICgob2Zmc2V0KzEpKnNlZ21lbnRXaWR0aCkgLSAoc2VnbWVudFdpZHRoLzIpIH0pO1xuXG4gICAgICAgIHpvbmUuYmFycyA9IGJhcnM7XG5cbiAgICAgIH0gZWxzZSBpZiAoem9uZS51bml0ID09IFwicXVhcnRlcmx5XCIpIHtcblxuICAgICAgICBjb25zdCBxdWFydGVyID0gW1wicTFcIixcInEyXCIsXCJxM1wiLFwicTRcIl07XG4gICAgICAgIGNvbnN0IG1vbnRocyA9IFtcImphblwiLFwiZmViXCIsXCJtYXJcIixcImFwclwiLFwibWF5XCIsXCJqdW5cIixcImp1bFwiLFwiYXVnXCIsXCJzZXBcIixcIm9jdFwiLFwibm92XCIsXCJkZWNcIl07XG4gICAgICAgIGRhdGUgPSBuZXcgRGF0ZSh6b25lLnN0YXJ0ICsgJy0yMCcpO1xuXG4gICAgICAgIGxldCBzZWdtZW50cyA9IDMqOTsgLy8gMTAgcXVhcnRlcnNcblxuICAgICAgICBtaWxlc3RvbmVHYXAgPSBzZWdtZW50cyAqIDMwO1xuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc2VnbWVudHM7IGkrKykge1xuICAgICAgICAgICAgbWlsZXN0b25lcy5wdXNoKHtcImxhYmVsXCI6XCJcIiArIG1vbnRoc1tkYXRlLmdldE1vbnRoKCldICsgXCJcIiwgXCJwb3NpdGlvblwiOjEyMCsoKGkqMzApKzE1KX0pO1xuICAgICAgICAgICAgaWYgKGRhdGUuZ2V0TW9udGgoKSUzID09IDApIHtcbiAgICAgICAgICAgICAgICBiYXJzLnB1c2goe1wid2lkdGhcIjo5MCwgXCJjbGFzc1wiOnF1YXJ0ZXJbTWF0aC5mbG9vcihkYXRlLmdldE1vbnRoKCkvMyldLCBcInBvc2l0aW9uXCI6MTIwICsgKGJhcnMubGVuZ3RoKjkwKSB9KTtcbiAgICAgICAgICAgICAgICBpZiAoZGF0ZS5nZXRNb250aCgpJTEyID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKChzZWdtZW50cyAtIGkpIDwgMTIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsMS5wdXNoKHtcIndpZHRoXCI6KDMwKihzZWdtZW50cy1pKSktNSwgXCJsYWJlbFwiOiBkYXRlLmdldEZ1bGxZZWFyKCksIFwiY2xhc3NcIjpcImxldmVsMVwiLCBcInBvc2l0aW9uXCI6MTIwICsgKChiYXJzLmxlbmd0aC0xKSo5MCkgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXZlbDEucHVzaCh7XCJ3aWR0aFwiOigzMCozKjQpLTUsIFwibGFiZWxcIjogZGF0ZS5nZXRGdWxsWWVhcigpLCBcImNsYXNzXCI6XCJsZXZlbDFcIiwgXCJwb3NpdGlvblwiOjEyMCArICgoYmFycy5sZW5ndGgtMSkqOTApIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWRkTW9udGhzKGRhdGUsIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgem9uZS5iYXJzID0gYmFycztcblxuICAgICAgfVxuXG4gICAgICBvYmouc2VsZWN0QWxsKFwidGV4dFwiKS5kYXRhKG1pbGVzdG9uZXMpLmVudGVyKCkuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwidGV4dC1hbmNob3JcIiwgXCJtaWRkbGVcIilcbiAgICAgICAgICAuYXR0cihcInRyYW5zZm9ybVwiLCBmdW5jdGlvbiAoZCxpKSB7IHJldHVybiBcInRyYW5zbGF0ZShcIiArIChkLnBvc2l0aW9uKSArIFwiLDQwKVwiOyB9KVxuICAgICAgICAgIC50ZXh0KGZ1bmN0aW9uKGQpIHsgcmV0dXJuIGQubGFiZWw7IH0pO1xuXG4gICAgICBsZXQgY291bnQgPSAwO1xuICAgICAgb2JqLnNlbGVjdEFsbChcImxpbmVcIikuZGF0YSh6b25lLm1pbGVzdG9uZXMpLmVudGVyKCkuYXBwZW5kKFwibGluZVwiKVxuICAgICAgICAgIC5hdHRyKFwieDFcIiwgZnVuY3Rpb24gKGQsaSkgeyByZXR1cm4gMTIwICsgKG1pbGVzdG9uZUdhcCAqIGkpOyB9KVxuICAgICAgICAgIC5hdHRyKFwieDJcIiwgZnVuY3Rpb24gKGQsaSkgeyByZXR1cm4gMTIwICsgKG1pbGVzdG9uZUdhcCAqIGkpOyB9KVxuICAgICAgICAgIC5hdHRyKFwieTFcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzFdKVxuICAgICAgICAgIC5hdHRyKFwieTJcIiwgem9uZS5jYWxjdWxhdGVkUmVjdGFuZ2xlWzNdKVxuICAgICAgICAgIC5hdHRyKFwic3Ryb2tlXCIsIFwiZ3JheVwiKVxuICAgICAgICAgIC5hdHRyKFwic3Ryb2tlLXdpZHRoXCIsIFwiMVwiKTtcblxuICAgICAgY29uc3QgYmFubmVyID0gb2JqLmFwcGVuZChcImdcIikuc2VsZWN0QWxsKFwicmVjdFwiKS5kYXRhKGxldmVsMSkuZW50ZXIoKTtcblxuICAgICAgYmFubmVyXG4gICAgICAgIC5hcHBlbmQoXCJyZWN0XCIpXG4gICAgICAgIC5hdHRyKFwicnhcIiwgOClcbiAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gZC53aWR0aDsgfSlcbiAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgMTgpXG4gICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQuY2xhc3M7IH0pXG4gICAgICAgIC5hdHRyKFwieVwiLCAxMilcbiAgICAgICAgLmF0dHIoXCJ4XCIsIGZ1bmN0aW9uIChkLCBpKSB7IHJldHVybiBkLnBvc2l0aW9uOyB9KVxuXG4gICAgICBiYW5uZXJcbiAgICAgICAgLmFwcGVuZChcInRleHRcIilcbiAgICAgICAgLmF0dHIoXCJ0ZXh0LWFuY2hvclwiLCBcIm1pZGRsZVwiKVxuICAgICAgICAuY2xhc3NlZChcImJhbm5lclwiLCB0cnVlKVxuICAgICAgICAuYXR0cihcInRyYW5zZm9ybVwiLCBmdW5jdGlvbiAoZCxpKSB7IHJldHVybiBcInRyYW5zbGF0ZShcIiArICgoZC53aWR0aC8yKSArIChkLnBvc2l0aW9uKSkgKyBcIiwyNSlcIjsgfSlcbiAgICAgICAgLnRleHQoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC5sYWJlbDsgfSk7XG5cblxuICAgICAgb2JqLmFwcGVuZChcImdcIikuc2VsZWN0QWxsKFwicmVjdFwiKS5kYXRhKGJhcnMpLmVudGVyKCkuYXBwZW5kKFwicmVjdFwiKVxuICAgICAgICAuYXR0cihcIndpZHRoXCIsIGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLndpZHRoOyB9KVxuICAgICAgICAuYXR0cihcImhlaWdodFwiLCB6b25lLmNhbGN1bGF0ZWRSZWN0YW5nbGVbM10gLSA0MClcbiAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gZC5jbGFzczsgfSlcbiAgICAgICAgLmF0dHIoXCJ5XCIsIHpvbmUuY2FsY3VsYXRlZFJlY3RhbmdsZVsxXSArIDQwKVxuICAgICAgICAuYXR0cihcInhcIiwgZnVuY3Rpb24gKGQsIGkpIHsgcmV0dXJuIGQucG9zaXRpb247IH0pO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IExheW91dDsiLCJcbmltcG9ydCAqIGFzIGQzIGZyb20gXCJkM1wiO1xuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xuaW1wb3J0IHtWaWV3ZXJDYW52YXN9IGZyb20gJy4uLy4uL1ZpZXdlckNhbnZhcyc7XG5pbXBvcnQgRGVmYXVsdCBmcm9tICcuLi9kZWZhdWx0LmpzJztcblxuY2xhc3MgTGF5b3V0IGV4dGVuZHMgRGVmYXVsdCB7XG4gIGNvbnN0cnVjdG9yKGRvbWFpblR5cGUpIHtcbiAgICBzdXBlcihkb21haW5UeXBlKTtcbiAgICB0aGlzLmRvbWFpblR5cGUgPSBkb21haW5UeXBlO1xuICB9XG5cbiAgYnVpbGQoZDN2aXN1YWwsIHpvbmUsIHJvb3QpIHtcbiAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgICAgbGV0IG9iaiA9IHJvb3QuYXBwZW5kKFwiZ1wiKTtcblxuICAgICAgb2JqLmNsYXNzZWQoXCJfem9uZVwiLCB0cnVlKTtcbiAgICAgIG9iai5jbGFzc2VkKFwiX1wiICsgdGhpcy5kb21haW5UeXBlLCB0cnVlKTtcblxuICAgICAgbGV0IHZiID0gZDN2aXN1YWwuYXR0cihcInZpZXdCb3hcIikuc3BsaXQoJyAnKTtcblxuICAgICAgbGV0IHZpZXdXaWR0aCA9IHZiWzJdO1xuICAgICAgbGV0IHZpZXdIZWlnaHQgPSB2YlszXTtcblxuICAgICAgZm9yICggbGV0IHYgPSAwOyB2IDwgNDsgdisrKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiB6b25lLnJlY3RhbmdsZVt2XSA9PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiRXZhbHVhdGluZzogXCIgKyB6b25lLnJlY3RhbmdsZVt2XSk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiIC4uLiB0byA6IFwiICsgZXZhbCh6b25lLnJlY3RhbmdsZVt2XSkpO1xuICAgICAgICAgICAgem9uZS5yZWN0YW5nbGVbdl0gPSBldmFsKHpvbmUucmVjdGFuZ2xlW3ZdKTtcbiAgICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGxldCByZWN0ID0gb2JqLmFwcGVuZChcInJlY3RcIilcbiAgICAgICAgICAuc3R5bGUoXCJmaWx0ZXJcIiwgXCJ1cmwoI2Ryb3Atc2hhZG93KVwiKVxuICAgICAgICAgIC5hdHRyKFwieFwiLCB6b25lLnJlY3RhbmdsZVswXSlcbiAgICAgICAgICAuYXR0cihcInlcIiwgem9uZS5yZWN0YW5nbGVbMV0pXG4gICAgICAgICAgLmF0dHIoXCJ3aWR0aFwiLCB6b25lLnJlY3RhbmdsZVsyXSlcbiAgICAgICAgICAuYXR0cihcImhlaWdodFwiLCB6b25lLnJlY3RhbmdsZVszXSk7Ly9mZmY4OTlcblxuICB9XG5cbiAgZW5yaWNoRGF0YSAobm9kZXMpIHtcbi8vXG4vLyAgICAgIGxldCBzZWdzID0gW107XG4vLyAgICAgIGZvciAoIGxldCB4ID0gMCA7IHggPCBncmFwaC5ub2Rlcy5sZW5ndGg7IHgrKykge1xuLy8gICAgICAgICAgbGV0IHNlZyA9IChncmFwaC5ub2Rlc1t4XS50aXRsZSAmJiBncmFwaC5ub2Rlc1t4XS50aXRsZS5zZWdtZW50ID8gZ3JhcGgubm9kZXNbeF0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8gICAgICAgICAgaWYgKHNlZyAhPSBcIlwiICYmIHNlZ3MuaW5kZXhPZihzZWcpIDwgMCkge1xuLy8gICAgICAgICAgICAgIHNlZ3MucHVzaChzZWcpO1xuLy8gICAgICAgICAgfVxuLy8gICAgICB9XG5cbiAgICAgIHRoaXMucmVjdXJzZShub2RlcywgMCk7XG5cbi8vICAgICAgLy8gY2FsY3VsYXRlIHRoZSBzZWdtZW50IGxheW91dFxuLy8gICAgICAvL3ZhciBzZWdzID0gW1wiSmF2YVN0YWNrXCIsIFwiUmVhY3RTdGFja1wiLFwiQ29uZmlndXJhdGlvblwiLCBcIkFwcFwiLFwiRGF0YWJhc2VcIixcIk1vbml0b3JpbmdcIixcIkNvbmZpZ1wiLFwiU2VjdXJpdHlcIixcIkRlcGxveVwiLFwiUnVudGltZVwiLFwiSW5mcmFzdHJ1Y3R1cmVcIixcIkRldmVsb3BcIixcIlJlZ2lzdHJ5XCJdXG4vLyAgICAgIGZvciAoIGxldCBwID0gMDsgcCA8IHNlZ3MubGVuZ3RoOyBwKyspIHtcbi8vICAgICAgICAgIGxldCB0b3QgPSAwO1xuLy8gICAgICAgICAgbGV0IHgsbnVtO1xuLy8gICAgICAgICAgZm9yICggeCA9IDAgOyB4IDwgZ3JhcGgubm9kZXMubGVuZ3RoOyB4KyspIHtcbi8vICAgICAgICAgICAgICBsZXQgc2VnID0gKGdyYXBoLm5vZGVzW3hdLnRpdGxlICYmIGdyYXBoLm5vZGVzW3hdLnRpdGxlLnNlZ21lbnQgPyBncmFwaC5ub2Rlc1t4XS50aXRsZS5zZWdtZW50IDogXCJcIik7XG4vLyAgICAgICAgICAgICAgaWYgKHNlZyA9PSBzZWdzW3BdKSB7XG4vLyAgICAgICAgICAgICAgICAgIHRvdCsrO1xuLy8gICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICAgICBpZiAoZ3JhcGgubm9kZXNbeF0uY2hpbGRyZW4pIHtcbi8vICAgICAgICAgICAgICAgICAgbGV0IGxzID0gZ3JhcGgubm9kZXNbeF0uY2hpbGRyZW47XG4vLyAgICAgICAgICAgICAgICAgIGZvciAoIGxldCBudW1MMSA9IDAseEwxID0gMCA7IHhMMSA8IGxzLmxlbmd0aDsgeEwxKyspIHtcbi8vICAgICAgICAgICAgICAgICAgICAgIGxldCBzZWcgPSAobHNbeEwxXS50aXRsZSAmJiBsc1t4TDFdLnRpdGxlLnNlZ21lbnQgPyBsc1t4TDFdLnRpdGxlLnNlZ21lbnQgOiBcIlwiKTtcbi8vICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWcgPT0gc2Vnc1twXSkge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICB0b3QrKztcbi8vICAgICAgICAgICAgICAgICAgICAgIH1cbi8vXG4vLyAgICAgICAgICAgICAgICAgICAgICBpZiAobHNbeEwxXS5jaGlsZHJlbikge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBsc0wyID0gbHNbeEwxXS5jaGlsZHJlbjtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKCBsZXQgbnVtTDIgPSAwLHhMMiA9IDAgOyB4TDIgPCBsc0wyLmxlbmd0aDsgeEwyKyspIHtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHNlZyA9IChsc0wyW3hMMl0udGl0bGUgJiYgbHNMMlt4TDJdLnRpdGxlLnNlZ21lbnQgPyBsc0wyW3hMMl0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VnID09IHNlZ3NbcF0pIHtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3QrKztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgIH1cbi8vICAgICAgICAgIH1cbi8vICAgICAgICAgIGZvciAoIG51bSA9IDAseCA9IDAgOyB4IDwgZ3JhcGgubm9kZXMubGVuZ3RoOyB4KyspIHtcbi8vLy8gICAgICAgICAgICAgIHRoaXMucmVjdXJzZShncmFwaC5ub2RlcywgMSk7XG4vLyAgICAgICAgICAgICAgbGV0IHNlZyA9IChncmFwaC5ub2Rlc1t4XS50aXRsZSAmJiBncmFwaC5ub2Rlc1t4XS50aXRsZS5zZWdtZW50ID8gZ3JhcGgubm9kZXNbeF0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8vLyAgICAgICAgICAgICAgaWYgKHNlZyA9PSBzZWdzW3BdKSB7XG4vLy8vICAgICAgICAgICAgICAgICAgZ3JhcGgubm9kZXNbeF0ubGF5b3V0ID0geyBcInNlcVwiOm51bSxcInRvdGFsXCI6dG90LFwic2VnXCI6c2Vnc1twXSwgXCJzZWdudW1cIjpwLCBcImxldmVsXCI6MCB9O1xuLy8vLyAgICAgICAgICAgICAgICAgIG51bSsrO1xuLy8vLyAgICAgICAgICAgICAgfVxuLy8vLyAgICAgICAgICAgICAgdGhpcy5yZWN1cnNlKGdyYXBoLm5vZGVzW3hdLCAxKTtcbi8vLy8gICAgICAgICAgICAgIGlmIChncmFwaC5ub2Rlc1t4XS5jaGlsZHJlbikge1xuLy8vLyAgICAgICAgICAgICAgICAgIGxldCBscyA9IGdyYXBoLm5vZGVzW3hdLmNoaWxkcmVuO1xuLy8vLyAgICAgICAgICAgICAgICAgIGZvciAoIGxldCBudW1MMSA9IDAseEwxID0gMCA7IHhMMSA8IGxzLmxlbmd0aDsgeEwxKyspIHtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgbGV0IHNlZyA9IChsc1t4TDFdLnRpdGxlICYmIGxzW3hMMV0udGl0bGUuc2VnbWVudCA/IGxzW3hMMV0udGl0bGUuc2VnbWVudCA6IFwiXCIpO1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VnID09IHNlZ3NbcF0pIHtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgIGxzW3hMMV0ubGF5b3V0ID0geyBcInNlcVwiOm51bUwxLFwidG90YWxcIjp0b3QsXCJzZWdcIjpzZWdzW3BdLCBcInNlZ251bVwiOnAsIFwibGV2ZWxcIjoxIH07XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICBudW1MMSsrO1xuLy8vLyAgICAgICAgICAgICAgICAgICAgICB9XG4vLy8vICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVjdXJzZShsc1t4TDFdLCAyKTtcbi8vXG4vLy8vICAgICAgICAgICAgICAgICAgICAgIGlmIChsc1t4TDFdLmNoaWxkcmVuKSB7XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgbHNMMiA9IGxzW3hMMV0uY2hpbGRyZW47XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKCBsZXQgbnVtTDIgPSAwLHhMMiA9IDAgOyB4TDIgPCBsc0wyLmxlbmd0aDsgeEwyKyspIHtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VnID0gKGxzTDJbeEwyXS50aXRsZSAmJiBsc0wyW3hMMl0udGl0bGUuc2VnbWVudCA/IGxzTDJbeEwyXS50aXRsZS5zZWdtZW50IDogXCJcIik7XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlZyA9PSBzZWdzW3BdKSB7XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxzTDJbeEwyXS5sYXlvdXQgPSB7IFwic2VxXCI6bnVtTDIsXCJ0b3RhbFwiOnRvdCxcInNlZ1wiOnNlZ3NbcF0sIFwic2VnbnVtXCI6cCwgXCJsZXZlbFwiOjIgfTtcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVtTDIrKztcbi8vLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4vLy8vICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4vLy8vICAgICAgICAgICAgICAgICAgICAgIH1cbi8vXG4vLyAgICAgICAgICAgLy8gICAgICAgfVxuLy8gICAgICAgICAgICAvLyAgfVxuLy8gICAgICAgICAgfVxuLy8gICAgICB9XG4gIH1cblxuICByZWN1cnNlIChub2RlcywgbGV2ZWwpIHtcbiAgICAgIGxldCB0b3QgPSBub2Rlcy5sZW5ndGg7XG4gICAgICBmb3IgKCBsZXQgbnVtID0gMCx4ID0gMCA7IHggPCBub2Rlcy5sZW5ndGg7IHgrKykge1xuICAgICAgICAgIGNvbnN0IG5kID0gbm9kZXNbeF07XG4gICAgICAgICAgaWYgKG5kLnR5cGUgPT0gXCJwYW5lbFwiIHx8IGxldmVsID4gMCkge1xuICAgICAgICAgICAgICBuZC5sYXlvdXQgPSB7IFwic2VxXCI6bnVtLFwidG90YWxcIjp0b3QsXCJzZWdcIjpcIlNFR1wiK2xldmVsLCBcInNlZ251bVwiOmxldmVsLCBcImxldmVsXCI6bGV2ZWwgfTtcbiAgICAgICAgICAgICAgbnVtKys7XG4gICAgICAgICAgICAgIGlmIChuZC5jaGlsZHJlbikge1xuICAgICAgICAgICAgICAgICAgdGhpcy5yZWN1cnNlKG5kLmNoaWxkcmVuLCBsZXZlbCsxKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuXG4gIGNvbmZpZ0V2ZW50cyhkM3Zpc3VhbCwgbmV3Tm9kZXNUb3RhbCwgem9uZXMsIHZpZXdlcikge1xuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgICBsZXQgbmV3Tm9kZXMgPSBuZXdOb2Rlc1RvdGFsLmZpbHRlcihmdW5jdGlvbihkKSB7IHJldHVybiBkLnR5cGUgPT0gJ2ljb24nOyB9KTtcblxuICAgICAgbGV0IGZvcmNlID0gdmlld2VyLmZvcmNlO1xuXG4gICAgICAvLyBVc2UgeCBhbmQgeSBpZiBpdCB3YXMgcHJvdmlkZWQsIG90aGVyd2lzZSBzdGljayBpdCBpbiB0aGUgY2VudGVyIG9mIHRoZSB6b25lXG4gICAgICBuZXdOb2Rlc1RvdGFsXG4gICAgICAgICAgLmF0dHIoXCJjeFwiLCBmdW5jdGlvbihkKSB7IGNvbnN0IGRpbSA9IHNlbGYuZ2V0RGltKGQsIHpvbmVzKTsgcmV0dXJuIGQueCA9IChkLnggPyBkLnggOiAoZGltLngxICsgKGRpbS54Mi1kaW0ueDEpLzIpKTsgfSlcbiAgICAgICAgICAuYXR0cihcImN5XCIsIGZ1bmN0aW9uKGQpIHsgY29uc3QgZGltID0gc2VsZi5nZXREaW0oZCwgem9uZXMpOyByZXR1cm4gZC55ID0gKGQueSA/IGQueSA6IChkaW0ueTEgKyAoZGltLnkyLWRpbS55MSkvMikpOyB9KTtcblxuLy9ub2RlU2V0LCBpbmRleFxuICAgICAgbmV3Tm9kZXNcbiAgICAgICAgLm9uKCdtb3VzZWVudGVyJywgZnVuY3Rpb24gKGUpIHtcbi8vICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcIm5vZGUtaG92ZXJcIik7XG4vLyAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTk9ERTogXCIgK0pTT04uc3RyaW5naWZ5KG5vZGVTZXQpKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm9uKCdtb3VzZWxlYXZlJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIC8vJCh0aGlzKS5yZW1vdmVDbGFzcyhcIm5vZGUtaG92ZXJcIik7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZDMuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICBzZWxmLnRvZ2dsZVBhbmVsKHZpZXdlciwgdGhpcy5wYXJlbnROb2RlLCAoZS5uYW1lID09IFwiZG93blwiKSk7IC8qc2VsZi50b2dnbGVOYXZpZ2F0aW9uKHZpZXdlciwgZSwgdGhpcyk7ICovXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGZ1bmN0aW9uIGRyYWdzdGFydGVkKGQpIHtcbiAgICAgICAgICAgIGlmICghZDMuZXZlbnQuYWN0aXZlKSBmb3JjZS5hbHBoYVRhcmdldCgwLjMpLnJlc3RhcnQoKTtcbiAgICAgICAgICAgIGQuZnggPSBkLng7XG4gICAgICAgICAgICBkLmZ5ID0gZC55O1xuICAgICAgICAgICAgdmlld2VyLmRvd25YID0gTWF0aC5yb3VuZChkLnggKyBkLnkpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZHJhZ2dlZChkKSB7XG4gICAgICAgICAgICBkLmZ4ID0gZDMuZXZlbnQueDtcbiAgICAgICAgICAgIGQuZnkgPSBkMy5ldmVudC55O1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZHJhZ2VuZGVkKGQpIHtcbiAgICAgICAgICAgIGlmICghZDMuZXZlbnQuYWN0aXZlKSBmb3JjZS5hbHBoYVRhcmdldCgwKTtcbiAgICAgICAgICAgIGQuZnggPSBudWxsO1xuICAgICAgICAgICAgZC5meSA9IG51bGw7XG4gICAgICAgICAgICBzZXRUaW1lb3V0IChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmlld2VyLnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgICAgICAgfSwgMjAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIG5ld05vZGVzVG90YWwuY2FsbChkMy5kcmFnKClcbiAgICAgICAgICAgLm9uKFwic3RhcnRcIiwgZHJhZ3N0YXJ0ZWQpXG4gICAgICAgICAgIC5vbihcImRyYWdcIiwgZHJhZ2dlZClcbiAgICAgICAgICAgLm9uKFwiZW5kXCIsIGRyYWdlbmRlZCkpO1xuXG4gICAgICAgIG5ld05vZGVzLmVhY2goZnVuY3Rpb24gKGQsIGkpIHtcbiAgICAgICAgICAgIGlmIChpID09IDApIHtcbiAgICAgICAgICAgICAgICBzZWxmLnRvZ2dsZVBhbmVsICh2aWV3ZXIsIHRoaXMucGFyZW50Tm9kZSwgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICB9XG5cbiAgZ2V0RGltKGQsIHpvbmVzKSB7XG4gICAgICBjb25zdCBpbmRleCA9IGQuYm91bmRhcnkuY2hhckNvZGVBdCgwKSAtIDY1O1xuICAgICAgaWYgKHpvbmVzLmxlbmd0aCA8PSBpbmRleCkge1xuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJJTlZBTElEIEJPVU5EQVJZOiBcIiArIGQuYm91bmRhcnkpO1xuICAgICAgfVxuICAgICAgY29uc3QgcmVjdCA9IHpvbmVzW2luZGV4XS5yZWN0YW5nbGU7XG4gICAgICBjb25zdCBkaW0gPSB7eDE6cmVjdFswXSx5MTpyZWN0WzFdLHgyOnJlY3RbMF0gKyByZWN0WzJdLHkyOnJlY3RbMV0gKyByZWN0WzNdfTtcbiAgICAgIC8vY29uc29sZS5sb2coXCJLOiBcIiArIEpTT04uc3RyaW5naWZ5KGRpbSwgbnVsbCwgMikpO1xuICAgICAgcmV0dXJuIGRpbTtcbiAgfVxuXG4gIHRvZ2dsZVBhbmVsICh2aWV3ZXIsIHJvb3QsIGRvd24pIHtcbiAgICAgbGV0IHBhbmVscyA9IFtdO1xuICAgICBkMy5zZWxlY3Qocm9vdCkuc2VsZWN0QWxsKFwiZy5fcGFuZWxcIikuZWFjaCAoIGZ1bmN0aW9uIChkKSB7XG4gICAgICAgIHBhbmVscy5wdXNoKGQpO1xuICAgICAgICBkMy5zZWxlY3Qocm9vdCkuc2VsZWN0QWxsKFwiZy5ub2RlX1wiICsgZC5uYW1lKS5zdHlsZShcImRpc3BsYXlcIiwgXCJub25lXCIpO1xuICAgICB9KTtcblxuICAgICBpZiAodmlld2VyLmNob3NlblBhbmVsID49IDApIHtcbiAgICAgICAgbGV0IGRhdGEgPSBwYW5lbHNbdmlld2VyLmNob3NlblBhbmVsXTtcbiAgICAgICAgZDMuc2VsZWN0KHJvb3QpLnNlbGVjdEFsbChcImcubm9kZV9cIiArIGRhdGEubmFtZSkuc3R5bGUoXCJkaXNwbGF5XCIsIFwibm9uZVwiKTtcbiAgICAgfSBlbHNlIHtcbiAgICAgICAgdmlld2VyLmNob3NlblBhbmVsID0gLTE7XG4gICAgIH1cblxuICAgICBpZiAoZG93bikge1xuICAgICAgICAgdmlld2VyLmNob3NlblBhbmVsLS07XG4gICAgICAgICBpZiAodmlld2VyLmNob3NlblBhbmVsIDwgMCkge1xuICAgICAgICAgICAgdmlld2VyLmNob3NlblBhbmVsID0gMDsgLy8ocGFuZWxzLmxlbmd0aCAtIDEpO1xuICAgICAgICAgfVxuICAgICB9IGVsc2Uge1xuICAgICAgICAgdmlld2VyLmNob3NlblBhbmVsKys7XG4gICAgICAgICBpZiAodmlld2VyLmNob3NlblBhbmVsID49IHBhbmVscy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHZpZXdlci5jaG9zZW5QYW5lbCA9IChwYW5lbHMubGVuZ3RoIC0gMSk7XG4gICAgICAgICB9XG4gICAgIH1cbiAgICAgbGV0IGRhdGEgPSBwYW5lbHNbdmlld2VyLmNob3NlblBhbmVsXTtcbiAgICAgZDMuc2VsZWN0KHJvb3QpLnNlbGVjdEFsbChcImcubm9kZV9cIiArIGRhdGEubmFtZSkuc3R5bGUoXCJkaXNwbGF5XCIsIFwiYmxvY2tcIik7XG5cbiAgICAgdGhpcy51cGRhdGVUYWJsZU9mQ29udGVudHModmlld2VyLCByb290LCBwYW5lbHMpO1xuICB9XG5cbiAgdXBkYXRlVGFibGVPZkNvbnRlbnRzICh2aWV3ZXIsIHJvb3QsIHBhbmVscykge1xuICAgICBkMy5zZWxlY3Qocm9vdCkuc2VsZWN0KFwiZy5fY2Fyb3VzZWxcIikucmVtb3ZlKCk7XG5cbiAgICAgZDMuc2VsZWN0KHJvb3QpLmFwcGVuZChcImdcIikuY2xhc3NlZChcIl9jYXJvdXNlbFwiLCB0cnVlKS5zZWxlY3RBbGwoXCIudG9jXCIpXG4gICAgICAgIC5kYXRhKHBhbmVscylcbiAgICAgICAgLmVudGVyKClcbiAgICAgICAgLmFwcGVuZChcInRleHRcIilcbiAgICAgICAgLmNsYXNzZWQoXCJ0b2NcIiwgdHJ1ZSlcbiAgICAgICAgLnN0eWxlKFwiZmlsbFwiLCBcImJsYWNrXCIpXG4gICAgICAgIC5jbGFzc2VkKFwic2VsZWN0ZWRcIiwgZnVuY3Rpb24gKHgsIGkpIHsgcmV0dXJuIChpID09IHZpZXdlci5jaG9zZW5QYW5lbCA/IHRydWU6ZmFsc2UpOyB9KVxuICAgICAgICAuYXR0cihcInRyYW5zZm9ybVwiLCBmdW5jdGlvbiAoeCwgaSkgeyByZXR1cm4gXCJ0cmFuc2xhdGUoXCIgKyAxNSArIFwiLFwiICsgKDI1KyhpKjE0KSkgKyBcIilcIjsgfSlcbiAgICAgICAgLnRleHQoZnVuY3Rpb24gKHgpIHsgcmV0dXJuIHgubGFiZWw7IH0pO1xuXG4gICAgIGQzLnNlbGVjdChyb290KS5zZWxlY3RBbGwoXCIudG9jXCIpXG4gICAgICAgIC5kYXRhKHBhbmVscylcbiAgICAgICAgLmV4aXQoKS5yZW1vdmUoKTtcblxuICB9XG5cbiAgdG9nZ2xlTmF2aWdhdGlvbiAodmlld2VyLCBlLCBvYmplY3QpIHtcbiAgICAgZDMuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgaWYgKHZpZXdlci5kb3duWCA9PSBNYXRoLnJvdW5kKGUueCArIGUueSkpIHtcbiAgICAgICAgdmlld2VyLm5hdmlnYXRpb24udG9nZ2xlKGUsIGQzLnNlbGVjdChvYmplY3QpKTtcbiAgICAgfSBlbHNlIHtcbiAgICAgICAgdmlld2VyLm5hdmlnYXRpb24uY2xvc2UoZSk7XG4gICAgIH1cbiAgfVxuXG4gIG9yZ2FuaXplKGQzdmlzdWFsLCB6b25lSW5kZXgsIHpvbmVzKSB7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0OyIsIi8vZXhwb3J0ICogZnJvbSAnLi9WaWV3ZXJDYW52YXMnO1xuLy9leHBvcnQgKiBmcm9tICcuL1NoYXBlRmFjdG9yeSc7XG4vL2V4cG9ydCAqIGZyb20gJy4vTGF5b3V0RmFjdG9yeSc7XG5pbXBvcnQge1NoYXBlRmFjdG9yeX0gZnJvbSAnLi9TaGFwZUZhY3RvcnknO1xuaW1wb3J0IHtWaWV3ZXJDYW52YXN9IGZyb20gJy4vVmlld2VyQ2FudmFzJztcbmltcG9ydCB7TGF5b3V0RmFjdG9yeX0gZnJvbSAnLi9MYXlvdXRGYWN0b3J5JztcblxuZXhwb3J0IGZ1bmN0aW9uIHllbGwobmFtZSkgeyByZXR1cm4gYEhFWSAke25hbWUudG9VcHBlckNhc2UoKX0hIWA7IH1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRFVERSB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxufVxuXG5leHBvcnQgeyBTaGFwZUZhY3RvcnksIExheW91dEZhY3RvcnksIFZpZXdlckNhbnZhcyB9O1xuXG5leHBvcnQge1xuICB2ZXJzaW9uXG59IGZyb20gXCIuLi9idWlsZC9wYWNrYWdlXCI7XG5cblxuaW1wb3J0IENvbW1lbnRTaGFwZSBmcm9tICcuL3NoYXBlcy9jb21tZW50L3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgQ29tbWVudFNoYXBlKFwiY29tbWVudFwiKSk7XG5cbmltcG9ydCBBcHBsaWNhdGlvblNoYXBlIGZyb20gJy4vc2hhcGVzL2FwcGxpY2F0aW9uL3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgQXBwbGljYXRpb25TaGFwZShcImFwcGxpY2F0aW9uXCIpKTtcblxuaW1wb3J0IEljb24gZnJvbSAnLi9zaGFwZXMvaWNvbi9zaGFwZS5qcyc7XG5TaGFwZUZhY3RvcnkucmVnaXN0ZXIobmV3IEljb24oXCJpY29uXCIpKTtcbmltcG9ydCBJbWFnZSBmcm9tICcuL3NoYXBlcy9pbWFnZS9zaGFwZS5qcyc7XG5TaGFwZUZhY3RvcnkucmVnaXN0ZXIobmV3IEltYWdlKFwiaW1hZ2VcIikpO1xuXG5pbXBvcnQgQ2lyY2xlIGZyb20gJy4vc2hhcGVzL2NpcmNsZS9zaGFwZS5qcyc7XG5TaGFwZUZhY3RvcnkucmVnaXN0ZXIobmV3IENpcmNsZShcImNpcmNsZVwiKSk7XG5TaGFwZUZhY3RvcnkucmVnaXN0ZXIobmV3IENpcmNsZShcImNpcmNsZTJcIikpO1xuU2hhcGVGYWN0b3J5LnJlZ2lzdGVyKG5ldyBDaXJjbGUoXCJjaXJjbGUzXCIpKTtcblxuaW1wb3J0IE1pbGVzdG9uZSBmcm9tICcuL3NoYXBlcy9taWxlc3RvbmUvc2hhcGUuanMnO1xuU2hhcGVGYWN0b3J5LnJlZ2lzdGVyKG5ldyBNaWxlc3RvbmUoXCJtaWxlc3RvbmVcIikpO1xuXG5cbmltcG9ydCBCdXR0b24gZnJvbSAnLi9zaGFwZXMvYnV0dG9uL3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgQnV0dG9uKFwiYnV0dG9uXCIpKTtcblxuaW1wb3J0IFRpbWVsaW5lIGZyb20gJy4vc2hhcGVzL3RpbWVsaW5lL3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgVGltZWxpbmUoXCJ0aW1lbGluZVwiKSk7XG5cbmltcG9ydCBUaW1lbGluZTIgZnJvbSAnLi9zaGFwZXMvdGltZWxpbmUyL3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgVGltZWxpbmUyKFwidGltZWxpbmUyXCIpKTtcblxuaW1wb3J0IFBhbmVsIGZyb20gJy4vc2hhcGVzL3BhbmVsL3NoYXBlLmpzJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgUGFuZWwoXCJwYW5lbFwiKSk7XG5cbmltcG9ydCBEZWZhdWx0U2hhcGUgZnJvbSAnLi9zaGFwZXMvYm94L3NoYXBlJztcblNoYXBlRmFjdG9yeS5yZWdpc3RlcihuZXcgRGVmYXVsdFNoYXBlKFwiYm94XCIpKTtcblxuaW1wb3J0IFRhYlNoYXBlIGZyb20gJy4vc2hhcGVzL3RhYi9zaGFwZSc7XG5TaGFwZUZhY3RvcnkucmVnaXN0ZXIobmV3IFRhYlNoYXBlKFwidGFiXCIpKTtcblxuXG5pbXBvcnQgU3dpbWxhbmUgZnJvbSAnLi9sYXlvdXRzL3N3aW1sYW5lL2xheW91dC5qcyc7XG5MYXlvdXRGYWN0b3J5LnJlZ2lzdGVyKG5ldyBTd2ltbGFuZShcInN3aW1sYW5lXCIpKTtcblxuaW1wb3J0IERlZmF1bHRMYXlvdXQgZnJvbSAnLi9sYXlvdXRzL2RlZmF1bHQuanMnO1xuTGF5b3V0RmFjdG9yeS5yZWdpc3RlcihuZXcgRGVmYXVsdExheW91dChcImRlZmF1bHRcIikpO1xuXG5pbXBvcnQgVGFiTGF5b3V0IGZyb20gJy4vbGF5b3V0cy90YWJzL3RhYnMuanMnO1xuTGF5b3V0RmFjdG9yeS5yZWdpc3RlcihuZXcgVGFiTGF5b3V0KFwidGFic1wiKSk7XG5cbmltcG9ydCBUaW1lbGluZUxheW91dCBmcm9tICcuL2xheW91dHMvdGltZWxpbmUvdGltZWxpbmUuanMnO1xuTGF5b3V0RmFjdG9yeS5yZWdpc3RlcihuZXcgVGltZWxpbmVMYXlvdXQoXCJ0aW1lbGluZXNcIikpO1xuXG5pbXBvcnQgQ2Fyb3VzZWxMYXlvdXQgZnJvbSAnLi9sYXlvdXRzL2Nhcm91c2VsL2Nhcm91c2VsLmpzJztcbkxheW91dEZhY3RvcnkucmVnaXN0ZXIobmV3IENhcm91c2VsTGF5b3V0KFwiY2Fyb3VzZWxcIikpO1xuIl0sIm5hbWVzIjpbIl9TaGFwZUZhY3RvcnkiLCJzaGFwZXMiLCJPYmplY3QiLCJrZXlzIiwic2hhcGUiLCJoYXNPd25Qcm9wZXJ0eSIsIm9iamVjdCIsImRvbWFpblR5cGUiLCJTaGFwZUZhY3RvcnkiLCJOYXZpZ2F0aW9uIiwic3ZnIiwidmlld2VyIiwiY29udGVudE5vZGUiLCJzZWxmIiwiYXBwZW5kIiwiYXR0ciIsInN0YXRlIiwibWVudUl0ZW1zIiwib24iLCJlIiwiY2xvc2UiLCJpdGVtIiwicHVzaCIsInZpcyIsInJvb3QiLCJzZWxlY3QiLCJiYWNrZHJvcCIsInN0eWxlIiwibWVudU5vZGVzIiwic2VsZWN0QWxsIiwiZGF0YSIsIm5hdk5vZGVzIiwiZW50ZXIiLCJleGl0IiwicmVtb3ZlIiwidGV4dCIsImQiLCJpY29uIiwidGl0bGUiLCJ0cmlnZ2VyIiwibm9kZVNldCIsImluZGV4IiwicGFyZW50Tm9kZSIsImNsYXNzZWQiLCJtYXRjaGVkSXRlbSIsInBvcyIsIm5ld05vZGUiLCJyZW1vdmVDaGlsZCIsIm5vdGVzIiwiZWFjaCIsImJvdW5kZWQiLCJnZXRCQm94IiwiY2xvbmVOb2RlIiwibm9kZSIsImFwcGVuZENoaWxkIiwibGVmdCIsIngiLCJ0b3AiLCJ5Iiwid2lkdGgiLCJoZWlnaHQiLCJib3VuZGluZyIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsIm1vZGUiLCJkb09wZW4iLCJzZWxlY3RlZCIsInBvc2l0aW9uIiwic2NhbGUiLCJkZWx0YSIsInN0YXJ0IiwicG9zaXRpb25zIiwiaSIsImVsZW1lbnQiLCJib2R5UmVjdCIsImRvY3VtZW50IiwiYm9keSIsImVsZW1SZWN0Iiwib2Zmc2V0IiwicHJpbnRSZWN0IiwibWluSGVpZ2h0IiwiTWF0aCIsIm1heCIsImxlbmd0aCIsIm5vdGVOb2RlIiwiZDMiLCJodG1sIiwia2V5IiwidmFsdWUiLCJkaW1zIiwib2Zmc2V0MiIsImNvbnRlbnRIZWlnaHQiLCJ0cmFuc2l0aW9uIiwibWVudVBvc2l0aW9uIiwibG9nIiwiX0xheW91dEZhY3RvcnkiLCJsYXlvdXRzIiwibGF5b3V0IiwiTGF5b3V0RmFjdG9yeSIsIlZpZXdlckNhbnZhcyIsImRlZmF1bHRDb25maWciLCJjbGllbnRXaWR0aCIsImRvbU5vZGUiLCJpbml0Q2FudmFzIiwiZXZlbnQiLCJkb2MiLCJldmVudHMiLCJmdW5jIiwiZnVuY0luZGV4IiwiZWwiLCJwYXBlcldpZHRoIiwiZ2V0Q29uZmlnIiwicmF0aW8iLCJmaXhlZFdpZHRoIiwiZnVsbCIsIm93Iiwib2giLCJyZWN0Iiwiem9uZSIsImRlZnMiLCJmaWx0ZXIiLCJ6b25lUm9vdCIsImNhbGNMaW5rRGlzdGFuY2UiLCJhIiwiYiIsInNvdXJjZSIsImJvdW5kYXJ5IiwidGFyZ2V0IiwidHlwZSIsImhpbnQiLCJzaW11bGF0aW9uIiwiZm9yY2UiLCJkaXN0YW5jZSIsInN0cmVuZ3RoIiwiaXRlcmF0aW9ucyIsIm5hdkRhdGEiLCJmYSIsImJvb2siLCJ0aCIsIm5hdmlnYXRpb24iLCJhdHRhY2giLCJ1cGRhdGUiLCJ6b25lcyIsImNvbmZpZyIsInBhZ2VIZWlnaHQiLCJjbGllbnRIZWlnaHQiLCJwYWdlV2lkdGgiLCJmb3JFYWNoIiwiem9uZUluZGV4IiwiZ2V0TGF5b3V0IiwiYnVpbGQiLCJvcmdhbml6ZSIsImNsb3NlUG9wdXBzIiwiYmJveCIsInBhcmVudEJib3giLCJyZWZyZXNoU2l6ZSIsImNoYXJDb2RlQXQiLCJjYWxjdWxhdGVkUmVjdGFuZ2xlIiwicmVjdGFuZ2xlIiwiZGltIiwieDEiLCJ5MSIsIngyIiwieTIiLCJjIiwibmV3Tm9kZXMiLCJjaGlsZHJlbiIsIm5hbWUiLCJuZXdOb2Rlc0MiLCJuYW1lcyIsImdldFNoYXBlTmFtZXMiLCJubSIsInNoYXAiLCJnZXRTaGFwZSIsImdldERpbSIsImZ4IiwiZml4ZWQiLCJmeSIsImZpbHRlcmVkIiwiY29uZmlnRXZlbnRzIiwidHJhdmVyc2VDaGlsZHJlbiIsInJlZ2lzdGVyTGlua3MiLCJyZWZyZXNoWm9uZXMiLCJsaW5rSW5mbyIsImxpbmtzIiwibGlua0dyb3VwIiwiaW5zZXJ0IiwiY2xhc3MiLCJlbmRzIiwibm9kZXMiLCJlbnJpY2hEYXRhIiwidGlja2VkIiwicmFkaXVzIiwidyIsImgiLCJtaW4iLCJjb2xsaWRlIiwibG4iLCJjdXJ2ZSIsInN0ZXBCZWZvcmUiLCJzdGVwQWZ0ZXIiLCJjYXJkaW5hbCIsInNvdXJjZVBvaW50cyIsInRhcmdldFBvaW50cyIsImRlbHRhWCIsImRlbHRhWSIsInMxIiwiaW5mIiwiaW5kIiwidDEiLCJzb3VyY2VFbmQiLCJzdWJzdHJpbmciLCJ0YXJnZXRFbmQiLCJsaW5lRGF0YSIsImxhYmVsUG9zaXRpb24iLCJwb2ludCIsImdldFBvaW50QXRMZW5ndGgiLCJnZXRUb3RhbExlbmd0aCIsImxhYmVsIiwibGQiLCJjaXJjbGVTb3VyY2UiLCJzb3VyY2VQb2ludCIsImNpcmNsZVRhcmdldCIsInRhcmdldFBvaW50Iiwibm9kZUxpc3QiLCJqIiwiYWxwaGEiLCJwYWRkaW5nIiwibWF4UmFkaXVzIiwicXVhZHRyZWUiLCJyIiwibngxIiwibngyIiwibnkxIiwibnkyIiwidmlzaXQiLCJxdWFkIiwibCIsImFicyIsInNxcnQiLCJpc05hTiIsIm5kIiwibGluayIsImZpbmROb2RlIiwidmVyc2lvbiIsIlRleHRVdGlscyIsImluZGV4T2YiLCJzdWJzdHIiLCJjYXB0aW9uIiwibWF4V2lkdGgiLCJyb3VuZCIsInNwbGl0QnlXb3JkcyIsIm1heENoYXJzUGVyTGluZSIsIndvcmRzIiwic3BsaXQiLCJsaW5lIiwibGluZXMiLCJuIiwidGVzdExpbmUiLCJTaGFwZSIsImNoZ1NldCIsInRtcFR4dE5vZGUiLCJ0eHROb2RlIiwidGkiLCJzcGxpdEJ5V2lkdGgiLCJSZWN0YW5nbGVTaGFwZSIsImFwcElkIiwiYXBwTmFtZU5vZGUiLCJsYWJlbHMiLCJ0b3BMZWZ0IiwidG9wUmlnaHQiLCJib3R0b21MZWZ0IiwiYm90dG9tUmlnaHQiLCJpbWFnZSIsImFsZXJ0cyIsImFsZXJ0Tm9kZXMiLCJ0Iiwic3RvcFByb3BhZ2F0aW9uIiwiSlNPTiIsInN0cmluZ2lmeSIsImNpcmNsZSIsImNvbG9yIiwidGV4dE5vZGUiLCJzcGxpdEJ5TkwiLCJMYXlvdXQiLCJkM3Zpc3VhbCIsImRvd25YIiwidG9nZ2xlIiwiZHJhZ3N0YXJ0ZWQiLCJhY3RpdmUiLCJhbHBoYVRhcmdldCIsInJlc3RhcnQiLCJkcmFnZ2VkIiwiZHJhZ2VuZGVkIiwiY2FsbCIsImNsaWNrYWJsZSIsIm9iaiIsInpvbmVOb2RlIiwiY2FsY3VsYXRlUmVjdGFuZ2xlIiwidmIiLCJ2aWV3V2lkdGgiLCJ2aWV3SGVpZ2h0IiwicGFnZUhlaWdodFNldCIsInJlY3REaW1lbnNpb24iLCJ2IiwiZXZhbCIsIm9yaWVudGF0aW9uIiwidHJhbnNmb3JtIiwidGV4dE5vZGVzIiwic3BsaXRDYXB0aW9uIiwiTUFYSU1VTV9DSEFSU19QRVJfTElORSIsIkRlZmF1bHQiLCJ0YWJzVG9PcGVuIiwidGFiVG9PcGVuIiwidGFiTm9kZSIsImRhdHVtIiwicGFyZW50IiwidG8iLCJyZXZlcnNlIiwiY2xpY2tlZE9iamVjdCIsInRvZ2dsZVRhYiIsImJhZE5vZGVzIiwiZnVsbE5vZGVMaXN0IiwicmVjdXJzZSIsInJvb3RUYWciLCJsZXZlbCIsInRvdCIsIm51bSIsIm5ld05vZGVzVG90YWwiLCJhdHRhY2hJbmZvQ29udHJvbGxlciIsInRvZ2dsZU5hdmlnYXRpb24iLCJ0b3RhbCIsInNlcSIsInNlZ251bSIsInBiYm94IiwiY3VycmVudFRhYiIsInNpemUiLCJ0b2dnbGVPbiIsInR1cm5PZmZQcmV2aW91c1RhYiIsImR1cmF0aW9uIiwiY29udGVudCIsInVybCIsInJhbmRvbSIsImpzb24iLCJ2YyIsIl9pcnJlbGV2YW50X2RhdGEiLCJtb3VudE5vZGUiLCJzZXRTdGF0ZSIsInJlbmRlciIsInRhYiIsImNlaWwiLCJ0YWJEYXRhIiwicCIsInNlbGVjdGVkQ2hpbGRyZW4iLCJob29rQ2FsbGJhY2siLCJob29rcyIsImFwcGx5IiwiYXJndW1lbnRzIiwic2V0SG9va0NhbGxiYWNrIiwiY2FsbGJhY2siLCJpc0FycmF5IiwiaW5wdXQiLCJBcnJheSIsInByb3RvdHlwZSIsInRvU3RyaW5nIiwiaXNPYmplY3QiLCJpc09iamVjdEVtcHR5IiwiayIsImlzTnVtYmVyIiwiaXNEYXRlIiwiRGF0ZSIsIm1hcCIsImFyciIsImZuIiwicmVzIiwiaGFzT3duUHJvcCIsImV4dGVuZCIsInZhbHVlT2YiLCJjcmVhdGVVVEMiLCJmb3JtYXQiLCJsb2NhbGUiLCJzdHJpY3QiLCJjcmVhdGVMb2NhbE9yVVRDIiwidXRjIiwiZGVmYXVsdFBhcnNpbmdGbGFncyIsImdldFBhcnNpbmdGbGFncyIsIm0iLCJfcGYiLCJzb21lIiwiZnVuIiwibGVuIiwiaXNWYWxpZCIsIl9pc1ZhbGlkIiwiZmxhZ3MiLCJwYXJzZWRQYXJ0cyIsInBhcnNlZERhdGVQYXJ0cyIsImlzTm93VmFsaWQiLCJfZCIsImdldFRpbWUiLCJvdmVyZmxvdyIsImVtcHR5IiwiaW52YWxpZE1vbnRoIiwiaW52YWxpZFdlZWtkYXkiLCJudWxsSW5wdXQiLCJpbnZhbGlkRm9ybWF0IiwidXNlckludmFsaWRhdGVkIiwibWVyaWRpZW0iLCJfc3RyaWN0IiwiY2hhcnNMZWZ0T3ZlciIsInVudXNlZFRva2VucyIsImJpZ0hvdXIiLCJ1bmRlZmluZWQiLCJpc0Zyb3plbiIsImNyZWF0ZUludmFsaWQiLCJOYU4iLCJpc1VuZGVmaW5lZCIsIm1vbWVudFByb3BlcnRpZXMiLCJjb3B5Q29uZmlnIiwiZnJvbSIsInByb3AiLCJ2YWwiLCJfaXNBTW9tZW50T2JqZWN0IiwiX2kiLCJfZiIsIl9sIiwiX3R6bSIsIl9pc1VUQyIsIl9vZmZzZXQiLCJfbG9jYWxlIiwidXBkYXRlSW5Qcm9ncmVzcyIsIk1vbWVudCIsInVwZGF0ZU9mZnNldCIsImlzTW9tZW50IiwiYWJzRmxvb3IiLCJudW1iZXIiLCJmbG9vciIsInRvSW50IiwiYXJndW1lbnRGb3JDb2VyY2lvbiIsImNvZXJjZWROdW1iZXIiLCJpc0Zpbml0ZSIsImNvbXBhcmVBcnJheXMiLCJhcnJheTEiLCJhcnJheTIiLCJkb250Q29udmVydCIsImxlbmd0aERpZmYiLCJkaWZmcyIsIndhcm4iLCJtc2ciLCJzdXBwcmVzc0RlcHJlY2F0aW9uV2FybmluZ3MiLCJjb25zb2xlIiwiZGVwcmVjYXRlIiwiZmlyc3RUaW1lIiwiZGVwcmVjYXRpb25IYW5kbGVyIiwiYXJncyIsImFyZyIsImJhYmVsSGVscGVycy50eXBlb2YiLCJzbGljZSIsImpvaW4iLCJFcnJvciIsInN0YWNrIiwiZGVwcmVjYXRpb25zIiwiZGVwcmVjYXRlU2ltcGxlIiwiaXNGdW5jdGlvbiIsIkZ1bmN0aW9uIiwic2V0IiwiX2NvbmZpZyIsIl9vcmRpbmFsUGFyc2VMZW5pZW50IiwiUmVnRXhwIiwiX29yZGluYWxQYXJzZSIsIm1lcmdlQ29uZmlncyIsInBhcmVudENvbmZpZyIsImNoaWxkQ29uZmlnIiwiTG9jYWxlIiwiZGVmYXVsdENhbGVuZGFyIiwiY2FsZW5kYXIiLCJtb20iLCJub3ciLCJvdXRwdXQiLCJfY2FsZW5kYXIiLCJkZWZhdWx0TG9uZ0RhdGVGb3JtYXQiLCJsb25nRGF0ZUZvcm1hdCIsIl9sb25nRGF0ZUZvcm1hdCIsImZvcm1hdFVwcGVyIiwidG9VcHBlckNhc2UiLCJyZXBsYWNlIiwiZGVmYXVsdEludmFsaWREYXRlIiwiaW52YWxpZERhdGUiLCJfaW52YWxpZERhdGUiLCJkZWZhdWx0T3JkaW5hbCIsImRlZmF1bHRPcmRpbmFsUGFyc2UiLCJvcmRpbmFsIiwiX29yZGluYWwiLCJkZWZhdWx0UmVsYXRpdmVUaW1lIiwicmVsYXRpdmVUaW1lIiwid2l0aG91dFN1ZmZpeCIsInN0cmluZyIsImlzRnV0dXJlIiwiX3JlbGF0aXZlVGltZSIsInBhc3RGdXR1cmUiLCJkaWZmIiwiYWxpYXNlcyIsImFkZFVuaXRBbGlhcyIsInVuaXQiLCJzaG9ydGhhbmQiLCJsb3dlckNhc2UiLCJ0b0xvd2VyQ2FzZSIsIm5vcm1hbGl6ZVVuaXRzIiwidW5pdHMiLCJub3JtYWxpemVPYmplY3RVbml0cyIsImlucHV0T2JqZWN0Iiwibm9ybWFsaXplZElucHV0Iiwibm9ybWFsaXplZFByb3AiLCJwcmlvcml0aWVzIiwiYWRkVW5pdFByaW9yaXR5IiwicHJpb3JpdHkiLCJnZXRQcmlvcml0aXplZFVuaXRzIiwidW5pdHNPYmoiLCJ1Iiwic29ydCIsIm1ha2VHZXRTZXQiLCJrZWVwVGltZSIsImdldCIsInN0cmluZ0dldCIsInN0cmluZ1NldCIsInByaW9yaXRpemVkIiwiemVyb0ZpbGwiLCJ0YXJnZXRMZW5ndGgiLCJmb3JjZVNpZ24iLCJhYnNOdW1iZXIiLCJ6ZXJvc1RvRmlsbCIsInNpZ24iLCJwb3ciLCJmb3JtYXR0aW5nVG9rZW5zIiwibG9jYWxGb3JtYXR0aW5nVG9rZW5zIiwiZm9ybWF0RnVuY3Rpb25zIiwiZm9ybWF0VG9rZW5GdW5jdGlvbnMiLCJhZGRGb3JtYXRUb2tlbiIsInRva2VuIiwicGFkZGVkIiwibG9jYWxlRGF0YSIsInJlbW92ZUZvcm1hdHRpbmdUb2tlbnMiLCJtYXRjaCIsIm1ha2VGb3JtYXRGdW5jdGlvbiIsImFycmF5IiwiZm9ybWF0TW9tZW50IiwiZXhwYW5kRm9ybWF0IiwicmVwbGFjZUxvbmdEYXRlRm9ybWF0VG9rZW5zIiwibGFzdEluZGV4IiwidGVzdCIsIm1hdGNoMSIsIm1hdGNoMiIsIm1hdGNoMyIsIm1hdGNoNCIsIm1hdGNoNiIsIm1hdGNoMXRvMiIsIm1hdGNoM3RvNCIsIm1hdGNoNXRvNiIsIm1hdGNoMXRvMyIsIm1hdGNoMXRvNCIsIm1hdGNoMXRvNiIsIm1hdGNoVW5zaWduZWQiLCJtYXRjaFNpZ25lZCIsIm1hdGNoT2Zmc2V0IiwibWF0Y2hTaG9ydE9mZnNldCIsIm1hdGNoVGltZXN0YW1wIiwibWF0Y2hXb3JkIiwicmVnZXhlcyIsImFkZFJlZ2V4VG9rZW4iLCJyZWdleCIsInN0cmljdFJlZ2V4IiwiaXNTdHJpY3QiLCJnZXRQYXJzZVJlZ2V4Rm9yVG9rZW4iLCJ1bmVzY2FwZUZvcm1hdCIsInMiLCJyZWdleEVzY2FwZSIsIm1hdGNoZWQiLCJwMSIsInAyIiwicDMiLCJwNCIsInRva2VucyIsImFkZFBhcnNlVG9rZW4iLCJhZGRXZWVrUGFyc2VUb2tlbiIsIl93IiwiYWRkVGltZVRvQXJyYXlGcm9tVG9rZW4iLCJfYSIsIllFQVIiLCJNT05USCIsIkRBVEUiLCJIT1VSIiwiTUlOVVRFIiwiU0VDT05EIiwiTUlMTElTRUNPTkQiLCJXRUVLIiwiV0VFS0RBWSIsIm8iLCJkYXlzSW5Nb250aCIsInllYXIiLCJtb250aCIsIlVUQyIsImdldFVUQ0RhdGUiLCJtb250aHNTaG9ydCIsIm1vbnRocyIsIm1vbnRoc1Nob3J0UmVnZXgiLCJtb250aHNSZWdleCIsIm1vbnRoc1BhcnNlIiwiTU9OVEhTX0lOX0ZPUk1BVCIsImRlZmF1bHRMb2NhbGVNb250aHMiLCJsb2NhbGVNb250aHMiLCJfbW9udGhzIiwiaXNGb3JtYXQiLCJkZWZhdWx0TG9jYWxlTW9udGhzU2hvcnQiLCJsb2NhbGVNb250aHNTaG9ydCIsIl9tb250aHNTaG9ydCIsImhhbmRsZVN0cmljdFBhcnNlIiwibW9udGhOYW1lIiwiaWkiLCJsbGMiLCJ0b0xvY2FsZUxvd2VyQ2FzZSIsIl9tb250aHNQYXJzZSIsIl9sb25nTW9udGhzUGFyc2UiLCJfc2hvcnRNb250aHNQYXJzZSIsImxvY2FsZU1vbnRoc1BhcnNlIiwiX21vbnRoc1BhcnNlRXhhY3QiLCJzZXRNb250aCIsImRheU9mTW9udGgiLCJkYXRlIiwiZ2V0U2V0TW9udGgiLCJnZXREYXlzSW5Nb250aCIsImRlZmF1bHRNb250aHNTaG9ydFJlZ2V4IiwiX21vbnRoc1Nob3J0U3RyaWN0UmVnZXgiLCJfbW9udGhzU2hvcnRSZWdleCIsImRlZmF1bHRNb250aHNSZWdleCIsIl9tb250aHNTdHJpY3RSZWdleCIsIl9tb250aHNSZWdleCIsImNvbXB1dGVNb250aHNQYXJzZSIsImNtcExlblJldiIsInNob3J0UGllY2VzIiwibG9uZ1BpZWNlcyIsIm1peGVkUGllY2VzIiwicGFyc2VUd29EaWdpdFllYXIiLCJwYXJzZUludCIsImRheXNJblllYXIiLCJpc0xlYXBZZWFyIiwiZ2V0U2V0WWVhciIsImdldElzTGVhcFllYXIiLCJjcmVhdGVEYXRlIiwiTSIsIm1zIiwiZ2V0RnVsbFllYXIiLCJzZXRGdWxsWWVhciIsImNyZWF0ZVVUQ0RhdGUiLCJnZXRVVENGdWxsWWVhciIsInNldFVUQ0Z1bGxZZWFyIiwiZmlyc3RXZWVrT2Zmc2V0IiwiZG93IiwiZG95IiwiZndkIiwiZ2V0VVRDRGF5IiwiZndkbHciLCJkYXlPZlllYXJGcm9tV2Vla3MiLCJ3ZWVrIiwid2Vla2RheSIsImxvY2FsV2Vla2RheSIsIndlZWtPZmZzZXQiLCJkYXlPZlllYXIiLCJyZXNZZWFyIiwicmVzRGF5T2ZZZWFyIiwid2Vla09mWWVhciIsInJlc1dlZWsiLCJ3ZWVrc0luWWVhciIsIndlZWtPZmZzZXROZXh0IiwibG9jYWxlV2VlayIsIl93ZWVrIiwiZGVmYXVsdExvY2FsZVdlZWsiLCJsb2NhbGVGaXJzdERheU9mV2VlayIsImxvY2FsZUZpcnN0RGF5T2ZZZWFyIiwiZ2V0U2V0V2VlayIsImFkZCIsImdldFNldElTT1dlZWsiLCJ3ZWVrZGF5c01pbiIsIndlZWtkYXlzU2hvcnQiLCJ3ZWVrZGF5cyIsIndlZWtkYXlzTWluUmVnZXgiLCJ3ZWVrZGF5c1Nob3J0UmVnZXgiLCJ3ZWVrZGF5c1JlZ2V4Iiwid2Vla2RheXNQYXJzZSIsInBhcnNlV2Vla2RheSIsInBhcnNlSXNvV2Vla2RheSIsImRlZmF1bHRMb2NhbGVXZWVrZGF5cyIsImxvY2FsZVdlZWtkYXlzIiwiX3dlZWtkYXlzIiwiZGF5IiwiZGVmYXVsdExvY2FsZVdlZWtkYXlzU2hvcnQiLCJsb2NhbGVXZWVrZGF5c1Nob3J0IiwiX3dlZWtkYXlzU2hvcnQiLCJkZWZhdWx0TG9jYWxlV2Vla2RheXNNaW4iLCJsb2NhbGVXZWVrZGF5c01pbiIsIl93ZWVrZGF5c01pbiIsIndlZWtkYXlOYW1lIiwiX3dlZWtkYXlzUGFyc2UiLCJfc2hvcnRXZWVrZGF5c1BhcnNlIiwiX21pbldlZWtkYXlzUGFyc2UiLCJsb2NhbGVXZWVrZGF5c1BhcnNlIiwiX3dlZWtkYXlzUGFyc2VFeGFjdCIsIl9mdWxsV2Vla2RheXNQYXJzZSIsImdldFNldERheU9mV2VlayIsImdldERheSIsImdldFNldExvY2FsZURheU9mV2VlayIsImdldFNldElTT0RheU9mV2VlayIsImRlZmF1bHRXZWVrZGF5c1JlZ2V4IiwiX3dlZWtkYXlzU3RyaWN0UmVnZXgiLCJfd2Vla2RheXNSZWdleCIsImRlZmF1bHRXZWVrZGF5c1Nob3J0UmVnZXgiLCJfd2Vla2RheXNTaG9ydFN0cmljdFJlZ2V4IiwiX3dlZWtkYXlzU2hvcnRSZWdleCIsImRlZmF1bHRXZWVrZGF5c01pblJlZ2V4IiwiX3dlZWtkYXlzTWluU3RyaWN0UmVnZXgiLCJfd2Vla2RheXNNaW5SZWdleCIsImNvbXB1dGVXZWVrZGF5c1BhcnNlIiwibWluUGllY2VzIiwibWlucCIsInNob3J0cCIsImxvbmdwIiwiaEZvcm1hdCIsImhvdXJzIiwia0Zvcm1hdCIsIm1pbnV0ZXMiLCJzZWNvbmRzIiwibG93ZXJjYXNlIiwibWF0Y2hNZXJpZGllbSIsIl9tZXJpZGllbVBhcnNlIiwiX2lzUG0iLCJpc1BNIiwiX21lcmlkaWVtIiwicG9zMSIsInBvczIiLCJsb2NhbGVJc1BNIiwiY2hhckF0IiwiZGVmYXVsdExvY2FsZU1lcmlkaWVtUGFyc2UiLCJsb2NhbGVNZXJpZGllbSIsImlzTG93ZXIiLCJnZXRTZXRIb3VyIiwiYmFzZUNvbmZpZyIsImxvY2FsZXMiLCJsb2NhbGVGYW1pbGllcyIsImdsb2JhbExvY2FsZSIsIm5vcm1hbGl6ZUxvY2FsZSIsImNob29zZUxvY2FsZSIsIm5leHQiLCJsb2FkTG9jYWxlIiwib2xkTG9jYWxlIiwibW9kdWxlIiwiZXhwb3J0cyIsIl9hYmJyIiwiZ2V0U2V0R2xvYmFsTG9jYWxlIiwidmFsdWVzIiwiZ2V0TG9jYWxlIiwiZGVmaW5lTG9jYWxlIiwiYWJiciIsInBhcmVudExvY2FsZSIsInVwZGF0ZUxvY2FsZSIsImxpc3RMb2NhbGVzIiwiY2hlY2tPdmVyZmxvdyIsIl9vdmVyZmxvd0RheU9mWWVhciIsIl9vdmVyZmxvd1dlZWtzIiwiX292ZXJmbG93V2Vla2RheSIsImV4dGVuZGVkSXNvUmVnZXgiLCJiYXNpY0lzb1JlZ2V4IiwidHpSZWdleCIsImlzb0RhdGVzIiwiaXNvVGltZXMiLCJhc3BOZXRKc29uUmVnZXgiLCJjb25maWdGcm9tSVNPIiwiZXhlYyIsImFsbG93VGltZSIsImRhdGVGb3JtYXQiLCJ0aW1lRm9ybWF0IiwidHpGb3JtYXQiLCJpc28iLCJjb25maWdGcm9tU3RyaW5nIiwiY3JlYXRlRnJvbUlucHV0RmFsbGJhY2siLCJfdXNlVVRDIiwiZGVmYXVsdHMiLCJjdXJyZW50RGF0ZUFycmF5Iiwibm93VmFsdWUiLCJnZXRVVENNb250aCIsImdldE1vbnRoIiwiZ2V0RGF0ZSIsImNvbmZpZ0Zyb21BcnJheSIsImN1cnJlbnREYXRlIiwieWVhclRvVXNlIiwiX2RheU9mWWVhciIsIl9uZXh0RGF5Iiwic2V0VVRDTWludXRlcyIsImdldFVUQ01pbnV0ZXMiLCJkYXlPZlllYXJGcm9tV2Vla0luZm8iLCJ3ZWVrWWVhciIsInRlbXAiLCJ3ZWVrZGF5T3ZlcmZsb3ciLCJHRyIsIlciLCJFIiwiY3JlYXRlTG9jYWwiLCJjdXJXZWVrIiwiZ2ciLCJJU09fODYwMSIsImNvbmZpZ0Zyb21TdHJpbmdBbmRGb3JtYXQiLCJwYXJzZWRJbnB1dCIsInNraXBwZWQiLCJzdHJpbmdMZW5ndGgiLCJ0b3RhbFBhcnNlZElucHV0TGVuZ3RoIiwidW51c2VkSW5wdXQiLCJtZXJpZGllbUZpeFdyYXAiLCJob3VyIiwiaXNQbSIsIm1lcmlkaWVtSG91ciIsImNvbmZpZ0Zyb21TdHJpbmdBbmRBcnJheSIsInRlbXBDb25maWciLCJiZXN0TW9tZW50Iiwic2NvcmVUb0JlYXQiLCJjdXJyZW50U2NvcmUiLCJzY29yZSIsImNvbmZpZ0Zyb21PYmplY3QiLCJtaW51dGUiLCJzZWNvbmQiLCJtaWxsaXNlY29uZCIsImNyZWF0ZUZyb21Db25maWciLCJwcmVwYXJlQ29uZmlnIiwicHJlcGFyc2UiLCJjb25maWdGcm9tSW5wdXQiLCJpc1VUQyIsInByb3RvdHlwZU1pbiIsIm90aGVyIiwicHJvdG90eXBlTWF4IiwicGlja0J5IiwibW9tZW50cyIsIkR1cmF0aW9uIiwieWVhcnMiLCJxdWFydGVycyIsInF1YXJ0ZXIiLCJ3ZWVrcyIsImRheXMiLCJtaWxsaXNlY29uZHMiLCJfbWlsbGlzZWNvbmRzIiwiX2RheXMiLCJfZGF0YSIsIl9idWJibGUiLCJpc0R1cmF0aW9uIiwiYWJzUm91bmQiLCJzZXBhcmF0b3IiLCJ1dGNPZmZzZXQiLCJvZmZzZXRGcm9tU3RyaW5nIiwiY2h1bmtPZmZzZXQiLCJtYXRjaGVyIiwibWF0Y2hlcyIsImNodW5rIiwicGFydHMiLCJjbG9uZVdpdGhPZmZzZXQiLCJtb2RlbCIsImNsb25lIiwic2V0VGltZSIsImxvY2FsIiwiZ2V0RGF0ZU9mZnNldCIsImdldFRpbWV6b25lT2Zmc2V0IiwiZ2V0U2V0T2Zmc2V0Iiwia2VlcExvY2FsVGltZSIsImxvY2FsQWRqdXN0IiwiX2NoYW5nZUluUHJvZ3Jlc3MiLCJjcmVhdGVEdXJhdGlvbiIsImdldFNldFpvbmUiLCJzZXRPZmZzZXRUb1VUQyIsInNldE9mZnNldFRvTG9jYWwiLCJzdWJ0cmFjdCIsInNldE9mZnNldFRvUGFyc2VkT2Zmc2V0IiwidFpvbmUiLCJoYXNBbGlnbmVkSG91ck9mZnNldCIsImlzRGF5bGlnaHRTYXZpbmdUaW1lIiwiaXNEYXlsaWdodFNhdmluZ1RpbWVTaGlmdGVkIiwiX2lzRFNUU2hpZnRlZCIsInRvQXJyYXkiLCJpc0xvY2FsIiwiaXNVdGNPZmZzZXQiLCJpc1V0YyIsImFzcE5ldFJlZ2V4IiwiaXNvUmVnZXgiLCJyZXQiLCJkaWZmUmVzIiwicGFyc2VJc28iLCJtb21lbnRzRGlmZmVyZW5jZSIsImlucCIsInBhcnNlRmxvYXQiLCJwb3NpdGl2ZU1vbWVudHNEaWZmZXJlbmNlIiwiYmFzZSIsImlzQWZ0ZXIiLCJpc0JlZm9yZSIsImNyZWF0ZUFkZGVyIiwiZGlyZWN0aW9uIiwicGVyaW9kIiwiZHVyIiwidG1wIiwiYWRkU3VidHJhY3QiLCJpc0FkZGluZyIsImdldENhbGVuZGFyRm9ybWF0IiwibXlNb21lbnQiLCJ0aW1lIiwiZm9ybWF0cyIsInNvZCIsInN0YXJ0T2YiLCJjYWxlbmRhckZvcm1hdCIsImxvY2FsSW5wdXQiLCJlbmRPZiIsImlzQmV0d2VlbiIsImluY2x1c2l2aXR5IiwiaXNTYW1lIiwiaW5wdXRNcyIsImlzU2FtZU9yQWZ0ZXIiLCJpc1NhbWVPckJlZm9yZSIsImFzRmxvYXQiLCJ0aGF0Iiwiem9uZURlbHRhIiwibW9udGhEaWZmIiwid2hvbGVNb250aERpZmYiLCJhbmNob3IyIiwiYWRqdXN0IiwiYW5jaG9yIiwiZGVmYXVsdEZvcm1hdCIsImRlZmF1bHRGb3JtYXRVdGMiLCJ0b0lTT1N0cmluZyIsInRvRGF0ZSIsImluc3BlY3QiLCJwcmVmaXgiLCJkYXRldGltZSIsInN1ZmZpeCIsImlucHV0U3RyaW5nIiwicG9zdGZvcm1hdCIsImh1bWFuaXplIiwiZnJvbU5vdyIsInRvTm93IiwibmV3TG9jYWxlRGF0YSIsImxhbmciLCJpc29XZWVrZGF5IiwidW5peCIsInRvT2JqZWN0IiwidG9KU09OIiwicGFyc2luZ0ZsYWdzIiwiaW52YWxpZEF0IiwiY3JlYXRpb25EYXRhIiwiaXNvV2Vla1llYXIiLCJhZGRXZWVrWWVhckZvcm1hdFRva2VuIiwiZ2V0dGVyIiwiZ2V0U2V0V2Vla1llYXIiLCJnZXRTZXRXZWVrWWVhckhlbHBlciIsImdldFNldElTT1dlZWtZZWFyIiwiaXNvV2VlayIsImdldElTT1dlZWtzSW5ZZWFyIiwiZ2V0V2Vla3NJblllYXIiLCJ3ZWVrSW5mbyIsIndlZWtzVGFyZ2V0Iiwic2V0V2Vla0FsbCIsImRheU9mWWVhckRhdGEiLCJnZXRTZXRRdWFydGVyIiwiZ2V0U2V0RGF5T2ZNb250aCIsImdldFNldERheU9mWWVhciIsImdldFNldE1pbnV0ZSIsImdldFNldFNlY29uZCIsInBhcnNlTXMiLCJnZXRTZXRNaWxsaXNlY29uZCIsImdldFpvbmVBYmJyIiwiZ2V0Wm9uZU5hbWUiLCJwcm90byIsImlzb1dlZWtzIiwiaXNvV2Vla3NJblllYXIiLCJwYXJzZVpvbmUiLCJpc0RTVCIsInpvbmVBYmJyIiwiem9uZU5hbWUiLCJkYXRlcyIsImlzRFNUU2hpZnRlZCIsImNyZWF0ZVVuaXgiLCJjcmVhdGVJblpvbmUiLCJwcmVQYXJzZVBvc3RGb3JtYXQiLCJmaXJzdERheU9mWWVhciIsImZpcnN0RGF5T2ZXZWVrIiwiZmllbGQiLCJzZXR0ZXIiLCJsaXN0TW9udGhzSW1wbCIsIm91dCIsImxpc3RXZWVrZGF5c0ltcGwiLCJsb2NhbGVTb3J0ZWQiLCJzaGlmdCIsImxpc3RNb250aHMiLCJsaXN0TW9udGhzU2hvcnQiLCJsaXN0V2Vla2RheXMiLCJsaXN0V2Vla2RheXNTaG9ydCIsImxpc3RXZWVrZGF5c01pbiIsImxhbmdEYXRhIiwibWF0aEFicyIsImFic0NlaWwiLCJidWJibGUiLCJtb250aHNGcm9tRGF5cyIsIm1vbnRoc1RvRGF5cyIsImRheXNUb01vbnRocyIsImFzIiwibWFrZUFzIiwiYWxpYXMiLCJhc01pbGxpc2Vjb25kcyIsImFzU2Vjb25kcyIsImFzTWludXRlcyIsImFzSG91cnMiLCJhc0RheXMiLCJhc1dlZWtzIiwiYXNNb250aHMiLCJhc1llYXJzIiwibWFrZUdldHRlciIsInRocmVzaG9sZHMiLCJzdWJzdGl0dXRlVGltZUFnbyIsInBvc05lZ0R1cmF0aW9uIiwiZ2V0U2V0UmVsYXRpdmVUaW1lUm91bmRpbmciLCJyb3VuZGluZ0Z1bmN0aW9uIiwiZ2V0U2V0UmVsYXRpdmVUaW1lVGhyZXNob2xkIiwidGhyZXNob2xkIiwibGltaXQiLCJ3aXRoU3VmZml4IiwiWSIsIkQiLCJ0b0lzb1N0cmluZyIsIm1vbWVudCIsImludmFsaWQiLCJyZWxhdGl2ZVRpbWVSb3VuZGluZyIsInJlbGF0aXZlVGltZVRocmVzaG9sZCIsIk51bWJlciIsInN0YXJ0RGF0ZSIsImFkZE1vbnRocyIsImRhdGVPYmoiLCJjdXJyZW50TW9udGgiLCJzZXREYXRlIiwibWlsZXN0b25lR2FwIiwibWlsZXN0b25lcyIsImJhcnMiLCJsZXZlbDEiLCJzZWdtZW50V2lkdGgiLCJzZWdtZW50cyIsImNvdW50IiwiYmFubmVyIiwidG9nZ2xlUGFuZWwiLCJkb3duIiwicGFuZWxzIiwiY2hvc2VuUGFuZWwiLCJ1cGRhdGVUYWJsZU9mQ29udGVudHMiLCJ5ZWxsIiwiRFVERSIsInJlZ2lzdGVyIiwiQ29tbWVudFNoYXBlIiwiQXBwbGljYXRpb25TaGFwZSIsIkljb24iLCJJbWFnZSIsIkNpcmNsZSIsIk1pbGVzdG9uZSIsIkJ1dHRvbiIsIlRpbWVsaW5lIiwiVGltZWxpbmUyIiwiUGFuZWwiLCJEZWZhdWx0U2hhcGUiLCJUYWJTaGFwZSIsIlN3aW1sYW5lIiwiRGVmYXVsdExheW91dCIsIlRhYkxheW91dCIsIlRpbWVsaW5lTGF5b3V0IiwiQ2Fyb3VzZWxMYXlvdXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOztJQUVNQTs2QkFFWTs7O2FBQ0xDLE1BQUwsR0FBYyxFQUFkOzs7Ozt3Q0FHWTttQkFDTEMsT0FBT0MsSUFBUCxDQUFZLEtBQUtGLE1BQWpCLENBQVA7Ozs7aUNBR01HLE9BQU87Z0JBQ1QsQ0FBQyxLQUFLSCxNQUFMLENBQVlJLGNBQVosQ0FBMkJELEtBQTNCLENBQUwsRUFBd0M7dUJBQzdCLElBQVA7O21CQUVHLEtBQUtILE1BQUwsQ0FBWUcsS0FBWixDQUFQOzs7O2lDQUdNRSxRQUFRO2lCQUNUTCxNQUFMLENBQVlLLE9BQU9DLFVBQW5CLElBQWlDRCxNQUFqQzttQkFDTyxJQUFQOzs7Ozs7QUFJUixBQUFPLElBQUlFLGVBQWUsSUFBSVIsYUFBSixFQUFuQjs7SUNyQkRTOzs7Ozs7OzJCQUVJQyxLQUFLQyxRQUFRQyxhQUFhO1VBQzNCQyxPQUFPLElBQVg7V0FDS0gsR0FBTCxHQUFXQSxHQUFYO1dBQ0tFLFdBQUwsR0FBbUJBLFdBQW5CO1dBQ0tELE1BQUwsR0FBY0EsTUFBZDs7VUFFSUcsTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCLE9BQXJCLEVBQThCLEtBQTlCO1dBQ0tDLEtBQUwsR0FBYSxFQUFFLFFBQU8sUUFBVCxFQUFtQixZQUFXLENBQUMsQ0FBL0IsRUFBYjs7V0FFS0MsU0FBTCxHQUFpQixFQUFqQjs7YUFFT0MsRUFBUCxDQUFVLGlCQUFWLEVBQTZCLFVBQVVDLENBQVYsRUFBYTthQUNsQ0MsS0FBTDtPQURIOzs7O2dDQUtVQyxNQUFNO1dBQ1hKLFNBQUwsQ0FBZUssSUFBZixDQUFvQkQsSUFBcEI7Ozs7NkJBR087VUFDRlIsT0FBTyxJQUFYO1VBQ0lVLE1BQU0sS0FBS2IsR0FBZjs7V0FHS2MsSUFBTCxHQUFZRCxJQUFJRSxNQUFKLENBQVcsT0FBWCxFQUFvQlgsTUFBcEIsQ0FBMkIsR0FBM0IsQ0FBWjs7V0FFS1ksUUFBTCxHQUFnQkgsSUFBSUUsTUFBSixDQUFXLE9BQVgsRUFBb0JBLE1BQXBCLENBQTJCLEdBQTNCLEVBQWdDWCxNQUFoQyxDQUF1QyxNQUF2QyxFQUNiQyxJQURhLENBQ1IsTUFEUSxFQUNBLFNBREEsRUFFYkEsSUFGYSxDQUVSLEdBRlEsRUFFSCxDQUZHLEVBR2JBLElBSGEsQ0FHUixHQUhRLEVBR0gsQ0FIRyxFQUliQSxJQUphLENBSVIsSUFKUSxFQUlGLENBSkUsRUFLYkEsSUFMYSxDQUtSLElBTFEsRUFLRixDQUxFLEVBTWJBLElBTmEsQ0FNUixPQU5RLEVBTUMsS0FORCxFQU9iQSxJQVBhLENBT1IsUUFQUSxFQU9FLElBUEYsRUFRYlksS0FSYSxDQVFQLFlBUk8sRUFRTyxRQVJQLENBQWhCOztVQVVNQyxZQUFZTCxJQUFJRSxNQUFKLENBQVcsT0FBWCxFQUFvQkEsTUFBcEIsQ0FBMkIsR0FBM0IsRUFBZ0NJLFNBQWhDLENBQTBDLGFBQTFDLEVBQ1NDLElBRFQsQ0FDYyxLQUFLYixTQURuQixDQUFsQjs7VUFHTWMsV0FBV0gsVUFBVUksS0FBVixHQUNabEIsTUFEWSxDQUNMLEdBREssRUFFWkMsSUFGWSxDQUVQLE9BRk8sRUFFRSxXQUZGLEVBR1pZLEtBSFksQ0FHTixZQUhNLEVBR1EsUUFIUixDQUFqQjs7Z0JBS1VNLElBQVYsR0FBaUJDLE1BQWpCOztlQUVTcEIsTUFBVCxDQUFnQixNQUFoQixFQUNHQyxJQURILENBQ1EsTUFEUixFQUNnQixPQURoQixFQUVHQSxJQUZILENBRVEsR0FGUixFQUVhLENBRmIsRUFHR0EsSUFISCxDQUdRLEdBSFIsRUFHYSxDQUhiLEVBSUdBLElBSkgsQ0FJUSxJQUpSLEVBSWMsQ0FKZCxFQUtHQSxJQUxILENBS1EsSUFMUixFQUtjLENBTGQsRUFNR0EsSUFOSCxDQU1RLFdBTlIsRUFNcUIsWUFOckIsRUFPR0EsSUFQSCxDQU9RLE9BUFIsRUFPaUIsSUFQakIsRUFRR0EsSUFSSCxDQVFRLFFBUlIsRUFRa0IsSUFSbEI7O2VBVVNELE1BQVQsQ0FBZ0IsUUFBaEIsRUFDR2EsS0FESCxDQUNTLFFBRFQsRUFDbUIsU0FEbkIsRUFFR1osSUFGSCxDQUVRLE1BRlIsRUFFZ0IsT0FGaEIsRUFHR0EsSUFISCxDQUdRLFFBSFIsRUFHa0IsT0FIbEIsRUFJR0EsSUFKSCxDQUlRLGNBSlIsRUFJd0IsR0FKeEIsRUFLR0EsSUFMSCxDQUtRLEdBTFIsRUFLYSxJQUxiOztlQU9TRCxNQUFULENBQWdCLE1BQWhCLEVBQ0dhLEtBREgsQ0FDUyxRQURULEVBQ21CLFNBRG5CLEVBRUdaLElBRkgsQ0FFUSxJQUZSLEVBRWMsQ0FGZCxFQUdHQSxJQUhILENBR1EsSUFIUixFQUdjLEdBSGQsRUFJR0EsSUFKSCxDQUlRLGFBSlIsRUFJdUIsUUFKdkIsRUFLR0EsSUFMSCxDQUtRLG1CQUxSLEVBSzZCLFNBTDdCLEVBTUdZLEtBTkgsQ0FNUyxhQU5ULEVBTXdCLGFBTnhCLEVBT0dBLEtBUEgsQ0FPUyxXQVBULEVBT3NCLE1BUHRCLEVBUUdBLEtBUkgsQ0FRUyxNQVJULEVBUWlCLE9BUmpCLEVBU0dRLElBVEgsQ0FTUSxVQUFTQyxDQUFULEVBQVk7ZUFBUUEsRUFBRUMsSUFBVDtPQVRyQjs7ZUFXU3ZCLE1BQVQsQ0FBZ0IsTUFBaEIsRUFDR2EsS0FESCxDQUNTLFFBRFQsRUFDbUIsU0FEbkIsRUFFR1osSUFGSCxDQUVRLElBRlIsRUFFYyxDQUZkLEVBR0dBLElBSEgsQ0FHUSxJQUhSLEVBR2MsRUFIZCxFQUlHQSxJQUpILENBSVEsTUFKUixFQUlnQixPQUpoQixFQUtHQSxJQUxILENBS1EsYUFMUixFQUt1QixNQUx2QixFQU1HWSxLQU5ILENBTVMsV0FOVCxFQU1zQixPQU50QixFQU9HWixJQVBILENBT1EsV0FQUixFQU9xQixZQVByQixFQVFHb0IsSUFSSCxDQVFRLFVBQVNDLENBQVQsRUFBWTtlQUFTQSxFQUFFRSxLQUFUO09BUnRCOztXQVVLWixRQUFMLENBQ0dSLEVBREgsQ0FDTSxPQUROLEVBQ2UsVUFBVUMsQ0FBVixFQUFhO2FBQ2pCQyxLQUFMO09BRk47O2VBTUdGLEVBREgsQ0FDTSxPQUROLEVBQ2UsVUFBVUMsQ0FBVixFQUFhO2FBQ2pCUixNQUFMLENBQVk0QixPQUFaLENBQW9CLGVBQXBCLEVBQXFDLEVBQUMsUUFBTzFCLEtBQUtHLEtBQUwsQ0FBV2MsSUFBbkIsRUFBeUIsWUFBV1gsQ0FBcEMsRUFBckM7O09BRk47O2VBTVNVLFNBQVQsQ0FBbUIsa0JBQW5CLEVBQ0dYLEVBREgsQ0FDTSxZQUROLEVBQ29CLFVBQVVzQixPQUFWLEVBQW1CQyxLQUFuQixFQUEwQjtpQkFDeEMsQ0FBVSxLQUFLQyxVQUFmLEVBQTJCYixTQUEzQixDQUFxQyxhQUFyQyxFQUFvRGMsT0FBcEQsQ0FBNEQsV0FBNUQsRUFBeUUsSUFBekU7T0FGTixFQUlHekIsRUFKSCxDQUlNLFlBSk4sRUFJb0IsVUFBVUMsQ0FBVixFQUFhO2lCQUMzQixDQUFVLEtBQUt1QixVQUFmLEVBQTJCYixTQUEzQixDQUFxQyxhQUFyQyxFQUFvRGMsT0FBcEQsQ0FBNEQsV0FBNUQsRUFBeUUsS0FBekU7O09BTE47Ozs7MkJBVUl4QixHQUFHeUIsYUFBYTtVQUNoQi9CLE9BQU8sSUFBWDtVQUNJZ0MsTUFBTSxJQUFWLENBRm9CO1VBR2hCN0IsUUFBUUgsS0FBS0csS0FBakI7O1VBRUksT0FBT0EsTUFBTThCLE9BQWIsSUFBd0IsV0FBeEIsSUFBdUM5QixNQUFNOEIsT0FBTixJQUFpQixJQUE1RCxFQUFrRTs7Y0FFeERBLE9BQU4sQ0FBY0osVUFBZCxDQUF5QkssV0FBekIsQ0FBcUMvQixNQUFNOEIsT0FBM0M7Y0FDTUEsT0FBTixHQUFnQixJQUFoQjs7O1VBR0FFLFFBQVEsRUFBWjtVQUNJSixXQUFKLEVBQWlCO29CQUNISyxJQUFaLENBQWlCLFVBQVViLENBQVYsRUFBYTtjQUN4QmMsVUFBVSxLQUFLQyxPQUFMLEVBQWQ7ZUFDS25DLEtBQUwsQ0FBVzhCLE9BQVgsR0FBcUIsS0FBS00sU0FBTCxDQUFlLElBQWYsQ0FBckI7O2VBRUsxQyxHQUFMLENBQVNlLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUI0QixJQUF6QixHQUFnQ0MsV0FBaEMsQ0FBNEN6QyxLQUFLRyxLQUFMLENBQVc4QixPQUF2RDttQkFDQSxDQUFVakMsS0FBS0csS0FBTCxDQUFXOEIsT0FBckIsRUFDRzVCLEVBREgsQ0FDTSxPQUROLEVBQ2UsVUFBVUMsQ0FBVixFQUFhO2lCQUNqQkMsS0FBTDtXQUZOOztnQkFLTSxFQUFDbUMsTUFBS3BDLEVBQUVxQyxDQUFGLEdBQU1OLFFBQVFNLENBQXBCLEVBQXVCQyxLQUFJdEMsRUFBRXVDLENBQUYsR0FBTVIsUUFBUVEsQ0FBekMsRUFBNENDLE9BQU1ULFFBQVFTLEtBQTFELEVBQWlFQyxRQUFPVixRQUFRVSxNQUFoRixFQUF3RkMsVUFBUyxLQUFLQyxxQkFBTCxFQUFqRyxFQUFOOztrQkFFVTFCLEVBQUUvQixjQUFGLENBQWlCLE9BQWpCLElBQTRCK0IsRUFBRVksS0FBOUIsR0FBc0MsRUFBaEQ7ZUFDS2hDLEtBQUwsQ0FBV2MsSUFBWCxHQUFrQk0sQ0FBbEI7U0FiRjs7VUFnQkUsS0FBS3BCLEtBQUwsQ0FBVytDLElBQVgsSUFBbUIsUUFBdkIsRUFBaUM7YUFDMUJDLE1BQUwsQ0FBWTdDLENBQVosRUFBZTBCLEdBQWYsRUFBb0JHLEtBQXBCO09BREYsTUFFTztZQUNELEtBQUtoQyxLQUFMLENBQVdpRCxRQUFYLElBQXVCOUMsRUFBRXNCLEtBQTdCLEVBQW9DO2VBQzdCdUIsTUFBTCxDQUFZN0MsQ0FBWixFQUFlMEIsR0FBZixFQUFvQkcsS0FBcEI7U0FERixNQUVPO2VBQ0E1QixLQUFMLENBQVdELENBQVg7Ozs7Ozt5QkFLRkEsR0FBRztXQUNGNkMsTUFBTCxDQUFhN0MsQ0FBYixFQUFnQixFQUFDcUMsR0FBRXJDLEVBQUVxQyxDQUFMLEVBQVFFLEdBQUV2QyxFQUFFdUMsQ0FBWixFQUFoQjs7OzsyQkFHTXZDLEdBQUcrQyxVQUFVbEIsT0FBTztVQUNsQm5DLE9BQU8sSUFBYjtXQUNLRyxLQUFMLENBQVd3QyxDQUFYLEdBQWVVLFNBQVNYLElBQXhCO1dBQ0t2QyxLQUFMLENBQVcwQyxDQUFYLEdBQWVRLFNBQVNULEdBQXhCO1dBQ0t6QyxLQUFMLENBQVdpRCxRQUFYLEdBQXNCOUMsRUFBRXNCLEtBQXhCO1dBQ0t6QixLQUFMLENBQVcrQyxJQUFYLEdBQWtCLE1BQWxCOztVQUVJSSxRQUFRLEdBQVo7VUFDSUMsUUFBUSxDQUFaOztVQUVNQyxRQUFRLEdBQWQ7O1VBRUlDLFlBQVksQ0FBQyxDQUFELENBQWhCO1dBQ00sSUFBSUMsSUFBSSxDQUFkLEVBQWlCQSxJQUFJLEVBQXJCLEVBQXlCQSxHQUF6QixFQUE4QjtZQUN2QkYsUUFBTyxLQUFHRSxDQUFYLEdBQWlCLEdBQXJCLEVBQTBCO29CQUNkakQsSUFBVixDQUFlK0MsUUFBUyxLQUFLRSxDQUFkLEdBQW1CLEdBQWxDO1NBREYsTUFFTztvQkFDS2pELElBQVYsQ0FBZStDLFFBQVMsS0FBS0UsQ0FBN0I7Ozs7VUFJQWhELE1BQU0sS0FBS2IsR0FBZjs7OztVQUlJOEQsVUFBVSxLQUFLNUQsV0FBTCxDQUFpQjhCLFVBQS9CO1VBQ0krQixXQUFXQyxTQUFTQyxJQUFULENBQWNiLHFCQUFkLEVBQWY7VUFDSWMsV0FBV0osUUFBUVYscUJBQVIsRUFEZjtVQUVJZSxTQUFXRCxTQUFTbkIsR0FBVCxHQUFlZ0IsU0FBU2hCLEdBRnZDOztXQUlLcUIsU0FBTCxDQUFlWixRQUFmO1dBQ0tZLFNBQUwsQ0FBZUYsUUFBZjtXQUNLRSxTQUFMLENBQWVMLFFBQWY7O1VBRUlNLFlBQVlDLEtBQUtDLEdBQUwsQ0FBU2YsU0FBU0wsUUFBVCxDQUFrQkQsTUFBbEIsR0FBMkIsQ0FBcEMsRUFBd0MvQyxLQUFLSSxTQUFMLElBQWtCLENBQWxCLEdBQXNCLEVBQXRCLEdBQTJCLEdBQW5FLENBQWhCOztlQUVBLENBQVUsS0FBS0wsV0FBZixFQUNPK0IsT0FEUCxDQUNlLFNBRGYsRUFDMEIsSUFEMUIsRUFFT2hCLEtBRlAsQ0FFYSxTQUZiLEVBRXdCLE9BRnhCLEVBR09BLEtBSFAsQ0FHYSxVQUhiLEVBR3lCLFVBSHpCLEVBSU9BLEtBSlAsQ0FJYSxPQUpiLEVBSXNCLE1BQU9kLEtBQUtJLFNBQUwsQ0FBZWlFLE1BQWYsR0FBd0IsRUFBekIsR0FBZ0NoQixTQUFTTCxRQUFULENBQWtCRixLQUF4RCxJQUFrRSxJQUp4RixFQUtPaEMsS0FMUCxDQUthLE1BTGIsRUFLcUIsS0FBS3VDLFNBQVNMLFFBQVQsQ0FBa0JOLElBQXZCLEdBQStCLElBTHBELEVBTU81QixLQU5QLENBTWEsS0FOYixFQU1vQixNQUFNLENBQUNpRCxTQUFTbkIsR0FBVixHQUFnQlMsU0FBU0wsUUFBVCxDQUFrQkosR0FBbEMsR0FBd0NzQixTQUE5QyxJQUE0RCxJQU5oRjs7ZUFRQSxDQUFVLEtBQUtuRSxXQUFmLEVBQ01pQixTQUROLENBQ2dCLE9BRGhCLEVBRU1LLE1BRk47O1VBSUlpRCxXQUFXQyxTQUFBLENBQVUsS0FBS3hFLFdBQWYsRUFBNEJFLE1BQTVCLENBQW1DLE9BQW5DLEVBQ1FlLFNBRFIsQ0FDa0IsSUFEbEIsRUFFUUMsSUFGUixDQUVha0IsS0FGYixDQUFmO2VBR1NoQixLQUFULEdBQ01sQixNQUROLENBQ2EsSUFEYixFQUVVdUUsSUFGVixDQUVlLFVBQVNqRCxDQUFULEVBQVk7WUFBTSxPQUFPQSxDQUFQLElBQVksUUFBaEIsRUFBMEI7aUJBQVNBLENBQVA7U0FBNUIsTUFBNkM7aUJBQ3JELHFCQUFxQkEsRUFBRWtELEdBQXZCLEdBQTZCLHlCQUE3QixHQUF5RGxELEVBQUVtRCxLQUEzRCxHQUFtRSxPQUExRTs7T0FIZDs7VUFPSUMsT0FBTyxLQUFLNUUsV0FBTCxDQUFpQmtELHFCQUFqQixFQUFYO1dBQ0tnQixTQUFMLENBQWVVLElBQWY7O1VBRUlDLFVBQVVELEtBQUsvQixHQUFMLEdBQVdtQixTQUFTbkIsR0FBbEM7OztVQUlJaUMsZ0JBQWdCRixLQUFLNUIsTUFBekI7O1VBRUluQyxNQUFKLENBQVcsT0FBWCxFQUFvQkksU0FBcEIsQ0FBOEIsUUFBOUI7O09BRUdGLEtBRkgsQ0FFUyxnQkFGVCxFQUUyQixNQUYzQjs7VUFJSUYsTUFBSixDQUFXLE9BQVgsRUFBb0JBLE1BQXBCLENBQTJCLEdBQTNCLEVBQ0dWLElBREgsQ0FDUSxXQURSLEVBQ3FCLFVBQVNxQixDQUFULEVBQVk7ZUFDckIsZ0JBQWdCOEIsU0FBU1gsSUFBVCxHQUFpQlcsU0FBU1AsS0FBVCxHQUFlLENBQWhELElBQXNELEdBQXRELElBQTZETyxTQUFTVCxHQUFULEdBQWdCUyxTQUFTTixNQUFULEdBQWdCLENBQTdGLElBQW1HLEdBQTFHO09BRkw7VUFJSW5DLE1BQUosQ0FBVyxPQUFYLEVBQW9CQSxNQUFwQixDQUEyQixNQUEzQixFQUNHVixJQURILENBQ1EsU0FEUixFQUNtQixDQURuQixFQUVHWSxLQUZILENBRVMsWUFGVCxFQUV1QixTQUZ2QixFQUdHWixJQUhILENBR1EsV0FIUixFQUdxQixVQUFTcUIsQ0FBVCxFQUFZO2VBQ3RCLGdCQUFpQixDQUFDOEIsU0FBU1AsS0FBVixHQUFnQixDQUFqQixHQUFzQixDQUF0QyxJQUEyQyxHQUEzQyxJQUFtRCxDQUFDTyxTQUFTTixNQUFWLEdBQWlCLENBQWxCLEdBQXVCLENBQXpFLElBQThFLEdBQXJGO09BSkosRUFNRzdDLElBTkgsQ0FNUSxPQU5SLEVBTWlCLFVBQVVxQixDQUFWLEVBQWE7ZUFDbkIsQ0FBQ3ZCLEtBQUtJLFNBQUwsQ0FBZWlFLE1BQWYsR0FBd0IsQ0FBeEIsR0FBMEIsQ0FBM0IsSUFBaUNyRSxLQUFLSSxTQUFMLENBQWVpRSxNQUFmLEdBQXdCLEVBQXpELEdBQStEaEIsU0FBU1AsS0FBeEUsR0FBZ0YsRUFBdkY7T0FQSixFQVNHNUMsSUFUSCxDQVNRLFFBVFIsRUFTa0IsVUFBVXFCLENBQVYsRUFBYTtlQUNwQixJQUFJNEMsS0FBS0MsR0FBTCxDQUFTZixTQUFTTixNQUFULEdBQWtCOEIsYUFBM0IsRUFBMEMsRUFBMUMsQ0FBSixHQUFxRCxDQUE1RDtPQVZKLEVBWUdDLFVBWkgsR0FhSzVFLElBYkwsQ0FhVSxTQWJWLEVBYXFCLEdBYnJCLEVBYTBCRyxFQWIxQixDQWE2QixLQWI3QixFQWFvQyxZQUFXOztPQWIvQzs7VUFlSTBFLGVBQWUsQ0FBbkI7VUFDSW5FLE1BQUosQ0FBVyxPQUFYLEVBQW9CSSxTQUFwQixDQUE4QixhQUE5QixFQUNHRixLQURILENBQ1MsU0FEVCxFQUNvQixDQURwQixFQUVHQSxLQUZILENBRVMsWUFGVCxFQUV1QixTQUZ2QixFQUdHWixJQUhILENBR1EsV0FIUixFQUdxQixVQUFTcUIsQ0FBVCxFQUFZO2VBQ3JCLGVBQWUsQ0FBZixHQUFtQixHQUFuQixJQUEwQixLQUFLZ0MsS0FBL0IsSUFBd0MsVUFBeEMsR0FBcURELEtBQXJELEdBQTZELEdBQXBFO09BSkwsRUFNR3dCLFVBTkgsR0FPS2hFLEtBUEwsQ0FPVyxTQVBYLEVBT3NCLEdBUHRCLEVBUUtaLElBUkwsQ0FRVSxXQVJWLEVBUXVCLFVBQVNxQixDQUFULEVBQVk7WUFDeEJzQixJQUFLLENBQUNRLFNBQVNOLE1BQVYsR0FBaUIsQ0FBbEIsR0FBdUIsRUFBdkIsR0FBNEJRLEtBQXRDO1lBQ01aLElBQUtVLFNBQVNQLEtBQVQsR0FBZSxDQUFoQixHQUFzQixDQUFDLEtBQUcsQ0FBSixJQUFVaUMsY0FBVixHQUE0QnpCLEtBQWxELEdBQTJELEVBQXJFO2VBQ08sZUFBZVgsQ0FBZixHQUFtQixHQUFuQixHQUF5QkUsQ0FBekIsR0FBNkIsVUFBN0IsR0FBMENTLEtBQTFDLEdBQWtELEdBQXpEO09BWEw7Ozs7NEJBZ0JLO1VBQ0RuRCxRQUFRLEtBQUtBLEtBQWpCO1VBQ0lPLE1BQU0sS0FBS2IsR0FBZjs7VUFFSU0sTUFBTStDLElBQU4sSUFBYyxRQUFsQixFQUE0Qjs7O1lBR3RCQSxJQUFOLEdBQWEsUUFBYjs7ZUFFQSxDQUFVLEtBQUtuRCxXQUFmLEVBQ09lLEtBRFAsQ0FDYSxTQURiLEVBQ3dCLE1BRHhCOztVQUdJRixNQUFKLENBQVcsT0FBWCxFQUFvQkksU0FBcEIsQ0FBOEIsYUFBOUIsRUFDRzhELFVBREgsR0FFR2hFLEtBRkgsQ0FFUyxTQUZULEVBRW9CLENBRnBCLEVBR0daLElBSEgsQ0FHUSxXQUhSLEVBR3FCLFlBQVc7ZUFBUyxlQUFlQyxNQUFNd0MsQ0FBckIsR0FBeUIsR0FBekIsSUFBZ0N4QyxNQUFNMEMsQ0FBTixHQUFVLEVBQTFDLElBQStDLEdBQXREO09BSGxDLEVBSUd4QyxFQUpILENBSU0sS0FKTixFQUlhLFlBQVc7WUFBTU8sTUFBSixDQUFXLE9BQVgsRUFBb0JJLFNBQXBCLENBQThCLGFBQTlCLEVBQTZDRixLQUE3QyxDQUFtRCxZQUFuRCxFQUFpRSxRQUFqRTtPQUoxQjs7VUFNSVgsTUFBTThCLE9BQVYsRUFBbUI7O2NBRVRBLE9BQU4sQ0FBY0osVUFBZCxDQUF5QkssV0FBekIsQ0FBcUMvQixNQUFNOEIsT0FBM0M7Y0FDTUEsT0FBTixHQUFnQixJQUFoQjtjQUNNaEIsSUFBTixHQUFhLElBQWI7OztVQUdBTCxNQUFKLENBQVcsT0FBWCxFQUFvQkEsTUFBcEIsQ0FBMkIsTUFBM0IsRUFDR2tFLFVBREgsR0FFRzVFLElBRkgsQ0FFUSxTQUZSLEVBRW1CLENBRm5CLEVBR0dHLEVBSEgsQ0FHTSxLQUhOLEVBR2EsWUFBVztZQUFNTyxNQUFKLENBQVcsT0FBWCxFQUFvQkEsTUFBcEIsQ0FBMkIsTUFBM0IsRUFBbUNFLEtBQW5DLENBQXlDLFlBQXpDLEVBQXVELFFBQXZEO09BSDFCOzs7OzhCQU9NaUQsVUFBVTtjQUNUaUIsR0FBUixDQUFZLFlBQVlqQixTQUFTbkIsR0FBckIsR0FBeUIsUUFBekIsR0FBa0NtQixTQUFTckIsSUFBM0MsR0FBZ0QsU0FBaEQsR0FBMERxQixTQUFTakIsS0FBbkUsR0FBeUUsVUFBekUsR0FBb0ZpQixTQUFTaEIsTUFBekc7Ozs7SUFJTDs7Ozs7Ozs7Ozs7O0FDNVNBOztJQUVNa0M7OEJBRVk7OzthQUNMQyxPQUFMLEdBQWUsRUFBZjs7Ozs7eUNBR2E7bUJBQ043RixPQUFPQyxJQUFQLENBQVksS0FBSzRGLE9BQWpCLENBQVA7Ozs7a0NBR09DLFFBQVE7Z0JBQ1gsQ0FBQyxLQUFLRCxPQUFMLENBQWExRixjQUFiLENBQTRCMkYsTUFBNUIsQ0FBTCxFQUEwQzt1QkFDL0IsSUFBUDs7bUJBRUcsS0FBS0QsT0FBTCxDQUFhQyxNQUFiLENBQVA7Ozs7aUNBR00xRixRQUFRO2lCQUNUeUYsT0FBTCxDQUFhekYsT0FBT0MsVUFBcEIsSUFBa0NELE1BQWxDO21CQUNPLElBQVA7Ozs7OztBQUlSLEFBQU8sSUFBSTJGLGdCQUFnQixJQUFJSCxjQUFKLEVBQXBCOztJQ2pCTUksWUFBYjs0QkFDZ0I7OzthQUNQQyxhQUFMLEdBQXFCO3NDQUNRLEtBRFI7MENBRVksS0FGWjsrQkFHQyxTQUhEO2dDQUlFLE9BSkY7MkJBS0gsS0FMRzsyQkFNSCxJQU5HOzZCQU9EO1NBUHBCOzs7OztrQ0FXUTNFLElBYlosRUFha0I7aUJBQ1Q0RSxXQUFMLEdBQW1CNUUsS0FBSzRFLFdBQXhCO2lCQUNLQyxPQUFMLEdBQWVqQixTQUFBLENBQVU1RCxJQUFWLEVBQWdCVixNQUFoQixDQUF1QixLQUF2QixFQUE4QnVDLElBQTlCLEVBQWY7aUJBQ0t6QyxXQUFMLEdBQW1Cd0UsU0FBQSxDQUFVNUQsSUFBVixFQUFnQlYsTUFBaEIsQ0FBdUIsS0FBdkIsRUFBOEJ1QyxJQUE5QixFQUFuQjtpQkFDS2lELFVBQUw7Ozs7aUNBR090RixLQXBCWCxFQW9Ca0I7aUJBQ1RBLEtBQUwsR0FBYUEsS0FBYjs7OztnQ0FHT3VGLFFBeEJYLEVBd0JrQnpFLElBeEJsQixFQXdCMEM7Z0JBQWxCMEUsR0FBa0IsdUVBQVosS0FBS3hGLEtBQU87O2dCQUNsQyxLQUFLeUYsTUFBTCxJQUFlLEtBQUtBLE1BQUwsQ0FBWUYsUUFBWixDQUFuQixFQUF1QztvQkFDL0JHLE9BQU8sS0FBS0QsTUFBTCxDQUFZRixRQUFaLEVBQW1CLENBQW5CLENBQVg7cUJBQ0ssSUFBSUksWUFBWSxDQUFyQixFQUF3QkEsWUFBWSxLQUFLRixNQUFMLENBQVlGLFFBQVosRUFBbUJyQixNQUF2RCxFQUErRHlCLFdBQS9ELEVBQTZFO3dCQUNyRUQsUUFBTyxLQUFLRCxNQUFMLENBQVlGLFFBQVosRUFBbUJJLFNBQW5CLENBQVg7MEJBQ0tKLFFBQUwsRUFBWXpFLElBQVosRUFBa0IwRSxHQUFsQjs7Ozs7OzJCQUtORCxRQWxDTixFQWtDYUcsSUFsQ2IsRUFrQ21CO2dCQUNYLE9BQU8sS0FBS0QsTUFBWixJQUFzQixXQUExQixFQUF1QztxQkFDOUJBLE1BQUwsR0FBYyxFQUFkOzs7Z0JBR0EsT0FBTyxLQUFLQSxNQUFMLENBQVlGLFFBQVosQ0FBUCxJQUE2QixXQUFqQyxFQUE4QztxQkFDckNFLE1BQUwsQ0FBWUYsUUFBWixJQUFxQixFQUFyQjs7aUJBRUNFLE1BQUwsQ0FBWUYsUUFBWixFQUFtQmpGLElBQW5CLENBQXdCb0YsSUFBeEI7Ozs7cUNBR1c7Z0JBQ0g3RixPQUFPLElBQWI7Z0JBQ00rRixLQUFLLEtBQUtQLE9BQWhCOzs7O2dCQUlNUSxhQUFhLEtBQUtDLFNBQUwsQ0FBZSxhQUFmLENBQW5CO2dCQUNJQyxRQUFRLEtBQUtELFNBQUwsQ0FBZSxpQkFBZixDQUFaO2dCQUNNRSxhQUFhLEtBQUtGLFNBQUwsQ0FBZSxhQUFmLENBQW5COztnQkFHSUcsT0FBT0wsR0FBRzlDLHFCQUFILEVBQVg7b0JBQ1FtRCxLQUFLckQsTUFBTCxHQUFZcUQsS0FBS3RELEtBQXpCOztnQkFFTXVELEtBQU1GLGFBQWFILFVBQWIsR0FBMEJELEdBQUdsRSxVQUFILENBQWMwRCxXQUFwRDtnQkFDTWUsS0FBS0QsS0FBS0gsS0FBaEI7O29CQUdRbEIsR0FBUixDQUFZLFNBQU9xQixFQUFQLEdBQVcsUUFBWCxHQUFzQkMsRUFBdEIsR0FBMkIsS0FBM0IsR0FBbUNQLEdBQUdsRSxVQUFILENBQWMwRCxXQUFqRCxHQUErRCxlQUEvRCxHQUFpRnZGLEtBQUt1RixXQUF0RixHQUFrRyxHQUE5Rzs7Z0JBRUk3RSxNQUFNNkQsU0FBQSxDQUFVd0IsRUFBVixFQUNMN0YsSUFESyxDQUNBLE9BREEsRUFDU21HLEVBRFQsQ0FBVjs7Z0JBR0lFLE9BQU8sQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPUCxVQUFQLEVBQW1CQSxhQUFhRSxLQUFoQyxDQUFYOztnQkFFTU0sT0FBTyxLQUFNRCxLQUFLLENBQUwsQ0FBTixHQUFpQixHQUFqQixHQUF1QkEsS0FBSyxDQUFMLENBQXZCLEdBQWlDLEdBQWpDLEdBQXVDQSxLQUFLLENBQUwsQ0FBdkMsR0FBaUQsR0FBakQsR0FBdURBLEtBQUssQ0FBTCxDQUFwRTs7Z0JBRUlyRyxJQUFKLENBQVMscUJBQVQsRUFBZ0MsVUFBaEM7Z0JBQ0lBLElBQUosQ0FBUyxTQUFULEVBQW9Cc0csSUFBcEIsRUE1QlM7O2dCQThCSEMsT0FBTy9GLElBQUlULE1BQUosQ0FBVyxNQUFYLENBQWI7aUJBQ0tlLFNBQUwsQ0FBZSxRQUFmLEVBQ0tDLElBREwsQ0FDVSxDQUFDLE1BQUQsRUFBUyxXQUFULEVBQXNCLFVBQXRCLEVBQWtDLFdBQWxDLENBRFYsRUFFR0UsS0FGSCxHQUVXbEIsTUFGWCxDQUVrQixRQUZsQixFQUdLQyxJQUhMLENBR1UsSUFIVixFQUdnQixVQUFTcUIsQ0FBVCxFQUFZO3VCQUFTQSxDQUFQO2FBSDlCLEVBSUtyQixJQUpMLENBSVUsT0FKVixFQUltQixVQUFTcUIsQ0FBVCxFQUFZO3VCQUFTLFVBQVVBLENBQWpCO2FBSmpDLEVBS0tyQixJQUxMLENBS1UsU0FMVixFQUtxQixZQUxyQixFQU1LQSxJQU5MLENBTVUsTUFOVixFQU1rQixDQU5sQixFQU9LQSxJQVBMLENBT1UsTUFQVixFQU9rQixDQVBsQixFQVFLQSxJQVJMLENBUVUsYUFSVixFQVF5QixDQVJ6QixFQVNLQSxJQVRMLENBU1UsY0FUVixFQVMwQixDQVQxQixFQVVLQSxJQVZMLENBVVUsUUFWVixFQVVvQixNQVZwQixFQVdHRCxNQVhILENBV1UsTUFYVixFQVlLQyxJQVpMLENBWVUsR0FaVixFQVllLGdCQVpmOztnQkFjTXdHLFNBQVNELEtBQUt4RyxNQUFMLENBQVksUUFBWixFQUFzQkMsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBaUMsYUFBakMsRUFBZ0RBLElBQWhELENBQXFELEdBQXJELEVBQTBELENBQTFELEVBQTZEQSxJQUE3RCxDQUFrRSxHQUFsRSxFQUF1RSxDQUF2RSxFQUEwRUEsSUFBMUUsQ0FBK0UsUUFBL0UsRUFBeUYsTUFBekYsRUFBaUdBLElBQWpHLENBQXNHLE9BQXRHLEVBQStHLE1BQS9HLENBQWY7bUJBQ09ELE1BQVAsQ0FBYyxVQUFkLEVBQTBCQyxJQUExQixDQUErQixJQUEvQixFQUFxQyxhQUFyQyxFQUFvREEsSUFBcEQsQ0FBeUQsSUFBekQsRUFBK0QsQ0FBL0QsRUFBa0VBLElBQWxFLENBQXVFLElBQXZFLEVBQTZFLENBQTdFLEVBQWdGQSxJQUFoRixDQUFxRixRQUFyRixFQUErRixRQUEvRjttQkFDT0QsTUFBUCxDQUFjLGdCQUFkLEVBQWdDQyxJQUFoQyxDQUFxQyxJQUFyQyxFQUEyQyxRQUEzQyxFQUFxREEsSUFBckQsQ0FBMEQsY0FBMUQsRUFBMEUsQ0FBMUUsRUFBNkVBLElBQTdFLENBQWtGLFFBQWxGLEVBQTRGLFNBQTVGO21CQUNPRCxNQUFQLENBQWMsU0FBZCxFQUF5QkMsSUFBekIsQ0FBOEIsSUFBOUIsRUFBb0MsZUFBcEMsRUFBcURBLElBQXJELENBQTBELEtBQTFELEVBQWlFLFNBQWpFLEVBQTRFQSxJQUE1RSxDQUFpRixNQUFqRixFQUF5RixRQUF6Rjs7Ozs7Ozs7Ozs7OztnQkFhSXlHLFdBQVdqRyxJQUFJVCxNQUFKLENBQVcsR0FBWCxFQUNWQyxJQURVLENBQ0wsT0FESyxFQUNJLE9BREosQ0FBZjs7Z0JBR0lELE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQixPQUFyQixFQUE4QixXQUE5Qjs7Z0JBRUlELE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQixPQUFyQixFQUE4QixTQUE5Qjs7Z0JBR00wRyxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFVQyxDQUFWLEVBQWFDLENBQWIsRUFBZ0I7OztvQkFHbENELEVBQUVFLE1BQUYsQ0FBU0MsUUFBVCxJQUFxQkgsRUFBRUksTUFBRixDQUFTRCxRQUFsQyxFQUE0QzsyQkFDbENYLEtBQUcsQ0FBVjtpQkFESCxNQUVPLElBQUlRLEVBQUVLLElBQUYsSUFBVSxZQUFkLEVBQTRCOzJCQUN4QkwsRUFBRU0sSUFBRixHQUFTTixFQUFFTSxJQUFYLEdBQWtCLEdBQTFCO2lCQURJLE1BRUEsSUFBSU4sRUFBRUssSUFBRixJQUFVLFVBQVYsSUFBd0JMLEVBQUVLLElBQUYsSUFBVSxhQUF0QyxFQUFxRDs7Ozs7Ozs7OzsyQkFVakRMLEVBQUVNLElBQUYsR0FBU04sRUFBRU0sSUFBWCxHQUFrQixHQUExQjtpQkFWSSxNQVdBOzJCQUNJTixFQUFFTSxJQUFGLEdBQVNOLEVBQUVNLElBQVgsR0FBa0IsRUFBMUI7O2FBbkJOOztnQkF1QklDLGFBQWE3QyxrQkFBQSxHQUNaOEMsS0FEWSxDQUNOLE1BRE0sRUFDRTlDLFlBQUEsQ0FBYSxFQUFiLEVBQWlCK0MsUUFBakIsQ0FBMEJWLGdCQUExQixFQUE0Q1csUUFBNUMsQ0FBcUQsR0FBckQsQ0FERixFQUVaRixLQUZZLENBRU4sU0FGTSxFQUVJOUMsZUFBQSxDQUFpQixVQUFTaEQsQ0FBVCxFQUFXO3VCQUFTdkIsS0FBS2lHLFNBQUwsQ0FBZSxlQUFmLENBQVA7YUFBOUIsRUFBeUV1QixVQUF6RSxDQUFvRixDQUFwRixDQUZKLEVBR1pILEtBSFksQ0FHTixRQUhNLEVBR0k5QyxnQkFBQSxHQUFtQmdELFFBQW5CLENBQTRCLENBQTVCLENBSEosQ0FBakI7O2dCQUtJRixRQUFRRCxVQUFaOzs7Ozs7OztpQkFRSzFHLEdBQUwsR0FBV0EsR0FBWDtpQkFDSzJHLEtBQUwsR0FBYUEsS0FBYjs7O2dCQUdNSSxVQUFVLENBQ2QsRUFBQyxNQUFLLEdBQU4sRUFBVyxTQUFRLGVBQW5CLEVBQW9DLFFBQU9DLEdBQUdDLElBQTlDLEVBRGMsRUFFZCxFQUFDLE1BQUssR0FBTixFQUFXLFNBQVEsZUFBbkIsRUFBb0MsUUFBT0QsR0FBRyxPQUFILENBQTNDLEVBRmMsRUFHZCxFQUFDLE1BQUssR0FBTixFQUFXLFNBQVEsVUFBbkIsRUFBK0IsUUFBT0EsR0FBR0UsRUFBekMsRUFIYyxFQUlkLEVBQUMsTUFBSyxHQUFOLEVBQVcsU0FBUSxPQUFuQixFQUE0QixRQUFPRixHQUFHLGNBQUgsQ0FBbkMsRUFKYyxDQUFoQjs7aUJBT0tHLFVBQUwsR0FBa0IsSUFBSWpJLFVBQUosRUFBbEI7O2lCQUVLaUksVUFBTCxDQUFnQkMsTUFBaEIsQ0FBdUJwSCxHQUF2QixFQUE0QixJQUE1QixFQUFrQyxLQUFLWCxXQUF2Qzs7Ozs7O2lCQU1LOEgsVUFBTCxDQUFnQkUsTUFBaEI7Ozs7Ozs7OztzQ0FRVztpQkFDUEYsVUFBTCxDQUFnQnRILEtBQWhCOzs7O3FDQUdXeUgsS0FyTGhCLEVBcUx1QkMsTUFyTHZCLEVBcUwrQjs7Z0JBRXJCakksT0FBTyxJQUFYO2dCQUNJVSxNQUFNLEtBQUtBLEdBQWY7O2dCQUVJd0gsYUFBYXhILElBQUk4QixJQUFKLEdBQVdYLFVBQVgsQ0FBc0JzRyxZQUF2QztnQkFDSUMsWUFBWTFILElBQUk4QixJQUFKLEdBQVdYLFVBQVgsQ0FBc0IwRCxXQUF0Qzs7Z0JBRUlvQixXQUFXakcsSUFBSUUsTUFBSixDQUFXLFNBQVgsQ0FBZjs7cUJBRVNJLFNBQVQsQ0FBbUIsR0FBbkIsRUFBd0JLLE1BQXhCOztnQkFFSW5CLElBQUosQ0FBUyxXQUFULEVBQXNCa0ksU0FBdEI7Z0JBQ0lsSSxJQUFKLENBQVMsWUFBVCxFQUF1QmdJLFVBQXZCOztpQkFFS0YsS0FBTCxHQUFhQSxLQUFiOztrQkFFTUssT0FBTixDQUFjLFVBQVU3QixJQUFWLEVBQWdCOEIsU0FBaEIsRUFBMkI7O29CQUVuQ25ELFNBQVNDLGNBQWNtRCxTQUFkLENBQXdCL0IsS0FBS1UsSUFBN0IsQ0FBYjt1QkFDT3NCLEtBQVAsQ0FBYTlILEdBQWIsRUFBa0I4RixJQUFsQixFQUF3QkcsUUFBeEIsRUFBa0MzRyxJQUFsQzs7dUJBRU95SSxRQUFQLENBQWdCL0gsR0FBaEIsRUFBcUI0SCxTQUFyQixFQUFnQ04sS0FBaEM7YUFMRjs7cUJBUVNoSCxTQUFULENBQW1CLEdBQW5CLEVBQ0dYLEVBREgsQ0FDTSxZQUROLEVBQ29CLFVBQVVzQixPQUFWLEVBQW1CQyxLQUFuQixFQUEwQjt5QkFDeEMsQ0FBVSxJQUFWLEVBQWdCRSxPQUFoQixDQUF3QixZQUF4QixFQUFzQyxJQUF0QzthQUZOLEVBSUd6QixFQUpILENBSU0sWUFKTixFQUlvQixVQUFVQyxDQUFWLEVBQWE7eUJBQzNCLENBQVUsSUFBVixFQUFnQndCLE9BQWhCLENBQXdCLFlBQXhCLEVBQXNDLEtBQXRDO2FBTE47O3FCQVFTZCxTQUFULENBQW1CLEdBQW5CLEVBQXdCQSxTQUF4QixDQUFrQyxNQUFsQyxFQUEwQ1gsRUFBMUMsQ0FBNkMsT0FBN0MsRUFBc0QsVUFBVWtCLENBQVYsRUFBYTtxQkFDM0RtSCxXQUFMO2FBREg7Ozs7c0NBT1U7Z0JBQ0oxSSxPQUFPLElBQWI7Z0JBQ00rRixLQUFLLEtBQUtQLE9BQWhCO2dCQUNJOUUsTUFBTSxLQUFLQSxHQUFmOztnQkFFTXNGLGFBQWEsS0FBS0MsU0FBTCxDQUFlLGFBQWYsQ0FBbkI7Z0JBQ01DLFFBQVEsS0FBS0QsU0FBTCxDQUFlLGlCQUFmLENBQWQ7Z0JBQ01FLGFBQWEsS0FBS0YsU0FBTCxDQUFlLGFBQWYsQ0FBbkI7O2dCQUVNSSxLQUFNRixjQUFjLEtBQWQsR0FBc0JKLEdBQUdsRSxVQUFILENBQWMwRCxXQUFwQyxHQUFrRFMsVUFBOUQ7Z0JBQ01NLEtBQU1KLFNBQVMsQ0FBVCxHQUFhSCxHQUFHbEUsVUFBSCxDQUFjc0csWUFBM0IsR0FBd0M5QixLQUFHSCxLQUF2RCxDQVZVOzs7b0JBYUZsQixHQUFSLENBQVksOEJBQTRCcUIsRUFBNUIsR0FBZ0MsUUFBaEMsR0FBMkNDLEVBQTNDLEdBQWdELDJCQUFoRCxHQUE4RVAsR0FBR2xFLFVBQUgsQ0FBY3NHLFlBQXhHO29CQUNRbkQsR0FBUixDQUFZLCtCQUE4Qm5CLFNBQVNDLElBQVQsQ0FBY3lCLFdBQTVDLEdBQXlELFFBQXpELEdBQW9FMUIsU0FBU0MsSUFBVCxDQUFjcUUsWUFBOUY7O2dCQUdLakksSUFETCxDQUNVLE9BRFYsRUFDbUJtRyxFQURuQixFQUVLbkcsSUFGTCxDQUVVLFFBRlYsRUFFb0JvRyxFQUZwQjs7cUJBSUEsQ0FBVSxLQUFLZCxPQUFMLENBQWEzRCxVQUF2QixFQUFtQ2YsS0FBbkMsQ0FBeUMsa0JBQXpDLEVBQTZELEtBQUttRixTQUFMLENBQWUsa0JBQWYsQ0FBN0Q7Ozs7Z0JBS0lyRixNQUFKLENBQVcsU0FBWCxFQUFzQndCLElBQXRCLENBQTJCLFVBQVViLENBQVYsRUFBYTs7OztvQkFJaENvSCxPQUFPLEtBQUtyRyxPQUFMLEVBQVg7b0JBQ0lzRyxhQUFhLEtBQUsvRyxVQUFMLENBQWdCUyxPQUFoQixFQUFqQjs7b0JBRUk0RCxTQUFTLENBQWIsRUFBZ0I7d0JBQ1JLLE9BQU8sQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPUCxVQUFQLEVBQW1CQSxhQUFhRSxLQUFoQyxDQUFYO3dCQUNJTSxPQUFPLEtBQU1ELEtBQUssQ0FBTCxDQUFOLEdBQWlCLEdBQWpCLEdBQXVCQSxLQUFLLENBQUwsQ0FBdkIsR0FBaUMsR0FBakMsR0FBdUNBLEtBQUssQ0FBTCxDQUF2QyxHQUFpRCxHQUFqRCxHQUF1REEsS0FBSyxDQUFMLENBQWxFOzRCQUNRdkIsR0FBUixDQUFZLG9DQUFvQ3dCLElBQWhEO3dCQUNJdEcsSUFBSixDQUFTLFNBQVQsRUFBb0JzRyxJQUFwQjs7YUFYUjs7aUJBZUt3QixLQUFMLENBQVdLLE9BQVgsQ0FBbUIsVUFBVTdCLElBQVYsRUFBZ0I4QixTQUFoQixFQUEyQjs7b0JBRXhDbkQsU0FBU0MsY0FBY21ELFNBQWQsQ0FBd0IvQixLQUFLVSxJQUE3QixDQUFiO29CQUNJVixLQUFLVSxJQUFMLElBQWEsTUFBYixJQUF1QlYsS0FBS1UsSUFBTCxJQUFhLFNBQXBDLElBQWlEVixLQUFLVSxJQUFMLElBQWEsVUFBbEUsRUFBOEU7MkJBQ25FMkIsV0FBUCxDQUFtQjdJLElBQW5CLEVBQXlCd0csSUFBekIsRUFBK0I5RixJQUFJRSxNQUFKLENBQVcsU0FBWCxFQUFzQjhGLE1BQXRCLENBQTZCLFVBQVVuRixDQUFWLEVBQWFtQyxDQUFiLEVBQWdCOytCQUFTQSxLQUFLNEUsU0FBWjtxQkFBL0MsQ0FBL0I7O2FBSk47O2dCQVFJMUgsTUFBSixDQUFXLFNBQVgsRUFBc0J3QixJQUF0QixDQUEyQixZQUFZOzs7O29CQUkvQnVHLE9BQU8sS0FBS3JHLE9BQUwsRUFBWDtvQkFDSXNHLGFBQWEsS0FBSy9HLFVBQUwsQ0FBZ0JTLE9BQWhCLEVBQWpCOztvQkFFSTRELFNBQVMsQ0FBYixFQUFnQjt3QkFDUkssT0FBTyxDQUFDLENBQUQsRUFBSSxDQUFKLEVBQU9QLFVBQVAsRUFBb0IyQyxLQUFLNUYsTUFBekIsQ0FBWDs0QkFDUWlDLEdBQVIsQ0FBWSwrQkFBOEIyRCxLQUFLN0YsS0FBbkMsR0FBMEMsUUFBMUMsR0FBcUQ2RixLQUFLNUYsTUFBdEU7O3dCQUVJeUQsT0FBTyxLQUFNRCxLQUFLLENBQUwsQ0FBTixHQUFpQixHQUFqQixHQUF1QkEsS0FBSyxDQUFMLENBQXZCLEdBQWlDLEdBQWpDLEdBQXVDQSxLQUFLLENBQUwsQ0FBdkMsR0FBaUQsR0FBakQsR0FBdURBLEtBQUssQ0FBTCxDQUFsRTs7NEJBRVF2QixHQUFSLENBQVksa0NBQWtDd0IsSUFBOUM7d0JBQ0l0RyxJQUFKLENBQVMsU0FBVCxFQUFvQnNHLElBQXBCOzthQWRSOzs7Ozs7Ozs7Ozs7K0JBNEJHakYsQ0F6U1QsRUF5U1k7Z0JBQ0FLLFFBQVFMLEVBQUV5RixRQUFGLENBQVc4QixVQUFYLENBQXNCLENBQXRCLElBQTJCLEVBQXpDO2dCQUNJLEtBQUtkLEtBQUwsQ0FBVzNELE1BQVgsSUFBcUJ6QyxLQUF6QixFQUFnQzs7O2dCQUcxQjJFLE9BQVEsS0FBS3lCLEtBQUwsQ0FBV3BHLEtBQVgsRUFBa0JwQyxjQUFsQixDQUFpQyxxQkFBakMsSUFBMEQsS0FBS3dJLEtBQUwsQ0FBV3BHLEtBQVgsRUFBa0JtSCxtQkFBNUUsR0FBa0csS0FBS2YsS0FBTCxDQUFXcEcsS0FBWCxFQUFrQm9ILFNBQWxJO2dCQUNNQyxNQUFNLEVBQUNDLElBQUczQyxLQUFLLENBQUwsQ0FBSixFQUFZNEMsSUFBRzVDLEtBQUssQ0FBTCxDQUFmLEVBQXVCNkMsSUFBRzdDLEtBQUssQ0FBTCxJQUFVQSxLQUFLLENBQUwsQ0FBcEMsRUFBNEM4QyxJQUFHOUMsS0FBSyxDQUFMLElBQVVBLEtBQUssQ0FBTCxDQUF6RCxFQUFaOzttQkFFTzBDLEdBQVA7Ozs7Z0NBR0k7O2lCQUVDdkksR0FBTCxDQUFTRSxNQUFULENBQWdCLFdBQWhCLEVBQTZCSSxTQUE3QixDQUF1QyxRQUF2QyxFQUFpREssTUFBakQ7Ozs7a0NBR01pSSxDQXpUWixFQXlUZTtnQkFDTHJCLFNBQVUsS0FBSzlILEtBQUwsR0FBYSxLQUFLQSxLQUFMLENBQVc4SCxNQUF4QixHQUFpQyxJQUEvQztnQkFDSUEsVUFBVUEsT0FBT3pJLGNBQVAsQ0FBc0I4SixDQUF0QixDQUFkLEVBQXdDO3VCQUMvQnJCLE9BQU9xQixDQUFQLENBQVA7YUFERixNQUVPO3VCQUNFLEtBQUtoRSxhQUFMLENBQW1CZ0UsQ0FBbkIsQ0FBUDs7Ozs7eUNBSVlDLFFBbFVwQixFQWtVOEI7Z0JBQ2xCdkosT0FBTyxJQUFiO2dCQUNNVSxNQUFNLEtBQUtiLEdBQWpCOztxQkFFU3VDLElBQVQsQ0FBYyxVQUFVYixDQUFWLEVBQWE7OztvQkFFbkJBLEVBQUVpSSxRQUFOLEVBQWdCOzs7NEJBRVI3SCxVQUFVNEMsU0FBQSxDQUFVLE1BQUsxQyxVQUFmLEVBQTJCYixTQUEzQixDQUFxQyxZQUFZTyxFQUFFa0ksSUFBbkQsRUFDVHhJLElBRFMsQ0FDSk0sRUFBRWlJLFFBREUsQ0FBZDs7NEJBR0lFLFlBQVkvSCxRQUFRUixLQUFSLEdBQWdCbEIsTUFBaEIsQ0FBdUIsR0FBdkIsRUFDYkMsSUFEYSxDQUNSLE9BRFEsRUFDQyxVQUFVcUIsRUFBRWtJLElBQVosR0FBbUIsUUFEcEIsQ0FBaEI7OzRCQUdJRSxRQUFRaEssYUFBYWlLLGFBQWIsRUFBWjs7bURBQ1VDLEVBVEU7Z0NBVUpDLE9BQU9uSyxhQUFhb0ssUUFBYixDQUFzQkosTUFBTUUsRUFBTixDQUF0QixDQUFYO2lDQUNLckIsS0FBTCxDQUFXa0IsVUFBVWhELE1BQVYsQ0FBaUIsVUFBU25GLENBQVQsRUFBWTt1Q0FBU0EsRUFBRTJGLElBQUYsSUFBVXlDLE1BQU1FLEVBQU4sQ0FBakI7NkJBQS9CLEVBQThEL0gsT0FBOUQsQ0FBc0UsTUFBTTZILE1BQU1FLEVBQU4sQ0FBNUUsRUFBdUYsSUFBdkYsQ0FBWDs7OzZCQUZFLElBQUlBLEVBQVYsSUFBZ0JGLEtBQWhCLEVBQXVCO2tDQUFiRSxFQUFhOzs7a0NBTWxCM0osSUFETCxDQUNVLElBRFYsRUFDZ0IsVUFBU3FCLENBQVQsRUFBWTtnQ0FBUTBILE1BQU1qSixLQUFLZ0ssTUFBTCxDQUFZekksQ0FBWixDQUFaLENBQTRCLElBQU1vQixJQUFLcEIsRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVvQixDQUFSLEdBQWFzRyxJQUFJQyxFQUFKLEdBQVMsQ0FBQ0QsSUFBSUcsRUFBSixHQUFPSCxJQUFJQyxFQUFaLElBQWdCLENBQWpELENBQXNEM0gsRUFBRW9CLENBQUYsR0FBTUEsQ0FBTixDQUFTcEIsRUFBRTBJLEVBQUYsR0FBTzFJLEVBQUUySSxLQUFGLEdBQVV2SCxDQUFWLEdBQVksSUFBbkI7eUJBRHpILEVBRUt6QyxJQUZMLENBRVUsSUFGVixFQUVnQixVQUFTcUIsQ0FBVCxFQUFZO2dDQUFRMEgsTUFBTWpKLEtBQUtnSyxNQUFMLENBQVl6SSxDQUFaLENBQVosQ0FBNEIsSUFBTXNCLElBQUt0QixFQUFFc0IsQ0FBRixHQUFNdEIsRUFBRXNCLENBQVIsR0FBYW9HLElBQUlFLEVBQUosR0FBUyxDQUFDRixJQUFJSSxFQUFKLEdBQU9KLElBQUlFLEVBQVosSUFBZ0IsQ0FBakQsQ0FBc0Q1SCxFQUFFc0IsQ0FBRixHQUFNQSxDQUFOLENBQVN0QixFQUFFNEksRUFBRixHQUFPNUksRUFBRTJJLEtBQUYsR0FBVXJILENBQVYsR0FBWSxJQUFuQjt5QkFGekg7OzZCQUlLbUYsS0FBTCxDQUFXSyxPQUFYLENBQW1CLFVBQVU3QixJQUFWLEVBQWdCNUUsS0FBaEIsRUFBdUI7Z0NBQ3BDdUQsU0FBU0MsY0FBY21ELFNBQWQsQ0FBd0IvQixLQUFLVSxJQUE3QixDQUFiO2dDQUNJa0QsV0FBV1YsVUFBVWhELE1BQVYsQ0FBaUIsVUFBVW5GLENBQVYsRUFBYTtvQ0FBUW1DLElBQUluQyxFQUFFeUYsUUFBRixDQUFXOEIsVUFBWCxDQUFzQixDQUF0QixJQUEyQixFQUFyQyxDQUF5QyxPQUFPbEgsU0FBUzhCLENBQWhCOzZCQUF6RSxDQUFmO21DQUNPMkcsWUFBUCxDQUFvQjNKLEdBQXBCLEVBQXlCMEosUUFBekIsRUFBbUNwSyxLQUFLZ0ksS0FBeEMsRUFBK0NoSSxJQUEvQzt5QkFIRjs7Ozs7Ozs7NkJBWUtzSyxnQkFBTCxDQUF1QlosU0FBdkI7OzthQWhDUjs7OztpQ0FxQ0s7Z0JBQ0MxSixPQUFPLElBQWI7Z0JBQ01VLE1BQU0sS0FBS0EsR0FBakI7O2dCQUVNMkcsUUFBUSxLQUFLQSxLQUFuQjs7aUJBRUtrRCxhQUFMO2lCQUNLQyxZQUFMLENBQW1CLEtBQUtySyxLQUFMLENBQVc2SCxLQUE5QixFQUFxQyxFQUFyQzs7Z0JBRU15QyxXQUFXL0osSUFBSUUsTUFBSixDQUFXLGFBQVgsRUFDTkksU0FETSxDQUNJLEdBREosRUFFTkMsSUFGTSxDQUVELEtBQUtkLEtBQUwsQ0FBV3VLLEtBRlYsQ0FBakI7O2dCQUlNQyxZQUFZRixTQUFTdEosS0FBVCxHQUNmeUosTUFEZSxDQUNSLEdBRFEsRUFFZjFLLElBRmUsQ0FFVixPQUZVLEVBRUQsVUFBVXFCLENBQVYsRUFBYTt1QkFBVUEsRUFBRS9CLGNBQUYsQ0FBaUIsT0FBakIsSUFBNEIsTUFBSStCLEVBQUUyRixJQUFOLEdBQWEsSUFBYixHQUFvQjNGLEVBQUVzSixLQUFsRCxHQUEwRCxNQUFNdEosRUFBRTJGLElBQTFFO2FBRmQsQ0FBbEI7O3NCQUlVakgsTUFBVixDQUFpQixNQUFqQixFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixNQURuQjs7c0JBR1V3RyxNQUFWLENBQWlCLFVBQVVuRixDQUFWLEVBQWE7dUJBQVNBLEVBQUV1SixJQUFUO2FBQWhDLEVBQWtEN0ssTUFBbEQsQ0FBeUQsUUFBekQsRUFDV0MsSUFEWCxDQUNnQixHQURoQixFQUNxQixDQURyQixFQUVXQSxJQUZYLENBRWdCLE1BRmhCLEVBRXdCLFNBRnhCLEVBR1dBLElBSFgsQ0FHZ0IsT0FIaEIsRUFHeUIsY0FIekI7O3NCQUtVd0csTUFBVixDQUFpQixVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFdUosSUFBVDthQUFoQyxFQUFrRDdLLE1BQWxELENBQXlELFFBQXpELEVBQ1dDLElBRFgsQ0FDZ0IsR0FEaEIsRUFDcUIsQ0FEckIsRUFFV0EsSUFGWCxDQUVnQixNQUZoQixFQUV3QixTQUZ4QixFQUdXQSxJQUhYLENBR2dCLE9BSGhCLEVBR3lCLGNBSHpCOztzQkFLVUQsTUFBVixDQUFpQixNQUFqQixFQUNXQyxJQURYLENBQ2dCLGFBRGhCLEVBQytCLE1BRC9CLEVBRVdvQixJQUZYLENBRWdCLFVBQVVDLENBQVYsRUFBYTt1QkFBUyxXQUFQO2FBRi9CLEVBR1dyQixJQUhYLENBR2dCLE9BSGhCLEVBR3lCLFVBSHpCOztxQkFNU2tCLElBQVQsR0FBZ0JDLE1BQWhCOztnQkFFTXFKLFFBQVFDLFVBQVUzSixTQUFWLENBQW9CLE1BQXBCLENBQWQ7O2dCQUVJVyxVQUFVakIsSUFBSUUsTUFBSixDQUFXLFdBQVgsRUFBd0JJLFNBQXhCLENBQWtDLFFBQWxDLEVBQ1RDLElBRFMsQ0FDSixLQUFLZCxLQUFMLENBQVc0SyxLQURQLENBQWQ7O2dCQUdNeEIsV0FBVzVILFFBQVFSLEtBQVIsR0FBZ0JsQixNQUFoQixDQUF1QixHQUF2QixFQUNaQyxJQURZLENBQ1AsT0FETyxFQUNFLE1BREYsQ0FBakI7O2dCQUdJeUosUUFBUWhLLGFBQWFpSyxhQUFiLEVBQVo7O3lDQUNVQyxFQS9DTDtvQkFnREdDLE9BQU9uSyxhQUFhb0ssUUFBYixDQUFzQkosTUFBTUUsRUFBTixDQUF0QixDQUFYO3FCQUNLckIsS0FBTCxDQUFXZSxTQUFTN0MsTUFBVCxDQUFnQixVQUFTbkYsQ0FBVCxFQUFZOzJCQUFTQSxFQUFFMkYsSUFBRixJQUFVeUMsTUFBTUUsRUFBTixDQUFqQjtpQkFBOUIsRUFBNkQvSCxPQUE3RCxDQUFxRSxNQUFNNkgsTUFBTUUsRUFBTixDQUEzRSxFQUFzRixJQUF0RixDQUFYOzs7aUJBRkUsSUFBSUEsRUFBVixJQUFnQkYsS0FBaEIsRUFBdUI7dUJBQWJFLEVBQWE7OztxQkFNbEIzSixJQURMLENBQ1UsSUFEVixFQUNnQixVQUFTcUIsQ0FBVCxFQUFZO29CQUFRMEgsTUFBTWpKLEtBQUtnSyxNQUFMLENBQVl6SSxDQUFaLENBQVosQ0FBNEIsSUFBTW9CLElBQUtwQixFQUFFb0IsQ0FBRixHQUFNcEIsRUFBRW9CLENBQVIsR0FBYXNHLElBQUlDLEVBQUosR0FBUyxDQUFDRCxJQUFJRyxFQUFKLEdBQU9ILElBQUlDLEVBQVosSUFBZ0IsQ0FBakQsQ0FBc0QzSCxFQUFFb0IsQ0FBRixHQUFNQSxDQUFOLENBQVNwQixFQUFFMEksRUFBRixHQUFPMUksRUFBRTJJLEtBQUYsR0FBVXZILENBQVYsR0FBWSxJQUFuQjthQUR6SCxFQUVLekMsSUFGTCxDQUVVLElBRlYsRUFFZ0IsVUFBU3FCLENBQVQsRUFBWTtvQkFBUTBILE1BQU1qSixLQUFLZ0ssTUFBTCxDQUFZekksQ0FBWixDQUFaLENBQTRCLElBQU1zQixJQUFLdEIsRUFBRXNCLENBQUYsR0FBTXRCLEVBQUVzQixDQUFSLEdBQWFvRyxJQUFJRSxFQUFKLEdBQVMsQ0FBQ0YsSUFBSUksRUFBSixHQUFPSixJQUFJRSxFQUFaLElBQWdCLENBQWpELENBQXNENUgsRUFBRXNCLENBQUYsR0FBTUEsQ0FBTixDQUFTdEIsRUFBRTRJLEVBQUYsR0FBTzVJLEVBQUUySSxLQUFGLEdBQVVySCxDQUFWLEdBQVksSUFBbkI7YUFGekg7Ozs7Ozs7OztpQkFXS3lILGdCQUFMLENBQXNCZixRQUF0Qjs7aUJBR0t2QixLQUFMLENBQVdLLE9BQVgsQ0FBbUIsVUFBVTdCLElBQVYsRUFBZ0I1RSxLQUFoQixFQUF1QjtvQkFDcEN1RCxTQUFTQyxjQUFjbUQsU0FBZCxDQUF3Qi9CLEtBQUtVLElBQTdCLENBQWI7dUJBQ084RCxVQUFQLENBQWtCaEwsS0FBS0csS0FBTCxDQUFXNEssS0FBN0I7YUFGRjs7aUJBTUsvQyxLQUFMLENBQVdLLE9BQVgsQ0FBbUIsVUFBVTdCLElBQVYsRUFBZ0I1RSxLQUFoQixFQUF1QjtvQkFDcEN1RCxTQUFTQyxjQUFjbUQsU0FBZCxDQUF3Qi9CLEtBQUtVLElBQTdCLENBQWI7b0JBQ0lrRCxXQUFXYixTQUFTN0MsTUFBVCxDQUFnQixVQUFVbkYsQ0FBVixFQUFhO3dCQUFRbUMsSUFBSW5DLEVBQUV5RixRQUFGLENBQVc4QixVQUFYLENBQXNCLENBQXRCLElBQTJCLEVBQXJDLENBQXlDLE9BQU9sSCxTQUFTOEIsQ0FBaEI7aUJBQXhFLENBQWY7dUJBQ08yRyxZQUFQLENBQW9CM0osR0FBcEIsRUFBeUIwSixRQUF6QixFQUFtQ3BLLEtBQUtnSSxLQUF4QyxFQUErQ2hJLElBQS9DO2FBSEY7OztpQkFPS2dJLEtBQUwsQ0FBV0ssT0FBWCxDQUFtQixVQUFVN0IsSUFBVixFQUFnQjVFLEtBQWhCLEVBQXVCO29CQUNwQ3VELFNBQVNDLGNBQWNtRCxTQUFkLENBQXdCL0IsS0FBS1UsSUFBN0IsQ0FBYjt1QkFDT3VCLFFBQVAsQ0FBZ0IvSCxHQUFoQixFQUFxQmtCLEtBQXJCLEVBQTRCNUIsS0FBS2dJLEtBQWpDO2FBRkY7O29CQU9RNUcsSUFBUixHQUFlQyxNQUFmOztzQkFFVVgsSUFBSUUsTUFBSixDQUFXLFdBQVgsRUFBd0JJLFNBQXhCLENBQWtDLGlCQUFsQyxDQUFWOztnQkFFSWlLLFNBQVMsU0FBVEEsTUFBUyxHQUFXOztvQkFFZEMsU0FBUyxFQUFmO29CQUNNQyxJQUFJekssSUFBSVIsSUFBSixDQUFTLE9BQVQsQ0FBVjtvQkFDTWtMLElBQUkxSyxJQUFJUixJQUFKLENBQVMsUUFBVCxDQUFWOzs7b0JBR0lGLEtBQUtpRyxTQUFMLENBQWUsd0JBQWYsQ0FBSixFQUE4Qzs0QkFFdkMvRixJQURMLENBQ1UsSUFEVixFQUNnQixVQUFTcUIsQ0FBVCxFQUFZOzRCQUFRMEgsTUFBTWpKLEtBQUtnSyxNQUFMLENBQVl6SSxDQUFaLENBQVosQ0FBNEJBLEVBQUVvQixDQUFGLEdBQU13QixLQUFLQyxHQUFMLENBQVM2RSxJQUFJQyxFQUFKLEdBQVNnQyxNQUFsQixFQUEwQi9HLEtBQUtrSCxHQUFMLENBQVNwQyxJQUFJRyxFQUFKLEdBQVM4QixNQUFsQixFQUEwQjNKLEVBQUVvQixDQUE1QixDQUExQixDQUFOLENBQWlFLE9BQU9wQixFQUFFb0IsQ0FBVDtxQkFEM0gsRUFFS3pDLElBRkwsQ0FFVSxJQUZWLEVBRWdCLFVBQVNxQixDQUFULEVBQVk7NEJBQVEwSCxNQUFNakosS0FBS2dLLE1BQUwsQ0FBWXpJLENBQVosQ0FBWixDQUE0QkEsRUFBRXNCLENBQUYsR0FBTXNCLEtBQUtDLEdBQUwsQ0FBUzZFLElBQUlFLEVBQUosR0FBUytCLE1BQWxCLEVBQTBCL0csS0FBS2tILEdBQUwsQ0FBU3BDLElBQUlJLEVBQUosR0FBUzZCLE1BQWxCLEVBQTBCM0osRUFBRXNCLENBQTVCLENBQTFCLENBQU4sQ0FBaUUsT0FBT3RCLEVBQUVzQixDQUFUO3FCQUYzSDs7O29CQUtFN0MsS0FBS2lHLFNBQUwsQ0FBZSw0QkFBZixDQUFKLEVBQWtEOzRCQUN4QzdELElBQVIsQ0FBYXBDLEtBQUtzTCxPQUFMLENBQWEsR0FBYixDQUFiOzs7d0JBSUdwTCxJQURMLENBQ1UsV0FEVixFQUN1QixVQUFTcUIsQ0FBVCxFQUFZOzJCQUNwQixlQUFlQSxFQUFFb0IsQ0FBakIsR0FBcUIsR0FBckIsR0FBMkJwQixFQUFFc0IsQ0FBN0IsR0FBaUMsR0FBeEM7aUJBRlI7O29CQUtJMEksS0FBS2hILE9BQUEsR0FDSjVCLENBREksQ0FDRixVQUFVcEIsQ0FBVixFQUFhOzJCQUFTQSxFQUFFb0IsQ0FBVDtpQkFEYixFQUVKRSxDQUZJLENBRUYsVUFBVXRCLENBQVYsRUFBYTsyQkFBU0EsRUFBRXNCLENBQVQ7aUJBRmIsRUFHSjJJLEtBSEksQ0FHRWpILGNBSEYsQ0FBVCxDQXRCb0I7O29CQTJCaEJrSCxhQUFhbEgsT0FBQSxHQUNaNUIsQ0FEWSxDQUNWLFVBQVVwQixDQUFWLEVBQWE7MkJBQVNBLEVBQUVvQixDQUFUO2lCQURMLEVBRVpFLENBRlksQ0FFVixVQUFVdEIsQ0FBVixFQUFhOzJCQUFTQSxFQUFFc0IsQ0FBVDtpQkFGTCxFQUdaMkksS0FIWSxDQUdOakgsa0JBSE0sQ0FBakIsQ0EzQm9COztvQkFnQ2hCbUgsWUFBWW5ILE9BQUEsR0FDWDVCLENBRFcsQ0FDVCxVQUFVcEIsQ0FBVixFQUFhOzJCQUFTQSxFQUFFb0IsQ0FBVDtpQkFETixFQUVYRSxDQUZXLENBRVQsVUFBVXRCLENBQVYsRUFBYTsyQkFBU0EsRUFBRXNCLENBQVQ7aUJBRk4sRUFHWDJJLEtBSFcsQ0FHTGpILGlCQUhLLENBQWhCLENBaENvQjs7b0JBcUNoQm9ILFdBQVdwSCxPQUFBLEdBQ1Y1QixDQURVLENBQ1IsVUFBVXBCLENBQVYsRUFBYTsyQkFBU0EsRUFBRW9CLENBQVQ7aUJBRFAsRUFFVkUsQ0FGVSxDQUVSLFVBQVV0QixDQUFWLEVBQWE7MkJBQVNBLEVBQUVzQixDQUFUO2lCQUZQLEVBR1YySSxLQUhVLENBR0pqSCxnQkFISSxDQUFmLENBckNvQjs7O3NCQW9EakJyRSxJQURILENBQ1EsR0FEUixFQUNhLFVBQVVxQixDQUFWLEVBQWE7d0JBQ2xCcUssZUFBZSxFQUFDLEtBQUlySyxFQUFFd0YsTUFBRixDQUFTcEUsQ0FBZCxFQUFpQixLQUFJcEIsRUFBRXdGLE1BQUYsQ0FBU2xFLENBQTlCLEVBQW5CO3dCQUNJZ0osZUFBZSxFQUFDLEtBQUl0SyxFQUFFMEYsTUFBRixDQUFTdEUsQ0FBZCxFQUFpQixLQUFJcEIsRUFBRTBGLE1BQUYsQ0FBU3BFLENBQTlCLEVBQW5CO3dCQUNJZ0UsSUFBSSxDQUFSOzs7d0JBR0l0RixFQUFFL0IsY0FBRixDQUFpQixNQUFqQixDQUFKLEVBQThCOztnQ0FDdEJzTSxTQUFTLENBQWI7Z0NBQ0lDLFNBQVMsQ0FBYjs7OztnQ0FJSUMsS0FBS3JLLFFBQVErRSxNQUFSLENBQWUsVUFBVXVGLEdBQVYsRUFBZUMsR0FBZixFQUFvQjt1Q0FBUzNLLEVBQUV3RixNQUFGLENBQVNuRixLQUFULElBQWtCc0ssR0FBekI7NkJBQXJDLENBQVQ7Z0NBQ0lDLEtBQUt4SyxRQUFRK0UsTUFBUixDQUFlLFVBQVV1RixHQUFWLEVBQWVDLEdBQWYsRUFBb0I7dUNBQVMzSyxFQUFFMEYsTUFBRixDQUFTckYsS0FBVCxJQUFrQnNLLEdBQXpCOzZCQUFyQyxDQUFUOztnQ0FFSUUsWUFBWTdLLEVBQUV1SixJQUFGLENBQU91QixTQUFQLENBQWlCLENBQWpCLEVBQW1CLENBQW5CLENBQWhCO2dDQUNJQyxZQUFZL0ssRUFBRXVKLElBQUYsQ0FBT3VCLFNBQVAsQ0FBaUIsQ0FBakIsRUFBbUIsQ0FBbkIsQ0FBaEI7O2dDQUVJOUssRUFBRXVKLElBQUYsSUFBVSxNQUFWLEtBQXFCdkosRUFBRTJGLElBQUYsSUFBVSxVQUFWLElBQXdCM0YsRUFBRTJGLElBQUYsSUFBVSxhQUF2RCxDQUFKLEVBQTJFO29DQUNyRTBFLGFBQWFqSixDQUFiLEdBQWlCa0osYUFBYWxKLENBQTlCLElBQW1DaUosYUFBYS9JLENBQWIsR0FBaUJnSixhQUFhaEosQ0FBckUsRUFBd0U7Z0RBQ3hELEdBQVo7Z0RBQ1ksR0FBWjtpQ0FGSixNQUdPLElBQUkrSSxhQUFhakosQ0FBYixJQUFrQmtKLGFBQWFsSixDQUEvQixJQUFvQ2lKLGFBQWEvSSxDQUFiLEdBQWlCZ0osYUFBYWhKLENBQXRFLEVBQXlFO2dEQUNoRSxHQUFaO2dEQUNZLEdBQVo7aUNBRkcsTUFHQSxJQUFJK0ksYUFBYWpKLENBQWIsSUFBa0JrSixhQUFhbEosQ0FBL0IsSUFBb0NpSixhQUFhL0ksQ0FBYixJQUFrQmdKLGFBQWFoSixDQUF2RSxFQUEwRTtnREFDakUsR0FBWjtnREFDWSxHQUFaO2lDQUZHLE1BR0E7Z0RBQ1MsR0FBWjtnREFDWSxHQUFaOzs7O2dDQUlGdEIsRUFBRXVKLElBQUYsSUFBVSxNQUFWLElBQW9CdkosRUFBRTJGLElBQUYsSUFBVSxZQUFsQyxFQUFnRDtvQ0FDMUMwRSxhQUFhakosQ0FBYixHQUFpQmtKLGFBQWFsSixDQUE5QixJQUFtQ2lKLGFBQWEvSSxDQUFiLEdBQWlCZ0osYUFBYWhKLENBQXJFLEVBQXdFO2dEQUN4RCxHQUFaO2dEQUNZLEdBQVo7aUNBRkosTUFHTyxJQUFJK0ksYUFBYWpKLENBQWIsSUFBa0JrSixhQUFhbEosQ0FBL0IsSUFBb0NpSixhQUFhL0ksQ0FBYixHQUFpQmdKLGFBQWFoSixDQUF0RSxFQUF5RTtnREFDaEUsR0FBWjtnREFDWSxHQUFaO2lDQUZHLE1BR0EsSUFBSStJLGFBQWFqSixDQUFiLElBQWtCa0osYUFBYWxKLENBQS9CLElBQW9DaUosYUFBYS9JLENBQWIsSUFBa0JnSixhQUFhaEosQ0FBdkUsRUFBMEU7Z0RBQ2pFLEdBQVo7Z0RBQ1ksR0FBWjtpQ0FGRyxNQUdBO2dEQUNTLEdBQVo7Z0RBQ1ksR0FBWjs7OzsrQkFJSFQsSUFBSCxDQUFRLFlBQVk7b0NBQ1pnSyxhQUFhLEdBQWpCLEVBQXNCOzZDQUFXLENBQUMsS0FBSzlKLE9BQUwsR0FBZVEsS0FBaEIsR0FBc0IsQ0FBL0I7O29DQUNwQnNKLGFBQWEsR0FBakIsRUFBc0I7NkNBQVcsS0FBSzlKLE9BQUwsR0FBZVEsS0FBZixHQUFxQixDQUE5Qjs7b0NBQ3BCc0osYUFBYSxHQUFqQixFQUFzQjs2Q0FBVyxDQUFDLEtBQUs5SixPQUFMLEdBQWVTLE1BQWhCLEdBQXVCLENBQWhDOztvQ0FDcEJxSixhQUFhLEdBQWpCLEVBQXNCOzZDQUFXLEtBQUs5SixPQUFMLEdBQWVTLE1BQWYsR0FBc0IsQ0FBL0I7OzZCQUo1Qjs7MkNBT2UsRUFBRSxLQUFJeEIsRUFBRXdGLE1BQUYsQ0FBU3BFLENBQVQsR0FBYW1KLE1BQW5CLEVBQTJCLEtBQUl2SyxFQUFFd0YsTUFBRixDQUFTbEUsQ0FBVCxHQUFha0osTUFBNUMsRUFBZjs7cUNBRVMsQ0FBVDtxQ0FDUyxDQUFUOytCQUNHM0osSUFBSCxDQUFRLFlBQVk7b0NBQ1prSyxhQUFhLEdBQWpCLEVBQXNCOzZDQUFXLENBQUMsS0FBS2hLLE9BQUwsR0FBZVEsS0FBaEIsR0FBc0IsQ0FBL0I7O29DQUNwQndKLGFBQWEsR0FBakIsRUFBc0I7NkNBQVcsS0FBS2hLLE9BQUwsR0FBZVEsS0FBZixHQUFxQixDQUE5Qjs7b0NBQ3BCd0osYUFBYSxHQUFqQixFQUFzQjs2Q0FBVyxDQUFDLEtBQUtoSyxPQUFMLEdBQWVTLE1BQWhCLEdBQXVCLENBQWhDOztvQ0FDcEJ1SixhQUFhLEdBQWpCLEVBQXNCOzZDQUFXLEtBQUtoSyxPQUFMLEdBQWVTLE1BQWYsR0FBc0IsQ0FBL0I7OzZCQUo1Qjs7MkNBT2UsRUFBRSxLQUFJeEIsRUFBRTBGLE1BQUYsQ0FBU3RFLENBQVQsR0FBYW1KLE1BQW5CLEVBQTJCLEtBQUl2SyxFQUFFMEYsTUFBRixDQUFTcEUsQ0FBVCxHQUFha0osTUFBNUMsRUFBZjs7Ozt3QkFLRVEsV0FBVyxDQUFDWCxZQUFELEVBQWVDLFlBQWYsQ0FBakI7d0JBQ0l0SyxFQUFFMkYsSUFBRixJQUFVLFVBQVYsSUFBd0IzRixFQUFFMkYsSUFBRixJQUFVLGFBQXRDLEVBQXFEOytCQUM1Q3VFLFdBQVdjLFFBQVgsQ0FBUDtxQkFERixNQUVPLElBQUloTCxFQUFFMkYsSUFBRixJQUFVLFlBQWQsRUFBNEI7K0JBQzFCd0UsVUFBVWEsUUFBVixDQUFQO3FCQURLLE1BRUEsSUFBSWhMLEVBQUUyRixJQUFGLElBQVUsVUFBZCxFQUEwQjsrQkFDeEJ5RSxTQUFTWSxRQUFULENBQVA7cUJBREssTUFFQTsrQkFDRWhCLEdBQUdnQixRQUFILENBQVA7O2lCQWxGTjs7O3NCQXVGTW5LLElBQU4sQ0FBWSxVQUFVYixDQUFWLEVBQWE7O3dCQUVmaUwsZ0JBQWdCLEVBQXBCO3dCQUNJakwsRUFBRS9CLGNBQUYsQ0FBaUIsZUFBakIsQ0FBSixFQUF1Qzt3Q0FDckIrQixFQUFFaUwsYUFBbEI7O3dCQUVFQyxRQUFRLEtBQUtDLGdCQUFMLENBQXNCLEtBQUtDLGNBQUwsTUFBeUJILGdCQUFjLEdBQXZDLENBQXRCLENBQVo7d0JBQ0lJLFFBQVFySSxTQUFBLENBQVUsS0FBSzFDLFVBQWYsRUFBMkJqQixNQUEzQixDQUFrQyxNQUFsQyxDQUFaOzBCQUNNVSxJQUFOLENBQVcsVUFBVXVMLEVBQVYsRUFBYzsrQkFBU3RMLEVBQUVxTCxLQUFUO3FCQUEzQjswQkFDTTFNLElBQU4sQ0FBVyxXQUFYLEVBQXdCLGdCQUFnQnVNLE1BQU05SixDQUFOLEdBQVUsQ0FBMUIsSUFBNkIsR0FBN0IsSUFBa0M4SixNQUFNNUosQ0FBTixHQUFRLENBQTFDLElBQTZDLEdBQXJFO3dCQUNJaUssZUFBZXZJLFNBQUEsQ0FBVSxLQUFLMUMsVUFBZixFQUEyQmpCLE1BQTNCLENBQWtDLHFCQUFsQyxDQUFuQjt3QkFDSW1NLGNBQWMsS0FBS0wsZ0JBQUwsQ0FBc0IsQ0FBdEIsQ0FBbEI7aUNBQ2F4TSxJQUFiLENBQWtCLFdBQWxCLEVBQStCLGVBQWU2TSxZQUFZcEssQ0FBM0IsR0FBNkIsR0FBN0IsR0FBaUNvSyxZQUFZbEssQ0FBN0MsR0FBK0MsR0FBOUU7d0JBQ0ltSyxlQUFlekksU0FBQSxDQUFVLEtBQUsxQyxVQUFmLEVBQTJCakIsTUFBM0IsQ0FBa0MscUJBQWxDLENBQW5CO3dCQUNJcU0sY0FBYyxLQUFLUCxnQkFBTCxDQUFzQixLQUFLQyxjQUFMLEtBQXNCLENBQTVDLENBQWxCO2lDQUNhek0sSUFBYixDQUFrQixXQUFsQixFQUErQixlQUFlK00sWUFBWXRLLENBQTNCLEdBQTZCLEdBQTdCLEdBQWlDc0ssWUFBWXBLLENBQTdDLEdBQStDLEdBQTlFOztpQkFmTjthQTFJSjs7O2dCQWdLSXFLLFdBQVcsRUFBZjtpQkFDSyxJQUFJckcsQ0FBVCxJQUFjLEtBQUsxRyxLQUFMLENBQVc0SyxLQUF6QixFQUFnQzt5QkFBV3RLLElBQVQsQ0FBYyxLQUFLTixLQUFMLENBQVc0SyxLQUFYLENBQWlCbEUsQ0FBakIsQ0FBZDs7O2lCQUU1QixJQUFJbkQsSUFBSSxDQUFkLEVBQWtCQSxJQUFJLEtBQUt2RCxLQUFMLENBQVc0SyxLQUFYLENBQWlCMUcsTUFBdkMsRUFBK0NYLEdBQS9DLEVBQW9EO29CQUM1QyxLQUFLdkQsS0FBTCxDQUFXNEssS0FBWCxDQUFpQnJILENBQWpCLEVBQW9COEYsUUFBeEIsRUFBa0M7eUJBQ3hCLElBQUkyRCxJQUFJLENBQWQsRUFBa0JBLElBQUksS0FBS2hOLEtBQUwsQ0FBVzRLLEtBQVgsQ0FBaUJySCxDQUFqQixFQUFvQjhGLFFBQXBCLENBQTZCbkYsTUFBbkQsRUFBMkQ4SSxHQUEzRCxFQUFnRTtpQ0FDcEQxTSxJQUFULENBQWMsS0FBS04sS0FBTCxDQUFXNEssS0FBWCxDQUFpQnJILENBQWpCLEVBQW9COEYsUUFBcEIsQ0FBNkIyRCxDQUE3QixDQUFkOzs7OztrQkFRUnBDLEtBREgsQ0FDU21DLFFBRFQsRUFFRzdNLEVBRkgsQ0FFTSxNQUZOLEVBRWM0SyxNQUZkOztrQkFLRzVELEtBREgsQ0FDUyxNQURULEVBQ2lCcUQsS0FEakIsQ0FDdUIsS0FBS3ZLLEtBQUwsQ0FBV3VLLEtBRGxDOzs7O2tDQUtNO2lCQUNEckQsS0FBTCxDQUFXaEgsRUFBWCxDQUFjLE1BQWQsRUFBc0IsSUFBdEI7cUJBQ0EsQ0FBVSxLQUFLbUYsT0FBZixFQUF3Qm5FLE1BQXhCO3FCQUNBLENBQVUsS0FBS3RCLFdBQWYsRUFBNEJzQixNQUE1Qjs7Ozs7OztnQ0FJSStMLEtBbG9CVixFQWtvQmlCOztnQkFHUEMsVUFBVSxHQUFoQjtnQkFDTUMsWUFBWSxFQURsQjtnQkFFTUMsY0FBV2hKLFdBQUEsQ0FBWSxLQUFLcEUsS0FBTCxDQUFXNEssS0FBdkIsQ0FBakI7O21CQUVPLFVBQVN4SixDQUFULEVBQVk7a0JBQ2YySixNQUFGLEdBQVcsRUFBWDtvQkFDSXNDLElBQUlqTSxFQUFFMkosTUFBRixHQUFXb0MsU0FBWCxHQUF1QkQsT0FBL0I7b0JBQ0lJLE1BQU1sTSxFQUFFb0IsQ0FBRixHQUFNNkssQ0FEaEI7b0JBRUlFLE1BQU1uTSxFQUFFb0IsQ0FBRixHQUFNNkssQ0FGaEI7b0JBR0lHLE1BQU1wTSxFQUFFc0IsQ0FBRixHQUFNMkssQ0FIaEI7b0JBSUlJLE1BQU1yTSxFQUFFc0IsQ0FBRixHQUFNMkssQ0FKaEI7O29CQU1Jak0sRUFBRW9CLENBQUYsSUFBTyxJQUFYLEVBQWlCOzs7OzRCQUlSa0wsS0FBVCxDQUFlLFVBQVNDLElBQVQsRUFBZTVFLEVBQWYsRUFBbUJDLEVBQW5CLEVBQXVCQyxFQUF2QixFQUEyQkMsRUFBM0IsRUFBK0I7Ozs7Ozt3QkFNeEN5RSxLQUFLckIsS0FBTCxJQUFlcUIsS0FBS3JCLEtBQUwsS0FBZWxMLENBQWxDLEVBQXNDOzs0QkFFaEN1TSxLQUFLckIsS0FBTCxDQUFXekYsUUFBWCxJQUF1QnpGLEVBQUV5RixRQUE3QixFQUF1Qzs7Ozs7Ozs7NkJBUWxDeUYsS0FBTCxDQUFXdkIsTUFBWCxHQUFvQixFQUFwQjs0QkFDSXZJLElBQUlwQixFQUFFb0IsQ0FBRixHQUFNbUwsS0FBS3JCLEtBQUwsQ0FBVzlKLENBQXpCOzRCQUNJRSxJQUFJdEIsRUFBRXNCLENBQUYsR0FBTWlMLEtBQUtyQixLQUFMLENBQVc1SixDQUR6Qjs0QkFFSWtMLElBQUk1SixLQUFLNkosR0FBTCxDQUFTN0osS0FBSzhKLElBQUwsQ0FBVXRMLElBQUlBLENBQUosR0FBUUUsSUFBSUEsQ0FBdEIsQ0FBVCxDQUZSOzRCQUdJMkssS0FBSWpNLEVBQUUySixNQUFGLEdBQVc0QyxLQUFLckIsS0FBTCxDQUFXdkIsTUFBdEIsR0FBK0JtQyxPQUh2Qzs7OzRCQU1JVSxJQUFJUCxFQUFSLEVBQVc7Z0NBQ0wsQ0FBQ08sSUFBSVAsRUFBTCxJQUFVTyxDQUFWLEdBQWNYLEtBQWxCOzs7Z0NBR0l6SyxJQUFJb0wsQ0FBUjtnQ0FDSWxMLElBQUlrTCxDQUFSO2dDQUNJRyxNQUFNdkwsQ0FBTixLQUFZdUwsTUFBTXJMLENBQU4sQ0FBaEIsRUFBMEI7O29DQUVwQixJQUFKO29DQUNJLElBQUo7Ozs7OEJBSUFGLENBQUYsR0FBTXBCLEVBQUVvQixDQUFGLEdBQU1BLENBQVo7OEJBQ0VFLENBQUYsR0FBTXRCLEVBQUVzQixDQUFGLEdBQU1BLENBQVo7O2lDQUVLNEosS0FBTCxDQUFXOUosQ0FBWCxJQUFnQkEsQ0FBaEI7aUNBQ0s4SixLQUFMLENBQVc1SixDQUFYLElBQWdCQSxDQUFoQjs7Ozs7MkJBS0dxRyxLQUFLd0UsR0FBTCxJQUFZdEUsS0FBS3FFLEdBQWpCLElBQXdCdEUsS0FBS3lFLEdBQTdCLElBQW9DdkUsS0FBS3NFLEdBQWhEO2lCQTdDRjthQVpGOzs7O2dDQThETWxFLElBdnNCVixFQXVzQmdCUyxLQXZzQmhCLEVBdXNCdUJoRCxJQXZzQnZCLEVBdXNCNkJGLFFBdnNCN0IsRUF1c0J1QztnQkFDM0JtSCxLQUFLLEVBQUMsUUFBTzFFLElBQVIsRUFBYyxTQUFTUyxRQUFRLElBQVIsR0FBYSxLQUFwQyxFQUE0QyxRQUFPaEQsSUFBbkQsRUFBeUQsWUFBV0YsUUFBcEUsRUFBWDtpQkFDSzdHLEtBQUwsQ0FBVzRLLEtBQVgsQ0FBaUJ0SyxJQUFqQixDQUFzQjBOLEVBQXRCO2lCQUNLcEcsTUFBTDs7OzttQ0FHTzBCLElBN3NCYixFQTZzQm1COztpQkFFUnRKLEtBQUwsQ0FBVzRLLEtBQVgsR0FBbUIsS0FBSzVLLEtBQUwsQ0FBVzRLLEtBQVgsQ0FBaUJyRSxNQUFqQixDQUF3QixVQUFTbEUsSUFBVCxFQUFlO3VCQUFTQSxLQUFLLE1BQUwsS0FBZ0JpSCxJQUF4QjthQUF4QyxDQUFuQjtpQkFDS3RKLEtBQUwsQ0FBV3VLLEtBQVgsR0FBbUIsS0FBS3ZLLEtBQUwsQ0FBV3VLLEtBQVgsQ0FBaUJoRSxNQUFqQixDQUF3QixVQUFTMEgsSUFBVCxFQUFlO3VCQUFVQSxLQUFLLFFBQUwsRUFBZSxNQUFmLEtBQTBCM0UsSUFBM0IsSUFBbUMyRSxLQUFLLFFBQUwsRUFBZSxNQUFmLEtBQTBCM0UsSUFBckU7YUFBeEMsQ0FBbkI7aUJBQ0sxQixNQUFMOzs7O2lDQUdLMEIsSUFwdEJYLEVBb3RCaUI7aUJBQ04sSUFBSS9GLENBQVQsSUFBYyxLQUFLdkQsS0FBTCxDQUFXNEssS0FBekI7b0JBQW9DLEtBQUs1SyxLQUFMLENBQVc0SyxLQUFYLENBQWlCckgsQ0FBakIsRUFBb0IsTUFBcEIsTUFBZ0MrRixJQUFwQyxFQUEwQyxPQUFPLEtBQUt0SixLQUFMLENBQVc0SyxLQUFYLENBQWlCckgsQ0FBakIsQ0FBUDs7Ozs7d0NBRzdEO2dCQUNQZ0gsUUFBUSxLQUFLdkssS0FBTCxDQUFXdUssS0FBekI7aUJBQ0ssSUFBSWhILElBQUksQ0FBYixFQUFpQkEsSUFBSWdILE1BQU1yRyxNQUEzQixFQUFtQ1gsR0FBbkMsRUFBd0M7b0JBQy9CMEssT0FBTzFELE1BQU1oSCxDQUFOLENBQWI7b0JBQ0ssT0FBTzBLLEtBQUtySCxNQUFiLElBQXdCLFFBQTVCLEVBQXNDO3lCQUM3QkEsTUFBTCxHQUFjLEtBQUtzSCxRQUFMLENBQWNELEtBQUtySCxNQUFuQixDQUFkOztvQkFFQyxPQUFPcUgsS0FBS25ILE1BQWIsSUFBd0IsUUFBNUIsRUFBc0M7eUJBQzdCQSxNQUFMLEdBQWMsS0FBS29ILFFBQUwsQ0FBY0QsS0FBS25ILE1BQW5CLENBQWQ7Ozs7OztpQ0FLRjtnQkFDSCxLQUFLOUcsS0FBVCxFQUFnQjtxQkFDVDRILE1BQUw7Ozs7Ozs7OztBQzl1QkMsSUFBSXVHLFVBQVUsUUFBZCxDQUNQLEFBQU8sQUFDUCxBQUFPLEFBQ1AsQUFBTyxBQUNQLEFBQU8sQUFDUCxBQUFPLEFBQ1AsQUFBTyxBQUNQLEFBQU8sQUFDUCxBQUFPLEFBQ1AsQUFBTzs7OztJQ1REQzs7Ozs7OztrQ0FFZ0I5RSxNQUFNOztnQkFFaEJ5QyxNQUFNekMsS0FBSytFLE9BQUwsQ0FBYSxJQUFiLENBQVY7Z0JBQ0l0QyxPQUFPLENBQUMsQ0FBWixFQUFlO3VCQUNKLENBQUN6QyxLQUFLZ0YsTUFBTCxDQUFZLENBQVosRUFBZXZDLEdBQWYsQ0FBRCxFQUFzQnpDLEtBQUtnRixNQUFMLENBQVl2QyxHQUFaLENBQXRCLENBQVA7YUFESixNQUVPO3VCQUNJLENBQUN6QyxJQUFELENBQVA7Ozs7O3FDQUlZaUYsU0FBU0MsVUFBVTdMLE9BQU87Z0JBQ3RDNkwsV0FBVzdMLEtBQWYsRUFBc0I7dUJBQ1gsQ0FBQzRMLE9BQUQsQ0FBUDs7Z0JBRUV4SSxRQUFRL0IsS0FBS3lLLEtBQUwsQ0FBV0YsUUFBUXJLLE1BQVIsSUFBa0J2QixRQUFNNkwsUUFBeEIsQ0FBWCxDQUFkO29CQUNRM0osR0FBUixDQUFZLFdBQVMySixRQUFULEdBQWtCLElBQWxCLEdBQXVCN0wsS0FBdkIsR0FBK0IsVUFBL0IsR0FBNENvRCxLQUE1QyxHQUFvRCxLQUFwRCxHQUE0RHdJLFFBQVFySyxNQUFoRjttQkFDT2tLLFVBQVVNLFlBQVYsQ0FBd0JILE9BQXhCLEVBQWlDeEksS0FBakMsQ0FBUDs7OztxQ0FHZ0J3SSxTQUFTSSxpQkFBaUI7O2dCQUV0Q0MsUUFBUUwsUUFBUU0sS0FBUixDQUFjLEdBQWQsQ0FBWjtnQkFDSUMsVUFBTyxFQUFYOztnQkFFSUMsUUFBUSxFQUFaO2lCQUNLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSUosTUFBTTFLLE1BQTFCLEVBQWtDOEssR0FBbEMsRUFBdUM7b0JBQy9CQyxXQUFXSCxVQUFPRixNQUFNSSxDQUFOLENBQVAsR0FBa0IsR0FBakM7b0JBQ0lDLFNBQVMvSyxNQUFULEdBQWtCeUssZUFBbEIsSUFBcUNHLFFBQUs1SyxNQUFMLEdBQWMsQ0FBdkQsRUFDQTswQkFDVTVELElBQU4sQ0FBV3dPLE9BQVg7OzhCQUVPRixNQUFNSSxDQUFOLElBQVcsR0FBbEI7aUJBSkosTUFNSzs4QkFDTUMsUUFBUDs7O2tCQUdGM08sSUFBTixDQUFXd08sT0FBWDs7bUJBRU9DLEtBQVA7Ozs7SUFJUjs7SUN2Q01HO21CQUNRM1AsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7OEJBR0k0UCxRQUFRO2dCQUNOQyxhQUFhRCxPQUFPclAsTUFBUCxDQUFjLE1BQWQsRUFDWkMsSUFEWSxDQUNQLGFBRE8sRUFDUSxNQURSLEVBRVpBLElBRlksQ0FFUCxJQUZPLEVBRUQsQ0FGQyxFQUdaQSxJQUhZLENBR1AsSUFITyxFQUdELENBSEMsRUFJWkEsSUFKWSxDQUlQLE9BSk8sRUFJRSxVQUFTcUIsQ0FBVCxFQUFZO3VCQUFVQSxFQUFFc0osS0FBRixHQUFVdEosRUFBRXNKLEtBQVosR0FBa0IsT0FBMUI7YUFKaEIsRUFLWnZKLElBTFksQ0FLUCxVQUFVQyxDQUFWLEVBQWE7dUJBQVNBLEVBQUVxTCxLQUFUO2FBTFIsQ0FBakI7O2dCQU9JakUsT0FBTyxFQUFYO3VCQUNXdkcsSUFBWCxDQUFnQixVQUFVYixDQUFWLEVBQVltQyxDQUFaLEVBQWU7cUJBQU9BLENBQUwsSUFBVSxLQUFLcEIsT0FBTCxFQUFWO2FBQWpDOztnQkFFTWtOLFVBQVVGLE9BQU9yUCxNQUFQLENBQWMsTUFBZCxFQUNYQyxJQURXLENBQ04sYUFETSxFQUNTLE1BRFQsRUFFWEEsSUFGVyxDQUVOLElBRk0sRUFFQSxDQUZBLEVBR1hBLElBSFcsQ0FHTixJQUhNLEVBR0EsQ0FIQSxFQUlYQSxJQUpXLENBSU4sT0FKTSxFQUlHLFVBQVNxQixDQUFULEVBQVk7dUJBQVVBLEVBQUVzSixLQUFGLEdBQVV0SixFQUFFc0osS0FBWixHQUFrQixPQUExQjthQUpqQixFQUtYekksSUFMVyxDQUtOLFVBQVNiLENBQVQsRUFBWWtPLEVBQVosRUFBZ0I7eUJBQ2xCLENBQVUsSUFBVixFQUFnQnpPLFNBQWhCLENBQTBCLE9BQTFCLEVBQW1DQyxJQUFuQyxDQUF3Q3NOLFVBQVVtQixZQUFWLENBQXVCbk8sRUFBRXFMLEtBQXpCLEVBQWdDakUsS0FBSzhHLEVBQUwsRUFBUzNNLEtBQXpDLEVBQWlEdkIsRUFBRXVCLEtBQUYsR0FBVXZCLEVBQUV1QixLQUFaLEdBQWtCLE1BQW5FLENBQXhDLEVBQ0MzQixLQURELEdBQ1NsQixNQURULENBQ2dCLE9BRGhCLEVBRU9DLElBRlAsQ0FFWSxHQUZaLEVBRWlCLENBRmpCLEVBR09BLElBSFAsQ0FHWSxHQUhaLEVBR2lCLFVBQVVxQixDQUFWLEVBQWFtQyxDQUFiLEVBQWdCOzJCQUFVQSxJQUFFaUYsS0FBSzhHLEVBQUwsRUFBUzFNLE1BQW5CO2lCQUhuQyxFQUlPekIsSUFKUCxDQUlZLFVBQVUyTixPQUFWLEVBQWdCOzJCQUFTQSxPQUFQO2lCQUo5QjthQU5RLENBQWhCOzt1QkFhVzdNLElBQVgsQ0FBZ0IsVUFBVWIsQ0FBVixFQUFhO3FCQUFPTSxVQUFMLENBQWdCSyxXQUFoQixDQUE0QixJQUE1QjthQUEvQjs7bUJBRU93RSxNQUFQLENBQWMsVUFBU25GLENBQVQsRUFBWTt1QkFBU0EsRUFBRS9CLGNBQUYsQ0FBaUIsTUFBakIsQ0FBUDthQUE1QixFQUFnRVMsTUFBaEUsQ0FBdUUsTUFBdkUsRUFDS0MsSUFETCxDQUNVLE9BRFYsRUFDbUIsTUFEbkIsRUFFS0EsSUFGTCxDQUVVLGFBRlYsRUFFeUIsUUFGekIsRUFHS0EsSUFITCxDQUdVLElBSFYsRUFHZ0IsQ0FBQyxFQUhqQixFQUlLQSxJQUpMLENBSVUsSUFKVixFQUlnQixDQUpoQixFQUtLb0IsSUFMTCxDQUtVLFVBQVNDLENBQVQsRUFBWTt1QkFBU21HLEdBQUduRyxFQUFFQyxJQUFMLENBQVA7YUFMeEI7Ozs7SUFXTjs7OztJQzFDTW1POzRCQUVRalEsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7OEJBSUk0UCxRQUFRO2dCQUNOdFAsT0FBTyxJQUFYOztnQkFFSTJDLElBQUksR0FBUjtnQkFDSUUsSUFBSSxFQUFSOzttQkFFTzVDLE1BQVAsQ0FBYyxNQUFkLEVBQ0tDLElBREwsQ0FDVSxHQURWLEVBQ2UsQ0FBQ3lDLENBQUQsR0FBRyxDQURsQixFQUVLekMsSUFGTCxDQUVVLEdBRlYsRUFFZSxDQUFDMkMsQ0FBRCxHQUFHLENBRmxCLEVBR0szQyxJQUhMLENBR1UsT0FIVixFQUdtQnlDLENBSG5CLEVBSUt6QyxJQUpMLENBSVUsUUFKVixFQUlvQjJDLENBSnBCLEVBS0szQyxJQUxMLENBS1UsT0FMVixFQUttQixVQUFVcUIsQ0FBVixFQUFhO3VCQUNqQkEsRUFBRXBCLEtBQVQ7YUFOUjs7OzttQkFXT0YsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLE9BRFYsRUFDbUIsT0FEbkIsRUFFS0EsSUFGTCxDQUVVLGFBRlYsRUFFeUIsUUFGekIsRUFHS0EsSUFITCxDQUdVLElBSFYsRUFHZ0IsQ0FIaEIsRUFJS0EsSUFKTCxDQUlVLElBSlYsRUFJZ0IsRUFKaEIsRUFLS29CLElBTEwsQ0FLVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUVxTyxLQUFUO2FBTHhCOztnQkFPTUMsY0FBY1AsT0FBT3JQLE1BQVAsQ0FBYyxNQUFkLEVBQ2ZDLElBRGUsQ0FDVixPQURVLEVBQ0QsT0FEQyxFQUVmQSxJQUZlLENBRVYsYUFGVSxFQUVLLFFBRkwsQ0FBcEI7O3dCQUlZRCxNQUFaLENBQW1CLE9BQW5CLEVBQ09DLElBRFAsQ0FDWSxHQURaLEVBQ2lCLENBRGpCLEVBRU9BLElBRlAsQ0FFWSxHQUZaLEVBRWlCLENBQUMsQ0FGbEIsRUFHT29CLElBSFAsQ0FHWSxVQUFVQyxDQUFWLEVBQWE7b0JBQVEyTixRQUFRWCxVQUFVTSxZQUFWLENBQXVCdE4sRUFBRXFMLEtBQXpCLEVBQWdDLEVBQWhDLENBQWQsQ0FBbUQsT0FBT3NDLE1BQU0sQ0FBTixDQUFQO2FBSDlFOzt3QkFLWWpQLE1BQVosQ0FBbUIsT0FBbkIsRUFDT0MsSUFEUCxDQUNZLEdBRFosRUFDaUIsQ0FEakIsRUFFT0EsSUFGUCxDQUVZLEdBRlosRUFFaUIsQ0FGakIsRUFHT29CLElBSFAsQ0FHWSxVQUFVQyxDQUFWLEVBQWE7b0JBQVEyTixRQUFRWCxVQUFVTSxZQUFWLENBQXVCdE4sRUFBRXFMLEtBQXpCLEVBQWdDLEVBQWhDLENBQWQsQ0FBbUQsT0FBUXNDLE1BQU03SyxNQUFOLElBQWdCLENBQWhCLEdBQW9CLEVBQXBCLEdBQXVCNkssTUFBTSxDQUFOLENBQS9CO2FBSDlFOzttQkFNT3hJLE1BQVAsQ0FBYyxVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFdU8sTUFBRixDQUFTdFEsY0FBVCxDQUF3QixTQUF4QixDQUFQO2FBQTdCLEVBQ0tTLE1BREwsQ0FDWSxNQURaLEVBRUtDLElBRkwsQ0FFVSxHQUZWLEVBRWUsQ0FBQ3lDLENBQUQsR0FBRyxDQUFILEdBQU8sQ0FGdEIsRUFHS3pDLElBSEwsQ0FHVSxHQUhWLEVBR2UsQ0FBQzJDLENBQUQsR0FBRyxDQUFILEdBQU8sQ0FIdEIsRUFJSzNDLElBSkwsQ0FJVSxPQUpWLEVBSW1CLFNBSm5CLEVBS0tBLElBTEwsQ0FLVSxhQUxWLEVBS3lCLE9BTHpCLEVBTUtvQixJQU5MLENBTVUsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFdU8sTUFBRixDQUFTQyxPQUFoQjthQU54Qjs7bUJBUU9ySixNQUFQLENBQWMsVUFBVW5GLENBQVYsRUFBYTt1QkFBU0EsRUFBRXVPLE1BQUYsQ0FBU3RRLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBUDthQUE3QixFQUNLUyxNQURMLENBQ1ksTUFEWixFQUVLQyxJQUZMLENBRVUsR0FGVixFQUVleUMsSUFBRSxDQUFGLEdBQU0sQ0FGckIsRUFHS3pDLElBSEwsQ0FHVSxHQUhWLEVBR2UsQ0FBQzJDLENBQUQsR0FBRyxDQUFILEdBQU8sQ0FIdEIsRUFJSzNDLElBSkwsQ0FJVSxPQUpWLEVBSW1CLFVBSm5CLEVBS0tBLElBTEwsQ0FLVSxhQUxWLEVBS3lCLEtBTHpCLEVBTUtvQixJQU5MLENBTVUsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFdU8sTUFBRixDQUFTRSxRQUFoQjthQU54Qjs7bUJBUU90SixNQUFQLENBQWMsVUFBVW5GLENBQVYsRUFBYTt1QkFBU0EsRUFBRXVPLE1BQUYsQ0FBU3RRLGNBQVQsQ0FBd0IsWUFBeEIsQ0FBUDthQUE3QixFQUNLUyxNQURMLENBQ1ksTUFEWixFQUVLQyxJQUZMLENBRVUsR0FGVixFQUVlLENBQUN5QyxDQUFELEdBQUcsQ0FBSCxHQUFPLENBRnRCLEVBR0t6QyxJQUhMLENBR1UsR0FIVixFQUdlMkMsSUFBRSxDQUFGLEdBQU0sQ0FIckIsRUFJSzNDLElBSkwsQ0FJVSxPQUpWLEVBSW1CLFlBSm5CLEVBS0tBLElBTEwsQ0FLVSxhQUxWLEVBS3lCLE9BTHpCLEVBTUtvQixJQU5MLENBTVUsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFdU8sTUFBRixDQUFTRyxVQUFoQjthQU54Qjs7bUJBUU92SixNQUFQLENBQWMsVUFBVW5GLENBQVYsRUFBYTt1QkFBU0EsRUFBRXVPLE1BQUYsQ0FBU3RRLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBUDthQUE3QixFQUNLUyxNQURMLENBQ1ksTUFEWixFQUVLQyxJQUZMLENBRVUsR0FGVixFQUVleUMsSUFBRSxDQUFGLEdBQU0sQ0FGckIsRUFHS3pDLElBSEwsQ0FHVSxHQUhWLEVBR2UyQyxJQUFFLENBQUYsR0FBTSxDQUhyQixFQUlLM0MsSUFKTCxDQUlVLE9BSlYsRUFJbUIsYUFKbkIsRUFLS0EsSUFMTCxDQUtVLGFBTFYsRUFLeUIsS0FMekIsRUFNS29CLElBTkwsQ0FNVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUV1TyxNQUFGLENBQVNJLFdBQWhCO2FBTnhCOzttQkFTT3hKLE1BQVAsQ0FBYyxVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFL0IsY0FBRixDQUFpQixPQUFqQixDQUFQO2FBQTdCLEVBQ0tTLE1BREwsQ0FDWSxPQURaLEVBRUtDLElBRkwsQ0FFVSxHQUZWLEVBRWdCeUMsSUFBRSxDQUFILEdBQU0sRUFBTixHQUFTLENBRnhCLEVBR0t6QyxJQUhMLENBR1UsR0FIVixFQUdnQjJDLElBQUUsQ0FBSCxHQUFNLEVBQU4sR0FBUyxDQUh4QixFQUlLM0MsSUFKTCxDQUlVLE9BSlYsRUFJbUIsVUFBU3FCLENBQVQsRUFBWTt1QkFBUyxFQUFQO2FBSmpDLEVBS0tyQixJQUxMLENBS1UsUUFMVixFQUtvQixVQUFTcUIsQ0FBVCxFQUFZO3VCQUFTLEVBQVA7YUFMbEMsRUFNS3JCLElBTkwsQ0FNVSxZQU5WLEVBTXdCLFVBQVNxQixDQUFULEVBQVk7dUJBQVNBLEVBQUU0TyxLQUFUO2FBTnRDOzttQkFRT3pKLE1BQVAsQ0FBYyxVQUFVbkYsQ0FBVixFQUFhO3VCQUFVLE9BQU9BLEVBQUU2TyxNQUFULElBQW1CLFdBQTNCO2FBQTdCLEVBQXlFaE8sSUFBekUsQ0FBOEUsVUFBVWIsQ0FBVixFQUFhOztvQkFFakY4TyxhQUFhOUwsU0FBQSxDQUFVLElBQVYsRUFBZ0J2RCxTQUFoQixDQUEwQixhQUExQixFQUF5Q0MsSUFBekMsQ0FBOENNLEVBQUU2TyxNQUFoRCxDQUFqQjs7MkJBRVdqUCxLQUFYLEdBQ0tsQixNQURMLENBQ1ksTUFEWixFQUVXQyxJQUZYLENBRWdCLE9BRmhCLEVBRXlCLFVBQVVvUSxDQUFWLEVBQWE7MkJBQVVBLEVBQUV6RixLQUFGLEdBQVUsVUFBVXlGLEVBQUV6RixLQUF0QixHQUE0QixNQUFwQztpQkFGeEMsRUFHVzNLLElBSFgsQ0FHZ0IsYUFIaEIsRUFHK0IsS0FIL0IsRUFJV0EsSUFKWCxDQUlnQixJQUpoQixFQUlzQixVQUFVMkcsQ0FBVixFQUFZbkQsQ0FBWixFQUFlOzJCQUFVLENBQUNmLENBQUQsR0FBRyxDQUFKLEdBQVMsRUFBVCxHQUFlZSxJQUFFLEVBQXhCO2lCQUp2QyxFQUtXeEQsSUFMWCxDQUtnQixJQUxoQixFQUt1QixDQUFDMkMsQ0FBRCxHQUFHLENBQUosR0FBUyxDQUwvQixFQU1XdkIsSUFOWCxDQU1nQixVQUFVZ1AsQ0FBVixFQUFhOzJCQUFTNUksR0FBRzRJLEVBQUU5TyxJQUFMLENBQVA7aUJBTi9CLEVBT1duQixFQVBYLENBT2MsT0FQZCxFQU91QixVQUFVQyxDQUFWLEVBQWFvRCxDQUFiLEVBQWdCOzRCQUN2QixDQUFTNk0sZUFBVDs7MEJBRU0sYUFBYTdNLENBQWIsR0FBaUIsS0FBakIsR0FBeUI4TSxLQUFLQyxTQUFMLENBQWVuUSxDQUFmLENBQXpCLEdBQTZDLFFBQTdDLEdBQXdEaUIsRUFBRWtJLElBQWhFO2lCQVZoQjthQUpOOzs7O0lBcUJOOzs7O0lDOUdNNEY7bUJBQ1EzUCxVQUFaLEVBQXdCOzs7YUFDakJBLFVBQUwsR0FBa0JBLFVBQWxCOzs7Ozs4QkFHSTRQLFFBQVE7O21CQUVIclAsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLE9BRFYsRUFDbUIsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVSxVQUFVdEosRUFBRXNKLEtBQXRCLEdBQTRCLE1BQXBDO2FBRGpDLEVBRUszSyxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsSUFIVixFQUdnQixDQUhoQixFQUlLQSxJQUpMLENBSVUsSUFKVixFQUlnQixDQUpoQixFQUtLb0IsSUFMTCxDQUtVLFVBQVNDLENBQVQsRUFBWTt1QkFBU21HLEdBQUduRyxFQUFFQyxJQUFMLENBQVA7YUFMeEI7O21CQU9PdkIsTUFBUCxDQUFjLE1BQWQsRUFBc0J5RyxNQUF0QixDQUE2QixVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFcUwsS0FBVDthQUE1QyxFQUNLMU0sSUFETCxDQUNVLE9BRFYsRUFDbUIsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVXRKLEVBQUVzSixLQUFaLEdBQWtCLE9BQTFCO2FBRGpDLEVBRUszSyxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsSUFIVixFQUdnQixDQUhoQixFQUlLQSxJQUpMLENBSVUsSUFKVixFQUlnQixPQUpoQixFQUtLb0IsSUFMTCxDQUtVLFVBQVNDLENBQVQsRUFBWTt1QkFBU0EsRUFBRXFMLEtBQVQ7YUFMeEI7Ozs7SUFVTjs7OztJQ3pCTXlDO2lCQUNRM1AsVUFBWixFQUF3Qjs7O1NBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7MEJBR0k0UCxRQUFRO2FBQ0hyUCxNQUFQLENBQWMsT0FBZCxFQUNLQyxJQURMLENBQ1UsR0FEVixFQUNlLFVBQVVxQixDQUFWLEVBQWE7ZUFBUyxDQUFDQSxFQUFFdUIsS0FBSCxHQUFTLENBQWhCO09BRDlCLEVBRUs1QyxJQUZMLENBRVUsR0FGVixFQUVlLFVBQVVxQixDQUFWLEVBQWE7ZUFBUyxDQUFDQSxFQUFFd0IsTUFBSCxHQUFVLENBQWpCO09BRjlCLEVBR0s3QyxJQUhMLENBR1UsT0FIVixFQUdtQixVQUFTcUIsQ0FBVCxFQUFZO2VBQVNBLEVBQUV1QixLQUFUO09BSGpDLEVBSUs1QyxJQUpMLENBSVUsUUFKVixFQUlvQixVQUFTcUIsQ0FBVCxFQUFZO2VBQVNBLEVBQUV3QixNQUFUO09BSmxDLEVBS0s3QyxJQUxMLENBS1UsT0FMVixFQUttQixVQUFTcUIsQ0FBVCxFQUFZO2VBQVVBLEVBQUVzSixLQUFGLEdBQVV0SixFQUFFc0osS0FBWixHQUFrQixPQUExQjtPQUxqQyxFQU1LM0ssSUFOTCxDQU1VLFlBTlYsRUFNd0IsVUFBU3FCLENBQVQsRUFBWTtlQUFTQSxFQUFFNE8sS0FBVDtPQU50Qzs7OztJQVVOOzs7O0lDaEJNZDttQkFDUTNQLFVBQVosRUFBd0I7OzthQUNqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7OzhCQUdJNFAsUUFBUTs7bUJBRUhyUCxNQUFQLENBQWMsUUFBZDs7bUJBRU95RyxNQUFQLENBQWMsVUFBVW5GLENBQVYsRUFBYTt1QkFBUyxDQUFDQSxFQUFFL0IsY0FBRixDQUFpQixVQUFqQixDQUFELElBQWlDK0IsRUFBRThCLFFBQUYsSUFBYyxPQUF0RDthQUE3QixFQUNLcEQsTUFETCxDQUNZLE1BRFosRUFFS0MsSUFGTCxDQUVVLGFBRlYsRUFFeUIsTUFGekIsRUFHS0EsSUFITCxDQUdVLElBSFYsRUFHZ0IsVUFBVXFCLENBQVYsRUFBYTtvQkFBUW1QLFNBQVNuTSxTQUFBLENBQVUsS0FBSzFDLFVBQWYsRUFBMkJqQixNQUEzQixDQUFrQyxRQUFsQyxFQUE0QzRCLElBQTVDLEVBQWYsQ0FBbUUsT0FBTyxJQUFLa08sT0FBT3BPLE9BQVAsR0FBaUJRLEtBQWpCLEdBQXVCLENBQW5DO2FBSGxHLEVBSUs1QyxJQUpMLENBSVUsSUFKVixFQUlnQixDQUpoQixFQUtLQSxJQUxMLENBS1UsT0FMVixFQUttQixVQUFTcUIsQ0FBVCxFQUFZO3VCQUFVQSxFQUFFc0osS0FBRixHQUFVdEosRUFBRXNKLEtBQVosR0FBa0IsT0FBMUI7YUFMakMsRUFNS3ZKLElBTkwsQ0FNVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUVxTCxLQUFUO2FBTnhCOzttQkFRT2xHLE1BQVAsQ0FBYyxVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFOEIsUUFBRixJQUFjLFFBQXJCO2FBQTdCLEVBQ0twRCxNQURMLENBQ1ksTUFEWixFQUVLQyxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsbUJBSFYsRUFHK0IsU0FIL0IsRUFJS0EsSUFKTCxDQUlVLE9BSlYsRUFJbUIsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVXRKLEVBQUVzSixLQUFaLEdBQWtCLE9BQTFCO2FBSmpDLEVBS0t2SixJQUxMLENBS1UsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFcUwsS0FBVDthQUx4Qjs7OztJQVVOOzs7O0lDM0JNeUM7bUJBQ1EzUCxVQUFaLEVBQXdCOzs7YUFDakJBLFVBQUwsR0FBa0JBLFVBQWxCOzs7Ozs4QkFHSTRQLFFBQVE7O2dCQUVKRSxVQUFVRixPQUFPclAsTUFBUCxDQUFjLE1BQWQsRUFDWEMsSUFEVyxDQUNOLGFBRE0sRUFDUyxNQURULEVBRVhBLElBRlcsQ0FFTixJQUZNLEVBRUEsRUFGQSxFQUdYQSxJQUhXLENBR04sSUFITSxFQUdBLENBSEEsRUFJWEEsSUFKVyxDQUlOLE9BSk0sRUFJRyxVQUFTcUIsQ0FBVCxFQUFZO3VCQUFVQSxFQUFFc0osS0FBRixHQUFVdEosRUFBRXNKLEtBQVosR0FBa0IsT0FBMUI7YUFKakIsRUFLWHZKLElBTFcsQ0FLTixVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUVxTCxLQUFUO2FBTFIsQ0FBaEI7O21CQU9PM00sTUFBUCxDQUFjLFFBQWQsRUFDS0MsSUFETCxDQUNVLEdBRFYsRUFDZSxDQURmLEVBRUtZLEtBRkwsQ0FFVyxRQUZYLEVBRXFCLFVBQVNTLENBQVQsRUFBWTt1QkFBU0EsRUFBRS9CLGNBQUYsQ0FBaUIsT0FBakIsSUFBNEIrQixFQUFFb1AsS0FBOUIsR0FBb0MsTUFBM0M7YUFGbkMsRUFHSzdQLEtBSEwsQ0FHVyxjQUhYLEVBRzJCLE1BSDNCOzs7O0lBT047Ozs7SUNuQk11TzttQkFDUTNQLFVBQVosRUFBd0I7OzthQUNqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7OzhCQUdJNFAsUUFBUTtnQkFDTnRQLE9BQU8sSUFBWDs7bUJBRU84QixPQUFQLENBQWUsV0FBZixFQUE0QixJQUE1Qjs7O21CQUdPN0IsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLEdBRFYsRUFDZSxDQUFDLEdBQUQsR0FBSyxDQURwQixFQUVLQSxJQUZMLENBRVUsR0FGVixFQUVlLENBQUMsRUFBRCxHQUFJLENBRm5CLEVBR0tBLElBSEwsQ0FHVSxRQUhWLEVBR29CLEVBSHBCLEVBSUtBLElBSkwsQ0FJVSxPQUpWLEVBSW1CLEdBSm5COztnQkFNTTBRLFdBQVd0QixPQUFPclAsTUFBUCxDQUFjLE1BQWQsRUFDWkMsSUFEWSxDQUNQLE9BRE8sRUFDRSxPQURGLEVBRVpBLElBRlksQ0FFUCxhQUZPLEVBRVEsUUFGUixDQUFqQjs7cUJBSVNELE1BQVQsQ0FBZ0IsT0FBaEIsRUFDT0MsSUFEUCxDQUNZLEdBRFosRUFDaUIsQ0FEakIsRUFFT0EsSUFGUCxDQUVZLEdBRlosRUFFaUIsVUFBVXFCLENBQVYsRUFBYTtvQkFBUTJOLFFBQVFYLFVBQVVzQyxTQUFWLENBQW9CdFAsRUFBRXFMLEtBQXRCLENBQWQsQ0FBNEMsT0FBUXNDLE1BQU03SyxNQUFOLElBQWdCLENBQWhCLEdBQW9CLENBQXBCLEdBQXNCLENBQUMsQ0FBL0I7YUFGNUUsRUFHTy9DLElBSFAsQ0FHWSxVQUFVQyxDQUFWLEVBQWE7b0JBQVEyTixRQUFRWCxVQUFVc0MsU0FBVixDQUFvQnRQLEVBQUVxTCxLQUF0QixDQUFkLENBQTRDLE9BQU9zQyxNQUFNLENBQU4sQ0FBUDthQUh2RTs7cUJBS1NqUCxNQUFULENBQWdCLE9BQWhCLEVBQ09DLElBRFAsQ0FDWSxHQURaLEVBQ2lCLENBRGpCLEVBRU9BLElBRlAsQ0FFWSxHQUZaLEVBRWlCLFVBQVVxQixDQUFWLEVBQWE7b0JBQVFvSCxPQUFPLEtBQUs5RyxVQUFMLENBQWdCUyxPQUFoQixFQUFiLENBQXdDLE9BQU9xRyxLQUFLNUYsTUFBTCxHQUFjLENBQXJCO2FBRnhFLEVBR096QixJQUhQLENBR1ksVUFBVUMsQ0FBVixFQUFhO29CQUFRMk4sUUFBUVgsVUFBVXNDLFNBQVYsQ0FBb0J0UCxFQUFFcUwsS0FBdEIsQ0FBZCxDQUE0QyxPQUFRc0MsTUFBTTdLLE1BQU4sSUFBZ0IsQ0FBaEIsR0FBb0IsRUFBcEIsR0FBdUI2SyxNQUFNLENBQU4sQ0FBL0I7YUFIdkU7Ozs7SUFTTjs7OztJQ3JDTUc7bUJBQ1EzUCxVQUFaLEVBQXdCOzs7YUFDakJBLFVBQUwsR0FBa0JBLFVBQWxCOzs7Ozs4QkFHSTRQLFFBQVE7Z0JBQ0pFLFVBQVVGLE9BQU9yUCxNQUFQLENBQWMsTUFBZCxFQUNYQyxJQURXLENBQ04sYUFETSxFQUNTLE1BRFQsRUFFWEEsSUFGVyxDQUVOLElBRk0sRUFFQSxFQUZBLEVBR1hBLElBSFcsQ0FHTixJQUhNLEVBR0EsQ0FBQyxDQUhELEVBSVhBLElBSlcsQ0FJTixPQUpNLEVBSUcsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVXRKLEVBQUVzSixLQUFaLEdBQWtCLE9BQTFCO2FBSmpCLEVBS1h2SixJQUxXLENBS04sVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFcUwsS0FBVDthQUxSLENBQWhCOzttQkFPT2xHLE1BQVAsQ0FBYyxVQUFTbkYsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFL0IsY0FBRixDQUFpQixNQUFqQixDQUFQO2FBQTVCLEVBQWdFUyxNQUFoRSxDQUF1RSxNQUF2RSxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixNQURuQixFQUVLQSxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLWSxLQUhMLENBR1csTUFIWCxFQUdtQixVQUFTUyxDQUFULEVBQVk7dUJBQVNBLEVBQUVvUCxLQUFUO2FBSGpDLEVBSUt6USxJQUpMLENBSVUsSUFKVixFQUlnQixDQUpoQixFQUtLQSxJQUxMLENBS1UsSUFMVixFQUtnQixDQUFDLENBTGpCLEVBTUtvQixJQU5MLENBTVUsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTbUcsR0FBR25HLEVBQUVDLElBQUwsQ0FBUDthQU54Qjs7bUJBUU92QixNQUFQLENBQWMsUUFBZCxFQUNLQyxJQURMLENBQ1UsR0FEVixFQUNlLENBRGYsRUFFS1ksS0FGTCxDQUVXLFFBRlgsRUFFcUIsVUFBU1MsQ0FBVCxFQUFZO3VCQUFVQSxFQUFFL0IsY0FBRixDQUFpQixPQUFqQixJQUE0QitCLEVBQUVvUCxLQUE5QixHQUFvQyxNQUE1QzthQUZuQyxFQUdLN1AsS0FITCxDQUdXLGNBSFgsRUFHMkIsTUFIM0IsRUFJS1osSUFKTCxDQUlVLFdBSlYsRUFJdUIsVUFBVXFCLENBQVYsRUFBYTtvQkFBUXVCLFFBQVN2QixFQUFFL0IsY0FBRixDQUFpQixPQUFqQixJQUE0QitCLEVBQUV1QixLQUE5QixHQUFzQyxHQUFyRCxDQUEyRCxPQUFPLGdCQUFnQkEsUUFBUSxDQUF4QixJQUE2QixNQUFwQzthQUpqRzs7bUJBTU83QyxNQUFQLENBQWMsTUFBZCxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixNQURuQixFQUVLWSxLQUZMLENBRVcsTUFGWCxFQUVtQixVQUFTUyxDQUFULEVBQVk7dUJBQVVBLEVBQUUvQixjQUFGLENBQWlCLE9BQWpCLElBQTRCK0IsRUFBRW9QLEtBQTlCLEdBQW9DLE1BQTVDO2FBRmpDLEVBR0t6USxJQUhMLENBR1UsT0FIVixFQUdtQixVQUFVcUIsQ0FBVixFQUFhO3VCQUFVQSxFQUFFL0IsY0FBRixDQUFpQixPQUFqQixJQUE0QitCLEVBQUV1QixLQUE5QixHQUFzQyxHQUE5QzthQUhsQyxFQUlLNUMsSUFKTCxDQUlVLFFBSlYsRUFJb0IsQ0FKcEI7Ozs7SUFRTjs7OztJQ25DTW1QO21CQUNRM1AsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7OEJBR0k0UCxRQUFRO2dCQUNKL1AsUUFBUSxLQUFLRyxVQUFuQjs7bUJBRU9PLE1BQVAsQ0FBYyxNQUFkLEVBQ0tDLElBREwsQ0FDVSxPQURWLEVBQ21CLE1BRG5CLEVBRUtBLElBRkwsQ0FFVSxJQUZWLEVBRWdCLFVBQVVxQixDQUFWLEVBQWE7dUJBQVNoQyxTQUFTLFdBQVQsR0FBdUIsQ0FBdkIsR0FBeUIsQ0FBaEM7YUFGL0IsRUFHS1csSUFITCxDQUdVLElBSFYsRUFHZ0IsVUFBVXFCLENBQVYsRUFBYTt1QkFBU2hDLFNBQVMsV0FBVCxHQUF1QixDQUF2QixHQUF5QixDQUFoQzthQUgvQixFQUlLdUIsS0FKTCxDQUlXLE1BSlgsRUFJbUIsVUFBU1MsQ0FBVCxFQUFZO3VCQUFVQSxFQUFFL0IsY0FBRixDQUFpQixPQUFqQixJQUE0QitCLEVBQUVvUCxLQUE5QixHQUFvQyxNQUE1QzthQUpqQyxFQUtLelEsSUFMTCxDQUtVLE9BTFYsRUFLbUIsVUFBVXFCLENBQVYsRUFBYTt1QkFBVUEsRUFBRS9CLGNBQUYsQ0FBaUIsT0FBakIsSUFBNEIrQixFQUFFdUIsS0FBOUIsR0FBc0MsR0FBOUM7YUFMbEMsRUFNSzVDLElBTkwsQ0FNVSxRQU5WLEVBTW9CLEVBTnBCOzttQkFTT3dHLE1BQVAsQ0FBYyxVQUFTbkYsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFL0IsY0FBRixDQUFpQixNQUFqQixDQUFQO2FBQTVCLEVBQWdFUyxNQUFoRSxDQUF1RSxNQUF2RSxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixNQURuQixFQUVLQSxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsSUFIVixFQUdnQixFQUhoQixFQUlLQSxJQUpMLENBSVUsSUFKVixFQUlnQixFQUpoQixFQUtLb0IsSUFMTCxDQUtVLFVBQVNDLENBQVQsRUFBWTt1QkFBU21HLEdBQUduRyxFQUFFQyxJQUFMLENBQVA7YUFMeEI7O2dCQU9NZ08sVUFBVUYsT0FBT3JQLE1BQVAsQ0FBYyxNQUFkLEVBQ1hDLElBRFcsQ0FDTixhQURNLEVBQ1MsTUFEVCxFQUVYQSxJQUZXLENBRU4sSUFGTSxFQUVBLEVBRkEsRUFHWEEsSUFIVyxDQUdOLElBSE0sRUFHQSxFQUhBLEVBSVhBLElBSlcsQ0FJTixPQUpNLEVBSUcsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVXRKLEVBQUVzSixLQUFaLEdBQWtCLE9BQTFCO2FBSmpCLEVBS1h2SixJQUxXLENBS04sVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFcUwsS0FBVDthQUxSLENBQWhCOzs7O0lBVU47Ozs7SUNuQ015QztpQkFDUTNQLFVBQVosRUFBd0I7OztTQUNqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7OzBCQUdJNkosVUFBVTtlQUNMbkgsSUFBVCxDQUFjLFVBQVViLENBQVYsRUFBYTtZQUNuQixDQUFDQSxFQUFFL0IsY0FBRixDQUFpQixNQUFqQixDQUFMLEVBQStCO2dCQUNyQiw0QkFBNEJnUixLQUFLQyxTQUFMLENBQWVsUCxDQUFmLENBQWxDOztPQUZSOzs7O0lBUUo7Ozs7SUNiTThOO21CQUNRM1AsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7OEJBR0k0UCxRQUFRO2dCQUNOM00sSUFBSSxHQUFSO2dCQUNJRSxJQUFJLEVBQVI7O21CQUVPNUMsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLEdBRFYsRUFDZSxDQUFDeUMsQ0FBRCxHQUFHLENBRGxCLEVBRUt6QyxJQUZMLENBRVUsR0FGVixFQUVlLENBQUMyQyxDQUFELEdBQUcsQ0FGbEIsRUFHSzNDLElBSEwsQ0FHVSxPQUhWLEVBR21CeUMsQ0FIbkIsRUFJS3pDLElBSkwsQ0FJVSxRQUpWLEVBSW9CMkMsQ0FKcEI7O21CQU1PNUMsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLE9BRFYsRUFDbUIsVUFBU3FCLENBQVQsRUFBWTt1QkFBVUEsRUFBRXNKLEtBQUYsR0FBVXRKLEVBQUVzSixLQUFaLEdBQWtCLE9BQTFCO2FBRGpDLEVBRUszSyxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsbUJBSFYsRUFHK0IsU0FIL0IsRUFJS0EsSUFKTCxDQUlVLElBSlYsRUFJZ0IsQ0FKaEIsRUFLS0EsSUFMTCxDQUtVLElBTFYsRUFLZ0IsQ0FMaEIsRUFNS29CLElBTkwsQ0FNVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUVxTCxLQUFUO2FBTnhCOzttQkFRT2xHLE1BQVAsQ0FBYyxVQUFTbkYsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFL0IsY0FBRixDQUFpQixNQUFqQixDQUFQO2FBQTVCLEVBQWdFUyxNQUFoRSxDQUF1RSxNQUF2RSxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixNQURuQixFQUVLQSxJQUZMLENBRVUsYUFGVixFQUV5QixRQUZ6QixFQUdLQSxJQUhMLENBR1UsSUFIVixFQUdpQnlDLElBQUUsQ0FBSCxHQUFRLEVBSHhCLEVBSUt6QyxJQUpMLENBSVUsSUFKVixFQUlnQixFQUFFMkMsSUFBRSxDQUFKLElBQVMsRUFKekIsRUFLS3ZCLElBTEwsQ0FLVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNtRyxHQUFHbkcsRUFBRUMsSUFBTCxDQUFQO2FBTHhCOzs7O0lBVU47Ozs7SUMvQk02TjttQkFDUTNQLFVBQVosRUFBd0I7OzthQUNqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7OzhCQUdJNFAsUUFBUTtnQkFDTnRQLE9BQU8sSUFBWDs7bUJBRU84QixPQUFQLENBQWUsS0FBZixFQUFzQixJQUF0Qjs7O21CQUdPN0IsTUFBUCxDQUFjLE1BQWQsRUFDS0MsSUFETCxDQUNVLEdBRFYsRUFDZSxDQUFDLEdBQUQsR0FBSyxDQURwQixFQUVLQSxJQUZMLENBRVUsR0FGVixFQUVlLENBQUMsRUFBRCxHQUFJLENBRm5CLEVBR0tBLElBSEwsQ0FHVSxRQUhWLEVBR29CLEVBSHBCLEVBSUtBLElBSkwsQ0FJVSxPQUpWLEVBSW1CLEdBSm5COztnQkFNTTBRLFdBQVd0QixPQUFPclAsTUFBUCxDQUFjLE1BQWQsRUFDWkMsSUFEWSxDQUNQLE9BRE8sRUFDRSxPQURGLEVBRVpBLElBRlksQ0FFUCxhQUZPLEVBRVEsUUFGUixDQUFqQjs7cUJBSVNELE1BQVQsQ0FBZ0IsT0FBaEIsRUFDT0MsSUFEUCxDQUNZLEdBRFosRUFDaUIsQ0FEakIsRUFFT29CLElBRlAsQ0FFWSxVQUFVQyxDQUFWLEVBQWE7b0JBQVEyTixRQUFRWCxVQUFVc0MsU0FBVixDQUFvQnRQLEVBQUVxTCxLQUF0QixDQUFkLENBQTRDLE9BQU9zQyxNQUFNLENBQU4sQ0FBUDthQUZ2RTs7cUJBSVNqUCxNQUFULENBQWdCLE9BQWhCLEVBQ09DLElBRFAsQ0FDWSxHQURaLEVBQ2lCLENBRGpCLEVBRU9BLElBRlAsQ0FFWSxHQUZaLEVBRWlCLFVBQVVxQixDQUFWLEVBQWE7b0JBQVFvSCxPQUFPLEtBQUs5RyxVQUFMLENBQWdCUyxPQUFoQixFQUFiLENBQXdDLE9BQU9xRyxLQUFLNUYsTUFBWjthQUZ4RSxFQUdPekIsSUFIUCxDQUdZLFVBQVVDLENBQVYsRUFBYTtvQkFBUTJOLFFBQVFYLFVBQVVzQyxTQUFWLENBQW9CdFAsRUFBRXFMLEtBQXRCLENBQWQsQ0FBNEMsT0FBUXNDLE1BQU03SyxNQUFOLElBQWdCLENBQWhCLEdBQW9CLEVBQXBCLEdBQXVCNkssTUFBTSxDQUFOLENBQS9CO2FBSHZFOzs7O0lBU047Ozs7OztJQ3BDTTRCO29CQUNRcFIsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7cUNBR1k7OztpQ0FHTHFSLFVBQVV2SyxNQUFNN0YsTUFBTTs7O3FDQUdsQm9RLFVBQVV4SCxVQUFVdkIsT0FBT2xJLFFBQVE7Z0JBQ3hDRSxPQUFPLElBQVg7O2dCQUVJcUgsUUFBUXZILE9BQU91SCxLQUFuQjs7O3FCQUlLbkgsSUFETCxDQUNVLElBRFYsRUFDZ0IsVUFBU3FCLENBQVQsRUFBWTtvQkFBUTBILE1BQU1qSixLQUFLZ0ssTUFBTCxDQUFZekksQ0FBWixFQUFleUcsS0FBZixDQUFaLENBQW1DLE9BQU96RyxFQUFFb0IsQ0FBRixHQUFPcEIsRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVvQixDQUFSLEdBQWFzRyxJQUFJQyxFQUFKLEdBQVMsQ0FBQ0QsSUFBSUcsRUFBSixHQUFPSCxJQUFJQyxFQUFaLElBQWdCLENBQXBEO2FBRGpFLEVBRUtoSixJQUZMLENBRVUsSUFGVixFQUVnQixVQUFTcUIsQ0FBVCxFQUFZO29CQUFRMEgsTUFBTWpKLEtBQUtnSyxNQUFMLENBQVl6SSxDQUFaLEVBQWV5RyxLQUFmLENBQVosQ0FBbUMsT0FBT3pHLEVBQUVzQixDQUFGLEdBQU90QixFQUFFc0IsQ0FBRixHQUFNdEIsRUFBRXNCLENBQVIsR0FBYW9HLElBQUlFLEVBQUosR0FBUyxDQUFDRixJQUFJSSxFQUFKLEdBQU9KLElBQUlFLEVBQVosSUFBZ0IsQ0FBcEQ7YUFGakU7OztxQkFNRzlJLEVBREgsQ0FDTSxZQUROLEVBQ29CLFVBQVVDLENBQVYsRUFBYTs7O2FBRGpDLEVBS0dELEVBTEgsQ0FLTSxZQUxOLEVBS29CLFVBQVVDLENBQVYsRUFBYTs7YUFMakMsRUFRR0QsRUFSSCxDQVFNLE9BUk4sRUFRZSxVQUFVQyxDQUFWLEVBQWE7O29CQUVsQlIsT0FBT2tSLEtBQVAsSUFBZ0I3TSxLQUFLeUssS0FBTCxDQUFXdE8sRUFBRXFDLENBQUYsR0FBTXJDLEVBQUV1QyxDQUFuQixDQUFwQixFQUEyQzs7MkJBRWxDZ0YsVUFBUCxDQUFrQm9KLE1BQWxCLENBQXlCM1EsQ0FBekIsRUFBNEJpRSxTQUFBLENBQVUsSUFBVixDQUE1QjtpQkFGRixNQUdPOzJCQUNFc0QsVUFBUCxDQUFrQnRILEtBQWxCLENBQXdCRCxDQUF4Qjs7YUFkUjs7cUJBbUJhNFEsV0FBVCxDQUFxQjNQLENBQXJCLEVBQXdCO3lCQUNsQixDQUFVLElBQVYsRUFBZ0JPLE9BQWhCLENBQXdCLE9BQXhCLEVBQWlDUCxFQUFFMkksS0FBRixJQUFXLElBQTVDO29CQUNJLENBQUMzRixRQUFBLENBQVM0TSxNQUFkLEVBQXNCOUosTUFBTStKLFdBQU4sQ0FBa0IsR0FBbEIsRUFBdUJDLE9BQXZCO2tCQUNwQnBILEVBQUYsR0FBTzFJLEVBQUVvQixDQUFUO2tCQUNFd0gsRUFBRixHQUFPNUksRUFBRXNCLENBQVQ7dUJBQ09tTyxLQUFQLEdBQWU3TSxLQUFLeUssS0FBTCxDQUFXck4sRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVzQixDQUFuQixDQUFmOzs7cUJBSUd5TyxPQUFULENBQWlCL1AsQ0FBakIsRUFBb0I7a0JBQ1owSSxFQUFGLEdBQU8xRixRQUFBLENBQVM1QixDQUFoQjtrQkFDRXdILEVBQUYsR0FBTzVGLFFBQUEsQ0FBUzFCLENBQWhCOzs7cUJBR0cwTyxTQUFULENBQW1CaFEsQ0FBbkIsRUFBc0I7b0JBQ1osQ0FBQ2dELFFBQUEsQ0FBUzRNLE1BQWQsRUFBc0I5SixNQUFNK0osV0FBTixDQUFrQixDQUFsQjs7b0JBRWxCLENBQUM3UCxFQUFFMkksS0FBUCxFQUFjO3NCQUNSRCxFQUFGLEdBQU8sSUFBUDtzQkFDRUUsRUFBRixHQUFPLElBQVA7OzJCQUVRLFlBQVk7MkJBQ2J6SSxPQUFQLENBQWUsUUFBZjtpQkFESixFQUVHLEdBRkg7OztxQkFLRzhQLElBQVQsQ0FBY2pOLE9BQUEsR0FDVGxFLEVBRFMsQ0FDTixPQURNLEVBQ0c2USxXQURILEVBRVQ3USxFQUZTLENBRU4sTUFGTSxFQUVFaVIsT0FGRixFQUdUalIsRUFIUyxDQUdOLEtBSE0sRUFHQ2tSLFNBSEQsQ0FBZDs7Z0JBTUFFLFlBQVlsSSxTQUFTN0MsTUFBVCxDQUFnQixVQUFTbkYsQ0FBVCxFQUFZO3VCQUFTZ0QsU0FBQSxDQUFVLElBQVYsRUFBZ0J6QyxPQUFoQixDQUF3QixXQUF4QixDQUFQO2FBQTlCLENBQWhCO3NCQUVHekIsRUFESCxDQUNNLE9BRE4sRUFDZSxVQUFVQyxDQUFWLEVBQWE7d0JBQUUsQ0FBU2lRLGVBQVQsR0FBNEJ6USxPQUFPNEIsT0FBUCxDQUFlLFlBQWYsRUFBNkJwQixDQUE3QixFQUE5QjthQUQ1Qjs7OzsrQkFJR2lCLEdBQUd5RyxPQUFPO2dCQUNQcEcsUUFBUUwsRUFBRXlGLFFBQUYsQ0FBVzhCLFVBQVgsQ0FBc0IsQ0FBdEIsSUFBMkIsRUFBekM7Z0JBQ0lkLE1BQU0zRCxNQUFOLElBQWdCekMsS0FBcEIsRUFBMkI7OztnQkFHckIyRSxPQUFReUIsTUFBTXBHLEtBQU4sRUFBYXBDLGNBQWIsQ0FBNEIscUJBQTVCLElBQXFEd0ksTUFBTXBHLEtBQU4sRUFBYW1ILG1CQUFsRSxHQUF3RmYsTUFBTXBHLEtBQU4sRUFBYW9ILFNBQW5IO2dCQUNNQyxNQUFNLEVBQUNDLElBQUczQyxLQUFLLENBQUwsQ0FBSixFQUFZNEMsSUFBRzVDLEtBQUssQ0FBTCxDQUFmLEVBQXVCNkMsSUFBRzdDLEtBQUssQ0FBTCxJQUFVQSxLQUFLLENBQUwsQ0FBcEMsRUFBNEM4QyxJQUFHOUMsS0FBSyxDQUFMLElBQVVBLEtBQUssQ0FBTCxDQUF6RCxFQUFaOzttQkFFTzBDLEdBQVA7Ozs7OEJBR0U4SCxVQUFVdkssTUFBTTdGLE1BQU1iLFFBQVE7Z0JBQzFCRSxPQUFPLElBQWI7Z0JBQ0kwUixNQUFNL1EsS0FBS1YsTUFBTCxDQUFZLEdBQVosQ0FBVjs7Z0JBRUk2QixPQUFKLENBQVksT0FBWixFQUFxQixJQUFyQjtnQkFDSUEsT0FBSixDQUFZLE1BQU0sS0FBS3BDLFVBQXZCLEVBQW1DLElBQW5DOztnQkFFSU8sTUFBSixDQUFXLE1BQVg7O2lCQUVLNEksV0FBTCxDQUFpQi9JLE1BQWpCLEVBQXlCMEcsSUFBekIsRUFBK0JrTCxHQUEvQjs7OztvQ0FJUzVSLFFBQVEwRyxNQUFNbUwsVUFBVTtnQkFDN0JaLFdBQVdqUixPQUFPWSxHQUF0Qjs7aUJBRUtrUixrQkFBTCxDQUF5QmIsUUFBekIsRUFBbUN2SyxJQUFuQzs7Z0JBRUlELE9BQU9vTCxTQUFTL1EsTUFBVCxDQUFnQixNQUFoQixFQUNORSxLQURNLENBQ0EsUUFEQSxFQUNVLG1CQURWLEVBRU5aLElBRk0sQ0FFRCxHQUZDLEVBRUlzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FGSixFQUdON0ksSUFITSxDQUdELEdBSEMsRUFHSXNHLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixDQUhKLEVBSU43SSxJQUpNLENBSUQsT0FKQyxFQUlRc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBSlIsRUFLTjdJLElBTE0sQ0FLRCxRQUxDLEVBS1NzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FMVCxDQUFYOzs7OzJDQVFnQmdJLFVBQVV2SyxNQUFNO2dCQUM1QnFMLEtBQUtkLFNBQVM3USxJQUFULENBQWMsU0FBZCxFQUF5QjhPLEtBQXpCLENBQStCLEdBQS9CLENBQVQ7O2dCQUVJOEMsWUFBWUQsR0FBRyxDQUFILENBQWhCO2dCQUNJRSxhQUFhRixHQUFHLENBQUgsQ0FBakI7O29CQUVRN00sR0FBUixDQUFZLGlDQUErQjhNLFNBQS9CLEdBQXlDLEtBQXpDLEdBQStDQyxVQUEzRDs7Z0JBRUkzSixZQUFZMkksU0FBUzdRLElBQVQsQ0FBYyxPQUFkLENBQWhCO2dCQUNJZ0ksYUFBYTZJLFNBQVM3USxJQUFULENBQWMsUUFBZCxDQUFqQjs7O2dCQUdJOFIsZ0JBQWlCLE9BQU94TCxLQUFLd0MsU0FBTCxDQUFlLENBQWYsQ0FBUCxJQUE0QixRQUE1QixJQUF3Q3hDLEtBQUt3QyxTQUFMLENBQWUsQ0FBZixFQUFrQndGLE9BQWxCLENBQTBCLFlBQTFCLEtBQTJDLENBQUMsQ0FBekc7O2dCQUVJeUQsZ0JBQWdCLEVBQXBCOztpQkFHTSxJQUFJQyxJQUFJLENBQWQsRUFBaUJBLElBQUksQ0FBckIsRUFBd0JBLEdBQXhCLEVBQTZCO29CQUNyQixPQUFPMUwsS0FBS3dDLFNBQUwsQ0FBZWtKLENBQWYsQ0FBUCxJQUE0QixRQUFoQyxFQUEwQzs7O2tDQUcxQnpSLElBQWQsQ0FBbUIwUixLQUFLM0wsS0FBS3dDLFNBQUwsQ0FBZWtKLENBQWYsQ0FBTCxDQUFuQjtpQkFIRixNQUlPO2tDQUNTelIsSUFBZCxDQUFtQitGLEtBQUt3QyxTQUFMLENBQWVrSixDQUFmLENBQW5COzs7Z0JBR0ZGLGFBQUosRUFBbUI7OEJBQ0QsQ0FBZCxJQUFtQjlKLGFBQWEsRUFBaEMsQ0FEZTs7O2lCQUlkYSxtQkFBTCxHQUEyQmtKLGFBQTNCO29CQUNRak4sR0FBUixDQUFZLGdDQUE4QmlOLGNBQWMsQ0FBZCxDQUE5QixHQUErQyxLQUEvQyxHQUFxREEsY0FBYyxDQUFkLENBQWpFOzs7O0lBS047O0lDeEpNbkI7OztvQkFDUXBSLFVBQVosRUFBd0I7OzttSEFDaEJBLFVBRGdCOztjQUVqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7Ozs4QkFHSXFSLFVBQVV2SyxNQUFNN0YsTUFBTTtnQkFDcEJYLE9BQU8sSUFBWDtnQkFDSTBSLE1BQU0vUSxLQUFLVixNQUFMLENBQVksR0FBWixDQUFWOztnQkFFSTZCLE9BQUosQ0FBWSxPQUFaLEVBQXFCLElBQXJCO2dCQUNJQSxPQUFKLENBQVksTUFBTSxLQUFLcEMsVUFBdkIsRUFBbUMsSUFBbkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2lCQWdDS2tTLGtCQUFMLENBQXlCYixRQUF6QixFQUFtQ3ZLLElBQW5DOztnQkFFSUQsT0FBT21MLElBQUl6UixNQUFKLENBQVcsTUFBWDs7YUFFTkMsSUFGTSxDQUVELEdBRkMsRUFFSXNHLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixDQUZKLEVBR043SSxJQUhNLENBR0QsR0FIQyxFQUdJc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBSEosRUFJTjdJLElBSk0sQ0FJRCxPQUpDLEVBSVFzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FKUixFQUtON0ksSUFMTSxDQUtELFFBTEMsRUFLU3NHLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixDQUxULEVBTU43SSxJQU5NLENBTUQsT0FOQyxFQU1RLE1BTlIsQ0FBWCxDQXZDd0I7O2dCQStDcEJELE1BQUosQ0FBVyxNQUFYLEVBQ0tDLElBREwsQ0FDVSxHQURWLEVBQ2VzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FEZixFQUVLN0ksSUFGTCxDQUVVLEdBRlYsRUFFZXNHLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixDQUZmLEVBR0s3SSxJQUhMLENBR1UsT0FIVixFQUdtQixVQUFVcUIsQ0FBVixFQUFhO3VCQUFTaUYsS0FBSzRMLFdBQUwsSUFBb0IsWUFBcEIsR0FBbUMsRUFBbkMsR0FBdUMsRUFBOUM7YUFIbEMsRUFJS2xTLElBSkwsQ0FJVSxRQUpWLEVBSW9Cc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBSnBCLEVBS0s3SSxJQUxMLENBS1UsT0FMVixFQUttQixXQUxuQixFQS9Dd0I7O2dCQXNEcEJtUyxZQUFZLGVBQWU3TCxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FBZixHQUE2QyxHQUE3QyxHQUFtRHZDLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixDQUFuRCxHQUFpRiwwQkFBakYsR0FBK0csRUFBR3ZDLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixJQUE4QixDQUFqQyxDQUEvRyxHQUFzSixHQUF0SixHQUE0SixJQUE1SixHQUFtSyxHQUFuTDtnQkFDSXZDLEtBQUs0TCxXQUFMLElBQW9CLFlBQXhCLEVBQXNDOzRCQUN4QixnQkFBZ0I1TCxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsSUFBK0IsS0FBRyxDQUFsRCxJQUF3RCxHQUF4RCxJQUErRHZDLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixJQUErQnZDLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixJQUE0QixDQUExSCxJQUFnSSxHQUE1STs7O2dCQUdJdUosWUFBWVosSUFDYnpSLE1BRGEsQ0FDTixNQURNLEVBRWJDLElBRmEsQ0FFUixXQUZRLEVBRUttUyxTQUZMLEVBR2JuUyxJQUhhLENBR1IsYUFIUSxFQUdPLFFBSFA7O2FBS2JBLElBTGEsQ0FLUixJQUxRLEVBS0YsQ0FMRSxFQU1iQSxJQU5hLENBTVIsSUFOUSxFQU1GLENBTkUsQ0FBbEI7O3NCQVFVRCxNQUFWLENBQWlCLE9BQWpCLEVBQ09DLElBRFAsQ0FDWSxHQURaLEVBQ2lCLENBRGpCLEVBRU9BLElBRlAsQ0FFWSxHQUZaLEVBRWlCLENBRmpCLEVBR09vQixJQUhQLENBR1ksVUFBVUMsQ0FBVixFQUFhO29CQUFRMk4sUUFBUWxQLEtBQUt1UyxZQUFMLENBQWtCL0wsS0FBSy9FLEtBQXZCLEVBQThCLEVBQTlCLENBQWQsQ0FBaUQsT0FBT3lOLE1BQU0sQ0FBTixDQUFQO2FBSDVFOztzQkFLVWpQLE1BQVYsQ0FBaUIsT0FBakIsRUFDT0MsSUFEUCxDQUNZLEdBRFosRUFDaUIsQ0FEakIsRUFFT0EsSUFGUCxDQUVZLEdBRlosRUFFaUIsRUFGakIsRUFHT29CLElBSFAsQ0FHWSxVQUFVQyxDQUFWLEVBQWE7b0JBQVEyTixRQUFRbFAsS0FBS3VTLFlBQUwsQ0FBa0IvTCxLQUFLL0UsS0FBdkIsRUFBOEIsRUFBOUIsQ0FBZCxDQUFpRCxPQUFReU4sTUFBTTdLLE1BQU4sSUFBZ0IsQ0FBaEIsR0FBb0IsRUFBcEIsR0FBdUI2SyxNQUFNLENBQU4sQ0FBL0I7YUFINUU7O2dCQUtJMUksS0FBS2hILGNBQUwsQ0FBb0IsT0FBcEIsQ0FBSixFQUFrQzs0QkFDbEIsZ0JBQWdCZ0gsS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLElBQStCLEVBQS9DLElBQXNELEdBQXRELElBQTZEdkMsS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLElBQStCdkMsS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLElBQTRCLENBQTNELEdBQWdFLEVBQTdILElBQW1JLEdBQS9JOztvQkFFSTlJLE1BQUosQ0FBVyxPQUFYLEVBQ0tDLElBREwsQ0FDVSxPQURWLEVBQ21CLFVBQVNxQixDQUFULEVBQVk7MkJBQVMsRUFBUDtpQkFEakMsRUFFS3JCLElBRkwsQ0FFVSxRQUZWLEVBRW9CLFVBQVNxQixDQUFULEVBQVk7MkJBQVMsRUFBUDtpQkFGbEMsRUFHS3JCLElBSEwsQ0FHVSxZQUhWLEVBR3dCLFVBQVNxQixDQUFULEVBQVk7MkJBQVNpRixLQUFLMkosS0FBWjtpQkFIdEMsRUFJS2pRLElBSkwsQ0FJVSxXQUpWLEVBSXVCbVMsU0FKdkI7Ozs7Ozs7Ozs7Ozs7cUNBZ0JLM0QsU0FBbUM7Z0JBQTFCOEQsc0JBQTBCLHVFQUFILENBQUc7OztnQkFFMUN6RCxRQUFRTCxRQUFRTSxLQUFSLENBQWMsR0FBZCxDQUFaO2dCQUNJQyxVQUFPLEVBQVg7O2dCQUVJQyxRQUFRLEVBQVo7aUJBQ0ssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJSixNQUFNMUssTUFBMUIsRUFBa0M4SyxHQUFsQyxFQUF1QztvQkFDL0JDLFdBQVdILFVBQU9GLE1BQU1JLENBQU4sQ0FBUCxHQUFrQixHQUFqQztvQkFDSUMsU0FBUy9LLE1BQVQsR0FBa0JtTyxzQkFBbEIsSUFBNEN2RCxRQUFLNUssTUFBTCxHQUFjLENBQTlELEVBQ0E7MEJBQ1U1RCxJQUFOLENBQVd3TyxPQUFYOzs4QkFFT0YsTUFBTUksQ0FBTixJQUFXLEdBQWxCO2lCQUpKLE1BTUs7OEJBQ01DLFFBQVA7OztrQkFHRjNPLElBQU4sQ0FBV3dPLE9BQVg7O21CQUVPQyxLQUFQOzs7O0VBMUhpQnVELFVBOEhyQjs7OztJQzNITTNCO29CQUNRcFIsVUFBWixFQUF3Qjs7O2FBQ2pCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7OEJBR0lxUixVQUFVdkssTUFBTTdGLE1BQU1iLFFBQVE7Z0JBQzVCRSxPQUFPLElBQVg7bUJBQ09LLEVBQVAsQ0FBVSxVQUFWLEVBQXNCLFVBQVVxRixRQUFWLEVBQWlCcEYsQ0FBakIsRUFBb0I7b0JBQ2xDb1MsYUFBYSxFQUFqQjtvQkFDSUMsWUFBWXJTLEVBQUVtSixJQUFsQjt1QkFDT2tKLGFBQWEsSUFBcEIsRUFBMkI7d0JBQ25CQyxVQUFVN0IsU0FBUy9QLFNBQVQsQ0FBbUIsT0FBbkIsRUFDWDBGLE1BRFcsQ0FDSixVQUFTbkYsQ0FBVCxFQUFZOytCQUNUQSxFQUFFa0ksSUFBRixJQUFVa0osU0FBakI7cUJBRlEsQ0FBZDt3QkFJSUMsUUFBUTlRLE9BQVIsQ0FBZ0IsY0FBaEIsS0FBbUMsS0FBbkMsSUFBNEN4QixFQUFFbUosSUFBRixJQUFVbUosUUFBUUMsS0FBUixHQUFnQnBKLElBQTFFLEVBQWdGO21DQUNqRWhKLElBQVgsQ0FBZ0JtUyxPQUFoQjs7Z0NBRVFBLFFBQVFDLEtBQVIsR0FBZ0IxTixNQUFoQixDQUF1QjJOLE1BQW5DOztvQkFFQUMsS0FBS0wsV0FBV00sT0FBWCxFQUFUO3FCQUNLLElBQUk5RyxHQUFULElBQWdCNkcsRUFBaEIsRUFBb0I7d0JBQ1pFLGdCQUFnQkYsR0FBRzdHLEdBQUgsQ0FBcEI7eUJBQ0tnSCxTQUFMLENBQWVwVCxNQUFmLEVBQXVCbVQsY0FBY0osS0FBZCxFQUF2QixFQUE4Q0ksY0FBY3pRLElBQWQsRUFBOUM7O2FBaEJSOzs7O21DQXFCUXVJLE9BQU87Ozs7Ozs7OztnQkFTWG9JLFdBQVcsRUFBZjtnQkFDSUMsZUFBZSxFQUFuQjs7aUJBRUtDLE9BQUwsQ0FBYSxJQUFiLEVBQW1CdEksS0FBbkIsRUFBMEIsRUFBMUIsRUFBOEIsQ0FBOUIsRUFBaUNvSSxRQUFqQyxFQUEyQ0MsWUFBM0M7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2dDQW1FS04sUUFBUS9ILE9BQU91SSxTQUFTQyxPQUFPSixVQUFVQyxjQUFjO2dCQUN4REksTUFBTSxDQUFWO2lCQUNNLElBQUlDLE1BQU0sQ0FBVixFQUFZOVEsSUFBSSxDQUF0QixFQUEwQkEsSUFBSW9JLE1BQU0xRyxNQUFwQyxFQUE0QzFCLEdBQTVDLEVBQWlEO29CQUN2Q3dMLEtBQUtwRCxNQUFNcEksQ0FBTixDQUFYO29CQUNJd0wsR0FBR2pILElBQUgsSUFBVyxLQUFmLEVBQXNCOzs7O2lCQUlwQixJQUFJdU0sT0FBTSxDQUFWLEVBQVk5USxLQUFJLENBQXRCLEVBQTBCQSxLQUFJb0ksTUFBTTFHLE1BQXBDLEVBQTRDMUIsSUFBNUMsRUFBaUQ7b0JBQ3ZDd0wsTUFBS3BELE1BQU1wSSxFQUFOLENBQVg7b0JBQ0l3TCxJQUFHakgsSUFBSCxJQUFXLEtBQWYsRUFBc0I7d0JBQ2YvQixNQUFILEdBQVksRUFBRSxPQUFNc08sSUFBUixFQUFZLFNBQVFELEdBQXBCLEVBQXdCLE9BQU9GLE9BQS9CLEVBQXdDLFVBQVNDLEtBQWpELEVBQXdELFNBQVFBLEtBQWhFLEVBQXVFLFVBQVdULFVBQVUsSUFBVixHQUFpQixJQUFqQixHQUFzQkEsT0FBT3JKLElBQS9HLEVBQVo7Ozt3QkFHSTBFLElBQUczTyxjQUFILENBQWtCLE1BQWxCLEtBQTZCLEtBQWpDLEVBQXdDOzhCQUNoQyxxQ0FBcUNnUixLQUFLQyxTQUFMLENBQWV0QyxHQUFmLENBQTNDO2lDQUNTMU4sSUFBVCxDQUFjME4sR0FBZDtxQkFGRixNQUdPLElBQUlBLElBQUcxRSxJQUFILElBQVcySixZQUFmLEVBQTZCOzhCQUM1Qiw0Q0FBNEM1QyxLQUFLQyxTQUFMLENBQWV0QyxHQUFmLENBQWxEO2lDQUNTMU4sSUFBVCxDQUFjME4sR0FBZDtxQkFGSyxNQUdBO3FDQUNRQSxJQUFHMUUsSUFBaEIsSUFBd0IwRSxHQUF4Qjs7O3dCQUdFQSxJQUFHM0UsUUFBUCxFQUFpQjs2QkFDUjZKLE9BQUwsQ0FBYWxGLEdBQWIsRUFBaUJBLElBQUczRSxRQUFwQixFQUE4QjhKLFVBQVUsSUFBVixHQUFrQkMsS0FBbEIsR0FBMkIsR0FBM0IsSUFBa0NFLE9BQUksQ0FBdEMsQ0FBOUIsRUFBd0VGLFFBQU0sQ0FBOUUsRUFBaUZKLFFBQWpGLEVBQTJGQyxZQUEzRjs7Ozs7OztxQ0FNSHJDLFVBQVUyQyxlQUFlMUwsT0FBT2xJLFFBQVE7Z0JBQzdDRSxPQUFPLElBQVg7O2dCQUVJdUosV0FBV21LLGNBQWNoTixNQUFkLENBQXFCLFVBQVNuRixDQUFULEVBQVk7dUJBQVNBLEVBQUUyRixJQUFGLElBQVUsS0FBakI7YUFBbkMsQ0FBZjs7Z0JBRUlHLFFBQVF2SCxPQUFPdUgsS0FBbkI7OztxQkFJS25ILElBREwsQ0FDVSxJQURWLEVBQ2dCLFVBQVNxQixDQUFULEVBQVk7b0JBQVEwSCxNQUFNakosS0FBS2dLLE1BQUwsQ0FBWXpJLENBQVosRUFBZXlHLEtBQWYsQ0FBWixDQUFtQyxPQUFPekcsRUFBRW9CLENBQUYsR0FBT3BCLEVBQUVvQixDQUFGLEdBQU1wQixFQUFFb0IsQ0FBUixHQUFhc0csSUFBSUMsRUFBSixHQUFTLENBQUNELElBQUlHLEVBQUosR0FBT0gsSUFBSUMsRUFBWixJQUFnQixDQUFwRDthQURqRSxFQUVLaEosSUFGTCxDQUVVLElBRlYsRUFFZ0IsVUFBU3FCLENBQVQsRUFBWTtvQkFBUTBILE1BQU1qSixLQUFLZ0ssTUFBTCxDQUFZekksQ0FBWixFQUFleUcsS0FBZixDQUFaLENBQW1DLE9BQU96RyxFQUFFc0IsQ0FBRixHQUFPdEIsRUFBRXNCLENBQUYsR0FBTXRCLEVBQUVzQixDQUFSLEdBQWFvRyxJQUFJRSxFQUFKLEdBQVMsQ0FBQ0YsSUFBSUksRUFBSixHQUFPSixJQUFJRSxFQUFaLElBQWdCLENBQXBEO2FBRmpFOzs7cUJBTUc5SSxFQURILENBQ00sWUFETixFQUNvQixVQUFVQyxDQUFWLEVBQWE7OzthQURqQyxFQUtHRCxFQUxILENBS00sWUFMTixFQUtvQixVQUFVQyxDQUFWLEVBQWE7O2FBTGpDLEVBUUdELEVBUkgsQ0FRTSxPQVJOLEVBUWUsVUFBVUMsQ0FBVixFQUFhO3VCQUFTb0IsT0FBUCxDQUFlLFVBQWYsRUFBMkJwQixDQUEzQixFQUFGO2FBUjVCOzs7cUJBV1c0USxXQUFULENBQXFCM1AsQ0FBckIsRUFBd0I7dUJBQ2J5UCxLQUFQLEdBQWU3TSxLQUFLeUssS0FBTCxDQUFXck4sRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVzQixDQUFuQixDQUFmOztxQkFFSzJPLElBQVQsQ0FBY2pOLE9BQUEsR0FDVGxFLEVBRFMsQ0FDTixPQURNLEVBQ0c2USxXQURILENBQWQ7O2lCQUdLeUMsb0JBQUwsQ0FBMEI3VCxNQUExQixFQUFrQ3lKLFFBQWxDOzs7OzZDQUdlekosUUFBUXdQLFFBQVE7Z0JBQzNCdFAsT0FBTyxJQUFiOzttQkFFTzBHLE1BQVAsQ0FBYyxVQUFVbkYsQ0FBVixFQUFhO3VCQUFTQSxFQUFFL0IsY0FBRixDQUFpQixPQUFqQixDQUFQO2FBQTdCLEVBQWtFUyxNQUFsRSxDQUF5RSxNQUF6RSxFQUNLQyxJQURMLENBQ1UsYUFEVixFQUN5QixRQUR6QixFQUVLb0IsSUFGTCxDQUVVLFVBQVNDLENBQVQsRUFBWTt1QkFBU21HLEdBQUcsYUFBSCxDQUFQO2FBRnhCLEVBR0s1RixPQUhMLENBR2EsWUFIYixFQUcyQixJQUgzQixFQUlLQSxPQUpMLENBSWEsVUFKYixFQUl5QixJQUp6QixFQUtLQSxPQUxMLENBS2EsTUFMYixFQUtxQixJQUxyQixFQU1LekIsRUFOTCxDQU1RLE9BTlIsRUFNaUIsVUFBU0MsQ0FBVCxFQUFZO3FCQUFPc1QsZ0JBQUwsQ0FBc0I5VCxNQUF0QixFQUE4QlEsQ0FBOUIsRUFBaUMsS0FBS3VCLFVBQXRDO2FBTi9COzs7OytCQVVHTixHQUFHeUcsT0FBTztnQkFDUHBHLFFBQVFMLEVBQUV5RixRQUFGLENBQVc4QixVQUFYLENBQXNCLENBQXRCLElBQTJCLEVBQXpDO2dCQUNJZCxNQUFNM0QsTUFBTixJQUFnQnpDLEtBQXBCLEVBQTJCOzs7Z0JBR3JCMkUsT0FBUXlCLE1BQU1wRyxLQUFOLEVBQWFwQyxjQUFiLENBQTRCLHFCQUE1QixJQUFxRHdJLE1BQU1wRyxLQUFOLEVBQWFtSCxtQkFBbEUsR0FBd0ZmLE1BQU1wRyxLQUFOLEVBQWFvSCxTQUFuSDtnQkFDTUMsTUFBTSxFQUFDQyxJQUFHM0MsS0FBSyxDQUFMLENBQUosRUFBWTRDLElBQUc1QyxLQUFLLENBQUwsQ0FBZixFQUF1QjZDLElBQUc3QyxLQUFLLENBQUwsSUFBVUEsS0FBSyxDQUFMLENBQXBDLEVBQTRDOEMsSUFBRzlDLEtBQUssQ0FBTCxJQUFVQSxLQUFLLENBQUwsQ0FBekQsRUFBWjs7bUJBRU8wQyxHQUFQOzs7O2lDQUdLOEgsVUFBVXpJLFdBQVdOLE9BQU87O2dCQUU1QjZKLEtBQUtkLFNBQVM3USxJQUFULENBQWMsU0FBZCxFQUF5QjhPLEtBQXpCLENBQStCLEdBQS9CLENBQVQ7O2dCQUVJOEMsWUFBWUQsR0FBRyxDQUFILENBQWhCOztnQkFFSWxELFdBQVdtRCxZQUFZLENBQTNCOztxQkFFU2xSLE1BQVQsQ0FBZ0IsV0FBaEIsRUFBNkJJLFNBQTdCLENBQXVDLGlCQUF2QyxFQUEwRDBGLE1BQTFELENBQWlFLFVBQVVuRixDQUFWLEVBQWE7b0JBQ3BFSyxRQUFRTCxFQUFFeUYsUUFBRixDQUFXOEIsVUFBWCxDQUFzQixDQUF0QixJQUEyQixFQUF6Qzt1QkFDUWxILFNBQVMwRyxTQUFqQjthQUZKLEVBSUNsRyxJQUpELENBSU0sVUFBVWIsQ0FBVixFQUFhO2tCQUNib0IsQ0FBRixHQUFPLENBQUVnTSxXQUFTcE4sRUFBRTRELE1BQUYsQ0FBUzBPLEtBQW5CLEdBQTRCLENBQTdCLElBQWdDLENBQWpDLEdBQXVDdFMsRUFBRTRELE1BQUYsQ0FBUzJPLEdBQVQsSUFBZ0JuRixXQUFTcE4sRUFBRTRELE1BQUYsQ0FBUzBPLEtBQWxDLENBQTdDO2tCQUNFaFIsQ0FBRixHQUFPLEtBQUcsQ0FBSixHQUFVdEIsRUFBRTRELE1BQUYsQ0FBUzRPLE1BQVQsR0FBa0IsRUFBbEM7eUJBQ0QsQ0FBVSxJQUFWLEVBQWdCblQsTUFBaEIsQ0FBdUIsTUFBdkIsRUFDR1YsSUFESCxDQUNRLEdBRFIsRUFDYSxVQUFVcUIsQ0FBVixFQUFhOzJCQUFTLEVBQUdvTixXQUFTcE4sRUFBRTRELE1BQUYsQ0FBUzBPLEtBQW5CLEdBQTRCLENBQTlCLElBQWlDLENBQXhDO2lCQUQ1QixFQUVHM1QsSUFGSCxDQUVRLEdBRlIsRUFFYSxVQUFVcUIsQ0FBVixFQUFhOzJCQUFTLENBQUNnRCxTQUFBLENBQVUsSUFBVixFQUFnQnJFLElBQWhCLENBQXFCLFFBQXJCLENBQUQsR0FBa0MsQ0FBekM7aUJBRjVCLEVBR0dBLElBSEgsQ0FHUSxPQUhSLEVBR2lCLFVBQVVxQixDQUFWLEVBQWE7MkJBQVVvTixXQUFTcE4sRUFBRTRELE1BQUYsQ0FBUzBPLEtBQTFCO2lCQUhoQzs7eUJBS0EsQ0FBVSxJQUFWLEVBQWdCalQsTUFBaEIsQ0FBdUIsZUFBdkIsRUFBd0NWLElBQXhDLENBQTZDLFdBQTdDLEVBQTBELFVBQVMyRyxDQUFULEVBQVk7MkJBQzdELGdCQUFrQjhILFdBQVNwTixFQUFFNEQsTUFBRixDQUFTME8sS0FBbkIsR0FBMEIsQ0FBM0IsR0FBOEIsRUFBOUMsSUFBb0QsUUFBM0Q7aUJBREY7O3lCQUlBLENBQVUsSUFBVixFQUFnQmpULE1BQWhCLENBQXVCLE1BQXZCLEVBQStCVixJQUEvQixDQUFvQyxXQUFwQyxFQUFpRCxVQUFTMkcsQ0FBVCxFQUFZO3dCQUNyRDhCLE9BQU8sS0FBS3JHLE9BQUwsRUFBYjt3QkFDTTBSLFFBQVF6UCxTQUFBLENBQVUsS0FBSzFDLFVBQWYsRUFBMkJqQixNQUEzQixDQUFrQyxNQUFsQyxFQUEwQzRCLElBQTFDLEdBQWlERixPQUFqRCxFQUFkO3dCQUNJZ0IsUUFBUSxDQUFaO3dCQUNLMFEsTUFBTWpSLE1BQU4sR0FBZTRGLEtBQUs1RixNQUFyQixHQUErQmlSLE1BQU1sUixLQUFOLElBQWU2RixLQUFLN0YsS0FBTCxHQUFhLEVBQTVCLENBQS9CLElBQW1Fa1IsTUFBTWpSLE1BQU4sR0FBZTRGLEtBQUs1RixNQUFyQixHQUErQixDQUFyRyxFQUF3RztnQ0FDM0ZpUixNQUFNalIsTUFBTixHQUFlNEYsS0FBSzVGLE1BQXJCLElBQWdDLENBQWhDLEdBQW9DLENBQXBDLEdBQXVDaVIsTUFBTWpSLE1BQU4sR0FBZTRGLEtBQUs1RixNQUFuRTtxQkFESixNQUVPO2dDQUNLb0IsS0FBS2tILEdBQUwsQ0FBUzJJLE1BQU1sUixLQUFOLElBQWU2RixLQUFLN0YsS0FBTCxHQUFhLEVBQTVCLENBQVQsRUFBMEMsQ0FBMUMsS0FBZ0QsQ0FBaEQsR0FBb0QsQ0FBcEQsR0FBc0RxQixLQUFLa0gsR0FBTCxDQUFTMkksTUFBTWxSLEtBQU4sSUFBZTZGLEtBQUs3RixLQUFMLEdBQWEsRUFBNUIsQ0FBVCxFQUEwQyxDQUExQyxDQUE5RDs7MkJBRUcsV0FBV1EsS0FBWCxHQUFtQixHQUExQjtpQkFURjthQWhCSDs7cUJBOEJVMUMsTUFBVCxDQUFnQixXQUFoQixFQUE2QkksU0FBN0IsQ0FBdUMsT0FBdkMsRUFBZ0QwRixNQUFoRCxDQUF1RCxVQUFVbkYsQ0FBVixFQUFhO3VCQUN6REEsRUFBRTRELE1BQUYsQ0FBU29PLEtBQVQsSUFBa0IsQ0FBMUI7YUFESCxFQUVHelMsS0FGSCxDQUVTLFNBRlQsRUFFb0IsQ0FGcEIsRUFFdUJBLEtBRnZCLENBRTZCLFNBRjdCLEVBRXVDLE1BRnZDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt5Q0EyQ1loQixRQUFRUSxHQUFHYixRQUFRO29CQUNsQyxDQUFTOFEsZUFBVDtnQkFDSXpRLE9BQU9rUixLQUFQLElBQWdCN00sS0FBS3lLLEtBQUwsQ0FBV3RPLEVBQUVxQyxDQUFGLEdBQU1yQyxFQUFFdUMsQ0FBbkIsQ0FBcEIsRUFBMkM7dUJBQ2pDZ0YsVUFBUCxDQUFrQm9KLE1BQWxCLENBQXlCM1EsQ0FBekIsRUFBNEJpRSxTQUFBLENBQVU5RSxNQUFWLENBQTVCO2FBREgsTUFFTzt1QkFDR29JLFVBQVAsQ0FBa0J0SCxLQUFsQixDQUF3QkQsQ0FBeEI7Ozs7O2tDQUlLUixRQUFReUIsR0FBRzBTLFlBQVk7Z0JBQzNCalUsT0FBTyxJQUFYOzs7OztnQkFLSXdKLFdBQVdqRixTQUFBLENBQVUwUCxXQUFXcFMsVUFBckIsRUFBaUNiLFNBQWpDLENBQTJDLFlBQVVPLEVBQUVrSSxJQUF2RCxDQUFmOztnQkFHSUQsU0FBUzBLLElBQVQsS0FBa0IsQ0FBdEIsRUFBeUI7O29CQUVqQkMsV0FBVzNLLFNBQVMxSSxLQUFULENBQWUsU0FBZixLQUE2QixNQUE1Qzs7cUJBRUtzVCxrQkFBTCxDQUF3QjdTLENBQXhCLEVBQTJCMFMsVUFBM0I7Ozt5QkFHYW5ULEtBQVQsQ0FBZSxTQUFmLEVBQTBCLENBQTFCLEVBQTZCZ0UsVUFBN0IsR0FBMEN1UCxRQUExQyxDQUFtRCxJQUFuRCxFQUF5RHZULEtBQXpELENBQStELFNBQS9ELEVBQTBFLENBQTFFLEVBQTZFQSxLQUE3RSxDQUFtRixTQUFuRixFQUE2RixPQUE3Rjt5QkFDQSxDQUFVbVQsVUFBVixFQUFzQm5TLE9BQXRCLENBQThCLGNBQTlCLEVBQThDLElBQTlDOzs7Ozs7O2FBUlIsTUFlTzs7O3FCQUdDc1Msa0JBQUwsQ0FBd0I3UyxDQUF4QixFQUEyQjBTLFVBQTNCOztvQkFFSTFQLFNBQUEsQ0FBVSxPQUFWLEVBQW1CMlAsSUFBbkIsTUFBNkIsQ0FBakMsRUFBb0M7eUJBQzNCRSxrQkFBTCxDQUF3QjdTLENBQXhCLEVBQTJCMFMsVUFBM0I7NkJBQ0EsQ0FBVUEsVUFBVixFQUFzQm5TLE9BQXRCLENBQThCLGNBQTlCLEVBQThDLElBQTlDOzt3QkFFSSxDQUFDUCxFQUFFK1MsT0FBUCxFQUFnQjs0QkFDUkEsVUFBVS9QLFNBQUEsQ0FBVXpFLE9BQU8wRixPQUFQLENBQWUzRCxVQUF6QixFQUFxQzVCLE1BQXJDLENBQTRDLEtBQTVDLEVBQW1ENkIsT0FBbkQsQ0FBMkQsTUFBM0QsRUFBbUUsSUFBbkUsQ0FBZDtnQ0FDUWhCLEtBQVIsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCO2dDQUNRMEQsSUFBUixDQUFhLFlBQWI7Ozs7d0JBSUErUCxNQUFPaFQsRUFBRStTLE9BQUYsQ0FBVUMsR0FBVixDQUFjL0YsT0FBZCxDQUFzQixHQUF0QixLQUE4QixDQUFDLENBQS9CLEdBQW1Dak4sRUFBRStTLE9BQUYsQ0FBVUMsR0FBVixHQUFnQixLQUFoQixHQUF3QnBRLEtBQUtxUSxNQUFMLEVBQTNELEdBQTJFalQsRUFBRStTLE9BQUYsQ0FBVUMsR0FBVixHQUFnQixLQUFoQixHQUF3QnBRLEtBQUtxUSxNQUFMLEVBQTlHOzRCQUNReFAsR0FBUixDQUFZLGNBQWF1UCxHQUF6QjsyQkFDQSxDQUFRQSxHQUFSLEVBQWEsVUFBU0UsT0FBVCxFQUFlOzs0QkFHcEJILFVBQVUvUCxTQUFBLENBQVV6RSxPQUFPMEYsT0FBUCxDQUFlM0QsVUFBekIsRUFBcUM1QixNQUFyQyxDQUE0QyxLQUE1QyxFQUFtRDZCLE9BQW5ELENBQTJELE1BQTNELEVBQW1FLElBQW5FLENBQWQ7Ozs7Ozs2QkFNSytHLFdBQUwsQ0FBa0IvSSxNQUFsQjs7NEJBRUk0VSxLQUFLLElBQUlyUCxZQUFKLEVBQVQ7MkJBQ0doRixFQUFILENBQU0sUUFBTixFQUFnQixVQUFTcUYsUUFBVCxFQUFnQmlQLGdCQUFoQixFQUFrQ2hQLEdBQWxDLEVBQXVDO21DQUFTakUsT0FBUCxDQUFlZ0UsUUFBZixFQUFzQm5FLENBQXRCLEVBQXlCb0UsR0FBekI7eUJBQXpEOzJCQUNHaVAsU0FBSCxDQUFjTixRQUFROVIsSUFBUixFQUFkOzJCQUNHcVMsUUFBSCxDQUFZSixPQUFaOzJCQUNHSyxNQUFIOzJCQUNHak0sV0FBSDs7NkJBRUs2TCxFQUFMLEdBQVVBLEVBQVY7cUJBbEJKOzs7Ozs7b0NBd0JHNVUsUUFBUTtnQkFDZitSLEtBQUt0TixTQUFBLENBQVV6RSxPQUFPMEYsT0FBakIsRUFBMEJ0RixJQUExQixDQUErQixTQUEvQixFQUEwQzhPLEtBQTFDLENBQWdELEdBQWhELENBQVQ7O2dCQUVJK0YsTUFBTXhRLFNBQUEsQ0FBVXpFLE9BQU8wRixPQUFqQixFQUEwQjVFLE1BQTFCLENBQWlDLFdBQWpDLEVBQThDNEIsSUFBOUMsR0FBcURTLHFCQUFyRCxFQUFWO2dCQUNJVyxXQUFXQyxTQUFTQyxJQUFULENBQWNiLHFCQUFkLEVBQWY7O2dCQUVJNk8sWUFBWUQsR0FBRyxDQUFILENBQWhCO2dCQUNJRSxhQUFhRixHQUFHLENBQUgsQ0FBakI7O2dCQUVJekosWUFBWTdELFNBQUEsQ0FBVXpFLE9BQU8wRixPQUFqQixFQUEwQnRGLElBQTFCLENBQStCLE9BQS9CLENBQWhCO2dCQUNJZ0ksYUFBYTNELFNBQUEsQ0FBVXpFLE9BQU8wRixPQUFqQixFQUEwQnRGLElBQTFCLENBQStCLFFBQS9CLENBQWpCOzt3QkFFWTJELFNBQVNDLElBQVQsQ0FBY3lCLFdBQTFCO3lCQUNhMUIsU0FBU0MsSUFBVCxDQUFjcUUsWUFBM0I7O2dCQUVJdkYsTUFBTXVCLEtBQUs2USxJQUFMLENBQVdELElBQUluUyxHQUFKLEdBQVVnQixTQUFTaEIsR0FBcEIsR0FBMkJtUyxJQUFJaFMsTUFBL0IsR0FBd0MsQ0FBbEQsQ0FBVjs7Z0JBRUlxRCxPQUFPLEVBQUMsVUFBUzhCLFVBQVYsRUFBc0IsU0FBUUUsU0FBOUIsRUFBWDs7Z0JBRUlrTSxVQUFVL1AsU0FBQSxDQUFVekUsT0FBTzBGLE9BQVAsQ0FBZTNELFVBQXpCLEVBQXFDakIsTUFBckMsQ0FBNEMsT0FBNUMsQ0FBZDs7b0JBRVFFLEtBQVIsQ0FBYyxLQUFkLEVBQXFCOEIsTUFBTSxJQUEzQjtvQkFDUTlCLEtBQVIsQ0FBYyxNQUFkLEVBQXNCLEtBQXRCO29CQUNRQSxLQUFSLENBQWMsT0FBZCxFQUF3QnNGLEtBQUt0RCxLQUFOLEdBQWUsSUFBdEM7b0JBQ1FoQyxLQUFSLENBQWMsUUFBZCxFQUF5QnNGLEtBQUtyRCxNQUFMLEdBQWdCSCxHQUFqQixHQUF5QixJQUFqRDs7Z0JBRUksS0FBSzhSLEVBQVQsRUFBYTtxQkFDSkEsRUFBTCxDQUFRN0wsV0FBUjs7Ozs7MkNBSWFvTSxTQUFTaEIsWUFBWTs7cUJBRXJDLENBQVVBLFdBQVdwUyxVQUFyQixFQUFpQ2IsU0FBakMsQ0FBMkMsZ0JBQTNDLEVBQTZEb0IsSUFBN0QsQ0FBa0UsVUFBVThTLENBQVYsRUFBYTs7b0JBRXZFRCxRQUFROVAsTUFBUixDQUFlb08sS0FBZixJQUF3QjJCLEVBQUUvUCxNQUFGLENBQVNvTyxLQUFyQyxFQUE0Qzs2QkFDeEMsQ0FBVSxJQUFWLEVBQWdCelIsT0FBaEIsQ0FBd0IsY0FBeEIsRUFBd0MsS0FBeEM7d0JBQ0lxVCxtQkFBbUI1USxTQUFBLENBQVUwUCxXQUFXcFMsVUFBckIsRUFBaUNiLFNBQWpDLENBQTJDLFlBQVVrVSxFQUFFekwsSUFBdkQsQ0FBdkI7cUNBQ2lCM0UsVUFBakIsR0FBOEJoRSxLQUE5QixDQUFvQyxTQUFwQyxFQUErQyxDQUEvQyxFQUFrREEsS0FBbEQsQ0FBd0QsU0FBeEQsRUFBa0UsTUFBbEU7O2FBTFI7Ozs7Ozs7Ozs7O3FCQWtCQSxDQUFVbVQsV0FBV3BTLFVBQVgsQ0FBc0JBLFVBQXRCLENBQWlDQSxVQUEzQyxFQUF1RGIsU0FBdkQsQ0FBaUUsT0FBakUsRUFBMEVLLE1BQTFFOzs7O0lBS0w7Ozs7QUMzWkEsSUFBSStULFlBQUo7O0FBRUEsU0FBU0MsS0FBVCxHQUFrQjtXQUNQRCxhQUFhRSxLQUFiLENBQW1CLElBQW5CLEVBQXlCQyxTQUF6QixDQUFQOzs7OztBQUtKLFNBQVNDLGVBQVQsQ0FBMEJDLFFBQTFCLEVBQW9DO21CQUNqQkEsUUFBZjs7O0FDWFcsU0FBU0MsT0FBVCxDQUFpQkMsS0FBakIsRUFBd0I7V0FDNUJBLGlCQUFpQkMsS0FBakIsSUFBMEJ2VyxPQUFPd1csU0FBUCxDQUFpQkMsUUFBakIsQ0FBMEJ0RSxJQUExQixDQUErQm1FLEtBQS9CLE1BQTBDLGdCQUEzRTs7O0FDRFcsU0FBU0ksUUFBVCxDQUFrQkosS0FBbEIsRUFBeUI7OztXQUc3QkEsU0FBUyxJQUFULElBQWlCdFcsT0FBT3dXLFNBQVAsQ0FBaUJDLFFBQWpCLENBQTBCdEUsSUFBMUIsQ0FBK0JtRSxLQUEvQixNQUEwQyxpQkFBbEU7OztBQ0hXLFNBQVNLLGFBQVQsQ0FBdUJ0RSxHQUF2QixFQUE0QjtRQUNuQ3VFLENBQUo7U0FDS0EsQ0FBTCxJQUFVdkUsR0FBVixFQUFlOztlQUVKLEtBQVA7O1dBRUcsSUFBUDs7O0FDTlcsU0FBU3dFLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCO1dBQzdCLE9BQU9BLEtBQVAsS0FBaUIsUUFBakIsSUFBNkJ0VyxPQUFPd1csU0FBUCxDQUFpQkMsUUFBakIsQ0FBMEJ0RSxJQUExQixDQUErQm1FLEtBQS9CLE1BQTBDLGlCQUE5RTs7O0FDRFcsU0FBU1EsTUFBVCxDQUFnQlIsS0FBaEIsRUFBdUI7V0FDM0JBLGlCQUFpQlMsSUFBakIsSUFBeUIvVyxPQUFPd1csU0FBUCxDQUFpQkMsUUFBakIsQ0FBMEJ0RSxJQUExQixDQUErQm1FLEtBQS9CLE1BQTBDLGVBQTFFOzs7QUNEVyxTQUFTVSxHQUFULENBQWFDLEdBQWIsRUFBa0JDLEVBQWxCLEVBQXNCO1FBQzdCQyxNQUFNLEVBQVY7UUFBYzlTLENBQWQ7U0FDS0EsSUFBSSxDQUFULEVBQVlBLElBQUk0UyxJQUFJalMsTUFBcEIsRUFBNEIsRUFBRVgsQ0FBOUIsRUFBaUM7WUFDekJqRCxJQUFKLENBQVM4VixHQUFHRCxJQUFJNVMsQ0FBSixDQUFILEVBQVdBLENBQVgsQ0FBVDs7V0FFRzhTLEdBQVA7OztBQ0xXLFNBQVNDLFVBQVQsQ0FBb0I1UCxDQUFwQixFQUF1QkMsQ0FBdkIsRUFBMEI7V0FDOUJ6SCxPQUFPd1csU0FBUCxDQUFpQnJXLGNBQWpCLENBQWdDZ1MsSUFBaEMsQ0FBcUMzSyxDQUFyQyxFQUF3Q0MsQ0FBeEMsQ0FBUDs7O0FDQ1csU0FBUzRQLE1BQVQsQ0FBZ0I3UCxDQUFoQixFQUFtQkMsQ0FBbkIsRUFBc0I7U0FDNUIsSUFBSXBELENBQVQsSUFBY29ELENBQWQsRUFBaUI7WUFDVDJQLFdBQVczUCxDQUFYLEVBQWNwRCxDQUFkLENBQUosRUFBc0I7Y0FDaEJBLENBQUYsSUFBT29ELEVBQUVwRCxDQUFGLENBQVA7Ozs7UUFJSitTLFdBQVczUCxDQUFYLEVBQWMsVUFBZCxDQUFKLEVBQStCO1VBQ3pCZ1AsUUFBRixHQUFhaFAsRUFBRWdQLFFBQWY7OztRQUdBVyxXQUFXM1AsQ0FBWCxFQUFjLFNBQWQsQ0FBSixFQUE4QjtVQUN4QjZQLE9BQUYsR0FBWTdQLEVBQUU2UCxPQUFkOzs7V0FHRzlQLENBQVA7OztBQ2ZHLFNBQVMrUCxTQUFULENBQW9CakIsS0FBcEIsRUFBMkJrQixNQUEzQixFQUFtQ0MsTUFBbkMsRUFBMkNDLE1BQTNDLEVBQW1EO1dBQy9DQyxpQkFBaUJyQixLQUFqQixFQUF3QmtCLE1BQXhCLEVBQWdDQyxNQUFoQyxFQUF3Q0MsTUFBeEMsRUFBZ0QsSUFBaEQsRUFBc0RFLEdBQXRELEVBQVA7OztBQ0hKLFNBQVNDLG1CQUFULEdBQStCOztXQUVwQjtlQUNlLEtBRGY7c0JBRWUsRUFGZjtxQkFHZSxFQUhmO2tCQUllLENBQUMsQ0FKaEI7dUJBS2UsQ0FMZjttQkFNZSxLQU5mO3NCQU9lLElBUGY7dUJBUWUsS0FSZjt5QkFTZSxLQVRmO2FBVWUsS0FWZjt5QkFXZSxFQVhmO2tCQVllO0tBWnRCOzs7QUFnQkosQUFBZSxTQUFTQyxlQUFULENBQXlCQyxDQUF6QixFQUE0QjtRQUNuQ0EsRUFBRUMsR0FBRixJQUFTLElBQWIsRUFBbUI7VUFDYkEsR0FBRixHQUFRSCxxQkFBUjs7V0FFR0UsRUFBRUMsR0FBVDs7O0FDdEJKLElBQUlDLElBQUo7QUFDQSxJQUFJMUIsTUFBTUMsU0FBTixDQUFnQnlCLElBQXBCLEVBQTBCO1dBQ2YxQixNQUFNQyxTQUFOLENBQWdCeUIsSUFBdkI7Q0FESixNQUVPO1dBQ0ksY0FBVUMsR0FBVixFQUFlO1lBQ2RqSCxJQUFJalIsT0FBTyxJQUFQLENBQVI7WUFDSW1ZLE1BQU1sSCxFQUFFak0sTUFBRixLQUFhLENBQXZCOzthQUVLLElBQUlYLElBQUksQ0FBYixFQUFnQkEsSUFBSThULEdBQXBCLEVBQXlCOVQsR0FBekIsRUFBOEI7Z0JBQ3RCQSxLQUFLNE0sQ0FBTCxJQUFVaUgsSUFBSS9GLElBQUosQ0FBUyxJQUFULEVBQWVsQixFQUFFNU0sQ0FBRixDQUFmLEVBQXFCQSxDQUFyQixFQUF3QjRNLENBQXhCLENBQWQsRUFBMEM7dUJBQy9CLElBQVA7Ozs7ZUFJRCxLQUFQO0tBVko7Q0FjSjs7QUNiTyxTQUFTbUgsT0FBVCxDQUFpQkwsQ0FBakIsRUFBb0I7UUFDbkJBLEVBQUVNLFFBQUYsSUFBYyxJQUFsQixFQUF3QjtZQUNoQkMsUUFBUVIsZ0JBQWdCQyxDQUFoQixDQUFaO1lBQ0lRLGNBQWNOLEtBQUs5RixJQUFMLENBQVVtRyxNQUFNRSxlQUFoQixFQUFpQyxVQUFVblUsQ0FBVixFQUFhO21CQUNyREEsS0FBSyxJQUFaO1NBRGMsQ0FBbEI7WUFHSW9VLGFBQWEsQ0FBQzVKLE1BQU1rSixFQUFFVyxFQUFGLENBQUtDLE9BQUwsRUFBTixDQUFELElBQ2JMLE1BQU1NLFFBQU4sR0FBaUIsQ0FESixJQUViLENBQUNOLE1BQU1PLEtBRk0sSUFHYixDQUFDUCxNQUFNUSxZQUhNLElBSWIsQ0FBQ1IsTUFBTVMsY0FKTSxJQUtiLENBQUNULE1BQU1VLFNBTE0sSUFNYixDQUFDVixNQUFNVyxhQU5NLElBT2IsQ0FBQ1gsTUFBTVksZUFQTSxLQVFaLENBQUNaLE1BQU1hLFFBQVAsSUFBb0JiLE1BQU1hLFFBQU4sSUFBa0JaLFdBUjFCLENBQWpCOztZQVVJUixFQUFFcUIsT0FBTixFQUFlO3lCQUNFWCxjQUNUSCxNQUFNZSxhQUFOLEtBQXdCLENBRGYsSUFFVGYsTUFBTWdCLFlBQU4sQ0FBbUJ0VSxNQUFuQixLQUE4QixDQUZyQixJQUdUc1QsTUFBTWlCLE9BQU4sS0FBa0JDLFNBSHRCOzs7WUFNQXhaLE9BQU95WixRQUFQLElBQW1CLElBQW5CLElBQTJCLENBQUN6WixPQUFPeVosUUFBUCxDQUFnQjFCLENBQWhCLENBQWhDLEVBQW9EO2NBQzlDTSxRQUFGLEdBQWFJLFVBQWI7U0FESixNQUdLO21CQUNNQSxVQUFQOzs7V0FHRFYsRUFBRU0sUUFBVDs7O0FBR0osQUFBTyxTQUFTcUIsYUFBVCxDQUF3QnBCLEtBQXhCLEVBQStCO1FBQzlCUCxJQUFJUixVQUFVb0MsR0FBVixDQUFSO1FBQ0lyQixTQUFTLElBQWIsRUFBbUI7ZUFDUlIsZ0JBQWdCQyxDQUFoQixDQUFQLEVBQTJCTyxLQUEzQjtLQURKLE1BR0s7d0JBQ2VQLENBQWhCLEVBQW1CbUIsZUFBbkIsR0FBcUMsSUFBckM7OztXQUdHbkIsQ0FBUDs7O0FDL0NXLFNBQVM2QixXQUFULENBQXFCdEQsS0FBckIsRUFBNEI7V0FDaENBLFVBQVUsS0FBSyxDQUF0Qjs7O0FDSUo7O0FBRUEsSUFBSXVELG1CQUFtQjdELE1BQU02RCxnQkFBTixHQUF5QixFQUFoRDs7QUFFQSxBQUFPLFNBQVNDLFVBQVQsQ0FBb0JwRyxFQUFwQixFQUF3QnFHLElBQXhCLEVBQThCO1FBQzdCMVYsQ0FBSixFQUFPMlYsSUFBUCxFQUFhQyxHQUFiOztRQUVJLENBQUNMLFlBQVlHLEtBQUtHLGdCQUFqQixDQUFMLEVBQXlDO1dBQ2xDQSxnQkFBSCxHQUFzQkgsS0FBS0csZ0JBQTNCOztRQUVBLENBQUNOLFlBQVlHLEtBQUtJLEVBQWpCLENBQUwsRUFBMkI7V0FDcEJBLEVBQUgsR0FBUUosS0FBS0ksRUFBYjs7UUFFQSxDQUFDUCxZQUFZRyxLQUFLSyxFQUFqQixDQUFMLEVBQTJCO1dBQ3BCQSxFQUFILEdBQVFMLEtBQUtLLEVBQWI7O1FBRUEsQ0FBQ1IsWUFBWUcsS0FBS00sRUFBakIsQ0FBTCxFQUEyQjtXQUNwQkEsRUFBSCxHQUFRTixLQUFLTSxFQUFiOztRQUVBLENBQUNULFlBQVlHLEtBQUtYLE9BQWpCLENBQUwsRUFBZ0M7V0FDekJBLE9BQUgsR0FBYVcsS0FBS1gsT0FBbEI7O1FBRUEsQ0FBQ1EsWUFBWUcsS0FBS08sSUFBakIsQ0FBTCxFQUE2QjtXQUN0QkEsSUFBSCxHQUFVUCxLQUFLTyxJQUFmOztRQUVBLENBQUNWLFlBQVlHLEtBQUtRLE1BQWpCLENBQUwsRUFBK0I7V0FDeEJBLE1BQUgsR0FBWVIsS0FBS1EsTUFBakI7O1FBRUEsQ0FBQ1gsWUFBWUcsS0FBS1MsT0FBakIsQ0FBTCxFQUFnQztXQUN6QkEsT0FBSCxHQUFhVCxLQUFLUyxPQUFsQjs7UUFFQSxDQUFDWixZQUFZRyxLQUFLL0IsR0FBakIsQ0FBTCxFQUE0QjtXQUNyQkEsR0FBSCxHQUFTRixnQkFBZ0JpQyxJQUFoQixDQUFUOztRQUVBLENBQUNILFlBQVlHLEtBQUtVLE9BQWpCLENBQUwsRUFBZ0M7V0FDekJBLE9BQUgsR0FBYVYsS0FBS1UsT0FBbEI7OztRQUdBWixpQkFBaUI3VSxNQUFqQixHQUEwQixDQUE5QixFQUFpQzthQUN4QlgsQ0FBTCxJQUFVd1YsZ0JBQVYsRUFBNEI7bUJBQ2pCQSxpQkFBaUJ4VixDQUFqQixDQUFQO2tCQUNNMFYsS0FBS0MsSUFBTCxDQUFOO2dCQUNJLENBQUNKLFlBQVlLLEdBQVosQ0FBTCxFQUF1QjttQkFDaEJELElBQUgsSUFBV0MsR0FBWDs7Ozs7V0FLTHZHLEVBQVA7OztBQUdKLElBQUlnSCxtQkFBbUIsS0FBdkI7OztBQUdBLEFBQU8sU0FBU0MsTUFBVCxDQUFnQi9SLE1BQWhCLEVBQXdCO2VBQ2hCLElBQVgsRUFBaUJBLE1BQWpCO1NBQ0s4UCxFQUFMLEdBQVUsSUFBSTNCLElBQUosQ0FBU25PLE9BQU84UCxFQUFQLElBQWEsSUFBYixHQUFvQjlQLE9BQU84UCxFQUFQLENBQVVDLE9BQVYsRUFBcEIsR0FBMENnQixHQUFuRCxDQUFWO1FBQ0ksQ0FBQyxLQUFLdkIsT0FBTCxFQUFMLEVBQXFCO2FBQ1pNLEVBQUwsR0FBVSxJQUFJM0IsSUFBSixDQUFTNEMsR0FBVCxDQUFWOzs7O1FBSUFlLHFCQUFxQixLQUF6QixFQUFnQzsyQkFDVCxJQUFuQjtjQUNNRSxZQUFOLENBQW1CLElBQW5COzJCQUNtQixLQUFuQjs7OztBQUlSLEFBQU8sU0FBU0MsUUFBVCxDQUFtQnhJLEdBQW5CLEVBQXdCO1dBQ3BCQSxlQUFlc0ksTUFBZixJQUEwQnRJLE9BQU8sSUFBUCxJQUFlQSxJQUFJNkgsZ0JBQUosSUFBd0IsSUFBeEU7OztBQzNFVyxTQUFTWSxRQUFULENBQW1CQyxNQUFuQixFQUEyQjtRQUNsQ0EsU0FBUyxDQUFiLEVBQWdCOztlQUVMalcsS0FBSzZRLElBQUwsQ0FBVW9GLE1BQVYsS0FBcUIsQ0FBNUI7S0FGSixNQUdPO2VBQ0lqVyxLQUFLa1csS0FBTCxDQUFXRCxNQUFYLENBQVA7Ozs7QUNITyxTQUFTRSxLQUFULENBQWVDLG1CQUFmLEVBQW9DO1FBQzNDQyxnQkFBZ0IsQ0FBQ0QsbUJBQXJCO1FBQ0k3VixRQUFRLENBRFo7O1FBR0k4VixrQkFBa0IsQ0FBbEIsSUFBdUJDLFNBQVNELGFBQVQsQ0FBM0IsRUFBb0Q7Z0JBQ3hDTCxTQUFTSyxhQUFULENBQVI7OztXQUdHOVYsS0FBUDs7O0FDUko7QUFDQSxBQUFlLFNBQVNnVyxhQUFULENBQXVCQyxNQUF2QixFQUErQkMsTUFBL0IsRUFBdUNDLFdBQXZDLEVBQW9EO1FBQzNEckQsTUFBTXJULEtBQUtrSCxHQUFMLENBQVNzUCxPQUFPdFcsTUFBaEIsRUFBd0J1VyxPQUFPdlcsTUFBL0IsQ0FBVjtRQUNJeVcsYUFBYTNXLEtBQUs2SixHQUFMLENBQVMyTSxPQUFPdFcsTUFBUCxHQUFnQnVXLE9BQU92VyxNQUFoQyxDQURqQjtRQUVJMFcsUUFBUSxDQUZaO1FBR0lyWCxDQUhKO1NBSUtBLElBQUksQ0FBVCxFQUFZQSxJQUFJOFQsR0FBaEIsRUFBcUI5VCxHQUFyQixFQUEwQjtZQUNqQm1YLGVBQWVGLE9BQU9qWCxDQUFQLE1BQWNrWCxPQUFPbFgsQ0FBUCxDQUE5QixJQUNDLENBQUNtWCxXQUFELElBQWdCUCxNQUFNSyxPQUFPalgsQ0FBUCxDQUFOLE1BQXFCNFcsTUFBTU0sT0FBT2xYLENBQVAsQ0FBTixDQUQxQyxFQUM2RDs7OztXQUkxRHFYLFFBQVFELFVBQWY7OztBQ1ZKLFNBQVNFLElBQVQsQ0FBY0MsR0FBZCxFQUFtQjtRQUNYNUYsTUFBTTZGLDJCQUFOLEtBQXNDLEtBQXRDLElBQ0ssT0FBT0MsT0FBUCxLQUFvQixXQUR6QixJQUN5Q0EsUUFBUUgsSUFEckQsRUFDMkQ7Z0JBQy9DQSxJQUFSLENBQWEsMEJBQTBCQyxHQUF2Qzs7OztBQUlSLEFBQU8sU0FBU0csU0FBVCxDQUFtQkgsR0FBbkIsRUFBd0IxRSxFQUF4QixFQUE0QjtRQUMzQjhFLFlBQVksSUFBaEI7O1dBRU8zRSxPQUFPLFlBQVk7WUFDbEJyQixNQUFNaUcsa0JBQU4sSUFBNEIsSUFBaEMsRUFBc0M7a0JBQzVCQSxrQkFBTixDQUF5QixJQUF6QixFQUErQkwsR0FBL0I7O1lBRUFJLFNBQUosRUFBZTtnQkFDUEUsT0FBTyxFQUFYO2dCQUNJQyxHQUFKO2lCQUNLLElBQUk5WCxJQUFJLENBQWIsRUFBZ0JBLElBQUk2UixVQUFVbFIsTUFBOUIsRUFBc0NYLEdBQXRDLEVBQTJDO3NCQUNqQyxFQUFOO29CQUNJK1gsUUFBT2xHLFVBQVU3UixDQUFWLENBQVAsTUFBd0IsUUFBNUIsRUFBc0M7MkJBQzNCLFFBQVFBLENBQVIsR0FBWSxJQUFuQjt5QkFDSyxJQUFJZSxHQUFULElBQWdCOFEsVUFBVSxDQUFWLENBQWhCLEVBQThCOytCQUNuQjlRLE1BQU0sSUFBTixHQUFhOFEsVUFBVSxDQUFWLEVBQWE5USxHQUFiLENBQWIsR0FBaUMsSUFBeEM7OzBCQUVFK1csSUFBSUUsS0FBSixDQUFVLENBQVYsRUFBYSxDQUFDLENBQWQsQ0FBTixDQUxrQztpQkFBdEMsTUFNTzswQkFDR25HLFVBQVU3UixDQUFWLENBQU47O3FCQUVDakQsSUFBTCxDQUFVK2EsR0FBVjs7aUJBRUNQLE1BQU0sZUFBTixHQUF3QnJGLE1BQU1DLFNBQU4sQ0FBZ0I2RixLQUFoQixDQUFzQmxLLElBQXRCLENBQTJCK0osSUFBM0IsRUFBaUNJLElBQWpDLENBQXNDLEVBQXRDLENBQXhCLEdBQW9FLElBQXBFLEdBQTRFLElBQUlDLEtBQUosRUFBRCxDQUFjQyxLQUE5Rjt3QkFDWSxLQUFaOztlQUVHdEYsR0FBR2pCLEtBQUgsQ0FBUyxJQUFULEVBQWVDLFNBQWYsQ0FBUDtLQXZCRyxFQXdCSmdCLEVBeEJJLENBQVA7OztBQTJCSixJQUFJdUYsZUFBZSxFQUFuQjs7QUFFQSxBQUFPLFNBQVNDLGVBQVQsQ0FBeUJ0UyxJQUF6QixFQUErQndSLEdBQS9CLEVBQW9DO1FBQ25DNUYsTUFBTWlHLGtCQUFOLElBQTRCLElBQWhDLEVBQXNDO2NBQzVCQSxrQkFBTixDQUF5QjdSLElBQXpCLEVBQStCd1IsR0FBL0I7O1FBRUEsQ0FBQ2EsYUFBYXJTLElBQWIsQ0FBTCxFQUF5QjthQUNoQndSLEdBQUw7cUJBQ2F4UixJQUFiLElBQXFCLElBQXJCOzs7O0FBSVI0TCxNQUFNNkYsMkJBQU4sR0FBb0MsS0FBcEM7QUFDQTdGLE1BQU1pRyxrQkFBTixHQUEyQixJQUEzQjs7QUN0RGUsU0FBU1UsVUFBVCxDQUFvQnJHLEtBQXBCLEVBQTJCO1dBQy9CQSxpQkFBaUJzRyxRQUFqQixJQUE2QjVjLE9BQU93VyxTQUFQLENBQWlCQyxRQUFqQixDQUEwQnRFLElBQTFCLENBQStCbUUsS0FBL0IsTUFBMEMsbUJBQTlFOzs7QUNJRyxTQUFTdUcsS0FBVCxDQUFjalUsTUFBZCxFQUFzQjtRQUNyQm9SLElBQUosRUFBVTNWLENBQVY7U0FDS0EsQ0FBTCxJQUFVdUUsTUFBVixFQUFrQjtlQUNQQSxPQUFPdkUsQ0FBUCxDQUFQO1lBQ0lzWSxXQUFXM0MsSUFBWCxDQUFKLEVBQXNCO2lCQUNiM1YsQ0FBTCxJQUFVMlYsSUFBVjtTQURKLE1BRU87aUJBQ0UsTUFBTTNWLENBQVgsSUFBZ0IyVixJQUFoQjs7O1NBR0g4QyxPQUFMLEdBQWVsVSxNQUFmOzs7U0FHS21VLG9CQUFMLEdBQTRCLElBQUlDLE1BQUosQ0FBVyxLQUFLQyxhQUFMLENBQW1CdlYsTUFBbkIsR0FBNEIsR0FBNUIsR0FBbUMsU0FBRCxDQUFZQSxNQUF6RCxDQUE1Qjs7O0FBR0osQUFBTyxTQUFTd1YsWUFBVCxDQUFzQkMsWUFBdEIsRUFBb0NDLFdBQXBDLEVBQWlEO1FBQ2hEakcsTUFBTUUsT0FBTyxFQUFQLEVBQVc4RixZQUFYLENBQVY7UUFBb0NuRCxJQUFwQztTQUNLQSxJQUFMLElBQWFvRCxXQUFiLEVBQTBCO1lBQ2xCaEcsV0FBV2dHLFdBQVgsRUFBd0JwRCxJQUF4QixDQUFKLEVBQW1DO2dCQUMzQnRELFNBQVN5RyxhQUFhbkQsSUFBYixDQUFULEtBQWdDdEQsU0FBUzBHLFlBQVlwRCxJQUFaLENBQVQsQ0FBcEMsRUFBaUU7b0JBQ3pEQSxJQUFKLElBQVksRUFBWjt1QkFDTzdDLElBQUk2QyxJQUFKLENBQVAsRUFBa0JtRCxhQUFhbkQsSUFBYixDQUFsQjt1QkFDTzdDLElBQUk2QyxJQUFKLENBQVAsRUFBa0JvRCxZQUFZcEQsSUFBWixDQUFsQjthQUhKLE1BSU8sSUFBSW9ELFlBQVlwRCxJQUFaLEtBQXFCLElBQXpCLEVBQStCO29CQUM5QkEsSUFBSixJQUFZb0QsWUFBWXBELElBQVosQ0FBWjthQURHLE1BRUE7dUJBQ0k3QyxJQUFJNkMsSUFBSixDQUFQOzs7O1NBSVBBLElBQUwsSUFBYW1ELFlBQWIsRUFBMkI7WUFDbkIvRixXQUFXK0YsWUFBWCxFQUF5Qm5ELElBQXpCLEtBQ0ksQ0FBQzVDLFdBQVdnRyxXQUFYLEVBQXdCcEQsSUFBeEIsQ0FETCxJQUVJdEQsU0FBU3lHLGFBQWFuRCxJQUFiLENBQVQsQ0FGUixFQUVzQzs7Z0JBRTlCQSxJQUFKLElBQVkzQyxPQUFPLEVBQVAsRUFBV0YsSUFBSTZDLElBQUosQ0FBWCxDQUFaOzs7V0FHRDdDLEdBQVA7OztBQzVDRyxTQUFTa0csTUFBVCxDQUFnQnpVLE1BQWhCLEVBQXdCO1FBQ3ZCQSxVQUFVLElBQWQsRUFBb0I7YUFDWGlVLEdBQUwsQ0FBU2pVLE1BQVQ7Ozs7QUNBUixJQUFJM0ksSUFBSjs7QUFFQSxJQUFJRCxPQUFPQyxJQUFYLEVBQWlCO1dBQ05ELE9BQU9DLElBQWQ7Q0FESixNQUVPO1dBQ0ksY0FBVW9TLEdBQVYsRUFBZTtZQUNkaE8sQ0FBSjtZQUFPOFMsTUFBTSxFQUFiO2FBQ0s5UyxDQUFMLElBQVVnTyxHQUFWLEVBQWU7Z0JBQ1ArRSxXQUFXL0UsR0FBWCxFQUFnQmhPLENBQWhCLENBQUosRUFBd0I7b0JBQ2hCakQsSUFBSixDQUFTaUQsQ0FBVDs7O2VBR0Q4UyxHQUFQO0tBUEo7Q0FXSjs7QUNsQk8sSUFBSW1HLGtCQUFrQjthQUNmLGVBRGU7YUFFZixrQkFGZTtjQUdkLGNBSGM7YUFJZixtQkFKZTtjQUtkLHFCQUxjO2NBTWQ7Q0FOUjs7QUFTUCxBQUVBLEFBQU8sU0FBU0MsUUFBVCxDQUFtQm5ZLEdBQW5CLEVBQXdCb1ksR0FBeEIsRUFBNkJDLEdBQTdCLEVBQWtDO1FBQ2pDQyxTQUFTLEtBQUtDLFNBQUwsQ0FBZXZZLEdBQWYsS0FBdUIsS0FBS3VZLFNBQUwsQ0FBZSxVQUFmLENBQXBDO1dBQ09oQixXQUFXZSxNQUFYLElBQXFCQSxPQUFPdkwsSUFBUCxDQUFZcUwsR0FBWixFQUFpQkMsR0FBakIsQ0FBckIsR0FBNkNDLE1BQXBEOzs7QUNiRyxJQUFJRSx3QkFBd0I7U0FDeEIsV0FEd0I7UUFFeEIsUUFGd0I7T0FHeEIsWUFId0I7UUFJeEIsY0FKd0I7U0FLeEIscUJBTHdCO1VBTXhCO0NBTko7O0FBU1AsQUFBTyxTQUFTQyxjQUFULENBQXlCelksR0FBekIsRUFBOEI7UUFDN0JvUyxTQUFTLEtBQUtzRyxlQUFMLENBQXFCMVksR0FBckIsQ0FBYjtRQUNJMlksY0FBYyxLQUFLRCxlQUFMLENBQXFCMVksSUFBSTRZLFdBQUosRUFBckIsQ0FEbEI7O1FBR0l4RyxVQUFVLENBQUN1RyxXQUFmLEVBQTRCO2VBQ2pCdkcsTUFBUDs7O1NBR0NzRyxlQUFMLENBQXFCMVksR0FBckIsSUFBNEIyWSxZQUFZRSxPQUFaLENBQW9CLGtCQUFwQixFQUF3QyxVQUFVaEUsR0FBVixFQUFlO2VBQ3hFQSxJQUFJb0MsS0FBSixDQUFVLENBQVYsQ0FBUDtLQUR3QixDQUE1Qjs7V0FJTyxLQUFLeUIsZUFBTCxDQUFxQjFZLEdBQXJCLENBQVA7OztBQ3JCRyxJQUFJOFkscUJBQXFCLGNBQXpCOztBQUVQLEFBQU8sU0FBU0MsV0FBVCxHQUF3QjtXQUNwQixLQUFLQyxZQUFaOzs7QUNIRyxJQUFJQyxpQkFBaUIsSUFBckI7QUFDUCxBQUFPLElBQUlDLHNCQUFzQixTQUExQjs7QUFFUCxBQUFPLFNBQVNDLE9BQVQsQ0FBa0J4RCxNQUFsQixFQUEwQjtXQUN0QixLQUFLeUQsUUFBTCxDQUFjUCxPQUFkLENBQXNCLElBQXRCLEVBQTRCbEQsTUFBNUIsQ0FBUDs7O0FDSkcsSUFBSTBELHNCQUFzQjtZQUNwQixPQURvQjtVQUVwQixRQUZvQjtPQUd4QixlQUh3QjtPQUl4QixVQUp3QjtRQUt4QixZQUx3QjtPQU14QixTQU53QjtRQU94QixVQVB3QjtPQVF4QixPQVJ3QjtRQVN4QixTQVR3QjtPQVV4QixTQVZ3QjtRQVd4QixXQVh3QjtPQVl4QixRQVp3QjtRQWF4QjtDQWJGOztBQWdCUCxBQUVBLEFBQU8sU0FBU0MsWUFBVCxDQUF1QjNELE1BQXZCLEVBQStCNEQsYUFBL0IsRUFBOENDLE1BQTlDLEVBQXNEQyxRQUF0RCxFQUFnRTtRQUMvRG5CLFNBQVMsS0FBS29CLGFBQUwsQ0FBbUJGLE1BQW5CLENBQWI7V0FDUWpDLFdBQVdlLE1BQVgsQ0FBRCxHQUNIQSxPQUFPM0MsTUFBUCxFQUFlNEQsYUFBZixFQUE4QkMsTUFBOUIsRUFBc0NDLFFBQXRDLENBREcsR0FFSG5CLE9BQU9PLE9BQVAsQ0FBZSxLQUFmLEVBQXNCbEQsTUFBdEIsQ0FGSjs7O0FBS0osQUFBTyxTQUFTZ0UsVUFBVCxDQUFxQkMsSUFBckIsRUFBMkJ0QixNQUEzQixFQUFtQztRQUNsQ2xHLFNBQVMsS0FBS3NILGFBQUwsQ0FBbUJFLE9BQU8sQ0FBUCxHQUFXLFFBQVgsR0FBc0IsTUFBekMsQ0FBYjtXQUNPckMsV0FBV25GLE1BQVgsSUFBcUJBLE9BQU9rRyxNQUFQLENBQXJCLEdBQXNDbEcsT0FBT3lHLE9BQVAsQ0FBZSxLQUFmLEVBQXNCUCxNQUF0QixDQUE3Qzs7O0FDekJKLElBQUl1QixVQUFVLEVBQWQ7O0FBRUEsQUFBTyxTQUFTQyxZQUFULENBQXVCQyxJQUF2QixFQUE2QkMsU0FBN0IsRUFBd0M7UUFDdkNDLFlBQVlGLEtBQUtHLFdBQUwsRUFBaEI7WUFDUUQsU0FBUixJQUFxQkosUUFBUUksWUFBWSxHQUFwQixJQUEyQkosUUFBUUcsU0FBUixJQUFxQkQsSUFBckU7OztBQUdKLEFBQU8sU0FBU0ksY0FBVCxDQUF3QkMsS0FBeEIsRUFBK0I7V0FDM0IsT0FBT0EsS0FBUCxLQUFpQixRQUFqQixHQUE0QlAsUUFBUU8sS0FBUixLQUFrQlAsUUFBUU8sTUFBTUYsV0FBTixFQUFSLENBQTlDLEdBQTZFOUYsU0FBcEY7OztBQUdKLEFBQU8sU0FBU2lHLG9CQUFULENBQThCQyxXQUE5QixFQUEyQztRQUMxQ0Msa0JBQWtCLEVBQXRCO1FBQ0lDLGNBREo7UUFFSTVGLElBRko7O1NBSUtBLElBQUwsSUFBYTBGLFdBQWIsRUFBMEI7WUFDbEJ0SSxXQUFXc0ksV0FBWCxFQUF3QjFGLElBQXhCLENBQUosRUFBbUM7NkJBQ2R1RixlQUFldkYsSUFBZixDQUFqQjtnQkFDSTRGLGNBQUosRUFBb0I7Z0NBQ0FBLGNBQWhCLElBQWtDRixZQUFZMUYsSUFBWixDQUFsQzs7Ozs7V0FLTDJGLGVBQVA7OztBQzNCSixJQUFJRSxhQUFhLEVBQWpCOztBQUVBLEFBQU8sU0FBU0MsZUFBVCxDQUF5QlgsSUFBekIsRUFBK0JZLFFBQS9CLEVBQXlDO2VBQ2pDWixJQUFYLElBQW1CWSxRQUFuQjs7O0FBR0osQUFBTyxTQUFTQyxtQkFBVCxDQUE2QkMsUUFBN0IsRUFBdUM7UUFDdENULFFBQVEsRUFBWjtTQUNLLElBQUlVLENBQVQsSUFBY0QsUUFBZCxFQUF3QjtjQUNkN2UsSUFBTixDQUFXLEVBQUMrZCxNQUFNZSxDQUFQLEVBQVVILFVBQVVGLFdBQVdLLENBQVgsQ0FBcEIsRUFBWDs7VUFFRUMsSUFBTixDQUFXLFVBQVUzWSxDQUFWLEVBQWFDLENBQWIsRUFBZ0I7ZUFDaEJELEVBQUV1WSxRQUFGLEdBQWF0WSxFQUFFc1ksUUFBdEI7S0FESjtXQUdPUCxLQUFQOzs7QUNSRyxTQUFTWSxVQUFULENBQXFCakIsSUFBckIsRUFBMkJrQixRQUEzQixFQUFxQztXQUNqQyxVQUFVaGIsS0FBVixFQUFpQjtZQUNoQkEsU0FBUyxJQUFiLEVBQW1CO2tCQUNYLElBQUosRUFBVThaLElBQVYsRUFBZ0I5WixLQUFoQjtrQkFDTXVWLFlBQU4sQ0FBbUIsSUFBbkIsRUFBeUJ5RixRQUF6QjttQkFDTyxJQUFQO1NBSEosTUFJTzttQkFDSUMsTUFBSSxJQUFKLEVBQVVuQixJQUFWLENBQVA7O0tBTlI7OztBQVdKLEFBQU8sU0FBU21CLEtBQVQsQ0FBYzlDLEdBQWQsRUFBbUIyQixJQUFuQixFQUF5QjtXQUNyQjNCLElBQUlwRixPQUFKLEtBQ0hvRixJQUFJOUUsRUFBSixDQUFPLFNBQVM4RSxJQUFJakQsTUFBSixHQUFhLEtBQWIsR0FBcUIsRUFBOUIsSUFBb0M0RSxJQUEzQyxHQURHLEdBQ2tEeEYsR0FEekQ7OztBQUlKLEFBQU8sU0FBU2tELEtBQVQsQ0FBY1csR0FBZCxFQUFtQjJCLElBQW5CLEVBQXlCOVosS0FBekIsRUFBZ0M7UUFDL0JtWSxJQUFJcEYsT0FBSixFQUFKLEVBQW1CO1lBQ1hNLEVBQUosQ0FBTyxTQUFTOEUsSUFBSWpELE1BQUosR0FBYSxLQUFiLEdBQXFCLEVBQTlCLElBQW9DNEUsSUFBM0MsRUFBaUQ5WixLQUFqRDs7Ozs7O0FBTVIsQUFBTyxTQUFTa2IsU0FBVCxDQUFvQmYsS0FBcEIsRUFBMkI7WUFDdEJELGVBQWVDLEtBQWYsQ0FBUjtRQUNJN0MsV0FBVyxLQUFLNkMsS0FBTCxDQUFYLENBQUosRUFBNkI7ZUFDbEIsS0FBS0EsS0FBTCxHQUFQOztXQUVHLElBQVA7OztBQUlKLEFBQU8sU0FBU2dCLFNBQVQsQ0FBb0JoQixLQUFwQixFQUEyQm5hLEtBQTNCLEVBQWtDO1FBQ2pDLFFBQU9tYSxLQUFQLHlDQUFPQSxLQUFQLE9BQWlCLFFBQXJCLEVBQStCO2dCQUNuQkMscUJBQXFCRCxLQUFyQixDQUFSO1lBQ0lpQixjQUFjVCxvQkFBb0JSLEtBQXBCLENBQWxCO2FBQ0ssSUFBSW5iLElBQUksQ0FBYixFQUFnQkEsSUFBSW9jLFlBQVl6YixNQUFoQyxFQUF3Q1gsR0FBeEMsRUFBNkM7aUJBQ3BDb2MsWUFBWXBjLENBQVosRUFBZThhLElBQXBCLEVBQTBCSyxNQUFNaUIsWUFBWXBjLENBQVosRUFBZThhLElBQXJCLENBQTFCOztLQUpSLE1BTU87Z0JBQ0tJLGVBQWVDLEtBQWYsQ0FBUjtZQUNJN0MsV0FBVyxLQUFLNkMsS0FBTCxDQUFYLENBQUosRUFBNkI7bUJBQ2xCLEtBQUtBLEtBQUwsRUFBWW5hLEtBQVosQ0FBUDs7O1dBR0QsSUFBUDs7O0FDckRXLFNBQVNxYixRQUFULENBQWtCM0YsTUFBbEIsRUFBMEI0RixZQUExQixFQUF3Q0MsU0FBeEMsRUFBbUQ7UUFDMURDLFlBQVksS0FBSy9iLEtBQUs2SixHQUFMLENBQVNvTSxNQUFULENBQXJCO1FBQ0krRixjQUFjSCxlQUFlRSxVQUFVN2IsTUFEM0M7UUFFSStiLE9BQU9oRyxVQUFVLENBRnJCO1dBR08sQ0FBQ2dHLE9BQVFILFlBQVksR0FBWixHQUFrQixFQUExQixHQUFnQyxHQUFqQyxJQUNIOWIsS0FBS2tjLEdBQUwsQ0FBUyxFQUFULEVBQWFsYyxLQUFLQyxHQUFMLENBQVMsQ0FBVCxFQUFZK2IsV0FBWixDQUFiLEVBQXVDckssUUFBdkMsR0FBa0RySCxNQUFsRCxDQUF5RCxDQUF6RCxDQURHLEdBQzJEeVIsU0FEbEU7OztBQ0ZHLElBQUlJLG1CQUFtQixzTEFBdkI7O0FBRVAsSUFBSUMsd0JBQXdCLDRDQUE1Qjs7QUFFQSxJQUFJQyxrQkFBa0IsRUFBdEI7O0FBRUEsQUFBTyxJQUFJQyx1QkFBdUIsRUFBM0I7Ozs7OztBQU1QLEFBQU8sU0FBU0MsY0FBVCxDQUF5QkMsS0FBekIsRUFBZ0NDLE1BQWhDLEVBQXdDaEQsT0FBeEMsRUFBaURuSSxRQUFqRCxFQUEyRDtRQUMxRDVQLE9BQU80UCxRQUFYO1FBQ0ksT0FBT0EsUUFBUCxLQUFvQixRQUF4QixFQUFrQztlQUN2QixnQkFBWTttQkFDUixLQUFLQSxRQUFMLEdBQVA7U0FESjs7UUFJQWtMLEtBQUosRUFBVzs2QkFDY0EsS0FBckIsSUFBOEI5YSxJQUE5Qjs7UUFFQSthLE1BQUosRUFBWTs2QkFDYUEsT0FBTyxDQUFQLENBQXJCLElBQWtDLFlBQVk7bUJBQ25DYixTQUFTbGEsS0FBS3lQLEtBQUwsQ0FBVyxJQUFYLEVBQWlCQyxTQUFqQixDQUFULEVBQXNDcUwsT0FBTyxDQUFQLENBQXRDLEVBQWlEQSxPQUFPLENBQVAsQ0FBakQsQ0FBUDtTQURKOztRQUlBaEQsT0FBSixFQUFhOzZCQUNZQSxPQUFyQixJQUFnQyxZQUFZO21CQUNqQyxLQUFLaUQsVUFBTCxHQUFrQmpELE9BQWxCLENBQTBCL1gsS0FBS3lQLEtBQUwsQ0FBVyxJQUFYLEVBQWlCQyxTQUFqQixDQUExQixFQUF1RG9MLEtBQXZELENBQVA7U0FESjs7OztBQU1SLFNBQVNHLHNCQUFULENBQWdDbkwsS0FBaEMsRUFBdUM7UUFDL0JBLE1BQU1vTCxLQUFOLENBQVksVUFBWixDQUFKLEVBQTZCO2VBQ2xCcEwsTUFBTTJILE9BQU4sQ0FBYyxVQUFkLEVBQTBCLEVBQTFCLENBQVA7O1dBRUczSCxNQUFNMkgsT0FBTixDQUFjLEtBQWQsRUFBcUIsRUFBckIsQ0FBUDs7O0FBR0osU0FBUzBELGtCQUFULENBQTRCbkssTUFBNUIsRUFBb0M7UUFDNUJvSyxRQUFRcEssT0FBT2tLLEtBQVAsQ0FBYVQsZ0JBQWIsQ0FBWjtRQUE0QzVjLENBQTVDO1FBQStDVyxNQUEvQzs7U0FFS1gsSUFBSSxDQUFKLEVBQU9XLFNBQVM0YyxNQUFNNWMsTUFBM0IsRUFBbUNYLElBQUlXLE1BQXZDLEVBQStDWCxHQUEvQyxFQUFvRDtZQUM1QytjLHFCQUFxQlEsTUFBTXZkLENBQU4sQ0FBckIsQ0FBSixFQUFvQztrQkFDMUJBLENBQU4sSUFBVytjLHFCQUFxQlEsTUFBTXZkLENBQU4sQ0FBckIsQ0FBWDtTQURKLE1BRU87a0JBQ0dBLENBQU4sSUFBV29kLHVCQUF1QkcsTUFBTXZkLENBQU4sQ0FBdkIsQ0FBWDs7OztXQUlELFVBQVVtWixHQUFWLEVBQWU7WUFDZEUsU0FBUyxFQUFiO1lBQWlCclosQ0FBakI7YUFDS0EsSUFBSSxDQUFULEVBQVlBLElBQUlXLE1BQWhCLEVBQXdCWCxHQUF4QixFQUE2QjtzQkFDZnVkLE1BQU12ZCxDQUFOLGFBQW9CdVksUUFBcEIsR0FBK0JnRixNQUFNdmQsQ0FBTixFQUFTOE4sSUFBVCxDQUFjcUwsR0FBZCxFQUFtQmhHLE1BQW5CLENBQS9CLEdBQTREb0ssTUFBTXZkLENBQU4sQ0FBdEU7O2VBRUdxWixNQUFQO0tBTEo7Ozs7QUFVSixBQUFPLFNBQVNtRSxZQUFULENBQXNCOUosQ0FBdEIsRUFBeUJQLE1BQXpCLEVBQWlDO1FBQ2hDLENBQUNPLEVBQUVLLE9BQUYsRUFBTCxFQUFrQjtlQUNQTCxFQUFFeUosVUFBRixHQUFlckQsV0FBZixFQUFQOzs7YUFHSzJELGFBQWF0SyxNQUFiLEVBQXFCTyxFQUFFeUosVUFBRixFQUFyQixDQUFUO29CQUNnQmhLLE1BQWhCLElBQTBCMkosZ0JBQWdCM0osTUFBaEIsS0FBMkJtSyxtQkFBbUJuSyxNQUFuQixDQUFyRDs7V0FFTzJKLGdCQUFnQjNKLE1BQWhCLEVBQXdCTyxDQUF4QixDQUFQOzs7QUFHSixBQUFPLFNBQVMrSixZQUFULENBQXNCdEssTUFBdEIsRUFBOEJDLE1BQTlCLEVBQXNDO1FBQ3JDcFQsSUFBSSxDQUFSOzthQUVTMGQsMkJBQVQsQ0FBcUN6TCxLQUFyQyxFQUE0QztlQUNqQ21CLE9BQU9vRyxjQUFQLENBQXNCdkgsS0FBdEIsS0FBZ0NBLEtBQXZDOzs7MEJBR2tCMEwsU0FBdEIsR0FBa0MsQ0FBbEM7V0FDTzNkLEtBQUssQ0FBTCxJQUFVNmMsc0JBQXNCZSxJQUF0QixDQUEyQnpLLE1BQTNCLENBQWpCLEVBQXFEO2lCQUN4Q0EsT0FBT3lHLE9BQVAsQ0FBZWlELHFCQUFmLEVBQXNDYSwyQkFBdEMsQ0FBVDs4QkFDc0JDLFNBQXRCLEdBQWtDLENBQWxDO2FBQ0ssQ0FBTDs7O1dBR0d4SyxNQUFQOzs7QUN6RkcsSUFBSTBLLFNBQWlCLElBQXJCO0FBQ1AsQUFBTyxJQUFJQyxTQUFpQixNQUFyQjtBQUNQLEFBQU8sSUFBSUMsU0FBaUIsT0FBckI7QUFDUCxBQUFPLElBQUlDLFNBQWlCLE9BQXJCO0FBQ1AsQUFBTyxJQUFJQyxTQUFpQixZQUFyQjtBQUNQLEFBQU8sSUFBSUMsWUFBaUIsT0FBckI7QUFDUCxBQUFPLElBQUlDLFlBQWlCLFdBQXJCO0FBQ1AsQUFBTyxJQUFJQyxZQUFpQixlQUFyQjtBQUNQLEFBQU8sSUFBSUMsWUFBaUIsU0FBckI7QUFDUCxBQUFPLElBQUlDLFlBQWlCLFNBQXJCO0FBQ1AsQUFBTyxJQUFJQyxZQUFpQixjQUFyQjs7QUFFUCxBQUFPLElBQUlDLGdCQUFpQixLQUFyQjtBQUNQLEFBQU8sSUFBSUMsY0FBaUIsVUFBckI7O0FBRVAsQUFBTyxJQUFJQyxjQUFpQixvQkFBckI7QUFDUCxBQUFPLElBQUlDLG1CQUFtQix5QkFBdkI7O0FBRVAsQUFBTyxJQUFJQyxpQkFBaUIsc0JBQXJCOzs7O0FBSVAsQUFBTyxJQUFJQyxZQUFZLGtIQUFoQjs7QUFHUCxBQUNBLEFBRUEsSUFBSUMsVUFBVSxFQUFkOztBQUVBLEFBQU8sU0FBU0MsYUFBVCxDQUF3QjlCLEtBQXhCLEVBQStCK0IsS0FBL0IsRUFBc0NDLFdBQXRDLEVBQW1EO1lBQzlDaEMsS0FBUixJQUFpQjNFLFdBQVcwRyxLQUFYLElBQW9CQSxLQUFwQixHQUE0QixVQUFVRSxRQUFWLEVBQW9CL0IsVUFBcEIsRUFBZ0M7ZUFDakUrQixZQUFZRCxXQUFiLEdBQTRCQSxXQUE1QixHQUEwQ0QsS0FBakQ7S0FESjs7O0FBS0osQUFBTyxTQUFTRyxxQkFBVCxDQUFnQ2xDLEtBQWhDLEVBQXVDMVksTUFBdkMsRUFBK0M7UUFDOUMsQ0FBQ3dPLFdBQVcrTCxPQUFYLEVBQW9CN0IsS0FBcEIsQ0FBTCxFQUFpQztlQUN0QixJQUFJdEUsTUFBSixDQUFXeUcsZUFBZW5DLEtBQWYsQ0FBWCxDQUFQOzs7V0FHRzZCLFFBQVE3QixLQUFSLEVBQWUxWSxPQUFPd1EsT0FBdEIsRUFBK0J4USxPQUFPNlIsT0FBdEMsQ0FBUDs7OztBQUlKLFNBQVNnSixjQUFULENBQXdCQyxDQUF4QixFQUEyQjtXQUNoQkMsWUFBWUQsRUFBRXpGLE9BQUYsQ0FBVSxJQUFWLEVBQWdCLEVBQWhCLEVBQW9CQSxPQUFwQixDQUE0QixxQ0FBNUIsRUFBbUUsVUFBVTJGLE9BQVYsRUFBbUJDLEVBQW5CLEVBQXVCQyxFQUF2QixFQUEyQkMsRUFBM0IsRUFBK0JDLEVBQS9CLEVBQW1DO2VBQzlHSCxNQUFNQyxFQUFOLElBQVlDLEVBQVosSUFBa0JDLEVBQXpCO0tBRGUsQ0FBWixDQUFQOzs7QUFLSixBQUFPLFNBQVNMLFdBQVQsQ0FBcUJELENBQXJCLEVBQXdCO1dBQ3BCQSxFQUFFekYsT0FBRixDQUFVLHdCQUFWLEVBQW9DLE1BQXBDLENBQVA7OztBQ2hESixJQUFJZ0csU0FBUyxFQUFiOztBQUVBLEFBQU8sU0FBU0MsYUFBVCxDQUF3QjVDLEtBQXhCLEVBQStCbEwsUUFBL0IsRUFBeUM7UUFDeEMvUixDQUFKO1FBQU9tQyxPQUFPNFAsUUFBZDtRQUNJLE9BQU9rTCxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO2dCQUNuQixDQUFDQSxLQUFELENBQVI7O1FBRUF6SyxTQUFTVCxRQUFULENBQUosRUFBd0I7ZUFDYixjQUFVRSxLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0I7a0JBQ3JCeEwsUUFBTixJQUFrQjZFLE1BQU0zRSxLQUFOLENBQWxCO1NBREo7O1NBSUNqUyxJQUFJLENBQVQsRUFBWUEsSUFBSWlkLE1BQU10YyxNQUF0QixFQUE4QlgsR0FBOUIsRUFBbUM7ZUFDeEJpZCxNQUFNamQsQ0FBTixDQUFQLElBQW1CbUMsSUFBbkI7Ozs7QUFJUixBQUFPLFNBQVMyZCxpQkFBVCxDQUE0QjdDLEtBQTVCLEVBQW1DbEwsUUFBbkMsRUFBNkM7a0JBQ2xDa0wsS0FBZCxFQUFxQixVQUFVaEwsS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCaFosTUFBeEIsRUFBZ0MwWSxLQUFoQyxFQUF1QztlQUNqRDhDLEVBQVAsR0FBWXhiLE9BQU93YixFQUFQLElBQWEsRUFBekI7aUJBQ1M5TixLQUFULEVBQWdCMU4sT0FBT3diLEVBQXZCLEVBQTJCeGIsTUFBM0IsRUFBbUMwWSxLQUFuQztLQUZKOzs7QUFNSixBQUFPLFNBQVMrQyx1QkFBVCxDQUFpQy9DLEtBQWpDLEVBQXdDaEwsS0FBeEMsRUFBK0MxTixNQUEvQyxFQUF1RDtRQUN0RDBOLFNBQVMsSUFBVCxJQUFpQmMsV0FBVzZNLE1BQVgsRUFBbUIzQyxLQUFuQixDQUFyQixFQUFnRDtlQUNyQ0EsS0FBUCxFQUFjaEwsS0FBZCxFQUFxQjFOLE9BQU8wYixFQUE1QixFQUFnQzFiLE1BQWhDLEVBQXdDMFksS0FBeEM7Ozs7QUM5QkQsSUFBSWlELE9BQU8sQ0FBWDtBQUNQLEFBQU8sSUFBSUMsUUFBUSxDQUFaO0FBQ1AsQUFBTyxJQUFJQyxPQUFPLENBQVg7QUFDUCxBQUFPLElBQUlDLE9BQU8sQ0FBWDtBQUNQLEFBQU8sSUFBSUMsU0FBUyxDQUFiO0FBQ1AsQUFBTyxJQUFJQyxTQUFTLENBQWI7QUFDUCxBQUFPLElBQUlDLGNBQWMsQ0FBbEI7QUFDUCxBQUFPLElBQUlDLE9BQU8sQ0FBWDtBQUNQLEFBQU8sSUFBSUMsVUFBVSxDQUFkOztBQ1JQLElBQUk1VixPQUFKOztBQUVBLElBQUlvSCxNQUFNQyxTQUFOLENBQWdCckgsT0FBcEIsRUFBNkI7Y0FDZm9ILE1BQU1DLFNBQU4sQ0FBZ0JySCxPQUExQjtDQURKLE1BRU87Y0FDTyxpQkFBVTZWLENBQVYsRUFBYTs7WUFFZjNnQixDQUFKO2FBQ0tBLElBQUksQ0FBVCxFQUFZQSxJQUFJLEtBQUtXLE1BQXJCLEVBQTZCLEVBQUVYLENBQS9CLEVBQWtDO2dCQUMxQixLQUFLQSxDQUFMLE1BQVkyZ0IsQ0FBaEIsRUFBbUI7dUJBQ1IzZ0IsQ0FBUDs7O2VBR0QsQ0FBQyxDQUFSO0tBUko7Q0FZSjs7QUNETyxTQUFTNGdCLFdBQVQsQ0FBcUJDLElBQXJCLEVBQTJCQyxLQUEzQixFQUFrQztXQUM5QixJQUFJcE8sSUFBSixDQUFTQSxLQUFLcU8sR0FBTCxDQUFTRixJQUFULEVBQWVDLFFBQVEsQ0FBdkIsRUFBMEIsQ0FBMUIsQ0FBVCxFQUF1Q0UsVUFBdkMsRUFBUDs7Ozs7QUFLSmhFLGVBQWUsR0FBZixFQUFvQixDQUFDLElBQUQsRUFBTyxDQUFQLENBQXBCLEVBQStCLElBQS9CLEVBQXFDLFlBQVk7V0FDdEMsS0FBSzhELEtBQUwsS0FBZSxDQUF0QjtDQURKOztBQUlBOUQsZUFBZSxLQUFmLEVBQXNCLENBQXRCLEVBQXlCLENBQXpCLEVBQTRCLFVBQVU3SixNQUFWLEVBQWtCO1dBQ25DLEtBQUtnSyxVQUFMLEdBQWtCOEQsV0FBbEIsQ0FBOEIsSUFBOUIsRUFBb0M5TixNQUFwQyxDQUFQO0NBREo7O0FBSUE2SixlQUFlLE1BQWYsRUFBdUIsQ0FBdkIsRUFBMEIsQ0FBMUIsRUFBNkIsVUFBVTdKLE1BQVYsRUFBa0I7V0FDcEMsS0FBS2dLLFVBQUwsR0FBa0IrRCxNQUFsQixDQUF5QixJQUF6QixFQUErQi9OLE1BQS9CLENBQVA7Q0FESjs7OztBQU1BMEgsYUFBYSxPQUFiLEVBQXNCLEdBQXRCOzs7O0FBSUFZLGdCQUFnQixPQUFoQixFQUF5QixDQUF6Qjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQXNCYixTQUF0QjtBQUNBYSxjQUFjLElBQWQsRUFBc0JiLFNBQXRCLEVBQWlDSixNQUFqQztBQUNBaUIsY0FBYyxLQUFkLEVBQXNCLFVBQVVHLFFBQVYsRUFBb0I5TCxNQUFwQixFQUE0QjtXQUN2Q0EsT0FBTytOLGdCQUFQLENBQXdCakMsUUFBeEIsQ0FBUDtDQURKO0FBR0FILGNBQWMsTUFBZCxFQUFzQixVQUFVRyxRQUFWLEVBQW9COUwsTUFBcEIsRUFBNEI7V0FDdkNBLE9BQU9nTyxXQUFQLENBQW1CbEMsUUFBbkIsQ0FBUDtDQURKOztBQUlBVyxjQUFjLENBQUMsR0FBRCxFQUFNLElBQU4sQ0FBZCxFQUEyQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCO1VBQ3pDNEMsS0FBTixJQUFldkosTUFBTTNFLEtBQU4sSUFBZSxDQUE5QjtDQURKOztBQUlBNE4sY0FBYyxDQUFDLEtBQUQsRUFBUSxNQUFSLENBQWQsRUFBK0IsVUFBVTVOLEtBQVYsRUFBaUJzTCxLQUFqQixFQUF3QmhaLE1BQXhCLEVBQWdDMFksS0FBaEMsRUFBdUM7UUFDOUQ2RCxRQUFRdmMsT0FBTzZSLE9BQVAsQ0FBZWlMLFdBQWYsQ0FBMkJwUCxLQUEzQixFQUFrQ2dMLEtBQWxDLEVBQXlDMVksT0FBT3dRLE9BQWhELENBQVo7O1FBRUkrTCxTQUFTLElBQWIsRUFBbUI7Y0FDVFgsS0FBTixJQUFlVyxLQUFmO0tBREosTUFFTzt3QkFDYXZjLE1BQWhCLEVBQXdCa1EsWUFBeEIsR0FBdUN4QyxLQUF2Qzs7Q0FOUjs7OztBQVlBLElBQUlxUCxtQkFBbUIsK0JBQXZCO0FBQ0EsQUFBTyxJQUFJQyxzQkFBc0Isd0ZBQXdGalcsS0FBeEYsQ0FBOEYsR0FBOUYsQ0FBMUI7QUFDUCxBQUFPLFNBQVNrVyxZQUFULENBQXVCOU4sQ0FBdkIsRUFBMEJQLE1BQTFCLEVBQWtDO1FBQ2pDLENBQUNPLENBQUwsRUFBUTtlQUNHLEtBQUsrTixPQUFaOztXQUVHelAsUUFBUSxLQUFLeVAsT0FBYixJQUF3QixLQUFLQSxPQUFMLENBQWEvTixFQUFFb04sS0FBRixFQUFiLENBQXhCLEdBQ0gsS0FBS1csT0FBTCxDQUFhLENBQUMsS0FBS0EsT0FBTCxDQUFhQyxRQUFiLElBQXlCSixnQkFBMUIsRUFBNEMxRCxJQUE1QyxDQUFpRHpLLE1BQWpELElBQTJELFFBQTNELEdBQXNFLFlBQW5GLEVBQWlHTyxFQUFFb04sS0FBRixFQUFqRyxDQURKOzs7QUFJSixBQUFPLElBQUlhLDJCQUEyQixrREFBa0RyVyxLQUFsRCxDQUF3RCxHQUF4RCxDQUEvQjtBQUNQLEFBQU8sU0FBU3NXLGlCQUFULENBQTRCbE8sQ0FBNUIsRUFBK0JQLE1BQS9CLEVBQXVDO1FBQ3RDLENBQUNPLENBQUwsRUFBUTtlQUNHLEtBQUttTyxZQUFaOztXQUVHN1AsUUFBUSxLQUFLNlAsWUFBYixJQUE2QixLQUFLQSxZQUFMLENBQWtCbk8sRUFBRW9OLEtBQUYsRUFBbEIsQ0FBN0IsR0FDSCxLQUFLZSxZQUFMLENBQWtCUCxpQkFBaUIxRCxJQUFqQixDQUFzQnpLLE1BQXRCLElBQWdDLFFBQWhDLEdBQTJDLFlBQTdELEVBQTJFTyxFQUFFb04sS0FBRixFQUEzRSxDQURKOzs7QUFJSixTQUFTZ0IsaUJBQVQsQ0FBMkJDLFNBQTNCLEVBQXNDNU8sTUFBdEMsRUFBOENFLE1BQTlDLEVBQXNEO1FBQzlDclQsQ0FBSjtRQUFPZ2lCLEVBQVA7UUFBVzdJLEdBQVg7UUFBZ0I4SSxNQUFNRixVQUFVRyxpQkFBVixFQUF0QjtRQUNJLENBQUMsS0FBS0MsWUFBVixFQUF3Qjs7YUFFZkEsWUFBTCxHQUFvQixFQUFwQjthQUNLQyxnQkFBTCxHQUF3QixFQUF4QjthQUNLQyxpQkFBTCxHQUF5QixFQUF6QjthQUNLcmlCLElBQUksQ0FBVCxFQUFZQSxJQUFJLEVBQWhCLEVBQW9CLEVBQUVBLENBQXRCLEVBQXlCO2tCQUNma1QsVUFBVSxDQUFDLElBQUQsRUFBT2xULENBQVAsQ0FBVixDQUFOO2lCQUNLcWlCLGlCQUFMLENBQXVCcmlCLENBQXZCLElBQTRCLEtBQUtpaEIsV0FBTCxDQUFpQjlILEdBQWpCLEVBQXNCLEVBQXRCLEVBQTBCK0ksaUJBQTFCLEVBQTVCO2lCQUNLRSxnQkFBTCxDQUFzQnBpQixDQUF0QixJQUEyQixLQUFLa2hCLE1BQUwsQ0FBWS9ILEdBQVosRUFBaUIsRUFBakIsRUFBcUIrSSxpQkFBckIsRUFBM0I7Ozs7UUFJSjdPLE1BQUosRUFBWTtZQUNKRixXQUFXLEtBQWYsRUFBc0I7aUJBQ2JySSxRQUFRZ0QsSUFBUixDQUFhLEtBQUt1VSxpQkFBbEIsRUFBcUNKLEdBQXJDLENBQUw7bUJBQ09ELE9BQU8sQ0FBQyxDQUFSLEdBQVlBLEVBQVosR0FBaUIsSUFBeEI7U0FGSixNQUdPO2lCQUNFbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLc1UsZ0JBQWxCLEVBQW9DSCxHQUFwQyxDQUFMO21CQUNPRCxPQUFPLENBQUMsQ0FBUixHQUFZQSxFQUFaLEdBQWlCLElBQXhCOztLQU5SLE1BUU87WUFDQzdPLFdBQVcsS0FBZixFQUFzQjtpQkFDYnJJLFFBQVFnRCxJQUFSLENBQWEsS0FBS3VVLGlCQUFsQixFQUFxQ0osR0FBckMsQ0FBTDtnQkFDSUQsT0FBTyxDQUFDLENBQVosRUFBZTt1QkFDSkEsRUFBUDs7aUJBRUNsWCxRQUFRZ0QsSUFBUixDQUFhLEtBQUtzVSxnQkFBbEIsRUFBb0NILEdBQXBDLENBQUw7bUJBQ09ELE9BQU8sQ0FBQyxDQUFSLEdBQVlBLEVBQVosR0FBaUIsSUFBeEI7U0FOSixNQU9PO2lCQUNFbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLc1UsZ0JBQWxCLEVBQW9DSCxHQUFwQyxDQUFMO2dCQUNJRCxPQUFPLENBQUMsQ0FBWixFQUFlO3VCQUNKQSxFQUFQOztpQkFFQ2xYLFFBQVFnRCxJQUFSLENBQWEsS0FBS3VVLGlCQUFsQixFQUFxQ0osR0FBckMsQ0FBTDttQkFDT0QsT0FBTyxDQUFDLENBQVIsR0FBWUEsRUFBWixHQUFpQixJQUF4Qjs7Ozs7QUFLWixBQUFPLFNBQVNNLGlCQUFULENBQTRCUCxTQUE1QixFQUF1QzVPLE1BQXZDLEVBQStDRSxNQUEvQyxFQUF1RDtRQUN0RHJULENBQUosRUFBT21aLEdBQVAsRUFBWTZGLEtBQVo7O1FBRUksS0FBS3VELGlCQUFULEVBQTRCO2VBQ2pCVCxrQkFBa0JoVSxJQUFsQixDQUF1QixJQUF2QixFQUE2QmlVLFNBQTdCLEVBQXdDNU8sTUFBeEMsRUFBZ0RFLE1BQWhELENBQVA7OztRQUdBLENBQUMsS0FBSzhPLFlBQVYsRUFBd0I7YUFDZkEsWUFBTCxHQUFvQixFQUFwQjthQUNLQyxnQkFBTCxHQUF3QixFQUF4QjthQUNLQyxpQkFBTCxHQUF5QixFQUF6Qjs7Ozs7O1NBTUNyaUIsSUFBSSxDQUFULEVBQVlBLElBQUksRUFBaEIsRUFBb0JBLEdBQXBCLEVBQXlCOztjQUVma1QsVUFBVSxDQUFDLElBQUQsRUFBT2xULENBQVAsQ0FBVixDQUFOO1lBQ0lxVCxVQUFVLENBQUMsS0FBSytPLGdCQUFMLENBQXNCcGlCLENBQXRCLENBQWYsRUFBeUM7aUJBQ2hDb2lCLGdCQUFMLENBQXNCcGlCLENBQXRCLElBQTJCLElBQUkyWSxNQUFKLENBQVcsTUFBTSxLQUFLdUksTUFBTCxDQUFZL0gsR0FBWixFQUFpQixFQUFqQixFQUFxQlMsT0FBckIsQ0FBNkIsR0FBN0IsRUFBa0MsRUFBbEMsQ0FBTixHQUE4QyxHQUF6RCxFQUE4RCxHQUE5RCxDQUEzQjtpQkFDS3lJLGlCQUFMLENBQXVCcmlCLENBQXZCLElBQTRCLElBQUkyWSxNQUFKLENBQVcsTUFBTSxLQUFLc0ksV0FBTCxDQUFpQjlILEdBQWpCLEVBQXNCLEVBQXRCLEVBQTBCUyxPQUExQixDQUFrQyxHQUFsQyxFQUF1QyxFQUF2QyxDQUFOLEdBQW1ELEdBQTlELEVBQW1FLEdBQW5FLENBQTVCOztZQUVBLENBQUN2RyxNQUFELElBQVcsQ0FBQyxLQUFLOE8sWUFBTCxDQUFrQm5pQixDQUFsQixDQUFoQixFQUFzQztvQkFDMUIsTUFBTSxLQUFLa2hCLE1BQUwsQ0FBWS9ILEdBQVosRUFBaUIsRUFBakIsQ0FBTixHQUE2QixJQUE3QixHQUFvQyxLQUFLOEgsV0FBTCxDQUFpQjlILEdBQWpCLEVBQXNCLEVBQXRCLENBQTVDO2lCQUNLZ0osWUFBTCxDQUFrQm5pQixDQUFsQixJQUF1QixJQUFJMlksTUFBSixDQUFXcUcsTUFBTXBGLE9BQU4sQ0FBYyxHQUFkLEVBQW1CLEVBQW5CLENBQVgsRUFBbUMsR0FBbkMsQ0FBdkI7OztZQUdBdkcsVUFBVUYsV0FBVyxNQUFyQixJQUErQixLQUFLaVAsZ0JBQUwsQ0FBc0JwaUIsQ0FBdEIsRUFBeUI0ZCxJQUF6QixDQUE4Qm1FLFNBQTlCLENBQW5DLEVBQTZFO21CQUNsRS9oQixDQUFQO1NBREosTUFFTyxJQUFJcVQsVUFBVUYsV0FBVyxLQUFyQixJQUE4QixLQUFLa1AsaUJBQUwsQ0FBdUJyaUIsQ0FBdkIsRUFBMEI0ZCxJQUExQixDQUErQm1FLFNBQS9CLENBQWxDLEVBQTZFO21CQUN6RS9oQixDQUFQO1NBREcsTUFFQSxJQUFJLENBQUNxVCxNQUFELElBQVcsS0FBSzhPLFlBQUwsQ0FBa0JuaUIsQ0FBbEIsRUFBcUI0ZCxJQUFyQixDQUEwQm1FLFNBQTFCLENBQWYsRUFBcUQ7bUJBQ2pEL2hCLENBQVA7Ozs7Ozs7QUFPWixBQUFPLFNBQVN3aUIsUUFBVCxDQUFtQnJKLEdBQW5CLEVBQXdCblksS0FBeEIsRUFBK0I7UUFDOUJ5aEIsVUFBSjs7UUFFSSxDQUFDdEosSUFBSXBGLE9BQUosRUFBTCxFQUFvQjs7ZUFFVG9GLEdBQVA7OztRQUdBLE9BQU9uWSxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO1lBQ3ZCLFFBQVE0YyxJQUFSLENBQWE1YyxLQUFiLENBQUosRUFBeUI7b0JBQ2I0VixNQUFNNVYsS0FBTixDQUFSO1NBREosTUFFTztvQkFDS21ZLElBQUlnRSxVQUFKLEdBQWlCa0UsV0FBakIsQ0FBNkJyZ0IsS0FBN0IsQ0FBUjs7Z0JBRUksQ0FBQ3dSLFNBQVN4UixLQUFULENBQUwsRUFBc0I7dUJBQ1htWSxHQUFQOzs7OztpQkFLQzFZLEtBQUtrSCxHQUFMLENBQVN3UixJQUFJdUosSUFBSixFQUFULEVBQXFCOUIsWUFBWXpILElBQUkwSCxJQUFKLEVBQVosRUFBd0I3ZixLQUF4QixDQUFyQixDQUFiO1FBQ0lxVCxFQUFKLENBQU8sU0FBUzhFLElBQUlqRCxNQUFKLEdBQWEsS0FBYixHQUFxQixFQUE5QixJQUFvQyxPQUEzQyxFQUFvRGxWLEtBQXBELEVBQTJEeWhCLFVBQTNEO1dBQ090SixHQUFQOzs7QUFHSixBQUFPLFNBQVN3SixXQUFULENBQXNCM2hCLEtBQXRCLEVBQTZCO1FBQzVCQSxTQUFTLElBQWIsRUFBbUI7aUJBQ04sSUFBVCxFQUFlQSxLQUFmO2NBQ011VixZQUFOLENBQW1CLElBQW5CLEVBQXlCLElBQXpCO2VBQ08sSUFBUDtLQUhKLE1BSU87ZUFDSTBGLE1BQUksSUFBSixFQUFVLE9BQVYsQ0FBUDs7OztBQUlSLEFBQU8sU0FBUzJHLGNBQVQsR0FBMkI7V0FDdkJoQyxZQUFZLEtBQUtDLElBQUwsRUFBWixFQUF5QixLQUFLQyxLQUFMLEVBQXpCLENBQVA7OztBQUdKLElBQUkrQiwwQkFBMEJoRSxTQUE5QjtBQUNBLEFBQU8sU0FBU3NDLGdCQUFULENBQTJCakMsUUFBM0IsRUFBcUM7UUFDcEMsS0FBS3FELGlCQUFULEVBQTRCO1lBQ3BCLENBQUN4UCxXQUFXLElBQVgsRUFBaUIsY0FBakIsQ0FBTCxFQUF1QzsrQkFDaEJqRixJQUFuQixDQUF3QixJQUF4Qjs7WUFFQW9SLFFBQUosRUFBYzttQkFDSCxLQUFLNEQsdUJBQVo7U0FESixNQUVPO21CQUNJLEtBQUtDLGlCQUFaOztLQVBSLE1BU087WUFDQyxDQUFDaFEsV0FBVyxJQUFYLEVBQWlCLG1CQUFqQixDQUFMLEVBQTRDO2lCQUNuQ2dRLGlCQUFMLEdBQXlCRix1QkFBekI7O2VBRUcsS0FBS0MsdUJBQUwsSUFBZ0M1RCxRQUFoQyxHQUNILEtBQUs0RCx1QkFERixHQUM0QixLQUFLQyxpQkFEeEM7Ozs7QUFLUixJQUFJQyxxQkFBcUJuRSxTQUF6QjtBQUNBLEFBQU8sU0FBU3VDLFdBQVQsQ0FBc0JsQyxRQUF0QixFQUFnQztRQUMvQixLQUFLcUQsaUJBQVQsRUFBNEI7WUFDcEIsQ0FBQ3hQLFdBQVcsSUFBWCxFQUFpQixjQUFqQixDQUFMLEVBQXVDOytCQUNoQmpGLElBQW5CLENBQXdCLElBQXhCOztZQUVBb1IsUUFBSixFQUFjO21CQUNILEtBQUsrRCxrQkFBWjtTQURKLE1BRU87bUJBQ0ksS0FBS0MsWUFBWjs7S0FQUixNQVNPO1lBQ0MsQ0FBQ25RLFdBQVcsSUFBWCxFQUFpQixjQUFqQixDQUFMLEVBQXVDO2lCQUM5Qm1RLFlBQUwsR0FBb0JGLGtCQUFwQjs7ZUFFRyxLQUFLQyxrQkFBTCxJQUEyQi9ELFFBQTNCLEdBQ0gsS0FBSytELGtCQURGLEdBQ3VCLEtBQUtDLFlBRG5DOzs7O0FBS1IsU0FBU0Msa0JBQVQsR0FBK0I7YUFDbEJDLFNBQVQsQ0FBbUJqZ0IsQ0FBbkIsRUFBc0JDLENBQXRCLEVBQXlCO2VBQ2RBLEVBQUV6QyxNQUFGLEdBQVd3QyxFQUFFeEMsTUFBcEI7OztRQUdBMGlCLGNBQWMsRUFBbEI7UUFBc0JDLGFBQWEsRUFBbkM7UUFBdUNDLGNBQWMsRUFBckQ7UUFDSXZqQixDQURKO1FBQ09tWixHQURQO1NBRUtuWixJQUFJLENBQVQsRUFBWUEsSUFBSSxFQUFoQixFQUFvQkEsR0FBcEIsRUFBeUI7O2NBRWZrVCxVQUFVLENBQUMsSUFBRCxFQUFPbFQsQ0FBUCxDQUFWLENBQU47b0JBQ1lqRCxJQUFaLENBQWlCLEtBQUtra0IsV0FBTCxDQUFpQjlILEdBQWpCLEVBQXNCLEVBQXRCLENBQWpCO21CQUNXcGMsSUFBWCxDQUFnQixLQUFLbWtCLE1BQUwsQ0FBWS9ILEdBQVosRUFBaUIsRUFBakIsQ0FBaEI7b0JBQ1lwYyxJQUFaLENBQWlCLEtBQUtta0IsTUFBTCxDQUFZL0gsR0FBWixFQUFpQixFQUFqQixDQUFqQjtvQkFDWXBjLElBQVosQ0FBaUIsS0FBS2trQixXQUFMLENBQWlCOUgsR0FBakIsRUFBc0IsRUFBdEIsQ0FBakI7Ozs7Z0JBSVEyQyxJQUFaLENBQWlCc0gsU0FBakI7ZUFDV3RILElBQVgsQ0FBZ0JzSCxTQUFoQjtnQkFDWXRILElBQVosQ0FBaUJzSCxTQUFqQjtTQUNLcGpCLElBQUksQ0FBVCxFQUFZQSxJQUFJLEVBQWhCLEVBQW9CQSxHQUFwQixFQUF5QjtvQkFDVEEsQ0FBWixJQUFpQnNmLFlBQVkrRCxZQUFZcmpCLENBQVosQ0FBWixDQUFqQjttQkFDV0EsQ0FBWCxJQUFnQnNmLFlBQVlnRSxXQUFXdGpCLENBQVgsQ0FBWixDQUFoQjs7U0FFQ0EsSUFBSSxDQUFULEVBQVlBLElBQUksRUFBaEIsRUFBb0JBLEdBQXBCLEVBQXlCO29CQUNUQSxDQUFaLElBQWlCc2YsWUFBWWlFLFlBQVl2akIsQ0FBWixDQUFaLENBQWpCOzs7U0FHQ2tqQixZQUFMLEdBQW9CLElBQUl2SyxNQUFKLENBQVcsT0FBTzRLLFlBQVl0TCxJQUFaLENBQWlCLEdBQWpCLENBQVAsR0FBK0IsR0FBMUMsRUFBK0MsR0FBL0MsQ0FBcEI7U0FDSzhLLGlCQUFMLEdBQXlCLEtBQUtHLFlBQTlCO1NBQ0tELGtCQUFMLEdBQTBCLElBQUl0SyxNQUFKLENBQVcsT0FBTzJLLFdBQVdyTCxJQUFYLENBQWdCLEdBQWhCLENBQVAsR0FBOEIsR0FBekMsRUFBOEMsR0FBOUMsQ0FBMUI7U0FDSzZLLHVCQUFMLEdBQStCLElBQUluSyxNQUFKLENBQVcsT0FBTzBLLFlBQVlwTCxJQUFaLENBQWlCLEdBQWpCLENBQVAsR0FBK0IsR0FBMUMsRUFBK0MsR0FBL0MsQ0FBL0I7OztBQzdRSjs7QUFFQStFLGVBQWUsR0FBZixFQUFvQixDQUFwQixFQUF1QixDQUF2QixFQUEwQixZQUFZO1FBQzlCN2QsSUFBSSxLQUFLMGhCLElBQUwsRUFBUjtXQUNPMWhCLEtBQUssSUFBTCxHQUFZLEtBQUtBLENBQWpCLEdBQXFCLE1BQU1BLENBQWxDO0NBRko7O0FBS0E2ZCxlQUFlLENBQWYsRUFBa0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFsQixFQUE2QixDQUE3QixFQUFnQyxZQUFZO1dBQ2pDLEtBQUs2RCxJQUFMLEtBQWMsR0FBckI7Q0FESjs7QUFJQTdELGVBQWUsQ0FBZixFQUFrQixDQUFDLE1BQUQsRUFBVyxDQUFYLENBQWxCLEVBQXVDLENBQXZDLEVBQTBDLE1BQTFDO0FBQ0FBLGVBQWUsQ0FBZixFQUFrQixDQUFDLE9BQUQsRUFBVyxDQUFYLENBQWxCLEVBQXVDLENBQXZDLEVBQTBDLE1BQTFDO0FBQ0FBLGVBQWUsQ0FBZixFQUFrQixDQUFDLFFBQUQsRUFBVyxDQUFYLEVBQWMsSUFBZCxDQUFsQixFQUF1QyxDQUF2QyxFQUEwQyxNQUExQzs7OztBQUlBbkMsYUFBYSxNQUFiLEVBQXFCLEdBQXJCOzs7O0FBSUFZLGdCQUFnQixNQUFoQixFQUF3QixDQUF4Qjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQXdCTixXQUF4QjtBQUNBTSxjQUFjLElBQWQsRUFBd0JiLFNBQXhCLEVBQW1DSixNQUFuQztBQUNBaUIsY0FBYyxNQUFkLEVBQXdCVCxTQUF4QixFQUFtQ04sTUFBbkM7QUFDQWUsY0FBYyxPQUFkLEVBQXdCUixTQUF4QixFQUFtQ04sTUFBbkM7QUFDQWMsY0FBYyxRQUFkLEVBQXdCUixTQUF4QixFQUFtQ04sTUFBbkM7O0FBRUE0QixjQUFjLENBQUMsT0FBRCxFQUFVLFFBQVYsQ0FBZCxFQUFtQ0ssSUFBbkM7QUFDQUwsY0FBYyxNQUFkLEVBQXNCLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0I7VUFDcEMyQyxJQUFOLElBQWNqTyxNQUFNdFIsTUFBTixLQUFpQixDQUFqQixHQUFxQmdSLE1BQU02UixpQkFBTixDQUF3QnZSLEtBQXhCLENBQXJCLEdBQXNEMkUsTUFBTTNFLEtBQU4sQ0FBcEU7Q0FESjtBQUdBNE4sY0FBYyxJQUFkLEVBQW9CLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0I7VUFDbEMyQyxJQUFOLElBQWN2TyxNQUFNNlIsaUJBQU4sQ0FBd0J2UixLQUF4QixDQUFkO0NBREo7QUFHQTROLGNBQWMsR0FBZCxFQUFtQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCO1VBQ2pDMkMsSUFBTixJQUFjdUQsU0FBU3hSLEtBQVQsRUFBZ0IsRUFBaEIsQ0FBZDtDQURKOzs7O0FBTUEsQUFBTyxTQUFTeVIsVUFBVCxDQUFvQjdDLElBQXBCLEVBQTBCO1dBQ3RCOEMsV0FBVzlDLElBQVgsSUFBbUIsR0FBbkIsR0FBeUIsR0FBaEM7OztBQUdKLFNBQVM4QyxVQUFULENBQW9COUMsSUFBcEIsRUFBMEI7V0FDZEEsT0FBTyxDQUFQLEtBQWEsQ0FBYixJQUFrQkEsT0FBTyxHQUFQLEtBQWUsQ0FBbEMsSUFBd0NBLE9BQU8sR0FBUCxLQUFlLENBQTlEOzs7OztBQUtKbFAsTUFBTTZSLGlCQUFOLEdBQTBCLFVBQVV2UixLQUFWLEVBQWlCO1dBQ2hDMkUsTUFBTTNFLEtBQU4sS0FBZ0IyRSxNQUFNM0UsS0FBTixJQUFlLEVBQWYsR0FBb0IsSUFBcEIsR0FBMkIsSUFBM0MsQ0FBUDtDQURKOzs7O0FBTUEsQUFBTyxJQUFJMlIsYUFBYTdILFdBQVcsVUFBWCxFQUF1QixJQUF2QixDQUFqQjs7QUFFUCxBQUFPLFNBQVM4SCxhQUFULEdBQTBCO1dBQ3RCRixXQUFXLEtBQUs5QyxJQUFMLEVBQVgsQ0FBUDs7O0FDekVHLFNBQVNpRCxVQUFULENBQXFCM2tCLENBQXJCLEVBQXdCdVUsQ0FBeEIsRUFBMkI3VixDQUEzQixFQUE4QjZKLENBQTlCLEVBQWlDcWMsQ0FBakMsRUFBb0MxRSxDQUFwQyxFQUF1QzJFLEVBQXZDLEVBQTJDOzs7UUFHMUN0QixPQUFPLElBQUloUSxJQUFKLENBQVN2VCxDQUFULEVBQVl1VSxDQUFaLEVBQWU3VixDQUFmLEVBQWtCNkosQ0FBbEIsRUFBcUJxYyxDQUFyQixFQUF3QjFFLENBQXhCLEVBQTJCMkUsRUFBM0IsQ0FBWDs7O1FBR0k3a0IsSUFBSSxHQUFKLElBQVdBLEtBQUssQ0FBaEIsSUFBcUI0WCxTQUFTMkwsS0FBS3VCLFdBQUwsRUFBVCxDQUF6QixFQUF1RDthQUM5Q0MsV0FBTCxDQUFpQi9rQixDQUFqQjs7V0FFR3VqQixJQUFQOzs7QUFHSixBQUFPLFNBQVN5QixhQUFULENBQXdCaGxCLENBQXhCLEVBQTJCO1FBQzFCdWpCLE9BQU8sSUFBSWhRLElBQUosQ0FBU0EsS0FBS3FPLEdBQUwsQ0FBU25QLEtBQVQsQ0FBZSxJQUFmLEVBQXFCQyxTQUFyQixDQUFULENBQVg7OztRQUdJMVMsSUFBSSxHQUFKLElBQVdBLEtBQUssQ0FBaEIsSUFBcUI0WCxTQUFTMkwsS0FBSzBCLGNBQUwsRUFBVCxDQUF6QixFQUEwRDthQUNqREMsY0FBTCxDQUFvQmxsQixDQUFwQjs7V0FFR3VqQixJQUFQOzs7QUNmSjtBQUNBLFNBQVM0QixlQUFULENBQXlCekQsSUFBekIsRUFBK0IwRCxHQUEvQixFQUFvQ0MsR0FBcEMsRUFBeUM7O1VBRTNCLElBQUlELEdBQUosR0FBVUMsR0FEcEI7OztZQUdZLENBQUMsSUFBSUwsY0FBY3RELElBQWQsRUFBb0IsQ0FBcEIsRUFBdUI0RCxHQUF2QixFQUE0QkMsU0FBNUIsRUFBSixHQUE4Q0gsR0FBL0MsSUFBc0QsQ0FIbEU7O1dBS08sQ0FBQ0ksS0FBRCxHQUFTRixHQUFULEdBQWUsQ0FBdEI7Ozs7QUFJSixBQUFPLFNBQVNHLGtCQUFULENBQTRCL0QsSUFBNUIsRUFBa0NnRSxJQUFsQyxFQUF3Q0MsT0FBeEMsRUFBaURQLEdBQWpELEVBQXNEQyxHQUF0RCxFQUEyRDtRQUMxRE8sZUFBZSxDQUFDLElBQUlELE9BQUosR0FBY1AsR0FBZixJQUFzQixDQUF6QztRQUNJUyxhQUFhVixnQkFBZ0J6RCxJQUFoQixFQUFzQjBELEdBQXRCLEVBQTJCQyxHQUEzQixDQURqQjtRQUVJUyxZQUFZLElBQUksS0FBS0osT0FBTyxDQUFaLENBQUosR0FBcUJFLFlBQXJCLEdBQW9DQyxVQUZwRDtRQUdJRSxPQUhKO1FBR2FDLFlBSGI7O1FBS0lGLGFBQWEsQ0FBakIsRUFBb0I7a0JBQ05wRSxPQUFPLENBQWpCO3VCQUNlNkMsV0FBV3dCLE9BQVgsSUFBc0JELFNBQXJDO0tBRkosTUFHTyxJQUFJQSxZQUFZdkIsV0FBVzdDLElBQVgsQ0FBaEIsRUFBa0M7a0JBQzNCQSxPQUFPLENBQWpCO3VCQUNlb0UsWUFBWXZCLFdBQVc3QyxJQUFYLENBQTNCO0tBRkcsTUFHQTtrQkFDT0EsSUFBVjt1QkFDZW9FLFNBQWY7OztXQUdHO2NBQ0dDLE9BREg7bUJBRVFDO0tBRmY7OztBQU1KLEFBQU8sU0FBU0MsVUFBVCxDQUFvQmpNLEdBQXBCLEVBQXlCb0wsR0FBekIsRUFBOEJDLEdBQTlCLEVBQW1DO1FBQ2xDUSxhQUFhVixnQkFBZ0JuTCxJQUFJMEgsSUFBSixFQUFoQixFQUE0QjBELEdBQTVCLEVBQWlDQyxHQUFqQyxDQUFqQjtRQUNJSyxPQUFPcGtCLEtBQUtrVyxLQUFMLENBQVcsQ0FBQ3dDLElBQUk4TCxTQUFKLEtBQWtCRCxVQUFsQixHQUErQixDQUFoQyxJQUFxQyxDQUFoRCxJQUFxRCxDQURoRTtRQUVJSyxPQUZKO1FBRWFILE9BRmI7O1FBSUlMLE9BQU8sQ0FBWCxFQUFjO2tCQUNBMUwsSUFBSTBILElBQUosS0FBYSxDQUF2QjtrQkFDVWdFLE9BQU9TLFlBQVlKLE9BQVosRUFBcUJYLEdBQXJCLEVBQTBCQyxHQUExQixDQUFqQjtLQUZKLE1BR08sSUFBSUssT0FBT1MsWUFBWW5NLElBQUkwSCxJQUFKLEVBQVosRUFBd0IwRCxHQUF4QixFQUE2QkMsR0FBN0IsQ0FBWCxFQUE4QztrQkFDdkNLLE9BQU9TLFlBQVluTSxJQUFJMEgsSUFBSixFQUFaLEVBQXdCMEQsR0FBeEIsRUFBNkJDLEdBQTdCLENBQWpCO2tCQUNVckwsSUFBSTBILElBQUosS0FBYSxDQUF2QjtLQUZHLE1BR0E7a0JBQ08xSCxJQUFJMEgsSUFBSixFQUFWO2tCQUNVZ0UsSUFBVjs7O1dBR0c7Y0FDR1EsT0FESDtjQUVHSDtLQUZWOzs7QUFNSixBQUFPLFNBQVNJLFdBQVQsQ0FBcUJ6RSxJQUFyQixFQUEyQjBELEdBQTNCLEVBQWdDQyxHQUFoQyxFQUFxQztRQUNwQ1EsYUFBYVYsZ0JBQWdCekQsSUFBaEIsRUFBc0IwRCxHQUF0QixFQUEyQkMsR0FBM0IsQ0FBakI7UUFDSWUsaUJBQWlCakIsZ0JBQWdCekQsT0FBTyxDQUF2QixFQUEwQjBELEdBQTFCLEVBQStCQyxHQUEvQixDQURyQjtXQUVPLENBQUNkLFdBQVc3QyxJQUFYLElBQW1CbUUsVUFBbkIsR0FBZ0NPLGNBQWpDLElBQW1ELENBQTFEOzs7QUN0REo7O0FBRUF2SSxlQUFlLEdBQWYsRUFBb0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFwQixFQUErQixJQUEvQixFQUFxQyxNQUFyQztBQUNBQSxlQUFlLEdBQWYsRUFBb0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFwQixFQUErQixJQUEvQixFQUFxQyxTQUFyQzs7OztBQUlBbkMsYUFBYSxNQUFiLEVBQXFCLEdBQXJCO0FBQ0FBLGFBQWEsU0FBYixFQUF3QixHQUF4Qjs7OztBQUlBWSxnQkFBZ0IsTUFBaEIsRUFBd0IsQ0FBeEI7QUFDQUEsZ0JBQWdCLFNBQWhCLEVBQTJCLENBQTNCOzs7O0FBSUFzRCxjQUFjLEdBQWQsRUFBb0JiLFNBQXBCO0FBQ0FhLGNBQWMsSUFBZCxFQUFvQmIsU0FBcEIsRUFBK0JKLE1BQS9CO0FBQ0FpQixjQUFjLEdBQWQsRUFBb0JiLFNBQXBCO0FBQ0FhLGNBQWMsSUFBZCxFQUFvQmIsU0FBcEIsRUFBK0JKLE1BQS9COztBQUVBZ0Msa0JBQWtCLENBQUMsR0FBRCxFQUFNLElBQU4sRUFBWSxHQUFaLEVBQWlCLElBQWpCLENBQWxCLEVBQTBDLFVBQVU3TixLQUFWLEVBQWlCNFMsSUFBakIsRUFBdUJ0Z0IsTUFBdkIsRUFBK0IwWSxLQUEvQixFQUFzQztTQUN2RUEsTUFBTWxTLE1BQU4sQ0FBYSxDQUFiLEVBQWdCLENBQWhCLENBQUwsSUFBMkI2TCxNQUFNM0UsS0FBTixDQUEzQjtDQURKOzs7Ozs7QUFRQSxBQUFPLFNBQVN1VCxVQUFULENBQXFCck0sR0FBckIsRUFBMEI7V0FDdEJpTSxXQUFXak0sR0FBWCxFQUFnQixLQUFLc00sS0FBTCxDQUFXbEIsR0FBM0IsRUFBZ0MsS0FBS2tCLEtBQUwsQ0FBV2pCLEdBQTNDLEVBQWdESyxJQUF2RDs7O0FBR0osQUFBTyxJQUFJYSxvQkFBb0I7U0FDckIsQ0FEcUI7U0FFckIsQ0FGcUI7Q0FBeEI7O0FBS1AsQUFBTyxTQUFTQyxvQkFBVCxHQUFpQztXQUM3QixLQUFLRixLQUFMLENBQVdsQixHQUFsQjs7O0FBR0osQUFBTyxTQUFTcUIsb0JBQVQsR0FBaUM7V0FDN0IsS0FBS0gsS0FBTCxDQUFXakIsR0FBbEI7Ozs7O0FBS0osQUFBTyxTQUFTcUIsVUFBVCxDQUFxQjVULEtBQXJCLEVBQTRCO1FBQzNCNFMsT0FBTyxLQUFLMUgsVUFBTCxHQUFrQjBILElBQWxCLENBQXVCLElBQXZCLENBQVg7V0FDTzVTLFNBQVMsSUFBVCxHQUFnQjRTLElBQWhCLEdBQXVCLEtBQUtpQixHQUFMLENBQVMsQ0FBQzdULFFBQVE0UyxJQUFULElBQWlCLENBQTFCLEVBQTZCLEdBQTdCLENBQTlCOzs7QUFHSixBQUFPLFNBQVNrQixhQUFULENBQXdCOVQsS0FBeEIsRUFBK0I7UUFDOUI0UyxPQUFPTyxXQUFXLElBQVgsRUFBaUIsQ0FBakIsRUFBb0IsQ0FBcEIsRUFBdUJQLElBQWxDO1dBQ081UyxTQUFTLElBQVQsR0FBZ0I0UyxJQUFoQixHQUF1QixLQUFLaUIsR0FBTCxDQUFTLENBQUM3VCxRQUFRNFMsSUFBVCxJQUFpQixDQUExQixFQUE2QixHQUE3QixDQUE5Qjs7O0FDckRKOztBQUVBN0gsZUFBZSxHQUFmLEVBQW9CLENBQXBCLEVBQXVCLElBQXZCLEVBQTZCLEtBQTdCOztBQUVBQSxlQUFlLElBQWYsRUFBcUIsQ0FBckIsRUFBd0IsQ0FBeEIsRUFBMkIsVUFBVTdKLE1BQVYsRUFBa0I7V0FDbEMsS0FBS2dLLFVBQUwsR0FBa0I2SSxXQUFsQixDQUE4QixJQUE5QixFQUFvQzdTLE1BQXBDLENBQVA7Q0FESjs7QUFJQTZKLGVBQWUsS0FBZixFQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixVQUFVN0osTUFBVixFQUFrQjtXQUNuQyxLQUFLZ0ssVUFBTCxHQUFrQjhJLGFBQWxCLENBQWdDLElBQWhDLEVBQXNDOVMsTUFBdEMsQ0FBUDtDQURKOztBQUlBNkosZUFBZSxNQUFmLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLEVBQTZCLFVBQVU3SixNQUFWLEVBQWtCO1dBQ3BDLEtBQUtnSyxVQUFMLEdBQWtCK0ksUUFBbEIsQ0FBMkIsSUFBM0IsRUFBaUMvUyxNQUFqQyxDQUFQO0NBREo7O0FBSUE2SixlQUFlLEdBQWYsRUFBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsRUFBMEIsU0FBMUI7QUFDQUEsZUFBZSxHQUFmLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCLEVBQTBCLFlBQTFCOzs7O0FBSUFuQyxhQUFhLEtBQWIsRUFBb0IsR0FBcEI7QUFDQUEsYUFBYSxTQUFiLEVBQXdCLEdBQXhCO0FBQ0FBLGFBQWEsWUFBYixFQUEyQixHQUEzQjs7O0FBR0FZLGdCQUFnQixLQUFoQixFQUF1QixFQUF2QjtBQUNBQSxnQkFBZ0IsU0FBaEIsRUFBMkIsRUFBM0I7QUFDQUEsZ0JBQWdCLFlBQWhCLEVBQThCLEVBQTlCOzs7O0FBSUFzRCxjQUFjLEdBQWQsRUFBc0JiLFNBQXRCO0FBQ0FhLGNBQWMsR0FBZCxFQUFzQmIsU0FBdEI7QUFDQWEsY0FBYyxHQUFkLEVBQXNCYixTQUF0QjtBQUNBYSxjQUFjLElBQWQsRUFBc0IsVUFBVUcsUUFBVixFQUFvQjlMLE1BQXBCLEVBQTRCO1dBQ3ZDQSxPQUFPK1MsZ0JBQVAsQ0FBd0JqSCxRQUF4QixDQUFQO0NBREo7QUFHQUgsY0FBYyxLQUFkLEVBQXVCLFVBQVVHLFFBQVYsRUFBb0I5TCxNQUFwQixFQUE0QjtXQUN4Q0EsT0FBT2dULGtCQUFQLENBQTBCbEgsUUFBMUIsQ0FBUDtDQURKO0FBR0FILGNBQWMsTUFBZCxFQUF3QixVQUFVRyxRQUFWLEVBQW9COUwsTUFBcEIsRUFBNEI7V0FDekNBLE9BQU9pVCxhQUFQLENBQXFCbkgsUUFBckIsQ0FBUDtDQURKOztBQUlBWSxrQkFBa0IsQ0FBQyxJQUFELEVBQU8sS0FBUCxFQUFjLE1BQWQsQ0FBbEIsRUFBeUMsVUFBVTdOLEtBQVYsRUFBaUI0UyxJQUFqQixFQUF1QnRnQixNQUF2QixFQUErQjBZLEtBQS9CLEVBQXNDO1FBQ3ZFNkgsVUFBVXZnQixPQUFPNlIsT0FBUCxDQUFla1EsYUFBZixDQUE2QnJVLEtBQTdCLEVBQW9DZ0wsS0FBcEMsRUFBMkMxWSxPQUFPd1EsT0FBbEQsQ0FBZDs7UUFFSStQLFdBQVcsSUFBZixFQUFxQjthQUNaam5CLENBQUwsR0FBU2luQixPQUFUO0tBREosTUFFTzt3QkFDYXZnQixNQUFoQixFQUF3Qm1RLGNBQXhCLEdBQXlDekMsS0FBekM7O0NBTlI7O0FBVUE2TixrQkFBa0IsQ0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLEdBQVgsQ0FBbEIsRUFBbUMsVUFBVTdOLEtBQVYsRUFBaUI0UyxJQUFqQixFQUF1QnRnQixNQUF2QixFQUErQjBZLEtBQS9CLEVBQXNDO1NBQ2hFQSxLQUFMLElBQWNyRyxNQUFNM0UsS0FBTixDQUFkO0NBREo7Ozs7QUFNQSxTQUFTc1UsWUFBVCxDQUFzQnRVLEtBQXRCLEVBQTZCbUIsTUFBN0IsRUFBcUM7UUFDN0IsT0FBT25CLEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7ZUFDcEJBLEtBQVA7OztRQUdBLENBQUN6SCxNQUFNeUgsS0FBTixDQUFMLEVBQW1CO2VBQ1J3UixTQUFTeFIsS0FBVCxFQUFnQixFQUFoQixDQUFQOzs7WUFHSW1CLE9BQU9rVCxhQUFQLENBQXFCclUsS0FBckIsQ0FBUjtRQUNJLE9BQU9BLEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7ZUFDcEJBLEtBQVA7OztXQUdHLElBQVA7OztBQUdKLFNBQVN1VSxlQUFULENBQXlCdlUsS0FBekIsRUFBZ0NtQixNQUFoQyxFQUF3QztRQUNoQyxPQUFPbkIsS0FBUCxLQUFpQixRQUFyQixFQUErQjtlQUNwQm1CLE9BQU9rVCxhQUFQLENBQXFCclUsS0FBckIsSUFBOEIsQ0FBOUIsSUFBbUMsQ0FBMUM7O1dBRUd6SCxNQUFNeUgsS0FBTixJQUFlLElBQWYsR0FBc0JBLEtBQTdCOzs7OztBQUtKLEFBQU8sSUFBSXdVLHdCQUF3QiwyREFBMkRuYixLQUEzRCxDQUFpRSxHQUFqRSxDQUE1QjtBQUNQLEFBQU8sU0FBU29iLGNBQVQsQ0FBeUJoVCxDQUF6QixFQUE0QlAsTUFBNUIsRUFBb0M7UUFDbkMsQ0FBQ08sQ0FBTCxFQUFRO2VBQ0csS0FBS2lULFNBQVo7O1dBRUczVSxRQUFRLEtBQUsyVSxTQUFiLElBQTBCLEtBQUtBLFNBQUwsQ0FBZWpULEVBQUVrVCxHQUFGLEVBQWYsQ0FBMUIsR0FDSCxLQUFLRCxTQUFMLENBQWUsS0FBS0EsU0FBTCxDQUFlakYsUUFBZixDQUF3QjlELElBQXhCLENBQTZCekssTUFBN0IsSUFBdUMsUUFBdkMsR0FBa0QsWUFBakUsRUFBK0VPLEVBQUVrVCxHQUFGLEVBQS9FLENBREo7OztBQUlKLEFBQU8sSUFBSUMsNkJBQTZCLDhCQUE4QnZiLEtBQTlCLENBQW9DLEdBQXBDLENBQWpDO0FBQ1AsQUFBTyxTQUFTd2IsbUJBQVQsQ0FBOEJwVCxDQUE5QixFQUFpQztXQUM1QkEsQ0FBRCxHQUFNLEtBQUtxVCxjQUFMLENBQW9CclQsRUFBRWtULEdBQUYsRUFBcEIsQ0FBTixHQUFxQyxLQUFLRyxjQUFqRDs7O0FBR0osQUFBTyxJQUFJQywyQkFBMkIsdUJBQXVCMWIsS0FBdkIsQ0FBNkIsR0FBN0IsQ0FBL0I7QUFDUCxBQUFPLFNBQVMyYixpQkFBVCxDQUE0QnZULENBQTVCLEVBQStCO1dBQzFCQSxDQUFELEdBQU0sS0FBS3dULFlBQUwsQ0FBa0J4VCxFQUFFa1QsR0FBRixFQUFsQixDQUFOLEdBQW1DLEtBQUtNLFlBQS9DOzs7QUFHSixTQUFTcEYsbUJBQVQsQ0FBMkJxRixXQUEzQixFQUF3Q2hVLE1BQXhDLEVBQWdERSxNQUFoRCxFQUF3RDtRQUNoRHJULENBQUo7UUFBT2dpQixFQUFQO1FBQVc3SSxHQUFYO1FBQWdCOEksTUFBTWtGLFlBQVlqRixpQkFBWixFQUF0QjtRQUNJLENBQUMsS0FBS2tGLGNBQVYsRUFBMEI7YUFDakJBLGNBQUwsR0FBc0IsRUFBdEI7YUFDS0MsbUJBQUwsR0FBMkIsRUFBM0I7YUFDS0MsaUJBQUwsR0FBeUIsRUFBekI7O2FBRUt0bkIsSUFBSSxDQUFULEVBQVlBLElBQUksQ0FBaEIsRUFBbUIsRUFBRUEsQ0FBckIsRUFBd0I7a0JBQ2RrVCxVQUFVLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FBVixFQUFxQjBULEdBQXJCLENBQXlCNW1CLENBQXpCLENBQU47aUJBQ0tzbkIsaUJBQUwsQ0FBdUJ0bkIsQ0FBdkIsSUFBNEIsS0FBS2dtQixXQUFMLENBQWlCN00sR0FBakIsRUFBc0IsRUFBdEIsRUFBMEIrSSxpQkFBMUIsRUFBNUI7aUJBQ0ttRixtQkFBTCxDQUF5QnJuQixDQUF6QixJQUE4QixLQUFLaW1CLGFBQUwsQ0FBbUI5TSxHQUFuQixFQUF3QixFQUF4QixFQUE0QitJLGlCQUE1QixFQUE5QjtpQkFDS2tGLGNBQUwsQ0FBb0JwbkIsQ0FBcEIsSUFBeUIsS0FBS2ttQixRQUFMLENBQWMvTSxHQUFkLEVBQW1CLEVBQW5CLEVBQXVCK0ksaUJBQXZCLEVBQXpCOzs7O1FBSUo3TyxNQUFKLEVBQVk7WUFDSkYsV0FBVyxNQUFmLEVBQXVCO2lCQUNkckksUUFBUWdELElBQVIsQ0FBYSxLQUFLc1osY0FBbEIsRUFBa0NuRixHQUFsQyxDQUFMO21CQUNPRCxPQUFPLENBQUMsQ0FBUixHQUFZQSxFQUFaLEdBQWlCLElBQXhCO1NBRkosTUFHTyxJQUFJN08sV0FBVyxLQUFmLEVBQXNCO2lCQUNwQnJJLFFBQVFnRCxJQUFSLENBQWEsS0FBS3VaLG1CQUFsQixFQUF1Q3BGLEdBQXZDLENBQUw7bUJBQ09ELE9BQU8sQ0FBQyxDQUFSLEdBQVlBLEVBQVosR0FBaUIsSUFBeEI7U0FGRyxNQUdBO2lCQUNFbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLd1osaUJBQWxCLEVBQXFDckYsR0FBckMsQ0FBTDttQkFDT0QsT0FBTyxDQUFDLENBQVIsR0FBWUEsRUFBWixHQUFpQixJQUF4Qjs7S0FUUixNQVdPO1lBQ0M3TyxXQUFXLE1BQWYsRUFBdUI7aUJBQ2RySSxRQUFRZ0QsSUFBUixDQUFhLEtBQUtzWixjQUFsQixFQUFrQ25GLEdBQWxDLENBQUw7Z0JBQ0lELE9BQU8sQ0FBQyxDQUFaLEVBQWU7dUJBQ0pBLEVBQVA7O2lCQUVDbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLdVosbUJBQWxCLEVBQXVDcEYsR0FBdkMsQ0FBTDtnQkFDSUQsT0FBTyxDQUFDLENBQVosRUFBZTt1QkFDSkEsRUFBUDs7aUJBRUNsWCxRQUFRZ0QsSUFBUixDQUFhLEtBQUt3WixpQkFBbEIsRUFBcUNyRixHQUFyQyxDQUFMO21CQUNPRCxPQUFPLENBQUMsQ0FBUixHQUFZQSxFQUFaLEdBQWlCLElBQXhCO1NBVkosTUFXTyxJQUFJN08sV0FBVyxLQUFmLEVBQXNCO2lCQUNwQnJJLFFBQVFnRCxJQUFSLENBQWEsS0FBS3VaLG1CQUFsQixFQUF1Q3BGLEdBQXZDLENBQUw7Z0JBQ0lELE9BQU8sQ0FBQyxDQUFaLEVBQWU7dUJBQ0pBLEVBQVA7O2lCQUVDbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLc1osY0FBbEIsRUFBa0NuRixHQUFsQyxDQUFMO2dCQUNJRCxPQUFPLENBQUMsQ0FBWixFQUFlO3VCQUNKQSxFQUFQOztpQkFFQ2xYLFFBQVFnRCxJQUFSLENBQWEsS0FBS3daLGlCQUFsQixFQUFxQ3JGLEdBQXJDLENBQUw7bUJBQ09ELE9BQU8sQ0FBQyxDQUFSLEdBQVlBLEVBQVosR0FBaUIsSUFBeEI7U0FWRyxNQVdBO2lCQUNFbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLd1osaUJBQWxCLEVBQXFDckYsR0FBckMsQ0FBTDtnQkFDSUQsT0FBTyxDQUFDLENBQVosRUFBZTt1QkFDSkEsRUFBUDs7aUJBRUNsWCxRQUFRZ0QsSUFBUixDQUFhLEtBQUtzWixjQUFsQixFQUFrQ25GLEdBQWxDLENBQUw7Z0JBQ0lELE9BQU8sQ0FBQyxDQUFaLEVBQWU7dUJBQ0pBLEVBQVA7O2lCQUVDbFgsUUFBUWdELElBQVIsQ0FBYSxLQUFLdVosbUJBQWxCLEVBQXVDcEYsR0FBdkMsQ0FBTDttQkFDT0QsT0FBTyxDQUFDLENBQVIsR0FBWUEsRUFBWixHQUFpQixJQUF4Qjs7Ozs7QUFLWixBQUFPLFNBQVN1RixtQkFBVCxDQUE4QkosV0FBOUIsRUFBMkNoVSxNQUEzQyxFQUFtREUsTUFBbkQsRUFBMkQ7UUFDMURyVCxDQUFKLEVBQU9tWixHQUFQLEVBQVk2RixLQUFaOztRQUVJLEtBQUt3SSxtQkFBVCxFQUE4QjtlQUNuQjFGLG9CQUFrQmhVLElBQWxCLENBQXVCLElBQXZCLEVBQTZCcVosV0FBN0IsRUFBMENoVSxNQUExQyxFQUFrREUsTUFBbEQsQ0FBUDs7O1FBR0EsQ0FBQyxLQUFLK1QsY0FBVixFQUEwQjthQUNqQkEsY0FBTCxHQUFzQixFQUF0QjthQUNLRSxpQkFBTCxHQUF5QixFQUF6QjthQUNLRCxtQkFBTCxHQUEyQixFQUEzQjthQUNLSSxrQkFBTCxHQUEwQixFQUExQjs7O1NBR0N6bkIsSUFBSSxDQUFULEVBQVlBLElBQUksQ0FBaEIsRUFBbUJBLEdBQW5CLEVBQXdCOzs7Y0FHZGtULFVBQVUsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFWLEVBQXFCMFQsR0FBckIsQ0FBeUI1bUIsQ0FBekIsQ0FBTjtZQUNJcVQsVUFBVSxDQUFDLEtBQUtvVSxrQkFBTCxDQUF3QnpuQixDQUF4QixDQUFmLEVBQTJDO2lCQUNsQ3luQixrQkFBTCxDQUF3QnpuQixDQUF4QixJQUE2QixJQUFJMlksTUFBSixDQUFXLE1BQU0sS0FBS3VOLFFBQUwsQ0FBYy9NLEdBQWQsRUFBbUIsRUFBbkIsRUFBdUJTLE9BQXZCLENBQStCLEdBQS9CLEVBQW9DLEtBQXBDLENBQU4sR0FBbUQsR0FBOUQsRUFBbUUsR0FBbkUsQ0FBN0I7aUJBQ0t5TixtQkFBTCxDQUF5QnJuQixDQUF6QixJQUE4QixJQUFJMlksTUFBSixDQUFXLE1BQU0sS0FBS3NOLGFBQUwsQ0FBbUI5TSxHQUFuQixFQUF3QixFQUF4QixFQUE0QlMsT0FBNUIsQ0FBb0MsR0FBcEMsRUFBeUMsS0FBekMsQ0FBTixHQUF3RCxHQUFuRSxFQUF3RSxHQUF4RSxDQUE5QjtpQkFDSzBOLGlCQUFMLENBQXVCdG5CLENBQXZCLElBQTRCLElBQUkyWSxNQUFKLENBQVcsTUFBTSxLQUFLcU4sV0FBTCxDQUFpQjdNLEdBQWpCLEVBQXNCLEVBQXRCLEVBQTBCUyxPQUExQixDQUFrQyxHQUFsQyxFQUF1QyxLQUF2QyxDQUFOLEdBQXNELEdBQWpFLEVBQXNFLEdBQXRFLENBQTVCOztZQUVBLENBQUMsS0FBS3dOLGNBQUwsQ0FBb0JwbkIsQ0FBcEIsQ0FBTCxFQUE2QjtvQkFDakIsTUFBTSxLQUFLa21CLFFBQUwsQ0FBYy9NLEdBQWQsRUFBbUIsRUFBbkIsQ0FBTixHQUErQixJQUEvQixHQUFzQyxLQUFLOE0sYUFBTCxDQUFtQjlNLEdBQW5CLEVBQXdCLEVBQXhCLENBQXRDLEdBQW9FLElBQXBFLEdBQTJFLEtBQUs2TSxXQUFMLENBQWlCN00sR0FBakIsRUFBc0IsRUFBdEIsQ0FBbkY7aUJBQ0tpTyxjQUFMLENBQW9CcG5CLENBQXBCLElBQXlCLElBQUkyWSxNQUFKLENBQVdxRyxNQUFNcEYsT0FBTixDQUFjLEdBQWQsRUFBbUIsRUFBbkIsQ0FBWCxFQUFtQyxHQUFuQyxDQUF6Qjs7O1lBR0F2RyxVQUFVRixXQUFXLE1BQXJCLElBQStCLEtBQUtzVSxrQkFBTCxDQUF3QnpuQixDQUF4QixFQUEyQjRkLElBQTNCLENBQWdDdUosV0FBaEMsQ0FBbkMsRUFBaUY7bUJBQ3RFbm5CLENBQVA7U0FESixNQUVPLElBQUlxVCxVQUFVRixXQUFXLEtBQXJCLElBQThCLEtBQUtrVSxtQkFBTCxDQUF5QnJuQixDQUF6QixFQUE0QjRkLElBQTVCLENBQWlDdUosV0FBakMsQ0FBbEMsRUFBaUY7bUJBQzdFbm5CLENBQVA7U0FERyxNQUVBLElBQUlxVCxVQUFVRixXQUFXLElBQXJCLElBQTZCLEtBQUttVSxpQkFBTCxDQUF1QnRuQixDQUF2QixFQUEwQjRkLElBQTFCLENBQStCdUosV0FBL0IsQ0FBakMsRUFBOEU7bUJBQzFFbm5CLENBQVA7U0FERyxNQUVBLElBQUksQ0FBQ3FULE1BQUQsSUFBVyxLQUFLK1QsY0FBTCxDQUFvQnBuQixDQUFwQixFQUF1QjRkLElBQXZCLENBQTRCdUosV0FBNUIsQ0FBZixFQUF5RDttQkFDckRubkIsQ0FBUDs7Ozs7OztBQU9aLEFBQU8sU0FBUzBuQixlQUFULENBQTBCelYsS0FBMUIsRUFBaUM7UUFDaEMsQ0FBQyxLQUFLOEIsT0FBTCxFQUFMLEVBQXFCO2VBQ1Y5QixTQUFTLElBQVQsR0FBZ0IsSUFBaEIsR0FBdUJxRCxHQUE5Qjs7UUFFQXNSLE1BQU0sS0FBSzFRLE1BQUwsR0FBYyxLQUFLN0IsRUFBTCxDQUFRcVEsU0FBUixFQUFkLEdBQW9DLEtBQUtyUSxFQUFMLENBQVFzVCxNQUFSLEVBQTlDO1FBQ0kxVixTQUFTLElBQWIsRUFBbUI7Z0JBQ1BzVSxhQUFhdFUsS0FBYixFQUFvQixLQUFLa0wsVUFBTCxFQUFwQixDQUFSO2VBQ08sS0FBSzJJLEdBQUwsQ0FBUzdULFFBQVEyVSxHQUFqQixFQUFzQixHQUF0QixDQUFQO0tBRkosTUFHTztlQUNJQSxHQUFQOzs7O0FBSVIsQUFBTyxTQUFTZ0IscUJBQVQsQ0FBZ0MzVixLQUFoQyxFQUF1QztRQUN0QyxDQUFDLEtBQUs4QixPQUFMLEVBQUwsRUFBcUI7ZUFDVjlCLFNBQVMsSUFBVCxHQUFnQixJQUFoQixHQUF1QnFELEdBQTlCOztRQUVBd1AsVUFBVSxDQUFDLEtBQUs4QixHQUFMLEtBQWEsQ0FBYixHQUFpQixLQUFLekosVUFBTCxHQUFrQnNJLEtBQWxCLENBQXdCbEIsR0FBMUMsSUFBaUQsQ0FBL0Q7V0FDT3RTLFNBQVMsSUFBVCxHQUFnQjZTLE9BQWhCLEdBQTBCLEtBQUtnQixHQUFMLENBQVM3VCxRQUFRNlMsT0FBakIsRUFBMEIsR0FBMUIsQ0FBakM7OztBQUdKLEFBQU8sU0FBUytDLGtCQUFULENBQTZCNVYsS0FBN0IsRUFBb0M7UUFDbkMsQ0FBQyxLQUFLOEIsT0FBTCxFQUFMLEVBQXFCO2VBQ1Y5QixTQUFTLElBQVQsR0FBZ0IsSUFBaEIsR0FBdUJxRCxHQUE5Qjs7Ozs7OztRQU9BckQsU0FBUyxJQUFiLEVBQW1CO1lBQ1g2UyxVQUFVMEIsZ0JBQWdCdlUsS0FBaEIsRUFBdUIsS0FBS2tMLFVBQUwsRUFBdkIsQ0FBZDtlQUNPLEtBQUt5SixHQUFMLENBQVMsS0FBS0EsR0FBTCxLQUFhLENBQWIsR0FBaUI5QixPQUFqQixHQUEyQkEsVUFBVSxDQUE5QyxDQUFQO0tBRkosTUFHTztlQUNJLEtBQUs4QixHQUFMLE1BQWMsQ0FBckI7Ozs7QUFJUixJQUFJa0IsdUJBQXVCakosU0FBM0I7QUFDQSxBQUFPLFNBQVN3SCxhQUFULENBQXdCbkgsUUFBeEIsRUFBa0M7UUFDakMsS0FBS3NJLG1CQUFULEVBQThCO1lBQ3RCLENBQUN6VSxXQUFXLElBQVgsRUFBaUIsZ0JBQWpCLENBQUwsRUFBeUM7aUNBQ2hCakYsSUFBckIsQ0FBMEIsSUFBMUI7O1lBRUFvUixRQUFKLEVBQWM7bUJBQ0gsS0FBSzZJLG9CQUFaO1NBREosTUFFTzttQkFDSSxLQUFLQyxjQUFaOztLQVBSLE1BU087WUFDQyxDQUFDalYsV0FBVyxJQUFYLEVBQWlCLGdCQUFqQixDQUFMLEVBQXlDO2lCQUNoQ2lWLGNBQUwsR0FBc0JGLG9CQUF0Qjs7ZUFFRyxLQUFLQyxvQkFBTCxJQUE2QjdJLFFBQTdCLEdBQ0gsS0FBSzZJLG9CQURGLEdBQ3lCLEtBQUtDLGNBRHJDOzs7O0FBS1IsSUFBSUMsNEJBQTRCcEosU0FBaEM7QUFDQSxBQUFPLFNBQVN1SCxrQkFBVCxDQUE2QmxILFFBQTdCLEVBQXVDO1FBQ3RDLEtBQUtzSSxtQkFBVCxFQUE4QjtZQUN0QixDQUFDelUsV0FBVyxJQUFYLEVBQWlCLGdCQUFqQixDQUFMLEVBQXlDO2lDQUNoQmpGLElBQXJCLENBQTBCLElBQTFCOztZQUVBb1IsUUFBSixFQUFjO21CQUNILEtBQUtnSix5QkFBWjtTQURKLE1BRU87bUJBQ0ksS0FBS0MsbUJBQVo7O0tBUFIsTUFTTztZQUNDLENBQUNwVixXQUFXLElBQVgsRUFBaUIscUJBQWpCLENBQUwsRUFBOEM7aUJBQ3JDb1YsbUJBQUwsR0FBMkJGLHlCQUEzQjs7ZUFFRyxLQUFLQyx5QkFBTCxJQUFrQ2hKLFFBQWxDLEdBQ0gsS0FBS2dKLHlCQURGLEdBQzhCLEtBQUtDLG1CQUQxQzs7OztBQUtSLElBQUlDLDBCQUEwQnZKLFNBQTlCO0FBQ0EsQUFBTyxTQUFTc0gsZ0JBQVQsQ0FBMkJqSCxRQUEzQixFQUFxQztRQUNwQyxLQUFLc0ksbUJBQVQsRUFBOEI7WUFDdEIsQ0FBQ3pVLFdBQVcsSUFBWCxFQUFpQixnQkFBakIsQ0FBTCxFQUF5QztpQ0FDaEJqRixJQUFyQixDQUEwQixJQUExQjs7WUFFQW9SLFFBQUosRUFBYzttQkFDSCxLQUFLbUosdUJBQVo7U0FESixNQUVPO21CQUNJLEtBQUtDLGlCQUFaOztLQVBSLE1BU087WUFDQyxDQUFDdlYsV0FBVyxJQUFYLEVBQWlCLG1CQUFqQixDQUFMLEVBQTRDO2lCQUNuQ3VWLGlCQUFMLEdBQXlCRix1QkFBekI7O2VBRUcsS0FBS0MsdUJBQUwsSUFBZ0NuSixRQUFoQyxHQUNILEtBQUttSix1QkFERixHQUM0QixLQUFLQyxpQkFEeEM7Ozs7QUFNUixTQUFTQyxvQkFBVCxHQUFpQzthQUNwQm5GLFNBQVQsQ0FBbUJqZ0IsQ0FBbkIsRUFBc0JDLENBQXRCLEVBQXlCO2VBQ2RBLEVBQUV6QyxNQUFGLEdBQVd3QyxFQUFFeEMsTUFBcEI7OztRQUdBNm5CLFlBQVksRUFBaEI7UUFBb0JuRixjQUFjLEVBQWxDO1FBQXNDQyxhQUFhLEVBQW5EO1FBQXVEQyxjQUFjLEVBQXJFO1FBQ0l2akIsQ0FESjtRQUNPbVosR0FEUDtRQUNZc1AsSUFEWjtRQUNrQkMsTUFEbEI7UUFDMEJDLEtBRDFCO1NBRUszb0IsSUFBSSxDQUFULEVBQVlBLElBQUksQ0FBaEIsRUFBbUJBLEdBQW5CLEVBQXdCOztjQUVka1QsVUFBVSxDQUFDLElBQUQsRUFBTyxDQUFQLENBQVYsRUFBcUIwVCxHQUFyQixDQUF5QjVtQixDQUF6QixDQUFOO2VBQ08sS0FBS2dtQixXQUFMLENBQWlCN00sR0FBakIsRUFBc0IsRUFBdEIsQ0FBUDtpQkFDUyxLQUFLOE0sYUFBTCxDQUFtQjlNLEdBQW5CLEVBQXdCLEVBQXhCLENBQVQ7Z0JBQ1EsS0FBSytNLFFBQUwsQ0FBYy9NLEdBQWQsRUFBbUIsRUFBbkIsQ0FBUjtrQkFDVXBjLElBQVYsQ0FBZTByQixJQUFmO29CQUNZMXJCLElBQVosQ0FBaUIyckIsTUFBakI7bUJBQ1czckIsSUFBWCxDQUFnQjRyQixLQUFoQjtvQkFDWTVyQixJQUFaLENBQWlCMHJCLElBQWpCO29CQUNZMXJCLElBQVosQ0FBaUIyckIsTUFBakI7b0JBQ1kzckIsSUFBWixDQUFpQjRyQixLQUFqQjs7OztjQUlNN00sSUFBVixDQUFlc0gsU0FBZjtnQkFDWXRILElBQVosQ0FBaUJzSCxTQUFqQjtlQUNXdEgsSUFBWCxDQUFnQnNILFNBQWhCO2dCQUNZdEgsSUFBWixDQUFpQnNILFNBQWpCO1NBQ0twakIsSUFBSSxDQUFULEVBQVlBLElBQUksQ0FBaEIsRUFBbUJBLEdBQW5CLEVBQXdCO29CQUNSQSxDQUFaLElBQWlCc2YsWUFBWStELFlBQVlyakIsQ0FBWixDQUFaLENBQWpCO21CQUNXQSxDQUFYLElBQWdCc2YsWUFBWWdFLFdBQVd0akIsQ0FBWCxDQUFaLENBQWhCO29CQUNZQSxDQUFaLElBQWlCc2YsWUFBWWlFLFlBQVl2akIsQ0FBWixDQUFaLENBQWpCOzs7U0FHQ2dvQixjQUFMLEdBQXNCLElBQUlyUCxNQUFKLENBQVcsT0FBTzRLLFlBQVl0TCxJQUFaLENBQWlCLEdBQWpCLENBQVAsR0FBK0IsR0FBMUMsRUFBK0MsR0FBL0MsQ0FBdEI7U0FDS2tRLG1CQUFMLEdBQTJCLEtBQUtILGNBQWhDO1NBQ0tNLGlCQUFMLEdBQXlCLEtBQUtOLGNBQTlCOztTQUVLRCxvQkFBTCxHQUE0QixJQUFJcFAsTUFBSixDQUFXLE9BQU8ySyxXQUFXckwsSUFBWCxDQUFnQixHQUFoQixDQUFQLEdBQThCLEdBQXpDLEVBQThDLEdBQTlDLENBQTVCO1NBQ0tpUSx5QkFBTCxHQUFpQyxJQUFJdlAsTUFBSixDQUFXLE9BQU8wSyxZQUFZcEwsSUFBWixDQUFpQixHQUFqQixDQUFQLEdBQStCLEdBQTFDLEVBQStDLEdBQS9DLENBQWpDO1NBQ0tvUSx1QkFBTCxHQUErQixJQUFJMVAsTUFBSixDQUFXLE9BQU82UCxVQUFVdlEsSUFBVixDQUFlLEdBQWYsQ0FBUCxHQUE2QixHQUF4QyxFQUE2QyxHQUE3QyxDQUEvQjs7O0FDOVZKOztBQUVBLFNBQVMyUSxPQUFULEdBQW1CO1dBQ1IsS0FBS0MsS0FBTCxLQUFlLEVBQWYsSUFBcUIsRUFBNUI7OztBQUdKLFNBQVNDLE9BQVQsR0FBbUI7V0FDUixLQUFLRCxLQUFMLE1BQWdCLEVBQXZCOzs7QUFHSjdMLGVBQWUsR0FBZixFQUFvQixDQUFDLElBQUQsRUFBTyxDQUFQLENBQXBCLEVBQStCLENBQS9CLEVBQWtDLE1BQWxDO0FBQ0FBLGVBQWUsR0FBZixFQUFvQixDQUFDLElBQUQsRUFBTyxDQUFQLENBQXBCLEVBQStCLENBQS9CLEVBQWtDNEwsT0FBbEM7QUFDQTVMLGVBQWUsR0FBZixFQUFvQixDQUFDLElBQUQsRUFBTyxDQUFQLENBQXBCLEVBQStCLENBQS9CLEVBQWtDOEwsT0FBbEM7O0FBRUE5TCxlQUFlLEtBQWYsRUFBc0IsQ0FBdEIsRUFBeUIsQ0FBekIsRUFBNEIsWUFBWTtXQUM3QixLQUFLNEwsUUFBUWhYLEtBQVIsQ0FBYyxJQUFkLENBQUwsR0FBMkJ5SyxTQUFTLEtBQUswTSxPQUFMLEVBQVQsRUFBeUIsQ0FBekIsQ0FBbEM7Q0FESjs7QUFJQS9MLGVBQWUsT0FBZixFQUF3QixDQUF4QixFQUEyQixDQUEzQixFQUE4QixZQUFZO1dBQy9CLEtBQUs0TCxRQUFRaFgsS0FBUixDQUFjLElBQWQsQ0FBTCxHQUEyQnlLLFNBQVMsS0FBSzBNLE9BQUwsRUFBVCxFQUF5QixDQUF6QixDQUEzQixHQUNIMU0sU0FBUyxLQUFLMk0sT0FBTCxFQUFULEVBQXlCLENBQXpCLENBREo7Q0FESjs7QUFLQWhNLGVBQWUsS0FBZixFQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixZQUFZO1dBQzdCLEtBQUssS0FBSzZMLEtBQUwsRUFBTCxHQUFvQnhNLFNBQVMsS0FBSzBNLE9BQUwsRUFBVCxFQUF5QixDQUF6QixDQUEzQjtDQURKOztBQUlBL0wsZUFBZSxPQUFmLEVBQXdCLENBQXhCLEVBQTJCLENBQTNCLEVBQThCLFlBQVk7V0FDL0IsS0FBSyxLQUFLNkwsS0FBTCxFQUFMLEdBQW9CeE0sU0FBUyxLQUFLME0sT0FBTCxFQUFULEVBQXlCLENBQXpCLENBQXBCLEdBQ0gxTSxTQUFTLEtBQUsyTSxPQUFMLEVBQVQsRUFBeUIsQ0FBekIsQ0FESjtDQURKOztBQUtBLFNBQVNsVSxRQUFULENBQW1CbUksS0FBbkIsRUFBMEJnTSxTQUExQixFQUFxQzttQkFDbEJoTSxLQUFmLEVBQXNCLENBQXRCLEVBQXlCLENBQXpCLEVBQTRCLFlBQVk7ZUFDN0IsS0FBS0UsVUFBTCxHQUFrQnJJLFFBQWxCLENBQTJCLEtBQUsrVCxLQUFMLEVBQTNCLEVBQXlDLEtBQUtFLE9BQUwsRUFBekMsRUFBeURFLFNBQXpELENBQVA7S0FESjs7O0FBS0puVSxTQUFTLEdBQVQsRUFBYyxJQUFkO0FBQ0FBLFNBQVMsR0FBVCxFQUFjLEtBQWQ7Ozs7QUFJQStGLGFBQWEsTUFBYixFQUFxQixHQUFyQjs7O0FBR0FZLGdCQUFnQixNQUFoQixFQUF3QixFQUF4Qjs7OztBQUlBLFNBQVN5TixhQUFULENBQXdCaEssUUFBeEIsRUFBa0M5TCxNQUFsQyxFQUEwQztXQUMvQkEsT0FBTytWLGNBQWQ7OztBQUdKcEssY0FBYyxHQUFkLEVBQW9CbUssYUFBcEI7QUFDQW5LLGNBQWMsR0FBZCxFQUFvQm1LLGFBQXBCO0FBQ0FuSyxjQUFjLEdBQWQsRUFBb0JiLFNBQXBCO0FBQ0FhLGNBQWMsR0FBZCxFQUFvQmIsU0FBcEI7QUFDQWEsY0FBYyxJQUFkLEVBQW9CYixTQUFwQixFQUErQkosTUFBL0I7QUFDQWlCLGNBQWMsSUFBZCxFQUFvQmIsU0FBcEIsRUFBK0JKLE1BQS9COztBQUVBaUIsY0FBYyxLQUFkLEVBQXFCWixTQUFyQjtBQUNBWSxjQUFjLE9BQWQsRUFBdUJYLFNBQXZCO0FBQ0FXLGNBQWMsS0FBZCxFQUFxQlosU0FBckI7QUFDQVksY0FBYyxPQUFkLEVBQXVCWCxTQUF2Qjs7QUFFQXlCLGNBQWMsQ0FBQyxHQUFELEVBQU0sSUFBTixDQUFkLEVBQTJCUSxJQUEzQjtBQUNBUixjQUFjLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBZCxFQUEwQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCaFosTUFBeEIsRUFBZ0M7V0FDL0M2a0IsS0FBUCxHQUFlN2tCLE9BQU82UixPQUFQLENBQWVpVCxJQUFmLENBQW9CcFgsS0FBcEIsQ0FBZjtXQUNPcVgsU0FBUCxHQUFtQnJYLEtBQW5CO0NBRko7QUFJQTROLGNBQWMsQ0FBQyxHQUFELEVBQU0sSUFBTixDQUFkLEVBQTJCLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0JoWixNQUF4QixFQUFnQztVQUNqRDhiLElBQU4sSUFBY3pKLE1BQU0zRSxLQUFOLENBQWQ7b0JBQ2dCMU4sTUFBaEIsRUFBd0IyUSxPQUF4QixHQUFrQyxJQUFsQztDQUZKO0FBSUEySyxjQUFjLEtBQWQsRUFBcUIsVUFBVTVOLEtBQVYsRUFBaUJzTCxLQUFqQixFQUF3QmhaLE1BQXhCLEVBQWdDO1FBQzdDakcsTUFBTTJULE1BQU10UixNQUFOLEdBQWUsQ0FBekI7VUFDTTBmLElBQU4sSUFBY3pKLE1BQU0zRSxNQUFNbEgsTUFBTixDQUFhLENBQWIsRUFBZ0J6TSxHQUFoQixDQUFOLENBQWQ7VUFDTWdpQixNQUFOLElBQWdCMUosTUFBTTNFLE1BQU1sSCxNQUFOLENBQWF6TSxHQUFiLENBQU4sQ0FBaEI7b0JBQ2dCaUcsTUFBaEIsRUFBd0IyUSxPQUF4QixHQUFrQyxJQUFsQztDQUpKO0FBTUEySyxjQUFjLE9BQWQsRUFBdUIsVUFBVTVOLEtBQVYsRUFBaUJzTCxLQUFqQixFQUF3QmhaLE1BQXhCLEVBQWdDO1FBQy9DZ2xCLE9BQU90WCxNQUFNdFIsTUFBTixHQUFlLENBQTFCO1FBQ0k2b0IsT0FBT3ZYLE1BQU10UixNQUFOLEdBQWUsQ0FBMUI7VUFDTTBmLElBQU4sSUFBY3pKLE1BQU0zRSxNQUFNbEgsTUFBTixDQUFhLENBQWIsRUFBZ0J3ZSxJQUFoQixDQUFOLENBQWQ7VUFDTWpKLE1BQU4sSUFBZ0IxSixNQUFNM0UsTUFBTWxILE1BQU4sQ0FBYXdlLElBQWIsRUFBbUIsQ0FBbkIsQ0FBTixDQUFoQjtVQUNNaEosTUFBTixJQUFnQjNKLE1BQU0zRSxNQUFNbEgsTUFBTixDQUFheWUsSUFBYixDQUFOLENBQWhCO29CQUNnQmpsQixNQUFoQixFQUF3QjJRLE9BQXhCLEdBQWtDLElBQWxDO0NBTko7QUFRQTJLLGNBQWMsS0FBZCxFQUFxQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCaFosTUFBeEIsRUFBZ0M7UUFDN0NqRyxNQUFNMlQsTUFBTXRSLE1BQU4sR0FBZSxDQUF6QjtVQUNNMGYsSUFBTixJQUFjekosTUFBTTNFLE1BQU1sSCxNQUFOLENBQWEsQ0FBYixFQUFnQnpNLEdBQWhCLENBQU4sQ0FBZDtVQUNNZ2lCLE1BQU4sSUFBZ0IxSixNQUFNM0UsTUFBTWxILE1BQU4sQ0FBYXpNLEdBQWIsQ0FBTixDQUFoQjtDQUhKO0FBS0F1aEIsY0FBYyxPQUFkLEVBQXVCLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0JoWixNQUF4QixFQUFnQztRQUMvQ2dsQixPQUFPdFgsTUFBTXRSLE1BQU4sR0FBZSxDQUExQjtRQUNJNm9CLE9BQU92WCxNQUFNdFIsTUFBTixHQUFlLENBQTFCO1VBQ00wZixJQUFOLElBQWN6SixNQUFNM0UsTUFBTWxILE1BQU4sQ0FBYSxDQUFiLEVBQWdCd2UsSUFBaEIsQ0FBTixDQUFkO1VBQ01qSixNQUFOLElBQWdCMUosTUFBTTNFLE1BQU1sSCxNQUFOLENBQWF3ZSxJQUFiLEVBQW1CLENBQW5CLENBQU4sQ0FBaEI7VUFDTWhKLE1BQU4sSUFBZ0IzSixNQUFNM0UsTUFBTWxILE1BQU4sQ0FBYXllLElBQWIsQ0FBTixDQUFoQjtDQUxKOzs7O0FBVUEsQUFBTyxTQUFTQyxVQUFULENBQXFCeFgsS0FBckIsRUFBNEI7OztXQUd2QixDQUFDQSxRQUFRLEVBQVQsRUFBYWdKLFdBQWIsR0FBMkJ5TyxNQUEzQixDQUFrQyxDQUFsQyxNQUF5QyxHQUFqRDs7O0FBR0osQUFBTyxJQUFJQyw2QkFBNkIsZUFBakM7QUFDUCxBQUFPLFNBQVNDLGNBQVQsQ0FBeUJmLEtBQXpCLEVBQWdDRSxPQUFoQyxFQUF5Q2MsT0FBekMsRUFBa0Q7UUFDakRoQixRQUFRLEVBQVosRUFBZ0I7ZUFDTGdCLFVBQVUsSUFBVixHQUFpQixJQUF4QjtLQURKLE1BRU87ZUFDSUEsVUFBVSxJQUFWLEdBQWlCLElBQXhCOzs7Ozs7Ozs7O0FBV1IsQUFBTyxJQUFJQyxhQUFhL04sV0FBVyxPQUFYLEVBQW9CLElBQXBCLENBQWpCOztBQ25JUDtBQUNBLEFBS0E7QUFDQSxBQUVBO0FBQ0EsQUFNQTtBQUNBLEFBRUEsQUFBTyxJQUFJZ08sYUFBYTtjQUNWOVEsZUFEVTtvQkFFSk0scUJBRkk7aUJBR1BNLGtCQUhPO2FBSVhHLGNBSlc7a0JBS05DLG1CQUxNO2tCQU1ORyxtQkFOTTs7WUFRWm1ILG1CQVJZO2lCQVNQSSx3QkFUTzs7VUFXZCtELGlCQVhjOztjQWFWZSxxQkFiVTtpQkFjUE8sd0JBZE87bUJBZUxILDBCQWZLOzttQkFpQkw4QztDQWpCWjs7QUNkUDtBQUNBLElBQUlLLFVBQVUsRUFBZDtBQUNBLElBQUlDLGlCQUFpQixFQUFyQjtBQUNBLElBQUlDLFlBQUo7O0FBRUEsU0FBU0MsZUFBVCxDQUF5QnBwQixHQUF6QixFQUE4QjtXQUNuQkEsTUFBTUEsSUFBSWthLFdBQUosR0FBa0JyQixPQUFsQixDQUEwQixHQUExQixFQUErQixHQUEvQixDQUFOLEdBQTRDN1ksR0FBbkQ7Ozs7OztBQU1KLFNBQVNxcEIsWUFBVCxDQUFzQm5rQixLQUF0QixFQUE2QjtRQUNyQmpHLElBQUksQ0FBUjtRQUFXeUosQ0FBWDtRQUFjNGdCLElBQWQ7UUFBb0JqWCxNQUFwQjtRQUE0QjlILEtBQTVCOztXQUVPdEwsSUFBSWlHLE1BQU10RixNQUFqQixFQUF5QjtnQkFDYndwQixnQkFBZ0Jsa0IsTUFBTWpHLENBQU4sQ0FBaEIsRUFBMEJzTCxLQUExQixDQUFnQyxHQUFoQyxDQUFSO1lBQ0lBLE1BQU0zSyxNQUFWO2VBQ093cEIsZ0JBQWdCbGtCLE1BQU1qRyxJQUFJLENBQVYsQ0FBaEIsQ0FBUDtlQUNPcXFCLE9BQU9BLEtBQUsvZSxLQUFMLENBQVcsR0FBWCxDQUFQLEdBQXlCLElBQWhDO2VBQ083QixJQUFJLENBQVgsRUFBYztxQkFDRDZnQixXQUFXaGYsTUFBTTBNLEtBQU4sQ0FBWSxDQUFaLEVBQWV2TyxDQUFmLEVBQWtCd08sSUFBbEIsQ0FBdUIsR0FBdkIsQ0FBWCxDQUFUO2dCQUNJN0UsTUFBSixFQUFZO3VCQUNEQSxNQUFQOztnQkFFQWlYLFFBQVFBLEtBQUsxcEIsTUFBTCxJQUFlOEksQ0FBdkIsSUFBNEJ1TixjQUFjMUwsS0FBZCxFQUFxQitlLElBQXJCLEVBQTJCLElBQTNCLEtBQW9DNWdCLElBQUksQ0FBeEUsRUFBMkU7Ozs7Ozs7O1dBUTVFLElBQVA7OztBQUdKLFNBQVM2Z0IsVUFBVCxDQUFvQnZrQixJQUFwQixFQUEwQjtRQUNsQndrQixZQUFZLElBQWhCOztRQUVJLENBQUNQLFFBQVFqa0IsSUFBUixDQUFELElBQW1CLE9BQU95a0IsTUFBUCxLQUFrQixXQUFyQyxJQUNJQSxNQURKLElBQ2NBLE9BQU9DLE9BRHpCLEVBQ2tDO1lBQzFCO3dCQUNZUCxhQUFhUSxLQUF6QjtvQkFDUSxjQUFjM2tCLElBQXRCOzs7K0JBR21Cd2tCLFNBQW5CO1NBTEosQ0FNRSxPQUFPM3RCLENBQVAsRUFBVTs7V0FFVG90QixRQUFRamtCLElBQVIsQ0FBUDs7Ozs7O0FBTUosQUFBTyxTQUFTNGtCLGtCQUFULENBQTZCNXBCLEdBQTdCLEVBQWtDNnBCLE1BQWxDLEVBQTBDO1FBQ3pDcnRCLElBQUo7UUFDSXdELEdBQUosRUFBUztZQUNEd1UsWUFBWXFWLE1BQVosQ0FBSixFQUF5QjttQkFDZEMsVUFBVTlwQixHQUFWLENBQVA7U0FESixNQUdLO21CQUNNK3BCLGFBQWEvcEIsR0FBYixFQUFrQjZwQixNQUFsQixDQUFQOzs7WUFHQXJ0QixJQUFKLEVBQVU7OzJCQUVTQSxJQUFmOzs7O1dBSUQyc0IsYUFBYVEsS0FBcEI7OztBQUdKLEFBQU8sU0FBU0ksWUFBVCxDQUF1Qi9rQixJQUF2QixFQUE2QnhCLE1BQTdCLEVBQXFDO1FBQ3BDQSxXQUFXLElBQWYsRUFBcUI7WUFDYnVVLGVBQWVpUixVQUFuQjtlQUNPZ0IsSUFBUCxHQUFjaGxCLElBQWQ7WUFDSWlrQixRQUFRamtCLElBQVIsS0FBaUIsSUFBckIsRUFBMkI7NEJBQ1Asc0JBQWhCLEVBQ1EsMkRBQ0Esc0RBREEsR0FFQSx3REFGQSxHQUdBLHlFQUpSOzJCQUtlaWtCLFFBQVFqa0IsSUFBUixFQUFjMFMsT0FBN0I7U0FOSixNQU9PLElBQUlsVSxPQUFPeW1CLFlBQVAsSUFBdUIsSUFBM0IsRUFBaUM7Z0JBQ2hDaEIsUUFBUXpsQixPQUFPeW1CLFlBQWYsS0FBZ0MsSUFBcEMsRUFBMEM7K0JBQ3ZCaEIsUUFBUXpsQixPQUFPeW1CLFlBQWYsRUFBNkJ2UyxPQUE1QzthQURKLE1BRU87b0JBQ0MsQ0FBQ3dSLGVBQWUxbEIsT0FBT3ltQixZQUF0QixDQUFMLEVBQTBDO21DQUN2QnptQixPQUFPeW1CLFlBQXRCLElBQXNDLEVBQXRDOzsrQkFFV3ptQixPQUFPeW1CLFlBQXRCLEVBQW9DanVCLElBQXBDLENBQXlDOzBCQUMvQmdKLElBRCtCOzRCQUU3QnhCO2lCQUZaO3VCQUlPLElBQVA7OztnQkFHQXdCLElBQVIsSUFBZ0IsSUFBSWlULE1BQUosQ0FBV0gsYUFBYUMsWUFBYixFQUEyQnZVLE1BQTNCLENBQVgsQ0FBaEI7O1lBRUkwbEIsZUFBZWxrQixJQUFmLENBQUosRUFBMEI7MkJBQ1BBLElBQWYsRUFBcUJwQixPQUFyQixDQUE2QixVQUFVMUYsQ0FBVixFQUFhOzZCQUN6QkEsRUFBRThHLElBQWYsRUFBcUI5RyxFQUFFc0YsTUFBdkI7YUFESjs7Ozs7OzJCQVFld0IsSUFBbkI7O2VBR09pa0IsUUFBUWprQixJQUFSLENBQVA7S0F0Q0osTUF1Q087O2VBRUlpa0IsUUFBUWprQixJQUFSLENBQVA7ZUFDTyxJQUFQOzs7O0FBSVIsQUFBTyxTQUFTa2xCLFlBQVQsQ0FBc0JsbEIsSUFBdEIsRUFBNEJ4QixNQUE1QixFQUFvQztRQUNuQ0EsVUFBVSxJQUFkLEVBQW9CO1lBQ1o2TyxNQUFKO1lBQVkwRixlQUFlaVIsVUFBM0I7O1lBRUlDLFFBQVFqa0IsSUFBUixLQUFpQixJQUFyQixFQUEyQjsyQkFDUmlrQixRQUFRamtCLElBQVIsRUFBYzBTLE9BQTdCOztpQkFFS0ksYUFBYUMsWUFBYixFQUEyQnZVLE1BQTNCLENBQVQ7aUJBQ1MsSUFBSXlVLE1BQUosQ0FBV3pVLE1BQVgsQ0FBVDtlQUNPeW1CLFlBQVAsR0FBc0JoQixRQUFRamtCLElBQVIsQ0FBdEI7Z0JBQ1FBLElBQVIsSUFBZ0JxTixNQUFoQjs7OzJCQUdtQnJOLElBQW5CO0tBWkosTUFhTzs7WUFFQ2lrQixRQUFRamtCLElBQVIsS0FBaUIsSUFBckIsRUFBMkI7Z0JBQ25CaWtCLFFBQVFqa0IsSUFBUixFQUFjaWxCLFlBQWQsSUFBOEIsSUFBbEMsRUFBd0M7d0JBQzVCamxCLElBQVIsSUFBZ0Jpa0IsUUFBUWprQixJQUFSLEVBQWNpbEIsWUFBOUI7YUFESixNQUVPLElBQUloQixRQUFRamtCLElBQVIsS0FBaUIsSUFBckIsRUFBMkI7dUJBQ3ZCaWtCLFFBQVFqa0IsSUFBUixDQUFQOzs7O1dBSUxpa0IsUUFBUWprQixJQUFSLENBQVA7Ozs7QUFJSixBQUFPLFNBQVM4a0IsU0FBVCxDQUFvQjlwQixHQUFwQixFQUF5QjtRQUN4QnFTLE1BQUo7O1FBRUlyUyxPQUFPQSxJQUFJcVYsT0FBWCxJQUFzQnJWLElBQUlxVixPQUFKLENBQVlzVSxLQUF0QyxFQUE2QztjQUNuQzNwQixJQUFJcVYsT0FBSixDQUFZc1UsS0FBbEI7OztRQUdBLENBQUMzcEIsR0FBTCxFQUFVO2VBQ0NtcEIsWUFBUDs7O1FBR0EsQ0FBQ2xZLFFBQVFqUixHQUFSLENBQUwsRUFBbUI7O2lCQUVOdXBCLFdBQVd2cEIsR0FBWCxDQUFUO1lBQ0lxUyxNQUFKLEVBQVk7bUJBQ0RBLE1BQVA7O2NBRUUsQ0FBQ3JTLEdBQUQsQ0FBTjs7O1dBR0dxcEIsYUFBYXJwQixHQUFiLENBQVA7OztBQUdKLEFBQU8sU0FBU21xQixXQUFULEdBQXVCO1dBQ25CdHZCLEtBQUtvdUIsT0FBTCxDQUFQOzs7QUNwTFcsU0FBU21CLGFBQVQsQ0FBd0J6WCxDQUF4QixFQUEyQjtRQUNsQ2EsUUFBSjtRQUNJcFIsSUFBSXVRLEVBQUV1TSxFQUFWOztRQUVJOWMsS0FBS3NRLGdCQUFnQkMsQ0FBaEIsRUFBbUJhLFFBQW5CLEtBQWdDLENBQUMsQ0FBMUMsRUFBNkM7bUJBRXJDcFIsRUFBRWdkLEtBQUYsSUFBaUIsQ0FBakIsSUFBc0JoZCxFQUFFZ2QsS0FBRixJQUFpQixFQUF2QyxHQUE2Q0EsS0FBN0MsR0FDQWhkLEVBQUVpZCxJQUFGLElBQWlCLENBQWpCLElBQXNCamQsRUFBRWlkLElBQUYsSUFBaUJRLFlBQVl6ZCxFQUFFK2MsSUFBRixDQUFaLEVBQXFCL2MsRUFBRWdkLEtBQUYsQ0FBckIsQ0FBdkMsR0FBd0VDLElBQXhFLEdBQ0FqZCxFQUFFa2QsSUFBRixJQUFpQixDQUFqQixJQUFzQmxkLEVBQUVrZCxJQUFGLElBQWlCLEVBQXZDLElBQThDbGQsRUFBRWtkLElBQUYsTUFBWSxFQUFaLEtBQW1CbGQsRUFBRW1kLE1BQUYsTUFBYyxDQUFkLElBQW1CbmQsRUFBRW9kLE1BQUYsTUFBYyxDQUFqQyxJQUFzQ3BkLEVBQUVxZCxXQUFGLE1BQW1CLENBQTVFLENBQTlDLEdBQWdJSCxJQUFoSSxHQUNBbGQsRUFBRW1kLE1BQUYsSUFBaUIsQ0FBakIsSUFBc0JuZCxFQUFFbWQsTUFBRixJQUFpQixFQUF2QyxHQUE2Q0EsTUFBN0MsR0FDQW5kLEVBQUVvZCxNQUFGLElBQWlCLENBQWpCLElBQXNCcGQsRUFBRW9kLE1BQUYsSUFBaUIsRUFBdkMsR0FBNkNBLE1BQTdDLEdBQ0FwZCxFQUFFcWQsV0FBRixJQUFpQixDQUFqQixJQUFzQnJkLEVBQUVxZCxXQUFGLElBQWlCLEdBQXZDLEdBQTZDQSxXQUE3QyxHQUNBLENBQUMsQ0FQTDs7WUFTSS9NLGdCQUFnQkMsQ0FBaEIsRUFBbUIwWCxrQkFBbkIsS0FBMEM3VyxXQUFXMkwsSUFBWCxJQUFtQjNMLFdBQVc2TCxJQUF4RSxDQUFKLEVBQW1GO3VCQUNwRUEsSUFBWDs7WUFFQTNNLGdCQUFnQkMsQ0FBaEIsRUFBbUIyWCxjQUFuQixJQUFxQzlXLGFBQWEsQ0FBQyxDQUF2RCxFQUEwRDt1QkFDM0NrTSxJQUFYOztZQUVBaE4sZ0JBQWdCQyxDQUFoQixFQUFtQjRYLGdCQUFuQixJQUF1Qy9XLGFBQWEsQ0FBQyxDQUF6RCxFQUE0RDt1QkFDN0NtTSxPQUFYOzs7d0JBR1loTixDQUFoQixFQUFtQmEsUUFBbkIsR0FBOEJBLFFBQTlCOzs7V0FHR2IsQ0FBUDs7O0FDMUJKOztBQUVBLElBQUk2WCxtQkFBbUIsa0pBQXZCO0FBQ0EsSUFBSUMsZ0JBQWdCLDZJQUFwQjs7QUFFQSxJQUFJQyxVQUFVLHVCQUFkOztBQUVBLElBQUlDLFdBQVcsQ0FDWCxDQUFDLGNBQUQsRUFBaUIscUJBQWpCLENBRFcsRUFFWCxDQUFDLFlBQUQsRUFBZSxpQkFBZixDQUZXLEVBR1gsQ0FBQyxjQUFELEVBQWlCLGdCQUFqQixDQUhXLEVBSVgsQ0FBQyxZQUFELEVBQWUsYUFBZixFQUE4QixLQUE5QixDQUpXLEVBS1gsQ0FBQyxVQUFELEVBQWEsYUFBYixDQUxXLEVBTVgsQ0FBQyxTQUFELEVBQVksWUFBWixFQUEwQixLQUExQixDQU5XLEVBT1gsQ0FBQyxZQUFELEVBQWUsWUFBZixDQVBXLEVBUVgsQ0FBQyxVQUFELEVBQWEsT0FBYixDQVJXOztBQVVYLENBQUMsWUFBRCxFQUFlLGFBQWYsQ0FWVyxFQVdYLENBQUMsV0FBRCxFQUFjLGFBQWQsRUFBNkIsS0FBN0IsQ0FYVyxFQVlYLENBQUMsU0FBRCxFQUFZLE9BQVosQ0FaVyxDQUFmOzs7QUFnQkEsSUFBSUMsV0FBVyxDQUNYLENBQUMsZUFBRCxFQUFrQixxQkFBbEIsQ0FEVyxFQUVYLENBQUMsZUFBRCxFQUFrQixvQkFBbEIsQ0FGVyxFQUdYLENBQUMsVUFBRCxFQUFhLGdCQUFiLENBSFcsRUFJWCxDQUFDLE9BQUQsRUFBVSxXQUFWLENBSlcsRUFLWCxDQUFDLGFBQUQsRUFBZ0IsbUJBQWhCLENBTFcsRUFNWCxDQUFDLGFBQUQsRUFBZ0Isa0JBQWhCLENBTlcsRUFPWCxDQUFDLFFBQUQsRUFBVyxjQUFYLENBUFcsRUFRWCxDQUFDLE1BQUQsRUFBUyxVQUFULENBUlcsRUFTWCxDQUFDLElBQUQsRUFBTyxNQUFQLENBVFcsQ0FBZjs7QUFZQSxJQUFJQyxrQkFBa0IscUJBQXRCOzs7QUFHQSxBQUFPLFNBQVNDLGFBQVQsQ0FBdUJ0bkIsTUFBdkIsRUFBK0I7UUFDOUJ2RSxDQUFKO1FBQU9xSyxDQUFQO1FBQ0lrUSxTQUFTaFcsT0FBT3VSLEVBRHBCO1FBRUl1SCxRQUFRa08saUJBQWlCTyxJQUFqQixDQUFzQnZSLE1BQXRCLEtBQWlDaVIsY0FBY00sSUFBZCxDQUFtQnZSLE1BQW5CLENBRjdDO1FBR0l3UixTQUhKO1FBR2VDLFVBSGY7UUFHMkJDLFVBSDNCO1FBR3VDQyxRQUh2Qzs7UUFLSTdPLEtBQUosRUFBVzt3QkFDUzlZLE1BQWhCLEVBQXdCNG5CLEdBQXhCLEdBQThCLElBQTlCOzthQUVLbnNCLElBQUksQ0FBSixFQUFPcUssSUFBSXFoQixTQUFTL3FCLE1BQXpCLEVBQWlDWCxJQUFJcUssQ0FBckMsRUFBd0NySyxHQUF4QyxFQUE2QztnQkFDckMwckIsU0FBUzFyQixDQUFULEVBQVksQ0FBWixFQUFlOHJCLElBQWYsQ0FBb0J6TyxNQUFNLENBQU4sQ0FBcEIsQ0FBSixFQUFtQzs2QkFDbEJxTyxTQUFTMXJCLENBQVQsRUFBWSxDQUFaLENBQWI7NEJBQ1kwckIsU0FBUzFyQixDQUFULEVBQVksQ0FBWixNQUFtQixLQUEvQjs7OztZQUlKZ3NCLGNBQWMsSUFBbEIsRUFBd0I7bUJBQ2JoWSxRQUFQLEdBQWtCLEtBQWxCOzs7WUFHQXFKLE1BQU0sQ0FBTixDQUFKLEVBQWM7aUJBQ0xyZCxJQUFJLENBQUosRUFBT3FLLElBQUlzaEIsU0FBU2hyQixNQUF6QixFQUFpQ1gsSUFBSXFLLENBQXJDLEVBQXdDckssR0FBeEMsRUFBNkM7b0JBQ3JDMnJCLFNBQVMzckIsQ0FBVCxFQUFZLENBQVosRUFBZThyQixJQUFmLENBQW9Cek8sTUFBTSxDQUFOLENBQXBCLENBQUosRUFBbUM7O2lDQUVsQixDQUFDQSxNQUFNLENBQU4sS0FBWSxHQUFiLElBQW9Cc08sU0FBUzNyQixDQUFULEVBQVksQ0FBWixDQUFqQzs7OztnQkFJSmlzQixjQUFjLElBQWxCLEVBQXdCO3VCQUNialksUUFBUCxHQUFrQixLQUFsQjs7OztZQUlKLENBQUMrWCxTQUFELElBQWNFLGNBQWMsSUFBaEMsRUFBc0M7bUJBQzNCalksUUFBUCxHQUFrQixLQUFsQjs7O1lBR0FxSixNQUFNLENBQU4sQ0FBSixFQUFjO2dCQUNOb08sUUFBUUssSUFBUixDQUFhek8sTUFBTSxDQUFOLENBQWIsQ0FBSixFQUE0QjsyQkFDYixHQUFYO2FBREosTUFFTzt1QkFDSXJKLFFBQVAsR0FBa0IsS0FBbEI7Ozs7ZUFJRCtCLEVBQVAsR0FBWWlXLGNBQWNDLGNBQWMsRUFBNUIsS0FBbUNDLFlBQVksRUFBL0MsQ0FBWjtrQ0FDMEIzbkIsTUFBMUI7S0F4Q0osTUF5Q087ZUFDSXlQLFFBQVAsR0FBa0IsS0FBbEI7Ozs7O0FBS1IsQUFBTyxTQUFTb1ksZ0JBQVQsQ0FBMEI3bkIsTUFBMUIsRUFBa0M7UUFDakNnYixVQUFVcU0sZ0JBQWdCRSxJQUFoQixDQUFxQnZuQixPQUFPdVIsRUFBNUIsQ0FBZDs7UUFFSXlKLFlBQVksSUFBaEIsRUFBc0I7ZUFDWGxMLEVBQVAsR0FBWSxJQUFJM0IsSUFBSixDQUFTLENBQUM2TSxRQUFRLENBQVIsQ0FBVixDQUFaOzs7O2tCQUlVaGIsTUFBZDtRQUNJQSxPQUFPeVAsUUFBUCxLQUFvQixLQUF4QixFQUErQjtlQUNwQnpQLE9BQU95UCxRQUFkO2NBQ01xWSx1QkFBTixDQUE4QjluQixNQUE5Qjs7OztBQUlSb04sTUFBTTBhLHVCQUFOLEdBQWdDM1UsVUFDNUIsb0dBQ0EsbUZBREEsR0FFQSxnRkFGQSxHQUdBLCtEQUo0QixFQUs1QixVQUFVblQsTUFBVixFQUFrQjtXQUNQOFAsRUFBUCxHQUFZLElBQUkzQixJQUFKLENBQVNuTyxPQUFPdVIsRUFBUCxJQUFhdlIsT0FBTytuQixPQUFQLEdBQWlCLE1BQWpCLEdBQTBCLEVBQXZDLENBQVQsQ0FBWjtDQU53QixDQUFoQzs7QUMvR0E7QUFDQSxBQUFlLFNBQVNDLFVBQVQsQ0FBa0JwcEIsQ0FBbEIsRUFBcUJDLENBQXJCLEVBQXdCd0MsQ0FBeEIsRUFBMkI7UUFDbEN6QyxLQUFLLElBQVQsRUFBZTtlQUNKQSxDQUFQOztRQUVBQyxLQUFLLElBQVQsRUFBZTtlQUNKQSxDQUFQOztXQUVHd0MsQ0FBUDs7O0FDQ0osU0FBUzRtQixnQkFBVCxDQUEwQmpvQixNQUExQixFQUFrQzs7UUFFMUJrb0IsV0FBVyxJQUFJL1osSUFBSixDQUFTZixNQUFNeUgsR0FBTixFQUFULENBQWY7UUFDSTdVLE9BQU8rbkIsT0FBWCxFQUFvQjtlQUNULENBQUNHLFNBQVNySSxjQUFULEVBQUQsRUFBNEJxSSxTQUFTQyxXQUFULEVBQTVCLEVBQW9ERCxTQUFTekwsVUFBVCxFQUFwRCxDQUFQOztXQUVHLENBQUN5TCxTQUFTeEksV0FBVCxFQUFELEVBQXlCd0ksU0FBU0UsUUFBVCxFQUF6QixFQUE4Q0YsU0FBU0csT0FBVCxFQUE5QyxDQUFQOzs7Ozs7O0FBT0osQUFBTyxTQUFTQyxlQUFULENBQTBCdG9CLE1BQTFCLEVBQWtDO1FBQ2pDdkUsQ0FBSjtRQUFPMGlCLElBQVA7UUFBYXpRLFFBQVEsRUFBckI7UUFBeUI2YSxXQUF6QjtRQUFzQ0MsU0FBdEM7O1FBRUl4b0IsT0FBTzhQLEVBQVgsRUFBZTs7OztrQkFJRG1ZLGlCQUFpQmpvQixNQUFqQixDQUFkOzs7UUFHSUEsT0FBT3diLEVBQVAsSUFBYXhiLE9BQU8wYixFQUFQLENBQVVHLElBQVYsS0FBbUIsSUFBaEMsSUFBd0M3YixPQUFPMGIsRUFBUCxDQUFVRSxLQUFWLEtBQW9CLElBQWhFLEVBQXNFOzhCQUM1QzViLE1BQXRCOzs7O1FBSUFBLE9BQU95b0IsVUFBWCxFQUF1QjtvQkFDUFQsV0FBU2hvQixPQUFPMGIsRUFBUCxDQUFVQyxJQUFWLENBQVQsRUFBMEI0TSxZQUFZNU0sSUFBWixDQUExQixDQUFaOztZQUVJM2IsT0FBT3lvQixVQUFQLEdBQW9CdEosV0FBV3FKLFNBQVgsQ0FBeEIsRUFBK0M7NEJBQzNCeG9CLE1BQWhCLEVBQXdCNm1CLGtCQUF4QixHQUE2QyxJQUE3Qzs7O2VBR0dqSCxjQUFjNEksU0FBZCxFQUF5QixDQUF6QixFQUE0QnhvQixPQUFPeW9CLFVBQW5DLENBQVA7ZUFDTy9NLEVBQVAsQ0FBVUUsS0FBVixJQUFtQnVDLEtBQUtnSyxXQUFMLEVBQW5CO2VBQ096TSxFQUFQLENBQVVHLElBQVYsSUFBa0JzQyxLQUFLMUIsVUFBTCxFQUFsQjs7Ozs7Ozs7U0FRQ2hoQixJQUFJLENBQVQsRUFBWUEsSUFBSSxDQUFKLElBQVN1RSxPQUFPMGIsRUFBUCxDQUFVamdCLENBQVYsS0FBZ0IsSUFBckMsRUFBMkMsRUFBRUEsQ0FBN0MsRUFBZ0Q7ZUFDckNpZ0IsRUFBUCxDQUFVamdCLENBQVYsSUFBZWlTLE1BQU1qUyxDQUFOLElBQVc4c0IsWUFBWTlzQixDQUFaLENBQTFCOzs7O1dBSUdBLElBQUksQ0FBWCxFQUFjQSxHQUFkLEVBQW1CO2VBQ1JpZ0IsRUFBUCxDQUFVamdCLENBQVYsSUFBZWlTLE1BQU1qUyxDQUFOLElBQVl1RSxPQUFPMGIsRUFBUCxDQUFVamdCLENBQVYsS0FBZ0IsSUFBakIsR0FBMEJBLE1BQU0sQ0FBTixHQUFVLENBQVYsR0FBYyxDQUF4QyxHQUE2Q3VFLE9BQU8wYixFQUFQLENBQVVqZ0IsQ0FBVixDQUF2RTs7OztRQUlBdUUsT0FBTzBiLEVBQVAsQ0FBVUksSUFBVixNQUFvQixFQUFwQixJQUNJOWIsT0FBTzBiLEVBQVAsQ0FBVUssTUFBVixNQUFzQixDQUQxQixJQUVJL2IsT0FBTzBiLEVBQVAsQ0FBVU0sTUFBVixNQUFzQixDQUYxQixJQUdJaGMsT0FBTzBiLEVBQVAsQ0FBVU8sV0FBVixNQUEyQixDQUhuQyxFQUdzQztlQUMzQnlNLFFBQVAsR0FBa0IsSUFBbEI7ZUFDT2hOLEVBQVAsQ0FBVUksSUFBVixJQUFrQixDQUFsQjs7O1dBR0doTSxFQUFQLEdBQVksQ0FBQzlQLE9BQU8rbkIsT0FBUCxHQUFpQm5JLGFBQWpCLEdBQWlDTCxVQUFsQyxFQUE4Q2xTLEtBQTlDLENBQW9ELElBQXBELEVBQTBESyxLQUExRCxDQUFaOzs7UUFHSTFOLE9BQU8wUixJQUFQLElBQWUsSUFBbkIsRUFBeUI7ZUFDZDVCLEVBQVAsQ0FBVTZZLGFBQVYsQ0FBd0Izb0IsT0FBTzhQLEVBQVAsQ0FBVThZLGFBQVYsS0FBNEI1b0IsT0FBTzBSLElBQTNEOzs7UUFHQTFSLE9BQU8wb0IsUUFBWCxFQUFxQjtlQUNWaE4sRUFBUCxDQUFVSSxJQUFWLElBQWtCLEVBQWxCOzs7O0FBSVIsU0FBUytNLHFCQUFULENBQStCN29CLE1BQS9CLEVBQXVDO1FBQy9Ca0QsQ0FBSixFQUFPNGxCLFFBQVAsRUFBaUJ4SSxJQUFqQixFQUF1QkMsT0FBdkIsRUFBZ0NQLEdBQWhDLEVBQXFDQyxHQUFyQyxFQUEwQzhJLElBQTFDLEVBQWdEQyxlQUFoRDs7UUFFSWhwQixPQUFPd2IsRUFBWDtRQUNJdFksRUFBRStsQixFQUFGLElBQVEsSUFBUixJQUFnQi9sQixFQUFFZ21CLENBQUYsSUFBTyxJQUF2QixJQUErQmhtQixFQUFFaW1CLENBQUYsSUFBTyxJQUExQyxFQUFnRDtjQUN0QyxDQUFOO2NBQ00sQ0FBTjs7Ozs7O21CQU1XbkIsV0FBUzlrQixFQUFFK2xCLEVBQVgsRUFBZWpwQixPQUFPMGIsRUFBUCxDQUFVQyxJQUFWLENBQWYsRUFBZ0NrRixXQUFXdUksYUFBWCxFQUEwQixDQUExQixFQUE2QixDQUE3QixFQUFnQzlNLElBQWhFLENBQVg7ZUFDTzBMLFdBQVM5a0IsRUFBRWdtQixDQUFYLEVBQWMsQ0FBZCxDQUFQO2tCQUNVbEIsV0FBUzlrQixFQUFFaW1CLENBQVgsRUFBYyxDQUFkLENBQVY7WUFDSTVJLFVBQVUsQ0FBVixJQUFlQSxVQUFVLENBQTdCLEVBQWdDOzhCQUNWLElBQWxCOztLQVpSLE1BY087Y0FDR3ZnQixPQUFPNlIsT0FBUCxDQUFlcVAsS0FBZixDQUFxQmxCLEdBQTNCO2NBQ01oZ0IsT0FBTzZSLE9BQVAsQ0FBZXFQLEtBQWYsQ0FBcUJqQixHQUEzQjs7WUFFSW9KLFVBQVV4SSxXQUFXdUksYUFBWCxFQUEwQnBKLEdBQTFCLEVBQStCQyxHQUEvQixDQUFkOzttQkFFVytILFdBQVM5a0IsRUFBRW9tQixFQUFYLEVBQWV0cEIsT0FBTzBiLEVBQVAsQ0FBVUMsSUFBVixDQUFmLEVBQWdDME4sUUFBUS9NLElBQXhDLENBQVg7OztlQUdPMEwsV0FBUzlrQixFQUFFQSxDQUFYLEVBQWNtbUIsUUFBUS9JLElBQXRCLENBQVA7O1lBRUlwZCxFQUFFNUosQ0FBRixJQUFPLElBQVgsRUFBaUI7O3NCQUVINEosRUFBRTVKLENBQVo7Z0JBQ0lpbkIsVUFBVSxDQUFWLElBQWVBLFVBQVUsQ0FBN0IsRUFBZ0M7a0NBQ1YsSUFBbEI7O1NBSlIsTUFNTyxJQUFJcmQsRUFBRTdLLENBQUYsSUFBTyxJQUFYLEVBQWlCOztzQkFFVjZLLEVBQUU3SyxDQUFGLEdBQU0ybkIsR0FBaEI7Z0JBQ0k5YyxFQUFFN0ssQ0FBRixHQUFNLENBQU4sSUFBVzZLLEVBQUU3SyxDQUFGLEdBQU0sQ0FBckIsRUFBd0I7a0NBQ0YsSUFBbEI7O1NBSkQsTUFNQTs7c0JBRU8ybkIsR0FBVjs7O1FBR0pNLE9BQU8sQ0FBUCxJQUFZQSxPQUFPUyxZQUFZK0gsUUFBWixFQUFzQjlJLEdBQXRCLEVBQTJCQyxHQUEzQixDQUF2QixFQUF3RDt3QkFDcENqZ0IsTUFBaEIsRUFBd0I4bUIsY0FBeEIsR0FBeUMsSUFBekM7S0FESixNQUVPLElBQUlrQyxtQkFBbUIsSUFBdkIsRUFBNkI7d0JBQ2hCaHBCLE1BQWhCLEVBQXdCK21CLGdCQUF4QixHQUEyQyxJQUEzQztLQURHLE1BRUE7ZUFDSTFHLG1CQUFtQnlJLFFBQW5CLEVBQTZCeEksSUFBN0IsRUFBbUNDLE9BQW5DLEVBQTRDUCxHQUE1QyxFQUFpREMsR0FBakQsQ0FBUDtlQUNPdkUsRUFBUCxDQUFVQyxJQUFWLElBQWtCb04sS0FBS3pNLElBQXZCO2VBQ09tTSxVQUFQLEdBQW9CTSxLQUFLckksU0FBekI7Ozs7QUMvSFI7QUFDQXRULE1BQU1tYyxRQUFOLEdBQWlCLFlBQVksRUFBN0I7OztBQUdBLEFBQU8sU0FBU0MseUJBQVQsQ0FBbUN4cEIsTUFBbkMsRUFBMkM7O1FBRTFDQSxPQUFPd1IsRUFBUCxLQUFjcEUsTUFBTW1jLFFBQXhCLEVBQWtDO3NCQUNoQnZwQixNQUFkOzs7O1dBSUcwYixFQUFQLEdBQVksRUFBWjtvQkFDZ0IxYixNQUFoQixFQUF3QmlRLEtBQXhCLEdBQWdDLElBQWhDOzs7UUFHSStGLFNBQVMsS0FBS2hXLE9BQU91UixFQUF6QjtRQUNJOVYsQ0FESjtRQUNPZ3VCLFdBRFA7UUFDb0JwTyxNQURwQjtRQUM0QjNDLEtBRDVCO1FBQ21DZ1IsT0FEbkM7UUFFSUMsZUFBZTNULE9BQU81WixNQUYxQjtRQUdJd3RCLHlCQUF5QixDQUg3Qjs7YUFLUzFRLGFBQWFsWixPQUFPd1IsRUFBcEIsRUFBd0J4UixPQUFPNlIsT0FBL0IsRUFBd0NpSCxLQUF4QyxDQUE4Q1QsZ0JBQTlDLEtBQW1FLEVBQTVFOztTQUVLNWMsSUFBSSxDQUFULEVBQVlBLElBQUk0ZixPQUFPamYsTUFBdkIsRUFBK0JYLEdBQS9CLEVBQW9DO2dCQUN4QjRmLE9BQU81ZixDQUFQLENBQVI7c0JBQ2MsQ0FBQ3VhLE9BQU84QyxLQUFQLENBQWE4QixzQkFBc0JsQyxLQUF0QixFQUE2QjFZLE1BQTdCLENBQWIsS0FBc0QsRUFBdkQsRUFBMkQsQ0FBM0QsQ0FBZDs7O1lBR0l5cEIsV0FBSixFQUFpQjtzQkFDSHpULE9BQU94UCxNQUFQLENBQWMsQ0FBZCxFQUFpQndQLE9BQU96UCxPQUFQLENBQWVrakIsV0FBZixDQUFqQixDQUFWO2dCQUNJQyxRQUFRdHRCLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7Z0NBQ0o0RCxNQUFoQixFQUF3QjZwQixXQUF4QixDQUFvQ3J4QixJQUFwQyxDQUF5Q2t4QixPQUF6Qzs7cUJBRUsxVCxPQUFPdkMsS0FBUCxDQUFhdUMsT0FBT3pQLE9BQVAsQ0FBZWtqQixXQUFmLElBQThCQSxZQUFZcnRCLE1BQXZELENBQVQ7c0NBQzBCcXRCLFlBQVlydEIsTUFBdEM7OztZQUdBb2MscUJBQXFCRSxLQUFyQixDQUFKLEVBQWlDO2dCQUN6QitRLFdBQUosRUFBaUI7Z0NBQ0d6cEIsTUFBaEIsRUFBd0JpUSxLQUF4QixHQUFnQyxLQUFoQzthQURKLE1BR0s7Z0NBQ2VqUSxNQUFoQixFQUF3QjBRLFlBQXhCLENBQXFDbFksSUFBckMsQ0FBMENrZ0IsS0FBMUM7O29DQUVvQkEsS0FBeEIsRUFBK0IrUSxXQUEvQixFQUE0Q3pwQixNQUE1QztTQVBKLE1BU0ssSUFBSUEsT0FBT3dRLE9BQVAsSUFBa0IsQ0FBQ2laLFdBQXZCLEVBQW9DOzRCQUNyQnpwQixNQUFoQixFQUF3QjBRLFlBQXhCLENBQXFDbFksSUFBckMsQ0FBMENrZ0IsS0FBMUM7Ozs7O29CQUtRMVksTUFBaEIsRUFBd0J5USxhQUF4QixHQUF3Q2taLGVBQWVDLHNCQUF2RDtRQUNJNVQsT0FBTzVaLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7d0JBQ0g0RCxNQUFoQixFQUF3QjZwQixXQUF4QixDQUFvQ3J4QixJQUFwQyxDQUF5Q3dkLE1BQXpDOzs7O1FBSUFoVyxPQUFPMGIsRUFBUCxDQUFVSSxJQUFWLEtBQW1CLEVBQW5CLElBQ0E1TSxnQkFBZ0JsUCxNQUFoQixFQUF3QjJRLE9BQXhCLEtBQW9DLElBRHBDLElBRUEzUSxPQUFPMGIsRUFBUCxDQUFVSSxJQUFWLElBQWtCLENBRnRCLEVBRXlCO3dCQUNMOWIsTUFBaEIsRUFBd0IyUSxPQUF4QixHQUFrQ0MsU0FBbEM7OztvQkFHWTVRLE1BQWhCLEVBQXdCNFAsZUFBeEIsR0FBMEM1UCxPQUFPMGIsRUFBUCxDQUFVakksS0FBVixDQUFnQixDQUFoQixDQUExQztvQkFDZ0J6VCxNQUFoQixFQUF3QnVRLFFBQXhCLEdBQW1DdlEsT0FBTytrQixTQUExQzs7V0FFT3JKLEVBQVAsQ0FBVUksSUFBVixJQUFrQmdPLGdCQUFnQjlwQixPQUFPNlIsT0FBdkIsRUFBZ0M3UixPQUFPMGIsRUFBUCxDQUFVSSxJQUFWLENBQWhDLEVBQWlEOWIsT0FBTytrQixTQUF4RCxDQUFsQjs7b0JBRWdCL2tCLE1BQWhCO2tCQUNjQSxNQUFkOzs7QUFJSixTQUFTOHBCLGVBQVQsQ0FBMEJqYixNQUExQixFQUFrQ2tiLElBQWxDLEVBQXdDeFosUUFBeEMsRUFBa0Q7UUFDMUN5WixJQUFKOztRQUVJelosWUFBWSxJQUFoQixFQUFzQjs7ZUFFWHdaLElBQVA7O1FBRUFsYixPQUFPb2IsWUFBUCxJQUF1QixJQUEzQixFQUFpQztlQUN0QnBiLE9BQU9vYixZQUFQLENBQW9CRixJQUFwQixFQUEwQnhaLFFBQTFCLENBQVA7S0FESixNQUVPLElBQUkxQixPQUFPaVcsSUFBUCxJQUFlLElBQW5CLEVBQXlCOztlQUVyQmpXLE9BQU9pVyxJQUFQLENBQVl2VSxRQUFaLENBQVA7WUFDSXlaLFFBQVFELE9BQU8sRUFBbkIsRUFBdUI7b0JBQ1gsRUFBUjs7WUFFQSxDQUFDQyxJQUFELElBQVNELFNBQVMsRUFBdEIsRUFBMEI7bUJBQ2YsQ0FBUDs7ZUFFR0EsSUFBUDtLQVRHLE1BVUE7O2VBRUlBLElBQVA7Ozs7QUNsR1I7QUFDQSxBQUFPLFNBQVNHLHdCQUFULENBQWtDbHFCLE1BQWxDLEVBQTBDO1FBQ3pDbXFCLFVBQUosRUFDSUMsVUFESixFQUdJQyxXQUhKLEVBSUk1dUIsQ0FKSixFQUtJNnVCLFlBTEo7O1FBT0l0cUIsT0FBT3dSLEVBQVAsQ0FBVXBWLE1BQVYsS0FBcUIsQ0FBekIsRUFBNEI7d0JBQ1I0RCxNQUFoQixFQUF3QnFRLGFBQXhCLEdBQXdDLElBQXhDO2VBQ09QLEVBQVAsR0FBWSxJQUFJM0IsSUFBSixDQUFTNEMsR0FBVCxDQUFaOzs7O1NBSUN0VixJQUFJLENBQVQsRUFBWUEsSUFBSXVFLE9BQU93UixFQUFQLENBQVVwVixNQUExQixFQUFrQ1gsR0FBbEMsRUFBdUM7dUJBQ3BCLENBQWY7cUJBQ2F5VixXQUFXLEVBQVgsRUFBZWxSLE1BQWYsQ0FBYjtZQUNJQSxPQUFPK25CLE9BQVAsSUFBa0IsSUFBdEIsRUFBNEI7dUJBQ2JBLE9BQVgsR0FBcUIvbkIsT0FBTytuQixPQUE1Qjs7bUJBRU92VyxFQUFYLEdBQWdCeFIsT0FBT3dSLEVBQVAsQ0FBVS9WLENBQVYsQ0FBaEI7a0NBQzBCMHVCLFVBQTFCOztZQUVJLENBQUMzYSxRQUFRMmEsVUFBUixDQUFMLEVBQTBCOzs7Ozt3QkFLVmpiLGdCQUFnQmliLFVBQWhCLEVBQTRCMVosYUFBNUM7Ozt3QkFHZ0J2QixnQkFBZ0JpYixVQUFoQixFQUE0QnpaLFlBQTVCLENBQXlDdFUsTUFBekMsR0FBa0QsRUFBbEU7O3dCQUVnQit0QixVQUFoQixFQUE0QkksS0FBNUIsR0FBb0NELFlBQXBDOztZQUVJRCxlQUFlLElBQWYsSUFBdUJDLGVBQWVELFdBQTFDLEVBQXVEOzBCQUNyQ0MsWUFBZDt5QkFDYUgsVUFBYjs7OztXQUlEbnFCLE1BQVAsRUFBZW9xQixjQUFjRCxVQUE3Qjs7O0FDNUNHLFNBQVNLLGdCQUFULENBQTBCeHFCLE1BQTFCLEVBQWtDO1FBQ2pDQSxPQUFPOFAsRUFBWCxFQUFlOzs7O1FBSVhyVSxJQUFJb2IscUJBQXFCN1csT0FBT3VSLEVBQTVCLENBQVI7V0FDT21LLEVBQVAsR0FBWXROLElBQUksQ0FBQzNTLEVBQUU2Z0IsSUFBSCxFQUFTN2dCLEVBQUU4Z0IsS0FBWCxFQUFrQjlnQixFQUFFNG1CLEdBQUYsSUFBUzVtQixFQUFFMGlCLElBQTdCLEVBQW1DMWlCLEVBQUVzdUIsSUFBckMsRUFBMkN0dUIsRUFBRWd2QixNQUE3QyxFQUFxRGh2QixFQUFFaXZCLE1BQXZELEVBQStEanZCLEVBQUVrdkIsV0FBakUsQ0FBSixFQUFtRixVQUFVbGhCLEdBQVYsRUFBZTtlQUNuR0EsT0FBT3lWLFNBQVN6VixHQUFULEVBQWMsRUFBZCxDQUFkO0tBRFEsQ0FBWjs7b0JBSWdCekosTUFBaEI7OztBQ0tKLFNBQVM0cUIsZ0JBQVQsQ0FBMkI1cUIsTUFBM0IsRUFBbUM7UUFDM0J1TyxNQUFNLElBQUl3RCxNQUFKLENBQVc2VSxjQUFjaUUsY0FBYzdxQixNQUFkLENBQWQsQ0FBWCxDQUFWO1FBQ0l1TyxJQUFJbWEsUUFBUixFQUFrQjs7WUFFVm5ILEdBQUosQ0FBUSxDQUFSLEVBQVcsR0FBWDtZQUNJbUgsUUFBSixHQUFlOVgsU0FBZjs7O1dBR0dyQyxHQUFQOzs7QUFHSixBQUFPLFNBQVNzYyxhQUFULENBQXdCN3FCLE1BQXhCLEVBQWdDO1FBQy9CME4sUUFBUTFOLE9BQU91UixFQUFuQjtRQUNJM0MsU0FBUzVPLE9BQU93UixFQURwQjs7V0FHT0ssT0FBUCxHQUFpQjdSLE9BQU82UixPQUFQLElBQWtCeVUsVUFBVXRtQixPQUFPeVIsRUFBakIsQ0FBbkM7O1FBRUkvRCxVQUFVLElBQVYsSUFBbUJrQixXQUFXZ0MsU0FBWCxJQUF3QmxELFVBQVUsRUFBekQsRUFBOEQ7ZUFDbkRvRCxjQUFjLEVBQUNWLFdBQVcsSUFBWixFQUFkLENBQVA7OztRQUdBLE9BQU8xQyxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO2VBQ3BCNkQsRUFBUCxHQUFZN0QsUUFBUTFOLE9BQU82UixPQUFQLENBQWVpWixRQUFmLENBQXdCcGQsS0FBeEIsQ0FBcEI7OztRQUdBdUUsU0FBU3ZFLEtBQVQsQ0FBSixFQUFxQjtlQUNWLElBQUlxRSxNQUFKLENBQVc2VSxjQUFjbFosS0FBZCxDQUFYLENBQVA7S0FESixNQUVPLElBQUlRLE9BQU9SLEtBQVAsQ0FBSixFQUFtQjtlQUNmb0MsRUFBUCxHQUFZcEMsS0FBWjtLQURHLE1BRUEsSUFBSUQsUUFBUW1CLE1BQVIsQ0FBSixFQUFxQjtpQ0FDQzVPLE1BQXpCO0tBREcsTUFFQSxJQUFJNE8sTUFBSixFQUFZO2tDQUNXNU8sTUFBMUI7S0FERyxNQUVDO3dCQUNZQSxNQUFoQjs7O1FBR0EsQ0FBQ3dQLFFBQVF4UCxNQUFSLENBQUwsRUFBc0I7ZUFDWDhQLEVBQVAsR0FBWSxJQUFaOzs7V0FHRzlQLE1BQVA7OztBQUdKLFNBQVMrcUIsZUFBVCxDQUF5Qi9xQixNQUF6QixFQUFpQztRQUN6QjBOLFFBQVExTixPQUFPdVIsRUFBbkI7UUFDSTdELFVBQVVrRCxTQUFkLEVBQXlCO2VBQ2RkLEVBQVAsR0FBWSxJQUFJM0IsSUFBSixDQUFTZixNQUFNeUgsR0FBTixFQUFULENBQVo7S0FESixNQUVPLElBQUkzRyxPQUFPUixLQUFQLENBQUosRUFBbUI7ZUFDZm9DLEVBQVAsR0FBWSxJQUFJM0IsSUFBSixDQUFTVCxNQUFNZ0IsT0FBTixFQUFULENBQVo7S0FERyxNQUVBLElBQUksT0FBT2hCLEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7eUJBQ2pCMU4sTUFBakI7S0FERyxNQUVBLElBQUl5TixRQUFRQyxLQUFSLENBQUosRUFBb0I7ZUFDaEJnTyxFQUFQLEdBQVl0TixJQUFJVixNQUFNK0YsS0FBTixDQUFZLENBQVosQ0FBSixFQUFvQixVQUFVaEssR0FBVixFQUFlO21CQUNwQ3lWLFNBQVN6VixHQUFULEVBQWMsRUFBZCxDQUFQO1NBRFEsQ0FBWjt3QkFHZ0J6SixNQUFoQjtLQUpHLE1BS0EsSUFBSSxRQUFPME4sS0FBUCx5Q0FBT0EsS0FBUCxPQUFrQixRQUF0QixFQUFnQzt5QkFDbEIxTixNQUFqQjtLQURHLE1BRUEsSUFBSWlPLFNBQVNQLEtBQVQsQ0FBSixFQUFxQjs7ZUFFakJvQyxFQUFQLEdBQVksSUFBSTNCLElBQUosQ0FBU1QsS0FBVCxDQUFaO0tBRkcsTUFHQTtjQUNHb2EsdUJBQU4sQ0FBOEI5bkIsTUFBOUI7Ozs7QUFJUixBQUFPLFNBQVMrTyxnQkFBVCxDQUEyQnJCLEtBQTNCLEVBQWtDa0IsTUFBbEMsRUFBMENDLE1BQTFDLEVBQWtEQyxNQUFsRCxFQUEwRGtjLEtBQTFELEVBQWlFO1FBQ2hFM3BCLElBQUksRUFBUjs7UUFFSXdOLFdBQVcsSUFBWCxJQUFtQkEsV0FBVyxLQUFsQyxFQUF5QztpQkFDNUJBLE1BQVQ7aUJBQ1MrQixTQUFUOzs7UUFHQzlDLFNBQVNKLEtBQVQsS0FBbUJLLGNBQWNMLEtBQWQsQ0FBcEIsSUFDS0QsUUFBUUMsS0FBUixLQUFrQkEsTUFBTXRSLE1BQU4sS0FBaUIsQ0FENUMsRUFDZ0Q7Z0JBQ3BDd1UsU0FBUjs7OztNQUlGVSxnQkFBRixHQUFxQixJQUFyQjtNQUNFeVcsT0FBRixHQUFZMW1CLEVBQUVzUSxNQUFGLEdBQVdxWixLQUF2QjtNQUNFdlosRUFBRixHQUFPNUMsTUFBUDtNQUNFMEMsRUFBRixHQUFPN0QsS0FBUDtNQUNFOEQsRUFBRixHQUFPNUMsTUFBUDtNQUNFNEIsT0FBRixHQUFZMUIsTUFBWjs7V0FFTzhiLGlCQUFpQnZwQixDQUFqQixDQUFQOzs7QUN6R0csU0FBUytuQixXQUFULENBQXNCMWIsS0FBdEIsRUFBNkJrQixNQUE3QixFQUFxQ0MsTUFBckMsRUFBNkNDLE1BQTdDLEVBQXFEO1dBQ2pEQyxpQkFBaUJyQixLQUFqQixFQUF3QmtCLE1BQXhCLEVBQWdDQyxNQUFoQyxFQUF3Q0MsTUFBeEMsRUFBZ0QsS0FBaEQsQ0FBUDs7O0FDRUcsSUFBSW1jLGVBQWU5WCxVQUN0QixvR0FEc0IsRUFFdEIsWUFBWTtRQUNKK1gsUUFBUTlCLFlBQVkvYixLQUFaLENBQWtCLElBQWxCLEVBQXdCQyxTQUF4QixDQUFaO1FBQ0ksS0FBS2tDLE9BQUwsTUFBa0IwYixNQUFNMWIsT0FBTixFQUF0QixFQUF1QztlQUM1QjBiLFFBQVEsSUFBUixHQUFlLElBQWYsR0FBc0JBLEtBQTdCO0tBREosTUFFTztlQUNJcGEsZUFBUDs7Q0FQYyxDQUFuQjs7QUFZUCxBQUFPLElBQUlxYSxlQUFlaFksVUFDdEIsb0dBRHNCLEVBRXRCLFlBQVk7UUFDSitYLFFBQVE5QixZQUFZL2IsS0FBWixDQUFrQixJQUFsQixFQUF3QkMsU0FBeEIsQ0FBWjtRQUNJLEtBQUtrQyxPQUFMLE1BQWtCMGIsTUFBTTFiLE9BQU4sRUFBdEIsRUFBdUM7ZUFDNUIwYixRQUFRLElBQVIsR0FBZSxJQUFmLEdBQXNCQSxLQUE3QjtLQURKLE1BRU87ZUFDSXBhLGVBQVA7O0NBUGMsQ0FBbkI7Ozs7Ozs7QUFpQlAsU0FBU3NhLE1BQVQsQ0FBZ0I5YyxFQUFoQixFQUFvQitjLE9BQXBCLEVBQTZCO1FBQ3JCOWMsR0FBSixFQUFTOVMsQ0FBVDtRQUNJNHZCLFFBQVFqdkIsTUFBUixLQUFtQixDQUFuQixJQUF3QnFSLFFBQVE0ZCxRQUFRLENBQVIsQ0FBUixDQUE1QixFQUFpRDtrQkFDbkNBLFFBQVEsQ0FBUixDQUFWOztRQUVBLENBQUNBLFFBQVFqdkIsTUFBYixFQUFxQjtlQUNWZ3RCLGFBQVA7O1VBRUVpQyxRQUFRLENBQVIsQ0FBTjtTQUNLNXZCLElBQUksQ0FBVCxFQUFZQSxJQUFJNHZCLFFBQVFqdkIsTUFBeEIsRUFBZ0MsRUFBRVgsQ0FBbEMsRUFBcUM7WUFDN0IsQ0FBQzR2QixRQUFRNXZCLENBQVIsRUFBVytULE9BQVgsRUFBRCxJQUF5QjZiLFFBQVE1dkIsQ0FBUixFQUFXNlMsRUFBWCxFQUFlQyxHQUFmLENBQTdCLEVBQWtEO2tCQUN4QzhjLFFBQVE1dkIsQ0FBUixDQUFOOzs7V0FHRDhTLEdBQVA7Ozs7QUFJSixBQUFPLFNBQVNuTCxHQUFULEdBQWdCO1FBQ2ZrUSxPQUFPLEdBQUdHLEtBQUgsQ0FBU2xLLElBQVQsQ0FBYytELFNBQWQsRUFBeUIsQ0FBekIsQ0FBWDs7V0FFTzhkLE9BQU8sVUFBUCxFQUFtQjlYLElBQW5CLENBQVA7OztBQUdKLEFBQU8sU0FBU25YLEdBQVQsR0FBZ0I7UUFDZm1YLE9BQU8sR0FBR0csS0FBSCxDQUFTbEssSUFBVCxDQUFjK0QsU0FBZCxFQUF5QixDQUF6QixDQUFYOztXQUVPOGQsT0FBTyxTQUFQLEVBQWtCOVgsSUFBbEIsQ0FBUDs7O0FDN0RHLElBQUl1QixNQUFNLFNBQU5BLEdBQU0sR0FBWTtXQUNsQjFHLEtBQUswRyxHQUFMLEdBQVcxRyxLQUFLMEcsR0FBTCxFQUFYLEdBQXdCLENBQUUsSUFBSTFHLElBQUosRUFBakM7Q0FERzs7QUNHQSxTQUFTbWQsUUFBVCxDQUFtQmxmLFFBQW5CLEVBQTZCO1FBQzVCMkssa0JBQWtCRixxQkFBcUJ6SyxRQUFyQixDQUF0QjtRQUNJbWYsUUFBUXhVLGdCQUFnQnVGLElBQWhCLElBQXdCLENBRHBDO1FBRUlrUCxXQUFXelUsZ0JBQWdCMFUsT0FBaEIsSUFBMkIsQ0FGMUM7UUFHSTlPLFNBQVM1RixnQkFBZ0J3RixLQUFoQixJQUF5QixDQUh0QztRQUlJbVAsUUFBUTNVLGdCQUFnQnVKLElBQWhCLElBQXdCLENBSnBDO1FBS0lxTCxPQUFPNVUsZ0JBQWdCc0wsR0FBaEIsSUFBdUIsQ0FMbEM7UUFNSWlDLFFBQVF2TixnQkFBZ0JnVCxJQUFoQixJQUF3QixDQU5wQztRQU9JdkYsVUFBVXpOLGdCQUFnQjBULE1BQWhCLElBQTBCLENBUHhDO1FBUUloRyxVQUFVMU4sZ0JBQWdCMlQsTUFBaEIsSUFBMEIsQ0FSeEM7UUFTSWtCLGVBQWU3VSxnQkFBZ0I0VCxXQUFoQixJQUErQixDQVRsRDs7O1NBWUtrQixhQUFMLEdBQXFCLENBQUNELFlBQUQsR0FDakJuSCxVQUFVLEdBRE87Y0FFUCxHQUZPO1lBR1QsSUFBUixHQUFlLEVBQWYsR0FBb0IsRUFIeEIsQ0FiZ0M7OztTQW1CM0JxSCxLQUFMLEdBQWEsQ0FBQ0gsSUFBRCxHQUNURCxRQUFRLENBRFo7Ozs7U0FLS3hPLE9BQUwsR0FBZSxDQUFDUCxNQUFELEdBQ1g2TyxXQUFXLENBREEsR0FFWEQsUUFBUSxFQUZaOztTQUlLUSxLQUFMLEdBQWEsRUFBYjs7U0FFS2xhLE9BQUwsR0FBZXlVLFdBQWY7O1NBRUswRixPQUFMOzs7QUFHSixBQUFPLFNBQVNDLFVBQVQsQ0FBcUJ4aUIsR0FBckIsRUFBMEI7V0FDdEJBLGVBQWU2aEIsUUFBdEI7OztBQ3ZDVyxTQUFTWSxRQUFULENBQW1CL1osTUFBbkIsRUFBMkI7UUFDbENBLFNBQVMsQ0FBYixFQUFnQjtlQUNMalcsS0FBS3lLLEtBQUwsQ0FBVyxDQUFDLENBQUQsR0FBS3dMLE1BQWhCLElBQTBCLENBQUMsQ0FBbEM7S0FESixNQUVPO2VBQ0lqVyxLQUFLeUssS0FBTCxDQUFXd0wsTUFBWCxDQUFQOzs7O0FDWVI7O0FBRUEsU0FBU3BXLE1BQVQsQ0FBaUIyYyxLQUFqQixFQUF3QnlULFNBQXhCLEVBQW1DO21CQUNoQnpULEtBQWYsRUFBc0IsQ0FBdEIsRUFBeUIsQ0FBekIsRUFBNEIsWUFBWTtZQUNoQzNjLFNBQVMsS0FBS3F3QixTQUFMLEVBQWI7WUFDSWpVLE9BQU8sR0FBWDtZQUNJcGMsU0FBUyxDQUFiLEVBQWdCO3FCQUNILENBQUNBLE1BQVY7bUJBQ08sR0FBUDs7ZUFFR29jLE9BQU9MLFNBQVMsQ0FBQyxFQUFFL2IsU0FBUyxFQUFYLENBQVYsRUFBMEIsQ0FBMUIsQ0FBUCxHQUFzQ293QixTQUF0QyxHQUFrRHJVLFNBQVMsQ0FBQyxDQUFFL2IsTUFBSCxHQUFhLEVBQXRCLEVBQTBCLENBQTFCLENBQXpEO0tBUEo7OztBQVdKQSxPQUFPLEdBQVAsRUFBWSxHQUFaO0FBQ0FBLE9BQU8sSUFBUCxFQUFhLEVBQWI7Ozs7QUFJQXllLGNBQWMsR0FBZCxFQUFvQkosZ0JBQXBCO0FBQ0FJLGNBQWMsSUFBZCxFQUFvQkosZ0JBQXBCO0FBQ0FrQixjQUFjLENBQUMsR0FBRCxFQUFNLElBQU4sQ0FBZCxFQUEyQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCaFosTUFBeEIsRUFBZ0M7V0FDaEQrbkIsT0FBUCxHQUFpQixJQUFqQjtXQUNPclcsSUFBUCxHQUFjMmEsaUJBQWlCalMsZ0JBQWpCLEVBQW1DMU0sS0FBbkMsQ0FBZDtDQUZKOzs7Ozs7O0FBVUEsSUFBSTRlLGNBQWMsaUJBQWxCOztBQUVBLFNBQVNELGdCQUFULENBQTBCRSxPQUExQixFQUFtQ3ZXLE1BQW5DLEVBQTJDO1FBQ25Dd1csVUFBVSxDQUFDeFcsVUFBVSxFQUFYLEVBQWU4QyxLQUFmLENBQXFCeVQsT0FBckIsQ0FBZDs7UUFFSUMsWUFBWSxJQUFoQixFQUFzQjtlQUNYLElBQVA7OztRQUdBQyxRQUFVRCxRQUFRQSxRQUFRcHdCLE1BQVIsR0FBaUIsQ0FBekIsS0FBK0IsRUFBN0M7UUFDSXN3QixRQUFVLENBQUNELFFBQVEsRUFBVCxFQUFhM1QsS0FBYixDQUFtQndULFdBQW5CLEtBQW1DLENBQUMsR0FBRCxFQUFNLENBQU4sRUFBUyxDQUFULENBQWpEO1FBQ0k5SCxVQUFVLEVBQUVrSSxNQUFNLENBQU4sSUFBVyxFQUFiLElBQW1CcmEsTUFBTXFhLE1BQU0sQ0FBTixDQUFOLENBQWpDOztXQUVPbEksWUFBWSxDQUFaLEdBQ0wsQ0FESyxHQUVMa0ksTUFBTSxDQUFOLE1BQWEsR0FBYixHQUFtQmxJLE9BQW5CLEdBQTZCLENBQUNBLE9BRmhDOzs7O0FBTUosQUFBTyxTQUFTbUksZUFBVCxDQUF5QmpmLEtBQXpCLEVBQWdDa2YsS0FBaEMsRUFBdUM7UUFDdENyZSxHQUFKLEVBQVM2SCxJQUFUO1FBQ0l3VyxNQUFNamIsTUFBVixFQUFrQjtjQUNSaWIsTUFBTUMsS0FBTixFQUFOO2VBQ08sQ0FBQzVhLFNBQVN2RSxLQUFULEtBQW1CUSxPQUFPUixLQUFQLENBQW5CLEdBQW1DQSxNQUFNZ0IsT0FBTixFQUFuQyxHQUFxRDBhLFlBQVkxYixLQUFaLEVBQW1CZ0IsT0FBbkIsRUFBdEQsSUFBc0ZILElBQUlHLE9BQUosRUFBN0Y7O1lBRUlvQixFQUFKLENBQU9nZCxPQUFQLENBQWV2ZSxJQUFJdUIsRUFBSixDQUFPcEIsT0FBUCxLQUFtQjBILElBQWxDO2NBQ01wRSxZQUFOLENBQW1CekQsR0FBbkIsRUFBd0IsS0FBeEI7ZUFDT0EsR0FBUDtLQU5KLE1BT087ZUFDSTZhLFlBQVkxYixLQUFaLEVBQW1CcWYsS0FBbkIsRUFBUDs7OztBQUlSLFNBQVNDLGFBQVQsQ0FBd0I3ZCxDQUF4QixFQUEyQjs7O1dBR2hCLENBQUNqVCxLQUFLeUssS0FBTCxDQUFXd0ksRUFBRVcsRUFBRixDQUFLbWQsaUJBQUwsS0FBMkIsRUFBdEMsQ0FBRCxHQUE2QyxFQUFwRDs7Ozs7OztBQU9KN2YsTUFBTTRFLFlBQU4sR0FBcUIsWUFBWSxFQUFqQzs7Ozs7Ozs7Ozs7Ozs7QUFjQSxBQUFPLFNBQVNrYixZQUFULENBQXVCeGYsS0FBdkIsRUFBOEJ5ZixhQUE5QixFQUE2QztRQUM1Q3B4QixTQUFTLEtBQUs2VixPQUFMLElBQWdCLENBQTdCO1FBQ0l3YixXQURKO1FBRUksQ0FBQyxLQUFLNWQsT0FBTCxFQUFMLEVBQXFCO2VBQ1Y5QixTQUFTLElBQVQsR0FBZ0IsSUFBaEIsR0FBdUJxRCxHQUE5Qjs7UUFFQXJELFNBQVMsSUFBYixFQUFtQjtZQUNYLE9BQU9BLEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7b0JBQ25CMmUsaUJBQWlCalMsZ0JBQWpCLEVBQW1DMU0sS0FBbkMsQ0FBUjtnQkFDSUEsVUFBVSxJQUFkLEVBQW9CO3VCQUNULElBQVA7O1NBSFIsTUFLTyxJQUFJeFIsS0FBSzZKLEdBQUwsQ0FBUzJILEtBQVQsSUFBa0IsRUFBdEIsRUFBMEI7b0JBQ3JCQSxRQUFRLEVBQWhCOztZQUVBLENBQUMsS0FBS2lFLE1BQU4sSUFBZ0J3YixhQUFwQixFQUFtQzswQkFDakJILGNBQWMsSUFBZCxDQUFkOzthQUVDcGIsT0FBTCxHQUFlbEUsS0FBZjthQUNLaUUsTUFBTCxHQUFjLElBQWQ7WUFDSXliLGVBQWUsSUFBbkIsRUFBeUI7aUJBQ2hCN0wsR0FBTCxDQUFTNkwsV0FBVCxFQUFzQixHQUF0Qjs7WUFFQXJ4QixXQUFXMlIsS0FBZixFQUFzQjtnQkFDZCxDQUFDeWYsYUFBRCxJQUFrQixLQUFLRSxpQkFBM0IsRUFBOEM7NEJBQzlCLElBQVosRUFBa0JDLGVBQWU1ZixRQUFRM1IsTUFBdkIsRUFBK0IsR0FBL0IsQ0FBbEIsRUFBdUQsQ0FBdkQsRUFBMEQsS0FBMUQ7YUFESixNQUVPLElBQUksQ0FBQyxLQUFLc3hCLGlCQUFWLEVBQTZCO3FCQUMzQkEsaUJBQUwsR0FBeUIsSUFBekI7c0JBQ01yYixZQUFOLENBQW1CLElBQW5CLEVBQXlCLElBQXpCO3FCQUNLcWIsaUJBQUwsR0FBeUIsSUFBekI7OztlQUdELElBQVA7S0ExQkosTUEyQk87ZUFDSSxLQUFLMWIsTUFBTCxHQUFjNVYsTUFBZCxHQUF1Qml4QixjQUFjLElBQWQsQ0FBOUI7Ozs7QUFJUixBQUFPLFNBQVNPLFVBQVQsQ0FBcUI3ZixLQUFyQixFQUE0QnlmLGFBQTVCLEVBQTJDO1FBQzFDemYsU0FBUyxJQUFiLEVBQW1CO1lBQ1gsT0FBT0EsS0FBUCxLQUFpQixRQUFyQixFQUErQjtvQkFDbkIsQ0FBQ0EsS0FBVDs7O2FBR0MwZSxTQUFMLENBQWUxZSxLQUFmLEVBQXNCeWYsYUFBdEI7O2VBRU8sSUFBUDtLQVBKLE1BUU87ZUFDSSxDQUFDLEtBQUtmLFNBQUwsRUFBUjs7OztBQUlSLEFBQU8sU0FBU29CLGNBQVQsQ0FBeUJMLGFBQXpCLEVBQXdDO1dBQ3BDLEtBQUtmLFNBQUwsQ0FBZSxDQUFmLEVBQWtCZSxhQUFsQixDQUFQOzs7QUFHSixBQUFPLFNBQVNNLGdCQUFULENBQTJCTixhQUEzQixFQUEwQztRQUN6QyxLQUFLeGIsTUFBVCxFQUFpQjthQUNSeWEsU0FBTCxDQUFlLENBQWYsRUFBa0JlLGFBQWxCO2FBQ0t4YixNQUFMLEdBQWMsS0FBZDs7WUFFSXdiLGFBQUosRUFBbUI7aUJBQ1ZPLFFBQUwsQ0FBY1YsY0FBYyxJQUFkLENBQWQsRUFBbUMsR0FBbkM7OztXQUdELElBQVA7OztBQUdKLEFBQU8sU0FBU1csdUJBQVQsR0FBb0M7UUFDbkMsS0FBS2pjLElBQUwsSUFBYSxJQUFqQixFQUF1QjthQUNkMGEsU0FBTCxDQUFlLEtBQUsxYSxJQUFwQjtLQURKLE1BRU8sSUFBSSxPQUFPLEtBQUtILEVBQVosS0FBbUIsUUFBdkIsRUFBaUM7WUFDaENxYyxRQUFRdkIsaUJBQWlCbFMsV0FBakIsRUFBOEIsS0FBSzVJLEVBQW5DLENBQVo7WUFDSXFjLFNBQVMsSUFBYixFQUFtQjtpQkFDVnhCLFNBQUwsQ0FBZXdCLEtBQWY7U0FESixNQUdLO2lCQUNJeEIsU0FBTCxDQUFlLENBQWYsRUFBa0IsSUFBbEI7OztXQUdELElBQVA7OztBQUdKLEFBQU8sU0FBU3lCLG9CQUFULENBQStCbmdCLEtBQS9CLEVBQXNDO1FBQ3JDLENBQUMsS0FBSzhCLE9BQUwsRUFBTCxFQUFxQjtlQUNWLEtBQVA7O1lBRUk5QixRQUFRMGIsWUFBWTFiLEtBQVosRUFBbUIwZSxTQUFuQixFQUFSLEdBQXlDLENBQWpEOztXQUVPLENBQUMsS0FBS0EsU0FBTCxLQUFtQjFlLEtBQXBCLElBQTZCLEVBQTdCLEtBQW9DLENBQTNDOzs7QUFHSixBQUFPLFNBQVNvZ0Isb0JBQVQsR0FBaUM7V0FFaEMsS0FBSzFCLFNBQUwsS0FBbUIsS0FBS1MsS0FBTCxHQUFhdFEsS0FBYixDQUFtQixDQUFuQixFQUFzQjZQLFNBQXRCLEVBQW5CLElBQ0EsS0FBS0EsU0FBTCxLQUFtQixLQUFLUyxLQUFMLEdBQWF0USxLQUFiLENBQW1CLENBQW5CLEVBQXNCNlAsU0FBdEIsRUFGdkI7OztBQU1KLEFBQU8sU0FBUzJCLDJCQUFULEdBQXdDO1FBQ3ZDLENBQUMvYyxZQUFZLEtBQUtnZCxhQUFqQixDQUFMLEVBQXNDO2VBQzNCLEtBQUtBLGFBQVo7OztRQUdBM3NCLElBQUksRUFBUjs7ZUFFV0EsQ0FBWCxFQUFjLElBQWQ7UUFDSXdwQixjQUFjeHBCLENBQWQsQ0FBSjs7UUFFSUEsRUFBRXFhLEVBQU4sRUFBVTtZQUNGd1AsUUFBUTdwQixFQUFFc1EsTUFBRixHQUFXaEQsVUFBVXROLEVBQUVxYSxFQUFaLENBQVgsR0FBNkIwTixZQUFZL25CLEVBQUVxYSxFQUFkLENBQXpDO2FBQ0tzUyxhQUFMLEdBQXFCLEtBQUt4ZSxPQUFMLE1BQ2pCaUQsY0FBY3BSLEVBQUVxYSxFQUFoQixFQUFvQndQLE1BQU0rQyxPQUFOLEVBQXBCLElBQXVDLENBRDNDO0tBRkosTUFJTzthQUNFRCxhQUFMLEdBQXFCLEtBQXJCOzs7V0FHRyxLQUFLQSxhQUFaOzs7QUFHSixBQUFPLFNBQVNFLE9BQVQsR0FBb0I7V0FDaEIsS0FBSzFlLE9BQUwsS0FBaUIsQ0FBQyxLQUFLbUMsTUFBdkIsR0FBZ0MsS0FBdkM7OztBQUdKLEFBQU8sU0FBU3djLFdBQVQsR0FBd0I7V0FDcEIsS0FBSzNlLE9BQUwsS0FBaUIsS0FBS21DLE1BQXRCLEdBQStCLEtBQXRDOzs7QUFHSixBQUFPLFNBQVN5YyxLQUFULEdBQWtCO1dBQ2QsS0FBSzVlLE9BQUwsS0FBaUIsS0FBS21DLE1BQUwsSUFBZSxLQUFLQyxPQUFMLEtBQWlCLENBQWpELEdBQXFELEtBQTVEOzs7QUNoT0o7QUFDQSxJQUFJeWMsY0FBYyx1REFBbEI7Ozs7O0FBS0EsSUFBSUMsV0FBVyw2SUFBZjs7QUFFQSxBQUFPLFNBQVNoQixjQUFULENBQXlCNWYsS0FBekIsRUFBZ0NsUixHQUFoQyxFQUFxQztRQUNwQzRQLFdBQVdzQixLQUFmOzs7WUFFWSxJQUZaO1FBR0l5SyxJQUhKO1FBSUlvVyxHQUpKO1FBS0lDLE9BTEo7O1FBT0l2QyxXQUFXdmUsS0FBWCxDQUFKLEVBQXVCO21CQUNSO2dCQUNGQSxNQUFNbWUsYUFESjtlQUVGbmUsTUFBTW9lLEtBRko7ZUFHRnBlLE1BQU13UDtTQUhmO0tBREosTUFNTyxJQUFJalAsU0FBU1AsS0FBVCxDQUFKLEVBQXFCO21CQUNiLEVBQVg7WUFDSWxSLEdBQUosRUFBUztxQkFDSUEsR0FBVCxJQUFnQmtSLEtBQWhCO1NBREosTUFFTztxQkFDTWtlLFlBQVQsR0FBd0JsZSxLQUF4Qjs7S0FMRCxNQU9BLElBQUksQ0FBQyxFQUFFb0wsUUFBUXVWLFlBQVk5RyxJQUFaLENBQWlCN1osS0FBakIsQ0FBVixDQUFMLEVBQXlDO2VBQ3BDb0wsTUFBTSxDQUFOLE1BQWEsR0FBZCxHQUFxQixDQUFDLENBQXRCLEdBQTBCLENBQWpDO21CQUNXO2VBQ0YsQ0FERTtlQUVGekcsTUFBTXlHLE1BQU0rQyxJQUFOLENBQU4sSUFBNkMxRCxJQUYzQztlQUdGOUYsTUFBTXlHLE1BQU1nRCxJQUFOLENBQU4sSUFBNkMzRCxJQUgzQztlQUlGOUYsTUFBTXlHLE1BQU1pRCxNQUFOLENBQU4sSUFBNkM1RCxJQUozQztlQUtGOUYsTUFBTXlHLE1BQU1rRCxNQUFOLENBQU4sSUFBNkM3RCxJQUwzQztnQkFNRjlGLE1BQU02WixTQUFTcFQsTUFBTW1ELFdBQU4sSUFBcUIsSUFBOUIsQ0FBTixJQUE2QzlELElBTjNDO1NBQVg7S0FGRyxNQVVBLElBQUksQ0FBQyxFQUFFVyxRQUFRd1YsU0FBUy9HLElBQVQsQ0FBYzdaLEtBQWQsQ0FBVixDQUFMLEVBQXNDO2VBQ2pDb0wsTUFBTSxDQUFOLE1BQWEsR0FBZCxHQUFxQixDQUFDLENBQXRCLEdBQTBCLENBQWpDO21CQUNXO2VBQ0gyVixTQUFTM1YsTUFBTSxDQUFOLENBQVQsRUFBbUJYLElBQW5CLENBREc7ZUFFSHNXLFNBQVMzVixNQUFNLENBQU4sQ0FBVCxFQUFtQlgsSUFBbkIsQ0FGRztlQUdIc1csU0FBUzNWLE1BQU0sQ0FBTixDQUFULEVBQW1CWCxJQUFuQixDQUhHO2VBSUhzVyxTQUFTM1YsTUFBTSxDQUFOLENBQVQsRUFBbUJYLElBQW5CLENBSkc7ZUFLSHNXLFNBQVMzVixNQUFNLENBQU4sQ0FBVCxFQUFtQlgsSUFBbkIsQ0FMRztlQU1Ic1csU0FBUzNWLE1BQU0sQ0FBTixDQUFULEVBQW1CWCxJQUFuQixDQU5HO2VBT0hzVyxTQUFTM1YsTUFBTSxDQUFOLENBQVQsRUFBbUJYLElBQW5CO1NBUFI7S0FGRyxNQVdBLElBQUkvTCxZQUFZLElBQWhCLEVBQXNCOzttQkFDZCxFQUFYO0tBREcsTUFFQSxJQUFJLFFBQU9BLFFBQVAseUNBQU9BLFFBQVAsT0FBb0IsUUFBcEIsS0FBaUMsVUFBVUEsUUFBVixJQUFzQixRQUFRQSxRQUEvRCxDQUFKLEVBQThFO2tCQUN2RXNpQixrQkFBa0J0RixZQUFZaGQsU0FBUytFLElBQXJCLENBQWxCLEVBQThDaVksWUFBWWhkLFNBQVN0QixFQUFyQixDQUE5QyxDQUFWOzttQkFFVyxFQUFYO2lCQUNTMlUsRUFBVCxHQUFjK08sUUFBUTVDLFlBQXRCO2lCQUNTcE0sQ0FBVCxHQUFhZ1AsUUFBUTdSLE1BQXJCOzs7VUFHRSxJQUFJMk8sUUFBSixDQUFhbGYsUUFBYixDQUFOOztRQUVJNmYsV0FBV3ZlLEtBQVgsS0FBcUJjLFdBQVdkLEtBQVgsRUFBa0IsU0FBbEIsQ0FBekIsRUFBdUQ7WUFDL0NtRSxPQUFKLEdBQWNuRSxNQUFNbUUsT0FBcEI7OztXQUdHMGMsR0FBUDs7O0FBR0pqQixlQUFlaGYsRUFBZixHQUFvQmdkLFNBQVMxZCxTQUE3Qjs7QUFFQSxTQUFTNmdCLFFBQVQsQ0FBbUJFLEdBQW5CLEVBQXdCeFcsSUFBeEIsRUFBOEI7Ozs7UUFJdEI1SixNQUFNb2dCLE9BQU9DLFdBQVdELElBQUl0WixPQUFKLENBQVksR0FBWixFQUFpQixHQUFqQixDQUFYLENBQWpCOztXQUVPLENBQUNwUCxNQUFNc0ksR0FBTixJQUFhLENBQWIsR0FBaUJBLEdBQWxCLElBQXlCNEosSUFBaEM7OztBQUdKLFNBQVMwVyx5QkFBVCxDQUFtQ0MsSUFBbkMsRUFBeUM1RCxLQUF6QyxFQUFnRDtRQUN4QzNjLE1BQU0sRUFBQ3FkLGNBQWMsQ0FBZixFQUFrQmpQLFFBQVEsQ0FBMUIsRUFBVjs7UUFFSUEsTUFBSixHQUFhdU8sTUFBTTNPLEtBQU4sS0FBZ0J1UyxLQUFLdlMsS0FBTCxFQUFoQixHQUNULENBQUMyTyxNQUFNNU8sSUFBTixLQUFld1MsS0FBS3hTLElBQUwsRUFBaEIsSUFBK0IsRUFEbkM7UUFFSXdTLEtBQUtqQyxLQUFMLEdBQWF0TCxHQUFiLENBQWlCaFQsSUFBSW9PLE1BQXJCLEVBQTZCLEdBQTdCLEVBQWtDb1MsT0FBbEMsQ0FBMEM3RCxLQUExQyxDQUFKLEVBQXNEO1VBQ2hEM2MsSUFBSW9PLE1BQU47OztRQUdBaVAsWUFBSixHQUFtQixDQUFDVixLQUFELEdBQVMsQ0FBRTRELEtBQUtqQyxLQUFMLEdBQWF0TCxHQUFiLENBQWlCaFQsSUFBSW9PLE1BQXJCLEVBQTZCLEdBQTdCLENBQTlCOztXQUVPcE8sR0FBUDs7O0FBR0osU0FBU21nQixpQkFBVCxDQUEyQkksSUFBM0IsRUFBaUM1RCxLQUFqQyxFQUF3QztRQUNoQzNjLEdBQUo7UUFDSSxFQUFFdWdCLEtBQUt0ZixPQUFMLE1BQWtCMGIsTUFBTTFiLE9BQU4sRUFBcEIsQ0FBSixFQUEwQztlQUMvQixFQUFDb2MsY0FBYyxDQUFmLEVBQWtCalAsUUFBUSxDQUExQixFQUFQOzs7WUFHSWdRLGdCQUFnQnpCLEtBQWhCLEVBQXVCNEQsSUFBdkIsQ0FBUjtRQUNJQSxLQUFLRSxRQUFMLENBQWM5RCxLQUFkLENBQUosRUFBMEI7Y0FDaEIyRCwwQkFBMEJDLElBQTFCLEVBQWdDNUQsS0FBaEMsQ0FBTjtLQURKLE1BRU87Y0FDRzJELDBCQUEwQjNELEtBQTFCLEVBQWlDNEQsSUFBakMsQ0FBTjtZQUNJbEQsWUFBSixHQUFtQixDQUFDcmQsSUFBSXFkLFlBQXhCO1lBQ0lqUCxNQUFKLEdBQWEsQ0FBQ3BPLElBQUlvTyxNQUFsQjs7O1dBR0dwTyxHQUFQOzs7QUM5R0o7QUFDQSxTQUFTMGdCLFdBQVQsQ0FBcUJDLFNBQXJCLEVBQWdDMXRCLElBQWhDLEVBQXNDO1dBQzNCLFVBQVU2UCxHQUFWLEVBQWU4ZCxNQUFmLEVBQXVCO1lBQ3RCQyxHQUFKLEVBQVNDLEdBQVQ7O1lBRUlGLFdBQVcsSUFBWCxJQUFtQixDQUFDbHBCLE1BQU0sQ0FBQ2twQixNQUFQLENBQXhCLEVBQXdDOzRCQUNwQjN0QixJQUFoQixFQUFzQixjQUFjQSxJQUFkLEdBQXNCLHNEQUF0QixHQUErRUEsSUFBL0UsR0FBc0Ysb0JBQXRGLEdBQ3RCLDhFQURBO2tCQUVNNlAsR0FBTixDQUFXQSxNQUFNOGQsTUFBTixDQUFjQSxTQUFTRSxHQUFUOzs7Y0FHdkIsT0FBT2hlLEdBQVAsS0FBZSxRQUFmLEdBQTBCLENBQUNBLEdBQTNCLEdBQWlDQSxHQUF2QztjQUNNaWMsZUFBZWpjLEdBQWYsRUFBb0I4ZCxNQUFwQixDQUFOO29CQUNZLElBQVosRUFBa0JDLEdBQWxCLEVBQXVCRixTQUF2QjtlQUNPLElBQVA7S0FaSjs7O0FBZ0JKLEFBQU8sU0FBU0ksV0FBVCxDQUFzQjFhLEdBQXRCLEVBQTJCeEksUUFBM0IsRUFBcUNtakIsUUFBckMsRUFBK0N2ZCxZQUEvQyxFQUE2RDtRQUM1RDRaLGVBQWV4ZixTQUFTeWYsYUFBNUI7UUFDSUYsT0FBT08sU0FBUzlmLFNBQVMwZixLQUFsQixDQURYO1FBRUluUCxTQUFTdVAsU0FBUzlmLFNBQVM4USxPQUFsQixDQUZiOztRQUlJLENBQUN0SSxJQUFJcEYsT0FBSixFQUFMLEVBQW9COzs7OzttQkFLTHdDLGdCQUFnQixJQUFoQixHQUF1QixJQUF2QixHQUE4QkEsWUFBN0M7O1FBRUk0WixZQUFKLEVBQWtCO1lBQ1Y5YixFQUFKLENBQU9nZCxPQUFQLENBQWVsWSxJQUFJOUUsRUFBSixDQUFPcEIsT0FBUCxLQUFtQmtkLGVBQWUyRCxRQUFqRDs7UUFFQTVELElBQUosRUFBVTtjQUNGL1csR0FBSixFQUFTLE1BQVQsRUFBaUI4QyxNQUFJOUMsR0FBSixFQUFTLE1BQVQsSUFBbUIrVyxPQUFPNEQsUUFBM0M7O1FBRUE1UyxNQUFKLEVBQVk7aUJBQ0MvSCxHQUFULEVBQWM4QyxNQUFJOUMsR0FBSixFQUFTLE9BQVQsSUFBb0IrSCxTQUFTNFMsUUFBM0M7O1FBRUF2ZCxZQUFKLEVBQWtCO2NBQ1JBLFlBQU4sQ0FBbUI0QyxHQUFuQixFQUF3QitXLFFBQVFoUCxNQUFoQzs7OztBQUlSLEFBQU8sSUFBSTRFLE1BQVcwTixZQUFZLENBQVosRUFBZSxLQUFmLENBQWY7QUFDUCxBQUFPLElBQUl2QixXQUFXdUIsWUFBWSxDQUFDLENBQWIsRUFBZ0IsVUFBaEIsQ0FBZjs7QUNoREEsU0FBU08saUJBQVQsQ0FBMkJDLFFBQTNCLEVBQXFDNWEsR0FBckMsRUFBMEM7UUFDekN1QixPQUFPcVosU0FBU3JaLElBQVQsQ0FBY3ZCLEdBQWQsRUFBbUIsTUFBbkIsRUFBMkIsSUFBM0IsQ0FBWDtXQUNPdUIsT0FBTyxDQUFDLENBQVIsR0FBWSxVQUFaLEdBQ0NBLE9BQU8sQ0FBQyxDQUFSLEdBQVksVUFBWixHQUNBQSxPQUFPLENBQVAsR0FBVyxTQUFYLEdBQ0FBLE9BQU8sQ0FBUCxHQUFXLFNBQVgsR0FDQUEsT0FBTyxDQUFQLEdBQVcsU0FBWCxHQUNBQSxPQUFPLENBQVAsR0FBVyxVQUFYLEdBQXdCLFVBTGhDOzs7QUFRSixBQUFPLFNBQVN6QixVQUFULENBQW1CK2EsSUFBbkIsRUFBeUJDLE9BQXpCLEVBQWtDOzs7UUFHakM5YSxNQUFNNmEsUUFBUXRHLGFBQWxCO1FBQ0l3RyxNQUFNakQsZ0JBQWdCOVgsR0FBaEIsRUFBcUIsSUFBckIsRUFBMkJnYixPQUEzQixDQUFtQyxLQUFuQyxDQURWO1FBRUlqaEIsU0FBU3hCLE1BQU0waUIsY0FBTixDQUFxQixJQUFyQixFQUEyQkYsR0FBM0IsS0FBbUMsVUFGaEQ7O1FBSUk5YSxTQUFTNmEsWUFBWTViLFdBQVc0YixRQUFRL2dCLE1BQVIsQ0FBWCxJQUE4QitnQixRQUFRL2dCLE1BQVIsRUFBZ0JyRixJQUFoQixDQUFxQixJQUFyQixFQUEyQnNMLEdBQTNCLENBQTlCLEdBQWdFOGEsUUFBUS9nQixNQUFSLENBQTVFLENBQWI7O1dBRU8sS0FBS0EsTUFBTCxDQUFZa0csVUFBVSxLQUFLOEQsVUFBTCxHQUFrQmpFLFFBQWxCLENBQTJCL0YsTUFBM0IsRUFBbUMsSUFBbkMsRUFBeUN3YSxZQUFZdlUsR0FBWixDQUF6QyxDQUF0QixDQUFQOzs7QUN0QkcsU0FBU2dZLEtBQVQsR0FBa0I7V0FDZCxJQUFJOWEsTUFBSixDQUFXLElBQVgsQ0FBUDs7O0FDRUcsU0FBU2dkLE9BQVQsQ0FBa0JyaEIsS0FBbEIsRUFBeUJrSixLQUF6QixFQUFnQztRQUMvQm1aLGFBQWE5ZCxTQUFTdkUsS0FBVCxJQUFrQkEsS0FBbEIsR0FBMEIwYixZQUFZMWIsS0FBWixDQUEzQztRQUNJLEVBQUUsS0FBSzhCLE9BQUwsTUFBa0J1Z0IsV0FBV3ZnQixPQUFYLEVBQXBCLENBQUosRUFBK0M7ZUFDcEMsS0FBUDs7WUFFSW1ILGVBQWUsQ0FBQzNGLFlBQVk0RixLQUFaLENBQUQsR0FBc0JBLEtBQXRCLEdBQThCLGFBQTdDLENBQVI7UUFDSUEsVUFBVSxhQUFkLEVBQTZCO2VBQ2xCLEtBQUtsSSxPQUFMLEtBQWlCcWhCLFdBQVdyaEIsT0FBWCxFQUF4QjtLQURKLE1BRU87ZUFDSXFoQixXQUFXcmhCLE9BQVgsS0FBdUIsS0FBS21lLEtBQUwsR0FBYWdELE9BQWIsQ0FBcUJqWixLQUFyQixFQUE0QmxJLE9BQTVCLEVBQTlCOzs7O0FBSVIsQUFBTyxTQUFTc2dCLFFBQVQsQ0FBbUJ0aEIsS0FBbkIsRUFBMEJrSixLQUExQixFQUFpQztRQUNoQ21aLGFBQWE5ZCxTQUFTdkUsS0FBVCxJQUFrQkEsS0FBbEIsR0FBMEIwYixZQUFZMWIsS0FBWixDQUEzQztRQUNJLEVBQUUsS0FBSzhCLE9BQUwsTUFBa0J1Z0IsV0FBV3ZnQixPQUFYLEVBQXBCLENBQUosRUFBK0M7ZUFDcEMsS0FBUDs7WUFFSW1ILGVBQWUsQ0FBQzNGLFlBQVk0RixLQUFaLENBQUQsR0FBc0JBLEtBQXRCLEdBQThCLGFBQTdDLENBQVI7UUFDSUEsVUFBVSxhQUFkLEVBQTZCO2VBQ2xCLEtBQUtsSSxPQUFMLEtBQWlCcWhCLFdBQVdyaEIsT0FBWCxFQUF4QjtLQURKLE1BRU87ZUFDSSxLQUFLbWUsS0FBTCxHQUFhbUQsS0FBYixDQUFtQnBaLEtBQW5CLEVBQTBCbEksT0FBMUIsS0FBc0NxaEIsV0FBV3JoQixPQUFYLEVBQTdDOzs7O0FBSVIsQUFBTyxTQUFTdWhCLFNBQVQsQ0FBb0I5ZSxJQUFwQixFQUEwQnJHLEVBQTFCLEVBQThCOEwsS0FBOUIsRUFBcUNzWixXQUFyQyxFQUFrRDtrQkFDdkNBLGVBQWUsSUFBN0I7V0FDTyxDQUFDQSxZQUFZLENBQVosTUFBbUIsR0FBbkIsR0FBeUIsS0FBS25CLE9BQUwsQ0FBYTVkLElBQWIsRUFBbUJ5RixLQUFuQixDQUF6QixHQUFxRCxDQUFDLEtBQUtvWSxRQUFMLENBQWM3ZCxJQUFkLEVBQW9CeUYsS0FBcEIsQ0FBdkQsTUFDRnNaLFlBQVksQ0FBWixNQUFtQixHQUFuQixHQUF5QixLQUFLbEIsUUFBTCxDQUFjbGtCLEVBQWQsRUFBa0I4TCxLQUFsQixDQUF6QixHQUFvRCxDQUFDLEtBQUttWSxPQUFMLENBQWFqa0IsRUFBYixFQUFpQjhMLEtBQWpCLENBRG5ELENBQVA7OztBQUlKLEFBQU8sU0FBU3VaLE1BQVQsQ0FBaUJ6aUIsS0FBakIsRUFBd0JrSixLQUF4QixFQUErQjtRQUM5Qm1aLGFBQWE5ZCxTQUFTdkUsS0FBVCxJQUFrQkEsS0FBbEIsR0FBMEIwYixZQUFZMWIsS0FBWixDQUEzQztRQUNJMGlCLE9BREo7UUFFSSxFQUFFLEtBQUs1Z0IsT0FBTCxNQUFrQnVnQixXQUFXdmdCLE9BQVgsRUFBcEIsQ0FBSixFQUErQztlQUNwQyxLQUFQOztZQUVJbUgsZUFBZUMsU0FBUyxhQUF4QixDQUFSO1FBQ0lBLFVBQVUsYUFBZCxFQUE2QjtlQUNsQixLQUFLbEksT0FBTCxPQUFtQnFoQixXQUFXcmhCLE9BQVgsRUFBMUI7S0FESixNQUVPO2tCQUNPcWhCLFdBQVdyaEIsT0FBWCxFQUFWO2VBQ08sS0FBS21lLEtBQUwsR0FBYWdELE9BQWIsQ0FBcUJqWixLQUFyQixFQUE0QmxJLE9BQTVCLE1BQXlDMGhCLE9BQXpDLElBQW9EQSxXQUFXLEtBQUt2RCxLQUFMLEdBQWFtRCxLQUFiLENBQW1CcFosS0FBbkIsRUFBMEJsSSxPQUExQixFQUF0RTs7OztBQUlSLEFBQU8sU0FBUzJoQixhQUFULENBQXdCM2lCLEtBQXhCLEVBQStCa0osS0FBL0IsRUFBc0M7V0FDbEMsS0FBS3VaLE1BQUwsQ0FBWXppQixLQUFaLEVBQW1Ca0osS0FBbkIsS0FBNkIsS0FBS21ZLE9BQUwsQ0FBYXJoQixLQUFiLEVBQW1Ca0osS0FBbkIsQ0FBcEM7OztBQUdKLEFBQU8sU0FBUzBaLGNBQVQsQ0FBeUI1aUIsS0FBekIsRUFBZ0NrSixLQUFoQyxFQUF1QztXQUNuQyxLQUFLdVosTUFBTCxDQUFZemlCLEtBQVosRUFBbUJrSixLQUFuQixLQUE2QixLQUFLb1ksUUFBTCxDQUFjdGhCLEtBQWQsRUFBb0JrSixLQUFwQixDQUFwQzs7O0FDckRHLFNBQVNSLElBQVQsQ0FBZTFJLEtBQWYsRUFBc0JrSixLQUF0QixFQUE2QjJaLE9BQTdCLEVBQXNDO1FBQ3JDQyxJQUFKLEVBQ0lDLFNBREosRUFFSW4xQixLQUZKLEVBRVd3WixNQUZYOztRQUlJLENBQUMsS0FBS3RGLE9BQUwsRUFBTCxFQUFxQjtlQUNWdUIsR0FBUDs7O1dBR0c0YixnQkFBZ0JqZixLQUFoQixFQUF1QixJQUF2QixDQUFQOztRQUVJLENBQUM4aUIsS0FBS2hoQixPQUFMLEVBQUwsRUFBcUI7ZUFDVnVCLEdBQVA7OztnQkFHUSxDQUFDeWYsS0FBS3BFLFNBQUwsS0FBbUIsS0FBS0EsU0FBTCxFQUFwQixJQUF3QyxHQUFwRDs7WUFFUXpWLGVBQWVDLEtBQWYsQ0FBUjs7UUFFSUEsVUFBVSxNQUFWLElBQW9CQSxVQUFVLE9BQTlCLElBQXlDQSxVQUFVLFNBQXZELEVBQWtFO2lCQUNyRDhaLFVBQVUsSUFBVixFQUFnQkYsSUFBaEIsQ0FBVDtZQUNJNVosVUFBVSxTQUFkLEVBQXlCO3FCQUNaOUIsU0FBUyxDQUFsQjtTQURKLE1BRU8sSUFBSThCLFVBQVUsTUFBZCxFQUFzQjtxQkFDaEI5QixTQUFTLEVBQWxCOztLQUxSLE1BT087Z0JBQ0ssT0FBTzBiLElBQWY7aUJBQ1M1WixVQUFVLFFBQVYsR0FBcUJ0YixRQUFRLEdBQTdCO2tCQUNLLFFBQVYsR0FBcUJBLFFBQVEsR0FBN0I7a0JBQ1UsTUFBVixHQUFtQkEsUUFBUSxJQUEzQjtrQkFDVSxLQUFWLEdBQWtCLENBQUNBLFFBQVFtMUIsU0FBVCxJQUFzQixLQUF4QztrQkFDVSxNQUFWLEdBQW1CLENBQUNuMUIsUUFBUW0xQixTQUFULElBQXNCLE1BQXpDO2FBSko7O1dBT0dGLFVBQVV6YixNQUFWLEdBQW1CNUMsU0FBUzRDLE1BQVQsQ0FBMUI7OztBQUdKLFNBQVM0YixTQUFULENBQW9COXhCLENBQXBCLEVBQXVCQyxDQUF2QixFQUEwQjs7UUFFbEI4eEIsaUJBQWtCLENBQUM5eEIsRUFBRXlkLElBQUYsS0FBVzFkLEVBQUUwZCxJQUFGLEVBQVosSUFBd0IsRUFBekIsSUFBZ0N6ZCxFQUFFMGQsS0FBRixLQUFZM2QsRUFBRTJkLEtBQUYsRUFBNUMsQ0FBckI7OzthQUVhM2QsRUFBRWl1QixLQUFGLEdBQVV0TCxHQUFWLENBQWNvUCxjQUFkLEVBQThCLFFBQTlCLENBRmI7UUFHSUMsT0FISjtRQUdhQyxNQUhiOztRQUtJaHlCLElBQUlpeUIsTUFBSixHQUFhLENBQWpCLEVBQW9CO2tCQUNObHlCLEVBQUVpdUIsS0FBRixHQUFVdEwsR0FBVixDQUFjb1AsaUJBQWlCLENBQS9CLEVBQWtDLFFBQWxDLENBQVY7O2lCQUVTLENBQUM5eEIsSUFBSWl5QixNQUFMLEtBQWdCQSxTQUFTRixPQUF6QixDQUFUO0tBSEosTUFJTztrQkFDT2h5QixFQUFFaXVCLEtBQUYsR0FBVXRMLEdBQVYsQ0FBY29QLGlCQUFpQixDQUEvQixFQUFrQyxRQUFsQyxDQUFWOztpQkFFUyxDQUFDOXhCLElBQUlpeUIsTUFBTCxLQUFnQkYsVUFBVUUsTUFBMUIsQ0FBVDs7OztXQUlHLEVBQUVILGlCQUFpQkUsTUFBbkIsS0FBOEIsQ0FBckM7OztBQ3hESnpqQixNQUFNMmpCLGFBQU4sR0FBc0Isc0JBQXRCO0FBQ0EzakIsTUFBTTRqQixnQkFBTixHQUF5Qix3QkFBekI7O0FBRUEsQUFBTyxTQUFTbmpCLFFBQVQsR0FBcUI7V0FDakIsS0FBS2dmLEtBQUwsR0FBYWhlLE1BQWIsQ0FBb0IsSUFBcEIsRUFBMEJELE1BQTFCLENBQWlDLGtDQUFqQyxDQUFQOzs7QUFHSixBQUFPLFNBQVNxaUIsV0FBVCxHQUF3QjtRQUN2QjloQixJQUFJLEtBQUswZCxLQUFMLEdBQWE3ZCxHQUFiLEVBQVI7UUFDSSxJQUFJRyxFQUFFbU4sSUFBRixFQUFKLElBQWdCbk4sRUFBRW1OLElBQUYsTUFBWSxJQUFoQyxFQUFzQztZQUM5QnZJLFdBQVc1RixLQUFLUCxTQUFMLENBQWVxakIsV0FBMUIsQ0FBSixFQUE0Qzs7bUJBRWpDLEtBQUtDLE1BQUwsR0FBY0QsV0FBZCxFQUFQO1NBRkosTUFHTzttQkFDSWhZLGFBQWE5SixDQUFiLEVBQWdCLDhCQUFoQixDQUFQOztLQUxSLE1BT087ZUFDSThKLGFBQWE5SixDQUFiLEVBQWdCLGdDQUFoQixDQUFQOzs7Ozs7Ozs7O0FBVVIsQUFBTyxTQUFTZ2lCLE9BQVQsR0FBb0I7UUFDbkIsQ0FBQyxLQUFLM2hCLE9BQUwsRUFBTCxFQUFxQjtlQUNWLHVCQUF1QixLQUFLK0IsRUFBNUIsR0FBaUMsTUFBeEM7O1FBRUEzVCxPQUFPLFFBQVg7UUFDSVcsT0FBTyxFQUFYO1FBQ0ksQ0FBQyxLQUFLMnZCLE9BQUwsRUFBTCxFQUFxQjtlQUNWLEtBQUs5QixTQUFMLE9BQXFCLENBQXJCLEdBQXlCLFlBQXpCLEdBQXdDLGtCQUEvQztlQUNPLEdBQVA7O1FBRUFnRixTQUFTLE1BQU14ekIsSUFBTixHQUFhLEtBQTFCO1FBQ0kwZSxPQUFRLElBQUksS0FBS0EsSUFBTCxFQUFKLElBQW1CLEtBQUtBLElBQUwsTUFBZSxJQUFuQyxHQUEyQyxNQUEzQyxHQUFvRCxRQUEvRDtRQUNJK1UsV0FBVyx1QkFBZjtRQUNJQyxTQUFTL3lCLE9BQU8sTUFBcEI7O1dBRU8sS0FBS3FRLE1BQUwsQ0FBWXdpQixTQUFTOVUsSUFBVCxHQUFnQitVLFFBQWhCLEdBQTJCQyxNQUF2QyxDQUFQOzs7QUFHSixBQUFPLFNBQVMxaUIsTUFBVCxDQUFpQjJpQixXQUFqQixFQUE4QjtRQUM3QixDQUFDQSxXQUFMLEVBQWtCO3NCQUNBLEtBQUtuRCxLQUFMLEtBQWVoaEIsTUFBTTRqQixnQkFBckIsR0FBd0M1akIsTUFBTTJqQixhQUE1RDs7UUFFQWpjLFNBQVNtRSxhQUFhLElBQWIsRUFBbUJzWSxXQUFuQixDQUFiO1dBQ08sS0FBSzNZLFVBQUwsR0FBa0I0WSxVQUFsQixDQUE2QjFjLE1BQTdCLENBQVA7OztBQ2xERyxTQUFTM0QsSUFBVCxDQUFldWUsSUFBZixFQUFxQjNaLGFBQXJCLEVBQW9DO1FBQ25DLEtBQUt2RyxPQUFMLE9BQ015QyxTQUFTeWQsSUFBVCxLQUFrQkEsS0FBS2xnQixPQUFMLEVBQW5CLElBQ0E0WixZQUFZc0csSUFBWixFQUFrQmxnQixPQUFsQixFQUZMLENBQUosRUFFdUM7ZUFDNUI4ZCxlQUFlLEVBQUN4aUIsSUFBSSxJQUFMLEVBQVdxRyxNQUFNdWUsSUFBakIsRUFBZixFQUF1QzdnQixNQUF2QyxDQUE4QyxLQUFLQSxNQUFMLEVBQTlDLEVBQTZENGlCLFFBQTdELENBQXNFLENBQUMxYixhQUF2RSxDQUFQO0tBSEosTUFJTztlQUNJLEtBQUs2QyxVQUFMLEdBQWtCckQsV0FBbEIsRUFBUDs7OztBQUlSLEFBQU8sU0FBU21jLE9BQVQsQ0FBa0IzYixhQUFsQixFQUFpQztXQUM3QixLQUFLNUUsSUFBTCxDQUFVaVksYUFBVixFQUF5QnJULGFBQXpCLENBQVA7OztBQ1hHLFNBQVNqTCxFQUFULENBQWE0a0IsSUFBYixFQUFtQjNaLGFBQW5CLEVBQWtDO1FBQ2pDLEtBQUt2RyxPQUFMLE9BQ015QyxTQUFTeWQsSUFBVCxLQUFrQkEsS0FBS2xnQixPQUFMLEVBQW5CLElBQ0E0WixZQUFZc0csSUFBWixFQUFrQmxnQixPQUFsQixFQUZMLENBQUosRUFFdUM7ZUFDNUI4ZCxlQUFlLEVBQUNuYyxNQUFNLElBQVAsRUFBYXJHLElBQUk0a0IsSUFBakIsRUFBZixFQUF1QzdnQixNQUF2QyxDQUE4QyxLQUFLQSxNQUFMLEVBQTlDLEVBQTZENGlCLFFBQTdELENBQXNFLENBQUMxYixhQUF2RSxDQUFQO0tBSEosTUFJTztlQUNJLEtBQUs2QyxVQUFMLEdBQWtCckQsV0FBbEIsRUFBUDs7OztBQUlSLEFBQU8sU0FBU29jLEtBQVQsQ0FBZ0I1YixhQUFoQixFQUErQjtXQUMzQixLQUFLakwsRUFBTCxDQUFRc2UsYUFBUixFQUF1QnJULGFBQXZCLENBQVA7OztBQ1pKOzs7QUFHQSxBQUFPLFNBQVNsSCxNQUFULENBQWlCclMsR0FBakIsRUFBc0I7UUFDckJvMUIsYUFBSjs7UUFFSXAxQixRQUFRb1UsU0FBWixFQUF1QjtlQUNaLEtBQUtpQixPQUFMLENBQWFzVSxLQUFwQjtLQURKLE1BRU87d0JBQ2FHLFVBQVU5cEIsR0FBVixDQUFoQjtZQUNJbzFCLGlCQUFpQixJQUFyQixFQUEyQjtpQkFDbEIvZixPQUFMLEdBQWUrZixhQUFmOztlQUVHLElBQVA7Ozs7QUFJUixBQUFPLElBQUlDLE9BQU8xZSxVQUNkLGlKQURjLEVBRWQsVUFBVTNXLEdBQVYsRUFBZTtRQUNQQSxRQUFRb1UsU0FBWixFQUF1QjtlQUNaLEtBQUtnSSxVQUFMLEVBQVA7S0FESixNQUVPO2VBQ0ksS0FBSy9KLE1BQUwsQ0FBWXJTLEdBQVosQ0FBUDs7Q0FOTSxDQUFYOztBQVdQLEFBQU8sU0FBU29jLFVBQVQsR0FBdUI7V0FDbkIsS0FBSy9HLE9BQVo7OztBQzlCRyxTQUFTZ2UsT0FBVCxDQUFrQmpaLEtBQWxCLEVBQXlCO1lBQ3BCRCxlQUFlQyxLQUFmLENBQVI7OztZQUdRQSxLQUFSO2FBQ1MsTUFBTDtpQkFDUzJGLEtBQUwsQ0FBVyxDQUFYOzthQUVDLFNBQUw7YUFDSyxPQUFMO2lCQUNTNEIsSUFBTCxDQUFVLENBQVY7O2FBRUMsTUFBTDthQUNLLFNBQUw7YUFDSyxLQUFMO2FBQ0ssTUFBTDtpQkFDU21HLEtBQUwsQ0FBVyxDQUFYOzthQUVDLE1BQUw7aUJBQ1NFLE9BQUwsQ0FBYSxDQUFiOzthQUVDLFFBQUw7aUJBQ1NDLE9BQUwsQ0FBYSxDQUFiOzthQUVDLFFBQUw7aUJBQ1NtSCxZQUFMLENBQWtCLENBQWxCOzs7O1FBSUpoVixVQUFVLE1BQWQsRUFBc0I7YUFDYjJKLE9BQUwsQ0FBYSxDQUFiOztRQUVBM0osVUFBVSxTQUFkLEVBQXlCO2FBQ2hCa2IsVUFBTCxDQUFnQixDQUFoQjs7OztRQUlBbGIsVUFBVSxTQUFkLEVBQXlCO2FBQ2hCMkYsS0FBTCxDQUFXcmdCLEtBQUtrVyxLQUFMLENBQVcsS0FBS21LLEtBQUwsS0FBZSxDQUExQixJQUErQixDQUExQzs7O1dBR0csSUFBUDs7O0FBR0osQUFBTyxTQUFTeVQsS0FBVCxDQUFnQnBaLEtBQWhCLEVBQXVCO1lBQ2xCRCxlQUFlQyxLQUFmLENBQVI7UUFDSUEsVUFBVWhHLFNBQVYsSUFBdUJnRyxVQUFVLGFBQXJDLEVBQW9EO2VBQ3pDLElBQVA7Ozs7UUFJQUEsVUFBVSxNQUFkLEVBQXNCO2dCQUNWLEtBQVI7OztXQUdHLEtBQUtpWixPQUFMLENBQWFqWixLQUFiLEVBQW9CMkssR0FBcEIsQ0FBd0IsQ0FBeEIsRUFBNEIzSyxVQUFVLFNBQVYsR0FBc0IsTUFBdEIsR0FBK0JBLEtBQTNELEVBQW1FOFcsUUFBbkUsQ0FBNEUsQ0FBNUUsRUFBK0UsSUFBL0UsQ0FBUDs7O0FDekRHLFNBQVNoZixPQUFULEdBQW9CO1dBQ2hCLEtBQUtvQixFQUFMLENBQVFwQixPQUFSLEtBQXFCLENBQUMsS0FBS2tELE9BQUwsSUFBZ0IsQ0FBakIsSUFBc0IsS0FBbEQ7OztBQUdKLEFBQU8sU0FBU21nQixJQUFULEdBQWlCO1dBQ2I3MUIsS0FBS2tXLEtBQUwsQ0FBVyxLQUFLMUQsT0FBTCxLQUFpQixJQUE1QixDQUFQOzs7QUFHSixBQUFPLFNBQVN3aUIsTUFBVCxHQUFtQjtXQUNmLElBQUkvaUIsSUFBSixDQUFTLEtBQUtPLE9BQUwsRUFBVCxDQUFQOzs7QUFHSixBQUFPLFNBQVN1ZixTQUFULEdBQW9CO1FBQ25COWUsSUFBSSxJQUFSO1dBQ08sQ0FBQ0EsRUFBRW1OLElBQUYsRUFBRCxFQUFXbk4sRUFBRW9OLEtBQUYsRUFBWCxFQUFzQnBOLEVBQUVnUCxJQUFGLEVBQXRCLEVBQWdDaFAsRUFBRTRhLElBQUYsRUFBaEMsRUFBMEM1YSxFQUFFc2IsTUFBRixFQUExQyxFQUFzRHRiLEVBQUV1YixNQUFGLEVBQXRELEVBQWtFdmIsRUFBRXdiLFdBQUYsRUFBbEUsQ0FBUDs7O0FBR0osQUFBTyxTQUFTcUgsUUFBVCxHQUFxQjtRQUNwQjdpQixJQUFJLElBQVI7V0FDTztlQUNJQSxFQUFFbU4sSUFBRixFQURKO2dCQUVLbk4sRUFBRW9OLEtBQUYsRUFGTDtjQUdHcE4sRUFBRWdQLElBQUYsRUFISDtlQUlJaFAsRUFBRW1WLEtBQUYsRUFKSjtpQkFLTW5WLEVBQUVxVixPQUFGLEVBTE47aUJBTU1yVixFQUFFc1YsT0FBRixFQU5OO3NCQU9XdFYsRUFBRXljLFlBQUY7S0FQbEI7OztBQVdKLEFBQU8sU0FBU3FHLE1BQVQsR0FBbUI7O1dBRWYsS0FBS3ppQixPQUFMLEtBQWlCLEtBQUt5aEIsV0FBTCxFQUFqQixHQUFzQyxJQUE3Qzs7O0FDNUJHLFNBQVN6aEIsU0FBVCxHQUFvQjtXQUNoQkMsUUFBUyxJQUFULENBQVA7OztBQUdKLEFBQU8sU0FBU3lpQixZQUFULEdBQXlCO1dBQ3JCempCLE9BQU8sRUFBUCxFQUFXUyxnQkFBZ0IsSUFBaEIsQ0FBWCxDQUFQOzs7QUFHSixBQUFPLFNBQVNpakIsU0FBVCxHQUFzQjtXQUNsQmpqQixnQkFBZ0IsSUFBaEIsRUFBc0JjLFFBQTdCOzs7QUNiRyxTQUFTb2lCLFlBQVQsR0FBd0I7V0FDcEI7ZUFDSSxLQUFLN2dCLEVBRFQ7Z0JBRUssS0FBS0MsRUFGVjtnQkFHSyxLQUFLSyxPQUhWO2VBSUksS0FBS0YsTUFKVDtnQkFLSyxLQUFLbkI7S0FMakI7OztBQ1VKOztBQUVBaUksZUFBZSxDQUFmLEVBQWtCLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FBbEIsRUFBNkIsQ0FBN0IsRUFBZ0MsWUFBWTtXQUNqQyxLQUFLcVEsUUFBTCxLQUFrQixHQUF6QjtDQURKOztBQUlBclEsZUFBZSxDQUFmLEVBQWtCLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FBbEIsRUFBNkIsQ0FBN0IsRUFBZ0MsWUFBWTtXQUNqQyxLQUFLNFosV0FBTCxLQUFxQixHQUE1QjtDQURKOztBQUlBLFNBQVNDLHNCQUFULENBQWlDNVosS0FBakMsRUFBd0M2WixNQUF4QyxFQUFnRDttQkFDN0IsQ0FBZixFQUFrQixDQUFDN1osS0FBRCxFQUFRQSxNQUFNdGMsTUFBZCxDQUFsQixFQUF5QyxDQUF6QyxFQUE0Q20yQixNQUE1Qzs7O0FBR0pELHVCQUF1QixNQUF2QixFQUFtQyxVQUFuQztBQUNBQSx1QkFBdUIsT0FBdkIsRUFBbUMsVUFBbkM7QUFDQUEsdUJBQXVCLE1BQXZCLEVBQWdDLGFBQWhDO0FBQ0FBLHVCQUF1QixPQUF2QixFQUFnQyxhQUFoQzs7OztBQUlBaGMsYUFBYSxVQUFiLEVBQXlCLElBQXpCO0FBQ0FBLGFBQWEsYUFBYixFQUE0QixJQUE1Qjs7OztBQUlBWSxnQkFBZ0IsVUFBaEIsRUFBNEIsQ0FBNUI7QUFDQUEsZ0JBQWdCLGFBQWhCLEVBQStCLENBQS9COzs7O0FBS0FzRCxjQUFjLEdBQWQsRUFBd0JOLFdBQXhCO0FBQ0FNLGNBQWMsR0FBZCxFQUF3Qk4sV0FBeEI7QUFDQU0sY0FBYyxJQUFkLEVBQXdCYixTQUF4QixFQUFtQ0osTUFBbkM7QUFDQWlCLGNBQWMsSUFBZCxFQUF3QmIsU0FBeEIsRUFBbUNKLE1BQW5DO0FBQ0FpQixjQUFjLE1BQWQsRUFBd0JULFNBQXhCLEVBQW1DTixNQUFuQztBQUNBZSxjQUFjLE1BQWQsRUFBd0JULFNBQXhCLEVBQW1DTixNQUFuQztBQUNBZSxjQUFjLE9BQWQsRUFBd0JSLFNBQXhCLEVBQW1DTixNQUFuQztBQUNBYyxjQUFjLE9BQWQsRUFBd0JSLFNBQXhCLEVBQW1DTixNQUFuQzs7QUFFQTZCLGtCQUFrQixDQUFDLE1BQUQsRUFBUyxPQUFULEVBQWtCLE1BQWxCLEVBQTBCLE9BQTFCLENBQWxCLEVBQXNELFVBQVU3TixLQUFWLEVBQWlCNFMsSUFBakIsRUFBdUJ0Z0IsTUFBdkIsRUFBK0IwWSxLQUEvQixFQUFzQztTQUNuRkEsTUFBTWxTLE1BQU4sQ0FBYSxDQUFiLEVBQWdCLENBQWhCLENBQUwsSUFBMkI2TCxNQUFNM0UsS0FBTixDQUEzQjtDQURKOztBQUlBNk4sa0JBQWtCLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FBbEIsRUFBZ0MsVUFBVTdOLEtBQVYsRUFBaUI0UyxJQUFqQixFQUF1QnRnQixNQUF2QixFQUErQjBZLEtBQS9CLEVBQXNDO1NBQzdEQSxLQUFMLElBQWN0TCxNQUFNNlIsaUJBQU4sQ0FBd0J2UixLQUF4QixDQUFkO0NBREo7Ozs7QUFNQSxBQUFPLFNBQVM4a0IsY0FBVCxDQUF5QjlrQixLQUF6QixFQUFnQztXQUM1QitrQixxQkFBcUJscEIsSUFBckIsQ0FBMEIsSUFBMUIsRUFDQ21FLEtBREQsRUFFQyxLQUFLNFMsSUFBTCxFQUZELEVBR0MsS0FBS0MsT0FBTCxFQUhELEVBSUMsS0FBSzNILFVBQUwsR0FBa0JzSSxLQUFsQixDQUF3QmxCLEdBSnpCLEVBS0MsS0FBS3BILFVBQUwsR0FBa0JzSSxLQUFsQixDQUF3QmpCLEdBTHpCLENBQVA7OztBQVFKLEFBQU8sU0FBU3lTLGlCQUFULENBQTRCaGxCLEtBQTVCLEVBQW1DO1dBQy9CK2tCLHFCQUFxQmxwQixJQUFyQixDQUEwQixJQUExQixFQUNDbUUsS0FERCxFQUNRLEtBQUtpbEIsT0FBTCxFQURSLEVBQ3dCLEtBQUtiLFVBQUwsRUFEeEIsRUFDMkMsQ0FEM0MsRUFDOEMsQ0FEOUMsQ0FBUDs7O0FBSUosQUFBTyxTQUFTYyxpQkFBVCxHQUE4QjtXQUMxQjdSLFlBQVksS0FBS3pFLElBQUwsRUFBWixFQUF5QixDQUF6QixFQUE0QixDQUE1QixDQUFQOzs7QUFHSixBQUFPLFNBQVN1VyxjQUFULEdBQTJCO1FBQzFCQyxXQUFXLEtBQUtsYSxVQUFMLEdBQWtCc0ksS0FBakM7V0FDT0gsWUFBWSxLQUFLekUsSUFBTCxFQUFaLEVBQXlCd1csU0FBUzlTLEdBQWxDLEVBQXVDOFMsU0FBUzdTLEdBQWhELENBQVA7OztBQUdKLFNBQVN3UyxvQkFBVCxDQUE4Qi9rQixLQUE5QixFQUFxQzRTLElBQXJDLEVBQTJDQyxPQUEzQyxFQUFvRFAsR0FBcEQsRUFBeURDLEdBQXpELEVBQThEO1FBQ3REOFMsV0FBSjtRQUNJcmxCLFNBQVMsSUFBYixFQUFtQjtlQUNSbVQsV0FBVyxJQUFYLEVBQWlCYixHQUFqQixFQUFzQkMsR0FBdEIsRUFBMkIzRCxJQUFsQztLQURKLE1BRU87c0JBQ1d5RSxZQUFZclQsS0FBWixFQUFtQnNTLEdBQW5CLEVBQXdCQyxHQUF4QixDQUFkO1lBQ0lLLE9BQU95UyxXQUFYLEVBQXdCO21CQUNiQSxXQUFQOztlQUVHQyxXQUFXenBCLElBQVgsQ0FBZ0IsSUFBaEIsRUFBc0JtRSxLQUF0QixFQUE2QjRTLElBQTdCLEVBQW1DQyxPQUFuQyxFQUE0Q1AsR0FBNUMsRUFBaURDLEdBQWpELENBQVA7Ozs7QUFJUixTQUFTK1MsVUFBVCxDQUFvQmxLLFFBQXBCLEVBQThCeEksSUFBOUIsRUFBb0NDLE9BQXBDLEVBQTZDUCxHQUE3QyxFQUFrREMsR0FBbEQsRUFBdUQ7UUFDL0NnVCxnQkFBZ0I1UyxtQkFBbUJ5SSxRQUFuQixFQUE2QnhJLElBQTdCLEVBQW1DQyxPQUFuQyxFQUE0Q1AsR0FBNUMsRUFBaURDLEdBQWpELENBQXBCO1FBQ0k5QixPQUFPeUIsY0FBY3FULGNBQWMzVyxJQUE1QixFQUFrQyxDQUFsQyxFQUFxQzJXLGNBQWN2UyxTQUFuRCxDQURYOztTQUdLcEUsSUFBTCxDQUFVNkIsS0FBSzBCLGNBQUwsRUFBVjtTQUNLdEQsS0FBTCxDQUFXNEIsS0FBS2dLLFdBQUwsRUFBWDtTQUNLaEssSUFBTCxDQUFVQSxLQUFLMUIsVUFBTCxFQUFWO1dBQ08sSUFBUDs7O0FDakdKOztBQUVBaEUsZUFBZSxHQUFmLEVBQW9CLENBQXBCLEVBQXVCLElBQXZCLEVBQTZCLFNBQTdCOzs7O0FBSUFuQyxhQUFhLFNBQWIsRUFBd0IsR0FBeEI7Ozs7QUFJQVksZ0JBQWdCLFNBQWhCLEVBQTJCLENBQTNCOzs7O0FBSUFzRCxjQUFjLEdBQWQsRUFBbUJsQixNQUFuQjtBQUNBZ0MsY0FBYyxHQUFkLEVBQW1CLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0I7VUFDakM0QyxLQUFOLElBQWUsQ0FBQ3ZKLE1BQU0zRSxLQUFOLElBQWUsQ0FBaEIsSUFBcUIsQ0FBcEM7Q0FESjs7OztBQU1BLEFBQU8sU0FBU3dsQixhQUFULENBQXdCeGxCLEtBQXhCLEVBQStCO1dBQzNCQSxTQUFTLElBQVQsR0FBZ0J4UixLQUFLNlEsSUFBTCxDQUFVLENBQUMsS0FBS3dQLEtBQUwsS0FBZSxDQUFoQixJQUFxQixDQUEvQixDQUFoQixHQUFvRCxLQUFLQSxLQUFMLENBQVcsQ0FBQzdPLFFBQVEsQ0FBVCxJQUFjLENBQWQsR0FBa0IsS0FBSzZPLEtBQUwsS0FBZSxDQUE1QyxDQUEzRDs7O0FDckJKOztBQUVBOUQsZUFBZSxHQUFmLEVBQW9CLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FBcEIsRUFBK0IsSUFBL0IsRUFBcUMsTUFBckM7Ozs7QUFJQW5DLGFBQWEsTUFBYixFQUFxQixHQUFyQjs7O0FBR0FZLGdCQUFnQixNQUFoQixFQUF3QixDQUF4Qjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQW9CYixTQUFwQjtBQUNBYSxjQUFjLElBQWQsRUFBb0JiLFNBQXBCLEVBQStCSixNQUEvQjtBQUNBaUIsY0FBYyxJQUFkLEVBQW9CLFVBQVVHLFFBQVYsRUFBb0I5TCxNQUFwQixFQUE0QjtXQUNyQzhMLFdBQVc5TCxPQUFPd0YsYUFBbEIsR0FBa0N4RixPQUFPc0Ysb0JBQWhEO0NBREo7O0FBSUFtSCxjQUFjLENBQUMsR0FBRCxFQUFNLElBQU4sQ0FBZCxFQUEyQk8sSUFBM0I7QUFDQVAsY0FBYyxJQUFkLEVBQW9CLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0I7VUFDbEM2QyxJQUFOLElBQWN4SixNQUFNM0UsTUFBTW9MLEtBQU4sQ0FBWWEsU0FBWixFQUF1QixDQUF2QixDQUFOLEVBQWlDLEVBQWpDLENBQWQ7Q0FESjs7OztBQU1BLEFBQU8sSUFBSXdaLG1CQUFtQjNiLFdBQVcsTUFBWCxFQUFtQixJQUFuQixDQUF2Qjs7QUMxQlA7O0FBRUFpQixlQUFlLEtBQWYsRUFBc0IsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQUF0QixFQUFtQyxNQUFuQyxFQUEyQyxXQUEzQzs7OztBQUlBbkMsYUFBYSxXQUFiLEVBQTBCLEtBQTFCOzs7QUFHQVksZ0JBQWdCLFdBQWhCLEVBQTZCLENBQTdCOzs7O0FBSUFzRCxjQUFjLEtBQWQsRUFBc0JWLFNBQXRCO0FBQ0FVLGNBQWMsTUFBZCxFQUFzQmhCLE1BQXRCO0FBQ0E4QixjQUFjLENBQUMsS0FBRCxFQUFRLE1BQVIsQ0FBZCxFQUErQixVQUFVNU4sS0FBVixFQUFpQnNMLEtBQWpCLEVBQXdCaFosTUFBeEIsRUFBZ0M7V0FDcER5b0IsVUFBUCxHQUFvQnBXLE1BQU0zRSxLQUFOLENBQXBCO0NBREo7Ozs7OztBQVFBLEFBQU8sU0FBUzBsQixlQUFULENBQTBCMWxCLEtBQTFCLEVBQWlDO1FBQ2hDZ1QsWUFBWXhrQixLQUFLeUssS0FBTCxDQUFXLENBQUMsS0FBS2ttQixLQUFMLEdBQWFnRCxPQUFiLENBQXFCLEtBQXJCLElBQThCLEtBQUtoRCxLQUFMLEdBQWFnRCxPQUFiLENBQXFCLE1BQXJCLENBQS9CLElBQStELEtBQTFFLElBQW1GLENBQW5HO1dBQ09uaUIsU0FBUyxJQUFULEdBQWdCZ1QsU0FBaEIsR0FBNEIsS0FBS2EsR0FBTCxDQUFVN1QsUUFBUWdULFNBQWxCLEVBQThCLEdBQTlCLENBQW5DOzs7QUMxQko7O0FBRUFqSSxlQUFlLEdBQWYsRUFBb0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFwQixFQUErQixDQUEvQixFQUFrQyxRQUFsQzs7OztBQUlBbkMsYUFBYSxRQUFiLEVBQXVCLEdBQXZCOzs7O0FBSUFZLGdCQUFnQixRQUFoQixFQUEwQixFQUExQjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQW9CYixTQUFwQjtBQUNBYSxjQUFjLElBQWQsRUFBb0JiLFNBQXBCLEVBQStCSixNQUEvQjtBQUNBK0IsY0FBYyxDQUFDLEdBQUQsRUFBTSxJQUFOLENBQWQsRUFBMkJTLE1BQTNCOzs7O0FBSUEsQUFBTyxJQUFJc1gsZUFBZTdiLFdBQVcsU0FBWCxFQUFzQixLQUF0QixDQUFuQjs7QUNwQlA7O0FBRUFpQixlQUFlLEdBQWYsRUFBb0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFwQixFQUErQixDQUEvQixFQUFrQyxRQUFsQzs7OztBQUlBbkMsYUFBYSxRQUFiLEVBQXVCLEdBQXZCOzs7O0FBSUFZLGdCQUFnQixRQUFoQixFQUEwQixFQUExQjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQW9CYixTQUFwQjtBQUNBYSxjQUFjLElBQWQsRUFBb0JiLFNBQXBCLEVBQStCSixNQUEvQjtBQUNBK0IsY0FBYyxDQUFDLEdBQUQsRUFBTSxJQUFOLENBQWQsRUFBMkJVLE1BQTNCOzs7O0FBSUEsQUFBTyxJQUFJc1gsZUFBZTliLFdBQVcsU0FBWCxFQUFzQixLQUF0QixDQUFuQjs7QUNuQlA7O0FBRUFpQixlQUFlLEdBQWYsRUFBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsRUFBMEIsWUFBWTtXQUMzQixDQUFDLEVBQUUsS0FBS2tTLFdBQUwsS0FBcUIsR0FBdkIsQ0FBUjtDQURKOztBQUlBbFMsZUFBZSxDQUFmLEVBQWtCLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FBbEIsRUFBNkIsQ0FBN0IsRUFBZ0MsWUFBWTtXQUNqQyxDQUFDLEVBQUUsS0FBS2tTLFdBQUwsS0FBcUIsRUFBdkIsQ0FBUjtDQURKOztBQUlBbFMsZUFBZSxDQUFmLEVBQWtCLENBQUMsS0FBRCxFQUFRLENBQVIsQ0FBbEIsRUFBOEIsQ0FBOUIsRUFBaUMsYUFBakM7QUFDQUEsZUFBZSxDQUFmLEVBQWtCLENBQUMsTUFBRCxFQUFTLENBQVQsQ0FBbEIsRUFBK0IsQ0FBL0IsRUFBa0MsWUFBWTtXQUNuQyxLQUFLa1MsV0FBTCxLQUFxQixFQUE1QjtDQURKO0FBR0FsUyxlQUFlLENBQWYsRUFBa0IsQ0FBQyxPQUFELEVBQVUsQ0FBVixDQUFsQixFQUFnQyxDQUFoQyxFQUFtQyxZQUFZO1dBQ3BDLEtBQUtrUyxXQUFMLEtBQXFCLEdBQTVCO0NBREo7QUFHQWxTLGVBQWUsQ0FBZixFQUFrQixDQUFDLFFBQUQsRUFBVyxDQUFYLENBQWxCLEVBQWlDLENBQWpDLEVBQW9DLFlBQVk7V0FDckMsS0FBS2tTLFdBQUwsS0FBcUIsSUFBNUI7Q0FESjtBQUdBbFMsZUFBZSxDQUFmLEVBQWtCLENBQUMsU0FBRCxFQUFZLENBQVosQ0FBbEIsRUFBa0MsQ0FBbEMsRUFBcUMsWUFBWTtXQUN0QyxLQUFLa1MsV0FBTCxLQUFxQixLQUE1QjtDQURKO0FBR0FsUyxlQUFlLENBQWYsRUFBa0IsQ0FBQyxVQUFELEVBQWEsQ0FBYixDQUFsQixFQUFtQyxDQUFuQyxFQUFzQyxZQUFZO1dBQ3ZDLEtBQUtrUyxXQUFMLEtBQXFCLE1BQTVCO0NBREo7QUFHQWxTLGVBQWUsQ0FBZixFQUFrQixDQUFDLFdBQUQsRUFBYyxDQUFkLENBQWxCLEVBQW9DLENBQXBDLEVBQXVDLFlBQVk7V0FDeEMsS0FBS2tTLFdBQUwsS0FBcUIsT0FBNUI7Q0FESjs7OztBQU9BclUsYUFBYSxhQUFiLEVBQTRCLElBQTVCOzs7O0FBSUFZLGdCQUFnQixhQUFoQixFQUErQixFQUEvQjs7OztBQUlBc0QsY0FBYyxHQUFkLEVBQXNCVixTQUF0QixFQUFpQ1IsTUFBakM7QUFDQWtCLGNBQWMsSUFBZCxFQUFzQlYsU0FBdEIsRUFBaUNQLE1BQWpDO0FBQ0FpQixjQUFjLEtBQWQsRUFBc0JWLFNBQXRCLEVBQWlDTixNQUFqQzs7QUFFQSxJQUFJZCxLQUFKO0FBQ0EsS0FBS0EsUUFBUSxNQUFiLEVBQXFCQSxNQUFNdGMsTUFBTixJQUFnQixDQUFyQyxFQUF3Q3NjLFNBQVMsR0FBakQsRUFBc0Q7a0JBQ3BDQSxLQUFkLEVBQXFCdUIsYUFBckI7OztBQUdKLFNBQVNzWixPQUFULENBQWlCN2xCLEtBQWpCLEVBQXdCc0wsS0FBeEIsRUFBK0I7VUFDckJpRCxXQUFOLElBQXFCNUosTUFBTSxDQUFDLE9BQU8zRSxLQUFSLElBQWlCLElBQXZCLENBQXJCOzs7QUFHSixLQUFLZ0wsUUFBUSxHQUFiLEVBQWtCQSxNQUFNdGMsTUFBTixJQUFnQixDQUFsQyxFQUFxQ3NjLFNBQVMsR0FBOUMsRUFBbUQ7a0JBQ2pDQSxLQUFkLEVBQXFCNmEsT0FBckI7Ozs7QUFJSixBQUFPLElBQUlDLG9CQUFvQmhjLFdBQVcsY0FBWCxFQUEyQixLQUEzQixDQUF4Qjs7QUNsRVA7O0FBRUFpQixlQUFlLEdBQWYsRUFBcUIsQ0FBckIsRUFBd0IsQ0FBeEIsRUFBMkIsVUFBM0I7QUFDQUEsZUFBZSxJQUFmLEVBQXFCLENBQXJCLEVBQXdCLENBQXhCLEVBQTJCLFVBQTNCOzs7O0FBSUEsQUFBTyxTQUFTZ2IsV0FBVCxHQUF3QjtXQUNwQixLQUFLOWhCLE1BQUwsR0FBYyxLQUFkLEdBQXNCLEVBQTdCOzs7QUFHSixBQUFPLFNBQVMraEIsV0FBVCxHQUF3QjtXQUNwQixLQUFLL2hCLE1BQUwsR0FBYyw0QkFBZCxHQUE2QyxFQUFwRDs7O0FDWkosSUFBSWdpQixRQUFRNWhCLE9BQU9uRSxTQUFuQjs7QUFFQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFFQStsQixNQUFNcFMsR0FBTixHQUEwQkEsR0FBMUI7QUFDQW9TLE1BQU1oZixRQUFOLEdBQTBCQSxVQUExQjtBQUNBZ2YsTUFBTTlHLEtBQU4sR0FBMEJBLEtBQTFCO0FBQ0E4RyxNQUFNdmQsSUFBTixHQUEwQkEsSUFBMUI7QUFDQXVkLE1BQU0zRCxLQUFOLEdBQTBCQSxLQUExQjtBQUNBMkQsTUFBTS9rQixNQUFOLEdBQTBCQSxNQUExQjtBQUNBK2tCLE1BQU14aUIsSUFBTixHQUEwQkEsSUFBMUI7QUFDQXdpQixNQUFNakMsT0FBTixHQUEwQkEsT0FBMUI7QUFDQWlDLE1BQU03b0IsRUFBTixHQUEwQkEsRUFBMUI7QUFDQTZvQixNQUFNaEMsS0FBTixHQUEwQkEsS0FBMUI7QUFDQWdDLE1BQU1qYyxHQUFOLEdBQTBCQyxTQUExQjtBQUNBZ2MsTUFBTXhCLFNBQU4sR0FBMEJBLFNBQTFCO0FBQ0F3QixNQUFNNUUsT0FBTixHQUEwQkEsT0FBMUI7QUFDQTRFLE1BQU0zRSxRQUFOLEdBQTBCQSxRQUExQjtBQUNBMkUsTUFBTTFELFNBQU4sR0FBMEJBLFNBQTFCO0FBQ0EwRCxNQUFNeEQsTUFBTixHQUEwQkEsTUFBMUI7QUFDQXdELE1BQU10RCxhQUFOLEdBQTBCQSxhQUExQjtBQUNBc0QsTUFBTXJELGNBQU4sR0FBMEJBLGNBQTFCO0FBQ0FxRCxNQUFNbmtCLE9BQU4sR0FBMEJBLFNBQTFCO0FBQ0Fta0IsTUFBTTlCLElBQU4sR0FBMEJBLElBQTFCO0FBQ0E4QixNQUFNOWtCLE1BQU4sR0FBMEJBLE1BQTFCO0FBQ0E4a0IsTUFBTS9hLFVBQU4sR0FBMEJBLFVBQTFCO0FBQ0ErYSxNQUFNeDNCLEdBQU4sR0FBMEJndkIsWUFBMUI7QUFDQXdJLE1BQU12d0IsR0FBTixHQUEwQjZuQixZQUExQjtBQUNBMEksTUFBTXpCLFlBQU4sR0FBMEJBLFlBQTFCO0FBQ0F5QixNQUFNMWYsR0FBTixHQUEwQjJELFNBQTFCO0FBQ0ErYixNQUFNOUQsT0FBTixHQUEwQkEsT0FBMUI7QUFDQThELE1BQU1qRyxRQUFOLEdBQTBCQSxRQUExQjtBQUNBaUcsTUFBTTFGLE9BQU4sR0FBMEJBLFNBQTFCO0FBQ0EwRixNQUFNM0IsUUFBTixHQUEwQkEsUUFBMUI7QUFDQTJCLE1BQU16QyxNQUFOLEdBQTBCQSxNQUExQjtBQUNBeUMsTUFBTTFDLFdBQU4sR0FBMEJBLFdBQTFCO0FBQ0EwQyxNQUFNeEMsT0FBTixHQUEwQkEsT0FBMUI7QUFDQXdDLE1BQU0xQixNQUFOLEdBQTBCQSxNQUExQjtBQUNBMEIsTUFBTTlsQixRQUFOLEdBQTBCQSxRQUExQjtBQUNBOGxCLE1BQU01QixJQUFOLEdBQTBCQSxJQUExQjtBQUNBNEIsTUFBTWpsQixPQUFOLEdBQTBCQSxPQUExQjtBQUNBaWxCLE1BQU12QixZQUFOLEdBQTBCQSxZQUExQjs7O0FBR0EsQUFDQXVCLE1BQU1yWCxJQUFOLEdBQW1CK0MsVUFBbkI7QUFDQXNVLE1BQU12VSxVQUFOLEdBQW1CRSxhQUFuQjs7O0FBR0EsQUFDQXFVLE1BQU03SyxRQUFOLEdBQW9CMEosY0FBcEI7QUFDQW1CLE1BQU10QixXQUFOLEdBQW9CSyxpQkFBcEI7OztBQUdBLEFBQ0FpQixNQUFNbEksT0FBTixHQUFnQmtJLE1BQU1uSSxRQUFOLEdBQWlCMEgsYUFBakM7OztBQUdBLEFBQ0FTLE1BQU1wWCxLQUFOLEdBQW9CNkIsV0FBcEI7QUFDQXVWLE1BQU10WCxXQUFOLEdBQW9CZ0MsY0FBcEI7OztBQUdBLEFBQ0FzVixNQUFNclQsSUFBTixHQUF1QnFULE1BQU1qSSxLQUFOLEdBQXFCcEssVUFBNUM7QUFDQXFTLE1BQU1oQixPQUFOLEdBQXVCZ0IsTUFBTUMsUUFBTixHQUFxQnBTLGFBQTVDO0FBQ0FtUyxNQUFNNVMsV0FBTixHQUF1QjhSLGNBQXZCO0FBQ0FjLE1BQU1FLGNBQU4sR0FBdUJqQixpQkFBdkI7OztBQUdBLEFBQ0EsQUFDQSxBQUNBZSxNQUFNeFYsSUFBTixHQUFtQmdWLGdCQUFuQjtBQUNBUSxNQUFNdFIsR0FBTixHQUFtQnNSLE1BQU1oSSxJQUFOLEdBQXlCeEksZUFBNUM7QUFDQXdRLE1BQU1wVCxPQUFOLEdBQW1COEMscUJBQW5CO0FBQ0FzUSxNQUFNN0IsVUFBTixHQUFtQnhPLGtCQUFuQjtBQUNBcVEsTUFBTWpULFNBQU4sR0FBbUIwUyxlQUFuQjs7O0FBR0EsQUFDQU8sTUFBTTVKLElBQU4sR0FBYTRKLE1BQU1yUCxLQUFOLEdBQWNpQixVQUEzQjs7O0FBR0EsQUFDQW9PLE1BQU1sSixNQUFOLEdBQWVrSixNQUFNblAsT0FBTixHQUFnQjZPLFlBQS9COzs7QUFHQSxBQUNBTSxNQUFNakosTUFBTixHQUFlaUosTUFBTWxQLE9BQU4sR0FBZ0I2TyxZQUEvQjs7O0FBR0EsQUFDQUssTUFBTWhKLFdBQU4sR0FBb0JnSixNQUFNL0gsWUFBTixHQUFxQjRILGlCQUF6Qzs7O0FBR0EsQUFhQUcsTUFBTXZILFNBQU4sR0FBNkJjLFlBQTdCO0FBQ0F5RyxNQUFNM2tCLEdBQU4sR0FBNkJ3ZSxjQUE3QjtBQUNBbUcsTUFBTTVHLEtBQU4sR0FBNkJVLGdCQUE3QjtBQUNBa0csTUFBTUcsU0FBTixHQUE2Qm5HLHVCQUE3QjtBQUNBZ0csTUFBTTlGLG9CQUFOLEdBQTZCQSxvQkFBN0I7QUFDQThGLE1BQU1JLEtBQU4sR0FBNkJqRyxvQkFBN0I7QUFDQTZGLE1BQU16RixPQUFOLEdBQTZCQSxPQUE3QjtBQUNBeUYsTUFBTXhGLFdBQU4sR0FBNkJBLFdBQTdCO0FBQ0F3RixNQUFNdkYsS0FBTixHQUE2QkEsS0FBN0I7QUFDQXVGLE1BQU0zSSxLQUFOLEdBQTZCb0QsS0FBN0I7OztBQUdBLEFBQ0F1RixNQUFNSyxRQUFOLEdBQWlCUCxXQUFqQjtBQUNBRSxNQUFNTSxRQUFOLEdBQWlCUCxXQUFqQjs7O0FBR0EsQUFDQUMsTUFBTU8sS0FBTixHQUFlL2dCLFVBQVUsaURBQVYsRUFBNkRnZ0IsZ0JBQTdELENBQWY7QUFDQVEsTUFBTWhYLE1BQU4sR0FBZXhKLFVBQVUsa0RBQVYsRUFBOERpTCxXQUE5RCxDQUFmO0FBQ0F1VixNQUFNcEksS0FBTixHQUFlcFksVUFBVSxnREFBVixFQUE0RGtNLFVBQTVELENBQWY7QUFDQXNVLE1BQU1wMUIsSUFBTixHQUFlNFUsVUFBVSwwR0FBVixFQUFzSG9hLFVBQXRILENBQWY7QUFDQW9HLE1BQU1RLFlBQU4sR0FBcUJoaEIsVUFBVSx5R0FBVixFQUFxSDRhLDJCQUFySCxDQUFyQixDQUVBOztBQzdJQSxTQUFTcUcsVUFBVCxDQUFxQjFtQixLQUFyQixFQUE0QjtXQUNqQjBiLFlBQVkxYixRQUFRLElBQXBCLENBQVA7OztBQUdKLFNBQVMybUIsWUFBVCxHQUF5QjtXQUNkakwsWUFBWS9iLEtBQVosQ0FBa0IsSUFBbEIsRUFBd0JDLFNBQXhCLEVBQW1Dd21CLFNBQW5DLEVBQVA7Q0FHSjs7QUNoQk8sU0FBU1Esa0JBQVQsQ0FBNkJ0ZSxNQUE3QixFQUFxQztXQUNqQ0EsTUFBUDs7O0FDQ0osSUFBSTJkLFVBQVFsZixPQUFPN0csU0FBbkI7O0FBRUEsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFFQStsQixRQUFNaGYsUUFBTixHQUF3QkEsUUFBeEI7QUFDQWdmLFFBQU0xZSxjQUFOLEdBQXdCQSxjQUF4QjtBQUNBMGUsUUFBTXBlLFdBQU4sR0FBd0JBLFdBQXhCO0FBQ0FvZSxRQUFNaGUsT0FBTixHQUF3QkEsT0FBeEI7QUFDQWdlLFFBQU03SSxRQUFOLEdBQXdCd0osa0JBQXhCO0FBQ0FYLFFBQU1uQyxVQUFOLEdBQXdCOEMsa0JBQXhCO0FBQ0FYLFFBQU03ZCxZQUFOLEdBQXdCQSxZQUF4QjtBQUNBNmQsUUFBTXhkLFVBQU4sR0FBd0JBLFVBQXhCO0FBQ0F3ZCxRQUFNMWYsR0FBTixHQUF3QkEsS0FBeEI7OztBQUdBLEFBUUEwZixRQUFNaFgsTUFBTixHQUFpQ00sWUFBakM7QUFDQTBXLFFBQU1qWCxXQUFOLEdBQWlDVyxpQkFBakM7QUFDQXNXLFFBQU03VyxXQUFOLEdBQWlDaUIsaUJBQWpDO0FBQ0E0VixRQUFNOVcsV0FBTixHQUEwQkEsV0FBMUI7QUFDQThXLFFBQU0vVyxnQkFBTixHQUEwQkEsZ0JBQTFCOzs7QUFHQSxBQUNBK1csUUFBTXJULElBQU4sR0FBYVcsVUFBYjtBQUNBMFMsUUFBTVksY0FBTixHQUF1QmxULG9CQUF2QjtBQUNBc1MsUUFBTWEsY0FBTixHQUF1QnBULG9CQUF2Qjs7O0FBR0EsQUFXQXVTLFFBQU1oUyxRQUFOLEdBQThCUSxjQUE5QjtBQUNBd1IsUUFBTWxTLFdBQU4sR0FBOEJpQixpQkFBOUI7QUFDQWlSLFFBQU1qUyxhQUFOLEdBQThCYSxtQkFBOUI7QUFDQW9SLFFBQU01UixhQUFOLEdBQThCaUIsbUJBQTlCOztBQUVBMlEsUUFBTTdSLGFBQU4sR0FBbUNBLGFBQW5DO0FBQ0E2UixRQUFNOVIsa0JBQU4sR0FBbUNBLGtCQUFuQztBQUNBOFIsUUFBTS9SLGdCQUFOLEdBQW1DQSxnQkFBbkM7OztBQUdBLEFBRUErUixRQUFNN08sSUFBTixHQUFhSSxVQUFiO0FBQ0F5TyxRQUFNcGpCLFFBQU4sR0FBaUI4VSxjQUFqQjs7QUNoRUEsU0FBUzNOLEtBQVQsQ0FBYzlJLE1BQWQsRUFBc0JqVixLQUF0QixFQUE2Qjg2QixLQUE3QixFQUFvQ0MsTUFBcEMsRUFBNEM7UUFDcEM3bEIsU0FBU3lYLFdBQWI7UUFDSXRYLE1BQU1MLFlBQVlzRixHQUFaLENBQWdCeWdCLE1BQWhCLEVBQXdCLzZCLEtBQXhCLENBQVY7V0FDT2tWLE9BQU80bEIsS0FBUCxFQUFjemxCLEdBQWQsRUFBbUJKLE1BQW5CLENBQVA7OztBQUdKLFNBQVMrbEIsY0FBVCxDQUF5Qi9sQixNQUF6QixFQUFpQ2pWLEtBQWpDLEVBQXdDODZCLEtBQXhDLEVBQStDO1FBQ3ZDeG1CLFNBQVNXLE1BQVQsQ0FBSixFQUFzQjtnQkFDVkEsTUFBUjtpQkFDU2dDLFNBQVQ7OzthQUdLaEMsVUFBVSxFQUFuQjs7UUFFSWpWLFNBQVMsSUFBYixFQUFtQjtlQUNSK2QsTUFBSTlJLE1BQUosRUFBWWpWLEtBQVosRUFBbUI4NkIsS0FBbkIsRUFBMEIsT0FBMUIsQ0FBUDs7O1FBR0FoNUIsQ0FBSjtRQUNJbTVCLE1BQU0sRUFBVjtTQUNLbjVCLElBQUksQ0FBVCxFQUFZQSxJQUFJLEVBQWhCLEVBQW9CQSxHQUFwQixFQUF5QjtZQUNqQkEsQ0FBSixJQUFTaWMsTUFBSTlJLE1BQUosRUFBWW5ULENBQVosRUFBZWc1QixLQUFmLEVBQXNCLE9BQXRCLENBQVQ7O1dBRUdHLEdBQVA7Ozs7Ozs7Ozs7O0FBV0osU0FBU0MsZ0JBQVQsQ0FBMkJDLFlBQTNCLEVBQXlDbG1CLE1BQXpDLEVBQWlEalYsS0FBakQsRUFBd0Q4NkIsS0FBeEQsRUFBK0Q7UUFDdkQsT0FBT0ssWUFBUCxLQUF3QixTQUE1QixFQUF1QztZQUMvQjdtQixTQUFTVyxNQUFULENBQUosRUFBc0I7b0JBQ1ZBLE1BQVI7cUJBQ1NnQyxTQUFUOzs7aUJBR0toQyxVQUFVLEVBQW5CO0tBTkosTUFPTztpQkFDTWttQixZQUFUO2dCQUNRbG1CLE1BQVI7dUJBQ2UsS0FBZjs7WUFFSVgsU0FBU1csTUFBVCxDQUFKLEVBQXNCO29CQUNWQSxNQUFSO3FCQUNTZ0MsU0FBVDs7O2lCQUdLaEMsVUFBVSxFQUFuQjs7O1FBR0FDLFNBQVN5WCxXQUFiO1FBQ0l5TyxRQUFRRCxlQUFlam1CLE9BQU9xUyxLQUFQLENBQWFsQixHQUE1QixHQUFrQyxDQUQ5Qzs7UUFHSXJtQixTQUFTLElBQWIsRUFBbUI7ZUFDUitkLE1BQUk5SSxNQUFKLEVBQVksQ0FBQ2pWLFFBQVFvN0IsS0FBVCxJQUFrQixDQUE5QixFQUFpQ04sS0FBakMsRUFBd0MsS0FBeEMsQ0FBUDs7O1FBR0FoNUIsQ0FBSjtRQUNJbTVCLE1BQU0sRUFBVjtTQUNLbjVCLElBQUksQ0FBVCxFQUFZQSxJQUFJLENBQWhCLEVBQW1CQSxHQUFuQixFQUF3QjtZQUNoQkEsQ0FBSixJQUFTaWMsTUFBSTlJLE1BQUosRUFBWSxDQUFDblQsSUFBSXM1QixLQUFMLElBQWMsQ0FBMUIsRUFBNkJOLEtBQTdCLEVBQW9DLEtBQXBDLENBQVQ7O1dBRUdHLEdBQVA7OztBQUdKLEFBQU8sU0FBU0ksVUFBVCxDQUFxQnBtQixNQUFyQixFQUE2QmpWLEtBQTdCLEVBQW9DO1dBQ2hDZzdCLGVBQWUvbEIsTUFBZixFQUF1QmpWLEtBQXZCLEVBQThCLFFBQTlCLENBQVA7OztBQUdKLEFBQU8sU0FBU3M3QixlQUFULENBQTBCcm1CLE1BQTFCLEVBQWtDalYsS0FBbEMsRUFBeUM7V0FDckNnN0IsZUFBZS9sQixNQUFmLEVBQXVCalYsS0FBdkIsRUFBOEIsYUFBOUIsQ0FBUDs7O0FBR0osQUFBTyxTQUFTdTdCLFlBQVQsQ0FBdUJKLFlBQXZCLEVBQXFDbG1CLE1BQXJDLEVBQTZDalYsS0FBN0MsRUFBb0Q7V0FDaERrN0IsaUJBQWlCQyxZQUFqQixFQUErQmxtQixNQUEvQixFQUF1Q2pWLEtBQXZDLEVBQThDLFVBQTlDLENBQVA7OztBQUdKLEFBQU8sU0FBU3c3QixpQkFBVCxDQUE0QkwsWUFBNUIsRUFBMENsbUIsTUFBMUMsRUFBa0RqVixLQUFsRCxFQUF5RDtXQUNyRGs3QixpQkFBaUJDLFlBQWpCLEVBQStCbG1CLE1BQS9CLEVBQXVDalYsS0FBdkMsRUFBOEMsZUFBOUMsQ0FBUDs7O0FBR0osQUFBTyxTQUFTeTdCLGVBQVQsQ0FBMEJOLFlBQTFCLEVBQXdDbG1CLE1BQXhDLEVBQWdEalYsS0FBaEQsRUFBdUQ7V0FDbkRrN0IsaUJBQWlCQyxZQUFqQixFQUErQmxtQixNQUEvQixFQUF1Q2pWLEtBQXZDLEVBQThDLGFBQTlDLENBQVA7OztBQ3ZGSnlzQixtQkFBbUIsSUFBbkIsRUFBeUI7a0JBQ1Asc0JBRE87YUFFWCxpQkFBVWpVLE1BQVYsRUFBa0I7WUFDcEJ0VCxJQUFJc1QsU0FBUyxFQUFqQjtZQUNJMkMsU0FBVXpDLE1BQU1GLFNBQVMsR0FBVCxHQUFlLEVBQXJCLE1BQTZCLENBQTlCLEdBQW1DLElBQW5DLEdBQ1J0VCxNQUFNLENBQVAsR0FBWSxJQUFaLEdBQ0NBLE1BQU0sQ0FBUCxHQUFZLElBQVosR0FDQ0EsTUFBTSxDQUFQLEdBQVksSUFBWixHQUFtQixJQUp2QjtlQUtPc1QsU0FBUzJDLE1BQWhCOztDQVJSOztBQ0pBO0FBQ0EsQUFFQSxBQVFBLEFBUUEsQUFhQSxBQUNBLEFBRUExSCxNQUFNeWtCLElBQU4sR0FBYTFlLFVBQVUsdURBQVYsRUFBbUVpVCxrQkFBbkUsQ0FBYjtBQUNBaFosTUFBTWlvQixRQUFOLEdBQWlCbGlCLFVBQVUsK0RBQVYsRUFBMkVtVCxTQUEzRSxDQUFqQixDQUVBOztBQ3RDQSxJQUFJZ1AsVUFBVXA1QixLQUFLNkosR0FBbkI7O0FBRUEsQUFBTyxTQUFTQSxHQUFULEdBQWdCO1FBQ2YvTSxPQUFpQixLQUFLK3lCLEtBQTFCOztTQUVLRixhQUFMLEdBQXFCeUosUUFBUSxLQUFLekosYUFBYixDQUFyQjtTQUNLQyxLQUFMLEdBQXFCd0osUUFBUSxLQUFLeEosS0FBYixDQUFyQjtTQUNLNU8sT0FBTCxHQUFxQm9ZLFFBQVEsS0FBS3BZLE9BQWIsQ0FBckI7O1NBRUswTyxZQUFMLEdBQXFCMEosUUFBUXQ4QixLQUFLNHlCLFlBQWIsQ0FBckI7U0FDS25ILE9BQUwsR0FBcUI2USxRQUFRdDhCLEtBQUt5ckIsT0FBYixDQUFyQjtTQUNLRCxPQUFMLEdBQXFCOFEsUUFBUXQ4QixLQUFLd3JCLE9BQWIsQ0FBckI7U0FDS0YsS0FBTCxHQUFxQmdSLFFBQVF0OEIsS0FBS3NyQixLQUFiLENBQXJCO1NBQ0szSCxNQUFMLEdBQXFCMlksUUFBUXQ4QixLQUFLMmpCLE1BQWIsQ0FBckI7U0FDSzRPLEtBQUwsR0FBcUIrSixRQUFRdDhCLEtBQUt1eUIsS0FBYixDQUFyQjs7V0FFTyxJQUFQOzs7QUNkSixTQUFTK0QsYUFBVCxDQUFzQmxqQixRQUF0QixFQUFnQ3NCLEtBQWhDLEVBQXVDalIsS0FBdkMsRUFBOEN5eUIsU0FBOUMsRUFBeUQ7UUFDakRoRSxRQUFRb0MsZUFBZTVmLEtBQWYsRUFBc0JqUixLQUF0QixDQUFaOzthQUVTb3ZCLGFBQVQsSUFBMEJxRCxZQUFZaEUsTUFBTVcsYUFBNUM7YUFDU0MsS0FBVCxJQUEwQm9ELFlBQVloRSxNQUFNWSxLQUE1QzthQUNTNU8sT0FBVCxJQUEwQmdTLFlBQVloRSxNQUFNaE8sT0FBNUM7O1dBRU85USxTQUFTNGYsT0FBVCxFQUFQOzs7O0FBSUosQUFBTyxTQUFTekssS0FBVCxDQUFjN1QsS0FBZCxFQUFxQmpSLEtBQXJCLEVBQTRCO1dBQ3hCNnlCLGNBQVksSUFBWixFQUFrQjVoQixLQUFsQixFQUF5QmpSLEtBQXpCLEVBQWdDLENBQWhDLENBQVA7Ozs7QUFJSixBQUFPLFNBQVNpeEIsVUFBVCxDQUFtQmhnQixLQUFuQixFQUEwQmpSLEtBQTFCLEVBQWlDO1dBQzdCNnlCLGNBQVksSUFBWixFQUFrQjVoQixLQUFsQixFQUF5QmpSLEtBQXpCLEVBQWdDLENBQUMsQ0FBakMsQ0FBUDs7O0FDbkJXLFNBQVM4NEIsT0FBVCxDQUFrQnBqQixNQUFsQixFQUEwQjtRQUNqQ0EsU0FBUyxDQUFiLEVBQWdCO2VBQ0xqVyxLQUFLa1csS0FBTCxDQUFXRCxNQUFYLENBQVA7S0FESixNQUVPO2VBQ0lqVyxLQUFLNlEsSUFBTCxDQUFVb0YsTUFBVixDQUFQOzs7O0FDQUQsU0FBU3FqQixNQUFULEdBQW1CO1FBQ2xCNUosZUFBZSxLQUFLQyxhQUF4QjtRQUNJRixPQUFlLEtBQUtHLEtBQXhCO1FBQ0luUCxTQUFlLEtBQUtPLE9BQXhCO1FBQ0lsa0IsT0FBZSxLQUFLK3lCLEtBQXhCO1FBQ0l0SCxPQUFKLEVBQWFELE9BQWIsRUFBc0JGLEtBQXRCLEVBQTZCaUgsS0FBN0IsRUFBb0NrSyxjQUFwQzs7OztRQUlJLEVBQUc3SixnQkFBZ0IsQ0FBaEIsSUFBcUJELFFBQVEsQ0FBN0IsSUFBa0NoUCxVQUFVLENBQTdDLElBQ0dpUCxnQkFBZ0IsQ0FBaEIsSUFBcUJELFFBQVEsQ0FBN0IsSUFBa0NoUCxVQUFVLENBRGpELENBQUosRUFDMEQ7d0JBQ3RDNFksUUFBUUcsYUFBYS9ZLE1BQWIsSUFBdUJnUCxJQUEvQixJQUF1QyxLQUF2RDtlQUNPLENBQVA7aUJBQ1MsQ0FBVDs7Ozs7U0FLQ0MsWUFBTCxHQUFvQkEsZUFBZSxJQUFuQzs7Y0FFb0IxWixTQUFTMFosZUFBZSxJQUF4QixDQUFwQjtTQUNLbkgsT0FBTCxHQUFvQkEsVUFBVSxFQUE5Qjs7Y0FFb0J2UyxTQUFTdVMsVUFBVSxFQUFuQixDQUFwQjtTQUNLRCxPQUFMLEdBQW9CQSxVQUFVLEVBQTlCOztZQUVvQnRTLFNBQVNzUyxVQUFVLEVBQW5CLENBQXBCO1NBQ0tGLEtBQUwsR0FBb0JBLFFBQVEsRUFBNUI7O1lBRVFwUyxTQUFTb1MsUUFBUSxFQUFqQixDQUFSOzs7cUJBR2lCcFMsU0FBU3lqQixhQUFhaEssSUFBYixDQUFULENBQWpCO2NBQ1U4SixjQUFWO1lBQ1FGLFFBQVFHLGFBQWFELGNBQWIsQ0FBUixDQUFSOzs7WUFHUXZqQixTQUFTeUssU0FBUyxFQUFsQixDQUFSO2NBQ1UsRUFBVjs7U0FFS2dQLElBQUwsR0FBY0EsSUFBZDtTQUNLaFAsTUFBTCxHQUFjQSxNQUFkO1NBQ0s0TyxLQUFMLEdBQWNBLEtBQWQ7O1dBRU8sSUFBUDs7O0FBR0osQUFBTyxTQUFTb0ssWUFBVCxDQUF1QmhLLElBQXZCLEVBQTZCOzs7V0FHekJBLE9BQU8sSUFBUCxHQUFjLE1BQXJCOzs7QUFHSixBQUFPLFNBQVMrSixZQUFULENBQXVCL1ksTUFBdkIsRUFBK0I7O1dBRTNCQSxTQUFTLE1BQVQsR0FBa0IsSUFBekI7OztBQ3ZERyxTQUFTaVosRUFBVCxDQUFhaGYsS0FBYixFQUFvQjtRQUNuQitVLElBQUo7UUFDSWhQLE1BQUo7UUFDSWlQLGVBQWUsS0FBS0MsYUFBeEI7O1lBRVFsVixlQUFlQyxLQUFmLENBQVI7O1FBRUlBLFVBQVUsT0FBVixJQUFxQkEsVUFBVSxNQUFuQyxFQUEyQztlQUM5QixLQUFLa1YsS0FBTCxHQUFlRixlQUFlLEtBQXZDO2lCQUNTLEtBQUsxTyxPQUFMLEdBQWV5WSxhQUFhaEssSUFBYixDQUF4QjtlQUNPL1UsVUFBVSxPQUFWLEdBQW9CK0YsTUFBcEIsR0FBNkJBLFNBQVMsRUFBN0M7S0FISixNQUlPOztlQUVJLEtBQUttUCxLQUFMLEdBQWE1dkIsS0FBS3lLLEtBQUwsQ0FBVyt1QixhQUFhLEtBQUt4WSxPQUFsQixDQUFYLENBQXBCO2dCQUNRdEcsS0FBUjtpQkFDUyxNQUFMO3VCQUF1QitVLE9BQU8sQ0FBUCxHQUFlQyxlQUFlLE1BQXJDO2lCQUNYLEtBQUw7dUJBQXVCRCxPQUFlQyxlQUFlLEtBQXJDO2lCQUNYLE1BQUw7dUJBQXVCRCxPQUFPLEVBQVAsR0FBZUMsZUFBZSxJQUFyQztpQkFDWCxRQUFMO3VCQUF1QkQsT0FBTyxJQUFQLEdBQWVDLGVBQWUsR0FBckM7aUJBQ1gsUUFBTDt1QkFBdUJELE9BQU8sS0FBUCxHQUFlQyxlQUFlLElBQXJDOztpQkFFWCxhQUFMO3VCQUEyQjF2QixLQUFLa1csS0FBTCxDQUFXdVosT0FBTyxLQUFsQixJQUEyQkMsWUFBbEM7O3NCQUNMLElBQUlqWSxLQUFKLENBQVUsa0JBQWtCaUQsS0FBNUIsQ0FBTjs7Ozs7O0FBTXJCLEFBQU8sU0FBU2xJLFNBQVQsR0FBb0I7V0FFbkIsS0FBS21kLGFBQUwsR0FDQSxLQUFLQyxLQUFMLEdBQWEsS0FEYixHQUVDLEtBQUs1TyxPQUFMLEdBQWUsRUFBaEIsR0FBc0IsTUFGdEIsR0FHQTdLLE1BQU0sS0FBSzZLLE9BQUwsR0FBZSxFQUFyQixJQUEyQixPQUovQjs7O0FBUUosU0FBUzJZLE1BQVQsQ0FBaUJDLEtBQWpCLEVBQXdCO1dBQ2IsWUFBWTtlQUNSLEtBQUtGLEVBQUwsQ0FBUUUsS0FBUixDQUFQO0tBREo7OztBQUtKLEFBQU8sSUFBSUMsaUJBQWlCRixPQUFPLElBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlHLFlBQWlCSCxPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlJLFlBQWlCSixPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlLLFVBQWlCTCxPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlNLFNBQWlCTixPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlPLFVBQWlCUCxPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlRLFdBQWlCUixPQUFPLEdBQVAsQ0FBckI7QUFDUCxBQUFPLElBQUlTLFVBQWlCVCxPQUFPLEdBQVAsQ0FBckI7O0FDbkRBLFNBQVNuZSxLQUFULENBQWNkLEtBQWQsRUFBcUI7WUFDaEJELGVBQWVDLEtBQWYsQ0FBUjtXQUNPLEtBQUtBLFFBQVEsR0FBYixHQUFQOzs7QUFHSixTQUFTMmYsVUFBVCxDQUFvQi8wQixJQUFwQixFQUEwQjtXQUNmLFlBQVk7ZUFDUixLQUFLdXFCLEtBQUwsQ0FBV3ZxQixJQUFYLENBQVA7S0FESjs7O0FBS0osQUFBTyxJQUFJb3FCLGVBQWUySyxXQUFXLGNBQVgsQ0FBbkI7QUFDUCxBQUFPLElBQUk5UixVQUFlOFIsV0FBVyxTQUFYLENBQW5CO0FBQ1AsQUFBTyxJQUFJL1IsVUFBZStSLFdBQVcsU0FBWCxDQUFuQjtBQUNQLEFBQU8sSUFBSWpTLFFBQWVpUyxXQUFXLE9BQVgsQ0FBbkI7QUFDUCxBQUFPLElBQUk1SyxPQUFlNEssV0FBVyxNQUFYLENBQW5CO0FBQ1AsQUFBTyxJQUFJNVosU0FBZTRaLFdBQVcsUUFBWCxDQUFuQjtBQUNQLEFBQU8sSUFBSWhMLFFBQWVnTCxXQUFXLE9BQVgsQ0FBbkI7O0FBRVAsQUFBTyxTQUFTN0ssS0FBVCxHQUFrQjtXQUNkeFosU0FBUyxLQUFLeVosSUFBTCxLQUFjLENBQXZCLENBQVA7OztBQ3JCSixJQUFJaGxCLFFBQVF6SyxLQUFLeUssS0FBakI7QUFDQSxJQUFJNnZCLGFBQWE7T0FDVixFQURVO09BRVYsRUFGVTtPQUdWLEVBSFU7T0FJVixFQUpVO09BS1YsRUFMVTtDQUFqQjs7O0FBU0EsU0FBU0MsaUJBQVQsQ0FBMkJ6Z0IsTUFBM0IsRUFBbUM3RCxNQUFuQyxFQUEyQzRELGFBQTNDLEVBQTBERSxRQUExRCxFQUFvRXBILE1BQXBFLEVBQTRFO1dBQ2pFQSxPQUFPaUgsWUFBUCxDQUFvQjNELFVBQVUsQ0FBOUIsRUFBaUMsQ0FBQyxDQUFDNEQsYUFBbkMsRUFBa0RDLE1BQWxELEVBQTBEQyxRQUExRCxDQUFQOzs7QUFHSixTQUFTSCxjQUFULENBQXVCNGdCLGNBQXZCLEVBQXVDM2dCLGFBQXZDLEVBQXNEbEgsTUFBdEQsRUFBOEQ7UUFDdER6QyxXQUFXa2hCLGVBQWVvSixjQUFmLEVBQStCM3dCLEdBQS9CLEVBQWY7UUFDSTBlLFVBQVc5ZCxNQUFNeUYsU0FBU3dwQixFQUFULENBQVksR0FBWixDQUFOLENBQWY7UUFDSXBSLFVBQVc3ZCxNQUFNeUYsU0FBU3dwQixFQUFULENBQVksR0FBWixDQUFOLENBQWY7UUFDSXRSLFFBQVczZCxNQUFNeUYsU0FBU3dwQixFQUFULENBQVksR0FBWixDQUFOLENBQWY7UUFDSWpLLE9BQVdobEIsTUFBTXlGLFNBQVN3cEIsRUFBVCxDQUFZLEdBQVosQ0FBTixDQUFmO1FBQ0lqWixTQUFXaFcsTUFBTXlGLFNBQVN3cEIsRUFBVCxDQUFZLEdBQVosQ0FBTixDQUFmO1FBQ0lySyxRQUFXNWtCLE1BQU15RixTQUFTd3BCLEVBQVQsQ0FBWSxHQUFaLENBQU4sQ0FBZjs7UUFFSWgzQixJQUFJNmxCLFVBQVUrUixXQUFXMWIsQ0FBckIsSUFBMEIsQ0FBQyxHQUFELEVBQU0ySixPQUFOLENBQTFCLElBQ0FELFdBQVcsQ0FBWCxJQUEwQixDQUFDLEdBQUQsQ0FEMUIsSUFFQUEsVUFBVWdTLFdBQVdybkIsQ0FBckIsSUFBMEIsQ0FBQyxJQUFELEVBQU9xVixPQUFQLENBRjFCLElBR0FGLFNBQVcsQ0FBWCxJQUEwQixDQUFDLEdBQUQsQ0FIMUIsSUFJQUEsUUFBVWtTLFdBQVdyekIsQ0FBckIsSUFBMEIsQ0FBQyxJQUFELEVBQU9taEIsS0FBUCxDQUoxQixJQUtBcUgsUUFBVyxDQUFYLElBQTBCLENBQUMsR0FBRCxDQUwxQixJQU1BQSxPQUFVNkssV0FBV2w5QixDQUFyQixJQUEwQixDQUFDLElBQUQsRUFBT3F5QixJQUFQLENBTjFCLElBT0FoUCxVQUFXLENBQVgsSUFBMEIsQ0FBQyxHQUFELENBUDFCLElBUUFBLFNBQVU2WixXQUFXaFgsQ0FBckIsSUFBMEIsQ0FBQyxJQUFELEVBQU83QyxNQUFQLENBUjFCLElBU0E0TyxTQUFXLENBQVgsSUFBMEIsQ0FBQyxHQUFELENBVDFCLElBUzZDLENBQUMsSUFBRCxFQUFPQSxLQUFQLENBVHJEOztNQVdFLENBQUYsSUFBT3hWLGFBQVA7TUFDRSxDQUFGLElBQU8sQ0FBQzJnQixjQUFELEdBQWtCLENBQXpCO01BQ0UsQ0FBRixJQUFPN25CLE1BQVA7V0FDTzRuQixrQkFBa0JwcEIsS0FBbEIsQ0FBd0IsSUFBeEIsRUFBOEJ6TyxDQUE5QixDQUFQOzs7O0FBSUosQUFBTyxTQUFTKzNCLDBCQUFULENBQXFDQyxnQkFBckMsRUFBdUQ7UUFDdERBLHFCQUFxQmhtQixTQUF6QixFQUFvQztlQUN6QmpLLEtBQVA7O1FBRUEsT0FBT2l3QixnQkFBUCxLQUE2QixVQUFqQyxFQUE2QztnQkFDakNBLGdCQUFSO2VBQ08sSUFBUDs7V0FFRyxLQUFQOzs7O0FBSUosQUFBTyxTQUFTQywyQkFBVCxDQUFzQ0MsU0FBdEMsRUFBaURDLEtBQWpELEVBQXdEO1FBQ3ZEUCxXQUFXTSxTQUFYLE1BQTBCbG1CLFNBQTlCLEVBQXlDO2VBQzlCLEtBQVA7O1FBRUFtbUIsVUFBVW5tQixTQUFkLEVBQXlCO2VBQ2Q0bEIsV0FBV00sU0FBWCxDQUFQOztlQUVPQSxTQUFYLElBQXdCQyxLQUF4QjtXQUNPLElBQVA7OztBQUdKLEFBQU8sU0FBU3RGLFFBQVQsQ0FBbUJ1RixVQUFuQixFQUErQjtRQUM5Qm5vQixTQUFTLEtBQUsrSixVQUFMLEVBQWI7UUFDSTlELFNBQVNnQixlQUFhLElBQWIsRUFBbUIsQ0FBQ2toQixVQUFwQixFQUFnQ25vQixNQUFoQyxDQUFiOztRQUVJbW9CLFVBQUosRUFBZ0I7aUJBQ0hub0IsT0FBT3NILFVBQVAsQ0FBa0IsQ0FBQyxJQUFuQixFQUF5QnJCLE1BQXpCLENBQVQ7OztXQUdHakcsT0FBTzJpQixVQUFQLENBQWtCMWMsTUFBbEIsQ0FBUDs7O0FDekVKLElBQUkvTyxRQUFNN0osS0FBSzZKLEdBQWY7O0FBRUEsQUFBTyxTQUFTa3JCLGFBQVQsR0FBdUI7Ozs7Ozs7O1FBUXRCeE0sVUFBVTFlLE1BQUksS0FBSzhsQixhQUFULElBQTBCLElBQXhDO1FBQ0lGLE9BQWU1bEIsTUFBSSxLQUFLK2xCLEtBQVQsQ0FBbkI7UUFDSW5QLFNBQWU1VyxNQUFJLEtBQUttWCxPQUFULENBQW5CO1FBQ0lzSCxPQUFKLEVBQWFGLEtBQWIsRUFBb0JpSCxLQUFwQjs7O2NBR29CclosU0FBU3VTLFVBQVUsRUFBbkIsQ0FBcEI7WUFDb0J2UyxTQUFTc1MsVUFBVSxFQUFuQixDQUFwQjtlQUNXLEVBQVg7ZUFDVyxFQUFYOzs7WUFHU3RTLFNBQVN5SyxTQUFTLEVBQWxCLENBQVQ7Y0FDVSxFQUFWOzs7UUFJSXNhLElBQUkxTCxLQUFSO1FBQ0kvTCxJQUFJN0MsTUFBUjtRQUNJdWEsSUFBSXZMLElBQVI7UUFDSXhvQixJQUFJbWhCLEtBQVI7UUFDSW5WLElBQUlxVixPQUFSO1FBQ0kxSixJQUFJMkosT0FBUjtRQUNJN1ksUUFBUSxLQUFLb3FCLFNBQUwsRUFBWjs7UUFFSSxDQUFDcHFCLEtBQUwsRUFBWTs7O2VBR0QsS0FBUDs7O1dBR0csQ0FBQ0EsUUFBUSxDQUFSLEdBQVksR0FBWixHQUFrQixFQUFuQixJQUNILEdBREcsSUFFRnFyQixJQUFJQSxJQUFJLEdBQVIsR0FBYyxFQUZaLEtBR0Z6WCxJQUFJQSxJQUFJLEdBQVIsR0FBYyxFQUhaLEtBSUYwWCxJQUFJQSxJQUFJLEdBQVIsR0FBYyxFQUpaLEtBS0QvekIsS0FBS2dNLENBQUwsSUFBVTJMLENBQVgsR0FBZ0IsR0FBaEIsR0FBc0IsRUFMcEIsS0FNRjNYLElBQUlBLElBQUksR0FBUixHQUFjLEVBTlosS0FPRmdNLElBQUlBLElBQUksR0FBUixHQUFjLEVBUFosS0FRRjJMLElBQUlBLElBQUksR0FBUixHQUFjLEVBUlosQ0FBUDs7O0FDeENKLElBQUk2WSxVQUFRckksU0FBUzFkLFNBQXJCOztBQUVBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFFQStsQixRQUFNNXRCLEdBQU4sR0FBdUJBLEdBQXZCO0FBQ0E0dEIsUUFBTXBTLEdBQU4sR0FBdUJBLEtBQXZCO0FBQ0FvUyxRQUFNakcsUUFBTixHQUF1QkEsVUFBdkI7QUFDQWlHLFFBQU1pQyxFQUFOLEdBQXVCQSxFQUF2QjtBQUNBakMsUUFBTW9DLGNBQU4sR0FBdUJBLGNBQXZCO0FBQ0FwQyxRQUFNcUMsU0FBTixHQUF1QkEsU0FBdkI7QUFDQXJDLFFBQU1zQyxTQUFOLEdBQXVCQSxTQUF2QjtBQUNBdEMsUUFBTXVDLE9BQU4sR0FBdUJBLE9BQXZCO0FBQ0F2QyxRQUFNd0MsTUFBTixHQUF1QkEsTUFBdkI7QUFDQXhDLFFBQU15QyxPQUFOLEdBQXVCQSxPQUF2QjtBQUNBekMsUUFBTTBDLFFBQU4sR0FBdUJBLFFBQXZCO0FBQ0ExQyxRQUFNMkMsT0FBTixHQUF1QkEsT0FBdkI7QUFDQTNDLFFBQU1qbEIsT0FBTixHQUF1QkEsU0FBdkI7QUFDQWlsQixRQUFNM0gsT0FBTixHQUF1QndKLE1BQXZCO0FBQ0E3QixRQUFNamMsR0FBTixHQUF1QkEsS0FBdkI7QUFDQWljLFFBQU0vSCxZQUFOLEdBQXVCQSxZQUF2QjtBQUNBK0gsUUFBTWxQLE9BQU4sR0FBdUJBLE9BQXZCO0FBQ0FrUCxRQUFNblAsT0FBTixHQUF1QkEsT0FBdkI7QUFDQW1QLFFBQU1yUCxLQUFOLEdBQXVCQSxLQUF2QjtBQUNBcVAsUUFBTWhJLElBQU4sR0FBdUJBLElBQXZCO0FBQ0FnSSxRQUFNakksS0FBTixHQUF1QkEsS0FBdkI7QUFDQWlJLFFBQU1oWCxNQUFOLEdBQXVCQSxNQUF2QjtBQUNBZ1gsUUFBTXBJLEtBQU4sR0FBdUJBLEtBQXZCO0FBQ0FvSSxRQUFNbEMsUUFBTixHQUF1QkEsUUFBdkI7QUFDQWtDLFFBQU0xQyxXQUFOLEdBQXVCQSxhQUF2QjtBQUNBMEMsUUFBTTlsQixRQUFOLEdBQXVCb2pCLGFBQXZCO0FBQ0EwQyxRQUFNMUIsTUFBTixHQUF1QmhCLGFBQXZCO0FBQ0EwQyxRQUFNOWtCLE1BQU4sR0FBdUJBLE1BQXZCO0FBQ0E4a0IsUUFBTS9hLFVBQU4sR0FBdUJBLFVBQXZCOzs7QUFHQSxBQUVBK2EsUUFBTXdELFdBQU4sR0FBb0Joa0IsVUFBVSxxRkFBVixFQUFpRzhkLGFBQWpHLENBQXBCO0FBQ0EwQyxRQUFNOUIsSUFBTixHQUFhQSxJQUFiOztBQy9DQSxzQkFDQSxBQUVBLEFBQ0EsQUFDQSxBQUtBOztBQ0xBOztBQUVBcFosZUFBZSxHQUFmLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCLEVBQTBCLE1BQTFCO0FBQ0FBLGVBQWUsR0FBZixFQUFvQixDQUFwQixFQUF1QixDQUF2QixFQUEwQixTQUExQjs7OztBQUlBK0IsY0FBYyxHQUFkLEVBQW1CTixXQUFuQjtBQUNBTSxjQUFjLEdBQWQsRUFBbUJILGNBQW5CO0FBQ0FpQixjQUFjLEdBQWQsRUFBbUIsVUFBVTVOLEtBQVYsRUFBaUJzTCxLQUFqQixFQUF3QmhaLE1BQXhCLEVBQWdDO1dBQ3hDOFAsRUFBUCxHQUFZLElBQUkzQixJQUFKLENBQVN5Z0IsV0FBV2xoQixLQUFYLEVBQWtCLEVBQWxCLElBQXdCLElBQWpDLENBQVo7Q0FESjtBQUdBNE4sY0FBYyxHQUFkLEVBQW1CLFVBQVU1TixLQUFWLEVBQWlCc0wsS0FBakIsRUFBd0JoWixNQUF4QixFQUFnQztXQUN4QzhQLEVBQVAsR0FBWSxJQUFJM0IsSUFBSixDQUFTa0UsTUFBTTNFLEtBQU4sQ0FBVCxDQUFaO0NBREo7O0FDakJBLHNCQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUVBLEFBRUE7O0FDbkJBOzs7Ozs7QUFNQSxBQUVBMHBCLE1BQU8vd0IsT0FBUCxHQUFpQixRQUFqQjs7QUFFQSxBQWFBLEFBSUEsQUFhQSxBQU9BLEFBRUEsQUFFQWtILGdCQUFnQndmLFdBQWhCOztBQUVBcUssTUFBTzlvQixFQUFQLEdBQStCQSxLQUEvQjtBQUNBOG9CLE1BQU9oMEIsR0FBUCxHQUErQkEsR0FBL0I7QUFDQWcwQixNQUFPajdCLEdBQVAsR0FBK0JBLEdBQS9CO0FBQ0FpN0IsTUFBT3ZpQixHQUFQLEdBQStCQSxHQUEvQjtBQUNBdWlCLE1BQU9wb0IsR0FBUCxHQUErQkEsU0FBL0I7QUFDQW9vQixNQUFPckYsSUFBUCxHQUErQkEsVUFBL0I7QUFDQXFGLE1BQU96YSxNQUFQLEdBQStCQSxVQUEvQjtBQUNBeWEsTUFBT2xwQixNQUFQLEdBQStCQSxNQUEvQjtBQUNBa3BCLE1BQU92b0IsTUFBUCxHQUErQkEsa0JBQS9CO0FBQ0F1b0IsTUFBT0MsT0FBUCxHQUErQkEsYUFBL0I7QUFDQUQsTUFBT2hyQixRQUFQLEdBQStCQSxjQUEvQjtBQUNBZ3JCLE1BQU9ubEIsUUFBUCxHQUErQkEsUUFBL0I7QUFDQW1sQixNQUFPelYsUUFBUCxHQUErQkEsWUFBL0I7QUFDQXlWLE1BQU90RCxTQUFQLEdBQStCQSxZQUEvQjtBQUNBc0QsTUFBT3hlLFVBQVAsR0FBK0JBLFNBQS9CO0FBQ0F3ZSxNQUFPbkwsVUFBUCxHQUErQkEsVUFBL0I7QUFDQW1MLE1BQU8xYSxXQUFQLEdBQStCQSxlQUEvQjtBQUNBMGEsTUFBTzNWLFdBQVAsR0FBK0JBLGVBQS9CO0FBQ0EyVixNQUFPN1EsWUFBUCxHQUErQkEsWUFBL0I7QUFDQTZRLE1BQU8xUSxZQUFQLEdBQStCQSxZQUEvQjtBQUNBMFEsTUFBTzNSLE9BQVAsR0FBK0JBLFdBQS9CO0FBQ0EyUixNQUFPMVYsYUFBUCxHQUErQkEsaUJBQS9CO0FBQ0EwVixNQUFPemdCLGNBQVAsR0FBK0JBLGNBQS9CO0FBQ0F5Z0IsTUFBT0Usb0JBQVAsR0FBOEJBLDBCQUE5QjtBQUNBRixNQUFPRyxxQkFBUCxHQUErQkEsMkJBQS9CO0FBQ0FILE1BQU90SCxjQUFQLEdBQStCTixpQkFBL0I7QUFDQTRILE1BQU94cEIsU0FBUCxHQUErQlUsS0FBL0IsQ0FFQTs7SUMxRU16Rjs7O29CQUNRcFIsVUFBWixFQUF3Qjs7O21IQUNoQkEsVUFEZ0I7O2NBRWpCQSxVQUFMLEdBQWtCQSxVQUFsQjs7Ozs7O2lDQUdPcVIsVUFBVXpJLFdBQVdOLE9BQU87Z0JBQzdCeEIsT0FBT3dCLE1BQU1NLFNBQU4sQ0FBWDs7Z0JBRUl5dUIsT0FBTyxJQUFJM2dCLElBQUosQ0FBUzVQLEtBQUtoRCxLQUFMLEdBQWEsS0FBdEIsQ0FBWDtnQkFDSVEsU0FBUyt5QixLQUFLMUwsTUFBTCxFQUFiOzs7cUJBR1N6cUIsTUFBVCxDQUFnQixXQUFoQixFQUE2QkksU0FBN0IsQ0FBdUMsaUJBQXZDLEVBQTBEMEYsTUFBMUQsQ0FBaUUsVUFBVW5GLENBQVYsRUFBYTtvQkFDckVLLFFBQVFMLEVBQUV5RixRQUFGLENBQVc4QixVQUFYLENBQXNCLENBQXRCLElBQTJCLEVBQXpDO3VCQUNRbEgsU0FBUzBHLFNBQWpCO2FBRkgsRUFJQ2xHLElBSkQsQ0FJTSxVQUFVYixDQUFWLEVBQWFtQyxDQUFiLEVBQWdCO29CQUNmOEMsS0FBS2dZLElBQUwsSUFBYSxTQUFiLElBQTBCamQsRUFBRS9CLGNBQUYsQ0FBaUIsV0FBakIsQ0FBOUIsRUFBNkQ7d0JBQ3REbzBCLE9BQU95TCxNQUFPdEksSUFBUCxFQUFhdk4sR0FBYixDQUFpQmlXLE9BQU96N0IsTUFBUCxJQUFpQnk3QixPQUFPbCtCLEVBQUVtK0IsU0FBVCxDQUFsQyxFQUF1RCxNQUF2RCxFQUErRHJoQixJQUEvRCxDQUFvRTBZLElBQXBFLEVBQTBFLE1BQTFFLENBQVg7O3NCQUVFcDBCLENBQUYsR0FBTSxNQUFPaXhCLE9BQU8sRUFBcEI7NkJBQ0EsQ0FBVSxJQUFWLEVBQWdCaHpCLE1BQWhCLENBQXVCLE1BQXZCLEVBQ0tWLElBREwsQ0FDVSxPQURWLEVBQ21CLFVBQVVxQixDQUFWLEVBQWE7K0JBQVVBLEVBQUUvQixjQUFGLENBQWlCLFVBQWpCLElBQStCK0IsRUFBRThTLFFBQUYsR0FBYSxFQUE1QyxHQUFpRCxHQUF6RDtxQkFEbEM7O29CQUdDN04sS0FBS2dZLElBQUwsSUFBYSxXQUFiLElBQTRCamQsRUFBRS9CLGNBQUYsQ0FBaUIsV0FBakIsQ0FBaEMsRUFBK0Q7d0JBQ3hEb2xCLFNBQVN5YSxNQUFPdEksSUFBUCxFQUFhdk4sR0FBYixDQUFpQmlXLE9BQU9sK0IsRUFBRW0rQixTQUFULENBQWpCLEVBQXNDLFFBQXRDLEVBQWdEcmhCLElBQWhELENBQXFEMFksSUFBckQsRUFBMkQsUUFBM0QsQ0FBYjs7c0JBRUVwMEIsQ0FBRixHQUFNLE1BQU9paUIsU0FBUyxFQUF0Qjs7NkJBRUEsQ0FBVSxJQUFWLEVBQWdCaGtCLE1BQWhCLENBQXVCLE1BQXZCLEVBQ0tWLElBREwsQ0FDVSxPQURWLEVBQ21CLFVBQVVxQixDQUFWLEVBQWE7K0JBQVVBLEVBQUUvQixjQUFGLENBQWlCLFVBQWpCLElBQStCK0IsRUFBRThTLFFBQUYsR0FBYSxFQUE1QyxHQUFpRCxHQUF6RDtxQkFEbEM7NkJBRUEsQ0FBVSxJQUFWLEVBQWdCelQsTUFBaEIsQ0FBdUIsUUFBdkIsRUFDS1YsSUFETCxDQUNVLFdBRFYsRUFDdUIsVUFBVXFCLENBQVYsRUFBYTsrQkFBUyxnQkFBZ0JBLEVBQUUvQixjQUFGLENBQWlCLFVBQWpCLElBQWdDK0IsRUFBRThTLFFBQUYsR0FBYSxFQUFkLEdBQW9CLENBQW5ELEdBQXVELEdBQXZFLElBQThFLEdBQTlFLEdBQW9GLENBQXBGLEdBQXdGLEdBQS9GO3FCQUR0Qzs7YUFuQk47Ozs7OEJBeUJFdEQsVUFBVXZLLE1BQU03RixNQUFNOztxQkFFZmcvQixTQUFULENBQW1CQyxPQUFuQixFQUE0Qm5zQixHQUE1QixFQUFpQzs7b0JBRTNCb3NCLGVBQWVELFFBQVF2UCxRQUFSLEVBQW5CO3dCQUNRbkssUUFBUixDQUFpQjBaLFFBQVF2UCxRQUFSLEtBQXFCNWMsR0FBdEM7O29CQUVJbXNCLFFBQVF2UCxRQUFSLE1BQXVCLENBQUN3UCxlQUFlcHNCLEdBQWhCLElBQXVCLEVBQWxELEVBQXNEOzRCQUMxQ3FzQixPQUFSLENBQWdCLENBQWhCOzt1QkFFR0YsT0FBUDs7O2dCQUlJNS9CLE9BQU8sSUFBYjtnQkFDSTBSLE1BQU0vUSxLQUFLVixNQUFMLENBQVksR0FBWixDQUFWOztnQkFFSTZCLE9BQUosQ0FBWSxPQUFaLEVBQXFCLElBQXJCO2dCQUNJQSxPQUFKLENBQVksTUFBTSxLQUFLcEMsVUFBdkIsRUFBbUMsSUFBbkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7aUJBeUJLa1Msa0JBQUwsQ0FBeUJiLFFBQXpCLEVBQW1DdkssSUFBbkM7O2dCQUVJRCxPQUFPbUwsSUFBSXpSLE1BQUosQ0FBVyxNQUFYLEVBQ05DLElBRE0sQ0FDRCxPQURDLEVBQ1EsWUFEUixFQUVOQSxJQUZNLENBRUQsR0FGQyxFQUVJc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBRkosRUFHTjdJLElBSE0sQ0FHRCxHQUhDLEVBR0lzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FISixFQUlON0ksSUFKTSxDQUlELElBSkMsRUFJSyxDQUpMLEVBS05BLElBTE0sQ0FLRCxJQUxDLEVBS0ssQ0FMTCxFQU1OQSxJQU5NLENBTUQsT0FOQyxFQU1Rc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBTlIsRUFPTjdJLElBUE0sQ0FPRCxRQVBDLEVBT1NzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FQVCxDQUFYLENBN0N3Qjs7Ozs7Ozs7O2dCQTZEcEJnM0IsZUFBZSxDQUFuQjtnQkFDSUMsYUFBYSxFQUFqQjtnQkFDSUMsT0FBTyxFQUFYO2dCQUNJQyxTQUFTLEVBQWI7Z0JBQ0kxNUIsS0FBS2dZLElBQUwsSUFBYSxTQUFqQixFQUE0QjtvQkFDcEJvVixPQUFPLENBQUMsS0FBRCxFQUFPLEtBQVAsRUFBYSxLQUFiLEVBQW1CLEtBQW5CLEVBQXlCLEtBQXpCLEVBQStCLEtBQS9CLEVBQXFDLEtBQXJDLENBQWI7b0JBQ0l4TixPQUFPLElBQUloUSxJQUFKLENBQVM1UCxLQUFLaEQsS0FBTCxHQUFhLEtBQXRCLENBQVg7O29CQUVJMjhCLGVBQWUsRUFBbkI7K0JBQ2UsR0FBZjs7b0JBRUluOEIsU0FBU29pQixLQUFLaUYsTUFBTCxFQUFiO29CQUNJcm5CLFVBQVUsQ0FBVixJQUFlQSxVQUFVLENBQTdCLEVBQWdDO3lCQUN2Qjg3QixPQUFMLENBQWExWixLQUFLa0ssT0FBTCxLQUFpQnRzQixNQUE5Qjt5QkFDSyxJQUFJTixJQUFJLENBQWIsRUFBZ0JBLEtBQUtNLE1BQXJCLEVBQTZCTixHQUE3QixFQUFrQzttQ0FDbkJqRCxJQUFYLENBQWdCLEVBQUMsU0FBUSxLQUFLMmxCLEtBQUtrSyxPQUFMLEVBQUwsR0FBc0IsRUFBL0IsRUFBbUMsWUFBVyxNQUFLNXNCLElBQUV5OEIsWUFBckQsRUFBaEI7NkJBQ0sxL0IsSUFBTCxDQUFVLEVBQUMsU0FBUTAvQixZQUFULEVBQXVCLFNBQVF2TSxLQUFLeE4sS0FBS2lGLE1BQUwsRUFBTCxDQUEvQixFQUFvRCxZQUFXLE1BQU80VSxLQUFLNTdCLE1BQUwsR0FBWTg3QixZQUFuQixHQUFvQ0EsZUFBYSxDQUFoSCxFQUFWOzZCQUNLTCxPQUFMLENBQWExWixLQUFLa0ssT0FBTCxLQUFpQixDQUE5Qjs7O3FCQUdILElBQUk1c0IsS0FBS00sU0FBTyxDQUFyQixFQUF5Qk4sS0FBSSxFQUE3QixFQUFpQ0EsSUFBakMsRUFBc0M7K0JBQ3ZCakQsSUFBWCxDQUFnQixFQUFDLFNBQVEsS0FBSzJsQixLQUFLa0ssT0FBTCxFQUFMLEdBQXNCLEVBQS9CLEVBQW1DLFlBQVcsTUFBSzVzQixLQUFFeThCLFlBQXJELEVBQWhCO3lCQUNLMS9CLElBQUwsQ0FBVSxFQUFDLFNBQVEwL0IsWUFBVCxFQUF1QixTQUFRdk0sS0FBS3hOLEtBQUtpRixNQUFMLEVBQUwsQ0FBL0IsRUFBb0QsWUFBVyxNQUFPNFUsS0FBSzU3QixNQUFMLEdBQVk4N0IsWUFBbkIsR0FBb0NBLGVBQWEsQ0FBaEgsRUFBVjt5QkFDS0wsT0FBTCxDQUFhMVosS0FBS2tLLE9BQUwsS0FBaUIsQ0FBOUI7d0JBQ0lsSyxLQUFLa0ssT0FBTCxNQUFrQixDQUF0QixFQUF5Qjs7OztvQkFJeEJ0c0IsTUFBRCxHQUFXLENBQWYsRUFBa0I7d0JBQ1Z2QyxTQUFRNDlCLE1BQU9qWixJQUFQLEVBQWF1UCxRQUFiLENBQXNCLENBQXRCLEVBQXlCLE9BQXpCLEVBQWtDOWUsTUFBbEMsQ0FBeUMsV0FBekMsQ0FBWjsyQkFDT3BXLElBQVAsQ0FBWSxFQUFDLFNBQVMwL0IsZ0JBQWNuOEIsU0FBTyxDQUFyQixDQUFWLEVBQW9DLFNBQVN2QyxNQUE3QyxFQUFvRCxTQUFRLFFBQTVELEVBQXNFLFlBQVcsTUFBTzArQixlQUFhLENBQXJHLEVBQVo7O29CQUVBMStCLFFBQVE0OUIsTUFBT2paLElBQVAsRUFBYXZQLE1BQWIsQ0FBb0IsV0FBcEIsQ0FBWjs7dUJBRU9wVyxJQUFQLENBQVksRUFBQyxTQUFTMC9CLGdCQUFjRixLQUFLNTdCLE1BQUwsR0FBWUwsTUFBWixHQUFtQixDQUFqQyxDQUFWLEVBQWdELFNBQVN2QyxLQUF6RCxFQUFnRSxTQUFRLFFBQXhFLEVBQWtGLFlBQVcsTUFBTyxDQUFDdUMsU0FBTyxDQUFSLElBQVdtOEIsWUFBbEIsR0FBbUNBLGVBQWEsQ0FBN0ksRUFBWjs7cUJBRUtGLElBQUwsR0FBWUEsSUFBWjthQWhDRixNQWtDTyxJQUFJejVCLEtBQUtnWSxJQUFMLElBQWEsV0FBakIsRUFBOEI7O29CQUU3QmtWLFVBQVUsQ0FBQyxJQUFELEVBQU0sSUFBTixFQUFXLElBQVgsRUFBZ0IsSUFBaEIsQ0FBaEI7b0JBQ005TyxTQUFTLENBQUMsS0FBRCxFQUFPLEtBQVAsRUFBYSxLQUFiLEVBQW1CLEtBQW5CLEVBQXlCLEtBQXpCLEVBQStCLEtBQS9CLEVBQXFDLEtBQXJDLEVBQTJDLEtBQTNDLEVBQWlELEtBQWpELEVBQXVELEtBQXZELEVBQTZELEtBQTdELEVBQW1FLEtBQW5FLENBQWY7dUJBQ08sSUFBSXhPLElBQUosQ0FBUzVQLEtBQUtoRCxLQUFMLEdBQWEsS0FBdEIsQ0FBUDs7b0JBRUk0OEIsV0FBVyxJQUFFLENBQWpCLENBTm1DOzsrQkFRcEJBLFdBQVcsRUFBMUI7O3FCQUVLLElBQUkxOEIsTUFBSSxDQUFiLEVBQWdCQSxNQUFJMDhCLFFBQXBCLEVBQThCMThCLEtBQTlCLEVBQW1DOytCQUNwQmpELElBQVgsQ0FBZ0IsRUFBQyxTQUFRLEtBQUtta0IsT0FBT3dCLEtBQUtpSyxRQUFMLEVBQVAsQ0FBTCxHQUErQixFQUF4QyxFQUE0QyxZQUFXLE9BQU0zc0IsTUFBRSxFQUFILEdBQU8sRUFBWixDQUF2RCxFQUFoQjt3QkFDSTBpQixLQUFLaUssUUFBTCxLQUFnQixDQUFoQixJQUFxQixDQUF6QixFQUE0Qjs2QkFDbkI1dkIsSUFBTCxDQUFVLEVBQUMsU0FBUSxFQUFULEVBQWEsU0FBUWl6QixRQUFRdnZCLEtBQUtrVyxLQUFMLENBQVcrTCxLQUFLaUssUUFBTCxLQUFnQixDQUEzQixDQUFSLENBQXJCLEVBQTZELFlBQVcsTUFBTzRQLEtBQUs1N0IsTUFBTCxHQUFZLEVBQTNGLEVBQVY7NEJBQ0kraEIsS0FBS2lLLFFBQUwsS0FBZ0IsRUFBaEIsSUFBc0IsQ0FBMUIsRUFBNkI7Z0NBQ3BCK1AsV0FBVzE4QixHQUFaLEdBQWlCLEVBQXJCLEVBQXlCO3VDQUNkakQsSUFBUCxDQUFZLEVBQUMsU0FBUyxNQUFJMi9CLFdBQVMxOEIsR0FBYixDQUFELEdBQWtCLENBQTNCLEVBQThCLFNBQVMwaUIsS0FBS3VCLFdBQUwsRUFBdkMsRUFBMkQsU0FBUSxRQUFuRSxFQUE2RSxZQUFXLE1BQU8sQ0FBQ3NZLEtBQUs1N0IsTUFBTCxHQUFZLENBQWIsSUFBZ0IsRUFBL0csRUFBWjs2QkFESixNQUVPO3VDQUNJNUQsSUFBUCxDQUFZLEVBQUMsU0FBUyxLQUFHLENBQUgsR0FBSyxDQUFOLEdBQVMsQ0FBbEIsRUFBcUIsU0FBUzJsQixLQUFLdUIsV0FBTCxFQUE5QixFQUFrRCxTQUFRLFFBQTFELEVBQW9FLFlBQVcsTUFBTyxDQUFDc1ksS0FBSzU3QixNQUFMLEdBQVksQ0FBYixJQUFnQixFQUF0RyxFQUFaOzs7OzhCQUlGK2hCLElBQVYsRUFBZ0IsQ0FBaEI7OztxQkFHQzZaLElBQUwsR0FBWUEsSUFBWjs7O2dCQUlFai9CLFNBQUosQ0FBYyxNQUFkLEVBQXNCQyxJQUF0QixDQUEyQisrQixVQUEzQixFQUF1QzcrQixLQUF2QyxHQUErQ2xCLE1BQS9DLENBQXNELE1BQXRELEVBQ0tDLElBREwsQ0FDVSxhQURWLEVBQ3lCLFFBRHpCLEVBRUtBLElBRkwsQ0FFVSxXQUZWLEVBRXVCLFVBQVVxQixDQUFWLEVBQVltQyxDQUFaLEVBQWU7dUJBQVMsZUFBZ0JuQyxFQUFFOEIsUUFBbEIsR0FBOEIsTUFBckM7YUFGeEMsRUFHSy9CLElBSEwsQ0FHVSxVQUFTQyxDQUFULEVBQVk7dUJBQVNBLEVBQUVxTCxLQUFUO2FBSHhCOztnQkFLSXl6QixRQUFRLENBQVo7Z0JBQ0lyL0IsU0FBSixDQUFjLE1BQWQsRUFBc0JDLElBQXRCLENBQTJCdUYsS0FBS3c1QixVQUFoQyxFQUE0QzcrQixLQUE1QyxHQUFvRGxCLE1BQXBELENBQTJELE1BQTNELEVBQ0tDLElBREwsQ0FDVSxJQURWLEVBQ2dCLFVBQVVxQixDQUFWLEVBQVltQyxDQUFaLEVBQWU7dUJBQVMsTUFBT3E4QixlQUFlcjhCLENBQTdCO2FBRGpDLEVBRUt4RCxJQUZMLENBRVUsSUFGVixFQUVnQixVQUFVcUIsQ0FBVixFQUFZbUMsQ0FBWixFQUFlO3VCQUFTLE1BQU9xOEIsZUFBZXI4QixDQUE3QjthQUZqQyxFQUdLeEQsSUFITCxDQUdVLElBSFYsRUFHZ0JzRyxLQUFLdUMsbUJBQUwsQ0FBeUIsQ0FBekIsQ0FIaEIsRUFJSzdJLElBSkwsQ0FJVSxJQUpWLEVBSWdCc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLENBSmhCLEVBS0s3SSxJQUxMLENBS1UsUUFMVixFQUtvQixNQUxwQixFQU1LQSxJQU5MLENBTVUsY0FOVixFQU0wQixHQU4xQjs7Z0JBUU1vZ0MsU0FBUzV1QixJQUFJelIsTUFBSixDQUFXLEdBQVgsRUFBZ0JlLFNBQWhCLENBQTBCLE1BQTFCLEVBQWtDQyxJQUFsQyxDQUF1Q2kvQixNQUF2QyxFQUErQy8rQixLQUEvQyxFQUFmOzttQkFHR2xCLE1BREgsQ0FDVSxNQURWLEVBRUdDLElBRkgsQ0FFUSxJQUZSLEVBRWMsQ0FGZCxFQUdHQSxJQUhILENBR1EsT0FIUixFQUdpQixVQUFVcUIsQ0FBVixFQUFhO3VCQUFTQSxFQUFFdUIsS0FBVDthQUhoQyxFQUlHNUMsSUFKSCxDQUlRLFFBSlIsRUFJa0IsRUFKbEIsRUFLR0EsSUFMSCxDQUtRLE9BTFIsRUFLaUIsVUFBVXFCLENBQVYsRUFBYTt1QkFBU0EsRUFBRXNKLEtBQVQ7YUFMaEMsRUFNRzNLLElBTkgsQ0FNUSxHQU5SLEVBTWEsRUFOYixFQU9HQSxJQVBILENBT1EsR0FQUixFQU9hLFVBQVVxQixDQUFWLEVBQWFtQyxDQUFiLEVBQWdCO3VCQUFTbkMsRUFBRThCLFFBQVQ7YUFQL0I7O21CQVVHcEQsTUFESCxDQUNVLE1BRFYsRUFFR0MsSUFGSCxDQUVRLGFBRlIsRUFFdUIsUUFGdkIsRUFHRzRCLE9BSEgsQ0FHVyxRQUhYLEVBR3FCLElBSHJCLEVBSUc1QixJQUpILENBSVEsV0FKUixFQUlxQixVQUFVcUIsQ0FBVixFQUFZbUMsQ0FBWixFQUFlO3VCQUFTLGdCQUFpQm5DLEVBQUV1QixLQUFGLEdBQVEsQ0FBVCxHQUFldkIsRUFBRThCLFFBQWpDLElBQThDLE1BQXJEO2FBSnRDLEVBS0cvQixJQUxILENBS1EsVUFBU0MsQ0FBVCxFQUFZO3VCQUFTQSxFQUFFcUwsS0FBVDthQUx0Qjs7Z0JBUUkzTSxNQUFKLENBQVcsR0FBWCxFQUFnQmUsU0FBaEIsQ0FBMEIsTUFBMUIsRUFBa0NDLElBQWxDLENBQXVDZy9CLElBQXZDLEVBQTZDOStCLEtBQTdDLEdBQXFEbEIsTUFBckQsQ0FBNEQsTUFBNUQsRUFDR0MsSUFESCxDQUNRLE9BRFIsRUFDaUIsVUFBVXFCLENBQVYsRUFBYTt1QkFBU0EsRUFBRXVCLEtBQVQ7YUFEaEMsRUFFRzVDLElBRkgsQ0FFUSxRQUZSLEVBRWtCc0csS0FBS3VDLG1CQUFMLENBQXlCLENBQXpCLElBQThCLEVBRmhELEVBR0c3SSxJQUhILENBR1EsT0FIUixFQUdpQixVQUFVcUIsQ0FBVixFQUFhO3VCQUFTQSxFQUFFc0osS0FBVDthQUhoQyxFQUlHM0ssSUFKSCxDQUlRLEdBSlIsRUFJYXNHLEtBQUt1QyxtQkFBTCxDQUF5QixDQUF6QixJQUE4QixFQUozQyxFQUtHN0ksSUFMSCxDQUtRLEdBTFIsRUFLYSxVQUFVcUIsQ0FBVixFQUFhbUMsQ0FBYixFQUFnQjt1QkFBU25DLEVBQUU4QixRQUFUO2FBTC9COzs7O0VBdk1lb1AsVUFnTnJCOzs7O0lDak5NM0I7OztvQkFDUXBSLFVBQVosRUFBd0I7OzttSEFDaEJBLFVBRGdCOztjQUVqQkEsVUFBTCxHQUFrQkEsVUFBbEI7Ozs7Ozs4QkFHSXFSLFVBQVV2SyxNQUFNN0YsTUFBTTtnQkFDbEJYLE9BQU8sSUFBYjtnQkFDSTBSLE1BQU0vUSxLQUFLVixNQUFMLENBQVksR0FBWixDQUFWOztnQkFFSTZCLE9BQUosQ0FBWSxPQUFaLEVBQXFCLElBQXJCO2dCQUNJQSxPQUFKLENBQVksTUFBTSxLQUFLcEMsVUFBdkIsRUFBbUMsSUFBbkM7O2dCQUVJbVMsS0FBS2QsU0FBUzdRLElBQVQsQ0FBYyxTQUFkLEVBQXlCOE8sS0FBekIsQ0FBK0IsR0FBL0IsQ0FBVDs7Z0JBRUk4QyxZQUFZRCxHQUFHLENBQUgsQ0FBaEI7Z0JBQ0lFLGFBQWFGLEdBQUcsQ0FBSCxDQUFqQjs7aUJBRU0sSUFBSUssSUFBSSxDQUFkLEVBQWlCQSxJQUFJLENBQXJCLEVBQXdCQSxHQUF4QixFQUE2QjtvQkFDckIsT0FBTzFMLEtBQUt3QyxTQUFMLENBQWVrSixDQUFmLENBQVAsSUFBNEIsUUFBaEMsRUFBMEM7Ozt5QkFHbkNsSixTQUFMLENBQWVrSixDQUFmLElBQW9CQyxLQUFLM0wsS0FBS3dDLFNBQUwsQ0FBZWtKLENBQWYsQ0FBTCxDQUFwQjs7OztnQkFJRjNMLE9BQU9tTCxJQUFJelIsTUFBSixDQUFXLE1BQVgsRUFDTmEsS0FETSxDQUNBLFFBREEsRUFDVSxtQkFEVixFQUVOWixJQUZNLENBRUQsR0FGQyxFQUVJc0csS0FBS3dDLFNBQUwsQ0FBZSxDQUFmLENBRkosRUFHTjlJLElBSE0sQ0FHRCxHQUhDLEVBR0lzRyxLQUFLd0MsU0FBTCxDQUFlLENBQWYsQ0FISixFQUlOOUksSUFKTSxDQUlELE9BSkMsRUFJUXNHLEtBQUt3QyxTQUFMLENBQWUsQ0FBZixDQUpSLEVBS045SSxJQUxNLENBS0QsUUFMQyxFQUtTc0csS0FBS3dDLFNBQUwsQ0FBZSxDQUFmLENBTFQsQ0FBWCxDQXBCd0I7Ozs7bUNBNkJoQitCLE9BQU87Ozs7Ozs7Ozs7aUJBVVZzSSxPQUFMLENBQWF0SSxLQUFiLEVBQW9CLENBQXBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztnQ0FtRUtBLE9BQU93SSxPQUFPO2dCQUNmQyxNQUFNekksTUFBTTFHLE1BQWhCO2lCQUNNLElBQUlvUCxNQUFNLENBQVYsRUFBWTlRLElBQUksQ0FBdEIsRUFBMEJBLElBQUlvSSxNQUFNMUcsTUFBcEMsRUFBNEMxQixHQUE1QyxFQUFpRDtvQkFDdkN3TCxLQUFLcEQsTUFBTXBJLENBQU4sQ0FBWDtvQkFDSXdMLEdBQUdqSCxJQUFILElBQVcsT0FBWCxJQUFzQnFNLFFBQVEsQ0FBbEMsRUFBcUM7dUJBQzlCcE8sTUFBSCxHQUFZLEVBQUUsT0FBTXNPLEdBQVIsRUFBWSxTQUFRRCxHQUFwQixFQUF3QixPQUFNLFFBQU1ELEtBQXBDLEVBQTJDLFVBQVNBLEtBQXBELEVBQTJELFNBQVFBLEtBQW5FLEVBQVo7O3dCQUVJcEYsR0FBRzNFLFFBQVAsRUFBaUI7NkJBQ1I2SixPQUFMLENBQWFsRixHQUFHM0UsUUFBaEIsRUFBMEIrSixRQUFNLENBQWhDOzs7Ozs7O3FDQU1IeEMsVUFBVTJDLGVBQWUxTCxPQUFPbEksUUFBUTtnQkFDN0NFLE9BQU8sSUFBWDs7Z0JBRUl1SixXQUFXbUssY0FBY2hOLE1BQWQsQ0FBcUIsVUFBU25GLENBQVQsRUFBWTt1QkFBU0EsRUFBRTJGLElBQUYsSUFBVSxNQUFqQjthQUFuQyxDQUFmOztnQkFFSUcsUUFBUXZILE9BQU91SCxLQUFuQjs7OzBCQUlLbkgsSUFETCxDQUNVLElBRFYsRUFDZ0IsVUFBU3FCLENBQVQsRUFBWTtvQkFBUTBILE1BQU1qSixLQUFLZ0ssTUFBTCxDQUFZekksQ0FBWixFQUFleUcsS0FBZixDQUFaLENBQW1DLE9BQU96RyxFQUFFb0IsQ0FBRixHQUFPcEIsRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVvQixDQUFSLEdBQWFzRyxJQUFJQyxFQUFKLEdBQVMsQ0FBQ0QsSUFBSUcsRUFBSixHQUFPSCxJQUFJQyxFQUFaLElBQWdCLENBQXBEO2FBRGpFLEVBRUtoSixJQUZMLENBRVUsSUFGVixFQUVnQixVQUFTcUIsQ0FBVCxFQUFZO29CQUFRMEgsTUFBTWpKLEtBQUtnSyxNQUFMLENBQVl6SSxDQUFaLEVBQWV5RyxLQUFmLENBQVosQ0FBbUMsT0FBT3pHLEVBQUVzQixDQUFGLEdBQU90QixFQUFFc0IsQ0FBRixHQUFNdEIsRUFBRXNCLENBQVIsR0FBYW9HLElBQUlFLEVBQUosR0FBUyxDQUFDRixJQUFJSSxFQUFKLEdBQU9KLElBQUlFLEVBQVosSUFBZ0IsQ0FBcEQ7YUFGakU7OztxQkFNRzlJLEVBREgsQ0FDTSxZQUROLEVBQ29CLFVBQVVDLENBQVYsRUFBYTs7O2FBRGpDLEVBS0dELEVBTEgsQ0FLTSxZQUxOLEVBS29CLFVBQVVDLENBQVYsRUFBYTs7YUFMakMsRUFRR0QsRUFSSCxDQVFNLE9BUk4sRUFRZSxVQUFVQyxDQUFWLEVBQWE7d0JBQ3RCLENBQVNpUSxlQUFUO3FCQUNLZ3dCLFdBQUwsQ0FBaUJ6Z0MsTUFBakIsRUFBeUIsS0FBSytCLFVBQTlCLEVBQTJDdkIsRUFBRW1KLElBQUYsSUFBVSxNQUFyRCxFQUZzQjthQVI1Qjs7cUJBYVd5SCxXQUFULENBQXFCM1AsQ0FBckIsRUFBd0I7b0JBQ2hCLENBQUNnRCxRQUFBLENBQVM0TSxNQUFkLEVBQXNCOUosTUFBTStKLFdBQU4sQ0FBa0IsR0FBbEIsRUFBdUJDLE9BQXZCO2tCQUNwQnBILEVBQUYsR0FBTzFJLEVBQUVvQixDQUFUO2tCQUNFd0gsRUFBRixHQUFPNUksRUFBRXNCLENBQVQ7dUJBQ09tTyxLQUFQLEdBQWU3TSxLQUFLeUssS0FBTCxDQUFXck4sRUFBRW9CLENBQUYsR0FBTXBCLEVBQUVzQixDQUFuQixDQUFmOzs7cUJBR0t5TyxPQUFULENBQWlCL1AsQ0FBakIsRUFBb0I7a0JBQ2QwSSxFQUFGLEdBQU8xRixRQUFBLENBQVM1QixDQUFoQjtrQkFDRXdILEVBQUYsR0FBTzVGLFFBQUEsQ0FBUzFCLENBQWhCOzs7cUJBR0swTyxTQUFULENBQW1CaFEsQ0FBbkIsRUFBc0I7b0JBQ2QsQ0FBQ2dELFFBQUEsQ0FBUzRNLE1BQWQsRUFBc0I5SixNQUFNK0osV0FBTixDQUFrQixDQUFsQjtrQkFDcEJuSCxFQUFGLEdBQU8sSUFBUDtrQkFDRUUsRUFBRixHQUFPLElBQVA7MkJBQ1ksWUFBWTsyQkFDYnpJLE9BQVAsQ0FBZSxRQUFmO2lCQURKLEVBRUcsR0FGSDs7OzBCQUtVOFAsSUFBZCxDQUFtQmpOLE9BQUEsR0FDZmxFLEVBRGUsQ0FDWixPQURZLEVBQ0g2USxXQURHLEVBRWY3USxFQUZlLENBRVosTUFGWSxFQUVKaVIsT0FGSSxFQUdmalIsRUFIZSxDQUdaLEtBSFksRUFHTGtSLFNBSEssQ0FBbkI7O3FCQUtTblAsSUFBVCxDQUFjLFVBQVViLENBQVYsRUFBYW1DLENBQWIsRUFBZ0I7b0JBQ3RCQSxLQUFLLENBQVQsRUFBWTt5QkFDSDY4QixXQUFMLENBQWtCemdDLE1BQWxCLEVBQTBCLEtBQUsrQixVQUEvQixFQUEyQyxJQUEzQzs7YUFGUjs7OzsrQkFPQ04sR0FBR3lHLE9BQU87Z0JBQ1BwRyxRQUFRTCxFQUFFeUYsUUFBRixDQUFXOEIsVUFBWCxDQUFzQixDQUF0QixJQUEyQixFQUF6QztnQkFDSWQsTUFBTTNELE1BQU4sSUFBZ0J6QyxLQUFwQixFQUEyQjs7O2dCQUdyQjJFLE9BQU95QixNQUFNcEcsS0FBTixFQUFhb0gsU0FBMUI7Z0JBQ01DLE1BQU0sRUFBQ0MsSUFBRzNDLEtBQUssQ0FBTCxDQUFKLEVBQVk0QyxJQUFHNUMsS0FBSyxDQUFMLENBQWYsRUFBdUI2QyxJQUFHN0MsS0FBSyxDQUFMLElBQVVBLEtBQUssQ0FBTCxDQUFwQyxFQUE0QzhDLElBQUc5QyxLQUFLLENBQUwsSUFBVUEsS0FBSyxDQUFMLENBQXpELEVBQVo7O21CQUVPMEMsR0FBUDs7OztvQ0FHU25KLFFBQVFhLE1BQU02L0IsTUFBTTtnQkFDMUJDLFNBQVMsRUFBYjtxQkFDQSxDQUFVOS9CLElBQVYsRUFBZ0JLLFNBQWhCLENBQTBCLFVBQTFCLEVBQXNDb0IsSUFBdEMsQ0FBNkMsVUFBVWIsQ0FBVixFQUFhO3VCQUNoRGQsSUFBUCxDQUFZYyxDQUFaO3lCQUNBLENBQVVaLElBQVYsRUFBZ0JLLFNBQWhCLENBQTBCLFlBQVlPLEVBQUVrSSxJQUF4QyxFQUE4QzNJLEtBQTlDLENBQW9ELFNBQXBELEVBQStELE1BQS9EO2FBRkg7O2dCQUtJaEIsT0FBTzRnQyxXQUFQLElBQXNCLENBQTFCLEVBQTZCO29CQUN0QnovQixRQUFPdy9CLE9BQU8zZ0MsT0FBTzRnQyxXQUFkLENBQVg7eUJBQ0EsQ0FBVS8vQixJQUFWLEVBQWdCSyxTQUFoQixDQUEwQixZQUFZQyxNQUFLd0ksSUFBM0MsRUFBaUQzSSxLQUFqRCxDQUF1RCxTQUF2RCxFQUFrRSxNQUFsRTthQUZILE1BR087dUJBQ0c0L0IsV0FBUCxHQUFxQixDQUFDLENBQXRCOzs7Z0JBR0NGLElBQUosRUFBVTt1QkFDQ0UsV0FBUDtvQkFDSTVnQyxPQUFPNGdDLFdBQVAsR0FBcUIsQ0FBekIsRUFBNEI7MkJBQ2xCQSxXQUFQLEdBQXFCLENBQXJCLENBRHlCOzthQUZoQyxNQUtPO3VCQUNJQSxXQUFQO29CQUNJNWdDLE9BQU80Z0MsV0FBUCxJQUFzQkQsT0FBT3A4QixNQUFqQyxFQUF5QzsyQkFDL0JxOEIsV0FBUCxHQUFzQkQsT0FBT3A4QixNQUFQLEdBQWdCLENBQXRDOzs7Z0JBR0hwRCxPQUFPdy9CLE9BQU8zZ0MsT0FBTzRnQyxXQUFkLENBQVg7cUJBQ0EsQ0FBVS8vQixJQUFWLEVBQWdCSyxTQUFoQixDQUEwQixZQUFZQyxLQUFLd0ksSUFBM0MsRUFBaUQzSSxLQUFqRCxDQUF1RCxTQUF2RCxFQUFrRSxPQUFsRTs7aUJBRUs2L0IscUJBQUwsQ0FBMkI3Z0MsTUFBM0IsRUFBbUNhLElBQW5DLEVBQXlDOC9CLE1BQXpDOzs7OzhDQUdvQjNnQyxRQUFRYSxNQUFNOC9CLFFBQVE7cUJBQzFDLENBQVU5L0IsSUFBVixFQUFnQkMsTUFBaEIsQ0FBdUIsYUFBdkIsRUFBc0NTLE1BQXRDOztxQkFFQSxDQUFVVixJQUFWLEVBQWdCVixNQUFoQixDQUF1QixHQUF2QixFQUE0QjZCLE9BQTVCLENBQW9DLFdBQXBDLEVBQWlELElBQWpELEVBQXVEZCxTQUF2RCxDQUFpRSxNQUFqRSxFQUNJQyxJQURKLENBQ1N3L0IsTUFEVCxFQUVJdC9CLEtBRkosR0FHSWxCLE1BSEosQ0FHVyxNQUhYLEVBSUk2QixPQUpKLENBSVksS0FKWixFQUltQixJQUpuQixFQUtJaEIsS0FMSixDQUtVLE1BTFYsRUFLa0IsT0FMbEIsRUFNSWdCLE9BTkosQ0FNWSxVQU5aLEVBTXdCLFVBQVVhLENBQVYsRUFBYWUsQ0FBYixFQUFnQjt1QkFBVUEsS0FBSzVELE9BQU80Z0MsV0FBWixHQUEwQixJQUExQixHQUErQixLQUF2QzthQU4xQyxFQU9JeGdDLElBUEosQ0FPUyxXQVBULEVBT3NCLFVBQVV5QyxDQUFWLEVBQWFlLENBQWIsRUFBZ0I7dUJBQVMsZUFBZSxFQUFmLEdBQW9CLEdBQXBCLElBQTJCLEtBQUlBLElBQUUsRUFBakMsSUFBd0MsR0FBL0M7YUFQeEMsRUFRSXBDLElBUkosQ0FRUyxVQUFVcUIsQ0FBVixFQUFhO3VCQUFTQSxFQUFFaUssS0FBVDthQVJ4Qjs7cUJBVUEsQ0FBVWpNLElBQVYsRUFBZ0JLLFNBQWhCLENBQTBCLE1BQTFCLEVBQ0lDLElBREosQ0FDU3cvQixNQURULEVBRUlyL0IsSUFGSixHQUVXQyxNQUZYOzs7O3lDQU1ldkIsUUFBUVEsR0FBR2IsUUFBUTtvQkFDbEMsQ0FBUzhRLGVBQVQ7O2dCQUVJelEsT0FBT2tSLEtBQVAsSUFBZ0I3TSxLQUFLeUssS0FBTCxDQUFXdE8sRUFBRXFDLENBQUYsR0FBTXJDLEVBQUV1QyxDQUFuQixDQUFwQixFQUEyQzt1QkFDakNnRixVQUFQLENBQWtCb0osTUFBbEIsQ0FBeUIzUSxDQUF6QixFQUE0QmlFLFNBQUEsQ0FBVTlFLE1BQVYsQ0FBNUI7YUFESCxNQUVPO3VCQUNHb0ksVUFBUCxDQUFrQnRILEtBQWxCLENBQXdCRCxDQUF4Qjs7Ozs7aUNBSUd5USxVQUFVekksV0FBV04sT0FBTzs7O0VBaFFsQnlLLFVBb1FyQjs7QUMxUUE7OztBQUdBLEFBQ0EsQUFDQSxBQUVBLEFBQU8sU0FBU211QixJQUFULENBQWNuM0IsSUFBZCxFQUFvQjtvQkFBZ0JBLEtBQUs0VCxXQUFMLEVBQWQ7OztJQUVSd2pCLE9BQ2pCLGdCQUFjOzs7O0FBSWxCLEFBRUEsQUFLQSxBQUNBbGhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJQyxLQUFKLENBQWlCLFNBQWpCLENBQXRCOztBQUVBLEFBQ0FwaEMsYUFBYW1oQyxRQUFiLENBQXNCLElBQUlFLGNBQUosQ0FBcUIsYUFBckIsQ0FBdEI7O0FBRUEsQUFDQXJoQyxhQUFhbWhDLFFBQWIsQ0FBc0IsSUFBSUcsT0FBSixDQUFTLE1BQVQsQ0FBdEI7QUFDQSxBQUNBdGhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJSSxPQUFKLENBQVUsT0FBVixDQUF0Qjs7QUFFQSxBQUNBdmhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJSyxPQUFKLENBQVcsUUFBWCxDQUF0QjtBQUNBeGhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJSyxPQUFKLENBQVcsU0FBWCxDQUF0QjtBQUNBeGhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJSyxPQUFKLENBQVcsU0FBWCxDQUF0Qjs7QUFFQSxBQUNBeGhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJTSxPQUFKLENBQWMsV0FBZCxDQUF0Qjs7QUFHQSxBQUNBemhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJTyxPQUFKLENBQVcsUUFBWCxDQUF0Qjs7QUFFQSxBQUNBMWhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJUSxPQUFKLENBQWEsVUFBYixDQUF0Qjs7QUFFQSxBQUNBM2hDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJUyxPQUFKLENBQWMsV0FBZCxDQUF0Qjs7QUFFQSxBQUNBNWhDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJVSxPQUFKLENBQVUsT0FBVixDQUF0Qjs7QUFFQSxBQUNBN2hDLGFBQWFtaEMsUUFBYixDQUFzQixJQUFJVyxPQUFKLENBQWlCLEtBQWpCLENBQXRCOztBQUVBLEFBQ0E5aEMsYUFBYW1oQyxRQUFiLENBQXNCLElBQUlZLFFBQUosQ0FBYSxLQUFiLENBQXRCOztBQUdBLEFBQ0F0OEIsY0FBYzA3QixRQUFkLENBQXVCLElBQUlhLE1BQUosQ0FBYSxVQUFiLENBQXZCOztBQUVBLEFBQ0F2OEIsY0FBYzA3QixRQUFkLENBQXVCLElBQUljLFFBQUosQ0FBa0IsU0FBbEIsQ0FBdkI7O0FBRUEsQUFDQXg4QixjQUFjMDdCLFFBQWQsQ0FBdUIsSUFBSWUsUUFBSixDQUFjLE1BQWQsQ0FBdkI7O0FBRUEsQUFDQXo4QixjQUFjMDdCLFFBQWQsQ0FBdUIsSUFBSWdCLFFBQUosQ0FBbUIsV0FBbkIsQ0FBdkI7O0FBRUEsQUFDQTE4QixjQUFjMDdCLFFBQWQsQ0FBdUIsSUFBSWlCLFFBQUosQ0FBbUIsVUFBbkIsQ0FBdkI7Ozs7Ozs7OzsifQ==
