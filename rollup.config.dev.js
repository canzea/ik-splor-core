//import ascii from "rollup-plugin-ascii";
import resolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import eslint from "rollup-plugin-eslint";
import replace from "rollup-plugin-replace";

import commonjs from 'rollup-plugin-commonjs';

//import uglify from 'rollup-plugin-uglify';
import postcss from 'rollup-plugin-postcss';

import simplevars from 'postcss-simple-vars';
import nested from 'postcss-nested';
import cssnext from 'postcss-cssnext';
import cssnano from 'cssnano';

import serve from 'rollup-plugin-serve'


export default {
  moduleName: 'iksplor',
  entry: 'test/bootstrap.js',
  dest: 'build/iksplor-test.js',
  format: 'iife',
  sourceMap: 'inline',
  plugins: [
    resolve({
      jsnext: true,
      main: true,
      browser: true
    }),
    postcss({
       plugins: [
         simplevars(),
         nested(),
         cssnext({ warnForDuplicates: false, }),
         cssnano(),
       ],
       extensions: [ '.scss', '.css' ],
    }),
    commonjs({
      include: 'node_modules/**',
      sourceMap: 'inline',
      exclude: [ 'node_modules/moment/**' ]
    }),
    eslint({
      exclude: [
        'src/**/*.scss',
        'test/**/*.scss'
      ],
      fix: true
    }),
    replace({
       'process.env.NODE_ENV': JSON.stringify( 'development' )
    }),
    babel({
      babelrc: false,
      exclude: 'node_modules/**',
      presets: [ 'es2015-rollup', 'stage-1', 'react' ],
      plugins: [ 'external-helpers' ]
    }),
    serve({ contentBase: 'build', port: 3000 }),
  ]
};
